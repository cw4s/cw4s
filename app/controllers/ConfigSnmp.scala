package controllers

import play.api.mvc.{MessagesControllerComponents, MessagesAbstractController}
import play.api.data.Form
import play.api.data.Forms._
import play.filters.csrf.{CSRFAddToken, CSRFCheck}
import scala.collection.JavaConverters._
import models.Forms._
import to.lef.srxlib
import models.Device

import play.api.i18n._
import javax.inject.Inject
import scala.concurrent.ExecutionContext

class ConfigSnmp @Inject()(val deviceDAO:models.DeviceDAO, addToken:CSRFAddToken, checkToken:CSRFCheck, mcc:MessagesControllerComponents)(implicit ec:ExecutionContext) extends MessagesAbstractController(mcc) with models.DeviceSession {
	def snmpForm = Form(
		mapping(
		    "name" -> text,
		    "contact" -> text,
		    "location" -> text,
		    "description" -> text
		)
		(SnmpForm.apply)
		(SnmpForm.unapply)
	)
	
	def edit(deviceName:String) = addToken {
		editAction(deviceName, false)
	}
	
	def editPost(deviceName:String) = checkToken {
		editAction(deviceName, true)
	}
	
	def editAction(deviceName:String, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
			import views.html.config.snmp.{snmp => view}
			val form = snmpForm
			
			if (isPost) form.bindFromRequest.fold(
			    error => Ok(view(error)),
			    value => try {
			      device.config(value.toSnmp.getEditConfigure)
			      Redirect(routes.ConfigSnmp.edit(deviceName))
			    } catch {
			      case e:Throwable => Ok(view(form.fill(value), e.getMessage))
			    }
			) else Ok(view(form.fill(SnmpForm(device.config.snmp))))
		}
	}
	
	def communityForm(s:Option[srxlib.snmp.SnmpCommunity]) = Form(
	    mapping(
	        "name" -> s.map(v => ignored(v.getName)).getOrElse(nonEmptyText),
	        "write" -> boolean,
	        "trap" -> boolean,
	        "version" -> nonEmptyText
	    )
	    (SnmpCommunityForm.apply)
	    (SnmpCommunityForm.unapply)
	)

	def trapTargetForm = Form(
	    single(
	        "address" -> nonEmptyText.verifying("Invalid IP address.", v => srxlib.Util.isValidIPAddress(v))
	    )
	)
	
	def createCommunity(deviceName:String) = addToken {
		createCommunityAction(deviceName, false)
	}
	
	def createCommunityPost(deviceName:String) = checkToken {
		createCommunityAction(deviceName, true)
	}
	
	def createCommunityAction(deviceName:String, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  	import views.html.config.snmp.{createCommunity => view}
	  		val config = device.config
	  		val form = communityForm(None)
	  		
	  		if (isPost) form.bindFromRequest.fold(
	  		    error => Ok(view(error)),
	  		    value => try {
	  		    	device.config(value.toSnmpCommunity.getCreateConfigure)
	  		    	Redirect(routes.ConfigSnmp.edit(deviceName))
	  		    } catch {
	  		      case e:Throwable => Ok(view(form.fill(value), e.getMessage))
	  		    }
	  		) else Ok(view(form.fill(SnmpCommunityForm("", false, false, ""))))
	    }
	}
	
	def editCommunity(deviceName:String, cname:String) = addToken {
		editCommunityAction(deviceName, cname, 0)
	}
	
	def editCommunityPost(deviceName:String, cname:String) = checkToken {
		editCommunityAction(deviceName, cname, 1)
	}
	
	def editCommunityAction(deviceName:String, cname:String, op:Int) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
			val config = device.config
			config.findSnmpCommunity(cname) match {
			  case models.SnmpCommunity(v) => {
				  import views.html.config.snmp.{editCommunity => view}
				  val sv = Some(v)
				  val form = communityForm(sv)
				  val form2 = trapTargetForm
				  
				  op match {
				    case 2 => if (v.isTrapEnabled) form2.bindFromRequest.fold(
				        error => Ok(view(form.fill(SnmpCommunityForm(v)), error, sv)),
				        value => try {
				        	v.addTrapTarget(value)
				        	device.config(v.getTrapTargetConfigure)
				        	Redirect(routes.ConfigSnmp.editCommunity(deviceName, cname))
				        } catch {
				        	case e:Throwable => Ok(view(form.fill(SnmpCommunityForm(v)), form2.fill(value), sv, e.getMessage))
				        }
				    ) else Redirect(routes.ConfigSnmp.editCommunity(deviceName, cname))
				    
				    case 1 =>  form.bindFromRequest.fold(
				      error => Ok(view(error, form2.fill(""), sv)),
				      value => try {
				        device.config(value.toSnmpCommunity.getEditConfigure)
				        Redirect(routes.ConfigSnmp.edit(deviceName))
				      } catch {
				        case e:Throwable => Ok(view(form.fill(value), form2.fill(""), sv, e.getMessage))
				      }
				   	)
				   	
				    case _ => Ok(view(form.fill(SnmpCommunityForm(v)), form2.fill(""), sv))
				  }
			  }
			  case _ => Redirect(routes.ConfigSnmp.edit(deviceName))
			}
		}
	}

	def createTrapTargetPost(deviceName:String, cname:String) = checkToken { 
		editCommunityAction(deviceName, cname, 2)
	}
	
	def deleteTrapTargetPost(deviceName:String, cname:String, target:String) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
			val config = device.config
			config.findSnmpCommunity(cname) match {
			  case models.SnmpCommunity(v) if (v.isTrapEnabled) => try {
				  device.config(v.getDeleteTrapTargetConfigure(target))
				  Redirect(routes.ConfigSnmp.editCommunity(deviceName, cname))
			  } catch {
			    case e:Throwable => Redirect(routes.ConfigSnmp.editCommunity(deviceName, cname))
			  }
			  
			  case _ => Redirect(routes.ConfigSnmp.editCommunity(deviceName, cname))
			}
		}
	}
	
	def deleteCommunityPost(deviceName:String, cname:String) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
			val config = device.config
			config.findSnmpCommunity(cname) match {
			  case models.SnmpCommunity(v) => try {
				  device.config(v.getDeleteConfigure)
				  Redirect(routes.ConfigSnmp.edit(deviceName))
			  } catch {
			    case e:Throwable => Redirect(routes.ConfigSnmp.edit(deviceName))
			  }
			  
			  case _ => Redirect(routes.ConfigSnmp.edit(deviceName))
			}
		}
	}
}