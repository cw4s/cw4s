package controllers

import play.api.mvc.{ControllerComponents, AbstractController}
import javax.inject.Inject

class Application @Inject()(configuration: play.api.Configuration, components:ControllerComponents) extends AbstractController(components) {

  def index = Action {
    Redirect(routes.DeviceSetup.list)
  }

  def props = Action {
    import java.util.Properties
    val props = System.getProperties()
    
    Ok(views.html.props(props))
  }
  
  def confs = Action {
	 import play.api.Play.current
	 
	 val confs = configuration
	 Ok(views.html.confs(confs))
  }
}