package controllers

import play.api.mvc.{MessagesControllerComponents, MessagesAbstractController}
import play.api.data.Form
import play.api.data.Forms._
import play.filters.csrf.{CSRFAddToken, CSRFCheck}

import scala.collection.JavaConverters._
import models.Forms._

import to.lef.srxlib._

import javax.inject.Inject
import scala.concurrent.ExecutionContext

class NetworkZones @Inject()(val deviceDAO:models.DeviceDAO, addToken:CSRFAddToken, checkToken:CSRFCheck, mcc:MessagesControllerComponents)(implicit ec:ExecutionContext) extends MessagesAbstractController(mcc) with models.DeviceSession{
	def createForm(implicit device:to.lef.srxlib.Device) = Form(
		mapping(
				"name" -> nonEmptyText.verifying("Zone is already exist", !device.config.isSecurityZoneExist(_)),
				"tcp_rst" -> boolean
		)
		(ZoneForm.apply)
		(ZoneForm.unapply)
	)

	def basicForm(zoneName:String)(implicit device:to.lef.srxlib.Device) = Form(
	    mapping(
	        "name" -> ignored(zoneName),
	        "tcp_rst" -> boolean
	    )
	    (ZoneForm.apply)
	    (ZoneForm.unapply)
    )
    
	def list(deviceName:String) = Action.async {
		withDevice(deviceName) { implicit device => 
			val zones = device.config.securityZoneList
			Ok(views.html.network.zones.list(zones.asScala.toSeq))
		}
	}

	def create(deviceName:String) = addToken {
	  Action.async { implicit req => 
		  withDevice(deviceName) { implicit device => 
		  	val form = createForm.fill(ZoneForm("", false))
		  	Ok(views.html.network.zones.create(form))
		  }
	  }
	}
	
	def createPost(deviceName:String) = checkToken {
	  Action.async { implicit req => 
	    withDevice(deviceName) { implicit device => 
		  createForm.bindFromRequest.fold(
		      formWithErrors => {
		    	  Ok(views.html.network.zones.create(formWithErrors))
		      },
		      value => {
		    	  try {
		    	    device.config(value.toSecurityZone.getCreateConfigure());
		    		Redirect(routes.NetworkZones.list(device.name))
		    	  }
		    	  catch {
		    	    case e:Throwable => {
		    	      val form = createForm.fill(value) 
    	    		  Ok(views.html.network.zones.create(form, e.getMessage()))
		    	    }
		    	  }
		      }
	      )
	    }
	  }
	}

	def editBasic(deviceName:String, zoneName:String) = addToken {
		Action.async { implicit req => 
		  	withDevice(deviceName) { implicit device =>
		  	  val config = device.config
		  		config.findSecurityZone(zoneName) match {
		  		  case models.SecurityZone(v) if (v.isEditable(config)) => { 
		  			  val form = basicForm(zoneName).fill(ZoneForm(v));
		  			  Ok(views.html.network.zones.editBasic(form, v))
		  		  }
		  		  case _ => Redirect(routes.NetworkZones.list(deviceName))
		  		}
		  	}
		}
	}

	def editBasicPost(deviceName:String, zoneName:String) = checkToken {
	  Action.async { implicit req => 
	    withDevice(deviceName) { implicit device =>
	      val config = device.config
	    	config.findSecurityZone(zoneName) match {
	    	  case models.SecurityZone(v) if (v.isEditable(config)) => {
	    		  basicForm(zoneName).bindFromRequest.fold(
	    				  error => Ok(views.html.network.zones.editBasic(error, v)),
	    				  value => try {
	    					  v.setTcpReset(value.tcpRst)
	    					  device.config(v.getBasicConfigure)
	    					  Redirect(routes.NetworkZones.list(deviceName))
	    				  }
	    				  catch {
	    				    case e:Throwable => Ok(views.html.network.zones.editBasic(basicForm(zoneName).fill(value), v, e.getMessage))
	    				  }
	    		  )
	    	  }
	    	  case _ => Redirect(routes.NetworkZones.list(deviceName))
	    	}
	    }
	  }
	}
	
	def deletePost(deviceName:String, zoneName:String) = Action.async { implicit req => 
		  withDevice(deviceName) { implicit device => 
		    val config = device.config
		  	if (config.isSecurityZoneExist(zoneName) && zone.SecurityZone.isRemovable(zoneName, config)) {
		  		try {
		  		  device.config(zone.SecurityZone.getDeleteConfigure(zoneName))
		  		}
		  		catch {
		  		  case e:Throwable => Redirect(routes.NetworkZones.list(device.name))
		  		}
		  	}
    		Redirect(routes.NetworkZones.list(device.name))
		  }
	}
}