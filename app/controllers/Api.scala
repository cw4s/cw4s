package controllers

import play.api.Logging
import play.api.mvc.{ControllerComponents, AbstractController}
import play.api.libs.json.Json

import play.api.data.Form
import play.api.data.Forms._

import play.api.routing.JavaScriptReverseRouter
import to.lef.srxlib
import javax.inject.Inject
import scala.concurrent.ExecutionContext

class Api @Inject()(val deviceDAO:models.DeviceDAO, components:ControllerComponents)(implicit ec:ExecutionContext) extends AbstractController(components) with Logging with models.DeviceSession {
	def routes = Action { implicit req =>
		Ok(
			JavaScriptReverseRouter("routes")(
					controllers.routes.javascript.Api.check,
					controllers.routes.javascript.Api.rollback,
					controllers.routes.javascript.Api.load,
					controllers.routes.javascript.Api.sync,
					controllers.routes.javascript.Api.commit,
					controllers.routes.javascript.Api.editConfig,
					controllers.routes.javascript.Api.candidateConfig,
					controllers.routes.javascript.Api.enablePolicy,
					controllers.routes.javascript.Api.refreshNetworkInterfaces,
					controllers.routes.javascript.Api.refreshNetworkPPPoE,
					controllers.routes.javascript.Api.refreshNetworkRoutes,
					controllers.routes.javascript.Api.refreshVpnMonitor,
					controllers.routes.javascript.Api.refreshSessionLog,
					controllers.routes.javascript.Api.md5crypt,
					controllers.routes.javascript.Resources.initialSetupData,
					controllers.routes.javascript.Addresses.list,
					controllers.routes.javascript.Addresses.createAddress,
					controllers.routes.javascript.Addresses.zoneGroups,
					controllers.routes.javascript.Addresses.createZoneGroup,
					controllers.routes.javascript.Modal.sourceSelector,
					controllers.routes.javascript.Modal.destinationSelector,
					controllers.routes.javascript.Modal.applicationSelector,
					controllers.routes.javascript.Policies.moveModal,
					controllers.routes.javascript.Home.index,
					controllers.routes.javascript.Reports.chassis
			)
		).as("text/javascript") 
	}

	def check(deviceName:String) = Action.async { implicit req => 
	    	withDevice(deviceName) { implicit device =>
	    	  try {
	    	    device.check
	    	    Json.obj("result" -> true)
	    	  }
	    	  catch {
	    	    case e:Throwable => {
	    	      logger.error(e.getMessage)
	    	      Json.obj("result" -> false, "error" -> e.getMessage)
	    	    }
	    	  }
	    	} map(json => Ok(json))
	}
	
	def sync(deviceName:String) = Action.async { implicit req =>
			withDevice(deviceName) { implicit device =>
			  try {
				device.syncConfig
				Json.obj("result" -> true)
			  }
			  catch {
			    case e:Throwable => Json.obj("result" -> false, "error" -> e.getMessage())
			  }
			} map(json => Ok(json))
	}
	
	def load(deviceName:String) = Action.async { implicit req =>
			withDevice(deviceName) { implicit device =>
			  try {
				device.loadConfig
				Json.obj("result" -> true)
			  }
			  catch {
			    case e:Throwable => Json.obj("result" -> false, "error" -> e.getMessage())
			  }
			} map(json => Ok(json))
	}
	
	def commit(deviceName:String) = Action.async { implicit req =>
			withDevice(deviceName) { implicit device =>
			  try {
				device.loadConfig(true)
				Json.obj("result" -> true)
			  }
			  catch {
			    case e:Throwable => Json.obj("result" -> false, "error" -> e.getMessage())
			  }
			} map(json => Ok(json))
	}
	
	def rollback(deviceName:String, idx:Int) = Action.async { implicit req =>
			withDevice(deviceName) { implicit device =>
			  try {
				device.rollbackConfig(idx)
				Json.obj("result" -> true)
			  }
			  catch {
			    case e:Throwable => Json.obj("result" -> false, "error" -> e.getMessage())
			  }
			} map(json => Ok(json))
	}
	
	def editConfig(deviceName:String) = Action.async { implicit req =>
		  withDevice(deviceName) { implicit device => 
		    device.editingConfig.toXMLString
		  } map(xml => Ok(xml))
	}
	
	def candidateConfig(deviceName:String) = Action.async { implicit req =>
		  withDevice(deviceName) { implicit device => 
		    device.candidateConfig.toXMLString
		  } map(xml => Ok(xml))
	}
	
	def enablePolicy(deviceName:String, fZname:String, tZname:String, pName:String, enabled:Boolean) = Action.async { implicit req =>
	    withDevice(deviceName) { implicit device => 
	      val config = device.config
	      config.findPolicy(fZname, tZname, pName) match {
	        case models.Policy(policy) => try {
	        	device.config(policy.getActivateConfigure(enabled))
	        	Json.obj("result" -> true)
	        } catch {
	          case e:Throwable => Json.obj("result" -> false, "error" -> e.getMessage)
	        }
	        case _ => Json.obj("result" -> false)
	      }
	    } map(json => Ok(json))
	}
	
	def refreshNetworkInterfaces(deviceName:String) = Action.async { implicit req =>
		  withDevice(deviceName) { implicit device =>
		  	import srxlib.Device.CACHE_NAME_INTERFACE_INFORMATION
		  	import srxlib.Device.CACHE_NAME_PPP_SUMMARY_INFORMATION
		  	import srxlib.Device.CACHE_UPDATE_FORCE
	  	
		  	try {
		  		device.updateCache(CACHE_UPDATE_FORCE, 
		  				CACHE_NAME_INTERFACE_INFORMATION,
		  				CACHE_NAME_PPP_SUMMARY_INFORMATION)
	  		   
		  		Json.obj("result" -> true)
		  	} catch {
		  		case e:Throwable => Json.obj("result" -> false, "error" -> e.getMessage)
		  	}
		  } map(json => Ok(json))
	}
	
	def refreshNetworkPPPoE(deviceName:String) = Action.async { implicit req =>
		  withDevice(deviceName) { implicit device =>
		  	import srxlib.Device.CACHE_NAME_PPP_SUMMARY_INFORMATION
		  	import srxlib.Device.CACHE_UPDATE_FORCE
	  	
		  	try {
		  		device.updateCache(CACHE_UPDATE_FORCE, CACHE_NAME_PPP_SUMMARY_INFORMATION)
	  		   
		  		Json.obj("result" -> true)
		  	} catch {
		  		case e:Throwable => Json.obj("result" -> false, "error" -> e.getMessage)
		  	}
		  } map(json => Ok(json))
	}
	
	def refreshNetworkRoutes(deviceName:String) = Action.async { implicit req =>
		  withDevice(deviceName) { implicit device =>
		  	import srxlib.Device.CACHE_NAME_ROUTE_INFORMATION
		  	import srxlib.Device.CACHE_UPDATE_FORCE
	  	
		  	try {
		  		device.updateCache(CACHE_UPDATE_FORCE, CACHE_NAME_ROUTE_INFORMATION)
	  		   
		  		Json.obj("result" -> true)
		  	} catch {
		  		case e:Throwable => Json.obj("result" -> false, "error" -> e.getMessage)
		  	}
		  } map(json => Ok(json))
	}
	
	def refreshVpnMonitor(deviceName:String) = Action.async { implicit req =>
		  withDevice(deviceName) { implicit device =>
		  	import srxlib.Device.CACHE_NAME_IPSEC_SECURITY_ASSOCIATIONS
		  	import srxlib.Device.CACHE_NAME_IPSEC_INACTIVE_TUNNELS
		  	import srxlib.Device.CACHE_UPDATE_FORCE
	  	
		  	try {
		  		device.updateCache(CACHE_UPDATE_FORCE, CACHE_NAME_IPSEC_SECURITY_ASSOCIATIONS, CACHE_NAME_IPSEC_INACTIVE_TUNNELS)
	  		   
		  		Json.obj("result" -> true)
		  	} catch {
		  		case e:Throwable => Json.obj("result" -> false, "error" -> e.getMessage)
		  	}
		  } map(json => Ok(json))
	}
	
	def refreshSessionLog(deviceName:String) = Action.async { implicit req =>
		  withDevice(deviceName) { implicit device =>
		  	import srxlib.Device.CACHE_NAME_RUNNING_CONFIG
		  	import srxlib.Device.CACHE_NAME_SESSION_LOG
		  	import srxlib.Device.CACHE_UPDATE_FORCE
	  	
		  	try {
		  		device.updateCache(CACHE_UPDATE_FORCE, CACHE_NAME_RUNNING_CONFIG, CACHE_NAME_SESSION_LOG)
	  		   
		  		Json.obj("result" -> true)
		  	} catch {
		  		case e:Throwable => Json.obj("result" -> false, "error" -> e.getMessage)
		  	}
		  } map(json => Ok(json))
	}
	
	def md5crypt = Action { implicit req =>
		val form = Form(
			  single(
					  "value" -> optional(text)
			      )
	      )
	   
	     val value = form.bindFromRequest.get.filter(v => !v.isEmpty).map(v => srxlib.Util.makeMD5Password(v)).getOrElse("")
	     Ok(Json.obj("result" -> value))
	}
}