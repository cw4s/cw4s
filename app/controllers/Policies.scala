package controllers

import play.api.mvc.{MessagesControllerComponents, MessagesAbstractController}
import play.api.data.Form
import play.api.data.Forms._

import play.filters.csrf.{CSRFAddToken, CSRFCheck}

import models.Forms._
import to.lef.srxlib
import scala.collection.JavaConverters._
import cw4s.Validations._

import javax.inject.Inject
import scala.concurrent.ExecutionContext

class Policies @Inject()(val deviceDAO:models.DeviceDAO, addToken:CSRFAddToken, checkToken:CSRFCheck, mcc:MessagesControllerComponents)(implicit ec:ExecutionContext) extends MessagesAbstractController(mcc) with models.DeviceSession {
    import models.Forms.{PolicyListForm => ListForm }
    
    def policies(deviceName:String, from:Option[String], to:Option[String], op:Option[String]) = Action.async { implicit req =>
    	withDevice(deviceName) { implicit device => 
    	  val config = device.config
    	  if (from.isDefined && to.isDefined && op.isDefined) Redirect(routes.Policies.create(deviceName, from.get, to.get))
    	  else {
    		  val form = Form(
    				  mapping(
    						  "from"  -> optional(text),
    						  "to"    -> optional(text),
    						  "op"    -> optional(text)
    						  )
    						  (ListForm.apply)
    						  (ListForm.unapply)
    				  ).fill(new ListForm(from, to, op)) 
    				  
   	    	val zoneOptions = config.securityZoneNameList.asScala.toList
	    	Ok(views.html.policies.index(form, zoneOptions, device.config.policyList(from.getOrElse(""), to.getOrElse("")).asScala.toList))   				  
    	  }
    	}
    }

    def policyForm(d:srxlib.Device, from:String, to:String, oldName:String = "") = Form(
        mapping(
            "name"      -> policyName(d, from, to, oldName),
            "src"       -> nonEmptyText,
            "dst"       -> nonEmptyText,
            "app"       -> nonEmptyText,
            "action"    -> nonEmptyText,
            "log"       -> boolean,
            "log_begin" -> boolean
        )
        (PolicyForm.apply)
        (PolicyForm.unapply)
    )
        
	def create(deviceName:String, fromName:String, toName:String) = addToken {
		createAction(deviceName, fromName, toName, false)
	}
	
	def createPost(deviceName:String, fromName:String, toName:String) = checkToken {
	  createAction(deviceName, fromName, toName, true)
	}
	
	def createAction(deviceName:String, fromName:String, toName:String, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  import views.html.policies.{create => view}
		  val config = device.config
		  val pf = policyForm(device, fromName, toName, "");
			if (fromName.isEmpty || !config.isSecurityZoneExist(fromName) || toName.isEmpty || !config.isSecurityZoneExist(toName))
			  Redirect(routes.Policies.policies(deviceName, None, None, None))
			else if (isPost) 
			  pf.bindFromRequest.fold(
				    error => Ok(view(fromName, toName, error)),
				    value => try {
				        device.config(value.toPolicy(device, fromName, toName).getCreateConfigure)
				        Redirect(routes.Policies.edit(deviceName, fromName, toName, value.name))
				    } catch {
				      case e:Throwable => Ok(view(fromName, toName, pf.fill(value), e.getMessage))
				    }
				)
			else 
			  Ok(view(fromName, toName, pf.fill(PolicyForm("", "", "", "", "", false, false))))
		}
	}
	
	def createClone(deviceName:String, fromName:String, toName:String, oldName:String) = checkToken {
	  Action.async { implicit req =>
	    withDevice(deviceName) { implicit device =>
		  import views.html.policies.{create => view}
		  import org.apache.commons.lang3.{StringUtils => su}
	      val config = device.config
		  val pf = policyForm(device, fromName, toName, "");
	      config.findPolicy(fromName, toName, oldName) match {
	        case models.Policy(v) => Ok(view(fromName, toName, pf.fill(PolicyForm("", su.join(v.getSourceNameList, ","), su.join(v.getDestinationNameList, ","), su.join(v.getApplicationNameList, ","), v.getAction, v.isLogClose, v.isLogInit))))
	        case _ => Redirect(routes.Policies.policies(deviceName, Some(fromName), Some(toName), None))
	      }
	    }
	  }
	}
	
	def edit(deviceName:String, fromName:String, toName:String, oldName:String) = addToken {
		editAction(deviceName, fromName, toName, oldName, false)
	}
	
	def editPost(deviceName:String, fromName:String, toName:String, oldName:String) = checkToken {
		editAction(deviceName, fromName, toName, oldName, true)
	}
	
	def editAction(deviceName:String, fromName:String, toName:String, oldName:String, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  import views.html.policies.{edit => view}
		  val config = device.config
		  val pf = policyForm(device, fromName, toName, oldName)
		  config.findPolicy(fromName, toName, oldName) match {
		    case models.Policy(v) if (v.isEditable(config)) => if (isPost) pf.bindFromRequest.fold(
		        error => Ok(view(fromName, toName, v.getName, error)),
		        value => try {
		        	device.config(value.toPolicy(device, fromName, toName).getEditConfigure(v, device.defaults))
		        	Redirect(routes.Policies.policies(deviceName, Some(fromName), Some(toName), None))
		        } catch {
		          case e:Throwable => Ok(view(fromName, toName, v.getName, pf.fill(value), e.getMessage))
		        }
		    ) else Ok(view(fromName, toName, v.getName, pf.fill(PolicyForm(v))))  
		    case _ => Redirect(routes.Policies.policies(deviceName, Some(fromName), Some(toName), None))
		  }
		}
	}

	lazy val advancedForm = Form(
			mapping(
					"srcNat" -> boolean,
					"dip" -> nonEmptyText
					)
					(AdvancedPolicyForm.apply)
					(AdvancedPolicyForm.unapply)
	    )
	    
	def advanced(deviceName:String, fromName:String, toName:String, oldName:String) = addToken {
		advancedAction(deviceName, fromName, toName, oldName, false)
	}
	
	def advancedPost(deviceName:String, fromName:String, toName:String, oldName:String) = checkToken {
		advancedAction(deviceName, fromName, toName, oldName, true)
	}

	def advancedAction(deviceName:String, fromName:String, toName:String, oldName:String, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  import views.html.policies.{advanced => view}
		  val config = device.config
		  val af = advancedForm
		  config.findPolicy(fromName, toName, oldName) match {
		    case models.Policy(v) => {
		      val dipOptions = Seq((srxlib.policy.Policy.EGRESS_INTERFACE_ADDRESS -> "None (Use Egress Interface IP)")) ++ config.dipList.asScala.toSeq.map(v => (v.getName, v.getDisplayName))

		        if (isPost) af.bindFromRequest.fold(
		          error => Ok(view(fromName, toName, v.getName, error, dipOptions)),
		          value => try {
		            v.setSourceNat(value.srcNat, value.dip)
		            device.config(v.getAdvancedConfigure(device.defaults))
		        	Redirect(routes.Policies.policies(deviceName, Some(fromName), Some(toName), None))
		          } catch {
		          	case e:Throwable => Ok(view(fromName, toName, v.getName, af.fill(value), dipOptions, e.getMessage))
		          }
		        ) else Ok(view(fromName, toName, v.getName, af.fill(AdvancedPolicyForm(v)), dipOptions))
		    }
		    case _ => Redirect(routes.Policies.policies(deviceName, Some(fromName), Some(toName), None))
		  }
		}
	}
	
	def deletePost(deviceName:String, fromName:String, toName:String, oldName:String) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  val config = device.config
		  config.findPolicy(fromName, toName, oldName) match {
		    case models.Policy(v) if (v.isRemovable(config)) => {
		    	device.config(v.getDeleteConfigure)
		    	Redirect(routes.Policies.policies(deviceName, Some(fromName), Some(toName), None)) 
		    }
		    case _ => Redirect(routes.Policies.policies(deviceName, Some(fromName), Some(toName), None)) 
		  }
		}
	}

	def move(deviceName:String, fromName:String, toName:String, oldName:String) = addToken { 
	  Action.async { implicit req =>
	    withDevice(deviceName) { implicit device => 
	      import views.html.policies.{move => view}
	      val config = device.config
	      config.findPolicy(fromName, toName, oldName) match {
	        case models.Policy(v) => Ok(view(v, config.policyList(fromName, toName).asScala.toList))
	        case _ => Redirect(routes.Policies.policies(deviceName, Some(fromName), Some(toName), None))
	      }
	    }
	  }
	}
	
	def moveModal(deviceName:String, fromName:String, toName:String, oldName:String) = addToken { Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	    import views.html.policies.{moveModal => view}
	    val config = device.config
	    config.findPolicy(fromName, toName, oldName) match {
	      case models.Policy(v) => Ok(view(Some(v), config.policyList(fromName, toName).asScala.toList))
	      case _ => Ok(view(None, List.empty))
	    }
	  }
	}}
	
	def movePost(deviceName:String, fromName:String, toName:String, oldName:String) = checkToken { 
	  Action.async { implicit req =>
	  	withDevice(deviceName) { implicit device => 
	  	  val before = Form(
	  	      single(
	  	          "before" -> text
	  	      )
	  	  ).bindFromRequest.value.getOrElse("")
	  	  
	  	  device.config.findPolicy(fromName, toName, oldName) match {
	  	  case models.Policy(o) => device.config(o.getMoveConfigure(before))
	  	  case _ => 
	  	  }
  		  Redirect(routes.Policies.policies(deviceName, Some(fromName), Some(toName), None))
	  	}
	  }
	}
	
	def log(deviceName:String, fromName:String, toName:String, oldName:String) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device => 
		  import views.html.policies.{log => view}
  		  val config = device.config
		  config.findPolicy(fromName, toName, oldName) match {
  		    case models.Policy(v) => {
//  		      val l = device.sessionLogXML.getRootElement.getTextContent
  		      val l = device.sessionLogList.asScala.toList.reverse
  		      Ok(view(v, l))
  		    }
  		    case _ => Redirect(routes.Policies.policies(deviceName, Some(fromName), Some(toName), None))
  		  }
		}
	}
}