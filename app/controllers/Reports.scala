package controllers

import play.api.mvc.{ControllerComponents, AbstractController}

import models.Forms._
import to.lef.srxlib
import scala.collection.JavaConverters._
import javax.inject.Inject
import scala.concurrent.ExecutionContext

class Reports @Inject()(val deviceDAO:models.DeviceDAO, components:ControllerComponents)(implicit ec:ExecutionContext) extends AbstractController(components) with models.DeviceSession{

  def chassis(deviceName:String, refresh:Option[Boolean]) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	    if (refresh.getOrElse(false)) {
	    	device.updateChassisCache
	    }
	    Ok(views.html.reports.chassis(device))
	  }
  }
}