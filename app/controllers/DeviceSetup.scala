package controllers

import scala.concurrent.Future
import scala.concurrent.ExecutionContext

import play.api.mvc.{MessagesControllerComponents, MessagesAbstractController}
import play.api.data.Form
import play.api.data.Forms._

import play.filters.csrf.{CSRFAddToken, CSRFCheck}

import models.Forms._
import javax.inject.Inject

class DeviceSetup @Inject()(deviceDAO: models.DeviceDAO, addToken:CSRFAddToken, checkToken:CSRFCheck, configuration:play.api.Configuration, mcc:MessagesControllerComponents)(implicit ec:ExecutionContext) extends MessagesAbstractController(mcc) {
  
	def list = Action.async { implicit  rs => 
	  deviceDAO.list.map(list => Ok(views.html.device.list(list, configuration)))
	}
	
	def deviceForm(oldName:Option[String]) = Form(
		mapping(
				"name"       -> nonEmptyText.verifying("Device name is already used.", v => ((oldName.isDefined && oldName.get.equals(v)) || !deviceDAO.findByNameSync(v, _.isDefined))),
				"hostname"   -> nonEmptyText,
				"port"       -> ignored(to.lef.srxlib.Device.DEFAULT_PORT),
				"username"   -> nonEmptyText,
				"password"   -> text.verifying("Password is required.", v => oldName.isDefined || !v.isEmpty),
				"persistent" -> boolean
		)
		(DeviceForm.apply)
		(DeviceForm.unapply)
	)
	
	def regist = addToken { 
		Action { implicit req => 
		  
			val form = deviceForm(None).fill(DeviceForm("", "", to.lef.srxlib.Device.DEFAULT_PORT, "", "", false))
			Ok(views.html.device.regist(form))
		}
	}
	
	def registPost = checkToken {
		Action.async { implicit req => 
		  val form = deviceForm(None)
		  form.bindFromRequest.fold(
		      formWithErrors => Future{ Ok(views.html.device.regist(formWithErrors)) },
		      value => deviceDAO.regist(value).map(id => Redirect(routes.DeviceSetup.list)).recover { case e:Throwable => Ok(views.html.device.regist(form.fill(value), e.getMessage()))}
	      )
		}
	}
	
	def edit(id:Int) = addToken {
	  Action.async { implicit req =>
	    deviceDAO.getFormById(id).map(_ match {
	      case Some(f) => Ok(views.html.device.edit(id, deviceForm(Some(f.name)).fill(f)))
	      case _ => Redirect(routes.DeviceSetup.list)
	    })
	  }
	}
	
	def editPost(id:Int) = checkToken {
		Action.async { implicit req =>
		  
		  val go = (f:DeviceForm) => {
		      import views.html.device.{edit => view}
		      
		    	val form = deviceForm(Some(f.name))
		    	form.bindFromRequest.fold(
		    			error => Future.successful(Ok(view(id, error))),
		    			value => {
		    					(if (value.password.length > 0) deviceDAO.edit(id, value) else deviceDAO.editExceptPassword(id, value))
		    					.map(i => if (i > 0) Redirect(routes.DeviceSetup.list) else Ok(view(id, form.fill(value), "Failed to edit device.")))
		    					.recover {
  		    					case e:Throwable => Ok(view(id, form.fill(value), e.getMessage()))
		    					}
		    			})
		    	}

		  deviceDAO.getFormById(id).flatMap(_.map(go(_)).getOrElse(Future.successful(Redirect(routes.DeviceSetup.list))))
		}
	}
	
	def deletePost(id:Int) = Action.async { implicit req =>
	  deviceDAO.delete(id).map(_ => Redirect(routes.DeviceSetup.list))
	}
}