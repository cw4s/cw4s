package controllers

import play.api.Logging
import play.api.mvc.{MessagesControllerComponents, MessagesAbstractController}
import play.api.data.{Form, Forms}
import play.api.data.Forms._

import play.filters.csrf.{CSRFAddToken, CSRFCheck}
import scala.collection.JavaConverters._

import models.Forms._
import to.lef.srxlib

import javax.inject.Inject
import scala.concurrent.ExecutionContext

class Services @Inject()(val deviceDAO:models.DeviceDAO, addToken:CSRFAddToken, checkToken:CSRFCheck, mcc:MessagesControllerComponents)(implicit ec:ExecutionContext) extends MessagesAbstractController(mcc) with Logging with models.DeviceSession {
	def predefined(deviceName:String) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	    val applications = device.defaults.applicationList
	    Ok(views.html.services.predefined(applications.asScala.toList))
	  }
	}
	
	def custom(deviceName:String) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	    val applications = device.config.applicationList
	    Ok(views.html.services.custom(applications.asScala.toList))
	  }
	}
	
	def customForm(d:srxlib.Device, oldName:String = "") = Form(
	    mapping(
	        "name" -> nonEmptyText.verifying("Invalid name.", srxlib.Util.isValidIdentifier(_)).verifying("Application name is already used.", v => v == oldName || !d.config.isApplicationOrApplicationSetExist(v)),
	        "timeout" -> mapping(
	            "type" -> number(min = AppTimeoutForm.typeDefault, max = AppTimeoutForm.typeCustom),
	            "sec"  -> optional(number)
	        )
	        (AppTimeoutForm.apply)
	        (AppTimeoutForm.unapply)
	        .verifying("Invalid timeout.", v => (v.toType != AppTimeoutForm.typeCustom) || v.sec.isDefined && srxlib.Util.isValidApplicationTimeout(v.sec.get)),
	       "protocol" -> list(
	           mapping(
	               "name"      -> optional(text),
	               "type"      -> number(min = AppProtocolForm.protoNone, max = AppProtocolForm.protoOther),
	               "protocol"  -> optional(text),
	               "src_low"   -> optional(text),
	               "src_high"  -> optional(text),
	               "dst_low"   -> optional(text),
	               "dst_high"  -> optional(text),
	               "icmp_type" -> optional(text),
	               "icmp_code" -> optional(text),
	               "alg"       -> optional(text)
	           )
	           (AppProtocolForm.apply)
	           (AppProtocolForm.unapply)
	           .verifying("Invalid protocol.", v => (v.protoType != AppProtocolForm.protoOther) || (v.protocol.isDefined && srxlib.Util.isValidApplicationProtocol(v.protocol.get)))
	           .verifying("Invalid source low port.", v => v.srcLowPort.isEmpty || srxlib.Util.isValidApplicationPort(v.srcLowPort.get))
	           .verifying("Invalid source hight port.", v => v.srcHighPort.isEmpty || srxlib.Util.isValidApplicationPort(v.srcHighPort.get))
	           .verifying("Invalid source port range.", v => (v.srcLowPort.isEmpty || v.srcHighPort.isEmpty) || srxlib.Util.isValidNumberRange(v.srcLowPort.get, v.srcHighPort.get))
   	           .verifying("Invalid destination low port.", v => v.dstLowPort.isEmpty || srxlib.Util.isValidApplicationPort(v.dstLowPort.get))
	           .verifying("Invalid destination hight port.", v => v.dstHighPort.isEmpty || srxlib.Util.isValidApplicationPort(v.dstHighPort.get))
	           .verifying("Invalid destination port range.", v => (v.dstLowPort.isEmpty || v.dstHighPort.isEmpty) || srxlib.Util.isValidNumberRange(v.dstLowPort.get, v.dstHighPort.get))
	           .verifying("Invalid icmp type.", v => v.icmpType.isEmpty || srxlib.Util.isValidApplicationIcmpType(v.icmpType.get))
	           .verifying("Invalid icmp code.", v => v.icmpCode.isEmpty || srxlib.Util.isValidApplicationIcmpCode(v.icmpType.get))
	        ) 
	    )
	    (AppCustomForm.apply)
	    (AppCustomForm.unapply)
	)
	
    def groupForm(d:srxlib.Device, oldName:String = "") = Form(
        mapping(
	        "name" -> nonEmptyText.verifying("Invalid name.", srxlib.Util.isValidIdentifier(_)).verifying("Application set name is already used.", v => v == oldName || !d.config.isApplicationOrApplicationSetExist(v)),
            "comment" -> optional(text),
            "apps" -> Forms.list(text)
        )
        (AppGroupForm.apply)
        (AppGroupForm.unapply)
        .verifying("No service is selected", v => (v.apps.exists(n => n.length() > 0)))
    )
 
	def createCustom(deviceName:String) = addToken {
		Action.async { implicit req =>
		  withDevice(deviceName) { implicit device =>
		  	val form = customForm(device).fill(AppCustomForm("", AppTimeoutForm(1, None), List.empty))
		  	Ok(views.html.services.createCustom(form))
		  }
		}
	}
	
	def createCustomPost(deviceName:String) = checkToken {
		Action.async { implicit req =>
		  	withDevice(deviceName) { implicit device => 
		  	  customForm(device).bindFromRequest.fold(
		  	      error => Ok(views.html.services.createCustom(error)),
		  	      value => try {
		  	        device.config(value.toApplicationElement.getCreateCustomConfigure)
		  	        Redirect(routes.Services.custom(deviceName))
		  	      } catch {
		  	        case e:Throwable => {
                  import scala.compat.Platform.EOL
		  	          logger.error(e.getCause.getStackTrace().mkString("", EOL, EOL))
		  	          Ok(views.html.services.createCustom(customForm(device).fill(value), e.getMessage))
		  	        }
		  	      }
		  	  )
		  	}
		}
	}
	
	def editCustom(deviceName:String, appName:String) = addToken {
		Action.async { implicit req => 
		  withDevice(deviceName) { implicit device => 
		    device.config.findApplication(appName) match {
		      case models.ApplicationElement(old) => {
		    	  val form = customForm(device, old.getName).fill(AppCustomForm(old))
		    	  Ok(views.html.services.editCustom(form, appName))
		      }
		      case _ => Redirect(routes.Services.custom(deviceName))
		    }
		  }
		}
	}
	
	def editCustomPost(deviceName:String, appName:String) =  checkToken {
		Action.async { implicit req =>
		  withDevice(deviceName) { implicit device =>
		    device.config.findApplication(appName) match {
		      case models.ApplicationElement(old) => {
		        customForm(device, appName).bindFromRequest.fold(
		            error => Ok(views.html.services.editCustom(error, appName)),
		            value => try {
		            	device.config(value.toApplicationElement.getEditCustomConfigure(old))
		            	Redirect(routes.Services.custom(deviceName))
		            } catch {
		            	case e:Throwable => Ok(views.html.services.editCustom(customForm(device, appName).fill(AppCustomForm(old)), appName, e.getMessage))
		            }
		        )
		      }
		      case _ => Redirect(routes.Services.custom(deviceName))
		      }
		  }
		}
	}
	
	def deleteCustomPost(deviceName:String, appName:String) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  	if (device.config.isApplicationExist(appName)) {
		  		try {
		  			device.config(srxlib.policy.ApplicationElement.getDeleteConfigure(appName))
		  			Redirect(routes.Services.custom(deviceName))
		  		}
		  		catch {
		  		  case e:Throwable => Redirect(routes.Services.custom(deviceName))
		  		}
		  	}
		  	else {
		  		Redirect(routes.Services.custom(deviceName))
		  	}
		}
	}

	def createRpc(deviceName:String) = addToken {
		createRpcAction(deviceName, false)
	}

	def createRpcPost(deviceName:String) = checkToken {
		createRpcAction(deviceName, true)
	}

	def createRpcAction(deviceName:String, isPost:Boolean) = TODO

	def editRpc(deviceName: String, appName:String) = TODO
	def editRpcPost(deviceName: String, appName:String) = TODO
	def deleteRpcPost(deviceName: String, appName:String) = TODO
	
	def createUuid(deviceName:String) = TODO
	def createUuidPost(deviceName:String) = TODO
	def editUuid(deviceName:String, appName:String) = TODO
	def editUuidPost(deviceName:String, appName:String) = TODO
	def deleteUuidPost(deviceName:String, appName:String) = TODO
	
	def groups(deviceName:String) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	    Ok(views.html.services.groups(
	        device.defaults.applicationSetList.asScala.toList,
	        device.config.applicationSetList.asScala.toList
	        ))
	  }
	}
	
	def createGroup(deviceName:String) = addToken {
		Action.async { implicit req => 
		  withDevice(deviceName) { implicit device =>
		    val form = groupForm(device).fill(AppGroupForm("", None, List.empty))
		    Ok(views.html.services.createGroup(form))
		  }
		}
	}
	
	def createGroupPost(deviceName:String) = checkToken {
		Action.async { implicit req =>
			withDevice(deviceName) { implicit device =>
			  groupForm(device).bindFromRequest.fold(
			      error => Ok(views.html.services.createGroup(error)),
			      value => try {
			    	  device.config(value.toApplicationSet.getCreateConfigure)
			    	  Redirect(routes.Services.groups(deviceName))
			      } catch {
			        case e:Throwable => Ok(views.html.services.createGroup(groupForm(device).fill(value), e.getMessage))
			      }
			  )
			}
		}
	}

	def editGroup(deviceName:String, grpName:String) = addToken {
		editGroupAction(deviceName, grpName, false)
	}

	def editGroupPost(deviceName:String, grpName:String) = checkToken {
		editGroupAction(deviceName, grpName, true)
	}

	private def editGroupAction(deviceName:String, grpName:String, isPost:Boolean) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
		    val config = device.config
		    val view = views.html.services.editGroup(grpName) _
		    config.findApplicationSet(grpName) match {
		      case models.ApplicationSet(old) if (old.isEditable(config)) => if (isPost) {
		    	  groupForm(device, grpName).bindFromRequest.fold(
		    	      error => Ok(view(error, "")),
		    	      value => try {
		    	    	  device.config(value.toApplicationSet.getEditConfigure(old))
		    	    	  Redirect(routes.Services.groups(deviceName))
		    	      } catch {
		    	        case e:Throwable => Ok(view(groupForm(device, grpName).fill(value), e.getMessage))
		    	      }
		    	  )
		      } else {
		        val form = groupForm(device, grpName).fill(AppGroupForm(old))
		        Ok(view(form, ""))
		      }
		      case _ => Redirect(routes.Services.groups(deviceName))
		    }
		  }
	}
	
	def deleteGroupPost(deviceName:String, gname:String) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	    if (device.config.isApplicationSetExist(gname)) {
	      device.config(srxlib.policy.ApplicationSet.getDeleteConfigure(gname))
	    }
	    Redirect(routes.Services.groups(deviceName))
	  }
	}
}