package controllers

import play.api.mvc.{MessagesControllerComponents, MessagesAbstractController}
import play.api.data.Form
import play.api.data.Forms._
import play.filters.csrf.{CSRFAddToken, CSRFCheck}
import scala.collection.JavaConverters._
import models.Forms._
import to.lef.srxlib

import javax.inject.Inject
import scala.concurrent.ExecutionContext

class SecurityScreen @Inject()(val deviceDAO:models.DeviceDAO, addToken:CSRFAddToken, checkToken:CSRFCheck, mcc:MessagesControllerComponents)(implicit ec:ExecutionContext) extends MessagesAbstractController(mcc) with models.DeviceSession {
	def index(deviceName:String, name:Option[String]) = Action.async { implicit req =>
     	import views.html.security.screen.{nozone => view}
	  withDevice(deviceName) { implicit device =>
	    val config = device.config
	    if (name.isDefined) config.findSecurityZone(name.get) match {
	    	  case models.SecurityZone(zone)  => Redirect(routes.SecurityScreen.edit(deviceName, zone.getName))
	    	  case _ => Ok(view(name))
	    } else 
	   		config.securityZoneList.asScala.toList.headOption.map(v => 
	    			Redirect(routes.SecurityScreen.edit(deviceName, v.getName()))
	    			).getOrElse(Ok(view(name)))
	  }
	}

	import srxlib.zone.IDSOption.ICMP_FLOOD_THRESHOLD_DEFAULT
	import srxlib.zone.IDSOption.ICMP_FLOOD_THRESHOLD_MIN    
	import srxlib.zone.IDSOption.ICMP_FLOOD_THRESHOLD_MAX    
	import srxlib.zone.IDSOption.UDP_FLOOD_THRESHOLD_DEFAULT 
	import srxlib.zone.IDSOption.UDP_FLOOD_THRESHOLD_MIN     
	import srxlib.zone.IDSOption.UDP_FLOOD_THRESHOLD_MAX     
	import srxlib.zone.IDSOption.TCP_SYN_FLOOD_ATTACK_THRESHOLD_DEFAULT
	import srxlib.zone.IDSOption.TCP_SYN_FLOOD_ATTACK_THRESHOLD_MIN    
	import srxlib.zone.IDSOption.TCP_SYN_FLOOD_ATTACK_THRESHOLD_MAX    
	import srxlib.zone.IDSOption.TCP_SYN_FLOOD_ALARM_THRESHOLD_DEFAULT 
	import srxlib.zone.IDSOption.TCP_SYN_FLOOD_ALARM_THRESHOLD_MIN     
	import srxlib.zone.IDSOption.TCP_SYN_FLOOD_ALARM_THRESHOLD_MAX     
	import srxlib.zone.IDSOption.TCP_SYN_FLOOD_SRC_THRESHOLD_DEFAULT   
	import srxlib.zone.IDSOption.TCP_SYN_FLOOD_SRC_THRESHOLD_MIN       
	import srxlib.zone.IDSOption.TCP_SYN_FLOOD_SRC_THRESHOLD_MAX       
	import srxlib.zone.IDSOption.TCP_SYN_FLOOD_DST_THRESHOLD_DEFAULT   
	import srxlib.zone.IDSOption.TCP_SYN_FLOOD_DST_THRESHOLD_MIN       
	import srxlib.zone.IDSOption.TCP_SYN_FLOOD_DST_THRESHOLD_MAX       
	import srxlib.zone.IDSOption.TCP_SYN_FLOOD_TIMEOUT_DEFAULT         
	import srxlib.zone.IDSOption.TCP_SYN_FLOOD_TIMEOUT_MIN             
	import srxlib.zone.IDSOption.TCP_SYN_FLOOD_TIMEOUT_MAX             
	import srxlib.zone.IDSOption.IP_SWEEP_THRESHOLD_DEFAULT        
	import srxlib.zone.IDSOption.IP_SWEEP_THRESHOLD_MIN            
	import srxlib.zone.IDSOption.IP_SWEEP_THRESHOLD_MAX            
	import srxlib.zone.IDSOption.TCP_PORT_SCAN_THRESHOLD_DEFAULT   
	import srxlib.zone.IDSOption.TCP_PORT_SCAN_THRESHOLD_MIN       
	import srxlib.zone.IDSOption.TCP_PORT_SCAN_THRESHOLD_MAX       
	import srxlib.zone.IDSOption.TCP_SWEEP_THRESHOLD_DEFAULT       
	import srxlib.zone.IDSOption.TCP_SWEEP_THRESHOLD_MIN           
	import srxlib.zone.IDSOption.TCP_SWEEP_THRESHOLD_MAX           
	import srxlib.zone.IDSOption.UDP_SWEEP_THRESHOLD_DEFAULT       
	import srxlib.zone.IDSOption.UDP_SWEEP_THRESHOLD_MIN           
	import srxlib.zone.IDSOption.UDP_SWEEP_THRESHOLD_MAX           
	import srxlib.zone.IDSOption.TCP_SYN_ACK_ACK_THRESHOLD_DEFAULT 
	import srxlib.zone.IDSOption.TCP_SYN_ACK_ACK_THRESHOLD_MIN     
	import srxlib.zone.IDSOption.TCP_SYN_ACK_ACK_THRESHOLD_MAX     
	import srxlib.zone.IDSOption.SRC_LIMIT_THRESHOLD_DEFAULT       
	import srxlib.zone.IDSOption.SRC_LIMIT_THRESHOLD_MIN           
	import srxlib.zone.IDSOption.SRC_LIMIT_THRESHOLD_MAX           
	import srxlib.zone.IDSOption.DST_LIMIT_THRESHOLD_DEFAULT       
	import srxlib.zone.IDSOption.DST_LIMIT_THRESHOLD_MIN           
	import srxlib.zone.IDSOption.DST_LIMIT_THRESHOLD_MAX
	
	def screenForm = Form(
	    mapping(
			"alarm" -> boolean,
			"flood" -> mapping(
			    "icmp" -> boolean,
			    "icmp_threshold" -> number(min = ICMP_FLOOD_THRESHOLD_MIN, max = ICMP_FLOOD_THRESHOLD_MAX),
			    "udp" -> boolean,
			    "udp_threshold" -> number(min = UDP_FLOOD_THRESHOLD_MIN, max = UDP_FLOOD_THRESHOLD_MAX),
			    "syn" -> boolean,
			    "syn_attack_threshold" -> number(min = TCP_SYN_FLOOD_ATTACK_THRESHOLD_MIN, max = TCP_SYN_FLOOD_ATTACK_THRESHOLD_MAX), // pps default = 200
			    "syn_alarm_threshold" -> number(min = TCP_SYN_FLOOD_ALARM_THRESHOLD_MIN, max = TCP_SYN_FLOOD_ALARM_THRESHOLD_MAX), // pps default = 512
			    "syn_src_threshold" -> number(min = TCP_SYN_FLOOD_SRC_THRESHOLD_MIN, max = TCP_SYN_FLOOD_SRC_THRESHOLD_MAX), //  pps default = 4000
			    "syn_dst_threshold" -> number(min = TCP_SYN_FLOOD_DST_THRESHOLD_MIN, max = TCP_SYN_FLOOD_DST_THRESHOLD_MAX), // pps default = 4000
			    "syn_timeout" -> number(min = TCP_SYN_FLOOD_TIMEOUT_MIN, max = TCP_SYN_FLOOD_TIMEOUT_MAX)
			    )
			    (ScreenFloodForm.apply)
			    (ScreenFloodForm.unapply),
			"ms" -> mapping(
			    "winnuke" -> boolean
				)
				(ScreenMSForm.apply)
				(ScreenMSForm.unapply),
			"scan" -> mapping(
			    "ip_spoof" -> boolean,
			    "ip_sweep" -> boolean,
			    "ip_sweep_threshold" -> number(min = IP_SWEEP_THRESHOLD_MIN, max = IP_SWEEP_THRESHOLD_MAX), // microseconds default = 5000
			    "port_scan" -> boolean,
			    "port_scan_threshold" -> number(min = TCP_PORT_SCAN_THRESHOLD_MIN, max = TCP_PORT_SCAN_THRESHOLD_MAX), // microseconds default = 5000
			    "tcp_sweep" -> boolean,
			    "tcp_sweep_threshold" -> number(min = TCP_SWEEP_THRESHOLD_MIN, max = TCP_SWEEP_THRESHOLD_MAX), // microseconds default = 5000
			    "udp_sweep" -> boolean,
			    "udp_sweep_threshold" -> number(min = UDP_SWEEP_THRESHOLD_MIN, max = UDP_SWEEP_THRESHOLD_MAX) // microseconds default = 5000
			    )
			    (ScreenScanForm.apply)
			    (ScreenScanForm.unapply),
			"dos" -> mapping(
			    "ping_of_death" -> boolean,
			    "tear_drop" -> boolean,
			    "icmp_fragment" -> boolean,
			    "icmp_large" -> boolean,
			    "block_fragment" -> boolean,
			    "land" -> boolean,
			    "syn_ack_ack" -> boolean,
			    "syn_ack_ack_threshold" -> number(min = TCP_SYN_ACK_ACK_THRESHOLD_MIN, max = TCP_SYN_ACK_ACK_THRESHOLD_MAX), // connections default = 512
			    "src_limit" -> boolean,
			    "src_limit_threshold" -> number(min = SRC_LIMIT_THRESHOLD_MIN, max = SRC_LIMIT_THRESHOLD_MAX), // default = 128
			    "dst_limit" -> boolean, 
			    "dst_limit_threshold" -> number(min = DST_LIMIT_THRESHOLD_MIN, max = DST_LIMIT_THRESHOLD_MAX) // default = 128
			    )
			    (ScreenDosForm.apply)
			    (ScreenDosForm.unapply),
			 "ip" -> mapping(
			     "bad_option" -> boolean,
			     "timestamp" -> boolean,
			     "security" -> boolean,
			     "stream" -> boolean,
			     "record_route" -> boolean,
			     "loose_src_route" -> boolean,
			     "strict_src_route" -> boolean,
			     "src_route" -> boolean
			     )
			     (ScreenIPOptionForm.apply)
			     (ScreenIPOptionForm.unapply),
			 "tcp" -> mapping(
			     "syn_fragment" -> boolean,
			     "without_fragment" -> boolean,
			     "syn_and_fin" -> boolean,
			     "fin_no_ack" -> boolean,
			     "unknown" -> boolean
			     )
			     (ScreenTcpForm.apply)
			     (ScreenTcpForm.unapply)
		     )
			(ScreenForm.apply)
			(ScreenForm.unapply)
			)
	    
	def edit(deviceName:String, name:String) = addToken {
	  editAction(deviceName, name, false)
	}
	
	def editPost(deviceName:String, name:String) = checkToken {
		editAction(deviceName, name, true)
	}
	
	def editAction(deviceName:String, name:String, isPost:Boolean) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	    	import views.html.security.screen.{form => view}
	  		val config = device.config
	  		val form = screenForm
	  		config.findSecurityZone(name) match {
	  		  case models.SecurityZone(zone) => {
	  		    val ids = Option(zone.getIDSOption(config)).getOrElse(models.IDSOption(zone.getScreenName))
	  		    if (isPost) form.bindFromRequest.fold(
	  		      error => Ok(view(error, name)),
	  		      value => {
	  		    	value.updateIDSOption(ids)
	  		    	device.config(zone.getIDSOptionConfigure(ids))
	  		        Redirect(routes.SecurityScreen.edit(deviceName, name))
	  		      }
	    		) else Ok(view(form.fill(ScreenForm(ids)), name))
	  		  }
	  		  case _ => Redirect(routes.Home.index(deviceName, None, None))
	  		}
	  }
	}
}