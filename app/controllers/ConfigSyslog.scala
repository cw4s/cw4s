package controllers

import play.api.mvc.{MessagesControllerComponents, MessagesAbstractController}

import play.api.data.Form
import play.api.data.Forms._

import play.filters.csrf._

import scala.collection.JavaConverters._

import to.lef.srxlib
import models.Forms._

import javax.inject.Inject
import scala.concurrent.ExecutionContext

class ConfigSyslog @Inject()(val deviceDAO:models.DeviceDAO, addToken:CSRFAddToken, checkToken:CSRFCheck, mcc:MessagesControllerComponents)(implicit ec:ExecutionContext) extends MessagesAbstractController(mcc) with models.DeviceSession {
	def syslogForm = Form(
	    mapping(
	        "host" -> list(
	            mapping(
	                "name" -> text,
	                "facility" -> text,
	                "level" -> text,
	                "port" -> optional(number(min = 0, max = 65535)),
	                "src_address" -> optional(text),
	                "is_traffic_log" -> ignored(false)
	            )
	            (SyslogHostForm.apply)
	            (SyslogHostForm.unapply)
	        ),
          "disable_traffic_log" -> boolean
//	        "file" -> list(
//	            mapping(
//	                "name" -> text,
//	                "facility" -> text,
//	                "level" -> text,
//	                "is_structured_data" -> boolean,
//                  "is_traffic_log" -> boolean                
//	            )
//	            (SyslogFileForm.apply)
//	            (SyslogFileForm.unapply)            
//	        )
	    )
	    (SyslogForm.apply)
	    (SyslogForm.unapply)
	)
	
	def index(deviceName:String) = addToken {
		indexAction(deviceName, false)
	}
	
	def indexPost(deviceName:String) = checkToken {
		indexAction(deviceName, true)
	}
	
	def indexAction(deviceName:String, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName){ implicit device =>
		  	import views.html.config.syslog.{index => view}
			val config = device.config
			val form = syslogForm

      val currentHostList = config.syslogHostList.asScala.toList.filter(!_.isTrafficLog)
//      val currentFileList = config.syslogFileList.asScala.toList.filter(!_.isTrafficLog)
      
			if (isPost) form.bindFromRequest.fold(
			    error => Ok(view(error)),
			    value => try {
			    	val dc = new srxlib.DeviceConfigures
            currentHostList.foreach(v => dc.add(v.getDeleteConfigure))
//           currentFileList.foreach(v => dc.add(v.getDeleteConfigure))
			    	value.host.filter(!_.name.isEmpty).foreach(v => dc.add(v.toSyslogHost.getCreateConfigure))
//		    	value.file.filter(!_.name.isEmpty).foreach(v => dc.add(v.toSyslogFile.getCreateConfigure))
            dc.add(srxlib.syslog.Syslog.getLogModeConfigure(value.disableEventMode))
			    	device.config(dc)
			    	Redirect(routes.ConfigSyslog.index(deviceName))
			    } catch {
			      case e:Throwable => {
              Ok(view(form.fill(value), e.getMessage))
            }
			    }
			) else Ok(view(form.fill(SyslogForm(currentHostList.map(v => SyslogHostForm(v.asInstanceOf[srxlib.syslog.SyslogHost])), config.isLogModeStream())))) //, 
//												currentFileList.map(v => SyslogFileForm(v.asInstanceOf[srxlib.syslog.SyslogFile]))))))
		}
	}
}