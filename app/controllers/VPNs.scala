package controllers

import play.api.mvc.{MessagesControllerComponents, MessagesAbstractController}
import play.api.data.Form
import play.api.data.Forms._

import play.filters.csrf.{CSRFAddToken, CSRFCheck}
import scala.collection.JavaConverters._

import models.Forms._
import to.lef.srxlib

import srxlib.vpn.IkeProposal.LIFETIME_MIN
import srxlib.vpn.IkeProposal.LIFETIME_MAX

import srxlib.vpn.IkeGateway.DPD_INTERVAL_MIN
import srxlib.vpn.IkeGateway.DPD_INTERVAL_MAX

import srxlib.vpn.IkeGateway.DPD_THRESHOLD_MIN
import srxlib.vpn.IkeGateway.DPD_THRESHOLD_MAX

import srxlib.vpn.IkeGateway.NAT_KEEPALIVE_MIN
import srxlib.vpn.IkeGateway.NAT_KEEPALIVE_MAX

import srxlib.vpn.IPSecProposal.PROTOCOL_AH
import srxlib.vpn.IPSecProposal.PROTOCOL_ESP

import srxlib.vpn.IPSecProposal.LIFETIME_SECONDS_MIN
import srxlib.vpn.IPSecProposal.LIFETIME_SECONDS_MAX

import srxlib.vpn.IPSecProposal.LIFETIME_KILOBYTES_MIN
import srxlib.vpn.IPSecProposal.LIFETIME_KILOBYTES_MAX

import javax.inject.Inject
import scala.concurrent.ExecutionContext

class VPNs @Inject()(val deviceDAO:models.DeviceDAO, addToken:CSRFAddToken, checkToken:CSRFCheck, mcc:MessagesControllerComponents)(implicit ec:ExecutionContext) extends MessagesAbstractController(mcc) with models.DeviceSession {
	def p1ProposalList(deviceName:String) = Action.async { implicit req =>
	  withDevice (deviceName) { implicit device => 
		import views.html.vpns.p1proposal.{index => view }
		
		Ok(view(device.config))
	  }
	}

	def p1Form(current:Option[srxlib.vpn.IkeProposal]) = Form(
			mapping(
			    "name" -> current.map(v => ignored(v.getName)).getOrElse(cw4s.Validations.idText),
			    "auth_method" -> nonEmptyText,
			    "dh_group" -> nonEmptyText,
			    "enc_alg" -> nonEmptyText,
			    "auth_alg" -> nonEmptyText,
			    "life_time" -> cw4s.Validations.numberOrZero(min = LIFETIME_MIN, max = LIFETIME_MAX)
			    )
			    (VpnP1ProposalForm.apply)
			    (VpnP1ProposalForm.unapply)
	    )
	    
	def createP1Proposal(deviceName:String) = addToken {
		createP1ProposalAction(deviceName, false)
	}
	
	def createP1ProposalPost(deviceName:String) = checkToken {
		createP1ProposalAction(deviceName, true)
	}
	
	def createP1ProposalAction(deviceName:String, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  	import views.html.vpns.p1proposal.{create => view}
			val form = p1Form(None)
			
			if (isPost) form.bindFromRequest.fold(
			    error => Ok(view(error)),
			    value => try {
			    	device.config(value.toIkeProposal.getCreateConfigure)
			    	Redirect(routes.VPNs.p1ProposalList(deviceName))
			    } catch {
			      case e:Throwable => Ok(view(form.fill(value), e.getMessage))
			    }
			) else Ok(view(form.fill(VpnP1ProposalForm("", "", "", "", "", 28800))))
		}
	}
	  
	def editP1Proposal(deviceName:String, name:String) = addToken {
		editP1ProposalAction(deviceName, name, false)
	}
	
	def editP1ProposalPost(deviceName:String, name:String) = checkToken {
		editP1ProposalAction(deviceName, name, true)
	}
	
	def editP1ProposalAction(deviceName:String, name:String, isPost:Boolean) = Action.async { implicit req =>
	  withDevice (deviceName) { implicit device =>
	    val config = device.config
	    
	    config.findIkeProposal(name) match {
	      case models.IkeProposal(v) => {
		  	import views.html.vpns.p1proposal.{edit => view}
	        val form = p1Form(Some(v))
	        if (isPost) form.bindFromRequest.fold(
	            error => Ok(view(error, name)),
	            value => try {
	            	device.config(value.toIkeProposal.getEditConfigure)
	            	Redirect(routes.VPNs.p1ProposalList(deviceName))
	            } catch {
	              case e:Throwable => Ok(view(form.fill(value), name, e.getMessage))
	            }
	        ) else Ok(view(form.fill(VpnP1ProposalForm(v)), name))
	      }
	      case _ => Redirect(routes.VPNs.p1ProposalList(deviceName))
	    }
	  }
	}

	def deleteP1ProposalPost(deviceName:String, name:String) = Action.async { implicit req =>
		withDevice (deviceName) { implicit device => 
		  device.config.findIkeProposal(name) match {
		    case models.IkeProposal(prop) => {
		    	device.config(prop.getDeleteConfigure)
		    }
		  }
		  Redirect(routes.VPNs.p1ProposalList(deviceName))
		}
	}
	
	def p2Form(current:Option[srxlib.vpn.IPSecProposal]) = Form(
	    mapping(
		    "name" -> current.map(v => ignored(v.getName)).getOrElse(cw4s.Validations.idText),
	        "encap" -> nonEmptyText.verifying("Invalid encapsulation", v => v == PROTOCOL_ESP || v == PROTOCOL_AH),
	        "enc_alg" -> text,
	        "auth_alg1" -> text,
	        "auth_alg2" -> text,
	        "lifetime" -> cw4s.Validations.numberOrZero(LIFETIME_SECONDS_MIN, LIFETIME_SECONDS_MAX),
	        "lifesize" -> cw4s.Validations.longNumberOrZero(LIFETIME_KILOBYTES_MIN, LIFETIME_KILOBYTES_MAX)
	    )
	    (VpnP2ProposalForm.apply)
	    (VpnP2ProposalForm.unapply)
	)
	
	def p2ProposalList(deviceName:String) = Action.async { implicit req =>
	  withDevice (deviceName) { implicit device => 
		import views.html.vpns.p2proposal.{index => view }
		
		Ok(view(device.config))
	  }
	}
	
	def createP2Proposal(deviceName:String) = addToken {
		createP2ProposalAction(deviceName, false)
	}
	
	def createP2ProposalPost(deviceName:String) = checkToken {
		createP2ProposalAction(deviceName, true)
	}
	
	def createP2ProposalAction(deviceName:String, isPost:Boolean) = Action.async { implicit req =>
		withDevice (deviceName) { implicit device =>
		  	import views.html.vpns.p2proposal.{create => view}
			val form = p2Form(None)
			val config = device.config
			
			if (isPost) form.bindFromRequest.fold(
			    error => Ok(view(error)),
			    value => try {
			    	device.config(value.toIPSecProposal.getCreateConfigure)
			    	Redirect(routes.VPNs.p2ProposalList(deviceName))
			    } catch {
			      case e:Throwable => Ok(view(form.fill(value), e.getMessage))
			    }
			) else Ok(view(form.fill(VpnP2ProposalForm("", srxlib.vpn.IPSecProposal.PROTOCOL_ESP, "", "", "", 28800, 0))))
		}
	}
	
	def editP2Proposal(deviceName:String, name:String) = addToken {
		editP2ProposalAction(deviceName, name, false)
	}
	
	def editP2ProposalPost(deviceName:String, name:String) = checkToken {
		editP2ProposalAction(deviceName, name, true)
	}
	
	def editP2ProposalAction(deviceName:String, name:String, isPost:Boolean) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	    val config = device.config
	    config.findIPSecProposal(name) match {
	      case models.IPSecProposal(proposal) => {
	    	  import views.html.vpns.p2proposal.{edit => view}
	    	  val form = p2Form(Some(proposal))
	    	  
	    	  if (isPost) form.bindFromRequest.fold(
	    	      error => Ok(view(error, name)),
	    	      value => try {
	    	    	  device.config(value.toIPSecProposal.getEditConfigure)
	    	    	  Redirect(routes.VPNs.p2ProposalList(deviceName))
	    	      } catch {
	    	        case e:Throwable => Ok(view(form.fill(value), name, e.getMessage))
	    	      }
	    	  ) else Ok(view(form.fill(VpnP2ProposalForm(proposal)), name))
	      }
	      case _ => Redirect(routes.VPNs.p2ProposalList(deviceName))
	    }
	  }
	}
	
	def deleteP2ProposalPost(deviceName:String, name:String) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device => 
		  device.config.findIPSecProposal(name) match {
		    case models.IPSecProposal(prop) => {
		    	device.config(prop.getDeleteConfigure)
		    }
		  }
		  Redirect(routes.VPNs.p2ProposalList(deviceName))
		}
	}
	
	def gatewayList(deviceName:String) = Action.async {  implicit req =>
		withDevice(deviceName) { implicit device =>
		  Ok(views.html.vpns.gateway.index(device.config))
		}
	}

	def gatewayForm(current:Option[srxlib.vpn.IkeGateway]) = Form(
	    mapping(
	        "name" -> current.map(v => ignored(v.getName)).getOrElse(cw4s.Validations.idText),
	        "ike_version" -> nonEmptyText,
	        "peer" -> mapping(
	        		"type"    -> nonEmptyText,
	        		"address" -> text,
	        		"peer_id" -> text
	        )
	        (VpnGatewayPeerForm.apply)
	        (VpnGatewayPeerForm.unapply),
	        "local_id" -> text,
	        "outgoing_interface" -> nonEmptyText,
	        "policy" -> mapping(
	            "preshared_key" -> nonEmptyText,
	            "mode" -> nonEmptyText,
	            "proposal_set" -> nonEmptyText,
	            "proposals" -> list(text)
	         )
	         (VpnP1PolicyForm.apply)
	         (VpnP1PolicyForm.unapply),
	         "nat_traversal_enabled" -> boolean,
	         "nat_keep_alive" -> cw4s.Validations.numberOrZero(min = NAT_KEEPALIVE_MIN, max = NAT_KEEPALIVE_MAX),
	         "dpd_interval" -> cw4s.Validations.numberOrZero(min = DPD_INTERVAL_MIN, max = DPD_INTERVAL_MAX),
	         "dpd_threshold" -> cw4s.Validations.numberOrZero(min = DPD_THRESHOLD_MIN, max = DPD_THRESHOLD_MAX),
	         "dpd_always_send" -> boolean
	    )
	    (VpnGatewayForm.apply)
	    (VpnGatewayForm.unapply)
	)
	
	def createGateway(deviceName:String) = addToken {
		createGatewayAction(deviceName, false)
	}
	
	def createGatewayPost(deviceName:String) = checkToken {
		createGatewayAction(deviceName, true)
	}
	
	def createGatewayAction(deviceName:String, isPost:Boolean) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	    	import srxlib.vpn.IkeGateway.PEER_TYPE_STATIC
	    	import views.html.vpns.gateway.{create => view}
	    	val config = device.config
	    	val form = gatewayForm(None)
	    
	    	import srxlib.vpn.IkePolicy.MODE_MAIN
	    	import srxlib.vpn.IkeGateway.IKE_VERSION_1
	    	import srxlib.vpn.IkePolicy.PROPOSAL_SET_STANDARD
	    	
	    	if (isPost) form.bindFromRequest.fold(
	    		error => Ok(view(error)),
	    		value => try {
	    			val pname = value.makePolicyName(config)
	    			device.config(new srxlib.DeviceConfigures(
	    			    value.policy.toIkePolicy(pname).getCreateConfigure,
	    			    value.toIkeGateway(pname).getCreateConfigure
	    			    ))
	    		  Redirect(routes.VPNs.gatewayList(deviceName))
	    		} catch {
	    		  case e:Throwable => Ok(view(form.fill(value), e.getMessage))
	    		}
	    	) else Ok(view(form.fill(VpnGatewayForm("", IKE_VERSION_1, VpnGatewayPeerForm(PEER_TYPE_STATIC, "", ""), "", "", VpnP1PolicyForm("", MODE_MAIN, PROPOSAL_SET_STANDARD,  List.empty), true, 0, 0, 0, false))))
	  }
	}

	def editGateway(deviceName:String, name:String) = addToken {
		editGatewayAction(deviceName, name, false)
	}
	
	def editGatewayPost(deviceName:String, name:String) = checkToken {
		editGatewayAction(deviceName, name, true)
	}
	
	def editGatewayAction(deviceName:String, name:String, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device => 
		  val config = device.config
		  config.findIkeGateway(name) match {
		    case models.IkeGateway(gw) if (models.IkeGateway.isEditable(gw, config))=> {
		    	import views.html.vpns.gateway.{edit => view}
		    	val form = gatewayForm(Some(gw))
		    	val policy = Option(gw.getPolicy(config))
		    	
		    	if (isPost) form.bindFromRequest.fold (
		    	    error => Ok(view(error, name)),
		    	    value => try {
		    	    	policy match {
		    	    	  case Some(pv) => device.config(new srxlib.DeviceConfigures(
		    	    	      value.policy.toIkePolicy(pv.getName).getEditConfigure,
		    	    	      value.toIkeGateway(pv.getName).getEditConfigure
		    	    	      ))
		    	    	  case _ => {
		    	    		  val pname = value.makePolicyName(config)
		    	    			device.config(new srxlib.DeviceConfigures(
		    	    						value.policy.toIkePolicy(pname).getCreateConfigure,
		    	    						value.toIkeGateway(pname).getEditConfigure
		    	    					))	    	    		  
		    	    	  }
		    	    	}
		    	    	Redirect(routes.VPNs.gatewayList(deviceName))
		    	    } catch {
		    	    	case e:Throwable => Ok(view(form.fill(value), name, e.getMessage))
		    	    }
		    	) else Ok(view(form.fill(VpnGatewayForm(gw, policy)), name))
		    }
		    case _ => Redirect(routes.VPNs.gatewayList(deviceName))
		  }
		}
	}
	
	def deleteGatewayPost(deviceName:String, name:String) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device => 
		  device.config.findIkeGateway(name) match {
		    case models.IkeGateway(gw) => {
		    	device.config(gw.getDeleteConfigure(true))
		    }
		  }
		  Redirect(routes.VPNs.gatewayList(deviceName))
		}
	}
		
	def vpnList(deviceName:String) = Action.async { 
		withDevice(deviceName) { implicit device =>
		  Ok(views.html.vpns.vpn.index(device.config))
		}
	}
	
	def vpnForm(current:Option[srxlib.vpn.IPSecVpn]) = Form(
	    mapping(
	        "name" -> current.map(v => ignored(v.getName)).getOrElse(cw4s.Validations.idText),
	        "gw_name" -> nonEmptyText,
	        "policy" -> mapping(
	            "pfs_keys" -> text,
	            "proposal_set" -> nonEmptyText,
	            "proposals" -> list(text)
        		)
        		(VpnP2PolicyForm.apply)
        		(VpnP2PolicyForm.unapply),
	         "bind_interface" -> nonEmptyText,
	         "anti_replay" -> boolean,
	         "establish_immediately" -> boolean,
	         "monitor_enabled" -> boolean,
//	         "monitor_dest" -> text.verifying("Invalid IP", v => v == null || v.isEmpty || srxlib.Util.isValidIPAddress(v)),
	         "monitor_dest" -> optional(text.verifying("Invalid IP", v => srxlib.Util.isValidIPAddress(v))),
	         "monitor_src_interface" -> optional(text),
	         "monitor_optimized" -> boolean
	         )
	         (VpnForm.apply)
	         (VpnForm.unapply)
        ) 
        
	def createVpn(deviceName:String) = addToken {
		createVpnAction(deviceName, false)
	}
	
	def createVpnPost(deviceName:String) = checkToken {
		createVpnAction(deviceName, true)
	}
	
	def createVpnAction(deviceName:String, isPost:Boolean) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	    	import views.html.vpns.vpn.{create => view}
	    	val config = device.config
	    	val form = vpnForm(None)
	    
	    	import srxlib.vpn.IPSecPolicy.PROPOSAL_SET_STANDARD
	    	
	    	if (isPost) form.bindFromRequest.fold(
	    		error => Ok(view(error)),
	    		value => try {
	    			val pname = value.makePolicyName(config)
	    			device.config(new srxlib.DeviceConfigures(
	    			    value.policy.toIPSecPolicy(pname).getCreateConfigure,
	    			    value.toIPSecVpn(pname).getCreateConfigure
	    			    ))
	    		  Redirect(routes.VPNs.vpnList(deviceName))
	    		} catch {
	    		  case e:Throwable => Ok(view(form.fill(value), e.getMessage))
	    		}
	    	) else Ok(view(form.fill(VpnForm("", "", VpnP2PolicyForm("", PROPOSAL_SET_STANDARD, List.empty), "", true, true, false, None, None, false))))
	  }
	}
	
	def editVpn(deviceName:String, name:String) = addToken {
		editVpnAction(deviceName, name, false)
	}	
	
	def editVpnPost(deviceName:String, name:String) = checkToken {
		editVpnAction(deviceName, name, true)
	}
	
	def editVpnAction(deviceName:String, name:String, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device => 
		  val config = device.config
		  config.findIPSecVpn(name) match {
		    case models.IPSecVpn(vpn) => {
		    	import views.html.vpns.vpn.{edit => view}
		    	val form = vpnForm(Some(vpn))
		    	val policy = Option(vpn.getPolicy(config))
		    	
		    	if (isPost) form.bindFromRequest.fold (
		    	    error => Ok(view(error, name)),
		    	    value => try {
		    	    	policy match {
		    	    	  case Some(pv) => device.config(new srxlib.DeviceConfigures(
		    	    	      value.policy.toIPSecPolicy(pv.getName).getEditConfigure,
		    	    	      value.toIPSecVpn(pv.getName).getEditConfigure
		    	    	      ))
		    	    	  case _ => {
		    	    		  val pname = value.makePolicyName(config)
		    	    			device.config(new srxlib.DeviceConfigures(
		    	    						value.policy.toIPSecPolicy(pname).getCreateConfigure,
		    	    						value.toIPSecVpn(pname).getEditConfigure
		    	    					))	    	    		  
		    	    	  }
		    	    	}
		    	    	Redirect(routes.VPNs.vpnList(deviceName))
		    	    } catch {
		    	    	case e:Throwable => Ok(view(form.fill(value), name, e.getMessage))
		    	    }
		    	) else Ok(view(form.fill(VpnForm(vpn, policy)), name))
		    }
		    case _ => Redirect(routes.VPNs.vpnList(deviceName))
		  }
		}
	}
	
	def editVpnProxyId(deviceName:String, name:String) = addToken {
		editVpnProxyIdAction(deviceName, name, false)
	}
	
	def editVpnProxyIdPost(deviceName:String, name:String) = checkToken {
		editVpnProxyIdAction(deviceName, name, true)
	}

	def proxyIdForm = Form(
	    mapping(
   	        "local" -> mapping(
	            "addr" -> nonEmptyText,
	            "mask" -> number
   	        	)
   	        	(RtNetForm.apply)
   	        	(RtNetForm.unapply)
   	        	.verifying("Invalid local IP.", v => srxlib.Util.isValidNetworkAddress(v.addr, v.mask)),
   	        "remote" -> mapping(
	            "addr" -> nonEmptyText,
	            "mask" -> number
   	        	)
   	        	(RtNetForm.apply)
   	        	(RtNetForm.unapply)
   	        	.verifying("Invalid remote IP.", v => srxlib.Util.isValidNetworkAddress(v.addr, v.mask)),
   	        "service" -> nonEmptyText
	    	)
	    	(VpnProxyIdForm.apply)
	    	(VpnProxyIdForm.unapply)
    )
	    
	def editVpnProxyIdAction(deviceName:String, name:String, isPost:Boolean) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	    val config = device.config
	    config.findIPSecVpn(name) match {
	      case models.IPSecVpn(vpn) => {
	    	  import views.html.vpns.vpn.{proxyid => view}
	    	  val form = proxyIdForm
	    	  
	    	  if (isPost) form.bindFromRequest.fold(
	    	      error => Ok(view(error, name)),
	    	      value => try {
	    	    	  device.config(vpn.getEditProxyIdConfigure(value.toProxyId))
	    	        Redirect(routes.VPNs.vpnList(deviceName))
	    	      } catch {
	    	        case e:Throwable => Ok(view(form.fill(value), name, e.getMessage))
	    	      }
	    	  )	else Ok(view(form.fill(VpnProxyIdForm(vpn)), name))
	      }
	      case _ => Redirect(routes.VPNs.vpnList(deviceName))
	    }
	  }
	}
	
	def deleteVpnProxyIdPost(deviceName:String, name:String) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	  	val config = device.config
	  	config.findIPSecVpn(name) match {
	  	  case models.IPSecVpn(vpn) => device.config(vpn.getDeleteProxyIdConfigure)
	  	  case _ =>
	  	}
	  	Redirect(routes.VPNs.vpnList(deviceName))
	  }
	}
	
	def deleteVpnPost(deviceName:String, name:String) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device => 
		  device.config.findIPSecVpn(name) match {
		    case models.IPSecVpn(vpn) => {
		    	device.config(vpn.getDeleteConfigure(true))
		    }
		  }
		  Redirect(routes.VPNs.vpnList(deviceName))
		}
	}
	
	def monitor(deviceName:String) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  	import views.html.vpns.{monitor => view}
		  
		  	Ok(view(device.ipsecSAInformationList.asScala.toList))
		}
	}
}