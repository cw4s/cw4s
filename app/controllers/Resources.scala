package controllers

import javax.inject.Inject
import play.api.mvc.{ControllerComponents, AbstractController}

import java.nio.file.Paths
import java.nio.file.Files
import java.io.File
import java.io.FileNotFoundException

import scala.collection.JavaConverters._

import models.Forms._
import to.lef.srxlib
import javax.inject.Inject;
import scala.concurrent.ExecutionContext

class Resources @Inject()(configuration: play.api.Configuration, components:ControllerComponents)(implicit ec:ExecutionContext) extends AbstractController(components) {
	def logo = Action {
	  try {
	    val userDir = configuration.get[String]("user.dir")
	    val path = Paths.get(userDir, "resources", "logo.png")

	    if (Files.exists(path) == false) {
	      throw new FileNotFoundException(path.toString());    
	    }
	    
	    Ok.sendFile(
	    		content = path.toFile,
	    		inline = true
	    		)
	  } catch {
	    case e:Throwable => Redirect(routes.Assets.at("images/logo.png"))
	  }
	}
	
	def initialSetupData = Action {
	  try {
	    val userDir = configuration.get[String]("user.dir")
	    val path = Paths.get(userDir, "resources", "initial_setup_data.json")
	    
		  if (Files.exists(path) == false) {
	      throw new FileNotFoundException(path.toString());    
	    }
    
	    Ok.sendFile(
	        content = path.toFile,
	        inline = true
        )
	  } catch {
	    case e:Throwable => Redirect(routes.Assets.at("javascripts/data.json"))
	  }  
	}
}