package controllers

import play.api.mvc.{MessagesControllerComponents, MessagesAbstractController}
import play.api.data.Form
import play.api.data.Forms._
import play.filters.csrf.{CSRFAddToken, CSRFCheck}
import scala.collection.JavaConverters._
import models.Forms._
import to.lef.srxlib

import play.api.i18n._
import javax.inject.Inject
import scala.concurrent.ExecutionContext

class NetworkDns @Inject()(val deviceDAO:models.DeviceDAO, addToken:CSRFAddToken, checkToken:CSRFCheck, mcc:MessagesControllerComponents)(implicit ec:ExecutionContext) extends MessagesAbstractController(mcc) with models.DeviceSession {
  def dnsHostForm = Form(
      mapping(
          "hostName" -> optional(text),
          "domainName" -> optional(text),
          "dnsServer1" -> optional(text).verifying("Invalid dns server address.", v => !v.isDefined || srxlib.Util.isValidIPAddress(v.get)),
          "dnsServer2" -> optional(text).verifying("Invalid dns server address.", v => !v.isDefined || srxlib.Util.isValidIPAddress(v.get)),
          "dnsServer3" -> optional(text).verifying("Invalid dns server address.", v => !v.isDefined || srxlib.Util.isValidIPAddress(v.get))
          )
      (DnsHostForm.apply)
      (DnsHostForm.unapply)
  )
  
  def editHost(deviceName:String) = addToken {
	  editHostAction(deviceName, false)
  }
  
  def editHostPost(deviceName:String) = checkToken {
	  editHostAction(deviceName, true)
  }
  
  def editHostAction(deviceName:String, isPost:Boolean) = Action.async { implicit req =>
    	withDevice(deviceName) { implicit device =>
    	  	import views.html.network.dns.{editHost => view}
    		val config = device.config
    		val form = dnsHostForm
    		
    		if (isPost) form.bindFromRequest.fold(
    		    error => Ok(view(error)),
    		    value => try {
    		      device.config(value.toDnsHost.getEditConfigure)
    		      Redirect(routes.NetworkDns.editHost(deviceName))
    		    } catch {
    		      case e:Throwable => Ok(view(form.fill(value), e.getMessage))
    		    }
    		) else Ok(view(form.fill(DnsHostForm(config.findDnsHost))))
    	}
  }
}