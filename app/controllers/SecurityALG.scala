package controllers

import play.api.mvc.{MessagesControllerComponents, MessagesAbstractController}
import play.api.data.Form
import play.api.data.Forms._
import play.filters.csrf.{CSRFAddToken, CSRFCheck}
import scala.collection.JavaConverters._
import models.Forms._
import to.lef.srxlib

import javax.inject.Inject
import scala.concurrent.ExecutionContext

class SecurityALG @Inject()(val deviceDAO:models.DeviceDAO, addToken:CSRFAddToken, checkToken:CSRFCheck, mcc:MessagesControllerComponents)(implicit ec:ExecutionContext) extends MessagesAbstractController(mcc) with models.DeviceSession {
    def algBasicForm = Form(
        mapping(
            "dns" -> boolean,
            "ftp" -> boolean,
            "h323" -> boolean,
            "ike-esp-nat" -> boolean,
            "mgcp" -> boolean,
            "msrpc" -> boolean,
            "pptp" -> boolean,
            "real" -> boolean,
            "rsh" -> boolean,
            "rtsp" -> boolean,
            "sccp" -> boolean,
            "sip" -> boolean,
            "sql" -> boolean,
            "sunrpc" -> boolean,
            "talk" -> boolean,
            "tftp" -> boolean
        )
        (ALGBasicForm.apply)
        (ALGBasicForm.unapply)
    )
    
	def editBasic(deviceName:String) = addToken {
		editBasicAction(deviceName, false)
	}
	
	def editBasicPost(deviceName:String) = checkToken {
		editBasicAction(deviceName, true)
	}
	
	def editBasicAction(deviceName:String, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device => 
		    import views.html.security.alg.{editBasic => view}
			val config = device.config
			val form = algBasicForm
			
			if (isPost) form.bindFromRequest.fold(
			    error => Ok(view(error)),
			    value => try {
			      device.config(value.toALG.getBasicConfigure)
			      Redirect(routes.SecurityALG.editBasic(deviceName))
			    } catch {
			      case e:Throwable => Ok(view(form.fill(value), e.getMessage))
			    }
			) else Ok(view(form.fill(ALGBasicForm(config.findALG))))
		}
	}
}