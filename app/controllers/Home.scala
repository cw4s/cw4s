package controllers

import play.api.mvc.{ControllerComponents, AbstractController}
import javax.inject.Inject
import scala.concurrent.ExecutionContext

class Home @Inject()(val deviceDAO:models.DeviceDAO, components:ControllerComponents)(implicit ec:ExecutionContext) extends AbstractController(components) with models.DeviceSession {
	def index(name:String, refresh:Option[Boolean], interval:Option[Int]) = Action.async {
	  withDevice(name) { implicit device => 
	    val ref = refresh.getOrElse(false)
	    if (ref) {
	     device.updateHomeCache
	    }
		  Ok(views.html.home.index(interval.getOrElse(0), ref))
	  }
	}
	
	def config(name:String) = Action.async {
	  withDevice(name) { implicit device =>
	    Ok(views.html.home.config(device))
	  }
	}
}