package controllers

import play.api.mvc.{ControllerComponents, AbstractController}
import javax.inject.Inject

class HACGenerator @Inject()(components:ControllerComponents) extends AbstractController(components) {
	def index = Action {
	    Ok(views.html.tools.hacg.index())
	}
}