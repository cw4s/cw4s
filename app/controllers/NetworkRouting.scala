package controllers

import play.api.Logging
import play.api.mvc.{MessagesControllerComponents, MessagesAbstractController}
import play.api.data.Form
import play.api.data.Forms._

import play.filters.csrf.{CSRFAddToken, CSRFCheck}

import scala.collection.JavaConverters._
import models.Forms._

import to.lef.srxlib

import javax.inject.Inject
import scala.concurrent.ExecutionContext

class NetworkRouting @Inject()(val deviceDAO:models.DeviceDAO, addToken:CSRFAddToken, checkToken:CSRFCheck, mcc:MessagesControllerComponents)(implicit ec:ExecutionContext) extends MessagesAbstractController(mcc) with Logging with models.DeviceSession{
	def destination(deviceName:String) = Action.async {
		withDevice(deviceName) { implicit device => 
			Ok(views.html.network.routing.destination(device.routeInformation))
		}
	}

	def rtStaticForm = Form(
	    mapping(
	        "net" -> mapping(
	            "addr" -> nonEmptyText,
	            "mask" -> number
	        )
	        (RtNetForm.apply)
	        (RtNetForm.unapply)
	        .verifying("Invalid network address.", v => srxlib.Util.isValidNetworkAddress(v.addr, v.mask)),
	        "gateway" -> optional(text).verifying("Invalid gateway.", v => !v.isDefined || srxlib.Util.isValidIPAddress(v.get)),
	        "interface" -> optional(text),
	        "tag" -> optional(number),
	        "metric" -> optional(number),
	        "preference" -> optional(number)
	    )
	    (RtStaticForm.apply)
	    (RtStaticForm.unapply)
	)
	
	def createDestination(deviceName:String) = addToken {
	  createAction(deviceName, false)
	}
	
	def createDestinationPost(deviceName:String) = checkToken {
	  createAction(deviceName, true)
	} 
	
	def createAction(deviceName:String, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  import views.html.network.routing.{create => view}
		  
		  if (isPost) {
			  rtStaticForm.bindFromRequest.fold(
			      error => Ok(view(error)),
			      value => try {
			        device.config(value.toStaticRoute.getCreateConfigure)
			        Redirect(routes.NetworkRouting.destination(deviceName))
			      } catch {
			        case e:Throwable => {
			          logger.debug(e.getMessage, e)
			          Ok(view(rtStaticForm.fill(value), e.getMessage))
			        }
			      }
			  )
		  } else {
			  Ok(view(rtStaticForm.fill(RtStaticForm(RtNetForm("", 0), None, None, None, None, None))))
		  }
		}
	}
	
	def deleteDestinationPost(deviceName:String, ribName:String, dstName:String, gwName:Option[String], intName:Option[String]) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device => try {
			device.config(srxlib.route.StaticRoute.getDeleteConfigure(ribName, dstName, gwName.getOrElse(null), intName.getOrElse(null)));
		} catch {
		  case e:Throwable => logger.debug(e.getMessage, e)
		} finally {}
			Redirect(routes.NetworkRouting.destination(deviceName))
		}
	}
}