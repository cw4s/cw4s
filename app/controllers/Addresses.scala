package controllers

import play.api.mvc.{MessagesControllerComponents, MessagesAbstractController}
import play.api.data.{Form, Forms}
import play.api.data.Forms._
import play.api.i18n._
import play.filters.csrf.{CSRFAddToken, CSRFCheck}
import scala.collection.JavaConverters._

import javax.inject.Inject

import models.Forms._
import to.lef.srxlib
import cw4s.Validations._

import srxlib.policy.AddressElement.ADDRESS_TYPE_IP_PREFIX
import srxlib.policy.AddressElement.ADDRESS_TYPE_DNS_NAME
import scala.concurrent.ExecutionContext

class Addresses @Inject()(val deviceDAO:models.DeviceDAO, addToken:CSRFAddToken, checkToken:CSRFCheck, mcc:MessagesControllerComponents)(implicit ec:ExecutionContext) extends MessagesAbstractController(mcc) with models.DeviceSession {
	def list(deviceName:String, zoneName:Option[String]) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	  	Ok(views.html.addresses.list(zoneName.getOrElse("")))
	  }
	}

	def addressForm(d:srxlib.Device, oldZoneName:String = "", oldName:String = "") = Form(
	    mapping(
	        "name" -> addressName,
	        "comment" -> optional(text),
	        "adr" -> tuple(
	            "option" -> number(min = 1, max = 2),
	            "ip" -> optional(text),
	            "mask" -> optional(number),
	            "dn" -> optional(text)
	        )
	        .verifying("Invalid IP address",   v => (v._1 != ADDRESS_TYPE_IP_PREFIX || (v._2.isDefined && srxlib.Util.isValidIPAddress(v._2.get))))
		    .verifying("Invalid Netmask",      v => (v._1 != ADDRESS_TYPE_IP_PREFIX || (v._3.isDefined && v._3.get >= 0 &&
		    														((srxlib.Util.isValidIPv4Address(v._2.getOrElse("")) && v._3.get <= 32)) ||
		    														 (srxlib.Util.isValidIPv6Address(v._2.getOrElse("")) && v._3.get <= 128))))
//		    .verifying("Invalid Netmask",      v => (v._1 != ADDRESS_TYPE_IP_PREFIX || (v._3.isDefined && 0 <= v._3.get && v._3.get <= 32)))
		    .verifying("Invalid Domain name",  v => (v._1 != ADDRESS_TYPE_DNS_NAME  || (v._4.isDefined && !v._4.get.isEmpty))),
		    "adr_regular_zone_name" -> nonEmptyText.verifying("Zone is not exist", d.config.isSecurityZoneExist(_))
	    )
	    ((n, c, a, z) => AddressForm(n, c, a._1, a._2, a._3, a._4, z))
	    (a => Some(a.name, a.comment, (a.addrType, a.ip, a.mask, a.dn), a.zoneName))
	    .verifying("Address name is already exist", v => ((v.name == oldName && v.zoneName == oldZoneName) || !d.config.isAddressOrAddressSetExist(v.zoneName, v.name)))
    )

    def addressGroupForm(d:srxlib.Device, zoneName:String, oldName:String = "") = Form(
        mapping(
            "name" -> addressSetName,
            "comment" -> optional(text),
            "addresses" -> Forms.list(text)
        )
        ((n, c, a) => AddressGroupForm(n, zoneName, c, a))
        ((g => Some(g.name, g.comment, g.addresses)))
        .verifying("AddressSet name is already exist", v => ((v.name == oldName) || !d.config.isAddressOrAddressSetExist(zoneName, v.name)))
        .verifying("No address is selected", v => (v.addresses.exists(n => n.length() > 0)))
    )
    
	def createAddress(deviceName:String, zoneName:Option[String]) = addToken {
		Action.async { implicit req => 
		  	withDevice(deviceName) { implicit device => 
		  	  val form = addressForm(device).fill(AddressForm("", None, ADDRESS_TYPE_IP_PREFIX, None, None, None, zoneName.getOrElse("")))
		  	  Ok(views.html.addresses.createAddress(form))
		  	}
		}
	}
	
	def createAddressPost(deviceName:String) = checkToken {
	  Action.async { implicit req => 
	    withDevice(deviceName) { implicit device =>
	      addressForm(device).bindFromRequest.fold(
	          error => Ok(views.html.addresses.createAddress(error)),
	          value => try {
	            device.config(value.toAddressElement.getCreateConfigure) 
	            Redirect(routes.Addresses.list(device.name, None))
	          }
	          catch {
	            case e:Throwable => Ok(views.html.addresses.createAddress(addressForm(device).fill(value), e.getMessage))
	          }
	      )
	    }
	  }
	}

	def editAddress(deviceName:String, zoneName:String, addrName:String) = addToken {
	  editAddressAction(deviceName, zoneName, addrName, false)
	}

	def editAddressPost(deviceName:String, zoneName:String, addrName:String) = checkToken {
	  editAddressAction(deviceName, zoneName, addrName, true)
	}

	def editAddressAction(deviceName:String, zoneName:String, addrName:String, isPost:Boolean) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	    import views.html.addresses.{editAddress => view}
	    val config = device.config
	    config.findAddress(zoneName, addrName) match {
	      case models.AddressElement(old) if (old.isEditable(config))=> {
	    	val form  = addressForm(device, zoneName, addrName)
	        val inUse = old.isInUse(config)
	        if (isPost) form.bindFromRequest.fold(
	            error => Ok(view(error, zoneName, addrName, inUse)),
    	        value => try {
    	        	device.config(value.toAddressElement.getEditConfigure(old))
    	        	Redirect(routes.Addresses.list(deviceName, None))
    	        }
    	        catch {
	    	        case e:Throwable => Ok(view(form.fill(value), zoneName, addrName, inUse, e.getMessage()))
    	        }
	        )
	        else Ok (view(form.fill(AddressForm(old)), zoneName, addrName, inUse))
	      }
	      case _ => Redirect(routes.Addresses.list(deviceName, None))
	    }
	  }
	}
	
	def deleteAddressPost(deviceName:String, zoneName:String, addrName:String) = Action.async { implicit req =>
	  	withDevice(deviceName) { implicit device =>
	  	  if (device.config.isAddressExist(zoneName, addrName)) {
	  	    try {
	  	    	device.config(srxlib.policy.AddressElement.getDeleteConfigure(zoneName, addrName));
	  	    }
	  	    catch {
	  	      case e:Throwable => Redirect(routes.Addresses.list(deviceName, None))
	  	    }
	  	  }
  	      Redirect(routes.Addresses.list(deviceName, None))
	  	}
	}
	
	def groups(deviceName:String) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device => 
			val zn = device.config.securityZoneNameList.asScala.toList.headOption.getOrElse("")
		
			if (zn.isEmpty)
			  Ok(views.html.addresses.groups(zn))
			else 
			  Redirect(routes.Addresses.zoneGroups(deviceName, zn))
		}
	}
	
	def zoneGroups(deviceName:String, zoneName:String) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  Ok(views.html.addresses.groups(zoneName))
		}
	}
	
	def createZoneGroup(deviceName:String, zoneName:String) = addToken {
		Action.async { implicit req => 
		  withDevice(deviceName) { implicit device =>
		  	val form = addressGroupForm(device, zoneName).fill(AddressGroupForm("", "", None, List.empty))
		  	Ok(views.html.addresses.createGroup(zoneName, form))
		  }
		}
	}
	
	def createZoneGroupPost(deviceName:String, zoneName:String) = checkToken {
		Action.async { implicit req => 
		  withDevice(deviceName) { implicit device =>
		  	addressGroupForm(device, zoneName).bindFromRequest.fold(
		  	    error => Ok(views.html.addresses.createGroup(zoneName, error)),
		  	    value => try {
		  	      device.config(value.toAddressSet.getCreateConfigure)
		  	      Redirect(routes.Addresses.zoneGroups(deviceName, zoneName))
		  	    }
		  	    catch {
		  	      case e:Throwable => Ok(views.html.addresses.createGroup(zoneName, addressGroupForm(device, zoneName).fill(value), e.getMessage))
		  	    }
		  	)
		  }
		}  
	}
	
	def editZoneGroup(deviceName:String, zoneName:String, groupName:String) = addToken {
	  Action.async { implicit req =>
	    withDevice(deviceName) { implicit device =>
	      val config = device.config
	      config.findAddressSet(zoneName, groupName) match {
	        case models.AddressSet(old) if (old.isEditable(config))=> {
	        	val form = addressGroupForm(device, zoneName, old.getName).fill(AddressGroupForm(old))
	        	Ok(views.html.addresses.editGroup(zoneName, groupName, form))
	        }
	        case _ => Redirect(routes.Addresses.zoneGroups(deviceName, zoneName))
	      }
	    }
	  }
	}
	
	def editZoneGroupPost(deviceName:String, zoneName:String, groupName:String) = checkToken {
		Action.async { implicit req =>
		  withDevice(deviceName) { implicit device =>
		    val config = device.config
		    config.findAddressSet(zoneName, groupName) match {
		    	case models.AddressSet(old) if (old.isEditable(config)) => {
		    		addressGroupForm(device, zoneName, old.getName).bindFromRequest.fold(
		    		    error => Ok(views.html.addresses.editGroup(zoneName, groupName, error)),
		    		    value => try {
		    		    	device.config(value.toAddressSet(device).getEditConfigure(old))
		    		    	Redirect(routes.Addresses.zoneGroups(deviceName, zoneName))
		    		    }
		    		    catch {
		    		      case e:Throwable => Ok(views.html.addresses.editGroup(zoneName, groupName, addressGroupForm(device, zoneName, old.getName).fill(value), e.getMessage()))
		    		    }
		    		)
		    	}
		    	case _ => Redirect(routes.Addresses.zoneGroups(deviceName, zoneName))
		    }
		  }
		}
	}
	
	def deleteZoneGroupPost(deviceName:String, zoneName:String, addrName:String) = Action.async { implicit req =>
	  	withDevice(deviceName) { implicit device =>
	  	  if (device.config.isAddressSetExist(zoneName, addrName)) {
	  	   	device.config(srxlib.policy.AddressSet.getDeleteConfigure(zoneName, addrName));
	  	  }
  	      Redirect(routes.Addresses.zoneGroups(deviceName, zoneName))
	  	}
	}
	
}