package controllers

import play.api.mvc.{MessagesControllerComponents, MessagesAbstractController}

import play.api.data.Form
import play.api.data.Forms._

import play.filters.csrf.{CSRFAddToken, CSRFCheck}

import scala.collection.JavaConverters._

import to.lef.srxlib
import models.Forms._

import javax.inject.Inject

import scala.concurrent.ExecutionContext

class NetworkDhcp @Inject()(val deviceDAO:models.DeviceDAO, addToken:CSRFAddToken, checkToken:CSRFCheck, mcc:MessagesControllerComponents)(implicit ec:ExecutionContext) extends MessagesAbstractController(mcc) with models.DeviceSession{
	def serviceList(deviceName:String) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	    	Ok(views.html.network.dhcp.list(device))
	  }
	}

	def dhcpForm(d:srxlib.dhcp.DhcpService) = Form(
	    mapping(
	        "service_type" -> number(min = 1, max = 3),
	        "relay1" -> optional(text),
	        "relay2" -> optional(text),
	        "relay3" -> optional(text),
	        "lease_type" -> optional(number),
	        "lease_day" -> optional(number(min = 0)),
	        "lease_hour" -> optional(number(min = 0, max = 59)),
	        "lease_min" -> optional(number(min = 0, max = 59)),
	        "address_low" -> optional(text).verifying("Invalid low address", v => !v.isDefined || (srxlib.Util.isValidIPv4Address(v.get) && d.getNetworkAddress.isSameNetwork(v.get))),
	        "address_high" -> optional(text).verifying("Invalid high address", v => !v.isDefined || (srxlib.Util.isValidIPv4Address(v.get) && d.getNetworkAddress.isSameNetwork(v.get))),
	        "gateway" -> optional(text).verifying("Invalid gateway address", v => !v.isDefined || (srxlib.Util.isValidIPv4Address(v.get) && d.getNetworkAddress.isSameNetwork(v.get))),
	        "dns_server" -> optional(text).verifying("Invalid DNS server address", v => !v.isDefined || srxlib.Util.isValidIPv4Address(v.get)),
	        "wins_server" -> optional(text).verifying("Invalid WINS server address", v => !v.isDefined || srxlib.Util.isValidIPv4Address(v.get))
	    )
	    (DhcpForm.apply)
	    (DhcpForm.unapply)
	)
	
	def edit(deviceName:String, lname:String, addr:String) = addToken {
		editAction(deviceName, lname, addr, false)
	}
	
	def editPost(deviceName:String, lname:String, addr:String) = checkToken {
		editAction(deviceName, lname, addr, true)
	}
	
	def editAction(deviceName:String, lname:String, addr:String, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  	import views.html.network.dhcp.{edit => view}
		  	val config = device.config
		  	config.findLogicalInterface(lname) match {
		  	  case models.LogicalInetInterface(lif) => {
		  		  lif.findDhcpService(addr, config) match {
		  		    case models.DhcpService(dhcp) => { 
		  		    	val form = dhcpForm(dhcp)
		  		      if (isPost) form.bindFromRequest.fold(
		  		        error => Ok(view(error, dhcp)),
		  		        value => try {
		  		        	device.config(value.toDhcpService(lname, addr).getEditConfigure(dhcp))
		  		        	Redirect(routes.NetworkDhcp.serviceList(deviceName))
		  		        } catch {
		  		          case e:Throwable => Ok(view(form.fill(value), dhcp, e.getMessage))
		  		        }
		  		      ) else Ok(view(form.fill(DhcpForm(dhcp)), dhcp))
		  		    }
		  		    case _ => Redirect(routes.NetworkDhcp.serviceList(deviceName))
		  		  }
		  	  }
		  	  case _ => Redirect(routes.NetworkDhcp.serviceList(deviceName))
		  	}
		}
	}
}