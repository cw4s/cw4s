package controllers

import play.api.Logging
import play.api.mvc.{MessagesControllerComponents, MessagesAbstractController}
import play.api.data.{Form, Forms}
import play.api.data.Forms._
import play.filters.csrf.{CSRFAddToken, CSRFCheck}
import scala.collection.JavaConverters._
import org.w3c.dom.NodeList
import org.w3c.dom.Node
import to.lef.srxlib
import models.Forms._
import to.lef.srxlib

import javax.inject.Inject

import scala.concurrent.ExecutionContext

class NetworkInterfaces @Inject()(val deviceDAO:models.DeviceDAO, addToken:CSRFAddToken, checkToken:CSRFCheck, mcc:MessagesControllerComponents)(implicit ec:ExecutionContext) extends MessagesAbstractController(mcc) with Logging with models.DeviceSession{
	def list(deviceName:String) = Action.async {
	    withDevice(deviceName) { implicit device =>  
	    	val interfaces = device.logicalInterfaceList()
	    	Ok(views.html.network.interfaces.list(
	    	    interfaces.asScala.toSeq,
	    	    device.interfaceInformation
	    	    ))
	    }
	}
	
	def create(deviceName:String) = Action { implicit req => 
	  Form("target" -> nonEmptyText).bindFromRequest.fold(
	      e => Redirect(routes.NetworkInterfaces.list(deviceName)),
	      v => v match {
	      	case "loopback" => Redirect(routes.NetworkInterfaces.createLoopback(deviceName))
	      	case "sub" 		=> Redirect(routes.NetworkInterfaces.createSub(deviceName))
	      	case "vlan" 	=> Redirect(routes.NetworkInterfaces.createVlan(deviceName))
	      	case "vpn"		=> Redirect(routes.NetworkInterfaces.createVpn(deviceName))
	      	case "irb" 		=> Redirect(routes.NetworkInterfaces.createIrb(deviceName))
	      	case _ 			=> Redirect(routes.NetworkInterfaces.list(deviceName))
	      }
      )
	}
	
	def createLoopback(deviceName:String) = TODO

	private lazy val protocolMapping = mapping(
	        		"bfd"   -> boolean,
	        		"bgp"   -> boolean,
	        		"dvmrp" -> boolean,
	        		"igmp"  -> boolean,
	        		"ldp"   -> boolean,
	        		"msdp"  -> boolean,
	        		"nhrp"  -> boolean,
	        		"ospf"  -> boolean,
	        		"ospf3" -> boolean,
	        		"pgm"   -> boolean,
	        		"pim"   -> boolean,
	        		"rip"   -> boolean,
	        		"ripng" -> boolean,
	        		"router-discovery" -> boolean,
	        		"rsvp"  -> boolean,
	        		"sap"   -> boolean,
	        		"vrrp"  -> boolean
	        		) (IfInboundProtocolForm.apply) (IfInboundProtocolForm.unapply)
	        		
    private lazy val service1Mapping = mapping(
	        		"bootp"          -> boolean,
	        		"dhcp"           -> boolean,
	        		"dhcpv6"         -> boolean,
	        		"dns"            -> boolean,
	        		"finger"         -> boolean,
	        		"ftp"            -> boolean,
	        		"http"           -> boolean,
	        		"https"          -> boolean,
	        		"ident-reset"    -> boolean,
	        		"ike"            -> boolean,
	        		"lsping"         -> boolean,
	        		"netconf"        -> boolean,
	        		"ntp"            -> boolean,
	        		"ping"           -> boolean,
	        		"r2cp"           -> boolean,
	        		"reverse-ssh"    -> boolean,
	        		"reverse-telnet" -> boolean,
	        		"rlogin"         -> boolean
	        		) (IfInboundService1Form.apply) (IfInboundService1Form.unapply)
	        		
	private lazy val service2Mapping =  mapping(
	        		"rpm"            -> boolean,
	        		"rsh"            -> boolean,
	        		"sip"            -> boolean,
	        		"snmp"           -> boolean,
	        		"snmp-trap"      -> boolean,
	        		"ssh"            -> boolean,
	        		"telnet"         -> boolean,
	        		"tftp"           -> boolean,
	        		"traceroute"     -> boolean,
	        		"xnm-clear-text" -> boolean,
	        		"xnm-ssl"        -> boolean
	        		) (IfInboundService2Form.apply) (IfInboundService2Form.unapply)
	        		
	def subIfForm(implicit d:srxlib.Device) = Form(
		mapping(
		    "subif" -> mapping(
		    		"name" -> nonEmptyText.verifying("Physical Interface is not exist", v => d.physicalInterfaceInformation.isPhysicalInterfaceExist(v) && srxlib.Util.canInterfaceCreateSubIf(v, d.config)),
		    		"tag" -> number
		    		)
		    		(IfNameForm.apply)
		    		(IfNameForm.unapply)
		    		.verifying("Interface is already exist.", v => !d.config.isLogicalInterfaceExist(v.phyName, v.unitId)),
	        "interface_zone_name" -> text.verifying("Zone is not exist", v => v.isEmpty || d.config.isSecurityZoneExist(v)),
  		    "ip" -> mapping (
		        "type" -> ignored(srxlib.nic.LogicalInetInterface.INET4_TYPE_STATIC),
		        "address" -> nonEmptyText.verifying("Invalid IP Address", v => srxlib.Util.isValidIPv4Address(v)),
		        "mask" -> number(min = 0, max = 32)
		        )
		        ((t, a, m) => IfInetFamilyForm(t, None, Some(a), Some(m)))
		        (v => Some(v.ipType, v.address.getOrElse("0.0.0.0"), v.mask.getOrElse(0)))
		        .verifying("Invalid IP Address", v => srxlib.Util.isValidHostAddress(v.address.get, v.mask.get, true))
		        ,

	        "vlan_tag" -> optional(number(min = 0, max = 4095)),
	        "services1" -> service1Mapping,
	        "services2" -> service2Mapping,
	        "protocols" -> protocolMapping,
	        "mtu" -> optional(number(min = 0)),
	        "admin_state_up" -> boolean
	    )
	    (IfBasicForm.apply)
	    (IfBasicForm.unapply)
	)

	def createSub(deviceName:String) = addToken {
		createSubAction(deviceName, false)
	}

	def createSubPost(deviceName:String) = checkToken {
		createSubAction(deviceName, true)
	}

	def createSubAction(deviceName:String, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		import views.html.network.interfaces.{createSub => view}
		val f = subIfForm
			if (isPost) f.bindFromRequest.fold(
					error => Ok(view(error)),
					value => try {
								device.config(value.toPortInetInterface.getCreateConfigure)
								Redirect(routes.NetworkInterfaces.list(device.name))
					}
					catch {
						case e:Throwable => Ok(view(f.fill(value), e.getMessage()))
					}
			)
			else Ok(view(f.fill(IfBasicForm(IfNameForm("", 0), "", IfInetFamilyForm("0.0.0.0", 0), None, 
			    IfInboundService1Form(),
			    IfInboundService2Form(),
			    IfInboundProtocolForm(),
			    None,
			    true))))
		}
	}

	def vlanIfForm(implicit d:srxlib.Device) = Form(
		mapping(
		    "subif" -> mapping(
		    		"name" -> ignored("vlan"),
		    		"tag" -> number(min = 0)
		    		)
		    		(IfNameForm.apply)
		    		(IfNameForm.unapply)
		    		.verifying("Interface is already exist.", v => !d.config.isLogicalInterfaceExist(v.phyName, v.unitId)),
	        "interface_zone_name" -> text.verifying("Zone is not exist", v => v.isEmpty || d.config.isSecurityZoneExist(v)),
  		    "ip" -> mapping (
		        "type" -> ignored(srxlib.nic.LogicalInetInterface.INET4_TYPE_STATIC),
		        "address" -> nonEmptyText.verifying("Invalid IP Address", v => srxlib.Util.isValidIPv4Address(v)),
		        "mask" -> number(min = 0, max = 32)
		        )
		        ((t, a, m) => IfInetFamilyForm(t, None, Some(a), Some(m)))
		        (v => Some(v.ipType, v.address.getOrElse("0.0.0.0"), v.mask.getOrElse(0)))
		        .verifying("Invalid IP Address", v => srxlib.Util.isValidHostAddress(v.address.get, v.mask.get, true))
		        ,
		    "vlan_tag" -> optional(number(min = 1, max = 4095)).verifying("Vlan ID is already used.", v => v.isDefined && !d.config.isBoundVlanIdExist(v.get)),
   	        "services1" -> service1Mapping,
	        "services2" -> service2Mapping,
	        "protocols" -> protocolMapping,
	        "mtu" -> optional(number(min = 0)),
	        "admin_state_up" -> boolean
	    )
	    (IfBasicForm.apply)
	    (IfBasicForm.unapply)
	)
	
	def createVlan(deviceName:String) = addToken {
	  createVlanAction(deviceName, false)
	}

	def createVlanPost(deviceName:String) = checkToken {
	  createVlanAction(deviceName, true)
	}

	def createVlanAction(deviceName:String, isPost:Boolean) = Action.async { implicit req =>
	   withDevice(deviceName) { implicit device =>
	     import views.html.network.interfaces.{createVlan => view}
	     val f = vlanIfForm(device)
	     if (isPost) f.bindFromRequest.fold(
	         error => Ok(view(error)),
	         value => try {
 	        	  device.config(value.toVlanInterface.getCreateConfigure)
	        	  Redirect(routes.NetworkInterfaces.list(device.name))          
	         }
	         catch {
	           case e:Throwable => Ok(view(f.fill(value), e.getMessage))
	         }
	     )
	     else 
	    	 Ok(view(f.fill(IfBasicForm(IfNameForm("", 0), "", IfInetFamilyForm("0.0.0.0", 0), None, 
	    	     IfInboundService1Form(), IfInboundService2Form(), IfInboundProtocolForm(),
	    	     None, true))))
	   }
	}
	
	def vpnIfForm(implicit d:srxlib.Device) = {
		import srxlib.nic.LogicalInetInterface.INET4_TYPE_STATIC
		import srxlib.nic.LogicalInetInterface.INET4_TYPE_UNNUMBERED
		
		Form(
				mapping(
						"subif" -> mapping(
								"name" -> ignored("st0"),
								"tag" -> number(min = 0, max = 16385)
								)
								(IfNameForm.apply)
								(IfNameForm.unapply),
						"interface_zone_name" -> text.verifying("Zone is not exist", v => v.isEmpty || d.config.isSecurityZoneExist(v)),
						"ip" -> mapping (
								"type" -> number.verifying("Invalid IP type.",  v => v == INET4_TYPE_STATIC || v == INET4_TYPE_UNNUMBERED),
								"pppoe_instance" -> ignored(Option[String](null)),
								"address" -> optional(text),
								"mask" -> optional(number)
								)
								(IfInetFamilyForm.apply)
								(IfInetFamilyForm.unapply)
								.verifying("Invalid netmask", v => (v.ipType != INET4_TYPE_STATIC || (v.mask.isDefined && 0 <= v.mask.get && v.mask.get <= 32)))
								.verifying("Invalid IP address", v => (v.ipType != INET4_TYPE_STATIC || (v.address.isDefined && v.mask.isDefined && srxlib.Util.isValidIPv4Address(v.address.get) && srxlib.Util.isValidHostAddress(v.address.get, v.mask.get, true))))
								,
						"vlan_tag"  -> ignored(Option[Int](0)),
						"services1" -> service1Mapping,
						"services2" -> service2Mapping,
						"protocols" -> protocolMapping,
						"mtu" -> optional(number(min = 0)),
						"admin_state_up" -> boolean
				)
				(IfBasicForm.apply)
				(IfBasicForm.unapply)
		)
	}
	
	def createVpn(deviceName:String) = addToken {
		createVpnAction(deviceName, false)
	}

	def createVpnPost(deviceName:String) = checkToken {
		createVpnAction(deviceName, true)
	}

	def createVpnAction(deviceName:String, isPost:Boolean) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	    import views.html.network.interfaces.{createVpn => view}
	    
	    val config = device.config
	    val f = vpnIfForm(device)
	
	    if (isPost) f.bindFromRequest.fold(
	         error => Ok(view(error)),
	         value => try {
 	        	  device.config(value.toVpnInterface.getCreateConfigure)
 	        	  Redirect(routes.NetworkInterfaces.list(deviceName))
	         } catch {
	           case e:Throwable => Ok(view(f.fill(value), e.getMessage))
	         }
	     ) else Ok(view(f.fill(IfBasicForm(IfNameForm("", 0), "", IfInetFamilyForm("0.0.0.0", 0), None, 
	    	     IfInboundService1Form(), IfInboundService2Form(), IfInboundProtocolForm(),
	    	     None, true))))
	   }
	}
	
	def createIrb(deviceName:String) = TODO

	def deletePost(deviceName:String, intName:String, unit:Int) = Action.async { implicit req => 
		  withDevice(deviceName) { implicit device => 
		    val config = device.config
		    config.findLogicalInterface(intName, unit) match {
		      case models.LogicalInterface(v) if (v.isRemovable(config)) => {
		    	  device.config(v.getDeleteConfigure())
		      }
		      case _ => 
		    }
    		Redirect(routes.NetworkInterfaces.list(device.name))
		  }
	}
	
	def edit(deviceName:String, pName:String, unitId:Int) = Action {
		Redirect(routes.NetworkInterfaces.editBasic(deviceName, pName, unitId));
	}
	
	def ifBasicForm(lint:srxlib.nic.LogicalInetInterface)(implicit d:srxlib.Device) = Form(
		mapping(
		    "subif" -> mapping(
		    		"name" -> ignored(lint.getPhysicalInterfaceName),
		    		"tag" -> ignored(lint.getUnitId)
		    		)
		    		(IfNameForm.apply)
		    		(IfNameForm.unapply),
    		"interface_zone_name" -> text.verifying("Zone is not exist", v => v.isEmpty || d.config.isSecurityZoneExist(v)),
		    "ip" -> mapping (
		        "type" -> number.verifying("Invalid IP type.", srxlib.Util.isValidInet4Type(_)),
		        "pppoe_instance" -> optional(text),
		        "address" -> optional(text),
		        "mask" -> optional(number)
		        )
		        (IfInetFamilyForm.apply)
		        (IfInetFamilyForm.unapply)
		        .verifying("Invalid netmask", v => (v.ipType != srxlib.nic.LogicalInetInterface.INET4_TYPE_STATIC || (v.mask.isDefined && 0 <= v.mask.get && v.mask.get <= 32)))
		        .verifying("Invalid IP address", v => (v.ipType != srxlib.nic.LogicalInetInterface.INET4_TYPE_STATIC || (v.address.isDefined && v.mask.isDefined && srxlib.Util.isValidIPv4Address(v.address.get) && srxlib.Util.isValidHostAddress(v.address.get, v.mask.get, true))))
		        .verifying("Invalid PPPoE instance", v => (v.ipType != srxlib.nic.LogicalInetInterface.INET4_TYPE_NEGOTIATE || (v.pppoeInstance.isDefined && lint.getName == v.pppoeInstance.get)))
		        ,
		    "vlan_tag" -> {lint match {
		      case models.PortInetInterface(o) => optional(number(min = 0, max = 4095)) 
		      case models.RethInterface(o) => optional(number(min = 0, max = 4095))
		      case _ => ignored(Option[Int](lint.getVlanId))
		    }},
		    "services1" -> service1Mapping,
		    "services2" -> service2Mapping,
		    "protocols" -> protocolMapping,
	        "mtu" -> optional(number(min = 0)),
	        "admin_state_up" -> boolean
	    )
	    (IfBasicForm.apply)
	    (IfBasicForm.unapply)
	)

	def editBasic(deviceName:String, pName:String, unitId:Int) = addToken {
		editBasicAction(deviceName, pName, unitId, false)
	}
	
	def editBasicPost(deviceName:String, pName:String, unitId:Int) = checkToken {
		editBasicAction(deviceName, pName, unitId, true)
	}
	
	def editBasicAction(deviceName:String, pName:String, unitId:Int, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  import views.html.network.interfaces.{editBasic => view}
			device.config.findLogicalInterface(pName, unitId) match {
			  case models.LogicalInetInterface(v) => {
				  val f = ifBasicForm(v)
			      if (isPost) f.bindFromRequest.fold(
					  error => Ok(view(error, v)),
					  value => try {
		            	  v.setZoneName(value.interfaceZoneName)
		            	  if (value.ip.ipType == srxlib.nic.LogicalInetInterface.INET4_TYPE_STATIC) 
		            		  v.setInet4AddressAndMask(value.ip.address.getOrElse("0.0.0.0"), value.ip.mask.getOrElse(0))
		            	  else 
		            		  v.setInet4Type(value.ip.ipType)

		                  v.setInboundProtocols(models.InboundProtocol(value.protocols))
		                  v.setInboundServices(models.InboundService(value.services1, value.services2))
		                  v.setMtu(if (value.mtu.isDefined) value.mtu.get else null)
		            	  v.setDisabled(!value.adminStateUp)
		            	  
		            	  v match {
		            	    case models.PortInetInterface(o) => o.setVlanId(value.vlanId.get)
		            	    case models.RethInterface(o) => o.setVlanId(value.vlanId.get)
		            	    case _ =>
		            	  }
		            	  device.config(v.getBasicConfigure)
		            	  Redirect(routes.NetworkInterfaces.list(deviceName))					  
					  }
					  catch  {
					    case e:Throwable => { e.printStackTrace(); Ok(view(f.fill(value), v, e.getMessage)) }
					  }
			      )
			      else Ok(view(f.fill(IfBasicForm(v)), v))
			  }
			  case _ => Redirect(routes.NetworkInterfaces.list(deviceName))
			}
		}
	}

    
	def editBind(deviceName:String, pName:String, unitId:Int) = addToken {
		editBindAction(deviceName, pName, unitId, false)
	}
	
	def editBindPost(deviceName:String, pName:String, unitId:Int) = addToken {
		editBindAction(deviceName, pName, unitId, true)
	}
	
	def editBindAction(deviceName:String, pName:String, unitId:Int, isPost:Boolean) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	  	import views.html.network.interfaces.{editBind => view}
		val f = Form(
			mapping(
			    "cb_port" -> Forms.list(text),
			    "cb_trunk" -> Forms.list(text)
			)
			(IfBindForm.apply)
			(IfBindForm.unapply)
	    )
	  	
	    device.config.findLogicalInterface(pName, unitId) match {
	      case models.LogicalBindInterface(v) => if(isPost) f.bindFromRequest.fold(
	          error => Ok(view(error, v)),
	          value => try {
	            import srxlib.nic.LogicalBoundInterface.PORT_MODE_ACCESS
	            import srxlib.nic.LogicalBoundInterface.PORT_MODE_TRUNK
	            def toPortMode(v:Boolean) = if (v) PORT_MODE_TRUNK else PORT_MODE_ACCESS
	            
	        	  device.config(v.getBindConfigure(value.ports.map(v => v -> toPortMode(value.trunk.contains(v))).toMap.asJava))
	        	  Redirect(routes.NetworkInterfaces.list(deviceName))
	          }
	          catch {
	            case e:Throwable => Ok(view(f.fill(value), v, e.getMessage))
	          }
	      )
	      else
	        Ok(view(f.fill(IfBindForm(List.empty, List.empty)), v))
	        
	      case _ => Redirect(routes.NetworkInterfaces.list(deviceName))
	    }
	  }
	}
	
	def dip(deviceName:String, pName:String, unitId:Int) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  device.config.findLogicalInterface(pName, unitId) match {
		    case models.LogicalInetInterface(lint) => {
		    	import views.html.network.interfaces.{dip => view}
		    	val candidate = device.candidateConfig.findLogicalInterface(lint.getName()) match {
		    	    case models.LogicalInetInterface(i) => i.getDipMap
		    	    case _ => Map.empty[String, srxlib.nat.Dip].asJava
		    	}
		    	Ok(view(lint, candidate))
		    }
		    case _ => Redirect(routes.NetworkInterfaces.list(deviceName))
		  }
		}
	}
	
	def dipForm(lint:srxlib.nic.LogicalInetInterface) = Form(
			mapping(
			    "startAddress" -> nonEmptyText,
			    "endAddress" -> nonEmptyText,
			    "proxyarp" -> boolean
			    )
			    (IfDipForm.apply)
			    (IfDipForm.unapply)
	    )
	    
	def createDip(deviceName:String, pName:String, unitId:Int) = addToken {
		createDipAction(deviceName, pName, unitId, false)
	}


	def createDipPost(deviceName:String, pName:String, unitId:Int) = checkToken {
	  createDipAction(deviceName, pName, unitId, true)
	}


	def createDipAction(deviceName:String, pName:String, unitId:Int, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  	device.config.findLogicalInterface(pName, unitId) match {
		  	  case models.LogicalInetInterface(lint) => {
		  	    import views.html.network.interfaces.{createDip => view}
		  	    val form = dipForm(lint)
		  	    if (isPost) form.bindFromRequest.fold(
		  	        error => {
		  	        	Ok(view(lint, error))
		  	        },
		  	        value => try {
		  	        	device.config(value.toDip.getCreateConfigure(lint))
		  	        	Redirect(routes.NetworkInterfaces.dip(deviceName, pName, unitId))
		  	        }
		  	        catch {
		  	          case e:Throwable => {
		  	            Ok(view(lint, form.fill(value), e.getMessage))
		  	            }
		  	        }
		  	    )
		  	    else Ok(view(lint, form.fill(IfDipForm("", "", false))))    
		  	  }
		  	  case _ => Redirect(routes.NetworkInterfaces.list(deviceName))
		  	}
		}
	}


	def mip(deviceName:String, pName:String, unitId:Int) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  device.config.findLogicalInterface(pName, unitId) match {
		    case models.LogicalInetInterface(lint) => {
		    	import views.html.network.interfaces.{mip => view}
		    	val candidate = device.candidateConfig.findLogicalInterface(lint.getName()) match {
		    	    case models.LogicalInetInterface(i) => i.getMipMap
		    	    case _ => Map.empty[String, srxlib.nat.Mip].asJava
		    	}
		    	
		    	Ok(view(lint, candidate))
		    }
		    case _ => Redirect(routes.NetworkInterfaces.list(deviceName))
		  }
		}
	}
	
	def mipForm(lint:srxlib.nic.LogicalInetInterface) = Form(
			mapping(
			    "mappedAddress" -> nonEmptyText,
			    "hostAddress" -> nonEmptyText,
			    "netmask" -> nonEmptyText,
			    "proxyarp" -> boolean
			    )
			    (IfMipForm.apply)
			    (IfMipForm.unapply)
	    )
	    
	def createMip(deviceName:String, pName:String, unitId:Int) = addToken {
		createMipAction(deviceName, pName, unitId, false)
	}


	def createMipPost(deviceName:String, pName:String, unitId:Int) = checkToken {
	  createMipAction(deviceName, pName, unitId, true)
	}


	def createMipAction(deviceName:String, pName:String, unitId:Int, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  	device.config.findLogicalInterface(pName, unitId) match {
		  	  case models.LogicalInetInterface(lint) => {
		  	    import views.html.network.interfaces.{createMip => view}
		  	    val form = mipForm(lint)
		  	    if (isPost) form.bindFromRequest.fold(
		  	        error => {
		  	        	Ok(view(lint, error))
		  	        },
		  	        value => try {
		  	        	device.config(value.toMip.getCreateConfigure(lint))
		  	        	Redirect(routes.NetworkInterfaces.mip(deviceName, pName, unitId))
		  	        }
		  	        catch {
		  	          case e:Throwable => {
                    import scala.compat.Platform.EOL
		  	        	  logger.error(e.getCause.getStackTrace().mkString("", EOL, EOL))
		  	            Ok(view(lint, form.fill(value), e.getMessage))
		  	            }
		  	        }
		  	    )
		  	    else Ok(view(lint, form.fill(IfMipForm("", "", "255.255.255.255", false))))    
		  	  }
		  	  case _ => Redirect(routes.NetworkInterfaces.list(deviceName))
		  	}
		}
	}
	
	def deleteMipPost(deviceName:String, intName:String, unit:Int, nname:String) = Action.async { implicit req => 
		  withDevice(deviceName) { implicit device => 
		    import srxlib.nat.Mip
		    val config = device.config
		    
		    config.findLogicalInterface(intName, unit) match {
		      case models.LogicalInterface(v) => config.findInterfaceMip(v.getName, nname) match {
		        case models.Mip(mip) if (mip.isRemovable(config)) => device.config(mip.getDeleteConfigure)
		        case _ =>  
		      }
		      case _ => 
		    }
		    
        	Redirect(routes.NetworkInterfaces.mip(deviceName, intName, unit))
		  }
	}
	
	def vip(deviceName:String, pName:String, unitId:Int) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  device.config.findLogicalInterface(pName, unitId) match {
		    case models.LogicalInetInterface(lint) => {
		    	import views.html.network.interfaces.{vip => view}
		    	val candidate = device.candidateConfig.findLogicalInterface(lint.getName()) match {
		    	    case models.LogicalInetInterface(i) => i.getVipMap
		    	    case _ => Map.empty[String, srxlib.nat.Vip].asJava
		    	}
	
		    	Ok(view(lint, candidate))
		    }
		    case _ => Redirect(routes.NetworkInterfaces.list(deviceName))
		  }
		}
	}
	
	def vipForm(lint:srxlib.nic.LogicalInetInterface) = Form(
			mapping(
			    "virtualAddress" -> nonEmptyText,
			    "virtualPort" -> number(min = 0, max = 65535),
			    "mapProtocol" -> nonEmptyText,
			    "mapPort" -> number(min = 0, max = 65535),
			    "mapAddress" -> nonEmptyText,
			    "proxyarp" -> boolean
			    )
			    (IfVipForm.apply)
			    (IfVipForm.unapply)
	    )
	    
	def createVip(deviceName:String, pName:String, unitId:Int) = addToken {
		createVipAction(deviceName, pName, unitId, false)
	}


	def createVipPost(deviceName:String, pName:String, unitId:Int) = checkToken {
	  createVipAction(deviceName, pName, unitId, true)
	}


	def createVipAction(deviceName:String, pName:String, unitId:Int, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  	device.config.findLogicalInterface(pName, unitId) match {
		  	  case models.LogicalInetInterface(lint) => {
		  	    import views.html.network.interfaces.{createVip => view}
		  	    val form = vipForm(lint)
		  	    if (isPost) form.bindFromRequest.fold(
		  	        error => {
		  	        	Ok(view(lint, error))
		  	        },
		  	        value => try {
		  	        	device.config(value.toVip.getCreateConfigure(lint))
		  	        	Redirect(routes.NetworkInterfaces.vip(deviceName, pName, unitId))
		  	        }
		  	        catch {
		  	          case e:Throwable => {
                    import scala.compat.Platform.EOL
		  	        	  logger.error(e.getCause.getStackTrace().mkString("", EOL, EOL))
		  	            Ok(view(lint, form.fill(value), e.getMessage))
		  	            }
		  	        }
		  	    )
		  	    else Ok(view(lint, form.fill(IfVipForm(lint.getInet4AddressString, 0, "tcp", 0, "", false))))    
		  	  }
		  	  case _ => Redirect(routes.NetworkInterfaces.list(deviceName))
		  	}
		}
	}

	def deleteVipPost(deviceName:String, intName:String, unit:Int, nname:String) = Action.async { implicit req => 
		  withDevice(deviceName) { implicit device => 
		    import srxlib.nat.Vip
		    val config = device.config
		    config.findLogicalInterface(intName, unit) match {
		      case models.LogicalInetInterface(v) => config.findInterfaceVip(v.getName, nname) match {
		        case models.Vip(vip) if (vip.isRemovable(config)) => device.config(vip.getDeleteConfigure)
		        case _ =>
		      	}
		      case _ => 
		    }
        	Redirect(routes.NetworkInterfaces.vip(deviceName, intName, unit))
		  }
	}
	
	def deleteDipPost(deviceName:String, intName:String, unit:Int, nname:String) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  import srxlib.nat.Dip
		  	val config = device.config
		  	config.findLogicalInterface(intName, unit) match {
		    case models.LogicalInetInterface(v) => if (Dip.isRemovable(nname, config)) {
		    	device.config(Dip.getDeleteConfigure(nname))
		    }
		    case _ =>
		  }
		  Redirect(routes.NetworkInterfaces.dip(deviceName, intName, unit))
		}
	}

	def ipForm = Form(
	    mapping(
	        "address" -> nonEmptyText,
	        "mask" -> number
	    )
	    (IfIPForm.apply)
	    (IfIPForm.unapply)
        .verifying("Invalid IP address", v => srxlib.Util.isValidIPv4Address(v.address) && srxlib.Util.isValidHostAddress(v.address, v.mask, false))
	)
	
	def editSecondaryIP(deviceName:String, intName:String, unit:Int) = addToken {
		editSecondaryIPAction(deviceName, intName, unit, false)
	}
	
	def editSecondaryIPPost(deviceName:String, intName:String, unit:Int) = checkToken {
		editSecondaryIPAction(deviceName, intName, unit, true)
	}
	
	def editSecondaryIPAction(deviceName:String, intName:String, unit:Int, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
			device.config.findLogicalInterface(intName, unit) match {
			  case models.LogicalInetInterface(lint) if (lint.isInet4Static) => {
				  import views.html.network.interfaces.{editSecondaryIP => view}
				  val form = ipForm
				  
				  if (isPost) form.bindFromRequest.fold(
				      error => Ok(view(error, lint)),
				      value => try {
				    	  val ip = value.toIPv4AddressAndMask(false)
				    	  lint.addSecondaryAddressAndMask(ip)
				    	  device.config(lint.getSecondaryIPConfigure)
				    	  Redirect(routes.NetworkInterfaces.editSecondaryIP(deviceName, intName, unit))
				      } catch {
				        case e:Throwable => Ok(view(form.fill(value), lint, e.getMessage))
				      }
				  ) else Ok(view(form.fill(IfIPForm("0.0.0.0", 0)), lint))
			  }
			  case _ => Redirect(routes.NetworkInterfaces.list(deviceName))
			}
		}
	}
	
	def deleteSecondaryIPPost(deviceName:String, intName:String, unit:Int) = Action.async { implicit req => 
	    val redirect = Redirect(routes.NetworkInterfaces.editSecondaryIP(deviceName, intName, unit))
		withDevice(deviceName) { implicit device => 
			device.config.findLogicalInterface(intName, unit) match {
			  case models.LogicalInetInterface(lint) => ipForm.bindFromRequest.fold(
			      error => redirect,
			      value => {
			    	 lint.removeSecondaryAddressAndMask(value.toIPv4AddressAndMask(false))
			    	 device.config(lint.getSecondaryIPConfigure())
			    	 redirect
			      } 
			      )
			  case _ => redirect
			}
		}
	}
}