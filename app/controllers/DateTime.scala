package controllers

import play.api.mvc.{MessagesControllerComponents, MessagesAbstractController}
import play.api.data.Form
import play.api.data.Forms._
import play.filters.csrf.{CSRFAddToken, CSRFCheck}
import scala.collection.JavaConverters._
import models.Forms._
import to.lef.srxlib
import javax.inject.Inject
import scala.concurrent.ExecutionContext

class DateTime @Inject()(val deviceDAO:models.DeviceDAO, addToken:CSRFAddToken, checkToken:CSRFCheck, mcc:MessagesControllerComponents)(implicit ec:ExecutionContext) extends MessagesAbstractController(mcc) with models.DeviceSession {
	def dateTimeForm = Form(
	    mapping(
	        "timeZone" -> nonEmptyText,
	        "ntpSourceAddress" -> optional(text).verifying("Invalid source address", v => !v.isDefined || srxlib.Util.isValidIPAddress(v.get)),
	        "ntp1" -> mapping(
	            "server" -> optional(text),
	            "keyId" -> optional(number),
	            "keyValue" -> optional(text)
	         )
	         (NtpForm.apply)
	         (NtpForm.unapply),
		     "ntp2" -> mapping(
	            "server" -> optional(text),
	            "keyId" -> optional(number),
	            "keyValue" -> optional(text)
	         )
	         (NtpForm.apply)
	         (NtpForm.unapply),
		     "ntp3" -> mapping(
	            "server" -> optional(text),
	            "keyId" -> optional(number),
	            "keyValue" -> optional(text)
	         )
	         (NtpForm.apply)
	         (NtpForm.unapply)
	    )
	    (DateTimeForm.apply)
	    (DateTimeForm.unapply)
	)
	
	def edit(deviceName:String) = addToken {
		editAction(deviceName, false)
	}
	
	def editPost(deviceName:String) = checkToken {
		editAction(deviceName, true)
	}
	
	def editAction(deviceName:String, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		    import views.html.datetime.{edit => view}
			val config = device.config
			val form = dateTimeForm
			
			if (isPost) dateTimeForm.bindFromRequest.fold(
			    error => Ok(view(error)),
			    value => try {
			    	device.config(value.toDateTime.getEditConfigure)
			    	Redirect(routes.DateTime.edit(device.name))
			    } catch {
			      case e:Throwable => Ok(view(form.fill(value)))
			    }
			) else Ok(view(form.fill(DateTimeForm(config.findDateTime))))
		}
	}
}