package controllers

import scala.collection.JavaConverters._

import play.api.mvc.{ControllerComponents, AbstractController}
import javax.inject.Inject
import scala.concurrent.ExecutionContext

class Modal @Inject()(val deviceDAO:models.DeviceDAO, components:ControllerComponents)(implicit ec:ExecutionContext) extends AbstractController(components) with models.DeviceSession{
  def sourceSelector(deviceName:String, zoneName:String) = addressSelector(deviceName, zoneName, true)
  def destinationSelector(deviceName:String, zoneName:String) = addressSelector(deviceName, zoneName, false)
  
	private def addressSelector(deviceName:String, zoneName:String, isSrc:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  val config = device.config
			val addrs = config.addressList(zoneName).asScala.toList  ++ config.addressList("global").asScala.toList
			val addrSets = config.addressSetList(zoneName).asScala.toList ++ config.addressSetList("global").asScala.toList
			Ok(views.html.modal.addressSelector(addrs, addrSets, zoneName, isSrc))
		}
	}
  
  def applicationSelector(deviceName:String) = Action.async { implicit req => 
    withDevice(deviceName) { implicit device => 
      val config = device.config
      val defaults = device.defaults
      
      val apps = device.allApplicationNameList.asScala.toList
      Ok(views.html.modal.applicationSelector(apps))
    }
  }
}