package controllers

import play.api.mvc.{MessagesControllerComponents, MessagesAbstractController}

import play.api.data.{Form, Forms}
import play.api.data.Forms._

import play.filters.csrf.{CSRFAddToken, CSRFCheck}

import scala.collection.JavaConverters._

import to.lef.srxlib
import models.Forms._

import play.api.i18n._
import javax.inject.Inject
import scala.concurrent.ExecutionContext

class AdminUser @Inject()(val deviceDAO:models.DeviceDAO, addToken:CSRFAddToken, checkToken:CSRFCheck, mcc:MessagesControllerComponents)(implicit ec:ExecutionContext) extends MessagesAbstractController(mcc) with models.DeviceSession {
	def userList(deviceName:String) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	    	val userList = device.config.loginUserList.asScala.toList
	    	Ok(views.html.admin.user.list(userList))
	  }
	}
	
	def createUser(deviceName:String) = addToken {
	  createUserAction(deviceName, false)
	}
	
	def createUserPost(deviceName:String) = checkToken {
	  createUserAction(deviceName, true)
	}

	def userForm(u:Option[srxlib.login.LoginUser]) = Form(
			mapping(
			    "name" -> u.filter(_.isRoot).map(v => ignored(v.getName)).getOrElse(cw4s.Validations.idText),
			    "password" -> tuple(
			        "password" -> u.map(v => text).getOrElse(nonEmptyText),
			        "confirm" -> u.map(v => text).getOrElse(nonEmptyText)
			        ).verifying("Your new password entries do not match.", v => v._1.isEmpty || v._1 == v._2),
			    "class_name" -> nonEmptyText
			    )
			    (LoginUserForm.apply)
			    (LoginUserForm.unapply)
	    )
	    
	def createUserAction(deviceName:String, isPost:Boolean) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	    import views.html.admin.user.{create => view}
	    val config = device.config
	    val form = userForm(None)
	    
	    if (isPost) form.bindFromRequest.fold(
	        error => Ok(view(error)),
	        value => try {
	        	device.config(value.toLoginUser.getCreateConfigure)
	        	Redirect(routes.AdminUser.userList(deviceName))
	        } catch {
	          case e:Throwable => Ok(view(form.fill(value), e.getMessage))
	        }
        ) else Ok(view(form.fill(LoginUserForm("", ("", ""), ""))))
	  }
	}
	
	def editUser(deviceName:String, name:String) = addToken {
	  editUserAction(deviceName, name, false)
	}
	
	def editUserPost(deviceName:String, name:String) = checkToken {
	  editUserAction(deviceName, name, true)
	}
	
	def editUserAction(deviceName:String, name:String, isPost:Boolean)  = Action.async { implicit req =>
	    import views.html.admin.user.{edit => view}
		withDevice(deviceName) { implicit device =>
		  val config = device.config
		  
		  config.findLoginUser(name) match {
		    case models.LoginUser(v) => {
		    	val form = userForm(Some(v))
		      if (isPost) form.bindFromRequest.fold(
		        error => Ok(view(error, v)),
		        value => try {
		        	device.config(value.toLoginUser.getEditConfigure(v))
		        	Redirect(routes.AdminUser.userList(deviceName))
		        } catch {
		          case e:Throwable => Ok(view(form.fill(value), v, e.getMessage))
		        }
		      ) else Ok(view(form.fill(LoginUserForm(v)), v))
		    }
		    case _ => Redirect(routes.AdminUser.userList(deviceName))
		  }
		}
	}
	
	def deleteUserPost(deviceName:String, name:String) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
	    	device.config.findLoginUser(name) match {
	    	  case models.LoginUser(user) => {
	    	    device.config(user.getDeleteConfigure)
	    	  }
	    	  case _ =>
	    	}
        	Redirect(routes.AdminUser.userList(deviceName))
	  }
	}
	
	def sshDsa(deviceName:String, name:String) = addToken {
		sshDsaAction(deviceName, name, false)
	}
	
	def sshDsaPost(deviceName:String, name:String) = checkToken {
		sshDsaAction(deviceName, name, true)
	}
	
	def sshDsaAction(deviceName:String, name:String, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  import views.html.admin.user.{dsa => view}
		  val config = device.config
		  val form = Form(single("key" -> nonEmptyText))
		  
		  config.findLoginUser(name) match {
		    case models.LoginUser(user) =>  if (isPost) form.bindFromRequest.fold(
		        error => Ok(view(error, user)),
		        value => try {
		        	user.addSshDsaKey(value)
		        	device.config(user.getSshDsaConfigure)
		        	
		        	Redirect(routes.AdminUser.sshDsa(deviceName, name))
		        } catch {
		          case e:Throwable => Ok(view(form.fill(value), user, e.getMessage))
		        }
		    ) else Ok(view(form.fill(""), user))
		    case _ => Redirect(routes.AdminUser.userList(deviceName)) 
		  }
		}
	}
	
	def deleteSshDsaKeyPost(deviceName:String, name:String, idx:Int) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
		  val config = device.config
		  val form = Form(single("key" -> nonEmptyText))
		  
		  config.findLoginUser(name) match {
		    case models.LoginUser(user) =>  try {
		        	user.removeSshDsaKey(idx)
		        	device.config(user.getSshDsaConfigure)
		        } catch {
		          case e:Throwable => 
		        }
		    case _ => 
		  }    
		  Redirect(routes.AdminUser.sshDsa(deviceName, name)) 
	  }
	}
	
	def sshRsa(deviceName:String, name:String) = addToken {
		sshRsaAction(deviceName, name, false)
	}
	
	def sshRsaPost(deviceName:String, name:String) = checkToken {
		sshRsaAction(deviceName, name, true)
	}
	
	def sshRsaAction(deviceName:String, name:String, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  import views.html.admin.user.{rsa => view}
		  val config = device.config
		  val form = Form(single("key" -> nonEmptyText))
		  
		  config.findLoginUser(name) match {
		    case models.LoginUser(user) =>  if (isPost) form.bindFromRequest.fold(
		        error => Ok(view(error, user)),
		        value => try {
		        	user.addSshRsaKey(value)
		        	device.config(user.getSshRsaConfigure)
		        	
		        	Redirect(routes.AdminUser.sshRsa(deviceName, name))
		        } catch {
		          case e:Throwable => Ok(view(form.fill(value), user, e.getMessage))
		        }
		    ) else Ok(view(form.fill(""), user))
		    case _ => Redirect(routes.AdminUser.userList(deviceName)) 
		  }
		}
	}
	
	def deleteSshRsaKeyPost(deviceName:String, name:String, idx:Int) = Action.async { implicit req =>
	  withDevice(deviceName) { implicit device =>
		  val config = device.config
		  val form = Form(single("key" -> nonEmptyText))
		  
		  config.findLoginUser(name) match {
		    case models.LoginUser(user) =>  try {
		        	user.removeSshRsaKey(idx)
		        	device.config(user.getSshRsaConfigure)
		        } catch {
		          case e:Throwable => 
		        }
		    case _ => 
		  }    
		  Redirect(routes.AdminUser.sshRsa(deviceName, name)) 
	  }
	}
}