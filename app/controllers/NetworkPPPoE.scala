package controllers

import play.api.Logging
import play.api.mvc.{MessagesControllerComponents, MessagesAbstractController}

import play.api.data.Form
import play.api.data.Forms._

import play.filters.csrf.{CSRFAddToken, CSRFCheck}

import scala.collection.JavaConverters._

import to.lef.srxlib
import models.Forms._

import javax.inject.Inject
import scala.concurrent.ExecutionContext

class NetworkPPPoE @Inject()(val deviceDAO:models.DeviceDAO, addToken:CSRFAddToken, checkToken:CSRFCheck, mcc:MessagesControllerComponents)(implicit ec:ExecutionContext) extends MessagesAbstractController(mcc) with Logging with models.DeviceSession{
	def pppoeList(deviceName:String) = Action.async {
		withDevice(deviceName) { implicit device =>
			Ok(views.html.network.pppoe.list(device))
		}
	}
	
	def pppoeForm(current:Option[srxlib.PPPoE], config:srxlib.DeviceConfig) = Form(
	    mapping(
	    	"unitId" -> { if (current.isDefined) ignored(current.get.getUnitId) else number.verifying("Instance is already exist.", v => !config.isLogicalInterfaceExist("pp0", v)) },
	    	"boundInterface" -> nonEmptyText,
	    	"username" -> nonEmptyText,
	    	"password" -> { if (current.isDefined) text else nonEmptyText },
	    	"auth" -> number(min = 1, max = 3),
	    	"ac" -> text,
	    	"service" -> text,
	    	"create_default_route" -> boolean,
	    	"default_route_metric" -> number(min = 0, max = 65535),
	    	"disconnect" -> number,
	    	"autoConnect" -> number,
	    	"idleDisconnect" -> number,
	    	"propagateToDhcp" -> boolean
	    )
	    (PPPoEForm.apply)
	    (PPPoEForm.unapply)
	)
	
	def create(deviceName:String) = addToken {
		createAction(deviceName, false)
	}
	
	def createPost(deviceName:String) = checkToken {
		createAction(deviceName, true)
	}
	
	def createAction(deviceName:String, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
			val config = device.config
			val form = pppoeForm(None, config)
			import views.html.network.pppoe.{create => view}
			
			if (isPost) form.bindFromRequest.fold(
			    error => Ok(view(error)),
			    value => try {
			    	val pppoe = value.toPPPoEInterface
			        val confList = new srxlib.DeviceConfigures(pppoe.getCreateConfigure)
			        
			        if (value.createDefaultRoute) {
			        	val rt = srxlib.route.StaticRoute.apply("0.0.0.0", 0, null, pppoe.getName, null, 
			        	    if (value.defaultRouteMetric > 0) value.defaultRouteMetric else null,
			        	    null
			        	    )
			        	confList.add(rt.getCreateConfigure)
			        }
			    	device.config(confList)
			    	Redirect(routes.NetworkPPPoE.pppoeList(deviceName))
			    }
			    catch {
			      case e:Throwable => {
//			        logger.debug(e.getCause.getStackTraceString)
			        Ok(view(form.fill(value), e.getMessage))
			      }
			    }
			)
			else Ok(view(form.fill(PPPoEForm(0, "", "", "", 1, "", "", true, 1, 0, 0, 0, false))))
		}
	}

	def edit(deviceName:String, unitId:Int) = addToken {
		editAction(deviceName, unitId, false)
	}
	
	def editPost(deviceName:String, unitId:Int) = checkToken {
		editAction(deviceName, unitId, true)
	}

	def editAction(deviceName:String, unitId:Int, isPost:Boolean) = Action.async { implicit req =>
		withDevice(deviceName) { implicit device =>
		  val config = device.config
		  config.findPPPoE(unitId) match {
		    case models.PPPoE(v) => {
		    	import views.html.network.pppoe.{edit => view}
		    	val form = pppoeForm(Some(v), config)
		    	val routeList = v.findDefaultRouteList(config).asScala.toList
		    	
		    	if (isPost) form.bindFromRequest.fold (
		    	    error => Ok(view(error, v)),
		    	    value => try {
		    	      val pppoe = value.toPPPoEInterface
		    	      val configList = new srxlib.DeviceConfigures(pppoe.getEditConfigure(v))
		    	      if (value.createDefaultRoute) {
		    	        if (routeList.size > 0) {
		    	        	routeList.foreach({rt => 
		    	        	  rt.setMetric(if (value.defaultRouteMetric > 0) value.defaultRouteMetric else null)
		    	        	  configList.add(rt.getEditConfigure)
		    	        	})
		    	        } else {
				        	val rt = srxlib.route.StaticRoute.apply("0.0.0.0", 0, null, pppoe.getName, null, 
			        	    if (value.defaultRouteMetric > 0) value.defaultRouteMetric else null,
			        	    null
			        	    )
			        	    configList.add(rt.getCreateConfigure)
		    	        }
		    	      } else {
		    	    	  routeList.foreach(rt => configList.add(rt.getDeleteConfigure))
		    	      }
		    	      device.config(configList)
		    	    	Redirect(routes.NetworkPPPoE.pppoeList(deviceName))
		    	    }
		    	    catch {
		    	      case e:Throwable => Ok(view(form.fill(value), v, e.getMessage))
		    	    }
		    	)
		    	else Ok(view(form.fill(PPPoEForm(v, routeList.headOption)), v))
		    }
		    case _ => Redirect(routes.NetworkPPPoE.pppoeList(deviceName))
		  }

		}
	}
	
	def deletePost(deviceName:String, unit:Int) = Action.async { implicit req => 
		  withDevice(deviceName) { implicit device => 
		    val config = device.config
		    config.findLogicalInterface("pp0", unit) match {
		      case models.LogicalInterface(v) if (v.isRemovable(config)) => {
		    	  try {
		    		  device.config(v.getDeleteConfigure())
		    	  } catch {
		    	    case e:Throwable  => logger.error(e.getMessage)
		    	  }
		    	  
		      }
		      case _ => 
		    }
    		Redirect(routes.NetworkPPPoE.pppoeList(device.name))
		  }
	}
}