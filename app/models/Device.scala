package models

import scala.collection.JavaConverters._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import slick.jdbc.JdbcProfile
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfigProvider

import javax.inject.Inject

import to.lef.srxlib
import models.Forms._

trait DeviceTable {
  protected val driver:JdbcProfile
  import driver.api._

  class Devices(tag:Tag) extends Table[DeviceRecord](tag, "devices") {
	    def id         = column[Int]("id", O.PrimaryKey, O.AutoInc)
			def name       = column[String]("name", O SqlType "varchar(255)")
			def hostname   = column[String]("hostname", O SqlType "varchar(255)")
			def port       = column[Int]("port")
			def username   = column[String]("username", O SqlType "varchar(255)")
			def password   = column[String]("password", O SqlType "varchar(255)")
			def persistent = column[Boolean]("persistent", O Default false)
			def createdAt  = column[Long]("createdAt")
			def updatedAt  = column[Long]("updatedAt")

			def idxName = index("idx_name", name, unique = true)
			def * = (id.?, name, hostname, port, username, password, persistent, createdAt, updatedAt) <> (DeviceRecord.tupled, DeviceRecord.unapply _)
  }
}

case class DeviceRecord (id:Option[Int], name:String, hostname:String, port:Int, username:String, password:String, persistent:Boolean, createdAt:Long, updatedAt:Long)
case class DeviceNotFoundException(message:String) extends RuntimeException(message)

class DeviceDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) extends DeviceTable with HasDatabaseConfigProvider[JdbcProfile] {
  import profile.api._
  protected val devices = TableQuery[Devices]
  
  def list:Future[List[DeviceRecord]] = db.run(devices.result).map(res => res.toList)

  def findByName(name:String):Future[Option[DeviceRecord]] = db.run(devices.filter(_.name === name.bind).result.headOption)
 
  def findByNameSync[R](name:String, f:Option[DeviceRecord] => R):R = f(Await.result(findByName(name), Duration.Inf))
 
  def findById(id:Int):Future[Option[DeviceRecord]] = db.run(devices.filter(_.id === id.bind).result.headOption)
 
  def getFormById(id:Int):Future[Option[DeviceForm]] = findById(id).map(x => x.map(d => new DeviceForm(d.name, d.hostname, d.port, d.username, d.password, d.persistent)))

  def regist(v:DeviceForm):Future[Int] = {
		val t = new java.util.Date().getTime()
		db.run((devices returning devices.map(_.id)) += new DeviceRecord(None, v.name, v.hostname, v.port, v.username, v.password, v.persistent, t, t))
  }
 
  def edit(id:Int, v:DeviceForm):Future[Int] = {
    val q = for {d <- devices if d.id === id.bind} yield (d.name, d.hostname, d.port, d.username, d.password, d.persistent, d.updatedAt)
	  val t = new java.util.Date().getTime()
	  for {
      _ <- findById(id).map {
        case Some(d) => Device.clear(d.name, d.hostname, false)
        case _ => ()
      }
      cnt <- db.run(q.update(v.name, v.hostname, v.port, v.username, v.password, v.persistent, t))
    } yield cnt
  }
  
  def editExceptPassword(id:Int, v:DeviceForm):Future[Int] = {
    val q = for {d <- devices if d.id === id.bind} yield (d.name, d.hostname, d.port, d.username, d.persistent, d.updatedAt)
	  val t = new java.util.Date().getTime()
	  for {
      _ <- findById(id).map {
        case Some(d) => Device.clear(d.name, d.hostname, false)
        case _ => ()
      }
      cnt <- db.run(q.update(v.name, v.hostname, v.port, v.username, v.persistent, t))
    } yield cnt
  }
  
  def delete(id:Int):Future[Int] = {
    for {
      _ <- findById(id).map(_.map(d => Device.clear(d.name, d.hostname, true)))
	    cnt <- db.run(devices.filter(_.id === id.bind).delete)
    } yield cnt
  }
}

object Device {
	def factory = srxlib.DeviceFactory.getInstance()
	
	def apply(name:String, hostname:String, username:String, password:String, persistent:Boolean, port:Int):srxlib.Device = factory(name, hostname, username, password, persistent, port)
	def unapply(a: Any): Option[srxlib.Device] = if (a.isInstanceOf[srxlib.Device]) Some(a.asInstanceOf[srxlib.Device]) else None
	def clear(name:String) = factory.clear(name)
	def clear(name:String, address:String, isRemoved:Boolean) = factory.clear(name, address)
}

trait DeviceSession {
  val deviceDAO:  DeviceDAO
  
  def withDevice[A](name:String)(block: srxlib.Device => A)(implicit ec:ExecutionContext):Future[A] = deviceDAO.findByName(name).map(_ match {
    case Some(d) => block(Device(d.name, d.hostname, d.username, d.password, d.persistent, d.port))
		case _ => throw new DeviceNotFoundException(s"$name is not found.")
    })
}

object SecurityZone {
	def apply(name:String) = new srxlib.zone.SecurityZone(name)
	def unapply(a: Any): Option[srxlib.zone.SecurityZone] = if (a.isInstanceOf[srxlib.zone.SecurityZone]) Some(a.asInstanceOf[srxlib.zone.SecurityZone]) else None
}

object LogicalInterface {
	def unapply(a: Any): Option[srxlib.nic.LogicalInterface] = if (a.isInstanceOf[srxlib.nic.LogicalInterface])  Some(a.asInstanceOf[srxlib.nic.LogicalInterface]) else None 
}

object LogicalInetInterface {
	def unapply(a: Any): Option[srxlib.nic.LogicalInetInterface] = if (a.isInstanceOf[srxlib.nic.LogicalInetInterface]) Some(a.asInstanceOf[srxlib.nic.LogicalInetInterface]) else None
}

object Dip {
	def apply(a:String, e:String, p:Boolean) = new srxlib.nat.Dip(a, e, p)
	def unapply(a: Any): Option[srxlib.nat.Dip] = if (a.isInstanceOf[srxlib.nat.Dip]) Some(a.asInstanceOf[srxlib.nat.Dip]) else None
}

object Mip {
	def apply(a:String, h:String, n:String, p:Boolean) = new srxlib.nat.Mip(a, srxlib.Util.string2Netmask(n), h, null, p)
	def unapply(a: Any): Option[srxlib.nat.Mip] = if (a.isInstanceOf[srxlib.nat.Mip]) Some(a.asInstanceOf[srxlib.nat.Mip]) else None
}

object Vip {
	def apply(va:String, vp:Int, p:String, mp:Int, ma:String, pa:Boolean) = new srxlib.nat.Vip(va, null, vp, ma, null, p, mp, pa)
	def unapply(a: Any): Option[srxlib.nat.Vip] = if (a.isInstanceOf[srxlib.nat.Vip]) Some(a.asInstanceOf[srxlib.nat.Vip]) else None
}

object LogicalBridgeInterface {
	def unapply(a: Any): Option[srxlib.nic.LogicalBridgeInterface] = if (a.isInstanceOf[srxlib.nic.LogicalBridgeInterface]) Some(a.asInstanceOf[srxlib.nic.LogicalBridgeInterface]) else None
}

object LogicalVlanInterface {
	def unapply(a: Any): Option[srxlib.nic.LogicalVlanInterface] = if (a.isInstanceOf[srxlib.nic.LogicalVlanInterface]) Some(a.asInstanceOf[srxlib.nic.LogicalVlanInterface]) else None
}

object LogicalBindInterface {
	def unapply(a: Any): Option[srxlib.nic.LogicalBindInterface] = if (a.isInstanceOf[srxlib.nic.LogicalBindInterface]) Some(a.asInstanceOf[srxlib.nic.LogicalBindInterface]) else None
}

object LogicalBoundInterface {
	def unapply(a: Any): Option[srxlib.nic.LogicalBoundInterface] = if (a.isInstanceOf[srxlib.nic.LogicalBoundInterface]) Some(a.asInstanceOf[srxlib.nic.LogicalBoundInterface]) else None
}

object BindableInterface {
	def unapply(a: Any): Option[srxlib.nic.BindableInterface] = if (a.isInstanceOf[srxlib.nic.BindableInterface]) Some(a.asInstanceOf[srxlib.nic.BindableInterface]) else None
}

object VlanInterface {
	def apply(unitId:Int, vlanId:Int) = new srxlib.nic.VlanInterface(unitId, vlanId)
	def unapply(a: Any): Option[srxlib.nic.VlanInterface] = if (a.isInstanceOf[srxlib.nic.VlanInterface]) Some(a.asInstanceOf[srxlib.nic.VlanInterface]) else None
}

object VPNInterface {
	def apply(unitId:Int) = new srxlib.nic.VPNInterface(unitId)
	def unapply(a: Any): Option[srxlib.nic.VPNInterface] = if (a.isInstanceOf[srxlib.nic.VPNInterface]) Some(a.asInstanceOf[srxlib.nic.VPNInterface]) else None
}

object IpOverIpInterface {
	def apply(phyName:String, unitId:Int) = new srxlib.nic.IpOverIpInterface(phyName, unitId)
	def unapply(a: Any): Option[srxlib.nic.IpOverIpInterface] = if (a.isInstanceOf[srxlib.nic.IpOverIpInterface]) Some(a.asInstanceOf[srxlib.nic.IpOverIpInterface]) else None
}

object GREInterface {
	def apply(phyName:String, unitId:Int) = new srxlib.nic.GREInterface(phyName, unitId)
	def unapply(a: Any): Option[srxlib.nic.GREInterface] = if (a.isInstanceOf[srxlib.nic.GREInterface]) Some(a.asInstanceOf[srxlib.nic.GREInterface]) else None
}

object RethInterface {
	def apply(pname:String, unitId:Int) = new srxlib.nic.RethInterface(pname, unitId)
	def unapply(a: Any): Option[srxlib.nic.RethInterface] = if (a.isInstanceOf[srxlib.nic.RethInterface]) Some(a.asInstanceOf[srxlib.nic.RethInterface]) else None
}

object IrbInterface {
	def apply(unitId:Int) = new srxlib.nic.IrbInterface(unitId)
	def unapply(a: Any): Option[srxlib.nic.IrbInterface] = if (a.isInstanceOf[srxlib.nic.IrbInterface]) Some(a.asInstanceOf[srxlib.nic.IrbInterface]) else None
}

object PortInetInterface {
    def apply(phyName:String, unitId:Int) = new srxlib.nic.PortInetInterface(phyName, unitId)
	def unapply(a: Any): Option[srxlib.nic.PortInetInterface] = if (a.isInstanceOf[srxlib.nic.PortInetInterface]) Some(a.asInstanceOf[srxlib.nic.PortInetInterface]) else None
}

object PortVlanInterface {
    def apply(phyName:String, unitId:Int) = new srxlib.nic.PortVlanInterface(phyName, unitId)
	def unapply(a: Any): Option[srxlib.nic.PortVlanInterface] = if (a.isInstanceOf[srxlib.nic.PortVlanInterface]) Some(a.asInstanceOf[srxlib.nic.PortVlanInterface]) else None
}

object PortBridgeInterface {
    def apply(phyName:String, unitId:Int) = new srxlib.nic.PortBridgeInterface(phyName, unitId)
	def unapply(a: Any): Option[srxlib.nic.PortBridgeInterface] = if (a.isInstanceOf[srxlib.nic.PortBridgeInterface]) Some(a.asInstanceOf[srxlib.nic.PortBridgeInterface]) else None
}

object PPPoEInetInterface {
	def unapply(a: Any): Option[srxlib.nic.PPPoEInetInterface] = if (a.isInstanceOf[srxlib.nic.PPPoEInetInterface]) Some(a.asInstanceOf[srxlib.nic.PPPoEInetInterface]) else None
}

object PPPoE {
	def apply(unitId:Int) = new srxlib.PPPoE(unitId)
	def unapply(a: Any): Option[srxlib.PPPoE] = if (a.isInstanceOf[srxlib.PPPoE]) Some(a.asInstanceOf[srxlib.PPPoE]) else None
}

object AddressElement {
   def apply(zoneName:String, name:String, addr:String, mask:Int) = new srxlib.policy.AddressElement(zoneName, name, addr, mask) 
   def apply(zoneName:String, name:String, addr:String) = new srxlib.policy.AddressElement(zoneName, name, addr)
   def unapply(a: Any): Option[srxlib.policy.AddressElement] = if (a.isInstanceOf[srxlib.policy.AddressElement]) Some(a.asInstanceOf[srxlib.policy.AddressElement]) else None
}

object AddressSet {
   def apply(zoneName:String, name:String, addrs:List[String], addrSets:List[String]) = new srxlib.policy.AddressSet(zoneName, name, addrs.asJava, addrSets.asJava) 
   def unapply(a: Any): Option[srxlib.policy.AddressSet] = if (a.isInstanceOf[srxlib.policy.AddressSet]) Some(a.asInstanceOf[srxlib.policy.AddressSet]) else None
}

object ApplicationSet {
	def apply(name:String, apps:List[String], appSets:List[String]) = new srxlib.policy.ApplicationSet(name, apps.asJava, appSets.asJava)
	def unapply(a: Any): Option[srxlib.policy.ApplicationSet] = if (a.isInstanceOf[srxlib.policy.ApplicationSet]) Some(a.asInstanceOf[srxlib.policy.ApplicationSet]) else None
}

object ApplicationElement {
  def apply(name:String) = new srxlib.policy.ApplicationElement(name)
  def unapply(a: Any): Option[srxlib.policy.ApplicationElement] = if (a.isInstanceOf[srxlib.policy.ApplicationElement]) Some(a.asInstanceOf[srxlib.policy.ApplicationElement]) else None
}

object ApplicationTerm {
  def apply(name:String) = new srxlib.policy.ApplicationTerm(name)
  def unapply(a: Any): Option[srxlib.policy.ApplicationTerm] = if (a.isInstanceOf[srxlib.policy.ApplicationTerm]) Some(a.asInstanceOf[srxlib.policy.ApplicationTerm]) else None
}

object Policy {
  def unapply(a: Any): Option[srxlib.policy.Policy] = if (a.isInstanceOf[srxlib.policy.Policy]) Some(a.asInstanceOf[srxlib.policy.Policy]) else None
}

object InboundProtocol {
  def apply(s:IfInboundProtocolForm) = new srxlib.zone.InboundProtocol(
		  s.bfd, s.bgp, s.dvmrp, s.igmp, s.ldp, s.msdp, s.nhrp, s.ospf, s.ospf3,
		  s.pgm, s.pim, s.rip, s.ripng, s.routerDiscovery, s.rsvp, s.sap, s.vrrp
      )
  def unapply(a: Any): Option[srxlib.zone.InboundProtocol] = if (a.isInstanceOf[srxlib.zone.InboundProtocol]) Some(a.asInstanceOf[srxlib.zone.InboundProtocol]) else None
}

object InboundService {
  def apply(s1:IfInboundService1Form, s2:IfInboundService2Form) = new srxlib.zone.InboundService(
		  s1.bootp, s1.dhcp, s1.dhcpv6, s1.dns, s1.finger, s1.ftp, s1.http, s1.https, s1.identReset, s1.ike,
		  s1.lsping, s1.netconf, s1.ntp, s1.ping, s1.r2cp, s1.reverseSsh, s1.reverseTelnet, s1.rlogin, 
		  s2.rpm, s2.rsh, s2.sip, s2.snmp, s2.snmpTrap, s2.ssh, s2.telnet, s2.tftp, s2.traceroute,
		  s2.xnmClearText, s2.xnmSsl
      )
  def unapply(a: Any): Option[srxlib.zone.InboundService] = if (a.isInstanceOf[srxlib.zone.InboundService]) Some(a.asInstanceOf[srxlib.zone.InboundService]) else None
}

object DhcpNone {
	def unapply(a: Any): Option[srxlib.dhcp.DhcpNone] = if (a.isInstanceOf[srxlib.dhcp.DhcpNone]) Some(a.asInstanceOf[srxlib.dhcp.DhcpNone]) else None
}

object DhcpRelayAgent {
	def unapply(a: Any): Option[srxlib.dhcp.DhcpRelayAgent] = if (a.isInstanceOf[srxlib.dhcp.DhcpRelayAgent]) Some(a.asInstanceOf[srxlib.dhcp.DhcpRelayAgent]) else None
}

object DhcpServer {
	def unapply(a: Any): Option[srxlib.dhcp.DhcpServer] = if (a.isInstanceOf[srxlib.dhcp.DhcpServer]) Some(a.asInstanceOf[srxlib.dhcp.DhcpServer]) else None
}

object DhcpService {
	def unapply(a: Any): Option[srxlib.dhcp.DhcpService] = if (a.isInstanceOf[srxlib.dhcp.DhcpService]) Some(a.asInstanceOf[srxlib.dhcp.DhcpService]) else None
}

object Snmp {
	def apply(name:String, contact:String, location:String, description:String) = new srxlib.snmp.Snmp(name, contact, location, description)
	def unapply(a: Any): Option[srxlib.snmp.Snmp] = if (a.isInstanceOf[srxlib.snmp.Snmp]) Some(a.asInstanceOf[srxlib.snmp.Snmp]) else None
}

object SnmpCommunity {
    def apply(name:String, writeEnabled:Boolean, trapEnabled:Boolean, trapVersion:String) = new srxlib.snmp.SnmpCommunity(name, writeEnabled, trapEnabled, trapVersion)
	def unapply(a: Any): Option[srxlib.snmp.SnmpCommunity] = if (a.isInstanceOf[srxlib.snmp.SnmpCommunity]) Some(a.asInstanceOf[srxlib.snmp.SnmpCommunity]) else None
}

object IkeProposal {
	def apply(name:String, authMethod:String, authAlg:String, dhGroup:String, encAlg:String, lifeTime:Int) = new srxlib.vpn.IkeProposal(name, authMethod, authAlg, dhGroup, encAlg, lifeTime)
	def unapply(a: Any): Option[srxlib.vpn.IkeProposal] = if (a.isInstanceOf[srxlib.vpn.IkeProposal]) Some(a.asInstanceOf[srxlib.vpn.IkeProposal]) else None
}

object IkePolicy {
	def apply(name:String, mode:String, key:String, propSet:String) = new srxlib.vpn.IkePolicy(name, mode, key, propSet)
	def unapply(a: Any): Option[srxlib.vpn.IkePolicy] = if (a.isInstanceOf[srxlib.vpn.IkePolicy]) Some(a.asInstanceOf[srxlib.vpn.IkePolicy]) else None
}

object IkeGateway {
    import srxlib.vpn.IkeGateway.{PEER_TYPE_STATIC,PEER_TYPE_DYNAMIC_HOSTNAME}
	def apply(name:String, version:String, peerType:String, peer:String, outgoingInterface:String, localId:String, policy:String, natTraversalEnabled:Boolean, natKeepAlive:Int, dpdInterval:Int, dpdThreshold:Int, dpdAlwaysSend:Boolean) = new srxlib.vpn.IkeGateway(name, version, peerType, peer, outgoingInterface, localId, policy, natTraversalEnabled, natKeepAlive, dpdInterval, dpdThreshold, dpdAlwaysSend)
	def unapply(a: Any): Option[srxlib.vpn.IkeGateway] = if (a.isInstanceOf[srxlib.vpn.IkeGateway]) Some(a.asInstanceOf[srxlib.vpn.IkeGateway]) else None
	def isEditable(gw:srxlib.vpn.IkeGateway, config:srxlib.DeviceConfig) = if(Option(gw.getPolicy(config)).map(v => Option(v.getPresharedKey).getOrElse("")).getOrElse("").length > 0) gw.getPeerType match {
		case PEER_TYPE_STATIC | PEER_TYPE_DYNAMIC_HOSTNAME => true
		case _ => false
	} else false
}

object IPSecProposal {
	def apply(name:String, protocol:String, authAlg:String, encAlg:String, lifeTime:Int, lifeSize:Long) = new srxlib.vpn.IPSecProposal(name, protocol, authAlg, encAlg, lifeTime, lifeSize)
	def unapply(a: Any): Option[srxlib.vpn.IPSecProposal] = if (a.isInstanceOf[srxlib.vpn.IPSecProposal]) Some(a.asInstanceOf[srxlib.vpn.IPSecProposal]) else None
}

object IPSecPolicy {
	def apply(name:String, pfs:String, propSet:String) = new srxlib.vpn.IPSecPolicy(name, pfs, propSet)
	def unapply(a: Any): Option[srxlib.vpn.IPSecPolicy] = if (a.isInstanceOf[srxlib.vpn.IPSecPolicy]) Some(a.asInstanceOf[srxlib.vpn.IPSecPolicy]) else None
}

object IPSecVpn {
	def apply(name:String, gwName:String, policyName:String, bindIntName:String, noAntiReplay:Boolean, establishImmediately:Boolean, monEnabled:Boolean, monDest:String, monOptimized:Boolean, monSrcIntName:String) = new srxlib.vpn.IPSecVpn(name, gwName, policyName, bindIntName, noAntiReplay, establishImmediately, monEnabled, monDest, monOptimized, monSrcIntName, null)
	def unapply(a: Any): Option[srxlib.vpn.IPSecVpn] = if (a.isInstanceOf[srxlib.vpn.IPSecVpn]) Some(a.asInstanceOf[srxlib.vpn.IPSecVpn]) else None
}

object IDSOption {
	def apply(name:String) = new srxlib.zone.IDSOption(name)
	def unapply(a: Any): Option[srxlib.zone.IDSOption] = if (a.isInstanceOf[srxlib.zone.IDSOption]) Some(a.asInstanceOf[srxlib.zone.IDSOption]) else None
}

object LoginUser {
	def apply(name:String, password:String, className:String) = new srxlib.login.LoginUser(name, password, className)
	def unapply(a: Any): Option[srxlib.login.LoginUser] = if (a.isInstanceOf[srxlib.login.LoginUser]) Some(a.asInstanceOf[srxlib.login.LoginUser]) else None
}

object Syslog {
	def unapply(a: Any): Option[srxlib.syslog.Syslog] = if (a.isInstanceOf[srxlib.syslog.Syslog]) Some(a.asInstanceOf[srxlib.syslog.Syslog]) else None
}

object SyslogFile {
	def apply(name:String, facility:String, level:String, structuredData:Boolean, trafficLog:Boolean) = new srxlib.syslog.SyslogFile(name, facility, level, structuredData, trafficLog)
	def unapply(a: Any): Option[srxlib.syslog.SyslogFile] = if (a.isInstanceOf[srxlib.syslog.SyslogFile]) Some(a.asInstanceOf[srxlib.syslog.SyslogFile]) else None
}

object SyslogHost {
	def apply(name:String, facility:String, level:String, port:Integer, sourceAddress:String, trafficLog:Boolean) = new srxlib.syslog.SyslogHost(name, facility, level, port, sourceAddress, trafficLog)
	def unapply(a: Any): Option[srxlib.syslog.SyslogHost] = if (a.isInstanceOf[srxlib.syslog.SyslogHost]) Some(a.asInstanceOf[srxlib.syslog.SyslogHost]) else None
}