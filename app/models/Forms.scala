package models

import scala.collection.JavaConverters._
import to.lef.srxlib
import to.lef.srxlib._

object Forms {
	case class DeviceForm (name:String, hostname:String, port:Int, username:String, password:String, persistent:Boolean)
	case class ZoneForm (name:String, tcpRst:Boolean) {
	  def toSecurityZone = new zone.SecurityZone(name, tcpRst)
	}
	object ZoneForm {
		def apply(s:srxlib.zone.SecurityZone) = new ZoneForm(s.getName(), s.isTcpReset())
	}
	
	case class IfNameForm(phyName:String, unitId:Int)
	case class IfIPForm(address:String, mask:Int) {
	  def toIPv4AddressAndMask(prefer:Boolean) = new srxlib.nic.LogicalInetInterface.IPv4AddressAndMask(address, mask, prefer)
	}
	
	case class IfInetFamilyForm (ipType:Int, pppoeInstance:Option[String], address:Option[String], mask:Option[Int]) {
		def this(iptype:Int) = this(iptype, None, None, None)
		def this(addr:String, mask:Int) = this(srxlib.nic.LogicalInetInterface.INET4_TYPE_STATIC, None, Some(addr), Some(mask))
		def this(pppoeInstance:String) = this(srxlib.nic.LogicalInetInterface.INET4_TYPE_NEGOTIATE, Some(pppoeInstance), None, None)
	}
	
	object IfInetFamilyForm {
		def apply(iptype:Int) = new IfInetFamilyForm(iptype)
		def apply(addr:String, mask:Int) = new IfInetFamilyForm(addr, mask)
		def apply(ipType:Int, pppoeInstance:Option[String], addr:srxlib.nic.LogicalInetInterface.IPv4AddressAndMask) = new IfInetFamilyForm(
		    ipType, pppoeInstance,
		    if (addr == null) None else Some(addr.getAddress),
		    if (addr == null) None else Some(addr.getNetmask)
		    )
	}
	
	case class IfInboundProtocolForm (
			bfd:Boolean,
			bgp:Boolean,
			dvmrp:Boolean,
			igmp:Boolean,
			ldp:Boolean,
			msdp:Boolean,
			nhrp:Boolean,
			ospf:Boolean,
			ospf3:Boolean,
			pgm:Boolean,
			pim:Boolean,
			rip:Boolean,
			ripng:Boolean,
			routerDiscovery:Boolean,
			rsvp:Boolean,
			sap:Boolean,
			vrrp:Boolean
	    )

	 object IfInboundProtocolForm {
	  def apply() = new IfInboundProtocolForm (
	      false, false, false, false, false,
	      false, false, false, false, false,
	      false, false, false, false, false,
	      false, false
	  )
	  def apply(s:srxlib.zone.InboundProtocol) = new IfInboundProtocolForm(
	      s.isBfdEnabled,
	      s.isBgpEnabled,
	      s.isDvmrpEnabled,
	      s.isIgmpEnabled,
	      s.isLdpEnabled,
	      s.isMsdpEnabled,
	      s.isNhrpEnabled,
	      s.isOspfEnabled,
	      s.isOspf3Enabled,
	      s.isPgmEnabled,
	      s.isPimEnabled,
	      s.isRipEnabled,
	      s.isRipngEnabled,
	      s.isRouterDiscoveryEnabled,
	      s.isRsvpEnabled,
	      s.isSapEnabled,
	      s.isVrrpEnabled
	  )
	}

	case class IfInboundService1Form (
			bootp:Boolean,
			dhcp:Boolean,
			dhcpv6:Boolean,
			dns:Boolean,
			finger:Boolean,
			ftp:Boolean,
			http:Boolean,
			https:Boolean,
			identReset:Boolean,
			ike:Boolean,
			lsping:Boolean,
			netconf:Boolean,
			ntp:Boolean,
			ping:Boolean,
			r2cp:Boolean,
			reverseSsh:Boolean,
			reverseTelnet:Boolean,
			rlogin:Boolean
			)
	
	object IfInboundService1Form {
	  def apply() = new IfInboundService1Form(
	      false, false, false, false, false,
	      false, false, false, false, false,
	      false, false, false, false, false,
	      false, false, false
	  )
	  def apply(s:srxlib.zone.InboundService) = new IfInboundService1Form(
	      s.isBootpEnabled,
	      s.isDhcpEnabled,
	      s.isDhcpv6Enabled,
	      s.isDnsEnabled,
	      s.isFingerEnabled,
	      s.isFtpEnabled,
	      s.isHttpEnabled,
	      s.isHttpsEnabled,
	      s.isIdentResetEnabled,
	      s.isIkeEnabled,
	      s.isLspingEnabled,
	      s.isNetconfEnabled,
	      s.isNtpEnabled,
	      s.isPingEnabled,
	      s.isR2cpEnabled,
	      s.isReverseSshEnabled,
	      s.isReverseTelnetEnabled,
	      s.isRloginEnabled
	      )
	}
	   
	case class IfInboundService2Form (
   			rpm:Boolean,
			rsh:Boolean,
			sip:Boolean,
			snmp:Boolean,
			snmpTrap:Boolean,
			ssh:Boolean,
			telnet:Boolean,
			tftp:Boolean,
			traceroute:Boolean,
			xnmClearText:Boolean,
			xnmSsl:Boolean
	    )
	   
	object IfInboundService2Form  {
	    def apply() = new IfInboundService2Form(
	        false, false, false, false, false, 
	        false, false, false, false, false, false
	    )
		def apply(s:srxlib.zone.InboundService) = new IfInboundService2Form(
			s.isRpmEnabled,
			s.isRshEnabled,
			s.isSipEnabled,
			s.isSnmpEnabled,
		    s.isSnmpTrapEnabled,
		    s.isSshEnabled,
		    s.isTelnetEnabled,
		    s.isTftpEnabled,
		    s.isTracerouteEnabled,
		    s.isXnmClearTextEnabled,
		    s.isXnmSslEnabled
		    )
	}
	
	case class IfBasicForm (ifName:IfNameForm, interfaceZoneName:String, ip:IfInetFamilyForm, vlanId:Option[Int], services1:IfInboundService1Form, services2:IfInboundService2Form, protocols:IfInboundProtocolForm, mtu:Option[Int], adminStateUp:Boolean) {
		def toPortInetInterface = {
			val o = new nic.PortInetInterface(ifName.phyName, ifName.unitId)
			updateLogicalInetInterface(o)
		}
	
		def toVlanInterface(implicit device:Device):nic.LogicalInetInterface = {
			val o = if (device.softwareInformation().isL2ng()) new l2ng.L2ngVlanInterface(ifName.unitId,vlanId.get) else new nic.VlanInterface(ifName.unitId, vlanId.get)
			updateLogicalInetInterface(o)
		}
	
		def toVpnInterface = {
			val o = new nic.VPNInterface(ifName.unitId)
			updateLogicalInetInterface(o)
		}
		
		def updateLogicalInetInterface(d:srxlib.nic.LogicalInetInterface) = {
			d.setZoneName(interfaceZoneName)
			d.setDisabled(!adminStateUp)
			if (ip.ipType == srxlib.nic.LogicalInetInterface.INET4_TYPE_STATIC)
				d.setInet4AddressAndMask(ip.address.get, ip.mask.get) 
			else 
				d.setInet4Type(ip.ipType)
			
			d.setInboundProtocols(InboundProtocol(protocols))
			d.setInboundServices(InboundService(services1, services2))
			
			d match {
			  case PortInetInterface(o) => o.setVlanId(if(vlanId.isDefined) vlanId.get else null)
			  case RethInterface(o) => o.setVlanId(if(vlanId.isDefined) vlanId.get else null)
			  case _ =>
			}	
			
			d.setMtu(if (mtu.isDefined) mtu.get else null)
			d
		}
	}
	
	object IfBasicForm {
		def apply(s:srxlib.nic.LogicalInetInterface) = new IfBasicForm(
				IfNameForm(s.getPhysicalInterfaceName, s.getUnitId),
				s.getZoneName,
				s match {
				case PPPoEInetInterface(v) => IfInetFamilyForm(s.getInet4Type, Some(s.getName), s.getInet4AddressAndMask)
				case _ => IfInetFamilyForm(s.getInet4Type, None, s.getInet4AddressAndMask)
				},
				Option(s.getVlanId).map(_.asInstanceOf[Int]),
				IfInboundService1Form(s.getInboundServices),
				IfInboundService2Form(s.getInboundServices),
				IfInboundProtocolForm(s.getInboundProtocols),
				Option(s.getMtu).map(_.asInstanceOf[Int]),
				s.isEnabled
				)  
	}
	
  	case class IfBindForm(ports:List[String], trunk:List[String])
  
  	case class IfDipForm(startAddress:String, endAddress:String, proxyArpEnabled:Boolean) {
  	  def toDip = Dip(startAddress, endAddress, proxyArpEnabled)
  	}

  	case class IfMipForm(mappedAddress:String, hostAddress:String, netmask:String, proxyArpEnabled:Boolean) {
  	  def toMip = Mip(mappedAddress, hostAddress, netmask, proxyArpEnabled)
  	}
  
  	case class IfVipForm(virtualAddress:String, virtualPort:Int, mapProtocol:String, mapPort:Int, mapAddress:String, proxyArpEnabled:Boolean) {
  		def toVip = Vip(virtualAddress, virtualPort, mapProtocol, mapPort, mapAddress, proxyArpEnabled)
  	}
  	
	case class AddressForm(name:String, comment:Option[String], addrType:Int, ip:Option[String], mask:Option[Int], dn:Option[String], zoneName:String) {
		import srxlib.policy.AddressElement.ADDRESS_TYPE_IP_PREFIX
		def toAddressElement = if (addrType == ADDRESS_TYPE_IP_PREFIX) 
		  AddressElement(zoneName, name, ip.get, mask.get)
		else
		  AddressElement(zoneName, name, dn.get)
	}
	object AddressForm {
		import srxlib.policy.AddressElement.ADDRESS_TYPE_IP_PREFIX
		import srxlib.policy.AddressElement.ADDRESS_TYPE_DNS_NAME
		def apply(s:srxlib.policy.AddressElement) = if (s.getAddressType == ADDRESS_TYPE_IP_PREFIX)
					new AddressForm(s.getName, None, ADDRESS_TYPE_IP_PREFIX, Some(s.getAddress), Some(s.getNetmask), None, s.getZoneName())
				else	
					new AddressForm(s.getName, None, ADDRESS_TYPE_DNS_NAME, None, None, Some(s.getAddress), s.getZoneName())
	}
	
	case class AddressGroupForm(name:String, zoneName:String, comment:Option[String], addresses:List[String]) {
		def toAddressSet(implicit d:srxlib.Device):srxlib.policy.AddressSet = {
		  val config = d.config
		  AddressSet(zoneName, name, 
		    addresses.filter(v => config.isAddressExist(zoneName, v)),
		    addresses.filter(v => config.isAddressSetExist(zoneName, v))
		  )
		}
	}
	
	object AddressGroupForm {
	  def apply(s:srxlib.policy.AddressSet):AddressGroupForm = AddressGroupForm(s.getName, s.getZoneName, None, s.getAddressNameList.asScala.toList ++ s.getAddressSetNameList.asScala.toList)
	}
	  
	case class AppGroupForm(name:String, comment:Option[String], apps:List[String]) {
		def toApplicationSet(implicit d:srxlib.Device):srxlib.policy.ApplicationSet = {
		  val config   = d.config
		  val defaults = d.defaults
		  ApplicationSet(name,
		    apps.filter(v => (config.isApplicationExist(v) || defaults.isApplicationExist(v))),
		    apps.filter(v => (config.isApplicationSetExist(v) || defaults.isApplicationSetExist(v))) 
		  )
		}
	}
	object AppGroupForm {
		def apply(s:srxlib.policy.ApplicationSet):AppGroupForm = AppGroupForm(s.getName, None, s.getApplicationNameList.asScala.toList ++ s.getApplicationSetNameList.asScala.toList)
	}
	
	case class AppProtocolForm(name:Option[String], protoType:Int, protocol:Option[String], srcLowPort:Option[String], srcHighPort:Option[String], dstLowPort:Option[String], dstHighPort:Option[String], icmpType:Option[String], icmpCode:Option[String], alg:Option[String]) {
		def toApplicationTerm = srxlib.policy.ApplicationTerm.createCustomInstance(
		   name.getOrElse(""), 
		   protoType match {
		     case AppProtocolForm.protoNone  => null
		     case AppProtocolForm.protoTcp   => "tcp"
		     case AppProtocolForm.protoUdp   => "udp"
		     case AppProtocolForm.protoIcmp  => "icmp"
		     case AppProtocolForm.protoIcmp6 => "icmp6"
		     case _ => protocol.get
		   },
		   srcLowPort.getOrElse(null),
		   srcHighPort.getOrElse(null),
		   dstLowPort.getOrElse(null),
		   dstHighPort.getOrElse(null),
		   icmpType.getOrElse(null),
		   icmpCode.getOrElse(null),
		   alg.getOrElse(null)
		)
	}
	
	object AppProtocolForm {
		val protoNone:Int  = 0
		val protoTcp:Int   = 1
		val protoUdp:Int   = 2
		val protoIcmp:Int  = 3
		val protoIcmp6:Int = 4
		val protoOther:Int = 5
		
		def apply(s:srxlib.policy.ApplicationBase):AppProtocolForm = AppProtocolForm(
		    Some(s.getName),
		    if 		(s.getProtocol.isEmpty) protoNone
		    else if (s.isProtocolTcp)       protoTcp
		    else if (s.isProtocolUdp)       protoUdp
		    else if (s.isProtocolIcmp)      protoIcmp
		    else if (s.isProtocolIcmp6)     protoIcmp6 
		    else                            protoOther,
		    Option(s.getProtocol),
		    Option(s.getSrcLowPort),
		    Option(s.getSrcHighPort),
		    Option(s.getDstLowPort),
		    Option(s.getDstHighPort),
		    Option(s.getIcmpType),
		    Option(s.getIcmpCode),
		    s match {
		      case ApplicationTerm(term) => Option(term.getAlg)
		      case _ => None
		    }
		)
	}
	
	case class AppTimeoutForm(toType:Int, sec:Option[Int])
	object AppTimeoutForm {
	  val typeDefault:Int = 1
	  val typeNever:Int   = 2
	  val typeCustom:Int  = 3
	  def apply(s:srxlib.policy.ApplicationBase):AppTimeoutForm = AppTimeoutForm(
			if      (s.getTimeout == null) typeDefault
		    else if (s.getTimeout == 0)    typeNever
		    else                           typeCustom,
		    if (s.getTimeout != null && s.getTimeout > 0) Some(s.getTimeout) else None
	  )
	}
	
	case class AppCustomForm(name:String, timeout:AppTimeoutForm, protocol:List[AppProtocolForm]) {
	  def toApplicationElement = srxlib.policy.ApplicationElement.createInstance(
	      name,
	      timeout.toType match {
	        case AppTimeoutForm.typeCustom => timeout.sec.get
	        case AppTimeoutForm.typeNever  => 0
	        case _                         => null
	      },
	      protocol.map{v => v.toApplicationTerm}.asJava
	  )
	}
	
	object AppCustomForm {
		def apply(s:srxlib.policy.ApplicationElement):AppCustomForm = AppCustomForm(
		    s.getName,
		    AppTimeoutForm(s),
		   if (s.getProtocol.isEmpty) s.getTermList.asScala.toList.filter(v => !v.getProtocol.isEmpty).map(v => AppProtocolForm(v))
		   else AppProtocolForm(s) :: s.getTermList.asScala.toList.filter(v => !v.getProtocol.isEmpty).map(v => AppProtocolForm(v))
		)
	}
	
	case class RtNetForm(addr:String, mask:Int)
	case class RtStaticForm(net:RtNetForm, gateway:Option[String], interface:Option[String], tag:Option[Int], metric:Option[Int], preference:Option[Int]) {
		def toStaticRoute:srxlib.route.StaticRoute = srxlib.route.StaticRoute.apply(net.addr, net.mask, gateway.getOrElse(null), interface.getOrElse(null), 
			if (tag.isDefined) tag.get else null,
			if (metric.isDefined) metric.get else null,
			if (preference.isDefined) preference.get else null)
	}

	case class PolicyForm(name:String, src:String, dst:String, app:String, action:String, log:Boolean, logWithBegin:Boolean) {
		def toPolicy(d:srxlib.Device, fromZone:String, toZone:String):srxlib.policy.Policy = new srxlib.policy.Policy(
			name, fromZone, toZone,
			src.split(",").filter(v => v == "any" || v == "any-ipv4" || v == "any-ipv6" || d.config.isAddressOrAddressSetExist(fromZone, v) || d.config.isAddressOrAddressSetExist("global", v)).toSeq.asJava,
			dst.split(",").filter(v => v == "any" || v == "any-ipv4" || v == "any-ipv6" || d.config.isAddressOrAddressSetExist(toZone, v) || d.config.isAddressOrAddressSetExist("global", v)).toSeq.asJava,
			app.split(",").filter(v => d.config.isApplicationOrApplicationSetExist(v) || d.defaults.isApplicationOrApplicationSetExist(v)).toSeq.asJava,
			action, logWithBegin, log
		)
	}

	object PolicyForm {
		def apply(s:srxlib.policy.Policy):PolicyForm = PolicyForm(
		    s.getName, 
		    s.getSourceNameList.asScala.mkString(","), 
		    s.getDestinationNameList.asScala.mkString(","),
		    s.getApplicationNameList.asScala.mkString(","),
		    s.getAction, s.isLogClose, s.isLogInit
		)
	}
	
  case class PolicyListForm(from:Option[String], to:Option[String], op:Option[String])
  
	case class AdvancedPolicyForm(srcNat:Boolean, dip:String)
	
	object AdvancedPolicyForm {
		def apply(s:srxlib.policy.Policy):AdvancedPolicyForm = AdvancedPolicyForm(s.isSourceNatEnabled, s.getSourceNat)
	}

	case class PPPoEForm(unitId:Int, boundInterface:String, userName:String, password:String, auth:Int, accessConcentrator:String, service:String, createDefaultRoute:Boolean, defaultRouteMetric:Int, disconnect:Int, autoConnect:Int, idleDisconnect:Int, propagateToDhcp:Boolean) {
		def toPPPoEInterface = {
		  val ret = PPPoE(unitId)
		  ret.setPPPOption(auth match {
		    case 1 => srxlib.PPPoE.PPPOE_AUTH.CHAP
		    case 2 => srxlib.PPPoE.PPPOE_AUTH.PAP
		    case 3 => srxlib.PPPoE.PPPOE_AUTH.ANY
		    case _ => srxlib.PPPoE.PPPOE_AUTH.NONE
		  }, userName, password)

		  if (disconnect == 0) 
			  ret.setPPPoEOption(boundInterface, autoConnect, 0, accessConcentrator, service)
		  else
			  ret.setPPPoEOption(boundInterface, 0, idleDisconnect, accessConcentrator, service)
		  ret.setPropagatedToDhcp(propagateToDhcp)
		  ret
		}
	}
	
	object PPPoEForm {
		def apply(s:srxlib.PPPoE, rt:Option[srxlib.route.StaticRoute]):PPPoEForm = PPPoEForm(
		    s.getUnitId,
		    s.getUnderlyingInterfaceName,
		    s.getUserName,
		    s.getPassword,
		    s.getAuthType match {
		      case srxlib.PPPoE.PPPOE_AUTH.CHAP => 1
		      case srxlib.PPPoE.PPPOE_AUTH.PAP  => 2
		      case srxlib.PPPoE.PPPOE_AUTH.ANY  => 3
		      case _                            => 0
		    },
		    s.getAccessConcentrator,
		    s.getServiceName,
		    if (rt.isDefined) true else false,
		    rt.map(v => Option(v.getMetric).map(_.toInt).getOrElse(0)).getOrElse(0),
		    if (s.getIdleTimeout > 0) 1 else 0,
		    s.getAutoReconnect,
		    s.getIdleTimeout,
		    s.isPropagatedToDhcp
			)
	}

	case class NtpForm(server:Option[String], keyId:Option[Int], keyValue:Option[String]) {
		def toNtpServer(prefer:Boolean) = if(server.isDefined) Some(new srxlib.DateTime.NtpServer(server.get,
		    if (keyId.isDefined && keyValue.isDefined) keyId.get else null,
		    if (keyId.isDefined && keyValue.isDefined) keyValue.get else null,
		    prefer
		    )) else None
	}
	object NtpForm {
	  def apply(s:srxlib.DateTime.NtpServer) = {
	    val address = s.getAddress
	    val keyId = s.getKeyId
	    val keyValue = s.getKeyValue
	    new NtpForm(
	      if (address == null || address.isEmpty) None else Some(address),
	      if (keyId == null) None else Some(keyId),
	      if (keyValue == null || keyValue.isEmpty) None else Some(keyValue)
	      )
	  }
	}
	
	case class DateTimeForm(timeZone:String, ntpSourceAddress:Option[String], ntp1:NtpForm, ntp2:NtpForm, ntp3:NtpForm) {
	  def toDateTime = {
			  val dt = new srxlib.DateTime(timeZone)
			  if (ntpSourceAddress.isDefined) dt.setNtpSourceAddress(ntpSourceAddress.get)
			  
			  val n1 = ntp1.toNtpServer(true)
			  val n2 = ntp2.toNtpServer(false)
			  val n3 = ntp3.toNtpServer(false)
			  
			  if (n1.isDefined) dt.addNtpServer(n1.get)
			  if (n2.isDefined) dt.addNtpServer(n2.get)
			  if (n3.isDefined) dt.addNtpServer(n3.get)
			  
			  dt
	  }
	}
	object DateTimeForm {
		def apply(s:srxlib.DateTime) = {
		    val ntpSourceAddress = s.getNtpSourceAddress
			s.sortNtpServerList
			val ntpList = s.getNtpServerList
			new DateTimeForm(s.getTimeZone, 
			    if (ntpSourceAddress == null || ntpSourceAddress.isEmpty) None else Some(ntpSourceAddress),
			    if (ntpList.size > 0) NtpForm(ntpList.get(0)) else NtpForm(None, None, None),
			    if (ntpList.size > 1) NtpForm(ntpList.get(1)) else NtpForm(None, None, None),
			    if (ntpList.size > 2) NtpForm(ntpList.get(2)) else NtpForm(None, None, None)
			    )
		}
	}
	
	case class DnsHostForm(hostName:Option[String], domainName:Option[String], dnsServer1:Option[String], dnsServer2:Option[String], dnsServer3:Option[String]) {
		def toDnsHost = {
		  val ret = new srxlib.DnsHost
		  if (hostName.isDefined) ret.setHostName(hostName.get)
		  if (domainName.isDefined) ret.setDomainName(domainName.get)
		  if (dnsServer1.isDefined) ret.addDnsServer(dnsServer1.get)
		  if (dnsServer2.isDefined) ret.addDnsServer(dnsServer2.get)
		  if (dnsServer3.isDefined) ret.addDnsServer(dnsServer3.get)
		  ret
		}
	}
	
	object DnsHostForm {
		def apply(s:srxlib.DnsHost) = {
			val hostName = s.getHostName
			val domainName = s.getDomainName
			val domainServerList = s.getDnsServerList

			new DnsHostForm(
			    if (hostName == null || hostName.isEmpty) None else Some(hostName),
			    if (domainName == null || domainName.isEmpty) None else Some(domainName),
			    if (domainServerList.size > 0) Some(domainServerList.get(0)) else None,
			    if (domainServerList.size > 1) Some(domainServerList.get(1)) else None,
			    if (domainServerList.size > 2) Some(domainServerList.get(2)) else None
			    )
		}
		    
	}
	
	case class ALGBasicForm(
	    dns:Boolean, ftp:Boolean, h323:Boolean, ikeEspNat:Boolean, mgcp:Boolean, 
	    msrpc:Boolean, pptp:Boolean, real:Boolean, rsh:Boolean, rtsp:Boolean, 
	    sccp:Boolean, sip:Boolean, sql:Boolean, sunrpc:Boolean, talk:Boolean, 
	    tftp:Boolean
	    ) {
	  def toALG = {
	    val alg = new srxlib.alg.ALG
	    alg.setDnsEnabled(dns)
	    alg.setFtpEnabled(ftp)
	    alg.setH323Enabled(h323)
	    alg.setIkeEspNatEnabled(ikeEspNat)
	    alg.setMgcpEnabled(mgcp)
	    alg.setMsrpcEnabled(msrpc)
	    alg.setPptpEnabled(pptp)
	    alg.setRealEnabled(real)
	    alg.setRshEnabled(rsh)
	    alg.setRtspEnabled(rtsp)
	    alg.setSccpEnabled(sccp)
	    alg.setSipEnabled(sip)
	    alg.setSqlEnabled(sql)
	    alg.setSunrpcEnabled(sunrpc)
	    alg.setTalkEnabled(talk)
	    alg.setTftpEnabled(tftp)
	    alg
	  }
	}
	    
	object ALGBasicForm {
		def apply(s:srxlib.alg.ALG) = new ALGBasicForm(
		    s.isDnsEnabled,
		    s.isFtpEnabled,
		    s.isH323Enabled,
		    s.isIkeEspNatEnabled,
		    s.isMgcpEnabled,
		    s.isMsrpcEnabled,
		    s.isPptpEnabled,
		    s.isRealEnabled,
		    s.isRshEnabled,
		    s.isRtspEnabled,
		    s.isSccpEnabled,
		    s.isSipEnabled,
		    s.isSqlEnabled,
		    s.isSunrpcEnabled,
		    s.isTalkEnabled,
		    s.isTftpEnabled
		)
	}
	
	case class DhcpForm(serviceType:Int, relay1:Option[String], relay2:Option[String], relay3:Option[String], 
	    				leaseType:Option[Int], leaseDay:Option[Int], leaseHour:Option[Int], leaseMin:Option[Int],
	    				addressLow:Option[String], addressHigh:Option[String],
						gateway:Option[String], dnsServer:Option[String], winsServer:Option[String]) {
	  def toDhcpService(lname:String, addr:String) = serviceType match {
	    case 1 => new srxlib.dhcp.DhcpNone(lname, new srxlib.nic.LogicalInetInterface.IPv4AddressAndMask(addr))
	    case 2 => {
	    	val o = new srxlib.dhcp.DhcpRelayAgent(lname, new srxlib.nic.LogicalInetInterface.IPv4AddressAndMask(addr))
	    	if (relay1.isDefined) o.addRelayServer(relay1.get)
	    	if (relay2.isDefined) o.addRelayServer(relay2.get)
	    	if (relay3.isDefined) o.addRelayServer(relay3.get)
	    	o
	    }
	    case 3 => {
	    	val o = new srxlib.dhcp.DhcpServer(lname, new srxlib.nic.LogicalInetInterface.IPv4AddressAndMask(addr))
	    	o.setLowAddress(addressLow.getOrElse(""))
	    	o.setHighAddress(addressHigh.getOrElse(""))
	    	if (leaseType.getOrElse(0) == 2) 
	    		o.setLeaseTime(86400 * leaseDay.getOrElse(0) + 3600 * leaseHour.getOrElse(0) + 60 * leaseMin.getOrElse(0))
	    	o.setGateway(gateway.getOrElse(""))
	    	o.insertDnsServer(0, dnsServer.getOrElse(""))
	    	o.insertWinsServer(0, winsServer.getOrElse(""))
	    	o
	    }
	  }
	}
						
	object DhcpForm {
	  def apply(s:srxlib.dhcp.DhcpService) = s match {
	    case DhcpNone(dhcp) => new DhcpForm(1, None, None, None, None, None, None, None, None, None, None, None, None)
	    case DhcpRelayAgent(dhcp) => { 
	      val serverList = dhcp.getRelayServerList
	      new DhcpForm(2, 
	        if (serverList.size > 0) Some(serverList.get(0)) else None, 
	        if (serverList.size > 1) Some(serverList.get(1)) else None, 
	        if (serverList.size > 2) Some(serverList.get(2)) else None, 
	        None, None, None, None, None, None, None, None, None)
	    }
	    case DhcpServer(dhcp) => {
	      val leaseTime = dhcp.getLeaseTime
	      val dnsList = dhcp.getDnsServerList
	      val winsList = dhcp.getWinsServerList
	      new DhcpForm(3, None, None, None, 
	          if (leaseTime == -1) Some(1) else Some(2), 
	          if (leaseTime == -1) Some(0) else Some(leaseTime / 86400), 
	          if (leaseTime == -1) Some(0) else Some((leaseTime % 86400) / 3600), 
	          if (leaseTime == -1) Some(0) else Some(((leaseTime % 86400) % 3600) / 60), 
	          Option(dhcp.getLowAddress), 
	          Option(dhcp.getHighAddress),
	          Option(dhcp.getGateway),
	          if (dnsList.size > 0) Some(dnsList.get(0)) else None,
	          if (winsList.size > 0) Some(winsList.get(0)) else None)
	    }
	  }
	}
	
	case class SnmpForm(name:String, contact:String, location:String, description:String) {
	  def toSnmp = Snmp(name, contact, location, description)
	}
	
	object SnmpForm {
		def apply(s:srxlib.snmp.Snmp) = new SnmpForm(s.getName, s.getContact, s.getLocation, s.getDescription)
	}
	
	case class SnmpCommunityForm(name:String, writeEnabled:Boolean, trapEnabled:Boolean, trapVersion:String) {
		def toSnmpCommunity = SnmpCommunity(name, writeEnabled, trapEnabled, trapVersion)
	}

	object SnmpCommunityForm {
		def apply(s:srxlib.snmp.SnmpCommunity) = new SnmpCommunityForm(s.getName, s.isWriteEnabled, s.isTrapEnabled, s.getTrapVersion)
	}
	
	case class VpnP1ProposalForm(name:String, authMethod:String, dhGroup:String, encAlg:String, hashAlg:String, lifeTime:Int) {
	  def toIkeProposal = IkeProposal(name, authMethod, hashAlg, dhGroup, encAlg, lifeTime)
	}
	
	object VpnP1ProposalForm {
	  def apply(s:srxlib.vpn.IkeProposal) = new VpnP1ProposalForm(s.getName, s.getAuthenticationMethod, s.getDhGroup, s.getEncryptionAlgorithm, s.getAuthenticationAlgorithm, s.getLifeTime)
	}
	
	case class VpnP2ProposalForm(name:String, encap:String, encAlg:String, authAlg1:String, authAlg2:String, lifeTime:Int, lifeSize:Long) {
		def toIPSecProposal = encap match {
		  case srxlib.vpn.IPSecProposal.PROTOCOL_AH  => IPSecProposal(name, encap, authAlg2, null, lifeTime, lifeSize)
		  case srxlib.vpn.IPSecProposal.PROTOCOL_ESP => IPSecProposal(name, encap, authAlg1, encAlg, lifeTime, lifeSize)
		}
	}
	
	object VpnP2ProposalForm {
		def apply(s:srxlib.vpn.IPSecProposal) = new VpnP2ProposalForm(s.getName, s.getProtocol, s.getEncryptionAlgorithm, s.getAuthenticationAlgorithm, s.getAuthenticationAlgorithm, s.getLifetimeSeconds, s.getLifetimeKilobytes)
	}

	case class VpnP1PolicyForm(presharedKey:String, mode:String, proposalSet:String, proposals:List[String]) {
		def toIkePolicy(name:String) = {
			val p = IkePolicy(name, mode, presharedKey, proposalSet)
			proposals.foreach(p.addProposalName(_))
			p
		}
	}
	
	object VpnP1PolicyForm {
		def apply(s:srxlib.vpn.IkePolicy) = new VpnP1PolicyForm(s.getPresharedKey, s.getMode, if (s.getProposalSet.isEmpty) "custom" else s.getProposalSet, s.getProposalNameList.asScala.toList)
	}
	
	case class VpnGatewayPeerForm(peerType:String, address:String, peerId:String) {
	  import srxlib.vpn.IkeGateway.PEER_TYPE_STATIC
	  def peer = peerType match {
	    case PEER_TYPE_STATIC => address
	    case _ => peerId
	  }
	}

	object VpnGatewayPeerForm {
	  import srxlib.vpn.IkeGateway.PEER_TYPE_STATIC
	  def apply(gw:srxlib.vpn.IkeGateway) = gw.getPeerType match {
	    case PEER_TYPE_STATIC => new VpnGatewayPeerForm(PEER_TYPE_STATIC, gw.getPeer, "")
	    case _ => new VpnGatewayPeerForm(gw.getPeerType, "", gw.getPeer)
	  }
	}
	
	case class VpnGatewayForm(name:String, ikeVersion:String, peer:VpnGatewayPeerForm, localId:String, outgoingInterface:String, policy:VpnP1PolicyForm, natTraversalEnabled:Boolean, natKeepAlive:Int,  dpdInterval:Int, dpdThreshold:Int, dpdAlwaysSend:Boolean) {
	  def toIkeGateway(policyName:String) = IkeGateway(name, ikeVersion, peer.peerType, peer.peer, outgoingInterface, localId, policyName, natTraversalEnabled, natKeepAlive, dpdInterval, dpdThreshold, dpdAlwaysSend)
	  def makePolicyName(config:srxlib.DeviceConfig) = srxlib.vpn.IkeGateway.makePolicyName(name, config)
	}
	
	object VpnGatewayForm {
	  def apply(gw:srxlib.vpn.IkeGateway, policy:Option[srxlib.vpn.IkePolicy]) = new VpnGatewayForm(gw.getName, gw.getVersion, VpnGatewayPeerForm(gw), gw.getLocalId, gw.getExternalInterface, 
	      policy.map(v => VpnP1PolicyForm(v)).getOrElse(VpnP1PolicyForm("", srxlib.vpn.IkePolicy.MODE_MAIN, srxlib.vpn.IkePolicy.PROPOSAL_SET_STANDARD, List.empty)), 
	      gw.isNatTraversalEnabled, gw.getNatKeepAlive, gw.getDpdInterval, gw.getDpdThreshold, gw.isDpdAlwaysSend)
	}
	
	case class VpnP2PolicyForm(pfsKeys:String, proposalSet:String, proposals:List[String]) {
		def toIPSecPolicy(name:String) = {
			val p = IPSecPolicy(name, pfsKeys, proposalSet)
			proposals.foreach(p.addProposalName(_))
			p
		}
	}
	
	object VpnP2PolicyForm {
		def apply(s:srxlib.vpn.IPSecPolicy) = new VpnP2PolicyForm(s.getPfsKeys, if (s.getProposalSet.isEmpty) "custom" else s.getProposalSet, s.getProposalNameList.asScala.toList)
	}

	case class VpnForm(name:String, gwName:String, policy:VpnP2PolicyForm, bindIntName:String, antiReplay:Boolean, establishImmediately:Boolean, monEnabled:Boolean, monDest:Option[String], monSrcIntName:Option[String], monOptimized:Boolean) {
		def toIPSecVpn(policyName:String) = IPSecVpn(name, gwName, policyName, bindIntName, !antiReplay, establishImmediately, monEnabled, monDest.getOrElse(""), monOptimized, monSrcIntName.getOrElse(""))
		def makePolicyName(config:srxlib.DeviceConfig) = srxlib.vpn.IPSecVpn.makePolicyName(name, config)
	}

	object VpnForm {
	  def apply(vpn:srxlib.vpn.IPSecVpn, policy:Option[srxlib.vpn.IPSecPolicy]) = new VpnForm(vpn.getName, vpn.getGatewayName, 
	      policy.map(v => VpnP2PolicyForm(v)).getOrElse(VpnP2PolicyForm("", srxlib.vpn.IPSecPolicy.PROPOSAL_SET_STANDARD, List.empty)), 
	      vpn.getBindInterfaceName, !vpn.isAntiReplayDisabled, vpn.isEstablishImmediately, vpn.isMonitorEnabled, Option(vpn.getMonitorDestinationIP), Option(vpn.getMonitorSourceInterfaceName), vpn.isMonitorOptimized)
	}	
	
	case class VpnProxyIdForm(local:RtNetForm, remote:RtNetForm, service:String) {
		def toProxyId = new srxlib.vpn.IPSecVpn.ProxyId(local.addr, local.mask, remote.addr, remote.mask, service)
	}
	
	object VpnProxyIdForm {
		def apply(s:srxlib.vpn.IPSecVpn) = Option(s.getProxyId) match {
		  case Some(pid) => new VpnProxyIdForm(RtNetForm(pid.getLocalAddress, pid.getLocalMask), RtNetForm(pid.getRemoteAddress, pid.getRemoteMask), pid.getServiceName)
		  case _ => new VpnProxyIdForm(RtNetForm("", 0), RtNetForm("", 0), "")
		}
	}
	
	case class ScreenFloodForm (icmp:Boolean, icmpThreshold:Int, udp:Boolean, udpThreshold:Int, 
	    syn:Boolean, synAttackThreshold:Int, synAlarmThreshold:Int, synSrcThreshold:Int, synDstThreshold:Int, synTimeout:Int) {
		def updateIDSOption(s:srxlib.zone.IDSOption) = {
			s.setIcmpFloodEnabled(icmp)
			s.setIcmpFloodThreshold(icmpThreshold)
			s.setUdpFloodEnabled(udp)
			s.setUdpFloodThreshold(udpThreshold)
			s.setTcpSynFloodEnabled(syn)
			s.setTcpSynFloodAttackThreshold(synAttackThreshold)
			s.setTcpSynFloodAlarmThreshold(synAlarmThreshold)
			s.setTcpSynFloodSrcThreshold(synSrcThreshold)
			s.setTcpSynFloodDstThreshold(synDstThreshold)
			s.setTcpSynFloodTimeout(synTimeout)
		}
	}
	   
	object ScreenFloodForm {
	  def apply(s:srxlib.zone.IDSOption) = new ScreenFloodForm(
			  s.isIcmpFloodEnabled,
			  s.getIcmpFloodThreshold,
			  s.isUdpFloodEnabled,
			  s.getUdpFloodThreshold,
			  s.isTcpSynFloodEnabled,
			  s.getTcpSynFloodAttackThreshold,
			  s.getTcpSynFloodAlarmThreshold,
			  s.getTcpSynFloodSrcThreshold,
			  s.getTcpSynFloodDstThreshold,
			  s.getTcpSynFloodTimeout
	      )
	}
	case class ScreenMSForm(winnuke:Boolean) {
		def updateIDSOption(s:srxlib.zone.IDSOption) = {
		  s.setTcpWinnukeEnabled(winnuke)
		}
	}

	object ScreenMSForm {
		def apply(s:srxlib.zone.IDSOption) = new ScreenMSForm(s.isTcpWinnukeEnabled)
	}
	
	case class ScreenScanForm(ipSpoof:Boolean, ipSweep:Boolean, ipSweepThreshold:Int, portScan:Boolean, portScanThreshold:Int,
	    tcpSweep:Boolean, tcpSweepThreshold:Int, udpSweep:Boolean, udpSweepThreshold:Int) {
		def updateIDSOption(s:srxlib.zone.IDSOption) = {
		  s.setIpSpoofEnabled(ipSpoof)
		  s.setIpSweepEnabled(ipSweep)
		  s.setIpSweepThreshold(ipSweepThreshold)
		  s.setTcpPortScanEnabled(portScan)
		  s.setTcpPortScanThreshold(portScanThreshold)
		  s.setTcpSweepEnabled(tcpSweep)
		  s.setTcpSweepThreshold(tcpSweepThreshold)
		  s.setUdpSweepEnabled(udpSweep)
		  s.setUdpSweepThreshold(udpSweepThreshold)
		}
	}
	
	object ScreenScanForm {
		def apply(s:srxlib.zone.IDSOption) = new ScreenScanForm(
		    s.isIpSpoofEnabled,
		    s.isIpSweepEnabled,
		    s.getIpSweepThreshold,
		    s.isTcpPortScanEnabled,
		    s.getTcpPortScanThreshold,
		    s.isTcpSweepEnabled,
		    s.getTcpSweepThreshold,
		    s.isUdpSweepEnabled,
		    s.getUdpSweepThreshold
		    )
	}
	
	case class ScreenDosForm(pingOfDeath:Boolean, tearDrop:Boolean, icmpFragment:Boolean, icmpLarge:Boolean, blockFragment:Boolean, land:Boolean, 
	    synAckAck:Boolean, synAckAckThreshold:Int, srcLimit:Boolean, srcLimitThreshold:Int, dstLimit:Boolean, dstLimitThreshold:Int) {
		def updateIDSOption(s:srxlib.zone.IDSOption) = {
			s.setIcmpPingOfDeathEnabled(pingOfDeath)
			s.setIpTeardropEnabled(tearDrop)
			s.setIcmpFragmentEnabled(icmpFragment)
			s.setIcmpLargeEnabled(icmpLarge)
			s.setIpBlockFragment(blockFragment)
			s.setTcpLandEnabled(land)
			s.setTcpSynAckAckEnabled(synAckAck)
			s.setTcpSynAckAckThreshold(synAckAckThreshold)
			s.setSrcLimitEnabled(srcLimit)
			s.setSrcLimitThreshold(srcLimitThreshold)
			s.setDstLimitEnabled(dstLimit)
			s.setDstLimitThreshold(dstLimitThreshold)
		}
	}

	object ScreenDosForm {
		def apply(s:srxlib.zone.IDSOption) = new ScreenDosForm(
				s.isIcmpPingOfDeathEnabled,
				s.isIpTeardropEnabled,
				s.isIcmpFragmentEnabled,
				s.isIcmpLargeEnabled,
				s.isIpBlockFragment,
				s.isTcpLandEnabled,
				s.isTcpSynAckAckEnabled,
				s.getTcpSynAckAckThreshold,
				s.isSrcLimitEnabled,
				s.getSrcLimitThreshold,
				s.isDstLimitEnabled,
				s.getDstLimitThreshold
		    )
	}
	
	case class ScreenIPOptionForm(badOption:Boolean, timestamp:Boolean, security:Boolean, stream:Boolean, recordRoute:Boolean, looseSrcRoute:Boolean, strictSrcRoute:Boolean, srcRoute:Boolean) {
		def updateIDSOption(s:srxlib.zone.IDSOption) = {
		  s.setIpBadOptionEnabled(badOption)
		  s.setIpTimestampOptionEnabled(timestamp)
		  s.setIpSecurityOptionEnabled(security)
		  s.setIpStreamOptionEnabled(stream)
		  s.setIpRecordRouteOptionEnabled(recordRoute)
		  s.setIpLooseSrcRouteOptionEnabled(looseSrcRoute)
		  s.setIpStrictSrcRouteOptionEnabled(strictSrcRoute)
		  s.setIpSrcRouteOptionEnabled(srcRoute)
		}
	}

	object ScreenIPOptionForm {
		def apply(s:srxlib.zone.IDSOption) = new ScreenIPOptionForm(
		    s.isIpBadOptionEnabled,
		    s.isIpTimestampOptionEnabled,
		    s.isIpSecurityOptionEnabled,
		    s.isIpStreamOptionEnabled,
		    s.isIpRecordRouteOptionEnabled,
		    s.isIpLooseSrcRouteOptionEnabled,
		    s.isIpStrictSrcRouteOptionEnabled,
		    s.isIpSrcRouteOptionEnabled
		    )
	}
	
	case class ScreenTcpForm(synFragment:Boolean, withoutFlag:Boolean, synAndFin:Boolean, finNoAck:Boolean, unknownProtocol:Boolean)  {
		def updateIDSOption(s:srxlib.zone.IDSOption) = {
			s.setTcpSynFragmentEnabled(synFragment)
			s.setTcpNoFlagEnabled(withoutFlag)
			s.setTcpSynAndFinEnabled(synAndFin)
			s.setTcpFinNoAckEnabled(finNoAck)
			s.setIpUnknownProtocolEnabled(unknownProtocol)
		}
	}

	object ScreenTcpForm {
		def apply(s:srxlib.zone.IDSOption) = new ScreenTcpForm(
		    s.isTcpSynFragmentEnabled,
		    s.isTcpNoFlagEnabled,
		    s.isTcpSynAndFinEnabled,
		    s.isTcpFinNoAckEnabled,
		    s.isIpUnknownProtocolEnabled
		    )
	}
	
	case class ScreenForm(alarm:Boolean, flood:ScreenFloodForm, ms:ScreenMSForm, scan:ScreenScanForm, dos:ScreenDosForm, ip:ScreenIPOptionForm, tcp:ScreenTcpForm) {
		def updateIDSOption(s:srxlib.zone.IDSOption) = {
		  s.setAlarmWithoutDrop(alarm)
		  flood.updateIDSOption(s)
		  ms.updateIDSOption(s)
		  scan.updateIDSOption(s)
		  dos.updateIDSOption(s)
		  ip.updateIDSOption(s)
		  tcp.updateIDSOption(s)
		}
	}
	
	object ScreenForm {
		def apply(s:srxlib.zone.IDSOption) = new ScreenForm(
		    s.isAlarmWithoutDrop,
		    ScreenFloodForm(s),
		    ScreenMSForm(s),
		    ScreenScanForm(s),
		    ScreenDosForm(s),
		    ScreenIPOptionForm(s),
		    ScreenTcpForm(s)
		    )
	}
	
	case class LoginUserForm(name:String, password:(String, String), className:String) {
	  def toLoginUser = LoginUser(name, password._1, className)
	}
	
	object LoginUserForm {
	  def apply(s:srxlib.login.LoginUser) = new LoginUserForm(s.getName, ("", ""), s.getClassName)
	}
	
	case class SyslogHostForm(name:String, facility:String, level:String, port:Option[Int], sourceAddress:Option[String], isTrafficLog:Boolean) {
	  def toSyslogHost = SyslogHost(name, facility, level, port.map(_.asInstanceOf[Integer]).orNull, sourceAddress.orNull, isTrafficLog)
	}
	
	object SyslogHostForm {
		def apply(s:srxlib.syslog.SyslogHost) = new SyslogHostForm(s.getName, s.getFacility, s.getLevel, Option(s.getPort).map(_.toInt), Option(s.getSourceAddress), s.isTrafficLog)
	}
	
	case class SyslogFileForm(name:String, facility:String, level:String, isStructuredData:Boolean, isTrafficLog:Boolean) {
	  def toSyslogFile = SyslogFile(name, facility, level, isStructuredData, isTrafficLog)
	}
	
	object SyslogFileForm {
		def apply(s:srxlib.syslog.SyslogFile) = new SyslogFileForm(s.getName, s.getFacility, s.getLevel, s.isStructuredData, s.isTrafficLog)
	}
	
	case class SyslogForm(host:List[SyslogHostForm], disableEventMode:Boolean) //, file:List[SyslogFileForm])
}
