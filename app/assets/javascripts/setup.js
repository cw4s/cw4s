(function($) {
	
	$(function() {
		$('a.homeBtn').click(function(evt) {
			var e = $(this);
			routes.controllers.Api.check(e.data("device")).ajax()
			.done(function(data) {
				if (data.result === true) 
					window.location = e.attr("href");
				else
					alert("Error occurred: " + data.error);
			})
			.fail(function(data) {
				alert("Error occurred.");
			})
			return false;
		});
	})
})(jQuery);