(function($){

	$(function() {
		var sm = $('#members');
		var sa = $('#availables');
		
		$('#addBtn').click(function(evt) {
			$('option:selected', sa).each(function() {
				var e = $(this);
				var v = e.val();
				if (v && v.length > 0)
					sm.append(e);
			})
			return false;
		});
		
		$('#removeBtn').click(function(evt) {
			$('option:selected', sm).each(function() {
				var e = $(this);
				var v = e.val();
				if (v && v.length > 0)
					sa.append(e);
			})
			return false;
		});	
		
		$('#form1').submit(function(evt) {
			$('option', sm).attr('selected', true);
		});
	});
})(jQuery);