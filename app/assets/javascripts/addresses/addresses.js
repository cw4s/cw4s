/**
 * 
 */
(function($) {
	$(function() {
		$('#zoneSelector').change(function(evt) {
			var e = $(this);
			window.location = routes.controllers.Addresses.list(e.data("device"), e.val()).url;
			return false;
		});
	
		$('#form1').submit(function(evt){
			var e = $('#zoneSelector');
			window.location = routes.controllers.Addresses.createAddress(e.data("device"), e.val()).url;
			return false;
		});
	});
})(jQuery);