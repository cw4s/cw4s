(function(d, $) {
	$(function() {
		$('select[name=ip_ver]').change(function(){
			switch( $(this).val() ) {
			case '-1':
				$('.formTable tr.inet').show();
				$('.formTable tr.inet6').show();
				break;
			case '0':
				$('.formTable tr.inet').show();
				$('.formTable tr.inet6').hide();
				break;
			case '1':
				$('.formTable tr.inet').hide();
				$('.formTable tr.inet6').show();
				break;
			}
		});
		
		$('#refresh_btn').click(function(evt) {
			routes.controllers.Api.refreshNetworkRoutes($(this).data('device')).ajax()
				.done(function(data) {
					if (!data.result) {
						alert(data.error);
					}
			
					window.location.reload();
				})
				return false;
			})	
	});
})(document, jQuery);