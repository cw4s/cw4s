/**
 * 
 */
(function($) {
	$(function() {
		var target = document.getElementById('view');
		var view = $(target);
		var name = view.data("device");
		
		$
		.when(routes.controllers.Api.candidateConfig(name).ajax(), routes.controllers.Api.editConfig(name).ajax())
		.done(function(edit, candidate) {
			CodeMirror.MergeView(target, {
				value: edit[0],
				orig: candidate[0],
				lineNumbers: true,
				mode: "text/xml",
				highlightDifferences: true,
				readOnly: true
			});
		
			var header = $('#header');
			var width1 = $('.CodeMirror-merge-pane').outerWidth();
			var width2 = $('.CodeMirror-merge-gap').outerWidth();
			
			header.append(
					$('<div style="background-color: silver; display: inline-block;">Candidate Config</div>').css('width', width1 + 'px'),
					$('<div style="display: inline-block;">&nbsp;</div>').css('width', width2 + 'px'),
					$('<div style="background-color: silver; display: inline-block;">Editing Config</div>').css('width', width1 + 'px')
					);
		});
	});
})(jQuery);