(function(d, $) {
	$(function() {
		var suffix = $('.formTable').hasClass('interface') ? '.parent' : '';

		var $page = $('select[name=goto_page]'),
			$view = $('select[name=num_per_page]'),
			selector = '.formTable tbody tr',
			$table = $(selector + suffix),
			count = $table.length;

		$view.change(function() {
			$('#pager')[$(this).val() < count ? 'show' : 'hide']();
			$page.empty()
			for ( var i = 1; i <= Math.ceil(count / $(this).val()); i++ ) 
				$page.append('<option value="'+i+'">'+i+'</option>');
			refleshTable(1);
		}).change();

		$page.change(function() {
			refleshTable($(this).val());
		});

		$('#pager [name=back]').click(function() {
			refleshTable( parseInt($page.val()) - 1);
		});

		$('#pager [name=forward]').click(function() {
			refleshTable( parseInt($page.val()) + 1);
		});

		function refleshTable(page) {
			var view = parseInt($view.val()),
				max_page = Math.ceil(count / view);
			if (page < 1 || page > max_page) return;

			$page.val(page);
			$(selector).hide();

			var offset = view * (page - 1),
				$back = $('#pager [name=back]'),
				$forward = $('#pager [name=forward]');
			$table.slice(offset, offset + view).each(function(){
				$(this).show();
				$('.formTable tbody tr[data-parent="'+$(this).data('parent')+'"]').show();
			});

			if (page == 1) {
				$back.attr('src', $back.attr('src').replace('backarror.gif', 'backarror_g.gif'));
			} else if(page == max_page) {
				$forward.attr('src', $forward.attr('src').replace('forwardarror.gif', 'forwardarror_g.gif'));
			} else {
				$back.attr('src', $back.attr('src').replace('backarror_g.gif', 'backarror.gif'));
				$forward.attr('src', $forward.attr('src').replace('forwardarror_g.gif', 'forwardarror.gif'));
			}
		}
	});
})(document, jQuery);