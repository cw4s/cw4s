(function(d, $) {
	$(function() {
		var intervalId = null,
			interval = location.search.replace('?','');
		if (interval == "") interval = 0;

		$('[name=ref_time]').change(function() {
			var val = $(this).val() ;
			clearInterval(intervalId);

			if (val != 0) {
				intervalId = setTimeout(function() {
					location.href = location.origin + location.pathname + '?' + val;
				}, val);
			}
		}).val(interval).change();
	});
})(document, jQuery);