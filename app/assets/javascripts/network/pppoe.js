/**
 * 
 */
(function($) {
	$(function() {
		$('#refresh_btn').click(function(evt) {
			routes.controllers.Api.refreshNetworkPPPoE($(this).data('device')).ajax()
				.done(function(data) {
					if (!data.result) {
						alert(data.error);
					}
			
					window.location.reload();
				})
				return false;
			})
	});
})(jQuery);