$(function(){
	var data = {};

	routes.controllers.Resources.initialSetupData().ajax({async: false}).done(function(json) {
		data = json;
		$('[name=platform]').append('<option>Select platform first...</option>');
		$.each(data, function(k, v)
		{
			if( v.enabled ) $('[name=platform]').append('<option>'+k+'</option>');
		});
	});


	//
	$('#flush').click(function(){
		location.href = location.href;
	});
	$('#exit').click(function(){
		location.href = '/';
	});
	
	// ------------------------------------------------------------
	$('[name=platform]').change( function()
	{
		$('option[value=""]', this).remove();
		$('[id^=add]').show();
		$('#addLogicalInterfaces1').hide();
		$('#LogicalInterfacesInfo').show();
		$('[name=cc][value=0]').click();
		$('#generate').prop('disabled', false);

		var param = data[this.value];

		//
		$('[name=passwd]').val("");

		//
		$('#SecurityZones [name=securityZoneName0]').val('trust');
		$('#SecurityZones [name=securityZoneName1]').val('untrust');
		$('#SecurityZones tbody tr:nth-child(n+3)').remove();
		$('#SecurityZones input').prop('disabled', false).prop('checked', false);
		$('#SecurityZones tbody tr:first [type=checkbox]:not(:first)').prop('disabled', true);
		$('#SecurityZones tbody tr:first [type=checkbox]').prop('checked', true);
		
		// ------------------------ cc enable
		if( $('[name=platform]').val() == 'Firefly Perimeter' ) 
		{
			$('[name=hostname0]').val('vsrx-a');
			$('[name=hostname1]').val('vsrx-b');
		}
		else
		{
			$('[name=hostname0]').val('srx-a');
			$('[name=hostname1]').val('srx-b');
		}
		$('[name=hostname0], [name=hostname1]').prop('disabled', false);

		//
		$('[name=ctrlport0], [name=ctrlport1]').val(param.controlPort);

		//
		$('[name=mgmtport0], [name=mgmtport1]').val(param.fxp0management);
		$('[name=fxpaddr0], [name=fxpaddr1]').prop('disabled', false);

		//
		$('#Fablics tbody tr:not(:first)').remove();
		$('#Fablics tfoot').hide();
		var ports = param.fabricPortNode0.split(',');
		$('[name=fabricport0]').empty().prop('disabled', false).append(
			$(ports).map(function(k, v)
			{ 
				return v.match(/^xe-/) ? '' : '<option>'+v+'</option>'; 
			}).get().join('')
		);
		$('[name=fabricport1]').val( getOffsetNode( ports.shift(), param.node1Offset ) );

		//
		var row = $('#Reths tbody tr:first-child').clone();
		row.find('select').prop('disabled', false);
		$('#Reths tbody').empty();
		for(var i = 0; i < parseInt(param.minReths); i++) 
		{
			row.find('th:nth-child(1)').text('reth'+i);
			row.find('select').empty().append(
				$(ports).map(function(k, v){ return '<option>'+v+'</option>'; }).get().join('')
			);
			row.find('input').val( getOffsetNode( ports.shift(), param.node1Offset ) );
			$('#Reths tbody').append(row.clone());
		}

		//
		$('#LogicalInterfaces').each(function(){
			$('select[name!=vlan]',this).prop('disabled', false);
			$('input[type!=text]', this).prop('disabled', false);
			$('select[name=ifl]',  this).prop('selectedIndex', 0);
			$('select[name=vlan]', this).empty();
			$('input[value=static]', this).click();
			$('input[name=ip]',    this).prop('disabled', false).val('');
			$('input[name=mask]',  this).prop('disabled', false).val('');
			$('tbody tr:nth-child(n+3)', this).remove();
		});

		updateLogicalInterfacesIFD();
		updateInterfacesZone();

		// ------------------------ cc disable
		
		$('[name=hostname]').val('');
		$('#VlanInterfaces tbody tr').remove();
		$('#LogicalInterfaces1 tbody tr').remove();
		$('#LogicalInterfaces2 tbody tr').remove();
	});


	$('input[name=cc]').click(function()
	{
		$('.ccenable')[parseInt(this.value) ? 'show' : 'hide']();
		$('.ccdisable')[parseInt(this.value) ? 'hide' : 'show']();
	});

	updateLogicalInterfacesIFD();

	// ------------------------------------------------------------

	$('#addFablics').click(function()
	{
		$('#Fablics tfoot').show();
		var value = $('#Fablics tbody tr:last-child select').val();
		var row = $('#Fablics tbody tr:last-child').clone();
		row.find('option:contains('+value+')').remove();
		var offset = data[$('[name=platform]').val()].node1Offset;
		row.find('input').val( getOffsetNode( row.find('select').val(), offset ) );
		$('#Fablics tbody').append(row);

		data[$('[name=platform]').val()].fabricPortNode0.split(',').length ==
			$('#Reths tbody tr select, #Fablics tbody tr select').length &&
			$('#addFablics, #addReths').hide();
		
		$('#Fablics tbody tr:last-child select').change();
	});

	$('#delFablics').click(function()
	{
		confirm('Remove this Fablic Port item?') &&
			$('#Fablics tbody tr:last-child').remove() &&
			$('#addFablics, #addReths').show();
		$('#Fablics tbody tr').length == 1 && $('#Fablics tfoot').hide();
		$('#Fablics tbody tr:first select').change();
	});

	// ------------------------------------------------------------

	$('#addReths').click(function()
	{
		$('#Reths tfoot').show();
		var value = $('#Reths tbody tr:last-child select').val();
		var row = $('#Reths tbody tr:last-child').clone();
		row.find('th').text('reth' + $('#Reths tbody tr').length);
		row.find('option:contains('+value+')').remove();
		var offset = data[$('[name=platform]').val()].node1Offset;
		row.find('input').val( getOffsetNode( row.find('select option:selected').text(), offset ) );
		$('#Reths tbody').append(row);
		
		$('#Reths tbody tr').length == 5 ||
		data[$('[name=platform]').val()].fabricPortNode0.split(',').length ==
			$('#Reths tbody tr select, #Fablics tbody tr select').length &&
		$('#addFablics, #addReths').hide();

		updateLogicalInterfacesIFD();
		checkInterfaceList();
		checkInterfaceListVlanId();
	});

	$('#delReths').click(function()
	{
		confirm('Remove this reth line item?') &&
			$('#Reths tbody tr:last-child').remove() &&
			$('#addFablics, #addReths').show();
		$('#Reths tbody tr').length == 2 && $('#Reths tfoot').hide();
		updateLogicalInterfacesIFD();
		checkInterfaceList();
		checkInterfaceListVlanId();
	});

	$(document).on('change','[name=fabricport0], #Reths select', function()
	{
		var ports = data[$('[name=platform]').val()].fabricPortNode0.split(',');
		var offset = data[$('[name=platform]').val()].node1Offset;
		$('[name=fabricport0], #Reths select').each(function()
		{
			var val = $(this).val();
			$(this).empty().append( $(ports).map(function(k, v){ return '<option>'+v+'</option>'; }).get().join('') );
			$.inArray(val, ports) != -1 && $(this).val(val);
			val = $(this).val();
			$(this).parent().parent().find('input').val( getOffsetNode( val, offset ) );
			ports = ports.filter(function(port){ return port != val; });
		});
	});
	
	// ------------------------------------------------------------
	
	$('#addSecurityZones').click(function()
	{
		var row = $('#SecurityZones tbody tr:last-child').clone();
		row.find('[type=text]').val('Name' + ($('#SecurityZones tbody tr').length + 1));
		row.find('[type=checkbox]').removeAttr('checked').removeAttr('disabled');
		row.find('td:last-child').html($('<a href="javascript:void(0);">X</a>').click(function()
		{
			confirm('Remove this Security Zone line item?') &&
			$(this).parent().parent().remove() &&
			updateInterfacesZone();
		}));
		$('#SecurityZones tbody').append(row);
		updateInterfacesZone();
	});

	$(document).on('keyup','#SecurityZones tr td:nth-child(1) input', function()
	{
		updateInterfacesZone();
	});

	$(document).on('click','#SecurityZones tr td:nth-child(2) input', function()
	{
		var flag = $(this).is(':checked');
		$(this).parent().parent().find('[type=checkbox]').each(function(k, v)
		{
			$(this).prop('checked', flag);
			k && $(this).attr('disabled', flag);
		});
	});
	
	// ------------------------------------------------------------
	$('#addLogicalInterfaces').click(function()
	{
		$('#LogicalInterfaces tbody tr').length > 9 &&
			alert('Limit for this tool is 10 Logical Interfaces') == undefined ||
		(function(){
			var row = $('#LogicalInterfaces tbody tr:last-child').clone();
			var count = $('#LogicalInterfaces tbody tr').length;
			var max =  $('#Reths tbody tr').length - 1;
			row.find('[name=ifd]').prop('selectedIndex', Math.min(max, count) );
			row.find('[name=vlan]').prop('disabled', row.find('[name=tag]').is('checked'));
			row.find('[name^=proto]').attr('name', 'proto' + count);
			row.find('[name=zonenames] option:nth-child(2)').prop('selected', true);
			$('#LogicalInterfaces tbody').append(row);
		})();
		checkInterfaceList();
		checkInterfaceListVlanId();
	});
	
	$(document).on('click','#LogicalInterfaces tr td:last-child a', function()
	{
		$('#LogicalInterfaces tbody tr').length < 3 &&
		alert("There must be 2 or more\nLogical Interface line items,\nbefore any one can be deleted.") == undefined ||
		confirm('Remove this Logical Interface line item?') &&
		$(this).parent().parent().remove();
		checkInterfaceList();
		checkInterfaceListVlanId();
	});
	
	$(document).on('change','#LogicalInterfaces select[name=ifd]', function()
	{
		checkInterfaceList();
		checkInterfaceListVlanId();
	});

	$(document).on('change','[name*=proto][type=radio]', function()
	{
		$(this).parent().find('[type=text]').prop('disabled', $(this).val() == 'dhcp').parent().removeClass('errorBgIpmask');
	});
	
	// ------
	
	$('#addVlanInterfaces').click(function()
	{
//		var row = $('#VlanInterfaces tbody tr:last-child').clone();
		var row = $('<tr>' +
						'<td class="text-center"><input type="text" name="ifd" value="vlan" style="width:90%" /></td>' +
						'<td class="text-center"><input type="text" name="vlan"></td>' +
						'<td class="text-center">' +
							'<input type="radio" name="vproto0" value="dhcp" /> DHCP ' +
							'<input type="radio" name="vproto0" value="static" checked /> STATIC' +
							'<br />' +
							'<input type="text" name="ip" title="e.g. 192.168.23.12" />' +
							' / ' +
							'<input type="text" name="mask" title="e.g. 24" />' +
						'</td>' +
						'<td class="text-center"><select name="zonenames" class="wFill"></select></td>' +
						'<td class="text-center"><a href="javascript:void(0);">X</a></td>' +
					'</tr>');

		row.find('[name=zonenames]').each(function() {
			var e = $(this);
			$('#SecurityZones [name^=securityZoneName]').each(function() {
				var v = $(this).val();
				e.append('<option>' + v + '</option>');
			})
		});
		
		row.find('[name=ifd] option:last').prop('selected', true);
		row.find('[name^=vproto]').attr('name', 'vproto' + $('#VlanInterfaces tbody tr').length);
		row.find('[name=zonenames] option:nth-child(2)').prop('selected', true);
		$('#VlanInterfaces tbody').append(row);
		updateLogicalInterfacesVlan();
		checkVlanInterfacesVlanId();
		checkVlanInterfacesIfd();
		checkVlanInterfacesVlanName();
		$('#addLogicalInterfaces1').show();
	});
	$(document).on('click','#VlanInterfaces tr td:last-child a', function()
	{
		confirm('Remove this Vlan Interface line item?') &&
		$(this).parent().parent().remove();
	
		if ($('#VlanInterfaces tbody tr').length == 0) {
			$('#LogicalInterfaces1 tbody tr').remove();
			$('#addLogicalInterfaces1').hide();
		}
		
		checkVlanInterfacesVlanId();
		checkVlanInterfacesIfd();
		checkVlanInterfacesVlanName();
	});

	$(document).on('keyup','#VlanInterfaces [name=ifd]', function()
	{
		checkVlanInterfacesVlanId();
		updateLogicalInterfacesVlan();
		checkVlanInterfacesVlanName();
	});
	$(document).on('change','#VlanInterfaces [name=vlan]', function()
	{
		checkVlanInterfacesVlanId();
		updateLogicalInterfacesVlan();
	});

	var setupIfdSelect = function(s) {
		var param = data[$('[name=platform]').val()];
		var list = param.fabricPortNode0.split(',');
		list.push(param.fxp0management);
		list.push(param.controlPort);
		s.empty().append(
				$(list).map( function(k, v)
				{ 
					return '<option>'+v+'</option>'; 
				}).get().join('')
		);
	}
	//
	$('#addLogicalInterfaces1').click(function()
	{
//		var row = $('#LogicalInterfaces1 tbody tr:last-child').clone();
		var row = $('<tr>' +
						'<td class="ifd text-center"><select name="ifd" class="wFill"><option>ge-0/0/1</option></select></td>' +
						'<td class="vlan text-center"><select name="vlan" class="wFill"><option value="1">vlan.0</option></select></td>' +
						'<td class="ip text-center"></td>' +
						'<td class="zone text-center"></td>' +
						'<td class="text-center"><a href="javascript:void(0);">X</a></td>' +
					'</tr>');
		setupIfdSelect(row.find('[name=ifd]'));
		row.find('[name=ifd] option:first').prop('selected', true);
		row.find('[name=zonenames] option:nth-child(1)').prop('selected', true);
		$('#LogicalInterfaces1 tbody').append(row);
		updateLogicalInterfacesVlan();
		checkLogicalInterfaceIfd();
	});
	$(document).on('click','#LogicalInterfaces1 tr td:last-child a', function()
	{
		confirm('Remove this Logical Interface line item?') &&
		$(this).parent().parent().remove();
		checkLogicalInterfaceIfd();
	});
	$(document).on('change','#LogicalInterfaces1 tr [name=ifd]', checkLogicalInterfaceIfd);

	//
	$('#addLogicalInterfaces2').click(function()
	{
//		var row = $('#LogicalInterfaces2 tbody tr:last-child').clone();
		var row = $('<tr>' +
						'<td class="text-center"><select name="ifd" class="wFill"><option>ge-0/0/1</option></select></td>' +
						'<td class="text-center"><input type="checkbox" name="tag" value="1" /></td>' +
						'<td class="text-center"><input type="text" name="vlan" disabled></td>' +
						'<td class="text-center">' +
							'<input type="radio" name="lproto0" value="dhcp" /> DHCP ' +
							'<input type="radio" name="lproto0" value="static" checked /> STATIC' +
							'<br />' +
							'<input type="text" name="ip" title="e.g. 192.168.23.12" />' +
							' / ' +
							'<input type="text" name="mask" title="e.g. 24" />' +
						'</td>' +
						'<td class="text-center"><select name="zonenames" class="wFill"></select></td>' +
						'<td class="text-center"><a href="javascript:void(0);">X</a></td>' +
					'</tr>');	
		setupIfdSelect(row.find('[name=ifd]'));
		row.find('[name=zonenames]').each(function() {
			var e = $(this);
			$('#SecurityZones [name^=securityZoneName]').each(function() {
				var v = $(this).val();
				e.append('<option>' + v + '</option>');
			})
		});

		row.find('[name=ifd] option:first').prop('selected', true);
		row.find('[name^=lproto]').attr('name', 'lproto' + $('#LogicalInterfaces2 tbody tr').length);
		row.find('[name=zonenames] option:nth-child(1)').prop('selected', true);
		$('#LogicalInterfaces2 tbody').append(row);
		checkLogicalInterfaceIfd();
		checkVlanInterfacesVlanId();
	});
	
	$(document).on('click','#LogicalInterfaces tr [name=tag]', function()
	{
		$(this).parent().parent().find('[name=vlan]').prop('disabled', !$(this).prop('checked'));
		checkInterfaceList();
		checkInterfaceListVlanId();
	});
	$(document).on('change','#LogicalInterfaces tr [name=vlan]', function(){
		checkInterfaceList();
		checkInterfaceListVlanId();
	});

	$(document).on('click','#LogicalInterfaces2 tr [name=tag]', function()
	{
		$(this).parent().parent().find('[name=vlan]').prop('disabled', !$(this).prop('checked'));
		checkLogicalInterfaceIfd();
		checkVlanInterfacesVlanId();
	});
	$(document).on('click','#LogicalInterfaces2 tr td:last-child a', function()
	{
		confirm('Remove this Logical Interface line item?') &&
		$(this).parent().parent().remove();
		checkLogicalInterfaceIfd();
		checkVlanInterfacesVlanId();
	});
	$(document).on('change','#LogicalInterfaces2 [name=vlan]', function(){
		checkLogicalInterfaceIfd();
		checkVlanInterfacesVlanId();
	});
	$(document).on('change','#LogicalInterfaces2 tr [name=ifd]', checkLogicalInterfaceIfd);

	// ------------------------------------------------------------
	
	$(document).on('blur', '[name=ip], [name=mask]', checkIpmask);
	
	// ------------------------------------------------------------
	
	function updateLogicalInterfacesIFD()
	{
		var objs = {};
		$('#Reths tbody tr').each(function(k){ objs['reth'+k] = []; });

		$('#LogicalInterfaces tbody tr [name=ifd]').each(function(i, v)
		{
			$(this).empty().append(
				$(Object.keys(objs)).map(function(k)
				{
					return '<option value="reth'+k+'">reth'+k+'</option>';
				}).get().join('')
			).prop('selectedIndex', i < Object.keys(objs).length ? i : 0);
		});
	}
/*	
	function checkInterfaceList()
	{
		var objs = {};
		$('#Reths tbody tr').each(function(k)
		{
			objs['reth'+k] = {elm: [], count:0, vlan:0}; 
		});

		$('#LogicalInterfaces tbody tr [name=ifd]').each(function(i, v)
		{
			objs[$(this).val()].elm.push(this);
			objs[$(this).val()].count++;
			objs[$(this).val()].vlan += $(this).parent().parent().find('[name=ifl]').val() == '.0';
		});
		
		$.each(objs, function(k, obj)
		{
			$(obj.elm)[obj.count > 1 && obj.vlan ? 'addClass' : 'removeClass']('error');
		});
		$('#LogicalInterfacesInfo')[ $('#LogicalInterfaces tbody tr [name=ifd]').hasClass('error') ? 'addClass' : 'removeClass' ]('error');
	}
	*/
	function updateInterfacesZone()
	{
		$('select[name=zonenames]').each(function()
		{
			var index = $(this).prop("selectedIndex");
			$(this).empty().append(
				$('#SecurityZones tbody tr [type=text]').map(function()
				{
					return '<option >'+this.value+'</option>';
				}).get().join('')
			).prop("selectedIndex", index);
		});
	}
	function updateLogicalInterfacesVlan()
	{
		$('#LogicalInterfaces1 select[name=vlan]').each(function()
		{
			var index = $(this).prop("selectedIndex");
			$(this).empty().append(
				$('#VlanInterfaces tbody tr').map(function()
				{
					var ifd = $('[name=ifd]', this).val();
					var vlan = $('[name=vlan]', this).val();
					return '<option value="' + ifd + '">' + ifd + '</option>';
				}).get().join('')
			).prop("selectedIndex", index);
		});
	}

	function checkInterfaceList() {
		var list = $('#LogicalInterfaces tbody tr').get();
		_checkLogicalInterfaceIfd(list, 'LogicalInterfaces');
	}

	function checkLogicalInterfaceIfd() {
		var list = $('#LogicalInterfaces1 tbody tr, #LogicalInterfaces2 tbody tr').get();
		_checkLogicalInterfaceIfd(list, 'LogicalInterfaces2');
	}
	
	function _checkLogicalInterfaceIfd(list, exceptId)
	{
//		var list = $('#LogicalInterfaces1 tbody tr, #LogicalInterfaces2 tbody tr').get();

		$(list).each(function(){ $('[name=ifd]', this).parent().removeClass('errorBgIfd'); });
		
		for (var i = 0; i < list.length - 1; i++)
		{
			var id = $(list[i]).parent().parent().attr('id');
			var element = $('[name=ifd]', list[i]);
			var parent = element.parent();
			var value = element.val();
			var vlan = $('[name=vlan]', list[i]).val();
			var tag = $('[name=tag]', list[i]).is(':checked');

			for (var j = i + 1; j < list.length; j++)
			{
				if( value == $('[name=ifd]', list[j]).val() ) {
					if( id == exceptId && $(list[j]).parent().parent().attr('id') == exceptId &&
						tag && $('[name=tag]', list[j]).is(':checked') && vlan != $('[name=vlan]', list[j]).val() ) {
						continue;
					}
					parent.addClass('errorBgIfd');
					$('[name=ifd]', list[j]).parent().addClass('errorBgIfd');
				}
			}
		}
	}
	
	function checkVlanInterfacesIfd()
	{
		$('#VlanInterfaces tbody tr').removeClass('errorBgIfd');

		$('#VlanInterfaces tbody tr').each(function(i){
			var ifd1 = $('[name=ifd]', this);
			
			$('#VlanInterfaces tbody tr').each(function(n){
				if (i != n) {
					var ifd2 = $('[name=ifd]', this);
					if( ifd1.val() == ifd2.val() ) {
						$([ifd1,ifd2]).parent().addClass('errorBgIfd');
					}
				}
			});
		});
	}

	function checkInterfaceListVlanId() {
		var list = $('#LogicalInterfaces tbody tr').get();
		_checkVlanInterfacesVlanId(list)
	}

	function checkVlanInterfacesVlanId() {
		var list = $('#VlanInterfaces tbody tr, #LogicalInterfaces2 tbody tr').get();
		_checkVlanInterfacesVlanId(list)
	}
	
	function _checkVlanInterfacesVlanId(list)
	{

		$(list).each(function(){ $('[name=vlan]', this).parent().removeClass('errorBgVlanId'); });

		for (var i = 0; i < list.length; i++)
		{
			var element = $('[name=vlan]', list[i]);
			var parent = element.parent();
			var value = Number(element.val());

			if( $('[name=tag]', list[i]).length == 0 || $('[name=tag]:checked', list[i]).length > 0 ) {
				if( isNaN(value) || value < 1 || value > 4095 ) {
					parent.addClass('errorBgVlanId');
				}

				for (var j = i; j < list.length; j++)
				{
					if( i == j ) continue;
					if( $('[name=tag]', list[j]).length == 0 || $('[name=tag]:checked', list[j]).length > 0 ) {
						if( value == Number($('[name=vlan]', list[j]).val()) ) {
							parent.addClass('errorBgVlanId');
							$('[name=vlan]', list[j]).parent().addClass('errorBgVlanId');
						}
					}
				}
			}
		}
	}
	function checkVlanInterfacesVlanName()
	{
		$('#VlanInterfaces tbody tr [name=ifd]').parent().removeClass('errorBgVlanName');

		$('#VlanInterfaces tbody tr').each(function(i){
			var ifd1 = $('[name=ifd]', this);
			
			if (ifd1.hasClass('errorBgVlanName'))
				return;

			$('#VlanInterfaces tbody tr').each(function(k){
				if (i != k) {
					var ifd2 = $('[name=ifd]', this);
					if( ifd1.val() == ifd2.val() ) {
						ifd1.parent().addClass('errorBgVlanName');
						ifd2.parent().addClass('errorBgVlanName');
					}
				}
			});
		});
	}
	function checkIpmask(){
		$(this).parent().removeClass('errorBgIpmask');
		($(this).parent().find('[name=ip]').val() == '' || 
		 $(this).parent().find('[name=mask]').val() == '' ) &&
		$(this).parent().addClass('errorBgIpmask');
	}
	function getOffsetNode(node, offset)
	{
		var p = node.split(/[-/]/);
		return p[0] + '-' + (parseInt(p[1]) + parseInt(offset)) + '/' + p[2] + '/' +  p[3];
	}

	// ------------------------------------------------------------
	$('#generate').click(function(){
		var config = [];
		var l2ng = (data[$('select[name=platform]').val()].l2ng === true) ? true : false;
		var vname = (l2ng) ? 'irb' : 'vlan';
		
		config.push('configure');
		config.push('delete');
		config.push('y');

		var pass = $('[name=passwd]').val();
	
		if (pass.length > 0) {
			routes.controllers.Api.md5crypt().ajax({async: false, data: {value: pass }}).done(function(resp) {
				if( resp && resp.result )
					config.push('set system root-authentication encrypted-password ' + resp.result);
			});
		}
		
		if( $('[name=logtype]:checked').val() == 'event' ) {
			config.push('set system syslog file sessions any any');
			config.push('set system syslog file sessions match RT_FLOW_SESSION');
		}
		else {
			config.push('set security log mode stream');
		}

		if( $('[name=cc]:checked').val() == '1' ) {
			//config.push('set chassis cluster cluster-id 1 node 0');
			//config.push('request system reboot');
			//config.push('y');
			//config.push('set chassis cluster cluster-id 1 node 1');
			//config.push('request system reboot');
			//config.push('y');

			config.push('set groups node0 system host-name ' + $('[name=hostname0]').val());
			if( $('[name=fxpaddr0]').val() )
				config.push('set groups node0 interfaces fxp0 unit 0 family inet address ' + $('[name=fxpaddr0]').val());
			config.push('set groups node1 system host-name ' + $('[name=hostname1]').val());
			if( $('[name=fxpaddr1]').val() )
				config.push('set groups node1 interfaces fxp0 unit 0 family inet address ' + $('[name=fxpaddr1]').val());

			config.push('set apply-groups "${node}"');
			config.push('set chassis cluster reth-count ' + $('#Reths tbody tr').length);
			config.push('set chassis cluster redundancy-group 0 node 0 priority 200');
			config.push('set chassis cluster redundancy-group 0 node 1 priority 100');
			config.push('set chassis cluster redundancy-group 1 node 0 priority 200');
			config.push('set chassis cluster redundancy-group 1 node 1 priority 100');
			
			$('#Fablics tbody tr').each(function(){
				config.push('set interfaces fab0 fabric-options member-interfaces ' + $('[name=fabricport0]', this).val());
				config.push('set interfaces fab1 fabric-options member-interfaces ' + $('[name=fabricport1]', this).val());
			});

			$('#Reths tbody tr').each(function(i){
				var itf = $('select',this).val();
				if (itf.match(/^ge-/) || itf.match(/^xe-/))
					config.push('set interfaces ' + itf + ' gigether-options redundant-parent reth' + i);
				else
					config.push('set interfaces ' + itf + ' fastether-options redundant-parent reth' + i);
				itf = $('input',this).val();
				if (itf.match(/^ge-/) || itf.match(/^xe-/))
					config.push('set interfaces ' + itf + ' gigether-options redundant-parent reth' + i);
				else
					config.push('set interfaces ' + itf + ' fastether-options redundant-parent reth' + i);
			});
			$('#Reths tbody tr').each(function(i){
				config.push('set interfaces reth' + i + ' redundant-ether-options redundancy-group 1');
			});
			$('#SecurityZones tbody tr').each(function(i){
				config.push('set security zones security-zone ' + $('[type=text]', this).val());
			});
			$('#SecurityZones tbody tr').each(function(){
				var name = $('[type=text]', this).val();
				$('[type=checkbox]:checked', this).each(function(){
					config.push('set security zones security-zone ' + name + ' host-inbound-traffic system-services ' + $(this).val() );
					if( $(this).val() == 'all' ) return false;
				});
			});
			var ifds = {};
			$('#LogicalInterfaces tbody tr').each(function(i){
				var ifd = $('[name=ifd]', this).val();
				var tag = $('[name=tag]', this).is(':checked');
				var vlan = tag ? $('[name=vlan]', this).val() : 0;
				var zone = $('[name=zonenames]', this).val();
				var proto = $('[name^=proto]:checked', this).val();
				
				config.push('set security zones security-zone ' + zone + ' interfaces ' + ifd + '.' + vlan );
				if ( proto == 'dhcp') {
					config.push('set security zones security-zone ' + zone + ' interfaces ' + ifd + '.' + vlan + ' host-inbound-traffic system-services dhcp');
				}
				
				if( tag && !ifds[ifd] ) 
				{
					ifds[ifd] = true;
					config.push('set interfaces ' + ifd + ' vlan-tagging');
					config.push('set interfaces ' + ifd + ' unit ' + vlan + ' vlan-id ' + vlan);
				}

				if( proto == 'dhcp' ) {
					config.push('set interfaces ' + ifd + ' unit ' + vlan + ' family inet dhcp');
				}
				else if( proto == 'static' ) {
					var ipmask = $('[name=ip]', this).val() +'/'+ $('[name=mask]', this).val();
					ipmask = (ipmask.length > 1) ? ' address ' + ipmask : '';
					config.push('set interfaces ' + ifd + ' unit ' + vlan + ' family inet' + ipmask);
				}
			});

		} 
		else if( $('[name=cc]:checked').val() == '0' ) 
		{
			var hostname = $('[name=hostname]').val();
			if (hostname.length > 0)
				config.push('set system host-name ' + hostname);

			$('#SecurityZones tbody tr').each(function(i){
				config.push('set security zones security-zone ' + $('[type=text]', this).val());
			});
			$('#SecurityZones tbody tr').each(function()
			{
				var name =  $('[type=text]', this).val();
				$('[type=checkbox]:checked', this).each(function()
				{
					config.push('set security zones security-zone ' + name + ' host-inbound-traffic system-services ' + $(this).val() );
					if( $(this).val() == 'all' ) return false;
				});
			});
			
			$('#VlanInterfaces tbody tr').each(function()
			{
				var ifd = $('[name=ifd]', this).val();
				var vlan = $('[name=vlan]', this).val();
				config.push('set vlans ' + ifd + ' vlan-id ' + vlan );
				config.push('set vlans ' + ifd + ' l3-interface ' + vname + '.' + vlan );
			});
			$('#VlanInterfaces tbody tr').each(function()
			{
				var ifd = $('[name=ifd]', this).val();
				var vlan = $('[name=vlan]', this).val()
				var proto = $('[name^=vproto]:checked', this).val();
				if( proto == 'dhcp' ) 
				{
					config.push('set interfaces ' + vname + ' unit ' + vlan + ' family inet dhcp');
				}
				else if( proto == 'static' )
				{
					var ipmask = $('[name=ip]', this).val() + '/' + $('[name=mask]', this).val();
					ipmask = (ipmask.length > 1) ? ' address ' + ipmask : '';
					config.push('set interfaces ' + vname + ' unit ' + vlan + ' family inet' + ipmask);
				}
				var zone = $('[name=zonenames]', this).val();
				config.push('set security zones security-zone ' + zone + ' interfaces ' + vname + '.' + vlan);
				if ( proto == 'dhcp') {
					config.push('set security zones security-zone ' + zone + ' interfaces ' + vname + '.' + vlan + ' host-inbound-traffic system-services dhcp');
				}

			});
			
			$('#LogicalInterfaces1 tbody tr').each(function()
			{
				var ifd = $('[name=ifd]', this).val();
				var vlan = $('[name=vlan]', this).val(); 
				config.push('set interfaces ' + ifd + ' unit 0 family ethernet-switching vlan members ' + vlan );
			});

			var ifds = {};
			$('#LogicalInterfaces2 tbody tr').each(function()
			{
				var ifd = $('[name=ifd]', this).val();
				var tag = $('[name=tag]', this).is(':checked');
				var vlan = tag ? $('[name=vlan]', this).val() : 0;
				var zone = $('[name=zonenames]', this).val();
				var proto = $('[name^=lproto]:checked', this).val();

				config.push('set security zones security-zone ' + zone + ' interfaces ' + ifd + '.' + vlan);
				if ( proto == 'dhcp') {
					config.push('set security zones security-zone ' + zone + ' interfaces ' + ifd + '.' + vlan + ' host-inbound-traffic system-services dhcp');
				}

				if( tag && !ifds[ifd] ) 
				{
					ifds[ifd] = true;
					config.push('set interfaces ' + ifd + ' vlan-tagging');
					config.push('set interfaces ' + ifd + ' unit ' + vlan + ' vlan-id ' + vlan);
				}

				if( proto == 'dhcp' )
				{
					config.push('set interfaces ' + ifd + ' unit ' + vlan + ' family inet dhcp');
				}
				else if( proto == 'static' ) 
				{
					var ipmask = $('[name=ip]', this).val() + '/' + $('[name=mask]', this).val();
					ipmask = (ipmask.length > 1) ? ' address ' + ipmask : '';
					config.push('set interfaces ' + ifd + ' unit ' + vlan + ' family inet' + ipmask);
				}
			});
		}

		config.push('set system services ssh protocol-version v2');
		config.push('set system services netconf ssh');

		config.push('commit');
		config.push('exit');
		config.push('request system configuration rescue save');

		$('#config').val(config.join("\n")).height($('#config').prop('scrollHeight'));
	});
});