(function(d, $) {
	$(function() {
		var e = $('#ref_time');
		var name = e.data('device');
		var intervalId = null,
			interval = e.data('interval');
		if (interval == "") interval = 0;

		e.change(function() {
			var val = $(this).val() ;
			clearInterval(intervalId);
			if (val != 0) {
				intervalId = setTimeout(function() {
					location.href = routes.controllers.Home.index(name, "true", val).url;
				}, val);
			}
		}).val(interval).change();
		
		 $( "#user-dialog" ).dialog({
			 autoOpen: false,
			 modal: true,
			 width: 700,
			 height: 500,
			 buttons: {
				 Close: function() { $(this).dialog('close')}
			 }
		 });	
		 
		$('#user-dialog-btn').click(function(evt) {
			$('#user-dialog').dialog('open');
			return false;
		});
		
		$('#refresh-btn').each(function() {
			var e = $(this);
			if (e.data('refreshed') == 1) {
				e.prop('disabled', true);
				setTimeout(function() { e.prop('disabled', false); }, 60000);
			}
		});
	});
})(document, jQuery);