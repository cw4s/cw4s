(function($) {
	var multiValue = "(Multiple)";
	var modal = null;
	
	var multiple = function(s, l) {
		var o = s.find("option:contains(" + multiValue + ")");
		
		if (l > 1) {
			if (o.length === 0) {
				o = $('<option value="">').text(multiValue);
				o.insertBefore(s.find("option:first"));
			}
			o.prop('selected', true);
		}
		else {
			if (o.length !== 0) {
				o.remove();
			}
		}
	}
	
	var update = function(h, s) {
		var vs = h.val().split(',');
		multiple(s, vs.length);
		if (vs.length == 1) {
			s.val(vs[0]);
		}
	};

	var showModal = function(url, callback) {
		if (modal === null) {
			modal = $('<div class="modal fade" data-width="800" data-height="600">')
			$('body').append(modal);	
		}
	
		modal.empty();
		$.ajax(url)
        .done(function(data) {
        	modal.append(data);
        	var okcb = callback(modal);
        	modal.dialog({
        		'modal': true,
        		height: 800,
        		width: 600,
        		buttons: {
        			"Ok": function() {
        				okcb($(this));
        			},
        			"Cancel": function() {
        				$(this).dialog('close');
        			}
        		},
        		close: function() {
        			$(this).empty();
        		}
        	});
        	modal.dialog('open');	
        })
	};

	var cb = function(h, s) {
		
		return function(m) {
			var ss = m.find('select.selected');
			var as = m.find('select.available');

			var vs = h.val().split(',');
			as.find('option').each(function() {
				var e = $(this);
                var v = e.val();
                if (v && v.length > 0 && $.inArray(v, vs) >= 0)
                	ss.append(e);
			});	
			
			m.find('button.removeBtn').click(function(evt) {
				ss.find('option:selected').each(function() {
                    var e = $(this);
                    var v = e.val();
                    if (v && v.length > 0)
                    	as.append(e);
				})
				return false;
			});
			
			m.find('button.addBtn').click(function(evt) {
				as.find('option:selected').each(function() {
                    var e = $(this);
                    var v = e.val();
                    if (v && v.length > 0)
                    	ss.append(e);
				})
				return false;
			});		

			return function(e) {
				var vs = [];
				ss.find('option').each(function() {
                    var v = $(this).val();
                    if (v && v.length > 0)
                    	vs.push(v);
				});
				
				h.val(vs.join(","));
				update(h, s);
				modal.dialog('close');
			};
		};
	}

	var change = function(h) {
		return function(evt) {
			var e = $(this);
			var v = e.val();
			if (v && v.length > 0) {
				h.val(v);
				update(h, e);
			}
		}
	}
	
	$(function() {
		var src1 = $('#src1');
		var dst1 = $('#dst1');
		var app1 = $('#app1');
		
		var src = $('#src');
		var dst = $('#dst');
		var app = $('#app');
		
		update(src, src1);
		update(dst, dst1);
		update(app, app1);
	
		src1.change(change(src));
		dst1.change(change(dst));
		app1.change(change(app));
		
		$('#srcbtn').click(function(evt) {
			var e = $(this);
			showModal(routes.controllers.Modal.sourceSelector(e.data("device"), e.data("zone")), cb(src, src1));
			return false;
		})
		
		$('#dstbtn').click(function(evt) {
			var e = $(this);
			showModal(routes.controllers.Modal.destinationSelector(e.data("device"), e.data("zone")), cb(dst, dst1));
			return false;
		})
		
		$('#appbtn').click(function(evt) {
			var e = $(this);
			showModal(routes.controllers.Modal.applicationSelector(e.data("device")), cb(app, app1));
			return false;
		})
	});
})(jQuery)