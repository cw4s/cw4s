/**
 * 
 */
(function($) {

	$(function() {
		$('a.moveBtn').click(function(evt) {
			var e = $(this);
			var f = e.parents('form');
			f.find('input[name=before]').val(e.data('before'));
			f.submit();
			return false;
		});
	});
})(jQuery);