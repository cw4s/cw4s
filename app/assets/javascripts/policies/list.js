(function($) {
	var modal = null;

	var showModal = function(url) {
        if (modal === null) {
        	modal = $('<div class="modal fade">');
            $('body').append(modal);
        }

        modal.empty();
        $.ajax(url)
        .done(function(data) {
        	modal.append(data);
        	modal.dialog({
        		'modal': true,
        		buttons: {
        			"Ok": function() {
        				$(this).find("form").submit();
        			},
        			"Cancel": function() {
        				$(this).dialog('close');
        			}
        		},
        		close: function() {
        			$(this).empty();
        		}
        	});
        	modal.dialog('open');
        })
	};
	
	$(function() {
		$('a.moveBtn').click(function(evt) {
			var e = $(this);
			showModal(routes.controllers.Policies.moveModal(e.data("device"), e.data("from"), e.data("to"), e.data("name")));
			return false;
		});
		
		$('input[name=op]').click(function(evt) {
			if ($('#from').val() == "" || $('#to').val() == "") { 
				alert("You cannot specify \"All zones\" for a new policy.");
				return false;
			}
			else if ($('#from').val() == "junos-host" && $('#to').val() == "junos-host") { 
				alert("from-zone and to-zone are both junos-host zone.");
				return false;
			}
			return true;
		});
		
		$('input.enableCheck').click(function(evt) {
			var e = $(this);
			var checked = e.prop('checked');

			routes.controllers.Api.enablePolicy(e.data("device"), e.data("from"), e.data("to"), e.data("name"), checked)
			.ajax()
			.done(function(data) {
				if (data.result)
					e.prop('checked', checked);
				window.location.reload();
			});
			return false;
		})
	})
	
})(jQuery)
