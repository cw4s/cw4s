/**
 * 
 */
(function($) {
	$('#syncButton').click(function(evt) {
		if (window.confirm('Do you want to sync configuration from device ?')) {
			routes.controllers.Api.sync($(this).data("name")).ajax()
			.done(function(data) {
				if (data.result) {
					alert("done.");
					setTimeout(window.location.reload(), 500);
				} 
				else {
					alert(data.error);
				}		  
			})
			.fail(function(data) {
				alert("failed."); 
			});
		}
		return false;
	});
	
	$('#commitButton').click(function(evt) {
		if (window.confirm('Do you want to commit editing configuration to device ?')) {
			routes.controllers.Api.commit($(this).data("name")).ajax()
			.done(function(data) {
				if (data.result) {
					alert("done.");
					setTimeout(function() { window.location.reload() }, 500);
				} 
				else {
					var message = '';

					try {
						var rpcerror = $('rpc-error', data.error);
						if (rpcerror.length > 0) {
							rpcerror.each(function(){
								message += '<b>severity:</b> ' + $(this).find('error-severity').text() + '<br />'
								+ '<b>path:</b> ' + $(this).find('error-path').text() + '<br />'
								+ '<b>element:</b> ' + $(this).find('bad-element').text() + '<br />'
								+ '<b>message:</b> ' + $(this).find('error-message').text() + '<br /><br />';
						
							});
						}
						else {
							message = data.error;
						}
						var modal = $('<div class="modal fade">')
						$('body').append(modal);
						modal.append(message);
						modal.dialog({
							'modal': true,
							width: 600,
							height: 500,
							buttons: {
								"Ok": function() {
									$(this).dialog('close');
								}
							}
						});
						modal.dialog('open');
					} catch (e){
						alert(data.error);
					}
				}
			})
			.fail(function(data) {
				alert("failed."); 
			});
		}
		return false;
	});
		
	$('#rollbackButton').click(function(evt) {
		var num = $('#rollbackNumberInput').val();
		if (isNaN(num)) {
			alert("Invalid rollback number.");
			return false;
		}
		
		if (window.confirm('Do you want to rollback configuration ?')) {
			routes.controllers.Api.rollback($(this).data("name"), num).ajax()
			.done(function(data) {
				if (data.result) {
					alert("done.");
					setInterval(window.location.reload(), 500);
				} 
				else {
					var message = '';
					$('rpc-error', data.error).each(function(){
						message += '<b>severity:</b> ' + $(this).find('error-severity').text() + '<br />'
								+ '<b>path:</b> ' + $(this).find('error-path').text() + '<br />'
								+ '<b>element:</b> ' + $(this).find('bad-element').text() + '<br />'
								+ '<b>message:</b> ' + $(this).find('error-message').text() + '<br /><br />';
						
					});
					var modal = $('<div class="modal fade">')
					$('body').append(modal);
					modal.append(message);
					modal.dialog({
						'modal': true,
						width: 600,
						height: 500,
						buttons: {
							"Ok": function() {
								$(this).dialog('close');
							}
						}
					});
					modal.dialog('open');				
				}
			})
			.fail(function(data) {
				alert("failed."); 
			});
		}
		return false;
	});
})(jQuery);