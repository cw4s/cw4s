/**
 * 
 */

(function(d, $) {
	$(d).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$(function() {
		$('.nav-tree').jstree({
			"core" : {
				"themes" : {
					"responsive" : false
				}
			},
			"plugins" : ["state"]
		})
		.bind("ready.jstree", function(e, data) {
			$(this).bind("select_node.jstree", function (e, data) {
				if (data.node.a_attr.href) {
					$(this).jstree(true).save_state();
					d.location.href = data.node.a_attr.href;
				}
			});
			$(this).on('click', 'a.jstree-anchor',  function(){
				$(this).parent().find('> i.jstree-ocl').click();
			});
		});
	});
})(document, jQuery);
