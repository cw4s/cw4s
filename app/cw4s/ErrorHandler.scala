package cw4s

import javax.inject._

import play.api.http.DefaultHttpErrorHandler
import play.api.{Environment,Configuration,OptionalSourceMapper}
import play.api.mvc.RequestHeader
import play.api.mvc.Results.Redirect
import play.api.routing.Router
import scala.concurrent._

class ErrorHandler @Inject() (
    env: Environment,
    config: Configuration,
    sourceMapper: OptionalSourceMapper,
    router: Provider[Router]
  ) extends DefaultHttpErrorHandler(env, config, sourceMapper, router) {

  override def onServerError(request: RequestHeader, exception: Throwable) = exception match {
		  case models.DeviceNotFoundException(v) => Future.successful(Redirect(controllers.routes.DeviceSetup.list))
		  case _ => super.onServerError(request, exception)
	}
}