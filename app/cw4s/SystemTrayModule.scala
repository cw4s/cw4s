package cw4s

import javax.inject._
import play.api.{ Configuration, Environment }
import java.awt.Desktop
import java.awt.SystemTray
import java.awt.TrayIcon
import java.awt.PopupMenu
import java.awt.MenuItem
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.imageio.ImageIO
import java.io.File
import java.net.URI
import com.google.inject.AbstractModule

trait SystemTrayManager

class SystemTrayManagerImpl @Inject()(env: Environment) extends SystemTrayManager {
  
 	val menu = new PopupMenu
	val icon = {
 	  val path = env.resource(List("public", "images", "favicon.png").mkString("/"))
	  new TrayIcon(ImageIO.read(path.get), "cw4s", menu)
	}

 	onStart
 	
	def onStart = {
	  menu.removeAll
	  val itemOpen = new MenuItem("open")
	  itemOpen.addActionListener(new ActionListener() {
		  def actionPerformed(event:ActionEvent):Unit = {
		    val desktop = Desktop.getDesktop
		    desktop.browse(new URI("http://localhost:" + Option(System.getProperty("http.port")).getOrElse("9000") + controllers.routes.DeviceSetup.list.url))
		  }	
	  })

	  val itemExit = new MenuItem("exit")
	  itemExit.addActionListener(new ActionListener() {
		  def actionPerformed(event:ActionEvent):Unit = {
			  System.exit(0)
		  }
	  })
	  menu.add(itemOpen)
	  menu.add(itemExit)
	 
	  try {
		  SystemTray.getSystemTray.add(icon)
	  }
	  catch {
	    case _:Throwable =>
	  }
	}
}

class SystemTrayModule extends AbstractModule {
  override def configure(): Unit =  {
    bind(classOf[SystemTrayManager]).to(classOf[SystemTrayManagerImpl]).asEagerSingleton()
  }
}