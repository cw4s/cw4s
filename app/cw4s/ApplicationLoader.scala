package cw4s

import play.api.ApplicationLoader
import play.api.Configuration
import play.api.inject._
import play.api.inject.guice._

import java.io.File

class Cw4sApplicationLoader extends GuiceApplicationLoader() {
  override def builder(context: ApplicationLoader.Context): GuiceApplicationBuilder = {
    val evo  = if (context.environment.mode == play.api.Mode.Prod) "true" else "false"
    val path = to.lef.srxlib.OSUtil.getAppDataPath("cw4s")
    val dir  = new File(path)

    if (!dir.isDirectory)
      dir.mkdirs

    java.lang.System.setProperty("user.dir", path)
		val extra = Configuration.from(Map(
		    "user.dir" -> path,
		    "slick.dbs.default.db.url" -> s"jdbc:sqlite:${path}/cw4s.db",
		    "play.evolutions.db.default.autoApply" -> evo
		    ))
   
    initialBuilder
      .in(context.environment)
      .loadConfig(context.initialConfiguration ++ extra)
      .overrides(overrides(context): _*)
  }
}