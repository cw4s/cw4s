package cw4s

import play.api.data.Forms._
import play.api.data.validation._

import to.lef.srxlib
import to.lef.srxlib.Util

object Validations {
	object Constraints {
		val identifier:Constraint[String] = Constraint("constraints.identifier") { o =>
			if (Util.isValidIdentifier(o)) Valid else Invalid(ValidationError("Invalid identifer"))
		}
	
		val addressName:Constraint[String] = Constraint("constraints.address.name") { o =>
		  if (o == "any" || o == "any-ipv4" || o == "any-ipv6") Invalid(ValidationError("Invalid address name.")) else Valid
		}
		
		val addressSetName:Constraint[String] = Constraint("constraints.addressSet.name") { o =>
		  if (o == "any" || o == "any-ipv4" || o == "any-ipv6") Invalid(ValidationError("Invalid address set name.")) else Valid
		}
		
		def policyName(device:srxlib.Device, from:String, to:String, oldName:String):Constraint[String] = Constraint("constraints.policy.name") { o =>
			if (oldName == o || !device.config.isPolicyExist(from, to, o)) Valid else Invalid(ValidationError("Policy is alraedy exist."))
		}

		def numOrZero[T](min:T, max:T)(implicit ordering: scala.math.Ordering[T]):Constraint[T] = Constraint("constraints.numOrZero") { o => 
			o match {
			  case 0 => Valid
			  case _ if (ordering.compare(o, min) < 0) => Invalid(ValidationError("0 or %d < value < %d".format(min, max)))
			  case _ if (ordering.compare(o, max) > 0) => Invalid(ValidationError("0 or %d < value < %d".format(min, max)))
			  case _ => Valid
			}
		}
	}
	
	def idText = text verifying Constraints.identifier
	def policyName(device:srxlib.Device, from:String, to:String, oldName:String) = idText verifying Constraints.policyName(device, from, to, oldName) 
	def addressName = idText verifying Constraints.addressName
	def addressSetName = idText verifying Constraints.addressSetName
	
	def numberOrZero(min:Int, max:Int) = number verifying Constraints.numOrZero(min, max)
	def longNumberOrZero(min:Long, max:Long) = longNumber verifying Constraints.numOrZero(min, max)
}