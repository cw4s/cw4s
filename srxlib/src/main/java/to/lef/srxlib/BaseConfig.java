package to.lef.srxlib;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import to.lef.srxlib.dom.XML;
import to.lef.srxlib.policy.ApplicationElement;
import to.lef.srxlib.policy.ApplicationSet;

public class BaseConfig extends XML {
	public static final String FORWARDING_INET6_OPTION_DROP = "drop";
	public static final String FORWARDING_INET6_OPTION_FLOW = "flow-based";
	public static final String FORWARDING_INET6_OPTION_PACKET = "packet-based";
	
	protected BaseConfig(Document doc, Element root) {
		super(doc, root);
	}
	
	public List<ApplicationElement> applicationList() {
		List<ApplicationElement> ret = new ArrayList<ApplicationElement>();
		
		final NodeList applications = getNodeList("applications/application");
		final int nl = applications.getLength();
		
		for (int i = 0; i < nl; i++) {
			ApplicationElement a = ApplicationElement.createFromNode(applications.item(i), this);
			if (a != null)
				ret.add(a);
		}
		
		return ret;
	}
	
	public List<String> applicationNameList() {
		List<String> ret = new ArrayList<String>();
		
		final NodeList applications = getNodeList("applications/application");
		final int nl = applications.getLength();
		
		for (int i = 0; i < nl; i++) {
			String s = getString(applications.item(i), "name/text()");
			if (StringUtils.isNotBlank(s))
				ret.add(s);
		}
		
		return ret;
	}

	public Node findApplicationNode(Node applications, String name) {
		return getNode(applications, "application/name[normalize-space(text())=%s]/..", xpc(name));
	}
	
	public Node findApplicationNode(String name) {
		return getNode("applications/application/name[normalize-space(text())=%s]/..", xpc(name));
	}

	public Node findApplicationTermNode(Node app, String name) {
		return getNode(app, "term/name[normalize-space(text())=%s]/..", xpc(name));
	}
	
	public boolean isApplicationExist(String name) {
		Node a = findApplicationNode(name);
		return (a != null);
	}
	
	public ApplicationElement findApplication(String name) {
		Node a = findApplicationNode(name);
		if (a != null)
			return ApplicationElement.createFromNode(a, this);
		return null;
	}
	
	public Node findApplicationSetNode(Node applications, String name) {
		return getNode(applications, "application-set/name[normalize-space(text())=%s]/..", xpc(name));
	}
	
	public Node findApplicationSetNode(String name) {
		return getNode("applications/application-set/name[normalize-space(text())=%s]/..", xpc(name));
	}
	
	public boolean isApplicationSetExist(String name) {
		Node a = findApplicationSetNode(name);
		return (a != null);
	}

	public ApplicationSet findApplicationSet(String name) {
		Node a = findApplicationSetNode(name);
		if (a != null)
			return ApplicationSet.createFromNode(a, this);
		return null;
	}

	public List<ApplicationSet> applicationSetList() {
		List<ApplicationSet> ret = new ArrayList<ApplicationSet>();
		
		final NodeList applications = getNodeList("applications/application-set");
		final int nl = applications.getLength();
		
		for (int i = 0; i < nl; i++) {
			ApplicationSet a = ApplicationSet.createFromNode(applications.item(i), this);
			if (a != null)
				ret.add(a);
		}
		
		return ret;	
	}
	
	public List<String> applicationSetNameList() {
		List<String> ret = new ArrayList<String>();
		
		final NodeList applications = getNodeList("applications/application-set");
		final int nl = applications.getLength();
		
		for (int i = 0; i < nl; i++) {
			String s = getString(applications.item(i), "name/text()");
			if (StringUtils.isNotBlank(s))
				ret.add(s);
		}
		
		return ret;	
	}
	
	public boolean isApplicationOrApplicationSetExist(String name) {
		return (isApplicationExist(name) || isApplicationSetExist(name));
	}
	
	public String getForwardingInet6Option() {
		Node mode = getNode("security/forwarding-options/family/inet6/mode");
		if (mode == null)
			return FORWARDING_INET6_OPTION_DROP;
		
		return getString(mode, "text()");
	}
	
	public boolean isInet6FlowEnabled() {
		return FORWARDING_INET6_OPTION_FLOW.equals(getForwardingInet6Option());
	}
}