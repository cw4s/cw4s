package to.lef.srxlib.policy;


import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.dom.XML;

public class ApplicationTerm extends ApplicationBase {
	private String alg;

	public static ApplicationTerm createCustomInstance(
			String name, String protocol, 
			String srcLowPort, String srcHighPort,
			String dstLowPort, String dstHighPort,
			String icmpType, String icmpCode,
			String alg) {
		ApplicationTerm ret = new ApplicationTerm(name);
		ret.setProtocol(protocol);
		ret.setSrcPort(srcLowPort, srcHighPort);
		ret.setDstPort(dstLowPort, dstHighPort);
		ret.setIcmpType(icmpType);
		ret.setIcmpCode(icmpCode);
		ret.setAlg(alg);
		return ret;
	}
	
	public static ApplicationTerm createFromNode(final Node n, final XML cfg) {
		final String name = cfg.getString(n, "name/text()");
		
		if (StringUtils.isBlank(name))
			return null;
		
		ApplicationTerm o = new ApplicationTerm(name);
		o.constructFromNode(n, cfg);
		
		return o;
	}
	
	public ApplicationTerm(String name) {
		super(name);
	}

	public final void setName(String name) {
		this.name = name;
	}
	
	public final String getAlg() {
		return alg;
	}

	public final void setAlg(String alg) {
		this.alg = alg;
	}	
	
	@Override
	protected void constructFromNode(final Node n, final XML cfg) {
		super.constructFromNode(n, cfg);
		
		setAlg(cfg.getString(n, "alg/text()"));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((alg == null) ? 0 : alg.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplicationTerm other = (ApplicationTerm) obj;
		if (StringUtils.isBlank(alg)) {
			if (StringUtils.isNotBlank(other.alg))
				return false;
		} else if (!alg.equals(other.alg))
			return false;
		return true;
	}

	@Override
	public boolean configCustom(Node a, DeviceConfig cfg) { 
		boolean ret = super.configCustom(a, cfg);
		if (ret) {
			if (StringUtils.isNotBlank(alg)) {
				a.appendChild(cfg.createElement("alg", alg));
			}
		}
		return ret;
	}
	

}
