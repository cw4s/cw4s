package to.lef.srxlib.policy;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.IPAddress;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class AddressElement {
	public final static int ADDRESS_TYPE_IP_PREFIX    = 1;
	public final static int ADDRESS_TYPE_DNS_NAME = 2;

	private final String name;
	private final String zoneName;
	private int addressType;
	private String dnsName;
	private IPAddress address;
	/*
	private final String address;
	private final int netmask;
	*/
	private String description;
	
	public static AddressElement createFromNode(String zoneName, Node address, DeviceConfig config) {
		AddressElement ret = null;
		
		final String name = config.getString(address, "name/text()");
		String addr = config.getString(address, "dns-name/name/text()");
		if (StringUtils.isNotBlank(addr)) {
			ret = new AddressElement(zoneName, name, addr);
		}
		else {
			addr = config.getString(address, "ip-prefix/text()");
			if (StringUtils.isNotBlank(addr)) {
				String[] s = addr.split("\\/");
				ret = new AddressElement(zoneName, name, s[0], Integer.parseInt(s[1]));
			}
		}
		
		if (ret != null) {
			ret.setDescription(config.getString(address, "description/text()"));
		}
		return ret;
	}
	
	private AddressElement(String zoneName, String name, int addressType) {
		super();
		this.name = name;
		this.zoneName = zoneName;
		this.addressType = addressType;
			this.address = null;
		this.dnsName = null;	
	}
	
	public	AddressElement(String zoneName, String name, IPAddress address) {
		this(zoneName, name, ADDRESS_TYPE_IP_PREFIX);
		this.address = address;
	}
	
	public	AddressElement(String zoneName, String name, String address, int mask) {
		this(zoneName, name, new IPAddress(address, mask));
	}
	
	public 	AddressElement(String zoneName, String name, String address) {
		this(zoneName, name, ADDRESS_TYPE_DNS_NAME);
		this.dnsName = address;
	}

	public final String getName() {
		return name;
	}

	public final String getZoneName() {
		return zoneName;
	}
	
	public final int getAddressType() {
		return addressType;
	}
	
	public final String getAddress() {
		return (addressType == ADDRESS_TYPE_IP_PREFIX) ? address.getAddress() : dnsName;
	}
	
	public final int getNetmask() {
		if (addressType != ADDRESS_TYPE_IP_PREFIX) 
			return 0;
		return address.getNetmask();
	}

	public final String getDescription() {
		return description;
	}

	public final void setDescription(String description) {
		this.description = description;
	}
	
	public final String getDisplayAddress() {
		return (addressType == ADDRESS_TYPE_IP_PREFIX) ? address.toString() : dnsName;
	}

	public final boolean hasIPv6Address() {
		return (addressType == ADDRESS_TYPE_IP_PREFIX && address.isIPv6Address());
	}
	
	public void configAddress(Node addr, DeviceConfig cfg) {
		{
			Node n = cfg.createElementsIfNotExist(addr, "name");
			XML.removeAllChildNodes(n);
			n.appendChild(cfg.createTextNode(name));
		}

		cfg.deleteNode(addr, "description");
		if (StringUtils.isNotBlank(description)) {
			addr.appendChild(cfg.createElement("description", description));
		}
		
		cfg.deleteNode(addr, "ip-prefix");
		cfg.deleteNode(addr, "dns-name");
		if (addressType == ADDRESS_TYPE_IP_PREFIX) {
			if (!cfg.isInet6FlowEnabled() && address.isIPv6Address()) {
				throw new IllegalArgumentException("Invalid address family. It's not allowed to configure ipv6 addresses when inet6 flow is disabled.");
			}
			addr.appendChild(cfg.createElement("ip-prefix", address.toString()));
		}
		else if (addressType == ADDRESS_TYPE_DNS_NAME) {
			Node dn = cfg.createElement("dns-name");
			dn.appendChild(cfg.createElement("name", dnsName));
			addr.appendChild(dn);
		}
	}
	
	public final DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				final AddressElement self = AddressElement.this;
				
				try {
					if (cfg.isAddressOrAddressSetExist(zoneName, name)) {
						throw new IllegalArgumentException("Address name is already exist.");
					}
	
					Node ab = cfg.addressBookNode(zoneName);
					if (ab == null) {
						throw new IllegalArgumentException("Zone is not found.");
					}
				
					Node a = cfg.createElement("address");
                    ab.insertBefore(a, cfg.getNode(ab, "attach"));

					self.configAddress(a, cfg);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to create address.", e);
				}
			}
			
		};
	}

	public final DeviceConfigure getEditConfigure(final AddressElement old) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				AddressElement self = AddressElement.this;
			
				try {
					if (self.isEditable(cfg)) {
						Node addr;
						if (!self.zoneName.equals(old.zoneName)) {
							if (old.isInUse(cfg)) {
								throw new IllegalArgumentException("zone can't be changed here due to either address in use or in a group!");
							}

							addr = cfg.findAddressNode(old.zoneName, old.name);
							Node ab = cfg.addressBookNode(self.zoneName);

							ab.insertBefore(addr, cfg.getNode(ab, "attach"));
						}
						else {
							addr = cfg.findAddressNode(old.zoneName, old.name);
						}

						self.setDescription(old.getDescription());
						self.configAddress(addr, cfg);

						if (!self.name.equals(old.name)) {
							self.configRename(old.name, cfg);
						}
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to edit addres.", e);
				}
			}
			
		};
	}
	
	public final DeviceConfigure getDeleteConfigure() {
		return getDeleteConfigure(zoneName, name);
	}
	
	public final static DeviceConfigure getDeleteConfigure(final String zoneName, final String addrName) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				try {
                    Node ab = cfg.findAddressBookNode(zoneName, false);
					if (ab == null) {
						return;
					}

                    cfg.deleteNode(ab, "address/name[normalize-space(text())=%s]/..", XML.xpc(addrName));
                    
                    if (cfg.getNode(ab, "address") == null && cfg.getNode(ab, "address-set") == null) {
                    	ab.getParentNode().removeChild(ab);
                    }
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to delete address.", e);
				}
			}
			
		};
	}
	
	public final boolean isEditable(DeviceConfig cfg) {
		return isEditable(getZoneName(), getName(), cfg);
	}
	
	public final static boolean isEditable(String zoneName, String name, DeviceConfig cfg) {
		return !(isMip(zoneName, name, cfg));
//		return !(isVip(zoneName, name, cfg) || isMip(zoneName, name, cfg));
	}

	public final boolean isRemovable(DeviceConfig cfg) {
		return isRemovable(getZoneName(), getName(), cfg);
	}
	
	public final static boolean isRemovable(String zoneName, String name, DeviceConfig cfg) {
		return !(isMip(zoneName, name, cfg) || isInUse(zoneName, name, cfg));
//		return !(isVip(zoneName, name, cfg) || isMip(zoneName, name, cfg) || isInUse(zoneName, name, cfg));
	}

//	public final boolean isVip(DeviceConfig cfg) {
//		return isVip(getZoneName(), getName(), cfg);
//	}
//	
//	public static boolean isVip(String zoneName, String name, DeviceConfig cfg) {
//		Node dnat = cfg.natDestinationNode();
//		Node poolNode = cfg.findNatDestinationPoolNode(dnat, name);
//		if (poolNode == null)
//			return false;
//		
//		for (Node ruleSet: XML.iterableNodeList(cfg.getNodeList(dnat, "rule-set"))) {
//			String z = cfg.getString(ruleSet, "from/zone/text()");
//			if (StringUtils.isBlank(z))
//				continue;
//			
//			if (!z.equals(zoneName))
//				continue;
//			
//			for(Node rule: XML.iterableNodeList(cfg.getNodeList(ruleSet, "rule"))) {
//				String p = cfg.getString(rule, "then/destination-nat/pool/pool-name/text()");
//				if (StringUtils.isBlank(p))
//					continue;
//				
//				if (p.equals(name))
//					return true;
//			}
//		}
//		
//		return false;
//	}
//	
	public final boolean isMip(DeviceConfig cfg) {
		return isMip(getZoneName(), getName(), cfg);
	}
	
	public static boolean isMip(String zoneName, String name, DeviceConfig cfg) {
		Node snat = cfg.natStaticNode();
		
		for (Node ruleSet: IterableNodeList.apply(cfg.getNodeList(snat, "rule-set"))) {
			String z = cfg.getString(ruleSet, "from/zone/text()");
			if (!"global".equals(zoneName) && StringUtils.isNotBlank(z) && !z.equals(zoneName))
				continue;
			
			for(Node rule: IterableNodeList.apply(cfg.getNodeList(ruleSet, "rule"))) {
				String n = cfg.getString(rule, "then/static-nat/prefix-name/addr-prefix-name/text()");
				if (StringUtils.isBlank(n))
					continue;
				
				if (n.equals(name))
					return true;
			}
		}
		
		return false;
	}
	
	public final boolean isInUse(DeviceConfig cfg) {
		return isInUse(getZoneName(), getName(), cfg);
	}
	
	public static final boolean isInUse(String zoneName, String name, DeviceConfig cfg) {
		return isInUse(zoneName, name, null, cfg);
	}
	
	public static boolean isInUse(String zoneName, String name, AddressSet exceptSet, DeviceConfig cfg) {
        Node ab = cfg.findAddressBookNode(zoneName, false);
        Node n;
		if (ab != null) {
			n = (exceptSet == null) 
					? cfg.getNode(ab, "address-set/address[normalize-space(name/text())=%s]", XML.xpc(name))
					: cfg.getNode(ab, "address-set[normalize-space(name/text())!=%s]/address[normalize-space(name/text())=%s]", XML.xpc(exceptSet.getName()), XML.xpc(name));
			
			if (n != null)
				return true;
		}

		Node p = cfg.policiesNode();
		if (p != null) {
			if ("global".equals(zoneName)) {
				n = cfg.getNode(p, "policy/policy/match[normalize-space(source-address/text())=%s]/..", XML.xpc(name));
				if (n != null)
					return true;

				n = cfg.getNode(p, "policy/policy/match[normalize-space(destination-address/text())=%s]/..", XML.xpc(name));
				if (n != null)
					return true;			
			} 
			else {
				n = cfg.getNode(p, "policy[normalize-space(from-zone-name/text())=%s]/policy/match[normalize-space(source-address/text())=%s]/..", XML.xpc(zoneName), XML.xpc(name));
				if (n != null)
					return true;

				n = cfg.getNode(p, "policy[normalize-space(to-zone-name/text())=%s]/policy/match[normalize-space(destination-address/text())=%s]/..", XML.xpc(zoneName), XML.xpc(name));
				if (n != null)
					return true;
			}
		}

		return false;
	}

	protected void configRename(String oldName, DeviceConfig cfg) {
        Node ab = cfg.findAddressBookNode(zoneName, false);
        NodeList nl;
        if (ab != null) {
            for (Node n : IterableNodeList.apply(cfg.getNodeList(ab, "address-set/address/name[normalize-space(text())=%s]", XML.xpc(oldName)))) {
 				XML.removeAllChildNodes(n);
				n.appendChild(cfg.createTextNode(name));
            }
        }

		Node p = cfg.policiesNode();
		if (p != null) {
			nl = cfg.getNodeList(p, "policy[normalize-space(from-zone-name/text())=%s]/policy/match/source-address[normalize-space(text())=%s]", XML.xpc(zoneName), XML.xpc(oldName));
            for (Node n : IterableNodeList.apply(nl)) {
				XML.removeAllChildNodes(n);
				n.appendChild(cfg.createTextNode(name));
			}

			nl = cfg.getNodeList(p, "policy[normalize-space(to-zone-name/text())=%s]/policy/match/destination-address[normalize-space(text())=%s]", XML.xpc(zoneName), XML.xpc(oldName));
            for (Node n : IterableNodeList.apply(nl)) {
				XML.removeAllChildNodes(n);
				n.appendChild(cfg.createTextNode(name));
			}
		}
	}

	public int isChanged(DeviceConfig candidate) {
		AddressElement c = candidate.findAddress(zoneName, name);
		if (c == null)
			return 1;
		if (!equals(c)) 
			return 2;
		return 0;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + addressType;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((dnsName == null) ? 0 : dnsName.hashCode());
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((zoneName == null) ? 0 : zoneName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AddressElement other = (AddressElement) obj;
		if (addressType != other.addressType)
			return false;
		if (StringUtils.isBlank(name)) {
			if (StringUtils.isNotBlank(other.name))
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (addressType == ADDRESS_TYPE_IP_PREFIX) {
			if (address == null) {
				if (other.address != null)
					return false;
			}
			else if (!address.equals(other.address))
				return false;
		} else {
			if (StringUtils.isBlank(dnsName)) {
				if (StringUtils.isNotBlank(other.dnsName))
					return false;
			} else if (!dnsName.equals(other.dnsName))
				return false;
		}
		if (StringUtils.isBlank(zoneName)) {
			if (StringUtils.isNotBlank(other.zoneName))
				return false;
		} else if (!zoneName.equals(other.zoneName))
			return false;
		return true;
	}
}
