package to.lef.srxlib.policy;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AddressSet {
	private final String name;
	private final String zoneName;
	private final List<String> addressNameList;
	private final List<String> addressSetNameList;
	private String description;
	
	public static AddressSet createFromNode(String zoneName, Node as, DeviceConfig cfg) {
		String name = cfg.getString(as, "name/text()");
		List<String> al  = new ArrayList<String>();
		List<String> asl = new ArrayList<String>();

        for (Node n : IterableNodeList.apply(cfg.getNodeList(as, "address"))) {
			String an = cfg.getString(n, "name/text()");
			if (StringUtils.isNotBlank(an)) {
				al.add(an);
			}
		}

        for (Node n : IterableNodeList.apply(cfg.getNodeList(as, "address-set"))) {
			String an = cfg.getString(n, "name/text()");
			if (StringUtils.isNotBlank(an)) {
				asl.add(an);
			}
		}
	
        String description = cfg.getString(as, "description/text()");
		return new AddressSet(zoneName, name, al, asl, description);
	}
	
	public AddressSet(String zoneName, String name, List<String> addressNameList, List<String> addressSetNameList) {
		this(zoneName, name, addressNameList, addressSetNameList, null);
	}
	
	public AddressSet(String zoneName, String name, List<String> addressNameList, List<String> addressSetNameList, String description) {
		super();
		this.zoneName = zoneName;
		this.name = name;
		this.addressNameList = new ArrayList<String>(addressNameList);
		this.addressSetNameList = new ArrayList<String>(addressSetNameList);
		this.description = description;
	}

	public AddressSet(AddressSet old) {
		this(old.zoneName, old.name, old.addressNameList, old.addressSetNameList, old.description);
	}
	
	public final String getZoneName() {
		return zoneName;
	}
	
	public final String getName() {
		return name;
	}

	public void addAddressName(String name) {
		addressNameList.add(name);
	}
	
	public final List<String> getAddressNameList() {
		return Collections.unmodifiableList(addressNameList);
	}

	public void addAddressSetName(String name) {
		addressSetNameList.add(name);
	}
	
	public final List<String> getAddressSetNameList() {
		return Collections.unmodifiableList(addressSetNameList);
	}

	public final String getDescription() {
		return description;
	}

	public final void setDescription(String description) {
		this.description = description;
	}

	public final boolean hasIPv6Address(DeviceConfig cfg) {
		for (String aname: addressNameList) {
			AddressElement a = cfg.findAddress(zoneName, aname);
			if (a != null && a.hasIPv6Address())
				return true;
		}
		
		for (String aname: addressSetNameList) {
			AddressSet a = cfg.findAddressSet(zoneName, aname);
			if (a != null && a.hasIPv6Address(cfg))
				return true;
		}	
		return false;
	}
	
	protected void configSet(Node set, DeviceConfig cfg) {
		Node n = cfg.createElementsIfNotExist(set, "name");
		XML.removeAllChildNodes(n);
		n.appendChild(cfg.createTextNode(name));

		cfg.deleteNode(set, "description");
		if (StringUtils.isNotBlank(description)) 
			set.appendChild(cfg.createElement("description", description));
		
		cfg.deleteNode(set, "address");
		for(String na: addressNameList) {
			if (!na.equals(name)) {
				n = cfg.createElement("address");
				n.appendChild(cfg.createElement("name", na));
				set.appendChild(n);
			}
		}

		cfg.deleteNode(set, "address-set");
		for(String na: addressSetNameList) {
			if (!na.equals(name)) {
				n = cfg.createElement("address-set");
				n.appendChild(cfg.createElement("name", na));
				set.appendChild(n);
			}
		}	
	}

	protected void configRename(String oldName, DeviceConfig cfg) {
        Node ab = cfg.findAddressBookNode(zoneName, false);
		NodeList nl;
		if (ab != null) {
			for(Node n : IterableNodeList.apply(cfg.getNodeList(ab, "address-set/address-set/name[normalize-space(text())=%s]", XML.xpc(oldName)))) {
				XML.removeAllChildNodes(n);
				n.appendChild(cfg.createTextNode(name));
			}
		}

		Node p = cfg.policiesNode();
		if (p != null) {
			nl = cfg.getNodeList(p, "policy[normalize-space(from-zone-name/text())=%s]/policy/match/source-address[normalize-space(text())=%s]",
					XML.xpc(zoneName), XML.xpc(oldName));
			for (int i = 0; i < nl.getLength(); i++) {
				Node n = nl.item(i);
				XML.removeAllChildNodes(n);
				n.appendChild(cfg.createTextNode(name));
			}

			nl = cfg.getNodeList(p, "policy[normalize-space(to-zone-name/text())=%s]/policy/match/destination-address[normalize-space(text())=%s]",
					XML.xpc(zoneName), XML.xpc(oldName));
			for (int i = 0; i < nl.getLength(); i++) {
				Node n = nl.item(i);
				XML.removeAllChildNodes(n);
				n.appendChild(cfg.createTextNode(name));
			}
		}	
	}
	
	public final DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				final AddressSet self = AddressSet.this;
				
				try {
					if (cfg.isAddressOrAddressSetExist(getZoneName(), getName())) {
						throw new IllegalArgumentException(String.format("AddressSet name is in use."));
					}
	
					Node ab = cfg.addressBookNode(self.zoneName);
					if (ab == null) {
						throw new IllegalArgumentException("Zone is not found.");
					}
					
					Node as = cfg.createElement("address-set");
					self.configSet(as, cfg);
					
					if (as.getChildNodes().getLength() > 1)
						ab.insertBefore(as, cfg.getNode(ab, "attach"));
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to create address set.", e);
				}
			}
			
		};
	}
	
	public final DeviceConfigure getEditConfigure(final AddressSet old) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				AddressSet self = AddressSet.this;
			
				try {
					if (self.isEditable(cfg)) {
						Node set;
						if (!self.zoneName.equals(old.zoneName)) {
							throw new IllegalArgumentException("Can't change zone.");
						}

						set = cfg.findAddressSetNode(old.zoneName, old.name);
						if (set == null) {
							throw new IllegalArgumentException("Address set is not found.");
						}

						self.setDescription(old.getDescription());
						self.configSet(set, cfg);

						if (!self.name.equals(old.name)) {
							self.configRename(old.name, cfg);
						}
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to edit address set.", e);
				}
			}
			
		};
	}
	
	public final DeviceConfigure getDeleteConfigure() {
		return getDeleteConfigure(zoneName, name);
	}
	
	public final static DeviceConfigure getDeleteConfigure(final String zoneName, final String addrName) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					if (isRemovable(zoneName, addrName, cfg)) {
 						Node ab = cfg.findAddressBookNode(zoneName, false);
						if (ab == null) {
							return;
						}

                        cfg.deleteNode(ab, "address-set/name[normalize-space(text())=%s]/..", XML.xpc(addrName));

                        if (cfg.getNode(ab, "address") == null && cfg.getNode(ab, "address-set") == null) {
                        	ab.getParentNode().removeChild(ab);
                        }
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to delete address set.", e);
				}
			}
		};
	}

	public final boolean isVip(DeviceConfig cfg) {
		return isVip(getZoneName(), getName(), cfg);
	}
	
	public static boolean isVip(String zoneName, String name, DeviceConfig cfg) {
		Node dnat = cfg.natDestinationNode();
		
		for (Node ruleSet: IterableNodeList.apply(cfg.getNodeList(dnat, "rule-set"))) {
			String z = cfg.getString(ruleSet, "from/zone/text()");
			if (StringUtils.isBlank(z))
				continue;
			
			if (!z.equals(zoneName))
				continue;
			
			String n = cfg.getString(ruleSet, "name/text()");
			if (StringUtils.isBlank(n))
				continue;

			if (n.equals(name))
				return true;
		}
		
		return false;
	}
	
	public final boolean isEditable(DeviceConfig cfg) {
		return isEditable(getZoneName(), getName(), cfg);
	}
	
	public static boolean isEditable(String zoneName, String name, DeviceConfig cfg) {
		return !(isVip(zoneName, name, cfg));
	}
	
	public final boolean isRemovable(DeviceConfig cfg) {
		return isRemovable(getZoneName(), getName(), cfg);
	}
	
	public static boolean isRemovable(String zoneName, String name, DeviceConfig cfg) {
		return !(isVip(zoneName, name, cfg) || isInUse(zoneName, name, cfg));
	}
	
	public final boolean isInUse(DeviceConfig cfg) {
		return isInUse(getZoneName(), getName(), cfg);
	}
	
	public static boolean isInUse(String zoneName, String name, DeviceConfig cfg) {
        Node ab = cfg.findAddressBookNode(zoneName, false);
		Node n;
		if (ab != null) {
			n = cfg.getNode(ab, "address-set/address-set/name[normalize-space(text())=%s]/..", XML.xpc(name));
			if (n != null)
				return true;
		}

		Node p = cfg.policiesNode();
		if (p != null) {
			n = cfg.getNode(p, "policy[normalize-space(from-zone-name/text())=%s]/policy/match[normalize-space(source-address/text())=%s]/..",
					XML.xpc(zoneName), XML.xpc(name));
			if (n != null)
				return true;
			
			n = cfg.getNode(p, "policy[normalize-space(to-zone-name/text())=%s]/policy/match[normalize-space(destination-address/text())=%s]/..",
					XML.xpc(zoneName), XML.xpc(name));
			if (n != null)
				return true;
		}

		return false;
	}

	public int isChanged(DeviceConfig candidate) {
		AddressSet c = candidate.findAddressSet(zoneName, name);
		if (c == null)
			return 1;
		if (!equals(c)) 
			return 2;
		return 0;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addressNameList == null) ? 0 : addressNameList.hashCode());
		result = prime * result + ((addressSetNameList == null) ? 0 : addressSetNameList .hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((zoneName == null) ? 0 : zoneName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AddressSet other = (AddressSet) obj;
		if (addressNameList == null) {
			if (other.addressNameList != null)
				return false;
		} else if (!(other.addressNameList != null && addressNameList.size() == other.addressNameList.size() && addressNameList.containsAll(other.addressNameList))) 
			return false;
		if (addressSetNameList == null) {
			if (other.addressSetNameList != null)
				return false;
		} else if (!(other.addressSetNameList != null && addressSetNameList.size() == other.addressSetNameList.size() && addressSetNameList.containsAll(other.addressSetNameList))) 
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (zoneName == null) {
			if (other.zoneName != null)
				return false;
		} else if (!zoneName.equals(other.zoneName))
			return false;
		return true;
	}
}
