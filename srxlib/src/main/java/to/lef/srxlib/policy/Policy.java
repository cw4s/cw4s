package to.lef.srxlib.policy;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Collections;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import to.lef.srxlib.DefaultsConfig;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.Util;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.nat.Dip;
import to.lef.srxlib.syslog.Syslog;
import to.lef.srxlib.syslog.SyslogFile;

public class Policy {
	public static final String EGRESS_INTERFACE_ADDRESS = "junos_egress_interface_address";
	
	public static class DipRuleSet {
		private static class DipRuleSetConfigure {
			private final Policy policy;
			private final Node policyNode;
			private final DefaultsConfig defaults;
			private final Map<String, Boolean> appCache;
			private Node refNode;
			
			public DipRuleSetConfigure(Policy policy, Node policyNode, DefaultsConfig defaults) {
				this.policy   = policy;
				this.policyNode = policyNode;
				this.defaults = defaults;
				this.appCache = new HashMap<String, Boolean>();
				this.refNode = null;
			}
		
			public DeviceConfigure getCreateConfigure() {
				return new DeviceConfigure() {

					@Override
					public void config(DeviceConfig configuration, DeviceInformationProvider info) throws DeviceConfigException {
						try {
							configCreate(configuration);
						}
						catch (Exception e) {
							throw new DeviceConfigException("Failed to config source nat.", e);
						}
					}
					
				};
			}
	
			public DeviceConfigure getMoveConfigure() {
				return new DeviceConfigure() {

					@Override
					public void config(DeviceConfig configuration, DeviceInformationProvider info) throws DeviceConfigException {
						try {
							configMove(configuration);
						}
						catch (Exception e) {
							throw new DeviceConfigException("Failed to config source nat.", e);
						}
					}
					
				};
			}

			public DeviceConfigure getActivateConfigure() {
					return new DeviceConfigure() {

					@Override
					public void config(DeviceConfig configuration, DeviceInformationProvider info) throws DeviceConfigException {
						try {
							configActivate(configuration);
						}
						catch (Exception e) {
							throw new DeviceConfigException("Failed to config source nat.", e);
						}
					}
					
				};			
			}
		
			public void configActivate(DeviceConfig cfg) {
				Node source = cfg.natSourceNode();
				Node ruleSet = cfg.findNatSourceRuleSetNodeByZone(source, policy.getFromZoneName(), policy.getToZoneName());
				final String inactive = "inactive";
				
				if (ruleSet == null) 
					return;

				if (policy.isInactive()) {
					for(Node rule: IterableNodeList.apply(cfg.getNodeList(ruleSet, "rule[normalize-space(description/text())=%s]", XML.xpc(policy.getName())))) {			
						((Element)rule).setAttribute(inactive, inactive);
					}
				}
				else {
					for(Node rule: IterableNodeList.apply(cfg.getNodeList(ruleSet, "rule[normalize-space(description/text())=%s]", XML.xpc(policy.getName())))) {			
						((Element)rule).removeAttribute(inactive);
					}
				}
			}
			
			public void configMove(DeviceConfig cfg) {
				Node source = cfg.natSourceNode();
				Node ruleSet = cfg.findNatSourceRuleSetNodeByZone(source, policy.getFromZoneName(), policy.getToZoneName());
				if (ruleSet == null) 
					return;

				prepareRefNode(ruleSet, cfg);
				for(Node rule: IterableNodeList.apply(cfg.getNodeList(ruleSet, "rule[normalize-space(description/text())=%s]", XML.xpc(policy.getName())))) {
					ruleSet.insertBefore(rule, refNode);
				};
			}
			
			public void configCreate(DeviceConfig cfg) {
				Node source = cfg.natSourceNode();
				Node ruleSet = cfg.findNatSourceRuleSetNodeByZone(source, policy.getFromZoneName(), policy.getToZoneName());

				for (String addr: policy.getSourceNameList()) {
					if ("any".equals(addr) || "any-ipv6".equals(addr)) {
						throw new IllegalArgumentException("Invalid IP version while parsing source address " + addr + ". Please use any-ipv4.");
					}
					AddressElement addressElement = cfg.findAddress(policy.getFromZoneName(), addr);
					if (addressElement != null && addressElement.hasIPv6Address()) {
						throw new IllegalArgumentException("Invalid IP version while parsing source address " + addr + ". Required: v4, input: v6 ");
					} 

					AddressSet addressSet = cfg.findAddressSet(policy.getFromZoneName(), addr);
					if (addressSet != null && addressSet.hasIPv6Address(cfg)) {
						throw new IllegalArgumentException("Invalid IP version while parsing source address " + addr + ". Required: v4, input: v6 ");
					} 
				}
				for (String addr: policy.getDestinationNameList()) {
					if ("any".equals(addr) || "any-ipv6".equals(addr)) {
						throw new IllegalArgumentException("Invalid IP version while parsing destination address " + addr + ". Please use any-ipv4.");
					}
					AddressElement addressElement = cfg.findAddress(policy.getToZoneName(), addr);
					if (addressElement != null && addressElement.hasIPv6Address()) {
						throw new IllegalArgumentException("Invalid IP version while parsing destination address " + addr + ". Required: v4, input: v6 ");
					} 

					AddressSet addressSet = cfg.findAddressSet(policy.getToZoneName(), addr);
					if (addressSet != null && addressSet.hasIPv6Address(cfg)) {
						throw new IllegalArgumentException("Invalid IP version while parsing destination address " + addr + ". Required: v4, input: v6 ");
					} 
				}
				
				if (ruleSet == null) {
					String ruleSetPrefix = String.format("%s-to-%s", policy.getFromZoneName(), policy.getToZoneName());

					for (int i = 0; ; i++) {
						String ruleSetName;
						if (i == 0) 
							ruleSetName = String.format("%s", ruleSetPrefix);
						else
							ruleSetName = String.format("%s-%d", ruleSetPrefix, i);

						if (cfg.getNode(source, "rule-set/name[normalize-space(text())=%s]/..", ruleSetName) == null) {
							ruleSet = cfg.createElement("rule-set");
							ruleSet.appendChild(cfg.createElement("name", ruleSetName));
							Node from = cfg.createElement("from");
							from.appendChild(cfg.createElement("zone", policy.getFromZoneName()));
							ruleSet.appendChild(from);
							Node to = cfg.createElement("to");
							to.appendChild(cfg.createElement("zone", policy.getToZoneName()));
							ruleSet.appendChild(to);

							break;
						}
					}
					source.appendChild(ruleSet);
				}

				configRules(ruleSet, cfg);
			}

			private void prepareRefNode(Node ruleSet, DeviceConfig cfg) {
				Node target = null;
				for (Node pNode: IterableNodeList.apply(cfg.getNodeList(policyNode, "following-sibling::policy"))) {
					String pName = cfg.getString(pNode, "name/text()");
					if (StringUtils.isNotBlank(pName)) {
						target = cfg.getNode(ruleSet, "rule[normalize-space(description/text())=%s]", XML.xpc(pName));
						if (target != null) {
							break;
						}
					}
				}
				refNode = target;
			}
			
			private void configRules(Node ruleSet, DeviceConfig cfg) {
				prepareRefNode(ruleSet, cfg);
				
				for (String name: policy.getApplicationNameList()) {
					ApplicationSet set = cfg.findApplicationSet(name);
					if (set == null)
						set = defaults.findApplicationSet(name);
					if (set != null) {
						configRule(ruleSet, set, cfg);
						continue;
					}
					
					configRule(ruleSet, name, cfg);
				}
			}

			private void configRule(Node ruleSet, ApplicationSet set, DeviceConfig cfg) {
				for (String name: set.getApplicationNameList()) {
					configRule(ruleSet, name, cfg);
				}
			}

			private Node prepareRuleNode(Node ruleSet, DeviceConfig cfg) {
				for (int i = 1; ; i++) {
					String ruleName = String.format("%s-%d", policy.getName(), i);
					Node rule = cfg.getNode(ruleSet, "rule/name[normalize-space(text())=%s]/..", XML.xpc(ruleName));

					if (rule == null) {
						rule = cfg.createElement("rule");
						rule.appendChild(cfg.createElement("name", ruleName));
						rule.appendChild(cfg.createElement("description", policy.getName()));
						
						if (policy.isInactive())
							((Element)rule).setAttribute("inactive", "inactive");
						ruleSet.insertBefore(rule, refNode);

						return rule;
					}
				}		
			}

			private void configRule(Node ruleSet, String appName, DeviceConfig cfg) {
				if (appCache.get(appName) == null) {
					appCache.put(appName, true);
					
					ApplicationElement app = cfg.findApplication(appName);
					if (app == null)
						app = defaults.findApplication(appName);
					if (app == null)
						throw new IllegalArgumentException("Unknown application: " + appName);
				
					configRule(ruleSet, app, cfg);
					for (ApplicationTerm term: app.getTermList()) {
						configRule(ruleSet, term, cfg);
					}
				}
			}
			
			private void configRule(Node ruleSet, ApplicationBase app, DeviceConfig cfg) {
				String  protocolName = app.getProtocol();
				Integer protocolNum = Util.applicationProtocolToNumber(protocolName);
				String  dstLowName = app.getDstLowPort();
				Integer dstLowNum = Util.applicationPortToNumber(dstLowName);
				String  dstHighName = app.getDstHighPort();
				Integer dstHighNum = Util.applicationPortToNumber(dstHighName);
				
				if (StringUtils.isNotBlank(protocolName) && protocolNum == null) 
					throw new IllegalArgumentException("Unknown protocol: " + protocolName);
				if (StringUtils.isNotBlank(dstLowName) && dstLowNum == null) 
					throw new IllegalArgumentException("Unknown destination low port: " + dstLowName);
				if (StringUtils.isNotBlank(dstHighName) && dstHighName == null)
					throw new IllegalArgumentException("Unknown destination high port: " + dstHighName);
				if (protocolNum == null)
					return;		
				
				Node rule = prepareRuleNode(ruleSet, cfg);
				Node match = cfg.createElement("src-nat-rule-match");
				rule.appendChild(match);			
				
				for(String name: policy.getSourceNameList()) {
					match.appendChild(cfg.createElement("source-address-name", name));
				}
				
				for(String name: policy.getDestinationNameList()) {
					match.appendChild(cfg.createElement("destination-address-name", name));
				}
	
				if (protocolNum != 0)
					match.appendChild(cfg.createElement("protocol", protocolName));
				
				if (dstLowNum != null) {
					Node dst = cfg.createElement("destination-port");
					if (cfg.isNewerThan12_1X47())
						dst.appendChild(cfg.createElement("name", "%d", dstLowNum));
					else
						dst.appendChild(cfg.createElement("low", "%d", dstLowNum));
					if (dstHighNum != null) {
						Node to = cfg.createElement("to");
						to.appendChild(cfg.createElement("high", "%d", dstHighNum));
						dst.appendChild(to);
					}
					match.appendChild(dst);
				}
				
				Node then = cfg.createElement("then");
				Node sourceNat = cfg.createElement("source-nat");
				
				if (EGRESS_INTERFACE_ADDRESS.equals(policy.getSourceNat())) {
					sourceNat.appendChild(cfg.createElement("interface"));
				}
				else {
					Node pool = cfg.createElement("pool");
					pool.appendChild(cfg.createElement("pool-name", policy.getSourceNat()));
					sourceNat.appendChild(pool);
				}
				then.appendChild(sourceNat);
				rule.appendChild(then);
			}
		}
	
		public static DeviceConfigure getCreateConfigure(Policy policy, Node policyNode, DefaultsConfig defaults) {
			return new DipRuleSetConfigure(policy, policyNode, defaults).getCreateConfigure();
		}
	
		public static DeviceConfigure getMoveConfigure(Policy policy, Node policyNode) {
			return new DipRuleSetConfigure(policy, policyNode, null).getMoveConfigure();
		}
	
		public static DeviceConfigure getActivateConfigure(Policy policy, Node policyNode) {
			return new DipRuleSetConfigure(policy, policyNode, null).getActivateConfigure();
		}
		
		public static DeviceConfigure getDeleteConfigure(final Policy policy) {
			return new DeviceConfigure() {

				@Override
				public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
					Node source = cfg.natSourceNode();
					Node ruleSet = cfg.findNatSourceRuleSetNodeByZone(source, policy.getFromZoneName(), policy.getToZoneName());

					if (ruleSet != null) {
						cfg.deleteNode(ruleSet, "rule[normalize-space(description/text())=%s]", XML.xpc(policy.getName()));
						if (cfg.getNode(ruleSet, "rule") == null) {
							ruleSet.getParentNode().removeChild(ruleSet);
						}
					}
				}
				
			};
		}
	}
	
	public static final String ACTION_PERMIT = "permit";
	public static final String ACTION_DENY   = "deny";
	public static final String ACTION_REJECT = "reject";
	
	private final String name;
	private final String fromZoneName;
	private final String toZoneName;
	private final int order;
	private List<String> sourceNameList;
	private List<String> destinationNameList;
	private List<String> applicationNameList;
	private String action;
	private boolean logInit;
	private boolean logClose;
	private boolean inactive;

	private boolean sourceNatEnabled;
	private String sourceNat;
	
	private static class InactiveFixer implements AutoCloseable {
		private final Node policies;
//		private final String from;
//		private final String to;
		private final DeviceConfig config;
		
		public InactiveFixer(String from, String to, DeviceConfig cfg) {
			this(from, to, false, cfg);
		}
	
		public InactiveFixer(String from, String to, boolean createIfNotExist, DeviceConfig cfg) {
			this.policies = cfg.policyZoneNode(from, to, createIfNotExist);
//			this.from     = from;
//			this.to       = to;
			this.config   = cfg;
		
			if (policies != null) {
				if (StringUtils.isNotBlank(config.getString(policies, "@inactive"))) {
					final String inactive = "inactive";
					for (Node n: IterableNodeList.apply(config.getNodeList(policies, "policy"))) {
						((Element)n).setAttribute(inactive, inactive);
					}
					((Element)policies).removeAttribute(inactive);
				}
			}
		}

		public Node find(String name) {
			if (policies != null) {
				return config.findPolicyNode(policies, name);
			}
			return null;
		}
	
		public final Node getPolicies() {
			return policies;
		}
		
		@Override
		public void close() throws Exception {
			int inactiveCount = 0;
			
			if (policies != null) {
				NodeList nl = config.getNodeList(policies, "policy");
				for (Node n: IterableNodeList.apply(nl)) {
					if (StringUtils.isNotBlank(config.getString(n, "@inactive"))) {
						inactiveCount++;
					}
				}
				
				if (inactiveCount == nl.getLength()) {
					final String inactive = "inactive";
					((Element)policies).setAttribute(inactive, inactive);
					
					for (Node n: IterableNodeList.apply(nl)) {
						((Element)n).removeAttribute(inactive);
					}
				}
			}
		}
	}
	
	public static Policy createFromNode(String from, String to, int order, Node pn, DeviceConfig cfg) {
		Node n;
		NodeList nl;
		int l;
		
		Node match = cfg.getNode(pn, "match");
		Node then  = cfg.getNode(pn, "then");
	
		List<String> srcList = new ArrayList<String>();
		List<String> dstList = new ArrayList<String>();
		List<String> appList = new ArrayList<String>();

		String name = cfg.getString(pn, "name/text()");
		String action;
	
		boolean logInit = false;
		boolean logClose = false;
		boolean inactive = false;
	
		String inactiveAttr = cfg.getString(pn, "@inactive");
		if (StringUtils.isNotBlank(inactiveAttr))
			inactive = true;
		inactiveAttr = cfg.getString(pn, "../@inactive");
		if (StringUtils.isNotBlank(inactiveAttr))
			inactive = true;
		
		nl = cfg.getNodeList(match, "source-address");
		l = nl.getLength();
		for (int i = 0; i < l; i++) {
			String s = cfg.getString(nl.item(i), "text()");
			if (StringUtils.isNotBlank(s))
				srcList.add(s);
		}	
		
		nl = cfg.getNodeList(match, "destination-address");
		l = nl.getLength();
		for (int i = 0; i < l; i++) {
			String s = cfg.getString(nl.item(i), "text()");
			if (StringUtils.isNotBlank(s))
				dstList.add(s);
		}
		
		nl = cfg.getNodeList(match, "application");
		l = nl.getLength();
		for (int i = 0; i < l; i++) {
			String s = cfg.getString(nl.item(i), "text()");
			if (StringUtils.isNotBlank(s))
				appList.add(s);
		}	

		n = cfg.getNode(then, ACTION_PERMIT);
		if (n != null) {
			action = ACTION_PERMIT;
		}
		else  {
			n = cfg.getNode(then, ACTION_DENY);
			if (n != null) {
				action = ACTION_DENY;
			}
			else {
				n = cfg.getNode(then, ACTION_REJECT);
				if (n != null) {
					action = ACTION_REJECT;
				}
				else {
					return null;
				}
			}
		}
		
		Node log = cfg.getNode(then, "log");
		if (log != null) {
			if (XML.isBooleanElementSet(log, "session-init")) 
				logInit = true;
			if (XML.isBooleanElementSet(log, "session-close")) 
				logClose = true;		
		}
		
		Policy ret = new Policy(name, from, to, order, srcList, dstList, appList, action, logInit, logClose, inactive);

		if (ret != null) {
			Node ruleSet = cfg.findNatSourceRuleSetNodeByZone(ret.getFromZoneName(), ret.getToZoneName());
			if (ruleSet != null) {
				Node rule = cfg.getNode(ruleSet, "rule[normalize-space(description/text())=%s]", XML.xpc(ret.getName()));
				if (rule != null) {
					if (cfg.getNode(rule, "then/source-nat/interface") != null) {
						ret.setSourceNat(true, EGRESS_INTERFACE_ADDRESS);
					}
					else {
						String pName = cfg.getString(rule, "then/source-nat/pool/pool-name/text()");
						if (StringUtils.isNotBlank(pName)) {
							ret.setSourceNat(true, pName);
							return ret;
						}				
					}
				}
			}
		}
		
		return ret;
	}
	
	public Policy(String name, String fromZoneName, String toZoneName, int order,
			List<String> sourceNameList, List<String> destinationNameList, List<String> applicationNameList, 
			String action, boolean logInit, boolean logClose, boolean inactive) {
		this.name                = name;
		this.fromZoneName        = fromZoneName;
		this.toZoneName          = toZoneName;
		this.order               = order;
		this.sourceNameList      = sourceNameList;
		this.destinationNameList = destinationNameList;
		this.applicationNameList = applicationNameList;
		this.action              = action;
		this.logInit             = logInit;
		this.logClose            = logClose;
		this.inactive            = inactive;
		this.sourceNatEnabled    = false;
		this.sourceNat           = "";
	}
	
	public Policy(String name, String fromZoneName, String toZoneName,
			List<String> sourceNameList, List<String> destinationNameList, List<String> applicationNameList, 
			String action, boolean logInit, boolean logClose) {
		this(name, fromZoneName, toZoneName, -1, sourceNameList, destinationNameList, applicationNameList, action, logInit, logClose, false);
	}

	public final String getName() {
		return name;
	}

	public final String getFromZoneName() {
		return fromZoneName;
	}

	public final String getToZoneName() {
		return toZoneName;
	}

	public final List<String> getSourceNameList() {
		return Collections.unmodifiableList(sourceNameList);
	}

	public final List<String> getDestinationNameList() {
		return Collections.unmodifiableList(destinationNameList);
	}

	public final List<String> getApplicationNameList() {
		return Collections.unmodifiableList(applicationNameList);
	}

	public final String getAction() {
		return action;
	}
	
	public final boolean isLogInit() {
		return logInit;
	}

	public final boolean isLogClose() {
		return logClose;
	}

	public final boolean isInactive() {
		return inactive;
	}

	public final boolean isSourceNatEnabled() {
		return sourceNatEnabled;
	}

	public final String getSourceNat() {
		return sourceNat;
	}

	public final void setSourceNat(Boolean enabled, String target) {
		if (enabled && StringUtils.isNotBlank(target)) {
			sourceNatEnabled = enabled;
			sourceNat = target;
		}
		else {
			sourceNatEnabled = false;
		}
	}
	
	public final boolean isEditable(DeviceConfig cfg) {
		return true;
	}

	public final boolean isRemovable(DeviceConfig cfg) {
		return true;
	}
	
	protected void configPolicy(Node policy, DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
		final String srcTag = "source-address";
		final String dstTag = "destination-address";
		final String appTag = "application";
		
		Node match = cfg.createElementsIfNotExist(policy, "match");
		Node then  = cfg.createElementsIfNotExist(policy, "then");
		Node log   = cfg.createElementsIfNotExist(then, "log");
	
		NodeList nl = cfg.getNodeList(policy, srcTag);
		int l = nl.getLength();
		
		for (int i = 0; i < l; i++) {
			policy.removeChild(nl.item(i));
		}
		
		NodeList nodeList = cfg.getNodeList(match, "//text()[normalize-space()='']");
		for (int i = 0; i < nodeList.getLength(); ++i) {
			Node node = nodeList.item(i);
			node.getParentNode().removeChild(node);
		}
			
		XML.deleteNodeList(cfg.getNodeList(match, srcTag));
		for(String name: sourceNameList) {
			match.appendChild(cfg.createElement(srcTag, name));
		}
		
		XML.deleteNodeList(cfg.getNodeList(match, dstTag));
		for(String name: destinationNameList) {
			match.appendChild(cfg.createElement(dstTag, name));
		}
		
		XML.deleteNodeList(cfg.getNodeList(match, appTag));
		for(String name: applicationNameList) {
			match.appendChild(cfg.createElement(appTag, name));
		}	
		
		XML.deleteNodeList(cfg.getNodeList(then, ACTION_PERMIT));
		XML.deleteNodeList(cfg.getNodeList(then, ACTION_DENY));
		XML.deleteNodeList(cfg.getNodeList(then, ACTION_REJECT));
	
		then.appendChild(cfg.createElement(action));
		if (logInit || logClose) {
			cfg.setBooleanElement(log, "session-init", logInit);
			cfg.setBooleanElement(log, "session-close", logClose);
			
			String sessionLogFileName = cfg.sessionLogFileName();
			if (sessionLogFileName == null) {
				for (int i = 0; ; i++) {
					String fileName = (i == 0) ? "sessions" : "sessions" + i;
					if (cfg.findSyslogFileNode(fileName) == null) {
						SyslogFile syslog = new SyslogFile(fileName, Syslog.FACILITY_ANY, Syslog.LEVEL_ANY, false, true);
						syslog.getCreateConfigure().config(cfg, info);
						break;
					}
				}
			}
		}
		else {
			then.removeChild(log);
		}
	
		final String aInactive = "inactive";
		if (inactive) {
			((Element)policy).setAttribute(aInactive, aInactive);
		}
		else {
			((Element)policy).removeAttribute(aInactive);
		}
	}
	
	public DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				Policy self = Policy.this;
				
				try (InactiveFixer fixer = new InactiveFixer(self.fromZoneName, self.toZoneName, true, cfg)) {
					if (cfg.isPolicyExist(self.fromZoneName, self.toZoneName, self.name))
						throw new IllegalArgumentException("Policy is already exist.");
					Node policies = fixer.getPolicies();
					Node policy = cfg.createElement("policy");
					
					policy.appendChild(cfg.createElement("name", self.name));
					policies.appendChild(policy);
//					policies.insertBefore(policy, cfg.getNode(policies, "policy"));
					self.configPolicy(policy, cfg, info);
				}
				catch (DeviceConfigException e) {
					throw e;
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to create policy", e);
				}
			}
		};
	}
	
	public DeviceConfigure getEditConfigure(final Policy old, final DefaultsConfig defaults) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				Policy self = Policy.this;
				try (InactiveFixer fixer = new InactiveFixer(old.fromZoneName, old.toZoneName, cfg)){
					Node policy = fixer.find(old.name);
					if (policy == null) 
						throw new IllegalArgumentException("Policy is not exist.");
					
					self.inactive = old.inactive;
					self.setSourceNat(old.sourceNatEnabled, old.sourceNat);
					
					policy.removeChild(cfg.getNode(policy, "name"));
					policy.insertBefore(cfg.createElement("name", self.name), policy.getFirstChild());
					self.configPolicy(policy, cfg, info);
					
					DipRuleSet.getDeleteConfigure(old).config(cfg, info);
				
					configSourceNat(policy, defaults, cfg, info);
				}
				catch (DeviceConfigException e) {
					throw e;
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to edit policy.", e);
				}
			}
			
		};
	}
	
	public DeviceConfigure getDeleteConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				Policy self = Policy.this;
				try (InactiveFixer fixer = new InactiveFixer(self.fromZoneName, self.toZoneName, cfg)){
					DipRuleSet.getDeleteConfigure(self).config(cfg, info);
					
					Node policy = fixer.find(self.name);
					
					if (policy != null) {
						Node policies = fixer.getPolicies();
						policies.removeChild(policy);
						if (cfg.getNode(policies, "policy") == null) {
							policies.getParentNode().removeChild(policies);
						}
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to remove policy.", e);
				}
			}
		};
	}
	
	public DeviceConfigure getMoveConfigure(final String before) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				Policy self = Policy.this;
				try (InactiveFixer fixer = new InactiveFixer(self.fromZoneName, self.toZoneName, cfg)){
					Node policy = fixer.find(self.name);
					if (policy != null) {
						if (StringUtils.isNotBlank(before)) {
							fixer.getPolicies().insertBefore(policy, fixer.find(before));
						}
						else {
							fixer.getPolicies().appendChild(policy);
						}
					}
					DipRuleSet.getMoveConfigure(self, policy).config(cfg, info);;
				}
				catch (DeviceConfigException e) {
					throw e;
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to move policy.", e);
				}
			}
			
		};
	}

	public DeviceConfigure getActivateConfigure(final boolean isActive) {
		this.inactive = !isActive;
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				final String inactive = "inactive";
				try (InactiveFixer fixer = new InactiveFixer(fromZoneName, toZoneName, cfg)){
					Element policy = (Element)fixer.find(name);
					if (policy == null) 
						throw new IllegalArgumentException("Policy is not found.");
			
					if (isActive) {
						policy.removeAttribute(inactive);
					}
					else {
						policy.setAttribute(inactive, inactive);
					}

					DipRuleSet.getActivateConfigure(Policy.this, policy).config(cfg, info);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config activate policy.", e);
				}
			}
			
		};
	}

	private void configSourceNat(Node policyNode, DefaultsConfig defaults, DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException, UnknownHostException {
		DipRuleSet.getDeleteConfigure(this).config(cfg, info);
		if (isSourceNatEnabled()) {
			if (!EGRESS_INTERFACE_ADDRESS.equals(getSourceNat())) {
				Dip dip = cfg.findNatSourcePool(sourceNat);
				if (dip == null)
					throw new IllegalArgumentException("DIP is not found.");
			}
			DipRuleSet.getCreateConfigure(this, policyNode, defaults).config(cfg, info);
		}
	}
	
	public DeviceConfigure getAdvancedConfigure(final DefaultsConfig defaults) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				try (InactiveFixer fixer = new InactiveFixer(fromZoneName, toZoneName, cfg)){
					Node policy = fixer.find(name);
					if (policy == null) 
						throw new IllegalArgumentException("Policy is not exist.");
					
					configSourceNat(policy, defaults, cfg, info);
				}
				catch (DeviceConfigException e) {
					throw e;
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config policy advanced settings.", e);
				}
			}
			
		};
	}
	
	public int isChanged(Map<String, Policy> candidate) {
		Policy c = candidate.get(getName());
		
		if (c == null)
			return 1;
		if (!equals(c))
			return 2;
		if (order != c.order)
			return 2;
		return 0;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime * result + ((applicationNameList == null) ? 0 : applicationNameList .hashCode());
		result = prime * result + ((destinationNameList == null) ? 0 : destinationNameList .hashCode());
		result = prime * result + ((fromZoneName == null) ? 0 : fromZoneName.hashCode());
		result = prime * result + (logClose ? 1231 : 1237);
		result = prime * result + (logInit ? 1231 : 1237);
		result = prime * result + (inactive ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((sourceNameList == null) ? 0 : sourceNameList.hashCode());
		result = prime * result + ((toZoneName == null) ? 0 : toZoneName.hashCode());
		result = prime * result + (sourceNatEnabled ? 1231:1237);
		result = prime * result + ((sourceNat == null) ? 0 : sourceNat.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Policy other = (Policy) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
	
		if (fromZoneName == null) {
			if (other.fromZoneName != null) {
				return false;
			}
		} else if (!fromZoneName.equals(other.fromZoneName)) {
			return false;
		}	
		if (toZoneName == null) {
			if (other.toZoneName != null) {
				return false;
			}
		} else if (!toZoneName.equals(other.toZoneName)) {
			return false;
		}	
		if (action == null) {
			if (other.action != null) {
				return false;
			}
		} else if (!action.equals(other.action)) {
			return false;
		}
		if (applicationNameList == null) {
			if (other.applicationNameList != null) {
				return false;
			}
		} else if (!(other.applicationNameList != null && applicationNameList.containsAll(other.applicationNameList))) {
			return false;
		}
		if (destinationNameList == null) {
			if (other.destinationNameList != null) {
				return false;
			}
		} else if (!(other.destinationNameList != null && destinationNameList.containsAll(other.destinationNameList))) {
			return false;
		}
		if (sourceNameList == null) {
			if (other.sourceNameList != null) {
				return false;
			}
		} else if (!(other.sourceNameList != null && sourceNameList.containsAll(other.sourceNameList))) {
			return false;
		}

		if (logClose != other.logClose) {
			return false;
		}
		if (logInit != other.logInit) {
			return false;
		}
		if (inactive != other.inactive) {
			return false;
		}
		if (sourceNatEnabled != other.sourceNatEnabled) {
			return false;
		}
		if (sourceNatEnabled) {
			if (StringUtils.isBlank(sourceNat)) {
				if (StringUtils.isNotBlank(other.sourceNat)) {
					return false;
				}
			} else if (!sourceNat.equals(other.sourceNat)) {
				return false;
			}
		}
		return true;
	}
}
