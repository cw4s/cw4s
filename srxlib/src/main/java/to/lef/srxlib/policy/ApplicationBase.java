package to.lef.srxlib.policy;


import javax.xml.xpath.XPath;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.Util;
import to.lef.srxlib.dom.XML;

public class ApplicationBase {
	protected String name;
	protected Integer timeout;

	protected String protocol;
	protected String dstLowPort;
	protected String dstHighPort;
	protected String srcLowPort;
	protected String srcHighPort;
	protected String icmpType;
	protected String icmpCode;
	protected String rpcLowNumber;
	protected String rpcHighNumber;
	protected String uuid;

	public final static boolean isProtocolTcp(String v) {
		return (StringUtils.isNotBlank(v) && (v.equals("tcp") || v.equals("6")));
	}

	public final boolean isProtocolTcp() {
		return isProtocolTcp(protocol);
	}
	
	public final static boolean isProtocolUdp(String v) {
		return (StringUtils.isNotBlank(v) && (v.equals("udp") || v.equals("17")));
	}

	public final boolean isProtocolUdp() {
		return isProtocolUdp(protocol);
	}
	
	public final static boolean isProtocolIcmp(String v) {
		return (StringUtils.isNotBlank(v) && (v.equals("icmp") || v.equals("1")));
	}

	public final boolean isProtocolIcmp() {
		return isProtocolIcmp(protocol);
	}
	
	public final static boolean isProtocolIcmp6(String v) {
		return (StringUtils.isNotBlank(v) && (v.equals("icmp6") || v.equals("58")));
	}

	public final boolean isProtocolIcmp6() {
		return isProtocolIcmp6(protocol);
	}
	
	protected ApplicationBase(String name) {
		this.name = name;
	}

	public final Integer getTimeout() {
		return timeout;
	}

	public final void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}

	public final void setTimeout(String timeout) {
		this.timeout = null;
		if (StringUtils.isNotBlank(timeout)) {
			if (timeout.equals("never"))
				this.timeout = 0;
			else
				this.timeout = Integer.valueOf(timeout);
		}
	}
	
	public final String getProtocol() {
		return protocol;
	}

	public final void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public final String getDstLowPort() {
		return dstLowPort;
	}

	public final void setDstLowPort(String dstLowPort) {
		this.dstLowPort = dstLowPort;
	}

	public final String getDstHighPort() {
		return dstHighPort;
	}

	public final void setDstHighPort(String dstHighPort) {
		this.dstHighPort = dstHighPort;
	}

	public final void setDstPort(String port) {
		final String[] s = Util.parseNumberRange(port);
		
		if (s.length == 2) {
			setDstPort(s[0], s[1]);
		}
		else {
			setDstPort(s[0], null);
		}
	}
	
	public final void setDstPort(String lowPort, String highPort) {
		if (lowPort != null && lowPort.equals(highPort)) {
			this.dstLowPort  = lowPort;
			this.dstHighPort = null;
		}
		else {
			this.dstLowPort  = lowPort;
			this.dstHighPort = highPort;
		}
	}

	public final String displayDstPort() {
		return Util.displayNumberRange(dstLowPort, dstHighPort);
	}
	
	public final String getSrcLowPort() {
		return srcLowPort;
	}

	public final void setSrcLowPort(String srcLowPort) {
		this.srcLowPort = srcLowPort;
	}

	public final String getSrcHighPort() {
		return srcHighPort;
	}

	public final void setSrcHighPort(String srcHighPort) {
		this.srcHighPort = srcHighPort;
	}

	public final void setSrcPort(String port) {
		final String[] s = Util.parseNumberRange(port);
		
		if (s.length == 2) {
			setSrcPort(s[0], s[1]);
		}
		else {
			setSrcPort(s[0], null);
		}
	}
	
	public final void setSrcPort(String lowPort, String highPort) {
		if (lowPort != null && lowPort.equals(highPort)) {
			this.srcLowPort  = lowPort;
			this.srcHighPort = null;		
		}
		else {
			this.srcLowPort  = lowPort;
			this.srcHighPort = highPort;
		}
	}

	public final String displaySrcPort() {
		return Util.displayNumberRange(srcLowPort, srcHighPort);
	}
	
	public final String getIcmpType() {
		return icmpType;
	}

	public final void setIcmpType(String icmpType) {
		this.icmpType = icmpType;
	}

	public final String getIcmpCode() {
		return icmpCode;
	}

	public final void setIcmpCode(String icmpCode) {
		this.icmpCode = icmpCode;
	}

	public final String getRpcLowNumber() {
		return rpcLowNumber;
	}

	public final void setRpcLowNumber(String rpcLowNumber) {
		this.rpcLowNumber = rpcLowNumber;
	}

	public final String getRpcHighNumber() {
		return rpcHighNumber;
	}

	public final void setRpcHighNumber(String rpcHighNumber) {
		this.rpcHighNumber = rpcHighNumber;
	}
	
	public final void setRpcNumber(String rpcNumber) {
		final String[] s = Util.parseNumberRange(rpcNumber);
		if (s.length == 2) {
			setRpcNumber(s[0], s[1]);
		}
		else {
			setRpcNumber(s[0], null);
		}
	}
	
	public final void setRpcNumber(String lowNumber, String highNumber) {
		if (lowNumber != null && lowNumber.equals(highNumber)) {
			this.rpcLowNumber  = lowNumber;
			this.rpcHighNumber = null;	
		}
		else {
			this.rpcLowNumber  = lowNumber;
			this.rpcHighNumber = highNumber;
		}
	}

	public final String displayRpcNumber() {
		return Util.displayNumberRange(rpcLowNumber, rpcHighNumber);
	}
	
	public final String getUuid() {
		return uuid;
	}

	public final void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public final String getName() {
		return name;
	}
	
	protected void constructFromNode(final Node n, final XML cfg) {
		final XPath xpath = cfg.getXPathInstance();
		
		setProtocol (XML.getString(xpath, n, "protocol/text()"));
		setSrcPort  (XML.getString(xpath, n, "source-port/text()"));
		setDstPort  (XML.getString(xpath, n, "destination-port/text()"));
		setRpcNumber(XML.getString(xpath, n, "rpc-program-number/text()"));
		setIcmpType (XML.getString(xpath, n, "icmp-type/text()"));
		setIcmpCode (XML.getString(xpath, n, "icmp-code/text()"));
		setUuid     (XML.getString(xpath, n, "uuid/text()"));
		setTimeout  (XML.getString(xpath, n, "inactivity-timeout/text()"));
	}

	protected void configTimeout(Node app, DeviceConfig cfg) {
		final String timeoutTag = "inactivity-timeout";
		cfg.deleteNode(app, timeoutTag);
		if (timeout != null && StringUtils.isNotBlank(protocol)) {
			if (timeout == 0) {
				app.insertBefore(cfg.createElement(timeoutTag, "never"), cfg.getNode(app, "term"));
			} else if (Util.isValidApplicationTimeout(timeout)) {
				app.insertBefore(cfg.createElement(timeoutTag, "%d", timeout), cfg.getNode(app, "term"));
			}
		}	
	}
	
	public void configBasic(Node app, DeviceConfig cfg) {
		final String nameTag    = "name";
	
		cfg.deleteNode(app, nameTag);
		app.insertBefore(cfg.createElement(nameTag, name), app.getFirstChild());
		
		configTimeout(app, cfg);
	}
	
	public boolean configCustom(final Node a, DeviceConfig cfg) {
		final String tagProtocol  = "protocol";
		final String tagSrcPort   = "source-port";
		final String tagDstPort   = "destination-port";
		final String tagIcmpType  = "icmp-type";
		final String tagIcmpCode  = "icmp-code";
		final String tagRpcNumber = "rpc-program-number";
		final String tagUuid      = "uuid";
		
		cfg.deleteNode(a, tagProtocol);
		cfg.deleteNode(a, tagSrcPort);
		cfg.deleteNode(a, tagDstPort);
		cfg.deleteNode(a, tagIcmpType);
		cfg.deleteNode(a, tagIcmpCode);
		cfg.deleteNode(a, tagRpcNumber);
		cfg.deleteNode(a, tagUuid);

		if (StringUtils.isNotBlank(protocol)) {
			a.appendChild(cfg.createElement(tagProtocol, protocol));

			if (isProtocolTcp() || isProtocolUdp()) {
				String s;
				s = displaySrcPort();
				if (StringUtils.isNotBlank(s)) {
					a.appendChild(cfg.createElement(tagSrcPort, s));
				}
				s = displayDstPort();
				if (StringUtils.isNotBlank(s)) {
					a.appendChild(cfg.createElement(tagDstPort, s));
				}
			}
			if (isProtocolIcmp() || isProtocolIcmp6()) {
				if (StringUtils.isNotBlank(icmpType)){
					a.appendChild(cfg.createElement(tagIcmpType, icmpType));
				}
				if (StringUtils.isNotBlank(icmpCode)){
					a.appendChild(cfg.createElement(tagIcmpCode, icmpCode));
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dstHighPort == null) ? 0 : dstHighPort.hashCode());
		result = prime * result + ((dstLowPort == null) ? 0 : dstLowPort.hashCode());
		result = prime * result + ((icmpCode == null) ? 0 : icmpCode.hashCode());
		result = prime * result + ((icmpType == null) ? 0 : icmpType.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((protocol == null) ? 0 : protocol.hashCode());
		result = prime * result + ((rpcHighNumber == null) ? 0 : rpcHighNumber.hashCode());
		result = prime * result + ((rpcLowNumber == null) ? 0 : rpcLowNumber.hashCode());
		result = prime * result + ((srcHighPort == null) ? 0 : srcHighPort.hashCode());
		result = prime * result + ((srcLowPort == null) ? 0 : srcLowPort.hashCode());
		result = prime * result + ((timeout == null) ? 0 : timeout.hashCode());
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ApplicationBase))
			return false;
		ApplicationBase other = (ApplicationBase) obj;
		if (StringUtils.isBlank(dstHighPort)) {
			if (StringUtils.isNotBlank(other.dstHighPort))
				return false;
		} else if (!dstHighPort.equals(other.dstHighPort))
			return false;
		if (StringUtils.isBlank(dstLowPort)) {
			if (StringUtils.isNotBlank(other.dstLowPort))
				return false;
		} else if (!dstLowPort.equals(other.dstLowPort))
			return false;
		if (StringUtils.isBlank(icmpCode)) {
			if (StringUtils.isNotBlank(other.icmpCode)) 
				return false;
		} else if (!icmpCode.equals(other.icmpCode))
			return false;
		if (StringUtils.isBlank(icmpType)) {
			if (StringUtils.isNotBlank(other.icmpType))
				return false;
		} else if (!icmpType.equals(other.icmpType))
			return false;
		if (StringUtils.isBlank(name)) {
			if (StringUtils.isNotBlank(other.name))
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (StringUtils.isBlank(protocol)) {
			if (StringUtils.isNotBlank(other.protocol))
				return false;
		} else if (!protocol.equals(other.protocol))
			return false;
		if (StringUtils.isBlank(rpcHighNumber)) {
			if (StringUtils.isNotBlank(other.rpcHighNumber))
				return false;
		} else if (!rpcHighNumber.equals(other.rpcHighNumber))
			return false;
		if (StringUtils.isBlank(rpcLowNumber)) {
			if (StringUtils.isNotBlank(other.rpcLowNumber))
				return false;
		} else if (!rpcLowNumber.equals(other.rpcLowNumber))
			return false;
		if (StringUtils.isBlank(srcHighPort)) {
			if (StringUtils.isNotBlank(other.srcHighPort))
				return false;
		} else if (!srcHighPort.equals(other.srcHighPort))
			return false;
		if (StringUtils.isBlank(srcLowPort)) {
			if (StringUtils.isNotBlank(other.srcLowPort))
				return false;
		} else if (!srcLowPort.equals(other.srcLowPort))
			return false;
		if (timeout == null) {
			if (other.timeout != null)
				return false;
		} else if (!timeout.equals(other.timeout))
			return false;
		if (StringUtils.isBlank(uuid)) {
			if (StringUtils.isNotBlank(other.uuid))
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
}
