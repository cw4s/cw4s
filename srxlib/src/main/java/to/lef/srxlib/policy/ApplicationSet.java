package to.lef.srxlib.policy;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import javax.xml.xpath.XPath;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;

public class ApplicationSet {
	private final String name;
	private final List<String> applicationNameList;
	private final List<String> applicationSetNameList;

	public static ApplicationSet createFromNode(Node as, XML cfg) {
		final XPath xpath = cfg.getXPathInstance();
		String name = XML.getString(xpath, as, "name/text()");
		List<String> al  = new ArrayList<String>();
		List<String> asl = new ArrayList<String>();
		
		NodeList nl = XML.getNodeList(xpath,  as, "application/name");
		int nll = nl.getLength();
		for (int i = 0; i < nll; i++) {
			Node n = nl.item(i);
			String an = n.getTextContent();
			if (StringUtils.isNotBlank(an)) {
				al.add(an);
			}
		}
		
		nl = XML.getNodeList(xpath,  as, "application-set/name");
		nll = nl.getLength();
		for (int i = 0; i < nll; i++) {
			Node n = nl.item(i);
			String an = n.getTextContent();
			if (StringUtils.isNotBlank(an)) {
				asl.add(an);
			}
		}
		
		return new ApplicationSet(name, al, asl);
	}
	
	public ApplicationSet(String name, List<String> applicationNameList, List<String> applicationSetNameList) {
		this.name = name;
		this.applicationNameList = applicationNameList;
		this.applicationSetNameList = applicationSetNameList;
	}

	public final String getName() {
		return name;
	}

	public final List<String> getApplicationNameList() {
		return Collections.unmodifiableList(applicationNameList);
	}

	public final List<String> getApplicationSetNameList() {
		return Collections.unmodifiableList(applicationSetNameList);
	}

	public void configRename(String oldName, DeviceConfig cfg) {
		Node applications = cfg.applicationsNode();
		NodeList nl;
		nl = cfg.getNodeList(applications, "application-set/application-set/name[normalize-space(text())=%s]", XML.xpc(oldName));
		
		for (int i = 0; i < nl.getLength(); i++) {
			Node n = nl.item(i);
			XML.removeAllChildNodes(n);
			n.appendChild(cfg.createTextNode(name));
		}
		
		Node policies = cfg.policiesNode();
		nl = cfg.getNodeList(policies, "policy/policy/match/application[normalize-space(text())=%s]", XML.xpc(oldName));

		for (int i = 0; i < nl.getLength(); i++) {
			Node n = nl.item(i);
			XML.removeAllChildNodes(n);
			n.appendChild(cfg.createTextNode(name));
		}
	}
	
	protected void configSet(Node set, DeviceConfig cfg) {
		final String appTag = "application";
		final String setTag = "application-set";
		
		Node n = cfg.createElementsIfNotExist(set, "name");
		XML.removeAllChildNodes(n);
		n.appendChild(cfg.createTextNode(name));

		cfg.deleteNode(set, appTag);
		for(String na: applicationNameList) {
			if (!na.equals(name)) {
				n = cfg.createElement(appTag);
				n.appendChild(cfg.createElement("name", na));
				set.appendChild(n);
			}
		}

		cfg.deleteNode(set, setTag);
		for(String na: applicationSetNameList) {
			if (!na.equals(name)) {
				n = cfg.createElement(setTag);
				n.appendChild(cfg.createElement("name", na));
				set.appendChild(n);
			}
		}	
	}

	public final DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				ApplicationSet self = ApplicationSet.this;
			
				try {
					Node as = cfg.applicationsNode();
					Node s  = cfg.findApplicationSetNode(as, name);
					
					if (s != null) {
						throw new IllegalArgumentException("ApplicationSet is already exist.");
					}
					
					s = cfg.createElement("application-set");
					self.configSet(s, cfg);
				
					if (s.getChildNodes().getLength() > 1) 
						as.appendChild(s);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to create application set.", e);
				}
			}
			
		};
	}

	public final DeviceConfigure getEditConfigure(final ApplicationSet old) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				ApplicationSet self = ApplicationSet.this;
				
				if (self.equals(old))
					return;
				
				try {
					Node set = cfg.findApplicationSetNode(old.name);
					if (set == null) {
						throw new IllegalArgumentException("Address set is not found.");
					}
					
					self.configSet(set, cfg);
					
					if (!self.name.equals(old.name)) {
						self.configRename(old.name, cfg);
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to edit address set.", e);
				}				
			}
			
		};
	}

	public final DeviceConfigure getDeleteConfigure() {
		return getDeleteConfigure(name);
	}
	
	public final static DeviceConfigure getDeleteConfigure(final String name) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					if (isRemovable(name, cfg)) {
						Node as = cfg.applicationsNode();
						Node s  = cfg.findApplicationSetNode(as, name);

						if (s != null) {
							s.getParentNode().removeChild(s);
						}
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to delete address-set.", e);
				}
			}

		};
	}

	public boolean isEditable(DeviceConfig cfg) {
		return isEditable(name, cfg);
	}

	public static boolean isEditable(String name, DeviceConfig cfg) {
		if (name.indexOf("junos-") == 0 || name.equals("any"))
			return false;
		return true;	
	}
	
	public boolean isRemovable(DeviceConfig cfg) {
		return isRemovable(name, cfg);
	}

	public static boolean isRemovable(String name, DeviceConfig cfg) {
		if (name.indexOf("junos-") == 0 || name.equals("any"))
			return false;

		return !isInUse(name, cfg);
	}
	
	public boolean isInUse(DeviceConfig cfg) {
		return isInUse(getName(), cfg);
	}
	
	public static boolean isInUse(String name, DeviceConfig cfg) {
		Node apps = cfg.applicationsNode();
		if (apps != null) {
			Node n = cfg.getNode(apps, "application-set/application-set/name[normalize-space(text())=%s]/..", XML.xpc(name));
			if (n != null)
				return true;
		}

		Node policies = cfg.policiesNode();
		if (policies != null) {
			Node n = cfg.getNode(policies, "policy/policy/match/application[normalize-space(text())=%s]/..", XML.xpc(name));
			if (n != null)
				return true;
		}

		return false;
	}

	public int isChanged(DeviceConfig candidate) {
		if (name.indexOf("junos-") == 0 || name.equals("any"))
			return 0;

		ApplicationSet c = candidate.findApplicationSet(name);
		if (c == null)
			return 1;
		if (!equals(c)) 
			return 2;
		return 0;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((applicationNameList == null) ? 0 : applicationNameList .hashCode());
		result = prime * result + ((applicationSetNameList == null) ? 0 : applicationSetNameList.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ApplicationSet other = (ApplicationSet) obj;
		if (applicationNameList == null) {
			if (other.applicationNameList != null) {
				return false;
			}
		} else if (!(other.applicationNameList != null && applicationNameList.size() == other.applicationNameList.size() && applicationNameList.containsAll(other.applicationNameList))) {
			return false;
		}
		if (applicationSetNameList == null) {
			if (other.applicationSetNameList != null) {
				return false;
			}
		} else if (!(other.applicationSetNameList != null && applicationSetNameList.size() == other.applicationSetNameList.size() && applicationSetNameList.containsAll(other.applicationSetNameList))) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}
}
