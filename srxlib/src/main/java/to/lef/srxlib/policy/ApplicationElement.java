package to.lef.srxlib.policy;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import javax.xml.xpath.XPath;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;

public class ApplicationElement extends ApplicationBase {
	private String applicationProtocol;
	private List<ApplicationTerm> termList;

	public static ApplicationElement createInstance(String name, Integer timeout, List<ApplicationTerm> terms) {
		ApplicationElement ret = new ApplicationElement(name);
		ret.setTimeout(timeout);
		ret.termList.clear();
		for(ApplicationTerm term: terms) {
			ret.termList.add(term);
		}
		return ret;
	}
	
	public static ApplicationElement createFromNode(final Node n, final XML cfg) {
		final XPath xpath = cfg.getXPathInstance();
		final String name = XML.getString(xpath, n, "name/text()");
		
		if (StringUtils.isBlank(name))
			return null;
		
		ApplicationElement o = new ApplicationElement(name);
		o.constructFromNode(n, cfg);
		
		return o;
	}
	
	public ApplicationElement(String name) {
		super(name);
		termList = new ArrayList<ApplicationTerm>();
	}

	public final String getApplicationProtocol() {
		return applicationProtocol;
	}

	public final void setApplicationProtocol(String applicationProtocol) {
		this.applicationProtocol = applicationProtocol;
	}
	
	public final List<ApplicationTerm> getTermList() {
		return Collections.unmodifiableList(termList);
	}

	@Override
	protected void constructFromNode(final Node n, final XML cfg) {
		super.constructFromNode(n, cfg);
		final XPath xpath = cfg.getXPathInstance();
		setApplicationProtocol(XML.getString(xpath, n, "application-protocol/text()"));
		
		termList.clear();
		NodeList terms = XML.getNodeList(xpath, n, "term");
		
		for (int i = 0; i < terms.getLength(); i++) {
			ApplicationTerm term = ApplicationTerm.createFromNode(terms.item(i), cfg);
			if (term != null)
				termList.add(term);
		}
		
		if (timeout == null && !termList.isEmpty())
			timeout = termList.get(0).timeout;
	}

	public String createTermName() {
		for (int i = 1; ; i++) {
			String name = "t" + i;
			if (!isTermExist(name) && !name.equals(this.name))
				return name;
		}
	}
	
	public boolean isTermExist(String name) {
		for (ApplicationTerm term: termList) {
			String tn = term.getName();
			if (StringUtils.isNotBlank(tn) && tn.equals(name))
				return true;
		}
		return false;
	}

	public void configTerms(Node n, DeviceConfig cfg) {
		configTerms(n, null, cfg);
	}

	public void configRename(String oldName, DeviceConfig cfg) {
		Node applications = cfg.applicationsNode();
		NodeList nl;
		nl = cfg.getNodeList(applications, "application-set/application/name[normalize-space(text())=%s]", XML.xpc(oldName));
		
		for (int i = 0; i < nl.getLength(); i++) {
			Node n = nl.item(i);
			XML.removeAllChildNodes(n);
			n.appendChild(cfg.createTextNode(name));
		}
		
		Node policies = cfg.policiesNode();
		nl = cfg.getNodeList(policies, "policy/policy/match/application[normalize-space(text())=%s]", XML.xpc(oldName));

		for (int i = 0; i < nl.getLength(); i++) {
			Node n = nl.item(i);
			XML.removeAllChildNodes(n);
			n.appendChild(cfg.createTextNode(name));
		}
	}
	
	public void configTerms(Node app, ApplicationElement old, DeviceConfig cfg) {
		if (old != null) {
			List<ApplicationTerm> newList = getTermList();
			List<ApplicationTerm> oldList = old.getTermList();
		
			for (ApplicationTerm oTerm: oldList) {
				boolean found = false;
				for (ApplicationTerm nTerm: newList) {
					if (oTerm.getName().equals(nTerm.getName())) {
						found = true;
						break;
					}
				}
				
				if (found == false) {
					cfg.deleteNode(app, "term[normalize-space(name/text())=%s]", XML.xpc(oTerm.getName()));
				}
			}
		}
		
		for (ApplicationTerm term: termList){ 
			term.setTimeout(timeout);
			if (StringUtils.isBlank(term.getName())) {
				term.setName(createTermName());
			}	
		
			Node t = cfg.findApplicationTermNode(app, term.getName());
			if (t == null) {
				t = cfg.createElement("term");
				t.appendChild(cfg.createElement("name", term.getName()));
				app.appendChild(t);
			}
			term.configCustom(t, cfg);
			term.configTimeout(t, cfg);

			if (t != null) {
				if (t.getChildNodes().getLength() <= 1) {
					Node p = t.getParentNode();
					if (p != null) {
						p.removeChild(t);
					}
				}
			}
		}	
	}

	private boolean isValidCustomTermExists() {
		boolean isTermOk = false;
		for (ApplicationTerm term: termList) {
			if (StringUtils.isNotBlank(term.getProtocol())) {
				isTermOk = true;
				break;
			}
		}

		return isTermOk;
	}
	
	public DeviceConfigure getCreateCustomConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				final ApplicationElement self = ApplicationElement.this;
				
				try {
					Node as = cfg.applicationsNode();
					Node a = cfg.findApplicationNode(as, self.name);
					if (a != null) {
						throw new IllegalArgumentException("Application is already exist.");
					}
				
					if (!self.isValidCustomTermExists()) {
						throw new IllegalArgumentException("No valid service.");
					}
					
					a = cfg.createElement("application");

					self.configBasic(a, cfg);
					self.configTerms(a, cfg);
					
					as.appendChild(a);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to create application.", e);
				}
			}
			
		};
	}
	
	public DeviceConfigure getEditCustomConfigure(final ApplicationElement old) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				final ApplicationElement self = ApplicationElement.this;
		
				if (self.equals(old))
					return;
				
				try {
					Node as = cfg.applicationsNode();
					String oldName = old.getName();
					
					Node a = cfg.findApplicationNode(as, oldName);
					if (a == null) {
						throw new IllegalArgumentException("Application is not exist.");
					}
					
					if (!self.isValidCustomTermExists()) {
						throw new IllegalArgumentException("No valid service.");
					}
					
					self.configBasic(a, cfg);
					self.configTerms(a, old, cfg);
					
					if (!self.name.equals(oldName)) {
						configRename(oldName, cfg);
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to edit application.", e);
				}
			}
			
		};
	}

	public DeviceConfigure getDeleteConfigure() {
		return getDeleteConfigure(name);
	}
	
	public static DeviceConfigure getDeleteConfigure(final String appName) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					Node a = cfg.findApplicationNode(appName);
					
					if (a != null) {
						a.getParentNode().removeChild(a);
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to delete application.", e);
				}
			}
			
		};
	}
	
	public boolean isCustomEditable() {
		//TODO
		if (name.indexOf("junos-") == 0)
			return false;
		return true;
	}
	
	public final boolean isRemovable(DeviceConfig cfg) {
		return isRemovable(getName(), cfg);
	}
	
	public static boolean isRemovable(String appName, DeviceConfig cfg) {
		if (appName.indexOf("junos-") == 0)
			return false;

		return !isInUse(appName, cfg);
	}

	public final boolean isInUse(DeviceConfig cfg) {
		return isInUse(getName(), cfg);
	}
	
	public static boolean isInUse(String appName, DeviceConfig cfg) {
		Node applications = cfg.applicationsNode();
		Node n = cfg.getNode(applications, "application-set/application[normalize-space(name/text())=%s]", XML.xpc(appName));
		if (n != null)
			return true;
		
		Node policies = cfg.policiesNode();
		n = cfg.getNode(policies, "policy/policy/match/application[normalize-space(text())=%s]", XML.xpc(appName));
		if (n != null)
			return true;

		return false;
	}

	public int isChanged(DeviceConfig candidate) {
		ApplicationElement c = candidate.findApplication(name);
		if (c == null)
			return 1;
		if (!equals(c)) 
			return 2;
		return 0;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((applicationProtocol == null) ? 0 : applicationProtocol .hashCode());
		result = prime * result + ((termList == null) ? 0 : termList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplicationElement other = (ApplicationElement) obj;
		if (StringUtils.isBlank(applicationProtocol)) {
			if (StringUtils.isNotBlank(other.applicationProtocol))
				return false;
		} else if (!applicationProtocol.equals(other.applicationProtocol))
			return false;
		if (termList == null) {
			if (other.termList != null)
				return false;
		} else if (!(other.termList != null && termList.size() == other.termList.size() && termList.containsAll(other.termList)))
			return false;
		return true;
	}
}
