package to.lef.srxlib;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;

import net.juniper.netconf.NetconfException;


public class DeviceFactory {
	private final ConcurrentHashMap<String, Device> map;
	private final File cacheDir;

	private static DeviceFactory instance = new DeviceFactory();
	public static DeviceFactory getInstance() {
		return instance;
	}

	private DeviceFactory() {
		this.map = new ConcurrentHashMap<String, Device>();
		this.cacheDir = new File(new File(System.getProperty("user.dir"), "cache"), "device");
	}
	
	public Device apply(String name, String address, String username, String password) throws NetconfException, ParserConfigurationException {
		return apply(name, address, username, password, false, Device.DEFAULT_PORT);
	}
	
	public Device apply(String name, String address, String username, String password, boolean persistent) throws NetconfException, ParserConfigurationException {
		return apply(name, address, username, password, persistent, Device.DEFAULT_PORT);
	}
	
	public synchronized Device apply(String name, String address, String username, String password, boolean persistent, int port) throws NetconfException, ParserConfigurationException {
		Device d = null;
	
		if (StringUtils.isNotBlank(name)) {
			d = map.get(name);
			File dir = new File(cacheDir, address);
			if (!dir.exists()) {
				dir.mkdirs();
			}	

			if (d == null) {
				d = new Device(address, username, password, port, dir);
				if (d != null) {
					d.setPersistent(persistent);
					d.prepare();
					d.setName(name);

					map.put(name, d);
				}
			}
		}
		return d;
	}

	public final void clear(String name) throws IOException {
		clear(name, null);
	}
	
	public synchronized void clear(String name, String address) throws IOException {
		if (StringUtils.isNotBlank(name)) {
			Device d = map.get(name);
			if (d != null) {
				d.close();
			}
			if (StringUtils.isNotBlank(address)) {
				File dir = new File(cacheDir, address);
				if (dir.exists()) {
					Files.walkFileTree(dir.toPath(), new SimpleFileVisitor<Path>() {

						@Override
						public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
							Files.delete(file);
							return FileVisitResult.CONTINUE;
						}

						@Override
						public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
//							Files.delete(file);
							return FileVisitResult.CONTINUE;
						}

						@Override
						public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
							if (exc == null) {
								Files.delete(dir);
								return FileVisitResult.CONTINUE;								
							} else {
								throw exc;
							}
						}

					});
				}	
			}
			map.remove(name);
		}
	}

//	@Override
//	protected void finalize() throws Throwable {
//		try {
//			super.finalize();
//		}
//		finally {
//			for (Entry<String, Device> e: map.entrySet()) {
//				e.getValue().close();
//			}
//			map.clear();
//		}
//	}
}
