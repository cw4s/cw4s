package to.lef.srxlib.snmp;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;

public class Snmp {
	public static final String VERSION_ANY = "all";
	public static final String VERSION_V1  = "v1";
	public static final String VERSION_V2C = "v2";

	public static final String AUTHORIZATION_READ_ONLY = "read-only";
	public static final String AUTHORIZATION_READ_WRITE = "read-write";
	
	private String name;
	private String contact;
	private String location;
	private String description;
	
	public static Snmp createFromConfig(DeviceConfig config) {
		Node snmp = config.snmpNode();
		String name = config.getString(snmp, "name/text()");
		String contact = config.getString(snmp, "contact/text()");
		String location = config.getString(snmp, "location/text()");
		String description = config.getString(snmp, "description/text()");
		
		if (StringUtils.isBlank(name)) {
			name = config.getString("system/host-name/text()");
		}
		
		return new Snmp(name, contact, location, description);
	}
	
	public Snmp(String name, String contact, String location, String description) {
		this.name = name;
		this.contact = contact;
		this.location = location;
		this.description = description;
	}
	
	public DeviceConfigure getEditConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				Node snmp = cfg.snmpNode();
				String hostName = cfg.getString("system/host-name/text()");
			
				cfg.deleteNode(snmp, "name");
				if (StringUtils.isNotBlank(name) && !name.equals(hostName)) {
					snmp.appendChild(cfg.createElement("name", name));
				}
				
				cfg.deleteNode("contact");
				if (StringUtils.isNotBlank(contact)) {
					snmp.appendChild(cfg.createElement("contact", contact));
				}
				
				cfg.deleteNode("location");
				if (StringUtils.isNotBlank(location)) {
					snmp.appendChild(cfg.createElement("location", location));
				}
				
				cfg.deleteNode("description");
				if (StringUtils.isNotBlank(description)) {
					snmp.appendChild(cfg.createElement("description", description));
				}			
				
				if (cfg.getNode(snmp, "*") == null) {
					snmp.getParentNode().removeChild(snmp);
				}
			}
		};
	}

	public final String getName() {
		return name;
	}

	public final String getContact() {
		return contact;
	}

	public final String getLocation() {
		return location;
	}
	
	public final String getDescription() {
		return description;
	}
}
