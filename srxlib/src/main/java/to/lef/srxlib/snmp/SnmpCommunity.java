package to.lef.srxlib.snmp;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.Util;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class SnmpCommunity {
	private String name;
	private String trapVersion;
	boolean writeEnabled;
	boolean trapEnabled;
	private List<String> trapTargetList;

	public static SnmpCommunity createFromNode(Node com, Node snmp, DeviceConfig config) {
		String name = config.getString(com, "name/text()");
		if (StringUtils.isBlank(name))
			return null;
	
		boolean writeEnabled = false;
		if (config.getNode(com, "authorization[normalize-space(text())=%s]", XML.xpc(Snmp.AUTHORIZATION_READ_WRITE)) != null)
			writeEnabled = true;
		
		boolean trapEnabled = false;
		String trapVersion = Snmp.VERSION_ANY;
		
		Node trap = config.findSnmpTrapGroupNode(name, snmp);
		if (trap != null) {
			trapEnabled = true;
			trapVersion = config.getString(trap, "version/text()");
			if (StringUtils.isBlank(trapVersion))
				trapVersion = Snmp.VERSION_ANY;
		}
	
		SnmpCommunity ret = new SnmpCommunity(name, writeEnabled, trapEnabled, trapVersion);
		if (trapEnabled) {
			for (Node target : IterableNodeList.apply(config.getNodeList(trap, "targets"))) {
				String targetName = config.getString(target, "name/text()");
				ret.addTrapTarget(targetName);
			}
		}
		return ret;
	}
	
	public SnmpCommunity(String name, boolean writeEnabled, boolean trapEnabled, String version) {
		this.name = name;
		this.trapVersion = version;
		this.writeEnabled = writeEnabled;
		this.trapEnabled = trapEnabled;
		this.trapTargetList = new ArrayList<>();
	}

	public final String getName() {
		return name;
	}

	public final String getTrapVersion() {
		return trapVersion;
	}

	public final boolean isWriteEnabled() {
		return writeEnabled;
	}

	public final boolean isTrapEnabled() {
		return trapEnabled;
	}

	public void addTrapTarget(String v) {
		try {
			if (StringUtils.isBlank(v))
				return;
			
			String addr = Util.fixInetAddressString(v);
			
			if (trapTargetList.contains(addr))
				return;
			
			trapTargetList.add(addr);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	
	public final List<String> getTrapTargetList() {
		return Collections.unmodifiableList(trapTargetList);
	}

	private void configAuthorization(Node node, DeviceConfig config) {
		final String tag = "authorization";
		config.deleteNode(node, tag);
		if (writeEnabled) {
			node.appendChild(config.createElement(tag, Snmp.AUTHORIZATION_READ_WRITE));
		}
		else {
			node.appendChild(config.createElement(tag, Snmp.AUTHORIZATION_READ_ONLY));
		}					
	}

	private void configTrapGroup(Node snmp, DeviceConfig config) {
		Node trap = config.findSnmpTrapGroupNode(name, snmp);
		if (trapEnabled) {
			if (trap == null) {
				trap = config.createElement("trap-group");
				trap.appendChild(config.createElement("name", name));
				snmp.appendChild(trap);
			}
		
			config.deleteNode(trap, "version");
			trap.appendChild(config.createElement("version", trapVersion));
		}
		else if (trap != null) {
			snmp.removeChild(trap);
		}
	}
	
	public DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					Node snmp = config.snmpNode();
					if (config.findSnmpCommunityNode(name) != null)
						throw new IllegalArgumentException("Snmp community is already exist.");
					if (config.findSnmpTrapGroupNode(name) != null)
						throw new IllegalArgumentException("Snmp trap group is already exist.");
					
					Node community = config.createElement("community");
					community.appendChild(config.createElement("name", name));
					
					configAuthorization(community, config);
					
					snmp.insertBefore(community, config.getNode(snmp, "trap-group"));
					
					configTrapGroup(snmp, config);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config snmp community.", e);
				}
			}
			
		};
	}
	
	public DeviceConfigure getEditConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					Node snmp = config.snmpNode();
					
					Node community = config.findSnmpCommunityNode(name, snmp);
					if (community == null)
						throw new IllegalArgumentException("Snmp community is not configured.");
					
					configAuthorization(community, config);
					
					configTrapGroup(snmp, config);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config snmp community.", e);
				}
			}
			
		};
	}
	
	public DeviceConfigure getTrapTargetConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				final String tag = "targets";
				try {
					Node snmp = config.snmpNode();
					Node trap = config.findSnmpTrapGroupNode(name, snmp);
					
					if (trap == null) 
						throw new IllegalArgumentException("Trap group is not configured.");
				
					config.deleteNode(trap, tag);
					for (String target: trapTargetList) {
						Node t = config.createElement(tag);
						t.appendChild(config.createElement("name", target));
						trap.appendChild(t);
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config trap target.", e);
				}
			}
			
		};
	}
	
	public DeviceConfigure getDeleteTrapTargetConfigure(final String target) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				if (StringUtils.isBlank(target))
					return;
				
				try {
					Node trap = config.findSnmpTrapGroupNode(name);
					
					if (trap == null) 
						throw new IllegalArgumentException("Trap group is not configured.");
				
					config.deleteNode(trap, "targets[normalize-space(name/text())=%s]", XML.xpc(target));
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config trap target.", e);
				}
			}
			
		};	
	}
	
	public DeviceConfigure getDeleteConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					Node snmp = config.snmpNode();
					Node community = config.findSnmpCommunityNode(name, snmp);
					Node trap = config.findSnmpTrapGroupNode(name, snmp);
					
					if (community != null)
						snmp.removeChild(community);
					if (trap != null)
						snmp.removeChild(trap);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config community.", e);
				}
			}
			
		};
	}
	
	public int isChanged(Map<String, SnmpCommunity> candidate) {
        SnmpCommunity c = candidate.get(getName());

        if (c == null)
        	return 1;
        if (!equals(c))
        	return 2;

        return 0;
	}
}
