package to.lef.srxlib.nic;

import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class VPNInterface extends LogicalInetInterface {
	public static final String PHYSICAL_INTERFACE_NAME = "st0";
	
	public VPNInterface(int unitId) {
		super("st0", unitId);
	}

	@Override
	public boolean isRemovable(DeviceConfig cfg) {
		if (!super.isRemovable(cfg))
			return false;
		
		for (Node vpn: IterableNodeList.apply(cfg.getNodeList(cfg.ipsecNode(), "vpn"))) {
			if (cfg.getNode(vpn, "bind-interface[normalize-space(text())=%s]", XML.xpc(getName())) != null)
					return false;
		}
		
		return true;
	}

	@Override
	protected void configCreate(Node unit, DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
		super.configCreate(unit, cfg, info);
		fixTcpMss(cfg);
	}

	@Override
	protected void configDelete(Node unit, DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
		super.configDelete(unit, cfg, info);
		fixTcpMss(cfg);
	}
	
}
