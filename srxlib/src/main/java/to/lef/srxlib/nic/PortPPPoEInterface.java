package to.lef.srxlib.nic;

import to.lef.srxlib.DeviceConfig;

public class PortPPPoEInterface extends LogicalInterface {

	protected PortPPPoEInterface(String name, int unitId) {
		super(name, unitId, FAMILY_NONE);
	}

	@Override
	public boolean isPPPoEBindable(DeviceConfig cfg) {
		return (getUnitId() == 0);
	}

	@Override
	public boolean isRemovable(DeviceConfig cfg) {
		return false;
	}

}
