package to.lef.srxlib.nic;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import org.apache.commons.lang3.StringUtils;

import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigure;

abstract public class LogicalBoundInterface extends LogicalInterface implements BindableInterface {
	public final static String PORT_MODE_ACCESS        = "access";
	public final static String PORT_MODE_TRUNK         = "trunk";
	public final static String PORT_MODE_TAGGED_ACCESS = "tagged-access";

	protected List<String> bindInterfaceNameList;
	protected String portMode;
	
	protected LogicalBoundInterface(String name, int unitId, String family) {
		super(name, unitId, family);
		this.portMode              = PORT_MODE_ACCESS;
		this.bindInterfaceNameList = new ArrayList<String>();
	}
	
	public final String getPortMode() {
		return portMode;
	}
	
	public final boolean canChangePortMode(String bindInterface) {
		int size = bindInterfaceNameList.size();
		if (size < 1)
			return true;
	
		if (size == 1 && bindInterfaceNameList.contains(bindInterface)) 
			return true;

		return false;
	}

	@Override
	public final boolean canBoundBy(String bindInterface, DeviceConfig config) {
		if (!PORT_MODE_ACCESS.equals(portMode))
			return true;

		if (bindInterfaceNameList.contains(bindInterface))
			return true;
		

		
		if (bindInterfaceNameList.size() < 1)
			return true;
		
		return false;
	}
	
	public final List<String> getBindInterfaceNameList() {
		return Collections.unmodifiableList(bindInterfaceNameList);
	}
	
	public void addBindInterfaceName(String physicalInterfaceName, int unitId) {
		addBindInterfaceName(String.format("%s.%d", physicalInterfaceName, unitId));
	}
	
	public void addBindInterfaceName(String logicalInterfaceName) {
		bindInterfaceNameList.add(logicalInterfaceName);
	}

	abstract public DeviceConfigure getBindConfigure(String intName, String name, String portMode);
	abstract public DeviceConfigure getUnbindConfigure(String intName, String name);

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((bindInterfaceNameList == null) ? 0 : bindInterfaceNameList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof LogicalBoundInterface)) {
			return false;
		}
		LogicalBoundInterface other = (LogicalBoundInterface) obj;
		if (StringUtils.isBlank(portMode)) {
			if (StringUtils.isNotBlank(other.portMode)) {
				return false;
			}
		} else if (!portMode.equals(other.portMode)) {
			return false;
		}
		if (bindInterfaceNameList == null) {
			if (other.bindInterfaceNameList != null) {
				return false;
			}
		} else if (!(other.bindInterfaceNameList != null && bindInterfaceNameList.size() == other.bindInterfaceNameList.size() && bindInterfaceNameList.containsAll(other.bindInterfaceNameList))) {
			return false;
		}
		return true;
	}

}
