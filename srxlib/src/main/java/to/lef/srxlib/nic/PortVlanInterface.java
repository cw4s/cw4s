package to.lef.srxlib.nic;


import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;

public class PortVlanInterface extends LogicalVlanInterface {
	protected final String portModeTagName;
	
	public PortVlanInterface(String name, int unitId) {
		this(name, unitId, "port-mode");
	}

	protected PortVlanInterface(String name, int unitId, String portModeTagName) {
		super(name, unitId);
		this.portModeTagName = portModeTagName;
	}
	
	@Override
	protected void constructFromNode(Node unit, DeviceConfig config) {
		super.constructFromNode(unit, config);

		Node es = config.getNode(unit, "family/" + FAMILY_VLAN);
		
		if (es != null) {
			String portMode = config.getString(es, portModeTagName + "/text()");
			if (StringUtils.isBlank(portMode))
				portMode = PORT_MODE_ACCESS;
			this.portMode = portMode;
			NodeList nl = config.getNodeList(es, "vlan/members");
			for (int i = 0; i < nl.getLength(); i++) {
				String n = config.getString(nl.item(i), "text()");
				if (StringUtils.isNotBlank(n)) {
					Node vlan = config.findVlanNodeByName(n);
					if (vlan != null) {
						n = config.getString(vlan, "l3-interface/text()");
						if (StringUtils.isNotBlank(n))
							addBindInterfaceName(n);
					}
				}
			}
		}
	}

	protected void configBind(DeviceConfig cfg, String intName, String vname, String mode) {
		Node unit = cfg.findLogicalInterfaceNode(getPhysicalInterfaceName(), getUnitId());
		if (unit == null) {
			throw new IllegalArgumentException("logical interface is not found.");
		}

		Node family = cfg.createElementsIfNotExist(unit, "family");
		for (Node n: IterableNodeList.apply(family.getChildNodes())) {
			if (n.getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element)n;
				if (!FAMILY_VLAN.equals(e.getTagName())) {
					family.removeChild(n);
				}
			}
		}

		if (canChangePortMode(intName) && StringUtils.isNotBlank(mode)) {
			cfg.deleteNode(family, FAMILY_VLAN + "/" + portModeTagName);
			if (!PORT_MODE_ACCESS.equals(mode)) {
				Node n = cfg.createElementsIfNotExist(family, FAMILY_VLAN, portModeTagName);
				n.appendChild(cfg.createTextNode(mode));
			}
		}

		Node n = cfg.createElementsIfNotExist(family, FAMILY_VLAN, "vlan");
		Node m = cfg.getNode(n, "members[normalize-space()=%s]", XML.xpc(vname));
		if (m == null)
			n.appendChild(cfg.createElement("members", vname));
	}
	
	protected void configUnbind(DeviceConfig cfg, String intName, String vname) {
		Node unit = cfg.findLogicalInterfaceNode(getPhysicalInterfaceName(), getUnitId());
		if (unit == null) {
			throw new IllegalArgumentException("logical interface is not found.");
		}

		Node n = cfg.getNode(unit, "family/" + FAMILY_VLAN + "/vlan/members[normalize-space(text())=%s]", XML.xpc(vname));
		if (n != null) {
			Node p = n.getParentNode();
			p.removeChild(n);

			// 不要なタグを削除
			n = p;
			NodeList nl = cfg.getNodeList(n, "members");
			if (nl.getLength() == 0) {
				cfg.deleteNode(unit, "family");
			}
		}
	}

	@Override
	public DeviceConfigure getBindConfigure(final String intName, final String vname, final String mode) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				PortVlanInterface self = PortVlanInterface.this;
				try {
					self.configBind(cfg, intName, vname, mode);
				}
				catch (Throwable e) {
					throw new DeviceConfigException(String.format("Failed to bind %s to %s.", self.getName(), vname), e);
				}
			}
			
		};
	}

	
	@Override
	public DeviceConfigure getUnbindConfigure(String intName, final String name) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				PortVlanInterface self = PortVlanInterface.this;
				try {
					self.configUnbind(cfg, intName, name);
				}
				catch (Throwable e) {
					throw new DeviceConfigException(String.format("Failed to unbind %s from %s.", self.getName(), name), e);
				}
			}
			
		};
	}

	@Override
	public LogicalBoundInterface toBoundVlanInterface(DeviceConfig config) {
		return this;
	}
}
