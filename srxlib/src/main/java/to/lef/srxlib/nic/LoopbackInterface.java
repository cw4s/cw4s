package to.lef.srxlib.nic;

public class LoopbackInterface extends LogicalInetInterface {

	public LoopbackInterface(int unitId) {
		this("lo", unitId);
	}
	
	public LoopbackInterface(String name, int unitId) {
		super(name, unitId);
	}

}
