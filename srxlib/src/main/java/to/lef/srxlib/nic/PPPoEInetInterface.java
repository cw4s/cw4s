package to.lef.srxlib.nic;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;

public class PPPoEInetInterface extends LogicalInetInterface {
	
	public PPPoEInetInterface(String name, int unitId) {
		super(name, unitId);
	}

	@Override
	protected void configDelete(Node unit, DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
		String intName = cfg.getString(unit, "pppoe-options/underlying-interface/text()");
		
		super.configDelete(unit, cfg, info);
		
		if (StringUtils.isNotBlank(intName)) {
			NodeList nl = cfg.pppoeNodeListByInterface(intName);
			if (nl.getLength() == 0) {
				Node intNode = cfg.findLogicalInterfaceNode(intName);
		
				if (intNode != null) {
					cfg.deleteNode(intNode, "encapsulation");
					cfg.deleteNode(intNode, "family");
				}
			}
		}
		
		cfg.deleteNode("system/services/dhcp/propagate-ppp-settings[normalize-space(text())=%s]", XML.xpc(getName()));
		fixTcpMss(cfg);
	}

	@Override
	protected void configCreate(Node unit, DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
		super.configCreate(unit, cfg, info);
		fixTcpMss(cfg);
	}
	
}
