package to.lef.srxlib.nic;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.xpath.XPath;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.Util;
import to.lef.srxlib.dom.XML;

import java.util.Set;

public class VlanInterface extends LogicalBindInterface {
	protected VlanInterface(String name, int unitId, int vlanId) {
		super(name, unitId);
		this.vlanId = vlanId;
	}
	
	public VlanInterface(int unitId, int vlanId) {
		this("vlan", unitId, vlanId);
	}
	
	@Override
	protected void constructFromNode(Node unit, DeviceConfig config) {
		super.constructFromNode(unit, config);
		final XPath xpath = XML.createXpathInstance();

		String vlanName = config.getString(
				String.format("vlans/vlan/l3-interface[normalize-space(text())=%s]/../name/text()", 
						XML.xpc(getName())));

		NodeList ifList = config.getNodeList(
				String.format("interfaces/interface/unit/family/ethernet-switching/vlan/members[normalize-space(text())=%s]/../../../..", 
						XML.xpc(vlanName))); 

		for (int i = 0; i < ifList.getLength(); i++) {
			Node ifNode = ifList.item(i);
			String pname = XML.getString(xpath, ifNode, "../name/text()");
			int unitid = Integer.parseInt(XML.getString(xpath, ifNode, "name/text()"));
			addBindingInterfaceName(pname, unitid);
		}
	}

	/*
	@Override
	public DeviceConfigure getCreateConfigure(PhysicalInterfaceInformation phyInfo) {
		final DeviceConfigure c = super.getCreateConfigure(phyInfo);
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg) throws DeviceConfigException {
				c.config(cfg);
				
				try {
					Node vlans = cfg.vlansNode();
					Node vlan = cfg.getNode(vlans, "vlan[normalize-space(vlan-id/text())=%d]", getVlanId());
					if (vlan == null) {
						vlan = cfg.createElement("vlan");
						vlan.appendChild(cfg.createElement("name", "vlan-%d", getVlanId()));
						vlan.appendChild(cfg.createElement("vlan-id", "%d", getVlanId()));
						vlans.appendChild(vlan);
					}
					else {
						Node n = cfg.getNode(vlan, "l3-interface");
						if (n != null) {
							throw new IllegalArgumentException(String.format("Vlan ID %d is already bound.", getVlanId()));
						}
					}
					
					vlan.appendChild(cfg.createElement("l3-interface", getName()));
				} catch (Throwable e) {
					throw new DeviceConfigException("Failed to create vlan logical interface.", e);
				}
			}
			
		};
	}
	*/
	
	@Override
	public DeviceConfigure getBindConfigure(final Map<String, String> bindMap) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				VlanInterface self = VlanInterface.this;
			
				Set<String>  bindList    = bindMap.keySet();
				List<String> addList     = new ArrayList<String>();
				List<String> removeList  = new ArrayList<String>();
				List<String> currentList = self.getBindingInterfaceNameList();
				
				for (String name : currentList) {
					if (!bindList.contains(name))
						removeList.add(name);
				}
				
				for (String name: bindList) {
//					if (!currentList.contains(name))
						addList.add(name);
				}
				
				try {
					Node vlans = cfg.vlansNode();
					Node vlan = cfg.getNode(vlans, "vlan[normalize-space(l3-interface/text())=%s]", XML.xpc(self.getName()));
					if (vlan == null) {
						throw new IllegalArgumentException("Vlan is not found.");
					}
					String vlanName = cfg.getString(vlan, "name/text()");
					if (StringUtils.isBlank(vlanName)) {
						throw new IllegalArgumentException("Invalid vlan name.");
					}
					
					for(String name : addList) {
						LogicalInterface lif = cfg.findLogicalInterface(name);
						if (lif == null) {
							String[] tmp = name.split("\\.");
							if (tmp.length == 2) {
								if (Util.isPortPhysicalInterface(tmp[0])) {
									lif = new PortInetInterface(tmp[0], Integer.parseInt(tmp[1]));
									lif.getCreateConfigure().config(cfg, info);
								}
							}
						}
						if (lif != null && lif instanceof BindableInterface && ((BindableInterface)lif).canBoundBy(getName(), cfg)) {
							((BindableInterface)lif).toBoundVlanInterface(cfg).getBindConfigure(getName(), vlanName, bindMap.get(name)).config(cfg, info);
						}
					}
					
					for(String name : removeList) {
						LogicalInterface lif = cfg.findLogicalInterface(name);
						if (lif != null && lif instanceof BindableInterface) {
							((BindableInterface)lif).toBoundVlanInterface(cfg).getUnbindConfigure(getName(), vlanName).config(cfg, info);
						}
					}
				}
				catch (DeviceConfigException e) {
					throw e;
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config bind.", e);
				}
			}
		};
	}

	@Override
	protected void configCreate(Node unit, DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
		super.configCreate(unit, cfg, info);
		
		Node vlans = cfg.vlansNode();
		Node vlan = cfg.getNode(vlans, "vlan[normalize-space(vlan-id/text())=%d]", getVlanId());
		if (vlan == null) {
			vlan = cfg.createElement("vlan");
			vlan.appendChild(cfg.createElement("name", "vlan-%d", getVlanId()));
			vlan.appendChild(cfg.createElement("vlan-id", "%d", getVlanId()));
			vlans.appendChild(vlan);
		}
		else {
			Node n = cfg.getNode(vlan, "l3-interface");
			if (n != null) {
				throw new IllegalArgumentException(String.format("Vlan ID %d is already bound.", getVlanId()));
			}
		}

		vlan.appendChild(cfg.createElement("l3-interface", getName()));
	}

	@Override
	protected void configDelete(Node unit, DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
		try {
			Node vlans = cfg.vlansNode();
			Node vlan = cfg.getNode(vlans, "vlan[normalize-space(l3-interface/text())=%s]", XML.xpc(getName()));
			
			if (vlan != null) {
				String vlanName = cfg.getString(vlan, "name/text()");

				if (StringUtils.isNotBlank(vlanName)) {
					for (String name: getBindingInterfaceNameList()) {
						LogicalInterface lif = cfg.findLogicalInterface(name);
						if (lif != null && lif instanceof BindableInterface) {
							((BindableInterface)lif).toBoundVlanInterface(cfg).getUnbindConfigure(getName(), vlanName).config(cfg, info);
						}			
					}
				}

				vlans.removeChild(vlan);
			}
		}
		catch (Throwable e) {
			throw new DeviceConfigException("Failed to delete logical interface.", e);
		}
		
		super.configDelete(unit, cfg, info);
	}
}
