package to.lef.srxlib.nic;


public abstract class LogicalVlanInterface extends LogicalBoundInterface {
	protected LogicalVlanInterface(String name, int unitId) {
		super(name, unitId, FAMILY_VLAN);
	}

	@Override
	public void setZoneName(String v) {
		super.setZoneName("");
	}
}
