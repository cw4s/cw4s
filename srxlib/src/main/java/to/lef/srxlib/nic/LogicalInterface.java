package to.lef.srxlib.nic;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.PhysicalInterfaceInformation;
import to.lef.srxlib.zone.SecurityZone;
import to.lef.srxlib.Util;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.l2ng.L2ngVlanInterface;
import to.lef.srxlib.l2ng.L2ngPortVlanInterface;

public class LogicalInterface {
	public final static String FAMILY_NONE   = "none";
	public final static String FAMILY_INET   = "inet";
	public final static String FAMILY_INET6  = "inet6";
	public final static String FAMILY_VLAN   = "ethernet-switching";
	public final static String FAMILY_BRIDGE = "bridge";
	public final static String ENCAPSULATION_PPPOE = "ppp-over-ether";
	private final String physicalInterfaceName;
	private final int unitId;
	private final String family;
	private String zoneName;
	protected Integer vlanId;
	private boolean disabled;
	
	public static LogicalInterface createFromNode(String physicalInterfaceName, Node node, DeviceConfig config) {
		return createFromNode(physicalInterfaceName, Integer.parseInt(config.getString(node, "name/text()")), node, config);
	}
	
	public static LogicalInterface createFromNode(String physicalInterfaceName, int unitId, Node node, DeviceConfig config) {
		LogicalInterface ret = null;
		Node tmp = null;

		if (physicalInterfaceName.indexOf("reth") == 0) {
			ret = new RethInterface(physicalInterfaceName, unitId);
		}
		else if (physicalInterfaceName.startsWith("ip-")) {
			ret = new IpOverIpInterface(physicalInterfaceName, unitId);
		}
		else if (physicalInterfaceName.startsWith("gr-")) {
			ret = new GREInterface(physicalInterfaceName, unitId);
		}
		else if (physicalInterfaceName.indexOf("vlan") == 0) {
			Integer vlanId = config.findVlanIdBoundBy(String.format("%s.%d", physicalInterfaceName, unitId));
			if (vlanId != null)
					ret = new VlanInterface(unitId, vlanId);
		}
		else if (physicalInterfaceName.indexOf("irb") == 0) {
			Integer vlanId = config.findVlanIdBoundBy(String.format("%s.%d", physicalInterfaceName, unitId));
			if (config.isL2ng() && vlanId != null) {
				ret = new L2ngVlanInterface(unitId, vlanId);
			}
			else {
				ret = new IrbInterface(unitId);
			}
		}
		else if (physicalInterfaceName.indexOf("lo0") == 0) {
			if (unitId == 16384 || unitId == 16385)
				return null;
			ret = new LoopbackInterface(unitId);
		}
		else if (physicalInterfaceName.equals("pp0")) {
			ret = new PPPoEInetInterface(physicalInterfaceName, unitId);
		}
		else if (physicalInterfaceName.equals("st0")) {
			ret = new VPNInterface(unitId);
		}
		else if (Util.isPortPhysicalInterface(physicalInterfaceName)) {
			if (ret == null) {
				tmp = config.getNode(node, "encapsulation[normalize-space(text())=%s]", XML.xpc(ENCAPSULATION_PPPOE));
				if (tmp != null) {
					ret = new PortPPPoEInterface(physicalInterfaceName, unitId);
				}
			}
			
			if (ret == null) {
				tmp = config.getNode(node, "family/%s", FAMILY_VLAN);
				if (tmp != null) {
					if (config.isL2ng())
						ret = new L2ngPortVlanInterface(physicalInterfaceName, unitId);
					else
						ret = new PortVlanInterface(physicalInterfaceName, unitId);
				}
			}

			if (ret == null) {
				tmp = config.getNode(node, "family/%s", FAMILY_BRIDGE);
				if (tmp != null) {
					ret = new PortBridgeInterface(physicalInterfaceName, unitId);
				}
			}

			if (ret == null) {
				ret = new PortInetInterface(physicalInterfaceName, unitId);
			}
		}

		if (ret != null) {
			ret.constructFromNode(node, config);
		}
		
		return ret;
	}
	
	protected LogicalInterface(String name, int unitId, String family) {
		this.physicalInterfaceName = name;
		this.unitId   = unitId;
		this.family   = family;
		this.zoneName = "";
		this.vlanId   = 0;
		this.disabled = false;
	}
	
	protected void constructFromNode(Node unit, DeviceConfig config) {
//		Node zone = config.findSecurityZoneNodeByInterface(getInternalName());
		Node zone = config.findSecurityZoneNodeByInterface(getName());
		if (zone != null)
			setZoneName(config.getString(zone, "name/text()"));
	}
	
	public final int getUnitId() {
		return unitId;
	}

	public final String getName() {
		return String.format("%s.%d", physicalInterfaceName, unitId);
	}

	public final String getNameForId() {
		return Util.interfaceNameForId(getName());
	}
	
	public final String getDisplayName() {
		return getName();
	}

//	public String getInternalName() {
//		return getName();
//	}

	public final String getPhysicalInterfaceName() {
		return physicalInterfaceName;
	}
	
	public final String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String v) {
		zoneName = v;
	}

	public final boolean isEnabled() {
		return !disabled;
	}
	
	public final boolean isDisabled() {
		return disabled;
	}
	
	public void setDisabled(boolean v) {
		disabled = v;
	}

	public final String getFamily() {
		return family;
	}
	
	public final Integer getVlanId() {
		return vlanId;
	}

	public final boolean isBindable() {
		return (this instanceof BindableInterface);
	}

	public boolean isRemovable(DeviceConfig cfg) {
		if (cfg.getNode("//interface[normalize-space(text())=%s]", XML.xpc(getName())) != null) {
			return false;
		}
		
		if (cfg.getNode("//external-interface[normalize-space(text())=%s]", XML.xpc(getName())) != null) {
			return false;
		}
		
		if (cfg.getNode("//source-interface[normalize-space(text())=%s]", XML.xpc(getName())) != null) {
			return false;	
		}
		
		NodeList nl = cfg.getNodeList("//*[normalize-space(interfaces/name/text())=%s]", XML.xpc(getName()));
		for (int i = 0; i < nl.getLength(); i++) {
			Element e = (Element)nl.item(i);
			if (!"security-zone".equals(e.getTagName())) {
				return false;
			}
		}

		return true;
	}

	protected void configCreate(Node unit, DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
		
	}
	
	public final DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				String phyName = physicalInterfaceName;
				
				try {
					PhysicalInterfaceInformation phyInfo = info.physicalInterfaceInformation();
					
					if (phyInfo != null && !phyInfo.isPhysicalInterfaceExist(phyName)) {
						throw new IllegalArgumentException("Physical interface " + phyName + " is not exist.");
					}

					Node is = config.createElementsIfNotExist("interfaces");
					Node i = config.getNode(is, "interface/name[normalize-space(text())=%s]/..", XML.xpc(phyName));
					if (i == null) {
						i = config.createElement("interface");
						i.appendChild(config.createElement("name", phyName));
						is.appendChild(i);
					}
					
					Node unit = config.getNode(i, "unit/name[normalize-space(text())=%d]/..", unitId); 
					if (unit != null) {
						throw new IllegalArgumentException(String.format("Logical interface %s.%d is already exist.", phyName, unitId));
					}

					unit = config.createElement("unit");
					unit.appendChild(config.createElement("name", String.format("%d", unitId)));	
					i.appendChild(unit);

					configCreate(unit, config, info);
				}
				catch (DeviceConfigException e) {
					throw e;
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config create logical interface.", e);
				}
				
			}
		};
	}
	
	protected void configDelete(Node unit, DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
		unit.getParentNode().removeChild(unit);
	}
	
	public final DeviceConfigure getDeleteConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				LogicalInterface self = LogicalInterface.this;
//				SecurityZone.getDeleteInterfaceConfigure(self.getInternalName()).config(config);
				SecurityZone.getDeleteInterfaceConfigure(self.getName()).config(config, info);
				try {
					Node n = config.findLogicalInterfaceNode(self.getPhysicalInterfaceName(), self.getUnitId());
					if (n != null) {
						Node p = n.getParentNode();
						configDelete(n, config, info);
						
						if (config.getNodeList(p, "*").getLength() <= 1)
							p.getParentNode().removeChild(p);
					}
				} catch (DeviceConfigException e) {
					throw e;
				} catch (Throwable e) {
					throw new DeviceConfigException("Failed to config delete logical interface.", e);
				}
			}
		};
	}
	
	public final DeviceConfigure getBasicConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				LogicalInterface lint = LogicalInterface.this;
				LogicalInterface lold = config.findLogicalInterface(getName());
				
				try {
					Node n = config.findLogicalInterfaceNode(lint.getPhysicalInterfaceName(), lint.getUnitId());
					lint.configBasic(n, lold, config, info);
				} catch (DeviceConfigException e) {
					throw e;
				} catch (Throwable e) {
					throw new DeviceConfigException("Failed to config logical interface.", e);
				} 
			}
			
		};
	}
	
	protected void configBasic(Node unit, LogicalInterface old, DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
		SecurityZone.getAddInterfaceConfigure(this).config(cfg, info);
	}
	
	public int isChanged(DeviceConfig candidate) {
		LogicalInterface c = candidate.findLogicalInterface(physicalInterfaceName, unitId);
		if (c == null)
			return 1;
		if (!equals(c)) 
			return 2;
		return 0;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (disabled ? 1231 : 1237);
		result = prime * result + ((family == null) ? 0 : family.hashCode());
		result = prime * result + ((physicalInterfaceName == null) ? 0 : physicalInterfaceName.hashCode());
		result = prime * result + unitId;
		result = prime * result + ((vlanId == null) ? 0 : vlanId.hashCode());
		result = prime * result + ((zoneName == null) ? 0 : zoneName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof LogicalInterface)) {
			return false;
		}
		LogicalInterface other = (LogicalInterface) obj;
		if (disabled != other.disabled) {
			return false;
		}
		if (StringUtils.isBlank(family)) {
			if (StringUtils.isNotBlank(other.family))
				return false;
		} else if (!family.equals(other.family)) {
			return false;
		}
		if (physicalInterfaceName == null) {
			if (other.physicalInterfaceName != null) {
				return false;
			}
		} else if (!physicalInterfaceName.equals(other.physicalInterfaceName)) {
			return false;
		}
		if (unitId != other.unitId) {
			return false;
		}
		if (vlanId == null) {
			if (other.vlanId != null) {
				return false;
			}
		} else if (!vlanId.equals(other.vlanId)) {
			return false;
		}
		if (StringUtils.isBlank(zoneName)) {
			if (StringUtils.isNotBlank(other.zoneName)) {
				return false;
			}
		} else if (!zoneName.equals(other.zoneName)) {
			return false;
		}
		return true;
	}
	
	public boolean isPPPoEBindable(DeviceConfig cfg) {
		return false;
	}
}
