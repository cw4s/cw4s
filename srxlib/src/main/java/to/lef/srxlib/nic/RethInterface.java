package to.lef.srxlib.nic;

public class RethInterface extends LogicalInetInterface implements VlanTaggableInterface {
	
	public RethInterface(String pname, int unitId) {
		super(pname, unitId);
	}
	
	public final void setVlanId(Integer v) {
		vlanId = v;
	}
}
