package to.lef.srxlib.nic;

import to.lef.srxlib.DeviceConfig;

public interface BindableInterface {
	public String getDisplayName();
	public String getName();
	
	public boolean canBoundBy(String bindInterface, DeviceConfig config);
	public LogicalBoundInterface toBoundVlanInterface(DeviceConfig config);
}
