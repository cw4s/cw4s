package to.lef.srxlib.nic;


import org.w3c.dom.Node;

import to.lef.srxlib.DeviceConfig;

public abstract class LogicalBridgeInterface extends LogicalBoundInterface {

	protected LogicalBridgeInterface(String name, int unitId) {
		super(name, unitId, FAMILY_BRIDGE);
	}
	
	@Override
	protected void constructFromNode(Node unit, DeviceConfig config) {
		super.constructFromNode(unit, config);
	}
}
