package to.lef.srxlib.nic;


import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.IPAddress;
import to.lef.srxlib.PPPoE;
import to.lef.srxlib.dhcp.DhcpRelayAgent;
import to.lef.srxlib.dhcp.DhcpServer;
import to.lef.srxlib.dhcp.DhcpService;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.nat.*;
import to.lef.srxlib.route.StaticRoute;
import to.lef.srxlib.zone.InboundProtocol;
import to.lef.srxlib.zone.InboundService;

public class LogicalInetInterface extends LogicalInterface {
	public static final int TCP_MSS_LOW = 1304;
	
	public static class IPv4AddressAndMask extends IPAddress {
		boolean preferred;
	
		public IPv4AddressAndMask(String s) {
			this(s, false);
		}
		
		public IPv4AddressAndMask(String a, boolean preferred) {
			super(a);
			this.preferred = preferred;
		}	
		
		public IPv4AddressAndMask(String address, int netmask) {
			this(address, netmask, false);
		}
		
		public IPv4AddressAndMask(String address, int netmask, boolean preferred) {
			super(address, netmask);
			this.preferred = preferred;
		}

		public final boolean isPreferred() {
			return preferred;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + (preferred ? 1231 : 1237);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (!super.equals(obj)) {
				return false;
			}
			if (!(obj instanceof IPv4AddressAndMask)) {
				return false;
			}
			IPv4AddressAndMask other = (IPv4AddressAndMask) obj;
			if (preferred != other.preferred) {
				return false;
			}
			return true;
		}
	}

	public static class IPv6AddressAndMask extends IPAddress {
		public IPv6AddressAndMask(String a) {
			super(a);
		}
		
		public IPv6AddressAndMask(String address, int netmask) {
			super(address, netmask);
		}
		
	}
	
	public final static int INET4_TYPE_NONE = 0;
	public final static int INET4_TYPE_DHCP = 1;
	public final static int INET4_TYPE_NEGOTIATE = 2;
	public final static int INET4_TYPE_STATIC = 3;
	public final static int INET4_TYPE_UNNUMBERED = 4;
	
	private int inet4Type;
	private IPv4AddressAndMask inet4AddressAndMask;
	private List<IPv4AddressAndMask> inet4SecondaryAddressAndMaskList;
	private IPv6AddressAndMask inet6AddressAndMask;

	private Integer mtu;
	
	protected InboundProtocol inboundProtocols;
	protected InboundService  inboundServices;

	protected List<Dip> dipList;
	protected List<Mip> mipList;
	protected List<Vip> vipList;
	
	protected LogicalInetInterface(String name, int unitId) {
		super(name, unitId, FAMILY_INET);
		this.inet4Type = INET4_TYPE_NONE;
		this.inet4AddressAndMask = null;
		this.inet4SecondaryAddressAndMaskList = new ArrayList<IPv4AddressAndMask>();
		this.inet6AddressAndMask = null;

		this.mtu = null;
		
		this.inboundProtocols = new InboundProtocol();
		this.inboundServices  = new InboundService();
		
		this.mipList = new ArrayList<Mip>();
		this.vipList = new ArrayList<Vip>();
		this.dipList = new ArrayList<Dip>();
	}

	@Override
	protected void constructFromNode(Node unit, DeviceConfig config) {
		super.constructFromNode(unit, config);

		Node inet = config.getNode(unit, "family/inet");
		if (inet != null) {
			if (config.getNode(inet, "dhcp") != null) {
				inet4Type = INET4_TYPE_DHCP;
			}
			else if (config.getNode(inet, "negotiate-address") != null) {
				inet4Type = INET4_TYPE_NEGOTIATE;
			}
			else if (config.getNode(inet, "address") != null) {
				inet4Type = INET4_TYPE_STATIC;
				for (Node addr : IterableNodeList.apply(config.getNodeList(inet, "address"))) {
					boolean preferred = XML.isBooleanElementSet(inet, "preferred");
					String v = config.getString(addr, "name/text()");
					if (StringUtils.isNotBlank(v)) {
						if (preferred && inet4AddressAndMask == null) {
							inet4AddressAndMask = new IPv4AddressAndMask(v, true);
						}
						else {
							inet4SecondaryAddressAndMaskList.add(new IPv4AddressAndMask(v, false));
						}
					}
				}
				
				if (inet4AddressAndMask == null && !inet4SecondaryAddressAndMaskList.isEmpty()) {
					IPv4AddressAndMask tmp = inet4SecondaryAddressAndMaskList.get(0);
					inet4AddressAndMask = new IPv4AddressAndMask(tmp.getAddress(), tmp.getNetmask(), true);
					inet4SecondaryAddressAndMaskList.remove(0);
				}
			}
			else {
				inet4Type = INET4_TYPE_UNNUMBERED;
			}
			
			String mtuString = config.getString(inet, "mtu/text()");
			if (StringUtils.isNumeric(mtuString)) {
				mtu = Integer.valueOf(mtuString);
			}
		}
		
		Node zn = config.findSecurityZoneNode(getZoneName());
		if (zn != null) {
			inboundProtocols.constructFromNode(zn, this, config);
			inboundServices.constructFromNode(zn, this, config);
		}
		
		for (Vip vip: config.vipList(getName())) {
			vipList.add(vip);
		}
		
		for (Mip mip: config.mipList(getName())) {
			mipList.add(mip);
		}
		
		for (Dip dip: config.dipList()) {
			if (dip.isBelongsTo(this))
				dipList.add(dip);
		}
		
		if (this instanceof VlanTaggableInterface) {
			String vlanTag = config.getString(unit, "vlan-id/text()");
			if (StringUtils.isNumeric(vlanTag))
				vlanId = Integer.valueOf(vlanTag);
		}
	}

	public final IPv4AddressAndMask getInet4AddressAndMask() {
		return inet4AddressAndMask;
	}
	
	public final String getInet4AddressString() {
		return (inet4AddressAndMask == null) ? "" : inet4AddressAndMask.getAddress();
	}
	
	public final void setInet4AddressAndMask(String a, int m) {
		inet4Type = INET4_TYPE_STATIC;
		inet4AddressAndMask = new IPv4AddressAndMask(a, m, true);
	}

	public final int getInet4Type() {
		return inet4Type;
	}

	public final boolean isInet4Static() {
		return (inet4Type == INET4_TYPE_STATIC);
	}
	
	public final void setInet4Type(int inet4Type) {
		this.inet4Type = inet4Type;
		if (inet4Type != INET4_TYPE_STATIC)
			this.inet4AddressAndMask = null;
	}

	public final List<IPv4AddressAndMask> getInet4AddressAndMaskList() {
		List<IPv4AddressAndMask> ret = new ArrayList<>();
		if (inet4AddressAndMask != null)
			ret.add(inet4AddressAndMask);
		
		for (IPv4AddressAndMask addr : inet4SecondaryAddressAndMaskList) {
			ret.add(addr);
		}
		
		return Collections.unmodifiableList(ret);
	}
	
	public final List<IPv4AddressAndMask> getInet4SecondaryAddressAndMaskList() {
		return Collections.unmodifiableList(inet4SecondaryAddressAndMaskList);
	}

	public final void addSecondaryAddressAndMask(IPv4AddressAndMask v) {
		if (v.equals(inet4AddressAndMask) || inet4SecondaryAddressAndMaskList.contains(v))
			return;
		inet4SecondaryAddressAndMaskList.add(v);
	}
	
	public final void removeSecondaryAddressAndMask(IPv4AddressAndMask v) {
		inet4SecondaryAddressAndMaskList.remove(v);
	}
	
	public final IPv6AddressAndMask getInet6AddressAndMask() {
		return inet6AddressAndMask;
	}
	
	public final void setInet6AddressAndMask(String a, int m) {
		inet6AddressAndMask = new IPv6AddressAndMask(a, m);
	}

	public final InboundProtocol getInboundProtocols() {
		return inboundProtocols;
	}

	public final InboundService getInboundServices() {
		return inboundServices;
	}
	
	public final void setInboundProtocols(InboundProtocol inboundProtocols) {
		this.inboundProtocols = inboundProtocols;
	}

	public final void setInboundServices(InboundService inboundServices) {
		this.inboundServices = inboundServices;
	}

	public Integer getMtu() {
		return mtu;
	}
	
	public void setMtu(Integer mtu) {
		this.mtu = mtu;
	}
	
	public final List<Mip> getMipList() {
		return Collections.unmodifiableList(mipList);
	}

	public final Map<String, Mip> getMipMap() {
		Map<String, Mip> map = new HashMap<String, Mip>();
		for (Mip mip: mipList) {
			map.put(mip.getName(), mip);
		}
		return map;
	}
	
	public final List<Vip> getVipList() {
		return Collections.unmodifiableList(vipList);
	}
	
	public final Map<String, Vip> getVipMap() {
		Map<String, Vip> map = new HashMap<String, Vip>();
		for (Vip vip: vipList) {
			map.put(vip.getName(), vip);
		}
		return map;
	}

	public final List<Dip> getDipList() {
		return Collections.unmodifiableList(dipList);
	}

	public final Map<String, Dip> getDipMap() {
		Map<String, Dip> map = new HashMap<String, Dip>();
		for (Dip dip: dipList) {
			map.put(dip.getName(), dip);
		}
		return map;
	}
	
	@Override
	protected void configCreate(Node unit, DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
		super.configCreate(unit, cfg, info);
		configBasic(unit, null, cfg, info);
	}

	protected void configInetAddress(IPv4AddressAndMask addr, Node inet, DeviceConfig config) {
		if (addr != null) {
			String name = String.format("%s/%d", addr.getAddress(), addr.getNetmask());
			Node address = config.getNode(inet, "address[normalize-space(name/text())=%s]", XML.xpc(name));
			if (address == null) {
				address = config.createElement("address");
				address.appendChild(config.createElement("name", name));
				inet.appendChild(address);
			}
			config.setBooleanElement(address, "preferred", addr.isPreferred());
		}
	}
	
	protected void configInetFamily(Node unit, DeviceConfig config) {
		Node family = config.createElementsIfNotExist(unit, "family");
		Node inet = null;

		NodeList nl = family.getChildNodes();
		for (int i = 0; i < nl.getLength(); i++) {
			Node n = nl.item(i);
			if (n.getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element)n;
				if (FAMILY_INET.equals(e.getTagName())) {
					inet = n;
				}
				else if (!FAMILY_INET6.equals(e.getTagName())) {
					family.removeChild(n);
				}
			}
		}

		if (inet == null) {
			inet = config.createElement("inet");
			family.appendChild(inet);
		}

		if (inet4Type == INET4_TYPE_STATIC) {
			config.deleteNode(inet, "address");
			config.deleteNode(inet, "dhcp");
			config.deleteNode(inet, "negotiate-address");
			configInetAddress(inet4AddressAndMask, inet, config);
			for (IPv4AddressAndMask addr : inet4SecondaryAddressAndMaskList) {
				configInetAddress(addr, inet, config);
			}
		}
		else if (inet4Type == INET4_TYPE_DHCP) {
			config.createElementsIfNotExist(inet, "dhcp");
			config.deleteNode(inet, "address");
			config.deleteNode(inet, "negotiate-address");
		}
		else if (inet4Type == INET4_TYPE_NEGOTIATE) {
			config.createElementsIfNotExist(inet, "negotiate-address");
			config.deleteNode(inet, "address");
			config.deleteNode(inet, "dhcp");
		}
		else if (inet4Type == INET4_TYPE_UNNUMBERED) {
			config.deleteNode(inet, "address");
			config.deleteNode(inet, "dhcp");		
			config.deleteNode(inet, "negotiate-address");
		}
		
		config.deleteNode(inet, "mtu");
		if (mtu != null) {
			inet.appendChild(config.createElement("mtu", "%d", mtu));
		}
			
//		if (config.getNode(inet, "*") == null) {
//			family.removeChild(inet);
//		}	
	}
	
	@Override
	protected void configBasic(Node unit, LogicalInterface old, DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
		super.configBasic(unit, old, config, info);

		configInetFamily(unit, config);

		if (StringUtils.isNotBlank(getZoneName())) {
			Node zn = config.findSecurityZoneNode(getZoneName());
			if (zn == null) {
				throw new IllegalArgumentException("Zone node is not found.");
			}

			inboundProtocols.getOptionsConfigure(zn, this).config(config, info);
			inboundServices.getOptionsConfigure(zn, this).config(config, info);
		}
		
		configFixDhcp(old, config, info);
		
		if (this instanceof VlanTaggableInterface) {
			configVlanId(unit, config);
		}
	}

	protected void configFixVlanTagging(Node parent, DeviceConfig config) throws DeviceConfigException {
		if (parent != null) {
			boolean tagFound = false;
			for (Node n : IterableNodeList.apply(config.getNodeList(parent, "unit"))) {
				if (config.getNode(n, "vlan-id") != null) {
					tagFound = true;
					break;
				}
			}

			config.deleteNode(parent, "vlan-tagging");
			if (tagFound)
				parent.insertBefore(config.createElement("vlan-tagging"), config.getNode(parent, "unit"));	
		}
	}
	
	protected void configVlanId(Node unit, DeviceConfig config) throws DeviceConfigException {
		int vid = 0;
		
		if (vlanId != null)
			vid = vlanId;
		
		config.deleteNode(unit, "vlan-id");
		if (vid > 0) {
			unit.appendChild(config.createElement("vlan-id", "%d", vlanId));
		}
		else if (getUnitId() != 0) {
			throw new IllegalArgumentException("VLAN-ID must be specified on tagged ethernet interfaces");
		}
		
		configFixVlanTagging(unit.getParentNode(), config);
	}
	
	protected void configFixDhcp(LogicalInterface old, DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
		if (old != null && old instanceof LogicalInetInterface) {
			LogicalInetInterface lold = (LogicalInetInterface )old;
			List<IPv4AddressAndMask> list1 = getInet4AddressAndMaskList();
			List<IPv4AddressAndMask> list2 = lold.getInet4AddressAndMaskList();
		
			for (IPv4AddressAndMask a2: list2) {
				boolean found = false;
				InetAddress netAddr2 = a2.getNetworkAddress();
				if (netAddr2 == null)
					continue;
				
				for (IPv4AddressAndMask a1: list1) {
					InetAddress netAddr1 = a1.getNetworkAddress();
					if (netAddr1 == null)
						continue;
					
					if (a1.equals(a2)) {
						found = true;
						break;
					}
				}
				
				if (!found) {
					String addr = String.format("%s/%d", netAddr2.getHostAddress(), a2.getNetmask());
					DhcpService dhcp = lold.findDhcpService(addr, config);
					if (dhcp != null && dhcp instanceof DhcpServer) {
						dhcp.getDeleteConfigure().config(config, info);
					}
				}
			}
		}
	}
	
	public DeviceConfigure getSecondaryIPConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				LogicalInetInterface self = LogicalInetInterface.this;
				LogicalInterface old = cfg.findLogicalInterface(getName());
				
				Node unit = cfg.findLogicalInterfaceNode(self.getName());
				
				Node inet = cfg.createElementsIfNotExist(unit, "family", "inet");
				cfg.deleteNode(inet, "address");
				configInetAddress(inet4AddressAndMask, inet, cfg);
				for (IPv4AddressAndMask addr : inet4SecondaryAddressAndMaskList){
					configInetAddress(addr, inet, cfg);
				}
				
				configFixDhcp(old, cfg, info);
			}
			
		};
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((mipList == null) ? 0 : mipList.hashCode());
		result = prime * result + ((vipList == null) ? 0 : vipList.hashCode());
		result = prime * result + ((dipList == null) ? 0 : dipList.hashCode());
		result = prime * result + ((inboundProtocols == null) ? 0 : inboundProtocols.hashCode());
		result = prime * result + ((inboundServices == null) ? 0 : inboundServices.hashCode());
		result = prime * result + ((inet4AddressAndMask == null) ? 0 : inet4AddressAndMask.hashCode());
		result = prime * result + ((inet4SecondaryAddressAndMaskList == null) ? 0 : inet4SecondaryAddressAndMaskList.hashCode());
		result = prime * result + inet4Type;
		result = prime * result + ((inet6AddressAndMask == null) ? 0 : inet6AddressAndMask.hashCode());
		result = prime * result + ((mtu == null) ? 0 : mtu.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof LogicalInetInterface)) {
			return false;
		}
		LogicalInetInterface other = (LogicalInetInterface) obj;
		if (inboundProtocols == null) {
			if (other.inboundProtocols != null) {
				return false;
			}
		} else if (!inboundProtocols.equals(other.inboundProtocols)) {
			return false;
		}
		if (inboundServices == null) {
			if (other.inboundServices != null) {
				return false;
			}
		} else if (!inboundServices.equals(other.inboundServices)) {
			return false;
		}
		if (inet4AddressAndMask == null) {
			if (other.inet4AddressAndMask != null) {
				return false;
			}
		} else if (!inet4AddressAndMask.equals(other.inet4AddressAndMask)) {
			return false;
		}
		if (inet4SecondaryAddressAndMaskList == null) {
			if (other.inet4SecondaryAddressAndMaskList != null) {
				return false;
			}
		}
		else if (!(other.inet4SecondaryAddressAndMaskList != null && inet4SecondaryAddressAndMaskList.size() == other.inet4SecondaryAddressAndMaskList.size() && inet4SecondaryAddressAndMaskList.containsAll(other.inet4SecondaryAddressAndMaskList))) {
			return false;
		}
		if (inet4Type != other.inet4Type) {
			return false;
		}
		if (inet6AddressAndMask == null) {
			if (other.inet6AddressAndMask != null) {
				return false;
			}
		} else if (!inet6AddressAndMask.equals(other.inet6AddressAndMask)) {
			return false;
		}
	
		if (mtu == null) {
			if (other.mtu != null) {
				return false;
			}
		} else if (!mtu.equals(other.mtu)){
			return false;
		}
		
		if (mipList == null) {
			if (other.mipList != null) {
				return false;
			}
		} else if (!(other.mipList != null && mipList.size() == other.mipList.size() && mipList.containsAll(other.mipList))) {
			return false;
		}
		
		if (vipList == null) {
			if (other.vipList != null) {
				return false;
			}
		} else if (!(other.vipList != null && vipList.size() == other.vipList.size() && vipList.containsAll(other.vipList))) {
			return false;
		}
		
		if (dipList == null) {
			if (other.dipList != null) {
				return false;
			}
		} else if (!(other.dipList != null && dipList.size() == other.dipList.size() && dipList.containsAll(other.dipList))) {
			return false;
		}
		
		return true;
	}

	@Override
	protected void configDelete(Node unit, DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
		Node parent = unit.getParentNode();
		
		super.configDelete(unit, cfg, info);
		
		if (this instanceof VlanTaggableInterface) {
			configFixVlanTagging(parent, cfg);
		}
	
		for (StaticRoute rt: cfg.staticRouteList()) {
			if (StringUtils.isNotBlank(rt.getInterfaceName()) && rt.getInterfaceName().equals(getName())) {
				rt.getDeleteConfigure().config(cfg, info);
			}
		}
		
		for (Mip mip: mipList) {
			mip.getDeleteConfigure().config(cfg, info);
		}
		
		for (Vip vip: vipList) {
			vip.getDeleteConfigure().config(cfg, info);
		}
	
		for (Dip dip: dipList) {
			dip.getDeleteConfigure().config(cfg, info);
		}
	}

	public List<DhcpService> getDhcpServiceList(DeviceConfig config) {
		List<DhcpService> ret = new ArrayList<>();
		Map<InetAddress, Boolean> cache = new HashMap<>();
		List<IPv4AddressAndMask> src = getInet4AddressAndMaskList();
		
		for (IPv4AddressAndMask addr : src) {
			String address = addr.getAddress();
			int mask = addr.getNetmask();
			
			if (StringUtils.isBlank(address) || "0.0.0.0".equals(address) || mask == 0 || mask == 32)  
				continue;
			
			InetAddress netAddr = IPAddress.getNetworkAddress(address, mask);
			if (netAddr == null)
				continue;
		
			if (cache.get(netAddr) == null) {
				DhcpService instance = DhcpService.createFromConfig(getName(), new IPv4AddressAndMask(netAddr.getHostAddress(), mask), config);
				ret.add(instance);
				
				if (instance instanceof DhcpRelayAgent) 
					return ret;
				cache.put(netAddr, true);
			}
		}
		
		return ret;
	}

	public Map<IPv4AddressAndMask, DhcpService> getDhcpServiceMap(DeviceConfig config) {
		Map<IPv4AddressAndMask, DhcpService> ret = new HashMap<>();
		List<DhcpService> list = getDhcpServiceList(config);
		
		for (DhcpService item : list) {
			ret.put(item.getNetworkAddress(), item);
		}
		
		return ret;
	}
	
	public DhcpService findDhcpService(String address, DeviceConfig config) {
		IPv4AddressAndMask key = new IPv4AddressAndMask(address);
		List<DhcpService> list = getDhcpServiceList(config);
		
		for (DhcpService service : list) {
			if (key.equals(service.getNetworkAddress()))
				return service;
		}
		return null;
	}

	@Override
	public boolean isRemovable(DeviceConfig cfg) {
		if(!super.isRemovable(cfg))
			return false;
	
//		Node n = cfg.getNode(cfg.routingOptionsNode(), "//next-hop[normalize-space(text())=%s]", XML.xpc(getName()));
//		if (n != null)
//			return false;
	
		return (mipList.size() == 0 && vipList.size() == 0 && dipList.size() == 0);
	}

	public final void fixTcpMss(DeviceConfig config) {
		fixTcpMss(TCP_MSS_LOW, config);
	}
	
	public void fixTcpMss(int value, DeviceConfig config) {
		boolean found = false;
		
		Node node = config.findPhysicalInterfaceNode(PPPoE.PHYSICAL_INTERFACE_NAME);
		if (node != null && config.getNode(node, "unit") != null) {
			found = true;
		}
		else {
			node = config.findPhysicalInterfaceNode(VPNInterface.PHYSICAL_INTERFACE_NAME);
			if (node != null && config.getNode(node, "unit") != null) {
				found = true;
			}
		}
			
		Node options = config.internetOptionsNode();
		if (found) {
			Node mss = config.getNode(options, "tcp-mss");
			if (mss == null) {
				options.appendChild(config.createElement("tcp-mss", "%d", value));
			}
		}
		else {
			config.deleteNode(options, "tcp-mss");
		}
	}
}
