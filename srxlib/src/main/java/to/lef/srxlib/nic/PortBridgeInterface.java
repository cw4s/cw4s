package to.lef.srxlib.nic;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;

public class PortBridgeInterface extends LogicalBridgeInterface {

	public PortBridgeInterface(String name, int unitId) {
		super(name, unitId);
	}

	@Override
	public DeviceConfigure getBindConfigure(String intName, String name, String portMode) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig configuration, DeviceInformationProvider info) throws DeviceConfigException {
				
			}
			
		};
	}

	@Override
	public DeviceConfigure getUnbindConfigure(String intName, String name) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig configuration, DeviceInformationProvider info) throws DeviceConfigException {
				
			}
			
		};
	}

	@Override
	public LogicalBoundInterface toBoundVlanInterface(DeviceConfig config) {
		return null;
	}

}
