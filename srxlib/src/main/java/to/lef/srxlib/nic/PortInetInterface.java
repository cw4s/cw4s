package to.lef.srxlib.nic;

import org.w3c.dom.Node;

import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.l2ng.L2ngPortVlanInterface;

public class PortInetInterface extends LogicalInetInterface implements BindableInterface, VlanTaggableInterface {

	public PortInetInterface(String name, int unitId) {
		super(name, unitId);
	}

	public final void setVlanId(Integer v) {
		vlanId = v;
	}
	
	@Override
	public boolean canBoundBy(String bindInterface, DeviceConfig config) {
		if (getUnitId() != 0) 
			return false;	
		
		Node pNode = config.findPhysicalInterfaceNode(getPhysicalInterfaceName());
		if (pNode != null) {
			if (config.getNode(pNode, "vlan-tagging") != null)
				return false;
		}
		return true;
	}
	
	@Override
	public LogicalBoundInterface toBoundVlanInterface(DeviceConfig config) {
		if (config.isL2ng())
			return new L2ngPortVlanInterface(getPhysicalInterfaceName(), getUnitId());
		return new PortVlanInterface(getPhysicalInterfaceName(), getUnitId());
	}
	
	@Override
	public boolean isPPPoEBindable(DeviceConfig cfg) {
		return (getUnitId() == 0);
	}
}
