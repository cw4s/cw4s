package to.lef.srxlib.nic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import to.lef.srxlib.Device;
import to.lef.srxlib.DeviceCacheException;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.Util;


abstract public class LogicalBindInterface extends LogicalInetInterface {
	private List<String> bindingInterfaceNameList;
	
	protected LogicalBindInterface(String name, int unitId) {
		super(name, unitId);
		bindingInterfaceNameList = new ArrayList<String>();
	}
	
	public List<String> getBindingInterfaceNameList() {
		return Collections.unmodifiableList(bindingInterfaceNameList);
	}

	public void addBindingInterfaceName(String physicalInterfaceName, int unitId) {
		addBindingInterfaceName(String.format("%s.%d", physicalInterfaceName, unitId));
	}
	
	public void addBindingInterfaceName(String logicalInterfaceName) {
		bindingInterfaceNameList.add(logicalInterfaceName);
	}

	public List<LogicalInterface> getBindCandidatesList(Device device) {
		List<LogicalInterface> ret = new ArrayList<>();
		
//				@for(v <- device.config.logicalInterfaceList.filter(v => models.BindableInterface.unapply(v).isDefined && models.BindableInterface.unapply(v).get.canBoundBy(lif.getName))) {
		try {
			DeviceConfig config = device.config();
			for (String phyName: device.physicalInterfaceInformation().physicalInterfaceNameList()) {
				if (Util.isPortPhysicalInterface(phyName)) {
					LogicalInterface lint = config.findLogicalInterface(phyName, 0);
					if (lint == null) {
						lint = new PortInetInterface(phyName, 0);
					}
					
					if (lint instanceof BindableInterface && ((BindableInterface)lint).canBoundBy(getName(), config)) {
						ret.add(lint);
					}
				}
			}
		} catch (DeviceCacheException e) {
			e.printStackTrace();
		}
		
		return ret;
	}
	
	abstract public DeviceConfigure getBindConfigure(final Map<String, String> bindMap);

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((bindingInterfaceNameList == null) ? 0 : bindingInterfaceNameList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof LogicalBindInterface)) {
			return false;
		}
		LogicalBindInterface other = (LogicalBindInterface) obj;
		if (bindingInterfaceNameList == null) {
			if (other.bindingInterfaceNameList != null) {
				return false;
			}
		} else if (!(other.bindingInterfaceNameList != null && bindingInterfaceNameList.size() == other.bindingInterfaceNameList.size() && bindingInterfaceNameList.containsAll(other.bindingInterfaceNameList))) {
			return false;
		}
		return true;
	} 
}
