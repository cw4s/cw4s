package to.lef.srxlib;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.dom.IterableNodeList;

public class DnsHost {
	private String hostName;
	private String domainName;
	private List<String> dnsServerList;
	
	public DnsHost() {
		this.hostName = null;
		this.domainName = null;
		this.dnsServerList = new ArrayList<String>();
	}
	
	public final String getHostName() {
		return hostName;
	}

	public final void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public final String getDomainName() {
		return domainName;
	}

	public final void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public final List<String> getDnsServerList() {
		return Collections.unmodifiableList(dnsServerList);
	}

	public void addDnsServer(String server) {
		dnsServerList.add(server);
	}
	
	public static DnsHost createFromNode(Node node, DeviceConfig cfg) {
		DnsHost ret = new DnsHost();
		
		ret.setHostName(cfg.getString(node, "host-name/text()"));
		ret.setDomainName(cfg.getString(node, "domain-name/text()"));
		
		for (Node server : IterableNodeList.apply(cfg.getNodeList(node, "name-server"))) {
			String n = cfg.getString(server, "name/text()");
			if (StringUtils.isNotBlank(n))
				ret.dnsServerList.add(n);
		}
		
		return ret;
	}
	
	public DeviceConfigure getEditConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					final String hostNameTag = "host-name";
					final String domainNameTag = "domain-name";
					final String nameServerTag = "name-server";
					
					Node system = cfg.systemNode();
					cfg.deleteNode(system, hostNameTag);
					cfg.deleteNode(system, domainNameTag);
				
					if (StringUtils.isNotBlank(domainName)) {
						system.insertBefore(cfg.createElement(domainNameTag, domainName), system.getFirstChild());
					}
					if (StringUtils.isNotBlank(hostName)) {
						system.insertBefore(cfg.createElement(hostNameTag, hostName), system.getFirstChild());
					}
				
					cfg.deleteNode(system, nameServerTag);
					for (String n : dnsServerList) {
						if (StringUtils.isNotBlank(n)) {
							Node ns = cfg.createElement(nameServerTag);
							ns.appendChild(cfg.createElement("name", n));
							system.appendChild(ns);
						}
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config dns host.", e);
				}
			}
			
		};
	}
}
