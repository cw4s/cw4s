package to.lef.srxlib;

public class DeviceConfigException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1187244000633182654L;

	public DeviceConfigException(String message, Throwable e) {
		super(message, e);
	}

	@Override
	public String getMessage() {
		Throwable cause = getCause();
		if (cause != null) {
			return super.getMessage() + ": " + cause.getMessage();
		}
		return super.getMessage();
	}
}
