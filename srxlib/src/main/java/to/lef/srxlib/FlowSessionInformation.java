package to.lef.srxlib;


import org.w3c.dom.Document;
import org.w3c.dom.Node;

import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class FlowSessionInformation extends XML {
	private int activeUnicastSessionCount;
	private int activeMulticastSessionCount;
	private int maxSessionCount;
	
	public FlowSessionInformation(Document doc) {
		super(doc);
		this.activeUnicastSessionCount   = 0;
		this.activeMulticastSessionCount = 0;
		this.maxSessionCount             = 0;
		
		for (Node n : IterableNodeList.apply(getNodeList("//flow-session-summary-information"))) {
			try {
			int a =  Integer.parseInt(getString(n, "active-unicast-sessions/text()"));
			int m =  Integer.parseInt(getString(n, "active-multicast-sessions/text()"));
			int t =  Integer.parseInt(getString(n, "max-sessions/text()"));
			
			activeUnicastSessionCount   += a;
			activeMulticastSessionCount += m;
			maxSessionCount             += t;
			}
			catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
	}

	public final int getActiveUnicastSessionCount() throws NumberFormatException {
		return activeUnicastSessionCount;
	}
	
	public final int getActiveMulticastSessionCount() throws NumberFormatException {
		return activeMulticastSessionCount;
	}
	
	public final int getMaxSessionCount() throws NumberFormatException {
		return maxSessionCount;
	}
	
	public int getSessionUtil() throws NumberFormatException {
		return (getActiveUnicastSessionCount() + getActiveMulticastSessionCount()) / getMaxSessionCount() * 100;
	}
}
