package to.lef.srxlib;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.PointerType;
import com.sun.jna.win32.W32APIFunctionMapper;
import com.sun.jna.win32.W32APITypeMapper;

public class OSUtil {
	public static String getAppDataPath(final String appName) {
		String osName = System.getProperty("os.name").toLowerCase();	
		String path = System.getProperty("user.home") + File.separator + "." + File.separator;
		
		if (osName.indexOf("win") != -1) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(Library.OPTION_TYPE_MAPPER, W32APITypeMapper.UNICODE);
			map.put(Library.OPTION_FUNCTION_MAPPER, W32APIFunctionMapper.UNICODE);

			char pszPath[] = new char[Shell32.MAX_PATH];
			Shell32 instance = (Shell32) Native.load("shell32", Shell32.class, map);
			int hResult	= instance.SHGetFolderPath(null, Shell32.CSIDL_LOCAL_APPDATA, null, Shell32.SHGFP_TYPE_CURRENT, pszPath);
			if (Shell32.S_OK == hResult) {
				path = new String(pszPath);
				int len = path.indexOf('\0');
				if (len != -1)
					path = path.substring(0, len);
			} else {
				System.err.println("Failed to SHGetFolderPath");
			}	
		} else if (osName.indexOf("mac") != -1) {
			path = System.getProperty("user.home") + File.separator + "Library" + File.separator + "Application Support";
		}
		
		return path + File.separator + appName;
	}
	
	private static interface Shell32 extends Library {
		public static final int MAX_PATH	        = 260;
		public static final int CSIDL_LOCAL_APPDATA	= 0x001c;
		public static final int SHGFP_TYPE_CURRENT	= 0;
		public static final int S_OK	            = 0;

		/**
		 * http://msdn.microsoft.com/en-us/library/bb762181(VS.85).aspx
		 *
		 * HRESULT SHGetFolderPath( HWND hwndOwner, int nFolder, HANDLE hToken, * DWORD dwFlags, LPTSTR pszPath);
		 */
		public int SHGetFolderPath(final PointerType hwndOwner, final int nFolder, final PointerType hToken, final int dwFlags, final char pszPath[]);
	}
}
