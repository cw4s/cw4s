package to.lef.srxlib;

import java.util.regex.*;

import org.apache.commons.lang3.StringUtils;

public class Version {
	private static final Pattern versionPattern = Pattern.compile("(\\d+)(?:\\.(\\d+))?(?:([xrXR])(\\d+)(?:\\.(\\d+))?)?(?:\\-?[dD](\\d+)(?:\\.(\\d+))?)?");
	private final int majorVersion;
	private final int minorVersion;
	private final String type;
	private final int typeMajorVersion;
	private final int typeMinorVersion;
	private final boolean hasD;
	private final int dMajorVersion;
	private final int dMinorVersion;
	
	public Version(String string) {
		if (StringUtils.isBlank(string)) 
			throw new IllegalArgumentException("Version string is blank.");
		
		Matcher matcher = versionPattern.matcher(string);
		if (matcher.find() == false)
			throw new IllegalArgumentException("Invalid version string " + string);
		
		try {
			String v1 = matcher.group(1);
			if (StringUtils.isNotBlank(v1)) {
				majorVersion = Integer.parseInt(v1);
				
				v1 = matcher.group(2);
				if (StringUtils.isNotBlank(v1)) {
					minorVersion = Integer.parseInt(v1);
				}
				else {
					minorVersion = 0;
				}
			}
			else {
				majorVersion = 0;
				minorVersion = 0;
			}

			v1 = matcher.group(3);
			if (StringUtils.isNotBlank(v1)) {
				type = v1.toUpperCase();
				
				v1 = matcher.group(4);
				if (StringUtils.isNotBlank(v1)) {
					typeMajorVersion = Integer.parseInt(v1);

					v1 = matcher.group(5);
					if (StringUtils.isNotBlank(v1)) {
						typeMinorVersion = Integer.parseInt(v1);
					}
					else {
						typeMinorVersion = 0;
					}
				}
				else {
					typeMajorVersion = 0;
					typeMinorVersion = 0;
				}
			}
			else {
				type = null;
				typeMajorVersion = 0;
				typeMinorVersion = 0;
			}
			
			v1 = matcher.group(6);
			if (StringUtils.isNotBlank(v1)) {
				hasD = true;
				dMajorVersion = Integer.parseInt(v1);
			
				v1 = matcher.group(7);
				if (StringUtils.isNotBlank(v1)) {
					dMinorVersion = Integer.parseInt(v1);
				}
				else {
					dMinorVersion = 0;
				}
			}
			else {
				hasD = false;
				dMajorVersion = 0;
				dMinorVersion = 0;
			}
		}
		catch (NumberFormatException ex) {
			throw new IllegalArgumentException("Invalid version string " + string, ex);
		}
	}

	public final boolean isTypeR() {
		return "R".equals(type);
	}
	
	public final boolean isTypeX() {
		return "X".equals(type);
	}

	public final boolean hasDVersion() {
		return hasD;
	}
	
	public final int getMajorVersion() {
		return majorVersion;
	}

	public final int getMinorVersion() {
		return minorVersion;
	}

	public final String getType() {
		return type;
	}

	public final int getTypeMajorVersion() {
		return typeMajorVersion;
	}

	public final int getTypeMinorVersion() {
		return typeMinorVersion;
	}

	public final int getDMajorVersion() {
		return dMajorVersion;
	}
	
	public final int getDMinorVersion() {
		return dMinorVersion;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + dMajorVersion;
		result = prime * result + dMinorVersion;
		result = prime * result + (hasD ? 1231 : 1237);
		result = prime * result + majorVersion;
		result = prime * result + minorVersion;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + typeMajorVersion;
		result = prime * result + typeMinorVersion;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Version)) {
			return false;
		}
		Version other = (Version) obj;
		if (dMajorVersion != other.dMajorVersion) {
			return false;
		}
		if (dMinorVersion != other.dMinorVersion) {
			return false;
		}
		if (hasD != other.hasD) {
			return false;
		}
		if (majorVersion != other.majorVersion) {
			return false;
		}
		if (minorVersion != other.minorVersion) {
			return false;
		}
		if (type == null) {
			if (other.type != null) {
				return false;
			}
		} else if (!type.equals(other.type)) {
			return false;
		}
		if (typeMajorVersion != other.typeMajorVersion) {
			return false;
		}
		if (typeMinorVersion != other.typeMinorVersion) {
			return false;
		}
		return true;
	}

	@Override
	public final String toString() {
		StringBuilder builder = new StringBuilder();
		builder
		.append(majorVersion)
		.append(".")
		.append(minorVersion);
		
		if (isTypeR() || isTypeX()) {
			builder
			.append(type)
			.append(typeMajorVersion);
			
			if (typeMinorVersion > 0) {
				builder
				.append(".")
				.append(typeMinorVersion);
			}
		}

		if (hasDVersion()) {
			builder
			.append("-D")
			.append(dMajorVersion);
			
			if (dMinorVersion > 0) {
				builder
				.append(".")
				.append(dMinorVersion);
			}
		}
		
		return builder.toString();
	}
	
	public final boolean isNewerThan12_1X47() {
		if (majorVersion > 12) {
			return true;
		}
		else if (majorVersion == 12 && (minorVersion >= 3 || (minorVersion == 1 && isTypeX() && typeMajorVersion >= 47))) {
			return true;
		}
		return false;
	}
	
	public static final boolean isNewerThan12_1X47(String string) {
		try {
			return (new Version(string)).isNewerThan12_1X47();
		}
		catch (Exception ex) {
			return false;
		}
	}


	public final boolean isSipHiddenVersion() {
		if (majorVersion > 15) 
			return true;
	
		if (majorVersion < 15) 
			return false;
	
		if (minorVersion > 1)
			return true;
	
		if (minorVersion < 1)
			return false;
	
		if (isTypeX() == false) 
			return false;
	
		if (typeMajorVersion > 49)
			return true;
		
		if (typeMajorVersion < 49)
			return false;
		
		if (hasDVersion() == false)
			return false;
		
		if (dMajorVersion >= 40)
			return true;
		
		return false;
	}
	
	public static final boolean isSipHiddenVersion(String string) {
		try {
			return (new Version(string)).isSipHiddenVersion();
		}
		catch (Exception ex) {
			return false;
		}
	}

}
