package to.lef.srxlib;

import java.util.ArrayList;
import java.util.List;

import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class PhysicalInterfaceInformation extends XML {

	public PhysicalInterfaceInformation(Document doc, Element root) {
		super(doc, root);
	}
	
	public NodeList physicalInterfaceNodeList() {
		return getNodeList("physical-interface");
	}

	public List<String> physicalInterfaceNameList() {
		ArrayList<String> ret = new ArrayList<String>();

		XPathExpression namePath = compileXpath("name/text()");

		for (Node n: IterableNodeList.apply(physicalInterfaceNodeList())) {
			String name = getString(namePath, n);
			if (Util.isPortPhysicalInterface(name) ||
					name.startsWith("reth") || 
					name.startsWith("irb")  || 
					name.startsWith("lo0")  || 
					name.startsWith("vlan")) {
				ret.add(name);
			}
		}

		return ret;
	}

	public boolean isPhysicalInterfaceExist(String name) {
		Node n = findPhysicalInterfaceNode(name);
		return (n == null) ? false : true;
	}

	public Node findPhysicalInterfaceNode(String name) {
		return getNode(String.format("physical-interface/name[normalize-space(text())=%s]/..", XML.xpc(name)));
	}

}
