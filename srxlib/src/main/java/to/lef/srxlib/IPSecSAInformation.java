package to.lef.srxlib;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.dom.XML;

public class IPSecSAInformation {
	private final String vpnName;
	private final String tunnelIndex;
	private final String remoteGateway;
	private final boolean active;

	public static IPSecSAInformation createFromNode(Node block, boolean active, XML xml) {
		String vpnName = xml.getString(block, "sa-vpn-name/text()");
		
		if (StringUtils.isBlank(vpnName))
			return null;
		
		String tunnelIndex = xml.getString(block, "sa-tunnel-index/text()");
		String remoteGateway = xml.getString(block, "sa-remote-gateway");
		
		return new IPSecSAInformation(vpnName, tunnelIndex, remoteGateway, active);
	}
	
	private IPSecSAInformation(String vpnName, String tunnelIndex, String remoteGateway, boolean active) {
		this.vpnName = vpnName;
		this.tunnelIndex = tunnelIndex;
		this.remoteGateway = remoteGateway;
		this.active = active;
	}

	public final String getVpnName() {
		return vpnName;
	}

	public final String getTunnelIndex() {
		return tunnelIndex;
	}

	public final String getRemoteGateway() {
		return remoteGateway;
	}

	public final boolean isActive() {
		return active;
	}
}
