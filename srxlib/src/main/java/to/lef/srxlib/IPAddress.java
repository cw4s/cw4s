package to.lef.srxlib;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class IPAddress {
	protected InetAddress address;
	protected int netmask;

	public IPAddress(String address) {
		setAddressAndNetmask(address);
	}

	public IPAddress(String address, int netmask) {
		setAddressAndNetmask(address, netmask);
	}	
	
	public IPAddress(InetAddress address, int netmask) {
		setAddressAndNetmask(address, netmask);
	}	
	
	public final void setAddressAndNetmask(String a) {
		String[] tmp = a.split("\\/");
		if (tmp.length > 1) {
			setAddress(tmp[0]);
			setNetmask(Integer.parseInt(tmp[1]));
		}
		else {
			setAddress(a);
			setNetmask(addressBit(this.address));
		}	
	}
	
	public final void setAddressAndNetmask(InetAddress a, int n) {
		setAddress(a);
		setNetmask(n);
	}
	
	public final void setAddressAndNetmask(String a, int n) {
		setAddress(a);
		setNetmask(n);
	}
	
	public final String getAddress() {
		return address.getHostAddress();
	}
	
	public final void setAddress(InetAddress address) {
		this.address = address;
	}
	
	public final void setAddress(String address) {
		try {
			this.address = InetAddress.getByName(address);
		} catch (UnknownHostException e) {
			throw new IllegalArgumentException("Invalid IP address.", e);
		}		
	}

	public final int getNetmask() {
		return netmask;
	}

	public final void setNetmask(int mask) {
		if (address == null)
			throw new IllegalStateException("Address is not set.");
	
		int max = addressBit(address);
		if (mask > max || mask < 0)
			throw new IllegalArgumentException("Invalid netmask.");
		
		this.netmask = mask;
	}

	private static int addressBit(InetAddress v) {
		if (v instanceof Inet4Address)
			return 32;
		else if (v instanceof Inet6Address) 
			return 128;
		throw new IllegalArgumentException("Unsupported address.");
	}
	
	public boolean isSameNetwork(String v) {
		boolean ret = false;
		try {
			ret = isSameNetwork(InetAddress.getByName(v));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	public boolean isSameNetwork(InetAddress v) {
		InetAddress a1 = getNetworkAddress();
		InetAddress a2 = getNetworkAddress(v, netmask);

		if (a1 == null || a2 == null) 
			return false;

		return a1.equals(a2);
	}

	public final InetAddress getNetworkAddress() {
		return getNetworkAddress(address, netmask);
	}
	
	public final static InetAddress getNetworkAddress(String addr, int mask) {
		InetAddress ret = null;
		try {
			ret = getNetworkAddress(InetAddress.getByName(addr), mask);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return ret;
	}

	public static InetAddress getNetworkAddress(InetAddress addr, int mask) {
		InetAddress ret = null;
		int  max = 0;
		byte m   = 0;
		try {
			max = addressBit(addr);
			if (mask < 0 || max < mask) 
				return null;

			byte[] a  = addr.getAddress();
			for (int i = 0, mod = 7; i < max; i++) {
				if (i < mask) {
					m |= (1 << mod);
				}

				if (mod == 0) {
					a[i / 8] &= m;
					m         = 0;
					mod       = 7;
				}
				else {
					mod--;
				}
			}

			ret = InetAddress.getByAddress(a);
		}
		catch (Throwable e) {
			e.printStackTrace();
		}

		return ret;
	}

	public final InetAddress getBroadcastAddress() {
		return getBroadcastAddress(address, netmask);
	}
	
	public final static InetAddress getBroadcastAddress(String addr, int mask) {
		InetAddress ret = null;
		try {
			ret = getBroadcastAddress(InetAddress.getByName(addr), mask);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	public static InetAddress getBroadcastAddress(InetAddress addr, int mask) {
		InetAddress ret = null;
		int  max = 0;
		byte m   = 0;
		try {
			max = addressBit(addr);
			if (mask < 0 || max < mask) 
				return null;
			
			byte[]      a  = addr.getAddress();
			for (int i = 0, mod = 7; i < max; i++) {
				if (i >= mask) {
					m |= (1 << mod);
				}
				
				if (mod == 0) {
					a[i / 8] |= m;
					m         = 0;
					mod       = 7;
				}
				else {
					mod--;
				}
			}
			
			ret = InetAddress.getByAddress(a);
		}
		catch (Throwable e) {
			e.printStackTrace();
		}
		
		return ret;	
	}

	public boolean isHostAddress() {
		int maxMask = addressBit(address);

		if (netmask != maxMask && (netmask == 0 || address.equals(getNetworkAddress()) || address.equals(getBroadcastAddress())))
			return false;

		if (address.isAnyLocalAddress() 
				|| address.isLinkLocalAddress()
				|| address.isLoopbackAddress()
				|| address.isMulticastAddress() 
				)
			return false; 
		
		return true;	
	}

	public final boolean isIPv4Address() {
		return (address instanceof Inet4Address);
	}
	
	public final boolean isIPv6Address() {
		return (address instanceof Inet6Address);
	}
	
	@Override
	public String toString() {
		return String.format("%s/%d", address.getHostAddress(), netmask);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + netmask;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof IPAddress)) {
			return false;
		}
		IPAddress other = (IPAddress) obj;
		if (address == null) {
			if (other.address != null) {
				return false;
			}
		} else if (!address.equals(other.address)) {
			return false;
		}
		if (netmask != other.netmask) {
			return false;
		}
		return true;
	}
}
