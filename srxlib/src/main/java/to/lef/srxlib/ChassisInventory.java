package to.lef.srxlib;

import java.util.ArrayList;
import java.util.List;


import org.w3c.dom.Document;
import org.w3c.dom.Node;

import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

import java.util.Collections;

public class ChassisInventory extends XML {
	private List<Node> nodeList;
	
	public static class ModuleEntry {
		private String name;
		private String version;
		private String serialNumber;
		
		public ModuleEntry(String name, String version, String serialNumber) {
			this.name = name;
			this.version = version;
			this.serialNumber = serialNumber;
		}

		public final String getName() {
			return name;
		}

		public final String getVersion() {
			return version;
		}

		public final String getSerialNumber() {
			return serialNumber;
		}
	}
	
	public ChassisInventory(Document doc) {
		super(doc);
	
		nodeList = new ArrayList<>();
		for (Node n : IterableNodeList.apply(getNodeList("//chassis-inventory"))) {
			nodeList.add(n);
		}
		
		if (nodeList.size() == 0) 
			throw new IllegalArgumentException("Invalid chassis inventory.");
	}

	public final String getSerialNumber() {
		return getSerialNumber(0);
	}
	
	public final String getSerialNumber(int nodeIdx) {
		return getString(nodeList.get(nodeIdx), "chassis/serial-number/text()");
	}
	
	public List<ModuleEntry> getModuleList() {
		List<ModuleEntry> ret = new ArrayList<ModuleEntry>();

		for (Node no: nodeList) {
			for (Node m : IterableNodeList.apply(getNodeList(no, "chassis/chassis-module"))) {
				String name         = getString(m, "name/text()");
				String version      = getString(m, "version/text()");
				String serialNumber = getString(m, "serial-number/text()");

				ModuleEntry entry = new ModuleEntry(name, version, serialNumber);
				ret.add(entry);
			}
		}
		
		return Collections.unmodifiableList(ret);
	}
}
