package to.lef.srxlib;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ArrayNodeList implements NodeList {
	List<Node> list;
	
	public ArrayNodeList(List<Node>list) {
		this.list = (list == null) ? new ArrayList<Node>() : list;
	}

	@Override
	public Node item(int index) {
		return list.get(index);
	}

	@Override
	public int getLength() {
		return list.size();
	}
}
