package to.lef.srxlib.l2ng;

import to.lef.srxlib.nic.VlanInterface;

public class L2ngVlanInterface extends VlanInterface {

	public L2ngVlanInterface(int unitId, int vlanId) {
		super("irb", unitId, vlanId);
	}

}
