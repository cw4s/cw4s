package to.lef.srxlib.l2ng;


import org.w3c.dom.Node;

import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.nic.PortVlanInterface;

public class L2ngPortVlanInterface extends PortVlanInterface {

	public L2ngPortVlanInterface(String name, int unitId) {
		super(name, unitId, "interface-mode");
	}

	@Override
	protected void configBind(DeviceConfig cfg, String intName, String vname, String mode) {
		super.configBind(cfg, intName, vname, mode);
		fixProtocol(cfg);
	}

	@Override
	protected void configUnbind(DeviceConfig cfg, String intName, String vname) {
		super.configUnbind(cfg, intName, vname);
		fixProtocol(cfg);
	}

	private void fixProtocol(DeviceConfig cfg) {
		if (cfg.getNodeList("interfaces/interface/unit[family/" + FAMILY_VLAN + "]").getLength() > 0) {
			Node p = cfg.createElementsIfNotExist("protocols", "l2-learning", "global-mode");
			XML.removeAllChildNodes(p);
			p.appendChild(cfg.createTextNode("switching"));
		}
		else {
			Node p = cfg.getNode("protocols/l2-learning/global-mode");
			if (p != null) {
				Node parent = p.getParentNode();
				if (parent != null) {
					parent.removeChild(p);
					p = parent;
					while (p != null && cfg.getNodeList(p, "*").getLength() == 0) {
						parent = p.getParentNode();
						if (parent != null) {
							parent.removeChild(p);
							p = parent;
						}
					}
				}
			}
		}

	}
}
