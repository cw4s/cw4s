package to.lef.srxlib;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Collections;


import org.w3c.dom.Node;

import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class AlarmInformation {
	public static class AlarmEntry {
		private Date date;
		private String alarmClass;
		private String description;
		private String alarmType;
	
		public static class Comparator implements java.util.Comparator<AlarmEntry> {

			@Override
			public int compare(AlarmEntry o1, AlarmEntry o2) {
				long l1 = o1.date.getTime();
				long l2 = o2.date.getTime();
				
				if (l1 < l2)
					return 1;
				else if (l1 > l2) 
					return -1;
				return 0;
			}
			
		}
		
		public AlarmEntry(Date time, String alarmClass, String description, String alarmType) {
			this.date = time;
			this.alarmClass = alarmClass;
			this.description = description;
			this.alarmType = alarmType;
		}

		public final Date getDate() {
			return date;
		}

		public final String getAlarmClass() {
			return alarmClass;
		}

		public final String getDescription() {
			return description;
		}

		public final String getAlarmType() {
			return alarmType;
		}
	}
	
	private List<AlarmEntry> alarmList;
	private int majorCount;
	private int minorCount;
	private int totalCount;
	public AlarmInformation(XML ... xmls) {
		alarmList = new ArrayList<AlarmEntry>();
		majorCount = 0;
		minorCount = 0;
		totalCount = 0;
		for(XML xml : xmls) {
			for (Node no : IterableNodeList.apply(xml.getNodeList("//alarm-information"))) {
				for(Node a: IterableNodeList.apply(xml.getNodeList(no, "alarm-detail"))) {
					try {
						Date date = new Date(Long.parseLong(xml.getString(a, "alarm-time/@seconds")) * 1000);
						String alarmClass = xml.getString(a, "alarm-class/text()");
						String description = xml.getString(a, "alarm-description/text()");
						String alarmType = xml.getString(a, "alarm-type/text()");

						if ("Minor".equals(alarmClass)) {
							minorCount++;
						}
						else if ("Major".equals(alarmClass)) {
							majorCount++;
						}
						totalCount++;
						AlarmEntry e = new AlarmEntry(date, alarmClass, description, alarmType);
						alarmList.add(e);
					}
					catch (NumberFormatException e) {
						e.printStackTrace();
					}
				}
			}
			
			Collections.sort(alarmList, new AlarmEntry.Comparator());
		}
	}
	
	public final List<AlarmEntry> getAlarmList() {
		return Collections.unmodifiableList(alarmList);
	}

	public final int getTotalCount() {
		return totalCount;
	}
	
	public final int getMajorCount() {
		return majorCount;
	}

	public final int getMinorCount() {
		return minorCount;
	}
	
}
