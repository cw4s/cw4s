package to.lef.srxlib.dhcp;

import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.nic.LogicalInetInterface.IPv4AddressAndMask;

public class DhcpNone extends DhcpService {
	public DhcpNone(String logicalInterfaceName, IPv4AddressAndMask networkAddress) {
		super(logicalInterfaceName, networkAddress);
	}

	@Override
	protected void configEdit(DhcpService old, DeviceConfig cfg) throws DeviceConfigException {
		if (old instanceof DhcpRelayAgent) {
			old.configDelete(cfg);
		}
	}

	@Override
	protected void configDelete(DeviceConfig cfg) throws DeviceConfigException {
		
	}
}
