package to.lef.srxlib.dhcp;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.nic.LogicalInetInterface;
import to.lef.srxlib.nic.LogicalInetInterface.IPv4AddressAndMask;
import to.lef.srxlib.nic.LogicalInterface;

public class DhcpRelayAgent extends DhcpService {
	private List<String> relayServerList;
	
	public DhcpRelayAgent(String logicalInterfaceName, IPv4AddressAndMask networkAddress) {
		super(logicalInterfaceName, networkAddress);
		relayServerList = new ArrayList<>();
	}

	public List<String> getRelayServerList() {
		return Collections.unmodifiableList(relayServerList);
	}

	public void addRelayServer(String v) {
		if (!relayServerList.contains(v))
			relayServerList.add(v);
	}
	
	@Override
	protected void configEdit(DhcpService old, DeviceConfig cfg) throws DeviceConfigException {
		LogicalInterface lint = cfg.findLogicalInterface(logicalInterfaceName);
		if (lint == null)
			return;
		
		if (lint instanceof LogicalInetInterface) {
			for(DhcpService dhcp : ((LogicalInetInterface) lint).getDhcpServiceList(cfg)) {
				if (dhcp instanceof DhcpServer) 
					dhcp.configDelete(cfg);
			}
		}
		else {
			return;
		}
		
		Node bootp = cfg.createElementsIfNotExist("forwarding-options", "helpers", "bootp");
		Node intf = cfg.getNode(bootp, "interface[normalize-space(name/text())=%s]", XML.xpc(logicalInterfaceName));
		if (intf == null) {
			intf = cfg.createElement("interface");
			intf.appendChild(cfg.createElement("name", logicalInterfaceName));
			bootp.appendChild(intf);
		}
		
		cfg.deleteNode(intf, "server");
		for (String serverName: relayServerList) {
			if (StringUtils.isNotBlank(serverName)) {
				Node server = cfg.createElement("server");
				server.appendChild(cfg.createElement("name", serverName));
				intf.appendChild(server);
			}
		}
	}

	@Override
	protected void configDelete(DeviceConfig cfg) throws DeviceConfigException {
		Node bootp = cfg.getNode("forwarding-options/helpers/bootp");
		if (bootp == null)
			return;
		
		Node intf = cfg.getNode(bootp, "interface[normalize-space(name/text())=%s]", XML.xpc(logicalInterfaceName));
		if (intf == null) {
			return;
		}
		
		bootp.removeChild(intf);
		
		Node target = bootp;
		while(cfg.getNode(target, "*") == null) {
			Node tmp = target;
			target = tmp.getParentNode();
			target.removeChild(tmp);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((relayServerList == null) ? 0 : relayServerList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof DhcpRelayAgent)) {
			return false;
		}
		DhcpRelayAgent other = (DhcpRelayAgent) obj;
		if (relayServerList == null) {
			if (other.relayServerList != null) {
				return false;
			}
		} else if (!(other.relayServerList != null && relayServerList.size() == other.relayServerList.size() && relayServerList.containsAll(other.relayServerList))) {
			return false;
		}
		return true;
	}
}
