package to.lef.srxlib.dhcp;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.nic.LogicalInetInterface.IPv4AddressAndMask;

public class DhcpServer extends DhcpService {
	private String gateway;
	private String lowAddress;
	private String highAddress;
	private List<String> dnsServerList;
	private List<String> winsServerList;
	private int leaseTime;
	
	public DhcpServer(String logicalInterfaceName, IPv4AddressAndMask networkAddress) {
		super(logicalInterfaceName, networkAddress);
		this.leaseTime = -1;
		this.dnsServerList = new ArrayList<>();
		this.winsServerList = new ArrayList<>();
	}

	public final String getGateway() {
		return gateway;
	}

	public final void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public final String getLowAddress() {
		return lowAddress;
	}

	public final void setLowAddress(String lowAddress) {
		this.lowAddress = lowAddress;
	}

	public final String getHighAddress() {
		return highAddress;
	}

	public final void setHighAddress(String highAddress) {
		this.highAddress = highAddress;
	}

	public final int getLeaseTime() {
		return leaseTime;
	}

	public final void setLeaseTime(int leaseTime) {
		this.leaseTime = leaseTime;
	}

	public final List<String> getDnsServerList() {
		return Collections.unmodifiableList(dnsServerList);
	}

	public final void insertDnsServer(int i, String v) {
		if (StringUtils.isBlank(v) || dnsServerList.contains(v))
			return;
		
		if (0 <= i && i < dnsServerList.size()) {
			dnsServerList.set(i, v);
		}
		else {
			dnsServerList.add(v);
		}
	}
	
	public final List<String> getWinsServerList() {
		return Collections.unmodifiableList(winsServerList);
	}
	
	public final void insertWinsServer(int i, String v) {
		if (StringUtils.isBlank(v) || winsServerList.contains(v))
			return;
		
		if (0 <= i && i < winsServerList.size()) {
			winsServerList.set(i, v);
		}
		else {
			winsServerList.add(v);
		}
	}
	
	@Override
	protected void configEdit(DhcpService old, DeviceConfig cfg) throws DeviceConfigException {
		if (old instanceof DhcpRelayAgent) {
			old.configDelete(cfg);
		}
		
		Node dhcp = cfg.createElementsIfNotExist("system", "services", "dhcp");
		Node pool = cfg.getNode(dhcp, "pool[normalize-space(name/text())=%s]", XML.xpc(networkAddress.toString()));
		if (pool == null) {
			pool = cfg.createElement("pool");
			pool.appendChild(cfg.createElement("name", networkAddress.toString()));
			dhcp.appendChild(pool);
		}
		
		cfg.deleteNode(pool, "address-range");
		if (StringUtils.isNotBlank(lowAddress) || StringUtils.isNotBlank(highAddress)) {
			Node n = cfg.createElement("address-range");
			if (StringUtils.isNotBlank(lowAddress)) {
				n.appendChild(cfg.createElement("low", lowAddress));
			}
			if (StringUtils.isNotBlank(highAddress)) {
				n.appendChild(cfg.createElement("high", highAddress));
			}
			pool.appendChild(n);
		}
	
		cfg.deleteNode(pool, "router");
		if (StringUtils.isNotBlank(gateway)) {
			Node n = cfg.createElement("router");
			n.appendChild(cfg.createElement("name", gateway));
			pool.appendChild(n);
		}
	
		cfg.deleteNode(pool, "default-lease-time");
		if (leaseTime >= 0) {
			pool.appendChild(cfg.createElement("default-lease-time", "%d", leaseTime));
		}
		
		if (old instanceof DhcpServer) {
			if (((DhcpServer) old).dnsServerList.size() > 0) {
				String tmp = ((DhcpServer) old).dnsServerList.get(0);
				if (StringUtils.isNotBlank(tmp)) {
					cfg.deleteNode(pool, "name-server[normalize-space(name/text())=%s]", XML.xpc(tmp));
				}
			}
			
			if (((DhcpServer) old).winsServerList.size() > 0) {
				String tmp = ((DhcpServer) old).winsServerList.get(0);
				if (StringUtils.isNotBlank(tmp)) {
					cfg.deleteNode(pool, "wins-server[normalize-space(name/text())=%s]", XML.xpc(tmp));
				}
			}
		}
		
		if (dnsServerList.size() > 0) {
			String tmp = dnsServerList.get(0);
			if (StringUtils.isNotBlank(tmp)) {
				Node n = cfg.getNode(pool, "name-server[normalize-space(name/text())=%s]", XML.xpc(tmp));
				if (n == null) {
					n = cfg.createElement("name-server");
					n.appendChild(cfg.createElement("name", tmp));
					pool.insertBefore(n, cfg.getNode(pool, "name-server"));
				}
			}
		}
		
		if (winsServerList.size() > 0) {
			String tmp = winsServerList.get(0);
			if (StringUtils.isNotBlank(tmp)) {
				Node n = cfg.getNode(pool, "wins-server[normalize-space(name/text())=%s]", XML.xpc(tmp));
				if (n == null) {
					n = cfg.createElement("wins-server");
					n.appendChild(cfg.createElement("name", tmp));
					pool.insertBefore(n, cfg.getNode(pool, "wins-server"));
				}
			}
		}
	}

	@Override
	protected void configDelete(DeviceConfig cfg) throws DeviceConfigException {
		Node dhcp = cfg.getNode("system/services/dhcp");
		if (dhcp == null)
			return;
		
		Node pool = cfg.getNode(dhcp, "pool[normalize-space(name/text())=%s]", XML.xpc(networkAddress.toString()));
		if (pool == null)
			return;
		
		dhcp.removeChild(pool);
		Node target = dhcp;
		while(cfg.getNode(target, "*") == null) {
			Node tmp = target;
			target = tmp.getParentNode();
			target.removeChild(tmp);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((dnsServerList == null) ? 0 : dnsServerList.hashCode());
		result = prime * result + ((gateway == null) ? 0 : gateway.hashCode());
		result = prime * result + ((highAddress == null) ? 0 : highAddress.hashCode());
		result = prime * result + leaseTime;
		result = prime * result + ((lowAddress == null) ? 0 : lowAddress.hashCode());
		result = prime * result + ((winsServerList == null) ? 0 : winsServerList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof DhcpServer)) {
			return false;
		}
		DhcpServer other = (DhcpServer) obj;
		if (dnsServerList == null) {
			if (other.dnsServerList != null) {
				return false;
			}
		} else if (!(other.dnsServerList != null && dnsServerList.size() == other.dnsServerList.size() && dnsServerList.containsAll(other.dnsServerList))) {
			return false;
		}
		if (StringUtils.isBlank(gateway)) {
			if (StringUtils.isNotBlank(other.gateway)) {
				return false;
			}
		} else if (!gateway.equals(other.gateway)) {
			return false;
		}
		if (StringUtils.isBlank(highAddress)) {
			if (StringUtils.isNotBlank(other.highAddress)) {
				return false;
			}
		} else if (!highAddress.equals(other.highAddress)) {
			return false;
		}
		if (leaseTime != other.leaseTime) {
			return false;
		}
		if (StringUtils.isBlank(lowAddress)) {
			if (StringUtils.isNotBlank(other.lowAddress)) {
				return false;
			}
		} else if (!lowAddress.equals(other.lowAddress)) {
			return false;
		}
		if (winsServerList == null) {
			if (other.winsServerList != null) {
				return false;
			}
		} else if (!(other.winsServerList != null && winsServerList.size() == other.winsServerList.size() && winsServerList.containsAll(other.winsServerList))) {
			return false;
		}
		return true;
	}
}
