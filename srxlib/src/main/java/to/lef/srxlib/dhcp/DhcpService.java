package to.lef.srxlib.dhcp;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.nic.LogicalInetInterface.IPv4AddressAndMask;

public abstract class DhcpService {

	protected String logicalInterfaceName;
	protected IPv4AddressAndMask networkAddress;
	
	public DhcpService(String logicalInterfaceName, IPv4AddressAndMask networkAddress) {
		this.logicalInterfaceName = logicalInterfaceName;
		this.networkAddress = networkAddress;
	}

	public final String getLogicalInterfaceName() {
		return logicalInterfaceName;
	}

	public final IPv4AddressAndMask getNetworkAddress() {
		return networkAddress;
	}

	public final DeviceConfigure getEditConfigure(final DhcpService old) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					configEdit(old, cfg);
				}
				catch (DeviceConfigException e) {
					throw e;
				}
				catch (Throwable e) {
					new DeviceConfigException("Failed to config dhcp service.", e);
				}
			}
			
		};
	}

	public final DeviceConfigure getDeleteConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					configDelete(cfg);
				}
				catch (DeviceConfigException e) {
					throw e;
				}
				catch (Throwable e) {
					new DeviceConfigException("Failed to delete dhcp service.", e);
				}			
			}
			
		};
	}
	
	abstract protected void configEdit(DhcpService old, DeviceConfig config) throws DeviceConfigException;
	abstract protected void configDelete(DeviceConfig config) throws DeviceConfigException;
	
	public static DhcpService createFromConfig(String logicalInterfaceName, IPv4AddressAndMask networkAddress, DeviceConfig config) {
		Node bootp = config.getNode("forwarding-options/helpers/bootp");
		if (bootp != null) {
			Node intf = config.getNode(bootp, "interface[normalize-space(name/text())=%s]", XML.xpc(logicalInterfaceName));
			if (intf != null) {
				DhcpRelayAgent agent = new DhcpRelayAgent(logicalInterfaceName, networkAddress);
				for (Node server: IterableNodeList.apply(config.getNodeList(intf, "server"))) {
					String serverName = config.getString(server, "name/text()");
					if (StringUtils.isNotBlank(serverName)) 
						agent.addRelayServer(serverName);
				}
				return agent;
			}
		}
		
		Node dhcp = config.getNode("system/services/dhcp");
		if (dhcp != null) {
			Node pool = config.getNode(dhcp, "pool[normalize-space(name/text())=%s]", XML.xpc(networkAddress.toString()));
			if (pool != null) {
				DhcpServer server = new DhcpServer(logicalInterfaceName, networkAddress);
				Node n = config.getNode(pool, "address-range");
				if (n != null) {
					server.setLowAddress(config.getString(n, "low/text()"));
					server.setHighAddress(config.getString(n, "high/text()"));
				}
				String lt = config.getString(pool, "default-lease-time/text()");
				if (StringUtils.isNotBlank(lt)) {
					try {
						server.setLeaseTime(Integer.parseInt(lt));
					}
					catch (NumberFormatException e) {
						server.setLeaseTime(-1);
					}
				}
				server.setGateway(config.getString(pool, "router/name/text()"));
				for (Node s: IterableNodeList.apply(config.getNodeList(pool, "dns-server"))) {
					String name = config.getString(s, "name/text()");
					if (StringUtils.isNotBlank(name)) {
						server.insertDnsServer(-1, name);
					}
				}
				for (Node s: IterableNodeList.apply(config.getNodeList(pool, "wins-server"))) {
					String name = config.getString(s, "name/text()");
					if (StringUtils.isNotBlank(name)) {
						server.insertWinsServer(-1, name);
					}
				}

				return server;
			}
		}
		
		return new DhcpNone(logicalInterfaceName, networkAddress);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((logicalInterfaceName == null) ? 0 : logicalInterfaceName.hashCode());
		result = prime * result + ((networkAddress == null) ? 0 : networkAddress.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DhcpService)) {
			return false;
		}
		DhcpService other = (DhcpService) obj;
		if (StringUtils.isBlank(logicalInterfaceName)) {
			if (StringUtils.isNotBlank(other.logicalInterfaceName)) {
				return false;
			}
		} else if (!logicalInterfaceName.equals(other.logicalInterfaceName)) {
			return false;
		}
		if (networkAddress == null) {
			if (other.networkAddress != null) {
				return false;
			}
		} else if (!networkAddress.equals(other.networkAddress)) {
			return false;
		}
		return true;
	}
	
	public int isChanged(Map<IPv4AddressAndMask, DhcpService> candidate) {
		if (candidate == null)
			return 1;
		
		DhcpService c = candidate.get(getNetworkAddress());

		if (c == null)
			return 1;
		if (!equals(c))
			return 2;
		return 0;
	}
}
