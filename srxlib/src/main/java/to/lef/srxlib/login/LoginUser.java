package to.lef.srxlib.login;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.Util;
import to.lef.srxlib.dom.IterableNodeList;

import java.util.Collections;

public class LoginUser {
	public static final String ROOT_NAME = "root";
	
	public static final String CLASS_OPERATOR = "operator";
	public static final String CLASS_READ_ONLY = "read-only";
	public static final String CLASS_SUPER_USER = "super-user";
	public static final String CLASS_UNAUTHORIZED = "unauthorized";
	
	protected static final String TAG_SSH_DSA = "ssh-dsa";
	protected static final String TAG_SSH_RSA = "ssh-rsa";
	
	private final String name;
	private final String password;
	private final String className;

	private final List<String> sshDsaKeyList;
	private final List<String> sshRsaKeyList;
	
	public static List<String> classList(DeviceConfig config) {
		List<String> ret = new ArrayList<>();
		
		ret.add(CLASS_OPERATOR);
		ret.add(CLASS_READ_ONLY);
		ret.add(CLASS_SUPER_USER);
		ret.add(CLASS_UNAUTHORIZED);
	
		for (Node c: IterableNodeList.apply(config.getNodeList(config.loginNode(), "class"))) {
			String n = config.getString(c, "name/text()");
			if (StringUtils.isNotBlank(n))
				ret.add(n);
		}
		return ret;
	}
	
	public static LoginUser createFromNode(Node node, DeviceConfig config) {
		String name = null;
		String password = null;
		LoginUser ret = null;
		Node auth = null;
		
		if ("root-authentication".equals(((Element)node).getTagName())) {
			password = config.getString(node, "encrypted-password/text()");
			auth = node;
			ret = new LoginUser(ROOT_NAME, password, CLASS_SUPER_USER);
		}
		else {
			name = config.getString(node, "name/text()");
			if (StringUtils.isBlank(name))
				return null;
		
			String className = config.getString(node, "class/text()");
			auth = config.getNode(node, "authentication");
			password = (auth == null) ? null : config.getString(auth, "encrypted-password/text()");
		
			ret = new LoginUser(name, password, className);
		}

		if (auth != null) {
			for(Node n: IterableNodeList.apply(config.getNodeList(auth, TAG_SSH_DSA))) {
				String key = config.getString(n, "name/text()");
				if (StringUtils.isNotBlank(key))
					ret.addSshDsaKey(key);
			}
			
			for(Node n: IterableNodeList.apply(config.getNodeList(auth, TAG_SSH_RSA))) {
				String key = config.getString(n, "name/text()");
				if (StringUtils.isNotBlank(key))
					ret.addSshRsaKey(key);
			}		
		}

		return ret;
	}
	
	public LoginUser(String name, String password, String className) {
		this.name = name;
		this.password = password;
		this.className = className;
		this.sshDsaKeyList = new ArrayList<>();
		this.sshRsaKeyList = new ArrayList<>();
	}

	public final String getName() {
		return name;
	}

	public final String getPassword() {
		return password;
	}

	public final String getClassName() {
		return className;
	}

	public final List<String> getSshDsaKeyList() {
		return Collections.unmodifiableList(sshDsaKeyList);
	}

	public void addSshDsaKey(String key) {
		if (!sshDsaKeyList.contains(key)) {
			if (StringUtils.isBlank(key))
				throw new IllegalArgumentException("Invalid ssh dsa key.");

			String[] tmp = key.split("\\s");
			if (tmp.length < 2)
				throw new IllegalArgumentException("Invalid ssh dsa key.");

			if (!"ssh-dss".equals(tmp[0]))
				throw new IllegalArgumentException("Invalid ssh dsa key.");		
			
			sshDsaKeyList.add(key);
		}
	}

	public void removeSshDsaKey(String key) {
		sshDsaKeyList.remove(key);
	}
	
	public void removeSshDsaKey(int index) {
		if (0 <= index && index < sshDsaKeyList.size())
			sshDsaKeyList.remove(index);
	}
	
	public final List<String> getSshRsaKeyList() {
		return Collections.unmodifiableList(sshRsaKeyList);
	}
	
	public void addSshRsaKey(String key) {
		if (!sshRsaKeyList.contains(key)) {
			if (StringUtils.isBlank(key))
				throw new IllegalArgumentException("Invalid ssh rsa key.");

			String[] tmp = key.split("\\s");
			if (tmp.length < 2)
				throw new IllegalArgumentException("Invalid ssh rsa key.");		
			
			if (!"ssh-rsa".equals(tmp[0]))
				throw new IllegalArgumentException("Invalid ssh rsa key.");		
			
			sshRsaKeyList.add(key);
		}
	}
	
	public void removeSshRsaKey(String key) {
		sshRsaKeyList.remove(key);
	}
	
	public void removeSshRsaKey(int index) {
		if (0 <= index && index < sshRsaKeyList.size())
			sshRsaKeyList.remove(index);
	}
		
	private void configPassword(Node n, DeviceConfig config) {
		config.deleteNode(n, "encrypted-password");

		if (StringUtils.isNotBlank(password)) {
			n.appendChild(config.createElement("encrypted-password", Util.makeMD5Password(password)));
		} else if (config.getNode(n, "*") == null) {
			n.getParentNode().removeChild(n);	
		}
	}
	
	private void configUser(Node n, DeviceConfig config) {
		Node uid = config.getNode(n, "uid");
		config.deleteNode(n, "name");
		n.insertBefore(config.createElement("name", name), uid);
		
		config.deleteNode(n, "class");
		if (StringUtils.isNotBlank(className)) {
			n.insertBefore(config.createElement("class", className), uid);
		}
	}
	
	public DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				
				try {
					Node loginNode = config.loginNode();
					Node n = config.findLoginUserNode(loginNode, name);
					
					if (n != null) {
						throw new IllegalArgumentException("Login user is already exist.");
					}
					
					n = config.createElement("user");
					loginNode.appendChild(n);
					
					configUser(n, config);
					if (StringUtils.isNotBlank(password)) {
						configPassword(config.createElementsIfNotExist(n, "authentication"), config);
					}
				}
				catch (Exception e) {
					throw new DeviceConfigException("Failed to config login user.", e);
				}
			}
			
		};
	}
	
	public DeviceConfigure getEditConfigure(final LoginUser old) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				
				try {
					Node loginNode = config.loginNode();
					Node n = config.findLoginUserNode(loginNode, old.name);
					
					if (n == null) {
						throw new IllegalArgumentException("Login user is not exist.");
					}	
			
					if (old.isRoot()) {
						if (StringUtils.isNotBlank(password)) {
							configPassword(n, config);
						}					
					}
					else {
						configUser(n, config);
						if (StringUtils.isNotBlank(password)) {
							configPassword(config.createElementsIfNotExist(n, "authentication"), config);
						}
					}
					
				}
				catch (Exception e) {
					throw new DeviceConfigException("Failed to config login user.", e);
				}				
			}
			
		};
	}
	
	public DeviceConfigure getDeleteConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					Node loginNode = config.loginNode();
					Node n = config.findLoginUserNode(loginNode, name);
					
					if (!isRoot() && n != null) {
						loginNode.removeChild(n);
						
						if (config.getNode(loginNode, "*") == null) 
							loginNode.getParentNode().removeChild(loginNode);
					}					
				}
				catch (Exception e) {
					throw new DeviceConfigException("Failed to config login user.", e);
				}				
			}
			
		};
	}
	
	protected void configSshKeys(String tag, Node authNode, DeviceConfig config) {
		config.deleteNode(authNode, tag);

		for (String key: (TAG_SSH_DSA.equals(tag)) ? sshDsaKeyList : sshRsaKeyList) {
			Node rsa = config.createElement(tag);
			rsa.appendChild(config.createElement("name", key));
			authNode.appendChild(rsa);
		}

		if (config.getNode(authNode, "*") == null)
			authNode.getParentNode().removeChild(authNode);	
	}

	protected DeviceConfigure getSshKeysConfigure(final String tag) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					Node loginNode = config.loginNode();
					Node n = config.findLoginUserNode(loginNode, name);
					
					if (n == null) {
						throw new IllegalArgumentException("Login user is not exist.");
					}	
			
					Node authNode = (isRoot()) ? n : config.createElementsIfNotExist(n, "authentication");
					configSshKeys(tag, authNode, config);
				}
				catch (Exception e) {
					throw new DeviceConfigException("Failed to config login user.", e);
				}
			}
			
		};	
	}
	
	public DeviceConfigure getSshDsaConfigure() {
		return getSshKeysConfigure(TAG_SSH_DSA);
	}

	public DeviceConfigure getSshRsaConfigure() {
		return getSshKeysConfigure(TAG_SSH_RSA);
	}
		
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((className == null) ? 0 : className.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((sshDsaKeyList == null) ? 0 : sshDsaKeyList.hashCode());
		result = prime * result + ((sshRsaKeyList == null) ? 0 : sshRsaKeyList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof LoginUser)) {
			return false;
		}
		LoginUser other = (LoginUser) obj;
		if (StringUtils.isBlank(className)) {
			if (StringUtils.isNotBlank(other.className)) {
				return false;
			}
		} else if (!className.equals(other.className)) {
			return false;
		}
		if (StringUtils.isBlank(name)) {
			if (StringUtils.isNotBlank(other.name)) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (StringUtils.isBlank(password)) {
			if (StringUtils.isNotBlank(other.password)) {
				return false;
			}
		} else if (!password.equals(other.password)) {
			return false;
		}
		if (sshDsaKeyList == null) {
			if (other.sshDsaKeyList != null) {
				return false;
			}
		} else if (!(other.sshDsaKeyList != null && sshDsaKeyList.size() == other.sshDsaKeyList.size() && sshDsaKeyList.containsAll(other.sshDsaKeyList))) {
			return false;
		}
		if (sshRsaKeyList == null) {
			if (other.sshRsaKeyList != null) {
				return false;
			}
		} else if (!(other.sshRsaKeyList != null && sshRsaKeyList.size() == other.sshRsaKeyList.size() && sshRsaKeyList.containsAll(other.sshRsaKeyList))) {
			return false;
		}
		return true;
	}
	
	public int isChanged(Map<String, LoginUser> candidate) {
        LoginUser u = candidate.get(getName());

        if (u == null)
        	return 1;
        if (!equals(u))
        	return 2;

        return 0;
	}

	public final  boolean isRoot() {
		return ROOT_NAME.equals(name);
	}
	
	public final boolean isRemovable() {
		return !isRoot();
	}
}
