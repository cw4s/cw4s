package to.lef.srxlib;


import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class UptimeInformation extends XML {
	private List<Node> nodeList;
	
	public UptimeInformation(Document doc) {
		super(doc);
		
		nodeList = new ArrayList<>();
		for (Node n : IterableNodeList.apply(getNodeList("//system-uptime-information"))) {
			nodeList.add(n);
		}
		
		if (nodeList.size() == 0)
			throw new IllegalArgumentException("Invalid uptime information.");
	}

	public final String getUptime() {
		return getUptime(0);
	}
	
	public final String getUptime(int nodeIdx) {
		return getString(nodeList.get(nodeIdx), "uptime-information/up-time/text()");
	}
	
	public final String getCurrentTime() {
		return getCurrentTime(0);
	}
	
	public final String getCurrentTime(int nodeIdx) {
		return getString(nodeList.get(nodeIdx), "current-time/date-time/text()");
	}
}
