package to.lef.srxlib;

public interface DeviceConfigure {
	public void config(DeviceConfig configuration, DeviceInformationProvider info) throws DeviceConfigException;
}
