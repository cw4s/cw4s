package to.lef.srxlib;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import java.net.UnknownHostException;
import java.util.*;

import to.lef.srxlib.alg.ALG;
import to.lef.srxlib.dom.EmptyNodeList;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.login.LoginUser;
import to.lef.srxlib.nic.*;
import to.lef.srxlib.nat.*;
import to.lef.srxlib.policy.AddressElement;
import to.lef.srxlib.policy.AddressSet;
import to.lef.srxlib.policy.Policy;
import to.lef.srxlib.route.StaticRoute;
import to.lef.srxlib.snmp.Snmp;
import to.lef.srxlib.snmp.SnmpCommunity;
import to.lef.srxlib.syslog.Syslog;
import to.lef.srxlib.vpn.IPSecPolicy;
import to.lef.srxlib.vpn.IPSecProposal;
import to.lef.srxlib.vpn.IPSecVpn;
import to.lef.srxlib.vpn.IkeGateway;
import to.lef.srxlib.vpn.IkePolicy;
import to.lef.srxlib.vpn.IkeProposal;
import to.lef.srxlib.zone.*;

public class DeviceConfig extends BaseConfig {
	private final SoftwareInformation softwareInformation;
	
	public DeviceConfig(Document doc, Element root, SoftwareInformation softwareInformation) {
		super(doc, root);
		this.softwareInformation = softwareInformation;
        fixAddressBook();
	}

    public void fixAddressBook() {
        for (Node zone : IterableNodeList.apply(securityZoneNodeList())) {
            String zName = getString(zone, "name/text()");
            Node oab = getNode(zone, "address-book");
            Node nab = addressBookNode(zName);
            Node attach = getNode(nab, "attach");

            if (oab != null) {
                for (Node a : IterableNodeList.apply(getNodeList(oab, "address"))) {
                    nab.insertBefore(a, attach);
                }
                for (Node a : IterableNodeList.apply(getNodeList(oab, "address-set"))) {
                    nab.insertBefore(a, attach);
                }

                zone.removeChild(oab);
            }
            
            if (getNode(nab, "address") == null && getNode(nab, "address-set") == null)
            	nab.getParentNode().removeChild(nab);
        }
    }

	public static DeviceConfig createFromDocument(Document doc, SoftwareInformation softwareInformation) {
		final Element e = doc.getDocumentElement();
		final String tagName = e.getTagName();

		DeviceConfig ret = null;
		if (tagName.equals("rpc-reply")) {
			final XPath xpath = createXpathInstance();
			Element root = (Element) getNode(xpath, e, "data/configuration");
			if (root != null) {
				ret = new DeviceConfig(doc, root, softwareInformation);
			}
		}
		else if (tagName.equals("configuration")) {
			ret = new DeviceConfig(doc, e, softwareInformation);
		}
		else {
			throw new IllegalArgumentException("Unknown document.");
		}
		
		return ret;
	}

	public final String getVersion() {
		return getString("version/text()");
	}

	public final boolean isNewerThan12_1X47() {
		return Version.isNewerThan12_1X47(getVersion());
	}
	
	@Override
	public String toXMLString() throws Exception {
//		final Element e = root;
//		for(Node n : iterableNodeList(getNodeList("@*"))) {
//			e.removeChild(n);
//		}

		final Element e = root;
		e.removeAttribute("junos:changed-localtime");
		e.removeAttribute("junos:changed-seconds");

		return super.toXMLString(e);
	}
	
	public Node findPhysicalInterfaceNode(String name) {
		return getNode("interfaces/interface/name[normalize-space(text())=%s]/..", xpc(name));
	}

	public Node findLogicalInterfaceNode(String name) throws NumberFormatException {
		String[] s = name.split("\\.");
		return findLogicalInterfaceNode(s[0], Integer.parseInt(s[1]));
	}
	
	public Node findLogicalInterfaceNode(String physicalInterfaceName, int unitId) {
		Node n = findPhysicalInterfaceNode(physicalInterfaceName);
		if (n == null)
			return null;
		
		return getNode(n, "unit/name[normalize-space(text())='%d']/..", unitId);
	}
	
	public LogicalInterface findLogicalInterface(String name) throws NumberFormatException {
		String[] s = name.split("\\.");
		return findLogicalInterface(s[0], Integer.parseInt(s[1]));
	}
	
	public LogicalInterface findLogicalInterface(String physicalInterfaceName, int unitId) {
		Node n = findLogicalInterfaceNode(physicalInterfaceName, unitId);
		if (n == null)
			return null;
		
		return LogicalInterface.createFromNode(physicalInterfaceName, unitId, n, this);
	}
	
	public boolean isLogicalInterfaceExist(String physicalInterfaceName, int unitId) {
		return (findLogicalInterfaceNode(physicalInterfaceName, unitId) != null);
	}

	public List<LogicalInterface> logicalInterfaceList() {
		List<LogicalInterface> ret = new ArrayList<LogicalInterface>();
		
		NodeList ints = getNodeList("interfaces/interface");
		int intl = ints.getLength();
		for (int i = 0; i < intl; i++) {
			Node intn = ints.item(i);
			String pname = getString(intn, "name/text()");
			
			NodeList units = getNodeList(intn, "unit");
			int unitl = units.getLength();
			for (int j = 0; j < unitl; j++) {
				Node unitn = units.item(j);
				ret.add(LogicalInterface.createFromNode(pname, unitn, this));
			}
		}
		
		return ret;
	}
	
	public Node findSecurityZoneNodeByInterface(String name) {
		return getNode("security/zones/security-zone/interfaces/name[normalize-space(text())=%s]/../..", xpc(name));
	}	
	
	public Node findSecurityZoneNodeByInterface(String name, Node zones) {
		return getNode(zones, "security-zone/interfaces/name[normalize-space(text())=%s]/../..", xpc(name));
	}	
	
	public Node findSecurityZoneNode(String name) {
		return getNode("security/zones/security-zone/name[normalize-space(text())=%s]/..", xpc(name));
	}	

	public SecurityZone findSecurityZone(String name) {
		SecurityZone ret = null;
		Node n = findSecurityZoneNode(name);

		if (n != null) {
			ret = SecurityZone.createFromNode(n, this);
		}
		return ret;
	}
	
	public boolean isSecurityZoneExist(String name) {
		if ("junos-host".equals(name))
			return true;
		Node n = findSecurityZoneNode(name);
		return (n == null) ? false : true;
	}
	
	public NodeList securityZoneNodeList() {
		return getNodeList("security/zones/security-zone");
	}
	
	public List<String> securityZoneNameList() {
		ArrayList<String> ret = new ArrayList<String>();
		NodeList zones = securityZoneNodeList();
		XPathExpression namePath;
		
		try {
			namePath = xpath.compile("name/text()");
		}
		catch (XPathExpressionException e) {
			throw new RuntimeException(e);
		}
		
		if (zones != null) {
			for (int i = 0; i < zones.getLength(); i++) {
				Node zone = zones.item(i);
				String name = getString(namePath, zone);
			
				if (StringUtils.isNotEmpty(name)) {
					ret.add(name);
				}
			}
		}
		return ret;	
	}
	
	public List<SecurityZone> securityZoneList() {
		ArrayList<SecurityZone> ret = new ArrayList<SecurityZone>();

        for (Node zone : IterableNodeList.apply(securityZoneNodeList())) {
            ret.add(SecurityZone.createFromNode(zone, this));
		}
		return ret;
	}
	
	public Node zonesNode() {
		return createElementsIfNotExist("security", "zones");
	}

	public Node vlansNode() {
		return createElementsIfNotExist("vlans");
	}
	
	public Node findVlanNodeById(int id) {
		return getNode("vlans/vlan/vlan-id[normalize-space(text())='%d']/..", id);
	}	
	
	public Node findVlanNodeByName(String name) {
		return getNode("vlans/vlan[normalize-space(name/text())=%s]", xpc(name));
	}
	
	public boolean isVlanIdExist(int id) {
		Node n = findVlanNodeById(id);
		return (n != null);
	}

	public boolean isBoundVlanIdExist(int id) {
		Node n = findVlanNodeById(id);
		if (n == null)
			return false;
	
		Node i = getNode(n, "l3-interface");
		return (i != null);
	}

	public Integer findVlanIdBoundBy(String l3InterfaceName) {
			Node vlan = getNode(vlansNode(), "vlan/l3-interface[normalize-space(text())=%s]/..", xpc(l3InterfaceName));
			
			if (vlan == null)
				return null;
			
			try {
				return Integer.valueOf(getString(vlan, "vlan-id/text()"));
			}
			catch (Throwable e) {
				return null;
			}
	}

    public Node findAddressBookNode(String zoneName, boolean createIfNotExist) {
        Node sec = createElementsIfNotExist("security");
        Node n = ("global".equals(zoneName)) ? 
       		getNode(sec, "address-book[normalize-space(name/text())=%s]", xpc(zoneName)) :
        	getNode(sec, "address-book[normalize-space(attach/zone/name/text())=%s]", xpc(zoneName));

        if (n == null && createIfNotExist) {
            n = createElement("address-book");
            
            if ("global".equals(zoneName)) {
            	n.appendChild(createElement("name", zoneName));
            }
            else {
            	n.appendChild(createElement("name", "%s-book", zoneName));
            	Node a = createElement("attach");
            	Node z = createElement("zone");
            	z.appendChild(createElement("name", zoneName));
            	a.appendChild(z);
            	n.appendChild(a);
            }
            sec.appendChild(n);
        }

        return n;
    }

    public final Node findAddressNode(String zoneName, String name) {
    	return findAddressNodeByTag(zoneName, "name", name);
    }
    
    private Node findAddressNodeByTag(String zoneName, String tag, String value) {
    	Node ab = findAddressBookNode(zoneName, false);
        return (ab == null) ? null : getNode(ab, "address[normalize-space(%s/text())=%s]", tag, xpc(value));
    }
    
 	public final AddressElement findAddress(String zoneName, String name) {
 		return findAddressByTag(zoneName, "name", name);
	}  

 	public final AddressElement findAddressByDescription(String zoneName, String description) {
 		return findAddressByTag(zoneName, "description", description);
 	}
 	
 	private AddressElement findAddressByTag(String zoneName, String tag, String value) {
 		Node a = findAddressNodeByTag(zoneName, tag, value);
		return (a == null) ? null : AddressElement.createFromNode(zoneName, a, this);		
 	}
 	
   /* 
    public Node findAddressNodeByIPPrefix(String zoneName, String addr, int mask) {
        Node ab = findAddressBookNode(zoneName, false);
        if (ab == null)
        	return null;
       
        String[] a;
        
        for (Node address : XML.iterableNodeList(getNodeList(ab, "address"))) {
        	String ipprefix = getString(address, "ip-prefix/text()");
        	a = ipprefix.split("\\/", 2);
        	
        	if (Util.isIPAddressEqualTo(a[0], addr) && mask == Integer.parseInt(a[1])) {
        		return address;
        	}
        }
        
        return null;
    }
*/


	public String findAddressDescription(String zoneName, String name, boolean searchGlobal) {
		Node a = findAddressNode(zoneName, name);
		if (a == null && searchGlobal) {
			a = findAddressNode("global", name);
		}	
		return (a == null) ? null : getString(a, "description/text()");
	}
	
	public boolean isAddressExist(String zoneName, String name) {
		Node a = findAddressNode(zoneName, name);
		return (a != null);
	}

    public NodeList addressNodeList(String zoneName) {
        Node ab = findAddressBookNode(zoneName, false);
        if (ab == null)
            return EmptyNodeList.instance;
        return getNodeList(ab, "address");
    }

 	public List<String> addressNameList(String zoneName) {
		ArrayList<String> ret = new ArrayList<String>();

        for (Node an : IterableNodeList.apply(addressNodeList(zoneName))) {
            String n = getString(an, "name/text()");
            if (StringUtils.isNotBlank(n)) {
                ret.add(n);
            }
        }

		return ret;
	}

    public List<AddressElement> addressList(String zoneName) {
		ArrayList<AddressElement> ret = new ArrayList<AddressElement>();

        for (Node an : IterableNodeList.apply(addressNodeList(zoneName))) {
            AddressElement b = AddressElement.createFromNode(zoneName, an, this);
            if (b != null) {
                ret.add(b);
            }
        }

		return ret;
	}

	public List<AddressElement> addressList() {
		ArrayList<AddressElement> ret = new ArrayList<AddressElement>();

        for (Node zone : IterableNodeList.apply(securityZoneNodeList())) {
			String zName = getString(zone, "name/text()");

            for (Node book : IterableNodeList.apply(addressNodeList(zName))) {
				AddressElement b = AddressElement.createFromNode(zName, book, this);
				if (b != null) {
					ret.add(b);
				}
			}
		}
		
		return ret;
	}
	
	public final Node findAddressSetNode(String zoneName, String name) {
		return findAddressSetNodeByTag(zoneName, "name", name);
	}

	public final Node findAddressSetNodeByDescription(String zoneName, String description) {
		return findAddressSetNodeByTag(zoneName, "description", description);
	}
	
	private Node findAddressSetNodeByTag(String zoneName, String tag, String value) {
		Node ab = findAddressBookNode(zoneName, false);
		return (ab == null) ? null : getNode(ab, "address-set[normalize-space(%s/text())=%s]", tag, xpc(value));	
	}
	
	public final AddressSet findAddressSet(String zoneName, String name) {
		return findAddressSetByTag(zoneName, "name", name);
	}

	public final AddressSet findAddressSetByDescription(String zoneName, String description) {
		return findAddressSetByTag(zoneName, "description", description);
	}

	private AddressSet findAddressSetByTag(String zoneName, String tag, String value) {
		Node a = findAddressSetNodeByTag(zoneName, tag, value);
		return (a == null) ? null : AddressSet.createFromNode(zoneName, a, this);	
	}
	
	public String findAddressSetDescription(String zoneName, String name, boolean searchGlobal) {
		Node a = findAddressSetNode(zoneName, name);
		if (a == null && searchGlobal) {
			a = findAddressSetNode("global", name);
		}
		return (a == null) ? null : getString(a, "description/text()");
	}
	
	public boolean isAddressSetExist(String zoneName, String name) {
		Node a = findAddressSetNode(zoneName, name);
		return (a != null);
	}

    public NodeList addressSetNodeList(String zoneName) {
        Node ab = findAddressBookNode(zoneName, false);
        return (ab == null) ? EmptyNodeList.instance : getNodeList(ab, "address-set");
    }

	public boolean isAddressOrAddressSetExist(String zoneName, String name) {
		return (isAddressExist(zoneName, name) || isAddressSetExist(zoneName, name));
	}

	public String findAddressOrAddressSetDescription(String zoneName, String name, boolean searchGlobal) {
		String ret = findAddressDescription(zoneName, name, searchGlobal);
		if (StringUtils.isBlank(ret))
			ret = findAddressSetDescription(zoneName, name, searchGlobal);
		return ret;
	}
	
	public List<AddressSet> addressSetList() {
		ArrayList<AddressSet> ret = new ArrayList<AddressSet>();

        for (Node zone : IterableNodeList.apply(securityZoneNodeList())) {
			String zName = getString(zone, "name/text()");

            for (Node book : IterableNodeList.apply(addressSetNodeList(zName))) {
				AddressSet b = AddressSet.createFromNode(zName, book, this);
				if (b != null) {
					ret.add(b);
				}
			}
		}
		
		return ret;
	}
	
	
	public List<AddressSet> addressSetList(String zoneName) {
		ArrayList<AddressSet> ret = new ArrayList<AddressSet>();

        for (Node book : IterableNodeList.apply(addressSetNodeList(zoneName))) {
            AddressSet b = AddressSet.createFromNode(zoneName, book, this);
            if (b != null) {
                ret.add(b);
            }
        }

		return ret;
	}
	
	public List<String> addressSetNameList(String zoneName) {
		ArrayList<String> ret = new ArrayList<String>();

        for (Node book : IterableNodeList.apply(addressSetNodeList(zoneName))) {
            String n = getString(book, "name/text()");
            if (StringUtils.isNotBlank(n)) {
                ret.add(n);
            }
        }

		return ret;
	}

	public final Node addressBookNode(String zoneName) {
        return findAddressBookNode(zoneName, true);
	}

	public Node applicationsNode() {
		final String name = "applications";
		return createElementsIfNotExist(name);
	}
	
	public Node routingOptionsNode() {
		final String name = "routing-options";
		Node ret = getNode(name);
		
		if (ret == null) {
			synchronized(root) {
				ret = getNode(name);
				if (ret == null) {
					ret = createElement(name);
					root.appendChild(ret);
				}
			}
		}
		
		return ret;
	}

	public Node ribNode(String name) {
		Node ro = routingOptionsNode();
		if (StaticRoute.DEFAULT_INET_RIB_NAME.equals(name)) {
			return ro;
		}
	
		final String path = String.format("rib/name[normalize-space(text())=%s]/..", xpc(name));
		Node ret = getNode(ro, path);
		if (ret == null) {
			synchronized(root) {
				ret = getNode(ro, path);
				if (ret == null) {
					ret = createElement("rib");
					ret.appendChild(createElement("name", name));
					ro.appendChild(ret);
				}
			}
		}
		
		return ret;
	}

	public final Node inetRibNode() {
		return ribNode(StaticRoute.DEFAULT_INET_RIB_NAME);
	}
	
	public final Node inet6RibNode() {
		return ribNode(StaticRoute.DEFAULT_INET6_RIB_NAME);
	}

	public final Node findStaticRouteNode(String ribName, String name) {
		Node ribNode = ribNode(ribName);
		if (ribNode != null) {
			for (Node route : IterableNodeList.apply(getNodeList(ribNode, "static/route"))) {
				if (Util.isNetworkAddressEqualTo(name, getString(route, "name/text()"))) {
					return route;
				}
			}			
		}	
		return null;
	}
	
	public final NodeList staticRouteNodeList(String ribName) {
		List<Node> list = new ArrayList<Node>();
		
		Node ribNode = ribNode(ribName);
		if (ribNode != null) {
			for (Node route : IterableNodeList.apply(getNodeList(ribNode, "static/route"))) {
				list.add(route);
			}
		}
		return new ArrayNodeList(list);
	}

	public List<StaticRoute> staticRouteList(String ribName, String name) {
		List <StaticRoute> ret = new ArrayList<StaticRoute>();
		
		Node route = findStaticRouteNode(ribName, name);
		if (route != null) {
			ret.addAll(StaticRoute.createListFromRouteNode(ribName, route, this));
		}
		return ret;
	}
	
	public List<StaticRoute> staticRouteList(String ribName) {
		List <StaticRoute> ret = new ArrayList<StaticRoute>();

		for (Node route : IterableNodeList.apply(staticRouteNodeList(ribName))) {
			ret.addAll(StaticRoute.createListFromRouteNode(ribName, route, this));
		}
		
		return ret;
	}
	
	protected List<StaticRoute> staticRouteList(String ribName, NodeList routes) {
		List <StaticRoute> ret = new ArrayList<StaticRoute>();
		
		for (Node route : IterableNodeList.apply(routes)) {
			ret.addAll(StaticRoute.createListFromRouteNode(ribName, route, this));
		}
		
		return ret;
	}
	
	public List<StaticRoute> staticRouteList() {
		List<StaticRoute> ret = new ArrayList<StaticRoute>();
		Node ro = routingOptionsNode();
		
		ret.addAll(staticRouteList(StaticRoute.DEFAULT_INET_RIB_NAME, getNodeList(ro, "static/route")));

		for (Node rib : IterableNodeList.apply(getNodeList(ro, "rib"))) {
			String ribName = getString(rib, "name/text()");

			ret.addAll(staticRouteList(ribName, getNodeList(rib, "static/route")));
		}
		
		return ret;
	}

	public final Node policiesNode() {
		return createElementsIfNotExist("security", "policies");
	}

	public final Node natNode() {
		return createElementsIfNotExist("security", "nat");
	}
	
	public final Node natSourceNode() {
		return createElementsIfNotExist("security", "nat", "source");
	}

	public final Node natStaticNode() {
		return createElementsIfNotExist("security", "nat", "static");
	}

	public final Node natDestinationNode() {
		return createElementsIfNotExist("security", "nat", "destination");
	}

	public final Node natProxyArpNode() {
		return createElementsIfNotExist("security", "nat", "proxy-arp");
	}
	
	public final Node natProxyNdpNode() {
		return createElementsIfNotExist("security", "nat", "proxy-ndp");
	}
	
	public Node policyZoneNode(String from, String to, boolean createIfNotExist) {
		Node policies = policiesNode();
		
		for (Node policy: IterableNodeList.apply(getNodeList(policies, "policy"))) {
			if (from.equals(getString(policy, "from-zone-name/text()")) &&
				to.equals(getString(policy, "to-zone-name/text()")))	
				return policy;
		}

		if (createIfNotExist) {
			Node policy = createElement("policy");
			policy.appendChild(createElement("from-zone-name", from));
			policy.appendChild(createElement("to-zone-name", to));
			policies.appendChild(policy);
			return policy;
		}
		
		return null;
	}
	
	public final Node findPolicyNode(String from, String to, String name) {
		return findPolicyNode(policyZoneNode(from, to, false), name);
	}
	
	public Node findPolicyNode(Node policies, String name) {
		if (policies == null)
			return null;
		
		return getNode(policies, "policy/name[normalize-space(text())=%s]/..", xpc(name));
	}

	public Policy findPolicy(String from, String to, String name) {
		Node policies = policyZoneNode(from, to, false);
		if (policies == null)
			return null;

		NodeList nl = getNodeList(policies, "policy");
		int nll = nl.getLength();
		for (int i = 0; i < nll; i++) {
			Node n = nl.item(i);
			if (name.equals(getString(n, "name/text()")))
				return Policy.createFromNode(from, to, i, n, this);
		}
	
		return null;
	}
	
	public final boolean isPolicyExist(String from, String to, String name) {
		Node n = findPolicyNode(from, to, name);
		return (n != null);
	}

	public Map<String,Policy> policyMap(String from, String to) {
		Map<String, Policy> ret = new HashMap<String, Policy>();
		
		if (StringUtils.isBlank(from)) 
			throw new IllegalArgumentException("Invalid from zone name.");
		if (StringUtils.isBlank(to)) 
			throw new IllegalArgumentException("Invalid to zone name.");
		
		Node policies = policiesNode();
		NodeList nl = getNodeList(policies, "policy");
		int l = nl.getLength();
		
		for (int i = 0; i < l; i++) {
			Node n = nl.item(i);
			String fname = getString(n, "from-zone-name/text()");
			String tname = getString(n, "to-zone-name/text()");
			
			if (!from.equals(fname) || !to.equals(tname))
				continue;
	
			NodeList pnl = getNodeList(n, "policy");
			int pl = pnl.getLength();
			for (int j = 0; j < pl; j++) {
				Node policy = pnl.item(j);
				if (policy != null) {
					Policy p = Policy.createFromNode(fname, tname, j, policy, this);
					if (p != null) {
						ret.put(p.getName(), p);
					}
				}
			}
		}

		return Collections.unmodifiableMap(ret);
	}
	
	public List<Policy> policyList(String from, String to) {
		List<Policy> ret = new ArrayList<Policy>();
		Node policies = policiesNode();

		NodeList nl = getNodeList(policies, "policy");
		int l = nl.getLength();
		
		for (int i = 0; i < l; i++) {
			Node n = nl.item(i);
			String fname = getString(n, "from-zone-name/text()");
			String tname = getString(n, "to-zone-name/text()");
			
			if (StringUtils.isNotBlank(from) && !from.equals(fname))
				continue;
			if (StringUtils.isNotBlank(to) && !to.equals(tname))
				continue;
	
			NodeList pnl = getNodeList(n, "policy");
			int pl = pnl.getLength();
			for (int j = 0; j < pl; j++) {
				Node policy = pnl.item(j);
				if (policy != null) {
					Policy p = Policy.createFromNode(fname, tname, j, policy, this);
					if (p != null) {
						ret.add(p);
					}
				}
			}
		}
		
		return ret;
	}
	
	public List<Dip> dipList() {
		List<Dip>ret = new ArrayList<>(); 
		Node source = natSourceNode();
		
		for (Node pool : IterableNodeList.apply(getNodeList(source, "pool"))) {
			try {
				Dip dip = Dip.createFromNode(pool, this);
				if (dip != null)
					ret.add(dip);
			}
			catch (UnknownHostException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}
	
	public Dip findNatSourcePool(String name) throws UnknownHostException {
		Node source = natSourceNode();
		Node pool = getNode(source, "pool/name[normalize-space(text())=%s]/..", xpc(name));
		
		if (pool != null) 
			return Dip.createFromNode(pool, this);
		return null;
	}

	public final Node findNatSourceRuleSetNodeByZone(String from, String to) {
		return findNatSourceRuleSetNodeByZone(natSourceNode(), from, to);
	}
	
	public Node findNatSourceRuleSetNodeByZone(Node source, String from, String to) {
		for (Node ruleSet: IterableNodeList.apply(getNodeList(source, "rule-set"))) {
			String f = getString(ruleSet, "from/zone/text()");
			String t = getString(ruleSet, "to/zone/text()");
			
			if (StringUtils.isNotBlank(f) && f.equals(from) && StringUtils.isNotBlank(t) && t.equals(to))
				return ruleSet;
		}
		
		return null;
	}
	
	public final Node findNatStaticRuleSetNodeByZone(String fromZoneName) {
		return findNatStaticRuleSetNodeByZone(natStaticNode(), fromZoneName);
	}
	public Node findNatStaticRuleSetNodeByZone(Node parent, String fromZoneName) {
		return getNode(parent, "rule-set/from[normalize-space(name/text())=%s]/..", xpc(fromZoneName));
	}
	
	public final Node findNatStaticRuleSetNode(String name) {
		return findNatStaticRuleSetNode(natStaticNode(), name);
	}
	
	public Node findNatStaticRuleSetNode(Node parent, String name) {
		return getNode(parent, "rule-set/name[normalize-space(text())=%s]/..", xpc(name));
	}
	
	public final Node findNatStaticRuleSetNodeByInterface(String name) {
		return findNatStaticRuleSetNodeByInterface(natStaticNode(), name);
	}
	
	public Node findNatStaticRuleSetNodeByInterface(Node parent, String name) {
		return getNode(parent, "rule-set[normalize-space(from/interface/text())=%s]", xpc(name));
	}

	public final List<Mip> mipList() {
		return mipList(null);
	}

	public Node findInterfaceMipNode(String fromInterfaceName, String name) {
		Node ruleSet = findNatStaticRuleSetNodeByInterface(fromInterfaceName);
		return (ruleSet == null) ? null : getNode(ruleSet, "rule[normalize-space(name/text())=%s]", xpc(name));
	}

	public Mip findInterfaceMip(String fromInterfaceName, String name) {
		Mip ret = null;
		Node mip = findInterfaceMipNode(fromInterfaceName, name);
		try {
			ret = (mip == null) ? null : Mip.createFromNode(mip, this);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		return ret;
	}
	
	public List<Mip> mipList(String fromInterfaceName) {
		List<Mip>ret = new ArrayList<Mip>();
		Node snat = natStaticNode();
		
		for(Node ruleSet: IterableNodeList.apply(getNodeList(snat, "rule-set"))) {
			if (StringUtils.isNotBlank(fromInterfaceName)) {
				String interfaceName = getString(ruleSet, "from/interface/text()");
				if (!fromInterfaceName.equals(interfaceName))
					continue;
			}
			
			for(Node rule: IterableNodeList.apply(getNodeList(ruleSet, "rule"))) {
				try {
					Mip mip = Mip.createFromNode(rule, this);
					if (mip != null) {
						ret.add(mip);
					}
				}
				catch (UnknownHostException e) {
					e.printStackTrace();
				}
			}
		}
		
		return ret;
	}
	
	public final Node findNatDestinationRuleSetNode(String name) {
		return findNatDestinationRuleSetNode(natDestinationNode(), name);
	}
	
	public Node findNatDestinationRuleSetNode(Node parent, String name) {
		return getNode(parent, "rule-set/name[normalize-space(text())=%s]/..", xpc(name));
	}
	
	public final Node findNatDestinationRuleSetNodeByInterface(String name) {
		return findNatDestinationRuleSetNodeByInterface(natDestinationNode(), name);
	}
	
	public Node findNatDestinationRuleSetNodeByInterface(Node parent, String name) {
		return getNode(parent, "rule-set/from[normalize-space(interface/text())=%s]/..", xpc(name));
	}

	public Node findNatDestinationPoolNode(String name) {
		return findNatDestinationPoolNode(natDestinationNode(), name);
	}
	
	public Node findNatDestinationPoolNode(Node parent, String name) {
		return getNode(parent, "pool/name[normalize-space(text())=%s]/..", xpc(name));
	}
	
	public Node findInterfaceVipNode(String fromInterfaceName, String name) {
		Node ruleSet = findNatDestinationRuleSetNodeByInterface(fromInterfaceName);
		return (ruleSet == null) ? null : getNode(ruleSet, "rule[normalize-space(name/text())=%s]", xpc(name));
	}

	public Vip findInterfaceVip(String fromInterfaceName, String name) {
		Node vip = findInterfaceVipNode(fromInterfaceName, name);
		Vip ret = null;
		try {
			ret = (vip == null) ? null : Vip.createFromNode(vip, this);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	public final List<Vip> vipList() {
		return vipList(null);
	}
	
	public List<Vip> vipList(String fromInterfaceName) {
		List<Vip>ret = new ArrayList<Vip>();
		Node dnat = natDestinationNode();
		
		for(Node ruleSet: IterableNodeList.apply(getNodeList(dnat, "rule-set"))) {
			if (StringUtils.isNotBlank(fromInterfaceName)) {
				String interfaceName = getString(ruleSet, "from/interface/text()");
				if (!fromInterfaceName.equals(interfaceName))
					continue;
			}
			
			for(Node rule: IterableNodeList.apply(getNodeList(ruleSet, "rule"))) {
				try {
					Vip vip = Vip.createFromNode(rule, this);
					if (vip != null) {
						ret.add(vip);
					}
				}
				catch (UnknownHostException e) {
					e.printStackTrace();
				}
			}
		}
		
		return ret;	
	}

	public List<ProxyArp> proxyArpList(String interfaceName) {
		List<ProxyArp> ret = new ArrayList<>();
		Node n = natProxyArpNode();
		for (Node intNode: IterableNodeList.apply(getNodeList(n, "interface"))) {
			if (StringUtils.isNotBlank(interfaceName)) {
				String intName = getString(intNode, "name/text()");
				if (!interfaceName.equals(intName))
					continue;
			}

			for(Node address: IterableNodeList.apply(getNodeList(intNode, "address")))  {
				try {
					ProxyArp o = ProxyArp.createFromNode(interfaceName, address, this);
					if (o != null)
						ret.add(o);
				}
				catch (UnknownHostException e) {
					e.printStackTrace();
				}
			}
		}
		
		n = natProxyNdpNode();
		for (Node intNode: IterableNodeList.apply(getNodeList(n, "interface"))) {
			if (StringUtils.isNotBlank(interfaceName)) {
				String intName = getString(intNode, "name/text()");
				if (!interfaceName.equals(intName))
					continue;
			}

			for(Node address: IterableNodeList.apply(getNodeList(intNode, "address")))  {
				try {
					ProxyArp o = ProxyArp.createFromNode(interfaceName, address, this);
					if (o != null)
						ret.add(o);
				}
				catch (UnknownHostException e) {
					e.printStackTrace();
				}
			}
		}	
		
		return ret;
	}

	public NodeList pppoeNodeListByInterface(String name)  {
		Node phy = findPhysicalInterfaceNode("pp0");
		if (phy == null)
			return null;
		
		return getNodeList(phy, "unit[normalize-space(pppoe-options/underlying-interface/text())=%s]", xpc(name));
	}

	public Node findPPPoENode(int unitId) {
		Node pp0 = findPhysicalInterfaceNode(PPPoE.PHYSICAL_INTERFACE_NAME);
		if (pp0 != null) {
			return getNode(pp0, "unit[normalize-space(name/text())=%d]", unitId);
		}
		return null;
	}

	public PPPoE findPPPoE(int unitId) {
		Node unit = findPPPoENode(unitId);
		if (unit != null)
			return PPPoE.createFromNode(unitId, unit, this);
		return null;
	}
	
	public List<PPPoE> pppoeList() {
		List<PPPoE> ret = new ArrayList<PPPoE>();

		Node pp0 = findPhysicalInterfaceNode(PPPoE.PHYSICAL_INTERFACE_NAME);
		if (pp0 != null) {
			for (Node unit : IterableNodeList.apply(getNodeList(pp0, "unit"))) {
				PPPoE pppoe = PPPoE.createFromNode(unit, this);
				if (pppoe != null) {
					ret.add(pppoe);
				}
			}
		}
		
		return ret;
	}
	
	public String sessionLogFileName() {
		Node syslog = getNode("system/syslog");
		if (syslog == null)
			return null;
		
		Node file = getNode(syslog, "file[normalize-space(match/text())=%s]", xpc("RT_FLOW_SESSION"));
		if (file == null)
			return null;
		
		return getString(file, "name/text()");
	}
	
	public Node systemNode() {
		return createElementsIfNotExist("system");
	}
	
	public DateTime findDateTime() {
		return DateTime.createFromNode(systemNode(), this);
	}
	
	public DnsHost findDnsHost() {
		return DnsHost.createFromNode(systemNode(), this);
	}
	
	public Node algNode() {
		return createElementsIfNotExist("security", "alg");
	}
	
	public ALG findALG() {
		return ALG.createFromNode(algNode(), this);
	}

	public Node snmpNode() {
		return createElementsIfNotExist("snmp");
	}
	
	public Snmp snmp() {
		return Snmp.createFromConfig(this);
	}
	
	public List<SnmpCommunity> snmpCommunityList() {
		List<SnmpCommunity> ret = new ArrayList<>();
		Node snmp = snmpNode();
		
		for (Node community: IterableNodeList.apply(getNodeList(snmp, "community"))) {
			SnmpCommunity o = SnmpCommunity.createFromNode(community, snmp, this);
			if (o != null)
				ret.add(o);
		}
		return ret;
	}

	public Map<String, SnmpCommunity> snmpCommunityMap() {
		Map<String, SnmpCommunity> ret = new HashMap<String, SnmpCommunity>();
		
		for (SnmpCommunity c : snmpCommunityList()) {
			ret.put(c.getName(), c);
		}
		
		return ret;
	}
	
	public final Node findSnmpCommunityNode(String name) {
		return findSnmpCommunityNode(name, snmpNode());
	}
	
	public Node findSnmpCommunityNode(String name, Node snmp) {
		return getNode(snmp, "community[normalize-space(name/text())=%s]", xpc(name));
	}

	public SnmpCommunity findSnmpCommunity(String name) {
		Node snmp = snmpNode();
		Node community = findSnmpCommunityNode(name, snmp);
		if (community == null)
			return null;
		return SnmpCommunity.createFromNode(community, snmp, this);
	}

	public final Node findSnmpTrapGroupNode(String name) {
		return findSnmpTrapGroupNode(name, snmpNode());
	}
	
	public Node findSnmpTrapGroupNode(String name, Node snmp) {
		return getNode(snmp, "trap-group[normalize-space(name/text())=%s]", xpc(name));
	}
	
	public Node ikeNode() {
		return createElementsIfNotExist("security", "ike");
	}
	
	public final Node findIkeProposalNode(String name) {
		return findIkeProposalNode(ikeNode(), name);
	}
	
	public Node findIkeProposalNode(Node ike, String name) {
		return getNode(ike, "proposal[normalize-space(name/text())=%s]", xpc(name));
	}
	
	public final IkeProposal findIkeProposal(String name) {
		return findIkeProposal(ikeNode(), name);
	}
	
	public IkeProposal findIkeProposal(Node ike, String name) {
		Node n = findIkeProposalNode(ike, name);
		if (n == null)
			return null;
		return IkeProposal.createFromNode(n, this);
	}
	
	public List<IkeProposal> ikeProposalList() {
		List<IkeProposal> ret = new ArrayList<>();
	
		for (Node n : IterableNodeList.apply(getNodeList(ikeNode(), "proposal"))) {
			IkeProposal o = IkeProposal.createFromNode(n, this);
			if (o != null)
				ret.add(o);
		}
		return ret;
	}

	public Map<String, IkeProposal> ikeProposalMap() {
		Map<String, IkeProposal> ret = new HashMap<>();
		
		for (IkeProposal prop : ikeProposalList()) {
			ret.put(prop.getName(), prop);
		}
		return ret;
	}
	
	public final Node findIkePolicyNode(String name) {
		return findIkePolicyNode(ikeNode(), name);
	}
	
	public Node findIkePolicyNode(Node ike, String name) {
		return getNode(ike, "policy[normalize-space(name/text())=%s]", xpc(name));
	}
	
	public final IkePolicy findIkePolicy(String name) {
		return findIkePolicy(ikeNode(), name);
	}
	
	public IkePolicy findIkePolicy(Node ike, String name) {
		Node n = findIkePolicyNode(ike, name);
		if (n == null)
			return null;
		return IkePolicy.createFromNode(n, this);
	}
	
	public List<IkePolicy> ikePolicyList() {
		List<IkePolicy> ret = new ArrayList<>();
	
		for (Node n : IterableNodeList.apply(getNodeList(ikeNode(), "policy"))) {
			IkePolicy o = IkePolicy.createFromNode(n, this);
			if (o != null)
				ret.add(o);
		}
		return ret;
	}

	public final Node findIkeGatewayNode(String name) {
		return findIkeGatewayNode(ikeNode(), name);
	}
	
	public Node findIkeGatewayNode(Node ike, String name) {
		return getNode(ike, "gateway[normalize-space(name/text())=%s]", xpc(name));
	}
	
	public final IkeGateway findIkeGateway(String name) {
		return findIkeGateway(ikeNode(), name);
	}
	
	public IkeGateway findIkeGateway(Node ike, String name) {
		Node n = findIkeGatewayNode(ike, name);
		if (n == null)
			return null;
		return IkeGateway.createFromNode(n, this);
	}
	
	public List<IkeGateway> ikeGatewayList() {
		List<IkeGateway> ret = new ArrayList<>();
	
		for (Node n : IterableNodeList.apply(getNodeList(ikeNode(), "gateway"))) {
			IkeGateway o = IkeGateway.createFromNode(n, this);
			if (o != null)
				ret.add(o);
		}
		return ret;
	}

	public Map<String, IkeGateway> ikeGatewayMap() {
		Map<String, IkeGateway> ret = new HashMap<>();
		
		for (IkeGateway gw : ikeGatewayList()) {
			ret.put(gw.getName(), gw);
		}
		
		return ret;
	}
	
	public Node ipsecNode() {
		return createElementsIfNotExist("security", "ipsec");
	}
	
	public final Node findIPSecProposalNode(String name) {
		return findIPSecProposalNode(ipsecNode(), name);
	}
	
	public Node findIPSecProposalNode(Node ipsec, String name) {
		return getNode(ipsec, "proposal[normalize-space(name/text())=%s]", xpc(name));
	}
	
	public final IPSecProposal findIPSecProposal(String name) {
		return findIPSecProposal(ipsecNode(), name);
	}
	
	public IPSecProposal findIPSecProposal(Node ipsec, String name) {
		Node n = findIPSecProposalNode(ipsec, name);
		if (n == null)
			return null;
		return IPSecProposal.createFromNode(n, this);
	}
	
	public List<IPSecProposal> ipsecProposalList() {
		List<IPSecProposal> ret = new ArrayList<>();
	
		for (Node n : IterableNodeList.apply(getNodeList(ipsecNode(), "proposal"))) {
			IPSecProposal o = IPSecProposal.createFromNode(n, this);
			if (o != null)
				ret.add(o);
		}
		return ret;
	}
	
	public Map<String, IPSecProposal> ipsecProposalMap() {
		Map<String, IPSecProposal> ret = new HashMap<>();
		
		for (IPSecProposal prop : ipsecProposalList()) {
			ret.put(prop.getName(), prop);
		}
		return ret;
	}
		
	public final Node findIPSecPolicyNode(String name) {
		return findIPSecPolicyNode(ipsecNode(), name);
	}
	
	public Node findIPSecPolicyNode(Node ipsec, String name) {
		return getNode(ipsec, "policy[normalize-space(name/text())=%s]", xpc(name));
	}
	
	public final IPSecPolicy findIPSecPolicy(String name) {
		return findIPSecPolicy(ipsecNode(), name);
	}
	
	public IPSecPolicy findIPSecPolicy(Node ipsec, String name) {
		Node n = findIPSecPolicyNode(ipsec, name);
		if (n == null)
			return null;
		return IPSecPolicy.createFromNode(n, this);
	}
	
	public List<IPSecPolicy> ipsecPolicyList() {
		List<IPSecPolicy> ret = new ArrayList<>();
	
		for (Node n : IterableNodeList.apply(getNodeList(ipsecNode(), "policy"))) {
			IPSecPolicy o = IPSecPolicy.createFromNode(n, this);
			if (o != null)
				ret.add(o);
		}
		return ret;
	}
	
	public final Node findIPSecVpnNode(String name) {
		return findIPSecVpnNode(ipsecNode(), name);
	}
	
	public Node findIPSecVpnNode(Node ipsec, String name) {
		return getNode(ipsec, "vpn[normalize-space(name/text())=%s]", xpc(name));
	}
	
	public final IPSecVpn findIPSecVpn(String name) {
		return findIPSecVpn(ipsecNode(), name);
	}
	
	public IPSecVpn findIPSecVpn(Node ipsec, String name) {
		Node n = findIPSecVpnNode(ipsec, name);
		if (n == null)
			return null;
		return IPSecVpn.createFromNode(n, this);
	}
	
	public List<IPSecVpn> ipsecVpnList() {
		List<IPSecVpn> ret = new ArrayList<>();
	
		for (Node n : IterableNodeList.apply(getNodeList(ipsecNode(), "vpn"))) {
			IPSecVpn o = IPSecVpn.createFromNode(n, this);
			if (o != null)
				ret.add(o);
		}
		return ret;
	}
	
	public Map<String, IPSecVpn> ipsecVpnMap() {
		Map <String, IPSecVpn> ret = new HashMap<>();
		
		for (IPSecVpn vpn : ipsecVpnList()) {
			ret.put(vpn.getName(), vpn);
		}
		
		return ret;
	}

	public Node securityNode() {
		return createElementsIfNotExist("security");
	}
	
	public Node screenNode() {
		return createElementsIfNotExist("security", "screen");
	}
	
	public final Node findIDSOptionNode(String name) {
		return findIDSOptionNode(screenNode(), name);
	}
	
	public Node findIDSOptionNode(Node screenNode, String name) {
		return getNode(screenNode, "ids-option[normalize-space(name/text())=%s]", xpc(name));
	}
	
	public final IDSOption findIDSOption(String name) {
		return findIDSOption(screenNode(), name);
	}
	
	public IDSOption findIDSOption(Node screenNode, String name) {
		Node node = findIDSOptionNode(screenNode, name);
		return (node == null) ? null : IDSOption.createFromNode(node, this);
	}
	
	public Node internetOptionsNode() {
		return createElementsIfNotExist("system", "internet-options");
	}
	
	public Node loginNode() {
		return createElementsIfNotExist("system", "login");
	}

	public final List<LoginUser> loginUserList() {
		return loginUserList(loginNode());
	}
	
	public List<LoginUser> loginUserList(Node loginNode) {
		List<LoginUser> ret = new ArrayList<>();

		ret.add(LoginUser.createFromNode(findLoginUserNode(loginNode, LoginUser.ROOT_NAME), this));
		
		for (Node n: IterableNodeList.apply(getNodeList(loginNode, "user"))) {
			LoginUser u = LoginUser.createFromNode(n, this);
			if (u != null)
				ret.add(u);
		}
		
		return ret;
	}

	public final Map<String, LoginUser> loginUserMap() {
		return loginUserMap(loginNode());
	}
	
	public Map<String, LoginUser> loginUserMap(Node loginNode) {
		Map<String, LoginUser> ret = new HashMap<>();

		for (LoginUser user: loginUserList(loginNode)) {
			ret.put(user.getName(), user);
		}
		
		return ret;
	}
	
	public final Node findLoginUserNode(String name) {
		return findLoginUserNode(loginNode(), name);
	}
	
	public Node findLoginUserNode(Node loginNode, String name) {
		if (LoginUser.ROOT_NAME.equals(name))
			return this.createElementsIfNotExist("system", "root-authentication");	
		return getNode(loginNode, "user[normalize-space(name/text())=%s]", xpc(name));
	}

	public final LoginUser findLoginUser(String name) {
		return findLoginUser(loginNode(), name);
	}
	
	public LoginUser findLoginUser(Node loginNode, String name) {
		Node n = findLoginUserNode(loginNode, name);
		return (n == null) ? null : LoginUser.createFromNode(n, this);
	}
	
	public Node interfacesNode() {
		return createElementsIfNotExist("interfaces");
	}
	
	public Node syslogNode() {
		return createElementsIfNotExist("system", "syslog");
	}

	public Node findSyslogFileNode(String name) {
		return getNode(syslogNode(), "file[normalize-space(name/text())=%s]", XML.xpc(name));
	}
	
	public List<Syslog> syslogHostList() {
		return syslogHostList(syslogNode());
	}

	public List<Syslog> syslogHostList(Node syslogNode) {
		return syslogList(syslogNode, "host");
	}
	
	public List<Syslog> syslogFileList() {
		return syslogFileList(syslogNode());
	}

	public List<Syslog> syslogFileList(Node syslogNode) {
		return syslogList(syslogNode, "file");
	}
	
	public List<Syslog> syslogList() {
		return syslogList(syslogNode());
	}

	public List<Syslog> syslogList(Node syslogNode) {
		return syslogList(syslogNode, "host|file");
	}
	
	protected List<Syslog> syslogList(Node syslogNode, String path) {
		List<Syslog> ret = new ArrayList<>();
		
		for (Node n : IterableNodeList.apply(getNodeList(syslogNode, path))) {
			List<Syslog> s = Syslog.createFromNode(n, this);
			if (s != null)
				ret.addAll(s);
		}
		
		return ret;
	}
	
	public boolean isLogModeStream() {
		String mode = getString("security/log/mode/text()");
		return "stream".equals(mode);
	}
	
	public final boolean isL2ng() {
		return softwareInformation.isL2ng();
	}
	
	public final boolean isSipHidden() {
		return softwareInformation.isShipHidden();
	}
	
	public final SoftwareInformation softwareInformation() {
		return softwareInformation;
	}
}
