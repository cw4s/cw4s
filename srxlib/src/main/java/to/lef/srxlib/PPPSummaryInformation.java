package to.lef.srxlib;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Node;

import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class PPPSummaryInformation {
	public static final String PPP_PHASE_AUTH      = "Authenticate";
	public static final String PPP_PHASE_PENDING   = "Pending";
	public static final String PPP_PHASE_ESTABLISH = "Establish";
	public static final String PPP_PHASE_NETWORK   = "Network";
	public static final String PPP_PHASE_DISABLED  = "Disabled";
	
	public static class Entry {
		private String name;
		private String type;
		private String phase;
		private String flags;
		
		public Entry(String name, String type, String phase, String flags) {
			this.name = name;
			this.type = type;
			this.phase = phase;
			this.flags = flags;
		}

		public final String getName() {
			return name;
		}

		public final String getType() {
			return type;
		}

		public final String getPhase() {
			return phase;
		}

		public final String getFlags() {
			return flags;
		}
	}

	private Map<String, Entry> entryMap;
	
	public PPPSummaryInformation() {
		entryMap = new HashMap<>();
	}

	public PPPSummaryInformation(XML xml) {
		this();
		
		for(Node n : IterableNodeList.apply(xml.getNodeList("ppp-session"))) {
			String name = xml.getString(n, "session-name");
			String type = xml.getString(n, "session-type");
			String phase = xml.getString(n, "session-phase");
			String flags = xml.getString(n, "sesstion-flags");
			
			Entry e = new Entry(name, type, phase, flags);
			entryMap.put(name, e);
		}
	}
	
	public Map<String,Entry> getEntryMap() {
		return entryMap;
	}
}
