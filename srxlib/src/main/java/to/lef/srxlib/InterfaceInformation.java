package to.lef.srxlib;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.nic.LogicalInetInterface;
import to.lef.srxlib.nic.LogicalInetInterface.IPv4AddressAndMask;
import to.lef.srxlib.nic.LogicalInterface;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.table.DatabaseTable;

public class InterfaceInformation extends XML {
	Map<String, Integer> physicalInterfaceMap;
	List <InterfaceEntry> interfaceEntryList;
	Map<String, List<AddressFamilyEntry>> addressFamilyMap;
	Map<String, InterfaceEntry> interfaceEntryMap;

	int totalPhysicalInterfaceCount;
	int upPhysicalInterfaceCount;
	int downPhysicalInterfaceCount;
	int unknownPhysicalInterfaceCount;
	
	@DatabaseTable(tableName = "interface_information")
	public static class InterfaceEntry {
		@DatabaseField(id = true)
		private String name;
		@DatabaseField
		private String status;
		@DatabaseField
		private Integer phyOrder;
		@DatabaseField
		private String familyType;
		@DatabaseField
		private String currentFamilyType;
		@DatabaseField 
		private String configFamilyType;
		@DatabaseField
		private String currentZoneName;
		@DatabaseField
		private String configZoneName;
		@DatabaseField
		private Integer currentVlanId;
		@DatabaseField
		private Integer configVlanId;
		
		public InterfaceEntry() {
			// ORMLite needs a no-arg constructor
		}

		public InterfaceEntry(String name, String status, Integer phyOrder, String familyType, String currentFamilyType, String configFamilyType) {
			this.name              = name;
			this.status            = status;
			this.phyOrder          = phyOrder;
			this.familyType        = familyType;
			this.currentFamilyType = currentFamilyType;
			this.configFamilyType  = configFamilyType;
			this.currentZoneName   = "";
			this.configZoneName    = "";
			this.currentVlanId     = null;
			this.configVlanId      = null;
		}

		public final String getName() {
			return name;
		}

		public final void setName(String name) {
			this.name = name;
		}

		public final String getStatus() {
			return status;
		}

		public final void setStatus(String status) {
			this.status = status;
		}

		public final Integer getPhyOrder() {
			return phyOrder;
		}

		public final void setPhyOrder(Integer phyOrder) {
			this.phyOrder = phyOrder;
		}

		public final String getFamilyType() {
			return familyType;
		}

		public final void setFamilyType(String familyType) {
			this.familyType = familyType;
		}
		
		public final String getCurrentFamilyType() {
			return currentFamilyType;
		}

		public final void setCurrentFamilyType(String familyType) {
			this.currentFamilyType = familyType;
		}

		public final String getConfigFamilyType() {
			return configFamilyType;
		}

		public final void setConfigFamilyType(String configFamilyType) {
			this.configFamilyType = configFamilyType;
		}

		public final String getCurrentZoneName() {
			return (StringUtils.isNotBlank(currentZoneName)) ? currentZoneName : "";
		}

		public final void setCurrentZoneName(String currentZoneName) {
			this.currentZoneName = (StringUtils.isNotBlank(currentZoneName) && !"Null".equals(currentZoneName)) ? currentZoneName : "";
		}

		public final String getConfigZoneName() {
			return (StringUtils.isNotBlank(configZoneName)) ? configZoneName : "";
		}

		public final void setConfigZoneName(String configZonename) {
			this.configZoneName = (StringUtils.isNotBlank(configZonename) && !"Null".equals(configZonename)) ? configZonename : "";
		}

		public final Integer getCurrentVlanId() {
			return this.currentVlanId;
		}
		
		public final void setCurrentVlanId(Integer currentVlanId) {
			this.currentVlanId = currentVlanId;
		}
		
		public final Integer getConfigVlanId() {
			return this.configVlanId;
		}
		
		public final void setConfigVlanId(Integer configVlanId) {
			this.configVlanId = configVlanId;
		}
	}

	@DatabaseTable(tableName = "interface_address_family")
	public static class AddressFamilyEntry {
		@DatabaseField(generatedId = true)
		private Integer id;
		@DatabaseField(canBeNull = false, index = true)
		private String interfaceName;
		@DatabaseField(canBeNull = false)
		private String name;
		@DatabaseField
		private String ifaLocal;
		@DatabaseField
		private Integer configStatus;
		
		public AddressFamilyEntry() {
			// ORMLite needs a no-arg constructor
		}

		public AddressFamilyEntry(String interfaceName, String name, String ifaLocal) {
			this.interfaceName = interfaceName;
			this.name = name;
			this.ifaLocal = ifaLocal;
			this.configStatus = 0;
		}

		public final String getInterfaceName() {
			return interfaceName;
		}

		public final void setInterfaceName(String interfaceName) {
			this.interfaceName = interfaceName;
		}

		public final String getName() {
			return name;
		}

		public final void setName(String name) {
			this.name = name;
		}

		public final String getIfaLocal() {
			return ifaLocal;
		}

		public final void setIfaLocal(String ifaLocal) {
			this.ifaLocal = ifaLocal;
		}

		public final Integer getConfigStatus() {
			return configStatus;
		}
		
		public final void setConfigStatus(Integer configStatus) {
			this.configStatus = configStatus;
		}
	}
	
	public static InterfaceInformation createFromDocument(Document doc, Element root, ZonesInformation zonesInformation, Device.DatabaseHandler dbh, DeviceConfig config) throws SQLException {
		InterfaceInformation ret = null;
		
		ret = new InterfaceInformation(doc, root);
		
		dbh.dropTable(InterfaceInformation.InterfaceEntry.class, true);
		dbh.dropTable(InterfaceInformation.AddressFamilyEntry.class, true);
		dbh.createTable(InterfaceInformation.InterfaceEntry.class);
		dbh.createTable(InterfaceInformation.AddressFamilyEntry.class);

		ret.updateInterfaceEntries(dbh, zonesInformation);
		ret.updateInterfaceEntries(dbh, config);
		
		Dao<InterfaceInformation.InterfaceEntry, String> ifDao = dbh.createDao(InterfaceInformation.InterfaceEntry.class);
		Dao<InterfaceInformation.AddressFamilyEntry, Integer> afDao = dbh.createDao(InterfaceInformation.AddressFamilyEntry.class);

		{
			QueryBuilder<InterfaceEntry, String> qb = ifDao.queryBuilder();
			qb
			.orderBy("phyOrder", true)
			.orderBy("name", true);

			ret.interfaceEntryList = qb.query();
		}

		for (InterfaceEntry entry : ret.interfaceEntryList) {
			String eName = entry.getName();

			ret.interfaceEntryMap.put(eName, entry);

			QueryBuilder<AddressFamilyEntry, Integer> qb = afDao.queryBuilder();
			Where<AddressFamilyEntry, Integer> where = qb.where();

			where.eq("interfaceName", entry.getName());

			qb
			.orderBy("name", true)			
			.orderBy("ifaLocal", true);	

			List<AddressFamilyEntry> afList = qb.query();
			if (afList == null)
				afList = new ArrayList<AddressFamilyEntry>();

			ret.addressFamilyMap.put(eName, afList);
		}
		return ret;
	}
	
	private InterfaceInformation(Document doc, Element root) {
		super(doc, root);
		this.physicalInterfaceMap = new HashMap<String, Integer>();
		this.addressFamilyMap     = new HashMap<String, List<AddressFamilyEntry>>();
		this.interfaceEntryMap    = new HashMap<String, InterfaceEntry>();
	}

	private void updateInterfaceEntries(final Device.DatabaseHandler dbh, ZonesInformation zonesInformation) throws SQLException {
		Dao<InterfaceInformation.InterfaceEntry, String> ifDao = dbh.createDao(InterfaceInformation.InterfaceEntry.class);
		Dao<InterfaceInformation.AddressFamilyEntry, Integer> afDao = dbh.createDao(InterfaceInformation.AddressFamilyEntry.class);
	
		NodeList piList = getNodeList("physical-interface");
		
		for (int i = 0; i < piList.getLength(); i++) {
			Node pi = piList.item(i);
			String  phyName        = getString(pi, "name/text()");
//			if ("pp0".equals(phyName))
//				continue;
			String  phyAdminStatus = getString(pi, "admin-status/text()");
			String  phyOperStatus  = getString(pi, "oper-status/text()");
			Integer phyOrder       = i + 1;

			this.totalPhysicalInterfaceCount++;
			if ("up".equals(phyOperStatus)) {
				this.upPhysicalInterfaceCount++;
			}
			else if ("down".equals(phyOperStatus)) {
				this.downPhysicalInterfaceCount++;
			}
			else {
				this.unknownPhysicalInterfaceCount++;
			}
			
			this.physicalInterfaceMap.put(phyName, phyOrder);
	
			Pattern pattern = Pattern.compile("0x8100\\.(\\d+)");
			for (Node li : IterableNodeList.apply(getNodeList(pi, "logical-interface"))) {
				if (getNode(li, "encapsulation[normalize-space(text())=%s]", LogicalInterface.ENCAPSULATION_PPPOE) != null)
					continue;
				
				String name        = getString(li, "name/text()");
				String adminStatus = getString(li, "admin-status/text()");
				String operStatus  = getString(li, "oper-status/text()");
				String status      = "up";
		
				Node tmpNode = getNode(li, "pppoe-information/pppoe-interface");
				if (tmpNode != null) {
						if ("SessionUp".equals(getString(tmpNode, "state/text()"))) {
							status = "up";
						}
						else {
							status = "down";
						}
				}
				else {
					if (StringUtils.isNotBlank(phyAdminStatus) && !phyAdminStatus.equals("up"))
						status = phyAdminStatus;
					else if (StringUtils.isNotBlank(phyOperStatus) && !phyOperStatus.equals("up"))
						status = phyOperStatus;
					else if (StringUtils.isNotBlank(adminStatus) && !adminStatus.equals("up"))
						status = adminStatus;
					else if (StringUtils.isNotBlank(operStatus) && !operStatus.equals("up"))
						status = operStatus;
				}
				
				String familyType = null;
				for (Node af : IterableNodeList.apply(getNodeList(li, "address-family"))) {
					String afName   = getString(af, "address-family-name/text()");
					
					familyType = afName;
					if (StringUtils.isNotBlank(afName) && afName.startsWith("inet")) {
						NodeList ifaList = getNodeList(af, "interface-address/ifa-local");
						for (int l = 0; l < ifaList.getLength(); l++) {
							Node ifa = ifaList.item(l);
							String ifaLocal = getString(ifa, "text()");
						
							if (StringUtils.isNotBlank(ifaLocal) && ifaLocal.indexOf("/") == -1) {
								if ("inet".equals(afName)) {
									ifaLocal += "/32";
								}
								else {
									ifaLocal += "/128";
								}
							}
							AddressFamilyEntry afEntry = new AddressFamilyEntry(name, afName, ifaLocal);
							afDao.create(afEntry);
						}
						familyType = "inet";
					}
					else {
						AddressFamilyEntry afEntry = new AddressFamilyEntry(name, afName, null);
						afDao.create(afEntry);
					}
				}
				
				InterfaceEntry ifEntry = new InterfaceEntry(name, status, phyOrder, familyType, familyType, null);
//				ifEntry.setCurrentZoneName(zonesInformation.getZoneNameByInterface(name));
				ifEntry.setCurrentZoneName(getString(li, "logical-interface-zone-name/text()"));
				String linkAddress = getString(li, "link-address/text()");
				if (StringUtils.isNotBlank(linkAddress)) {
					Matcher matcher = pattern.matcher(linkAddress);
					if (matcher.find()) {
						ifEntry.setCurrentVlanId(Integer.valueOf(matcher.group(1)));
					}
				}
				ifDao.create(ifEntry);
			}
		}
	}
	
	private void updateInterfaceEntries(final Device.DatabaseHandler dbh, DeviceConfig config) throws SQLException {
		Dao<InterfaceInformation.InterfaceEntry, String> ifDao = dbh.createDao(InterfaceInformation.InterfaceEntry.class);
		Dao<InterfaceInformation.AddressFamilyEntry, Integer> afDao = dbh.createDao(InterfaceInformation.AddressFamilyEntry.class);
	
		List<LogicalInterface> list = config.logicalInterfaceList();
		
		for (LogicalInterface item : list) {
			if (item instanceof LogicalInetInterface) {
				LogicalInetInterface lint = (LogicalInetInterface)item;
				InterfaceEntry ifEntry = ifDao.queryForId(lint.getName());
				if (ifEntry == null) {
					Integer phyOrder = this.physicalInterfaceMap.get(lint.getName());
					ifEntry = new InterfaceEntry(lint.getName(), null, phyOrder, "inet", null, "inet");
					ifEntry.setConfigZoneName(lint.getZoneName());
					ifEntry.setConfigVlanId(lint.getVlanId());
					ifDao.create(ifEntry);
				}
				else {
					ifEntry.setFamilyType("inet");
					ifEntry.setConfigFamilyType("inet");
					ifEntry.setConfigZoneName(lint.getZoneName());
					ifEntry.setConfigVlanId(lint.getVlanId());
					ifDao.update(ifEntry);
				}

				if (lint.isInet4Static()) {
					for (IPv4AddressAndMask addr : lint.getInet4AddressAndMaskList()) {
						String address = String.format("%s/%d", addr.getAddress(), addr.getNetmask());
						QueryBuilder<AddressFamilyEntry, Integer> qb = afDao.queryBuilder();
						Where<AddressFamilyEntry, Integer> where = qb.where();

						where
						.eq("interfaceName", lint.getName())
						.and()
						.eq("ifaLocal", address);
						
//						where.and(
//								where.eq("interfaceName", lint.getName()),
//								where.eq("ifaLocal", address)
//								);
						
						AddressFamilyEntry afEntry = qb.queryForFirst();
						if (afEntry == null) {
							afEntry = new AddressFamilyEntry(lint.getName(), "inet", address);
							afEntry.setConfigStatus(2);
							afDao.create(afEntry);
						}
						else {
							afEntry.setConfigStatus(1);
							afDao.update(afEntry);
						}
					}
				}
			}
		}
	}


	public final List<InterfaceEntry> getInterfaceEntryList() {
		return Collections.unmodifiableList(interfaceEntryList);
	}

	public final InterfaceEntry getInterfaceEntry(String name) {
		return interfaceEntryMap.get(name);
	}
	
	public final List<AddressFamilyEntry> getAddressFamilyList(String interfaceName) {
		return Collections.unmodifiableList(addressFamilyMap.get(interfaceName));
	}

	public final int getTotalPhysicalInterfaceCount() {
		return totalPhysicalInterfaceCount;
	}

	public final int getUpPhysicalInterfaceCount() {
		return upPhysicalInterfaceCount;
	}

	public final int getDownPhysicalInterfaceCount() {
		return downPhysicalInterfaceCount;
	}

	public final int getUnknownPhysicalInterfaceCount() {
		return unknownPhysicalInterfaceCount;
	}
	
}
