package to.lef.srxlib;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import java.util.Collections;

public class DeviceProfile {
	private final static String JSON_FILE_NAME = "device_profile.json";
	private Map<String, JSONObject> deviceMap;
	
	private static class InstanceHolder{
    	private static final DeviceProfile instance = new DeviceProfile();
	}

	public static DeviceProfile getInstance() {
		return InstanceHolder.instance;
	}
	
	private DeviceProfile() {
		deviceMap = new HashMap<>();
		String json = getJson();
	
		if (StringUtils.isNotBlank(json)) {
			JSONObject o = new JSONObject(json);
			
			for (Iterator<String> itr = o.keys(); itr.hasNext(); ) {
				String key = itr.next();
				JSONObject od = o.getJSONObject(key);
				
				deviceMap.put(key, od);
			}
		}
	}
	
	private String getJson() {
		Path path = null;
		byte[] buf = null;
		
		try {
			path = Paths.get(System.getProperty("user.dir"), "resources", JSON_FILE_NAME);
			buf = Files.readAllBytes(path);
		}
		catch (Exception e1) {
			try {
				path = Paths.get(DeviceProfile.class.getClassLoader().getResource(JSON_FILE_NAME).toURI());
				buf = Files.readAllBytes(path);
			}
			catch (Exception e2) {
				
			}
		}
	
		if (buf != null) {
			return new String(buf, StandardCharsets.UTF_8);	
		}
		return "";
	}
	
	public final Map<String, JSONObject> getDeviceMap() {
		return Collections.unmodifiableMap(deviceMap);
	}
}
