package to.lef.srxlib;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class DateTime {
	public static final String TZ_AFRICA_ABIDJAN = "Africa/Abidjan";
	public static final String TZ_AFRICA_ACCRA = "Africa/Accra";
	public static final String TZ_AFRICA_ADDIS_ABABA = "Africa/Addis_Ababa";
	public static final String TZ_AFRICA_ALGIERS = "Africa/Algiers";
	public static final String TZ_AFRICA_ASMERA = "Africa/Asmera";
	public static final String TZ_AFRICA_BAMAKO = "Africa/Bamako";
	public static final String TZ_AFRICA_BANGUI = "Africa/Bangui";
	public static final String TZ_AFRICA_BANJUL = "Africa/Banjul";
	public static final String TZ_AFRICA_BISSAU = "Africa/Bissau";
	public static final String TZ_AFRICA_BLANTYRE = "Africa/Blantyre";
	public static final String TZ_AFRICA_BRAZZAVILLE = "Africa/Brazzaville";
	public static final String TZ_AFRICA_BUJUMBURA = "Africa/Bujumbura";
	public static final String TZ_AFRICA_CAIRO = "Africa/Cairo";
	public static final String TZ_AFRICA_CASABLANCA = "Africa/Casablanca";
	public static final String TZ_AFRICA_CEUTA = "Africa/Ceuta";
	public static final String TZ_AFRICA_CONAKRY = "Africa/Conakry";
	public static final String TZ_AFRICA_DAKAR = "Africa/Dakar";
	public static final String TZ_AFRICA_DAR_ES_SALAAM = "Africa/Dar_es_Salaam";
	public static final String TZ_AFRICA_DJIBOUTI = "Africa/Djibouti";
	public static final String TZ_AFRICA_DOUALA = "Africa/Douala";
	public static final String TZ_AFRICA_EL_AAIUN = "Africa/El_Aaiun";
	public static final String TZ_AFRICA_FREETOWN = "Africa/Freetown";
	public static final String TZ_AFRICA_GABORONE = "Africa/Gaborone";
	public static final String TZ_AFRICA_HARARE = "Africa/Harare";
	public static final String TZ_AFRICA_JOHANNESBURG = "Africa/Johannesburg";
	public static final String TZ_AFRICA_KAMPALA = "Africa/Kampala";
	public static final String TZ_AFRICA_KHARTOUM = "Africa/Khartoum";
	public static final String TZ_AFRICA_KIGALI = "Africa/Kigali";
	public static final String TZ_AFRICA_KINSHASA = "Africa/Kinshasa";
	public static final String TZ_AFRICA_LAGOS = "Africa/Lagos";
	public static final String TZ_AFRICA_LIBREVILLE = "Africa/Libreville";
	public static final String TZ_AFRICA_LOME = "Africa/Lome";
	public static final String TZ_AFRICA_LUANDA = "Africa/Luanda";
	public static final String TZ_AFRICA_LUBUMBASHI = "Africa/Lubumbashi";
	public static final String TZ_AFRICA_LUSAKA = "Africa/Lusaka";
	public static final String TZ_AFRICA_MALABO = "Africa/Malabo";
	public static final String TZ_AFRICA_MAPUTO = "Africa/Maputo";
	public static final String TZ_AFRICA_MASERU = "Africa/Maseru";
	public static final String TZ_AFRICA_MBABANE = "Africa/Mbabane";
	public static final String TZ_AFRICA_MOGADISHU = "Africa/Mogadishu";
	public static final String TZ_AFRICA_MONROVIA = "Africa/Monrovia";
	public static final String TZ_AFRICA_NAIROBI = "Africa/Nairobi";
	public static final String TZ_AFRICA_NDJAMENA = "Africa/Ndjamena";
	public static final String TZ_AFRICA_NIAMEY = "Africa/Niamey";
	public static final String TZ_AFRICA_NOUAKCHOTT = "Africa/Nouakchott";
	public static final String TZ_AFRICA_OUAGADOUGOU = "Africa/Ouagadougou";
	public static final String TZ_AFRICA_PORTO_NOVO = "Africa/Porto-Novo";
	public static final String TZ_AFRICA_SAO_TOME = "Africa/Sao_Tome";
	public static final String TZ_AFRICA_TIMBUKTU = "Africa/Timbuktu";
	public static final String TZ_AFRICA_TRIPOLI = "Africa/Tripoli";
	public static final String TZ_AFRICA_TUNIS = "Africa/Tunis";
	public static final String TZ_AFRICA_WINDHOEK = "Africa/Windhoek";
	public static final String TZ_AMERICA_ADAK = "America/Adak";
	public static final String TZ_AMERICA_ANCHORAGE = "America/Anchorage";
	public static final String TZ_AMERICA_ANGUILLA = "America/Anguilla";
	public static final String TZ_AMERICA_ANTIGUA = "America/Antigua";
	public static final String TZ_AMERICA_ARAGUAINA = "America/Araguaina";
	public static final String TZ_AMERICA_ARUBA = "America/Aruba";
	public static final String TZ_AMERICA_ASUNCION = "America/Asuncion";
	public static final String TZ_AMERICA_BAHIA = "America/Bahia";
	public static final String TZ_AMERICA_BARBADOS = "America/Barbados";
	public static final String TZ_AMERICA_BELEM = "America/Belem";
	public static final String TZ_AMERICA_BELIZE = "America/Belize";
	public static final String TZ_AMERICA_BOA_VISTA = "America/Boa_Vista";
	public static final String TZ_AMERICA_BOGOTA = "America/Bogota";
	public static final String TZ_AMERICA_BOISE = "America/Boise";
	public static final String TZ_AMERICA_BUENOS_AIRES = "America/Buenos_Aires";
	public static final String TZ_AMERICA_CAMBRIDGE_BAY = "America/Cambridge_Bay";
	public static final String TZ_AMERICA_CAMPO_GRANDE = "America/Campo_Grande";
	public static final String TZ_AMERICA_CANCUN = "America/Cancun";
	public static final String TZ_AMERICA_CARACAS = "America/Caracas";
	public static final String TZ_AMERICA_CATAMARCA = "America/Catamarca";
	public static final String TZ_AMERICA_CAYENNE = "America/Cayenne";
	public static final String TZ_AMERICA_CAYMAN = "America/Cayman";
	public static final String TZ_AMERICA_CHICAGO = "America/Chicago";
	public static final String TZ_AMERICA_CHIHUAHUA = "America/Chihuahua";
	public static final String TZ_AMERICA_CORDOBA = "America/Cordoba";
	public static final String TZ_AMERICA_COSTA_RICA = "America/Costa_Rica";
	public static final String TZ_AMERICA_CUIABA = "America/Cuiaba";
	public static final String TZ_AMERICA_CURACAO = "America/Curacao";
	public static final String TZ_AMERICA_DANMARKSHAVN = "America/Danmarkshavn";
	public static final String TZ_AMERICA_DAWSON = "America/Dawson";
	public static final String TZ_AMERICA_DAWSON_CREEK = "America/Dawson_Creek";
	public static final String TZ_AMERICA_DENVER = "America/Denver";
	public static final String TZ_AMERICA_DETROIT = "America/Detroit";
	public static final String TZ_AMERICA_DOMINICA = "America/Dominica";
	public static final String TZ_AMERICA_EDMONTON = "America/Edmonton";
	public static final String TZ_AMERICA_EIRUNEPE = "America/Eirunepe";
	public static final String TZ_AMERICA_EL_SALVADOR = "America/El_Salvador";
	public static final String TZ_AMERICA_FORTALEZA = "America/Fortaleza";
	public static final String TZ_AMERICA_GLACE_BAY = "America/Glace_Bay";
	public static final String TZ_AMERICA_GODTHAB = "America/Godthab";
	public static final String TZ_AMERICA_GOOSE_BAY = "America/Goose_Bay";
	public static final String TZ_AMERICA_GRAND_TURK = "America/Grand_Turk";
	public static final String TZ_AMERICA_GRENADA = "America/Grenada";
	public static final String TZ_AMERICA_GUADELOUPE = "America/Guadeloupe";
	public static final String TZ_AMERICA_GUATEMALA = "America/Guatemala";
	public static final String TZ_AMERICA_GUAYAQUIL = "America/Guayaquil";
	public static final String TZ_AMERICA_GUYANA = "America/Guyana";
	public static final String TZ_AMERICA_HALIFAX = "America/Halifax";
	public static final String TZ_AMERICA_HAVANA = "America/Havana";
	public static final String TZ_AMERICA_HERMOSILLO = "America/Hermosillo";
	public static final String TZ_AMERICA_INDIANA_INDIANAPOLIS = "America/Indiana/Indianapolis";
	public static final String TZ_AMERICA_INDIANA_KNOX = "America/Indiana/Knox";
	public static final String TZ_AMERICA_INDIANA_MARENGO = "America/Indiana/Marengo";
	public static final String TZ_AMERICA_INDIANA_VEVAY = "America/Indiana/Vevay";
	public static final String TZ_AMERICA_INDIANAPOLIS = "America/Indianapolis";
	public static final String TZ_AMERICA_INUVIK = "America/Inuvik";
	public static final String TZ_AMERICA_IQALUIT = "America/Iqaluit";
	public static final String TZ_AMERICA_JAMAICA = "America/Jamaica";
	public static final String TZ_AMERICA_JUJUY = "America/Jujuy";
	public static final String TZ_AMERICA_JUNEAU = "America/Juneau";
	public static final String TZ_AMERICA_KENTUCKY_LOUISVILLE = "America/Kentucky/Louisville";
	public static final String TZ_AMERICA_KENTUCKY_MONTICELLO = "America/Kentucky/Monticello";
	public static final String TZ_AMERICA_LA_PAZ = "America/La_Paz";
	public static final String TZ_AMERICA_LIMA = "America/Lima";
	public static final String TZ_AMERICA_LOS_ANGELES = "America/Los_Angeles";
	public static final String TZ_AMERICA_LOUISVILLE = "America/Louisville";
	public static final String TZ_AMERICA_MACEIO = "America/Maceio";
	public static final String TZ_AMERICA_MANAGUA = "America/Managua";
	public static final String TZ_AMERICA_MANAUS = "America/Manaus";
	public static final String TZ_AMERICA_MARTINIQUE = "America/Martinique";
	public static final String TZ_AMERICA_MAZATLAN = "America/Mazatlan";
	public static final String TZ_AMERICA_MENDOZA = "America/Mendoza";
	public static final String TZ_AMERICA_MENOMINEE = "America/Menominee";
	public static final String TZ_AMERICA_MERIDA = "America/Merida";
	public static final String TZ_AMERICA_MEXICO_CITY = "America/Mexico_City";
	public static final String TZ_AMERICA_MIQUELON = "America/Miquelon";
	public static final String TZ_AMERICA_MONTERREY = "America/Monterrey";
	public static final String TZ_AMERICA_MONTEVIDEO = "America/Montevideo";
	public static final String TZ_AMERICA_MONTREAL = "America/Montreal";
	public static final String TZ_AMERICA_MONTSERRAT = "America/Montserrat";
	public static final String TZ_AMERICA_NASSAU = "America/Nassau";
	public static final String TZ_AMERICA_NEW_YORK = "America/New_York";
	public static final String TZ_AMERICA_NIPIGON = "America/Nipigon";
	public static final String TZ_AMERICA_NOME = "America/Nome";
	public static final String TZ_AMERICA_NORONHA = "America/Noronha";
	public static final String TZ_AMERICA_NORTH_DAKOTA_CENTER = "America/North_Dakota/Center";
	public static final String TZ_AMERICA_PANAMA = "America/Panama";
	public static final String TZ_AMERICA_PANGNIRTUNG = "America/Pangnirtung";
	public static final String TZ_AMERICA_PARAMARIBO = "America/Paramaribo";
	public static final String TZ_AMERICA_PHOENIX = "America/Phoenix";
	public static final String TZ_AMERICA_PORT_AU_PRINCE = "America/Port-au-Prince";
	public static final String TZ_AMERICA_PORT_OF_SPAIN = "America/Port_of_Spain";
	public static final String TZ_AMERICA_PORTO_VELHO = "America/Porto_Velho";
	public static final String TZ_AMERICA_PUERTO_RICO = "America/Puerto_Rico";
	public static final String TZ_AMERICA_RAINY_RIVER = "America/Rainy_River";
	public static final String TZ_AMERICA_RANKIN_INLET = "America/Rankin_Inlet";
	public static final String TZ_AMERICA_RECIFE = "America/Recife";
	public static final String TZ_AMERICA_REGINA = "America/Regina";
	public static final String TZ_AMERICA_RIO_BRANCO = "America/Rio_Branco";
	public static final String TZ_AMERICA_SANTIAGO = "America/Santiago";
	public static final String TZ_AMERICA_SANTO_DOMINGO = "America/Santo_Domingo";
	public static final String TZ_AMERICA_SAO_PAULO = "America/Sao_Paulo";
	public static final String TZ_AMERICA_SCORESBYSUND = "America/Scoresbysund";
	public static final String TZ_AMERICA_SHIPROCK = "America/Shiprock";
	public static final String TZ_AMERICA_ST_JOHNS = "America/St_Johns";
	public static final String TZ_AMERICA_ST_KITTS = "America/St_Kitts";
	public static final String TZ_AMERICA_ST_LUCIA = "America/St_Lucia";
	public static final String TZ_AMERICA_ST_THOMAS = "America/St_Thomas";
	public static final String TZ_AMERICA_ST_VINCENT = "America/St_Vincent";
	public static final String TZ_AMERICA_SWIFT_CURRENT = "America/Swift_Current";
	public static final String TZ_AMERICA_TEGUCIGALPA = "America/Tegucigalpa";
	public static final String TZ_AMERICA_THULE = "America/Thule";
	public static final String TZ_AMERICA_THUNDER_BAY = "America/Thunder_Bay";
	public static final String TZ_AMERICA_TIJUANA = "America/Tijuana";
	public static final String TZ_AMERICA_TORONTO = "America/Toronto";
	public static final String TZ_AMERICA_TORTOLA = "America/Tortola";
	public static final String TZ_AMERICA_VANCOUVER = "America/Vancouver";
	public static final String TZ_AMERICA_WHITEHORSE = "America/Whitehorse";
	public static final String TZ_AMERICA_WINNIPEG = "America/Winnipeg";
	public static final String TZ_AMERICA_YAKUTAT = "America/Yakutat";
	public static final String TZ_AMERICA_YELLOWKNIFE = "America/Yellowknife";
	public static final String TZ_ANTARCTICA_CASEY = "Antarctica/Casey";
	public static final String TZ_ANTARCTICA_DAVIS = "Antarctica/Davis";
	public static final String TZ_ANTARCTICA_DUMONTDURVILLE = "Antarctica/DumontDUrville";
	public static final String TZ_ANTARCTICA_MAWSON = "Antarctica/Mawson";
	public static final String TZ_ANTARCTICA_MCMURDO = "Antarctica/McMurdo";
	public static final String TZ_ANTARCTICA_PALMER = "Antarctica/Palmer";
	public static final String TZ_ANTARCTICA_ROTHERA = "Antarctica/Rothera";
	public static final String TZ_ANTARCTICA_SOUTH_POLE = "Antarctica/South_Pole";
	public static final String TZ_ANTARCTICA_SYOWA = "Antarctica/Syowa";
	public static final String TZ_ANTARCTICA_VOSTOK = "Antarctica/Vostok";
	public static final String TZ_ARCTIC_LONGYEARBYEN = "Arctic/Longyearbyen";
	public static final String TZ_ASIA_ADEN = "Asia/Aden";
	public static final String TZ_ASIA_ALMATY = "Asia/Almaty";
	public static final String TZ_ASIA_AMMAN = "Asia/Amman";
	public static final String TZ_ASIA_ANADYR = "Asia/Anadyr";
	public static final String TZ_ASIA_AQTAU = "Asia/Aqtau";
	public static final String TZ_ASIA_AQTOBE = "Asia/Aqtobe";
	public static final String TZ_ASIA_ASHGABAT = "Asia/Ashgabat";
	public static final String TZ_ASIA_BAGHDAD = "Asia/Baghdad";
	public static final String TZ_ASIA_BAHRAIN = "Asia/Bahrain";
	public static final String TZ_ASIA_BAKU = "Asia/Baku";
	public static final String TZ_ASIA_BANGKOK = "Asia/Bangkok";
	public static final String TZ_ASIA_BEIRUT = "Asia/Beirut";
	public static final String TZ_ASIA_BISHKEK = "Asia/Bishkek";
	public static final String TZ_ASIA_BRUNEI = "Asia/Brunei";
	public static final String TZ_ASIA_CALCUTTA = "Asia/Calcutta";
	public static final String TZ_ASIA_CHOIBALSAN = "Asia/Choibalsan";
	public static final String TZ_ASIA_CHONGQING = "Asia/Chongqing";
	public static final String TZ_ASIA_COLOMBO = "Asia/Colombo";
	public static final String TZ_ASIA_DAMASCUS = "Asia/Damascus";
	public static final String TZ_ASIA_DHAKA = "Asia/Dhaka";
	public static final String TZ_ASIA_DILI = "Asia/Dili";
	public static final String TZ_ASIA_DUBAI = "Asia/Dubai";
	public static final String TZ_ASIA_DUSHANBE = "Asia/Dushanbe";
	public static final String TZ_ASIA_GAZA = "Asia/Gaza";
	public static final String TZ_ASIA_HARBIN = "Asia/Harbin";
	public static final String TZ_ASIA_HONG_KONG = "Asia/Hong_Kong";
	public static final String TZ_ASIA_HOVD = "Asia/Hovd";
	public static final String TZ_ASIA_IRKUTSK = "Asia/Irkutsk";
	public static final String TZ_ASIA_ISTANBUL = "Asia/Istanbul";
	public static final String TZ_ASIA_JAKARTA = "Asia/Jakarta";
	public static final String TZ_ASIA_JAYAPURA = "Asia/Jayapura";
	public static final String TZ_ASIA_JERUSALEM = "Asia/Jerusalem";
	public static final String TZ_ASIA_KABUL = "Asia/Kabul";
	public static final String TZ_ASIA_KAMCHATKA = "Asia/Kamchatka";
	public static final String TZ_ASIA_KARACHI = "Asia/Karachi";
	public static final String TZ_ASIA_KASHGAR = "Asia/Kashgar";
	public static final String TZ_ASIA_KATMANDU = "Asia/Katmandu";
	public static final String TZ_ASIA_KRASNOYARSK = "Asia/Krasnoyarsk";
	public static final String TZ_ASIA_KUALA_LUMPUR = "Asia/Kuala_Lumpur";
	public static final String TZ_ASIA_KUCHING = "Asia/Kuching";
	public static final String TZ_ASIA_KUWAIT = "Asia/Kuwait";
	public static final String TZ_ASIA_MACAU = "Asia/Macau";
	public static final String TZ_ASIA_MAGADAN = "Asia/Magadan";
	public static final String TZ_ASIA_MAKASSAR = "Asia/Makassar";
	public static final String TZ_ASIA_MANILA = "Asia/Manila";
	public static final String TZ_ASIA_MUSCAT = "Asia/Muscat";
	public static final String TZ_ASIA_NICOSIA = "Asia/Nicosia";
	public static final String TZ_ASIA_NOVOSIBIRSK = "Asia/Novosibirsk";
	public static final String TZ_ASIA_OMSK = "Asia/Omsk";
	public static final String TZ_ASIA_ORAL = "Asia/Oral";
	public static final String TZ_ASIA_PHNOM_PENH = "Asia/Phnom_Penh";
	public static final String TZ_ASIA_PONTIANAK = "Asia/Pontianak";
	public static final String TZ_ASIA_PYONGYANG = "Asia/Pyongyang";
	public static final String TZ_ASIA_QATAR = "Asia/Qatar";
	public static final String TZ_ASIA_QYZYLORDA = "Asia/Qyzylorda";
	public static final String TZ_ASIA_RANGOON = "Asia/Rangoon";
	public static final String TZ_ASIA_RIYADH = "Asia/Riyadh";
	public static final String TZ_ASIA_SAIGON = "Asia/Saigon";
	public static final String TZ_ASIA_SAKHALIN = "Asia/Sakhalin";
	public static final String TZ_ASIA_SAMARKAND = "Asia/Samarkand";
	public static final String TZ_ASIA_SEOUL = "Asia/Seoul";
	public static final String TZ_ASIA_SHANGHAI = "Asia/Shanghai";
	public static final String TZ_ASIA_SINGAPORE = "Asia/Singapore";
	public static final String TZ_ASIA_TAIPEI = "Asia/Taipei";
	public static final String TZ_ASIA_TASHKENT = "Asia/Tashkent";
	public static final String TZ_ASIA_TBILISI = "Asia/Tbilisi";
	public static final String TZ_ASIA_TEHRAN = "Asia/Tehran";
	public static final String TZ_ASIA_THIMPHU = "Asia/Thimphu";
	public static final String TZ_ASIA_TOKYO = "Asia/Tokyo";
	public static final String TZ_ASIA_ULAANBAATAR = "Asia/Ulaanbaatar";
	public static final String TZ_ASIA_URUMQI = "Asia/Urumqi";
	public static final String TZ_ASIA_VIENTIANE = "Asia/Vientiane";
	public static final String TZ_ASIA_VLADIVOSTOK = "Asia/Vladivostok";
	public static final String TZ_ASIA_YAKUTSK = "Asia/Yakutsk";
	public static final String TZ_ASIA_YEKATERINBURG = "Asia/Yekaterinburg";
	public static final String TZ_ASIA_YEREVAN = "Asia/Yerevan";
	public static final String TZ_ATLANTIC_AZORES = "Atlantic/Azores";
	public static final String TZ_ATLANTIC_BERMUDA = "Atlantic/Bermuda";
	public static final String TZ_ATLANTIC_CANARY = "Atlantic/Canary";
	public static final String TZ_ATLANTIC_CAPE_VERDE = "Atlantic/Cape_Verde";
	public static final String TZ_ATLANTIC_FAEROE = "Atlantic/Faeroe";
	public static final String TZ_ATLANTIC_JAN_MAYEN = "Atlantic/Jan_Mayen";
	public static final String TZ_ATLANTIC_MADEIRA = "Atlantic/Madeira";
	public static final String TZ_ATLANTIC_REYKJAVIK = "Atlantic/Reykjavik";
	public static final String TZ_ATLANTIC_SOUTH_GEORGIA = "Atlantic/South_Georgia";
	public static final String TZ_ATLANTIC_ST_HELENA = "Atlantic/St_Helena";
	public static final String TZ_ATLANTIC_STANLEY = "Atlantic/Stanley";
	public static final String TZ_AUSTRALIA_ADELAIDE = "Australia/Adelaide";
	public static final String TZ_AUSTRALIA_BRISBANE = "Australia/Brisbane";
	public static final String TZ_AUSTRALIA_BROKEN_HILL = "Australia/Broken_Hill";
	public static final String TZ_AUSTRALIA_DARWIN = "Australia/Darwin";
	public static final String TZ_AUSTRALIA_HOBART = "Australia/Hobart";
	public static final String TZ_AUSTRALIA_LINDEMAN = "Australia/Lindeman";
	public static final String TZ_AUSTRALIA_LORD_HOWE = "Australia/Lord_Howe";
	public static final String TZ_AUSTRALIA_MELBOURNE = "Australia/Melbourne";
	public static final String TZ_AUSTRALIA_PERTH = "Australia/Perth";
	public static final String TZ_AUSTRALIA_SYDNEY = "Australia/Sydney";
	public static final String TZ_CET = "CET";
	public static final String TZ_CST6CDT = "CST6CDT";
	public static final String TZ_EET = "EET";
	public static final String TZ_EST = "EST";
	public static final String TZ_EST5EDT = "EST5EDT";
	public static final String TZ_EUROPE_AMSTERDAM = "Europe/Amsterdam";
	public static final String TZ_EUROPE_ANDORRA = "Europe/Andorra";
	public static final String TZ_EUROPE_ATHENS = "Europe/Athens";
	public static final String TZ_EUROPE_BELFAST = "Europe/Belfast";
	public static final String TZ_EUROPE_BELGRADE = "Europe/Belgrade";
	public static final String TZ_EUROPE_BERLIN = "Europe/Berlin";
	public static final String TZ_EUROPE_BRATISLAVA = "Europe/Bratislava";
	public static final String TZ_EUROPE_BRUSSELS = "Europe/Brussels";
	public static final String TZ_EUROPE_BUCHAREST = "Europe/Bucharest";
	public static final String TZ_EUROPE_BUDAPEST = "Europe/Budapest";
	public static final String TZ_EUROPE_CHISINAU = "Europe/Chisinau";
	public static final String TZ_EUROPE_COPENHAGEN = "Europe/Copenhagen";
	public static final String TZ_EUROPE_DUBLIN = "Europe/Dublin";
	public static final String TZ_EUROPE_GIBRALTAR = "Europe/Gibraltar";
	public static final String TZ_EUROPE_HELSINKI = "Europe/Helsinki";
	public static final String TZ_EUROPE_ISTANBUL = "Europe/Istanbul";
	public static final String TZ_EUROPE_KALININGRAD = "Europe/Kaliningrad";
	public static final String TZ_EUROPE_KIEV = "Europe/Kiev";
	public static final String TZ_EUROPE_LISBON = "Europe/Lisbon";
	public static final String TZ_EUROPE_LJUBLJANA = "Europe/Ljubljana";
	public static final String TZ_EUROPE_LONDON = "Europe/London";
	public static final String TZ_EUROPE_LUXEMBOURG = "Europe/Luxembourg";
	public static final String TZ_EUROPE_MADRID = "Europe/Madrid";
	public static final String TZ_EUROPE_MALTA = "Europe/Malta";
	public static final String TZ_EUROPE_MINSK = "Europe/Minsk";
	public static final String TZ_EUROPE_MONACO = "Europe/Monaco";
	public static final String TZ_EUROPE_MOSCOW = "Europe/Moscow";
	public static final String TZ_EUROPE_NICOSIA = "Europe/Nicosia";
	public static final String TZ_EUROPE_OSLO = "Europe/Oslo";
	public static final String TZ_EUROPE_PARIS = "Europe/Paris";
	public static final String TZ_EUROPE_PRAGUE = "Europe/Prague";
	public static final String TZ_EUROPE_RIGA = "Europe/Riga";
	public static final String TZ_EUROPE_ROME = "Europe/Rome";
	public static final String TZ_EUROPE_SAMARA = "Europe/Samara";
	public static final String TZ_EUROPE_SAN_MARINO = "Europe/San_Marino";
	public static final String TZ_EUROPE_SARAJEVO = "Europe/Sarajevo";
	public static final String TZ_EUROPE_SIMFEROPOL = "Europe/Simferopol";
	public static final String TZ_EUROPE_SKOPJE = "Europe/Skopje";
	public static final String TZ_EUROPE_SOFIA = "Europe/Sofia";
	public static final String TZ_EUROPE_STOCKHOLM = "Europe/Stockholm";
	public static final String TZ_EUROPE_TALLINN = "Europe/Tallinn";
	public static final String TZ_EUROPE_TIRANE = "Europe/Tirane";
	public static final String TZ_EUROPE_UZHGOROD = "Europe/Uzhgorod";
	public static final String TZ_EUROPE_VADUZ = "Europe/Vaduz";
	public static final String TZ_EUROPE_VATICAN = "Europe/Vatican";
	public static final String TZ_EUROPE_VIENNA = "Europe/Vienna";
	public static final String TZ_EUROPE_VILNIUS = "Europe/Vilnius";
	public static final String TZ_EUROPE_WARSAW = "Europe/Warsaw";
	public static final String TZ_EUROPE_ZAGREB = "Europe/Zagreb";
	public static final String TZ_EUROPE_ZAPOROZHYE = "Europe/Zaporozhye";
	public static final String TZ_EUROPE_ZURICH = "Europe/Zurich";
	public static final String TZ_GMT = "GMT";
	public static final String TZ_GMT_PLUS_1 = "GMT+1";
	public static final String TZ_GMT_PLUS_10 = "GMT+10";
	public static final String TZ_GMT_PLUS_11 = "GMT+11";
	public static final String TZ_GMT_PLUS_12 = "GMT+12";
	public static final String TZ_GMT_PLUS_2 = "GMT+2";
	public static final String TZ_GMT_PLUS_3 = "GMT+3";
	public static final String TZ_GMT_PLUS_4 = "GMT+4";
	public static final String TZ_GMT_PLUS_5 = "GMT+5";
	public static final String TZ_GMT_PLUS_6 = "GMT+6";
	public static final String TZ_GMT_PLUS_7 = "GMT+7";
	public static final String TZ_GMT_PLUS_8 = "GMT+8";
	public static final String TZ_GMT_PLUS_9 = "GMT+9";
	public static final String TZ_GMT_MINUS_1 = "GMT-1";
	public static final String TZ_GMT_MINUS_10 = "GMT-10";
	public static final String TZ_GMT_MINUS_11 = "GMT-11";
	public static final String TZ_GMT_MINUS_12 = "GMT-12";
	public static final String TZ_GMT_MINUS_13 = "GMT-13";
	public static final String TZ_GMT_MINUS_14 = "GMT-14";
	public static final String TZ_GMT_MINUS_2 = "GMT-2";
	public static final String TZ_GMT_MINUS_3 = "GMT-3";
	public static final String TZ_GMT_MINUS_4 = "GMT-4";
	public static final String TZ_GMT_MINUS_5 = "GMT-5";
	public static final String TZ_GMT_MINUS_6 = "GMT-6";
	public static final String TZ_GMT_MINUS_7 = "GMT-7";
	public static final String TZ_GMT_MINUS_8 = "GMT-8";
	public static final String TZ_GMT_MINUS_9 = "GMT-9";
	public static final String TZ_GREENWICH = "Greenwich";
	public static final String TZ_HST = "HST";
	public static final String TZ_INDIAN_ANTANANARIVO = "Indian/Antananarivo";
	public static final String TZ_INDIAN_CHAGOS = "Indian/Chagos";
	public static final String TZ_INDIAN_CHRISTMAS = "Indian/Christmas";
	public static final String TZ_INDIAN_COCOS = "Indian/Cocos";
	public static final String TZ_INDIAN_COMORO = "Indian/Comoro";
	public static final String TZ_INDIAN_KERGUELEN = "Indian/Kerguelen";
	public static final String TZ_INDIAN_MAHE = "Indian/Mahe";
	public static final String TZ_INDIAN_MALDIVES = "Indian/Maldives";
	public static final String TZ_INDIAN_MAURITIUS = "Indian/Mauritius";
	public static final String TZ_INDIAN_MAYOTTE = "Indian/Mayotte";
	public static final String TZ_INDIAN_REUNION = "Indian/Reunion";
	public static final String TZ_MET = "MET";
	public static final String TZ_MST = "MST";
	public static final String TZ_MST7MDT = "MST7MDT";
	public static final String TZ_PST8PDT = "PST8PDT";
	public static final String TZ_PACIFIC_APIA = "Pacific/Apia";
	public static final String TZ_PACIFIC_AUCKLAND = "Pacific/Auckland";
	public static final String TZ_PACIFIC_CHATHAM = "Pacific/Chatham";
	public static final String TZ_PACIFIC_EASTER = "Pacific/Easter";
	public static final String TZ_PACIFIC_EFATE = "Pacific/Efate";
	public static final String TZ_PACIFIC_ENDERBURY = "Pacific/Enderbury";
	public static final String TZ_PACIFIC_FAKAOFO = "Pacific/Fakaofo";
	public static final String TZ_PACIFIC_FIJI = "Pacific/Fiji";
	public static final String TZ_PACIFIC_FUNAFUTI = "Pacific/Funafuti";
	public static final String TZ_PACIFIC_GALAPAGOS = "Pacific/Galapagos";
	public static final String TZ_PACIFIC_GAMBIER = "Pacific/Gambier";
	public static final String TZ_PACIFIC_GUADALCANAL = "Pacific/Guadalcanal";
	public static final String TZ_PACIFIC_GUAM = "Pacific/Guam";
	public static final String TZ_PACIFIC_HONOLULU = "Pacific/Honolulu";
	public static final String TZ_PACIFIC_JOHNSTON = "Pacific/Johnston";
	public static final String TZ_PACIFIC_KIRITIMATI = "Pacific/Kiritimati";
	public static final String TZ_PACIFIC_KOSRAE = "Pacific/Kosrae";
	public static final String TZ_PACIFIC_KWAJALEIN = "Pacific/Kwajalein";
	public static final String TZ_PACIFIC_MAJURO = "Pacific/Majuro";
	public static final String TZ_PACIFIC_MARQUESAS = "Pacific/Marquesas";
	public static final String TZ_PACIFIC_MIDWAY = "Pacific/Midway";
	public static final String TZ_PACIFIC_NAURU = "Pacific/Nauru";
	public static final String TZ_PACIFIC_NIUE = "Pacific/Niue";
	public static final String TZ_PACIFIC_NORFOLK = "Pacific/Norfolk";
	public static final String TZ_PACIFIC_NOUMEA = "Pacific/Noumea";
	public static final String TZ_PACIFIC_PAGO_PAGO = "Pacific/Pago_Pago";
	public static final String TZ_PACIFIC_PALAU = "Pacific/Palau";
	public static final String TZ_PACIFIC_PITCAIRN = "Pacific/Pitcairn";
	public static final String TZ_PACIFIC_PONAPE = "Pacific/Ponape";
	public static final String TZ_PACIFIC_PORT_MORESBY = "Pacific/Port_Moresby";
	public static final String TZ_PACIFIC_RAROTONGA = "Pacific/Rarotonga";
	public static final String TZ_PACIFIC_SAIPAN = "Pacific/Saipan";
	public static final String TZ_PACIFIC_TAHITI = "Pacific/Tahiti";
	public static final String TZ_PACIFIC_TARAWA = "Pacific/Tarawa";
	public static final String TZ_PACIFIC_TONGATAPU = "Pacific/Tongatapu";
	public static final String TZ_PACIFIC_TRUK = "Pacific/Truk";
	public static final String TZ_PACIFIC_WAKE = "Pacific/Wake";
	public static final String TZ_PACIFIC_WALLIS = "Pacific/Wallis";
	public static final String TZ_PACIFIC_YAP = "Pacific/Yap";
	public static final String TZ_UCT = "UCT";
	public static final String TZ_UTC = "UTC";
	public static final String TZ_UNIVERSAL = "Universal";
	public static final String TZ_WET = "WET";
	public static final String TZ_ZULU = "Zulu";

	public static List<String> timeZoneList() {
		List<String> ret = new ArrayList<String>();
	
		ret.add(TZ_AFRICA_ABIDJAN);
		ret.add(TZ_AFRICA_ACCRA);
		ret.add(TZ_AFRICA_ADDIS_ABABA);
		ret.add(TZ_AFRICA_ALGIERS);
		ret.add(TZ_AFRICA_ASMERA);
		ret.add(TZ_AFRICA_BAMAKO);
		ret.add(TZ_AFRICA_BANGUI);
		ret.add(TZ_AFRICA_BANJUL);
		ret.add(TZ_AFRICA_BISSAU);
		ret.add(TZ_AFRICA_BLANTYRE);
		ret.add(TZ_AFRICA_BRAZZAVILLE);
		ret.add(TZ_AFRICA_BUJUMBURA);
		ret.add(TZ_AFRICA_CAIRO);
		ret.add(TZ_AFRICA_CASABLANCA);
		ret.add(TZ_AFRICA_CEUTA);
		ret.add(TZ_AFRICA_CONAKRY);
		ret.add(TZ_AFRICA_DAKAR);
		ret.add(TZ_AFRICA_DAR_ES_SALAAM);
		ret.add(TZ_AFRICA_DJIBOUTI);
		ret.add(TZ_AFRICA_DOUALA);
		ret.add(TZ_AFRICA_EL_AAIUN);
		ret.add(TZ_AFRICA_FREETOWN);
		ret.add(TZ_AFRICA_GABORONE);
		ret.add(TZ_AFRICA_HARARE);
		ret.add(TZ_AFRICA_JOHANNESBURG);
		ret.add(TZ_AFRICA_KAMPALA);
		ret.add(TZ_AFRICA_KHARTOUM);
		ret.add(TZ_AFRICA_KIGALI);
		ret.add(TZ_AFRICA_KINSHASA);
		ret.add(TZ_AFRICA_LAGOS);
		ret.add(TZ_AFRICA_LIBREVILLE);
		ret.add(TZ_AFRICA_LOME);
		ret.add(TZ_AFRICA_LUANDA);
		ret.add(TZ_AFRICA_LUBUMBASHI);
		ret.add(TZ_AFRICA_LUSAKA);
		ret.add(TZ_AFRICA_MALABO);
		ret.add(TZ_AFRICA_MAPUTO);
		ret.add(TZ_AFRICA_MASERU);
		ret.add(TZ_AFRICA_MBABANE);
		ret.add(TZ_AFRICA_MOGADISHU);
		ret.add(TZ_AFRICA_MONROVIA);
		ret.add(TZ_AFRICA_NAIROBI);
		ret.add(TZ_AFRICA_NDJAMENA);
		ret.add(TZ_AFRICA_NIAMEY);
		ret.add(TZ_AFRICA_NOUAKCHOTT);
		ret.add(TZ_AFRICA_OUAGADOUGOU);
		ret.add(TZ_AFRICA_PORTO_NOVO);
		ret.add(TZ_AFRICA_SAO_TOME);
		ret.add(TZ_AFRICA_TIMBUKTU);
		ret.add(TZ_AFRICA_TRIPOLI);
		ret.add(TZ_AFRICA_TUNIS);
		ret.add(TZ_AFRICA_WINDHOEK);
		ret.add(TZ_AMERICA_ADAK);
		ret.add(TZ_AMERICA_ANCHORAGE);
		ret.add(TZ_AMERICA_ANGUILLA);
		ret.add(TZ_AMERICA_ANTIGUA);
		ret.add(TZ_AMERICA_ARAGUAINA);
		ret.add(TZ_AMERICA_ARUBA);
		ret.add(TZ_AMERICA_ASUNCION);
		ret.add(TZ_AMERICA_BAHIA);
		ret.add(TZ_AMERICA_BARBADOS);
		ret.add(TZ_AMERICA_BELEM);
		ret.add(TZ_AMERICA_BELIZE);
		ret.add(TZ_AMERICA_BOA_VISTA);
		ret.add(TZ_AMERICA_BOGOTA);
		ret.add(TZ_AMERICA_BOISE);
		ret.add(TZ_AMERICA_BUENOS_AIRES);
		ret.add(TZ_AMERICA_CAMBRIDGE_BAY);
		ret.add(TZ_AMERICA_CAMPO_GRANDE);
		ret.add(TZ_AMERICA_CANCUN);
		ret.add(TZ_AMERICA_CARACAS);
		ret.add(TZ_AMERICA_CATAMARCA);
		ret.add(TZ_AMERICA_CAYENNE);
		ret.add(TZ_AMERICA_CAYMAN);
		ret.add(TZ_AMERICA_CHICAGO);
		ret.add(TZ_AMERICA_CHIHUAHUA);
		ret.add(TZ_AMERICA_CORDOBA);
		ret.add(TZ_AMERICA_COSTA_RICA);
		ret.add(TZ_AMERICA_CUIABA);
		ret.add(TZ_AMERICA_CURACAO);
		ret.add(TZ_AMERICA_DANMARKSHAVN);
		ret.add(TZ_AMERICA_DAWSON);
		ret.add(TZ_AMERICA_DAWSON_CREEK);
		ret.add(TZ_AMERICA_DENVER);
		ret.add(TZ_AMERICA_DETROIT);
		ret.add(TZ_AMERICA_DOMINICA);
		ret.add(TZ_AMERICA_EDMONTON);
		ret.add(TZ_AMERICA_EIRUNEPE);
		ret.add(TZ_AMERICA_EL_SALVADOR);
		ret.add(TZ_AMERICA_FORTALEZA);
		ret.add(TZ_AMERICA_GLACE_BAY);
		ret.add(TZ_AMERICA_GODTHAB);
		ret.add(TZ_AMERICA_GOOSE_BAY);
		ret.add(TZ_AMERICA_GRAND_TURK);
		ret.add(TZ_AMERICA_GRENADA);
		ret.add(TZ_AMERICA_GUADELOUPE);
		ret.add(TZ_AMERICA_GUATEMALA);
		ret.add(TZ_AMERICA_GUAYAQUIL);
		ret.add(TZ_AMERICA_GUYANA);
		ret.add(TZ_AMERICA_HALIFAX);
		ret.add(TZ_AMERICA_HAVANA);
		ret.add(TZ_AMERICA_HERMOSILLO);
		ret.add(TZ_AMERICA_INDIANA_INDIANAPOLIS);
		ret.add(TZ_AMERICA_INDIANA_KNOX);
		ret.add(TZ_AMERICA_INDIANA_MARENGO);
		ret.add(TZ_AMERICA_INDIANA_VEVAY);
		ret.add(TZ_AMERICA_INDIANAPOLIS);
		ret.add(TZ_AMERICA_INUVIK);
		ret.add(TZ_AMERICA_IQALUIT);
		ret.add(TZ_AMERICA_JAMAICA);
		ret.add(TZ_AMERICA_JUJUY);
		ret.add(TZ_AMERICA_JUNEAU);
		ret.add(TZ_AMERICA_KENTUCKY_LOUISVILLE);
		ret.add(TZ_AMERICA_KENTUCKY_MONTICELLO);
		ret.add(TZ_AMERICA_LA_PAZ);
		ret.add(TZ_AMERICA_LIMA);
		ret.add(TZ_AMERICA_LOS_ANGELES);
		ret.add(TZ_AMERICA_LOUISVILLE);
		ret.add(TZ_AMERICA_MACEIO);
		ret.add(TZ_AMERICA_MANAGUA);
		ret.add(TZ_AMERICA_MANAUS);
		ret.add(TZ_AMERICA_MARTINIQUE);
		ret.add(TZ_AMERICA_MAZATLAN);
		ret.add(TZ_AMERICA_MENDOZA);
		ret.add(TZ_AMERICA_MENOMINEE);
		ret.add(TZ_AMERICA_MERIDA);
		ret.add(TZ_AMERICA_MEXICO_CITY);
		ret.add(TZ_AMERICA_MIQUELON);
		ret.add(TZ_AMERICA_MONTERREY);
		ret.add(TZ_AMERICA_MONTEVIDEO);
		ret.add(TZ_AMERICA_MONTREAL);
		ret.add(TZ_AMERICA_MONTSERRAT);
		ret.add(TZ_AMERICA_NASSAU);
		ret.add(TZ_AMERICA_NEW_YORK);
		ret.add(TZ_AMERICA_NIPIGON);
		ret.add(TZ_AMERICA_NOME);
		ret.add(TZ_AMERICA_NORONHA);
		ret.add(TZ_AMERICA_NORTH_DAKOTA_CENTER);
		ret.add(TZ_AMERICA_PANAMA);
		ret.add(TZ_AMERICA_PANGNIRTUNG);
		ret.add(TZ_AMERICA_PARAMARIBO);
		ret.add(TZ_AMERICA_PHOENIX);
		ret.add(TZ_AMERICA_PORT_AU_PRINCE);
		ret.add(TZ_AMERICA_PORT_OF_SPAIN);
		ret.add(TZ_AMERICA_PORTO_VELHO);
		ret.add(TZ_AMERICA_PUERTO_RICO);
		ret.add(TZ_AMERICA_RAINY_RIVER);
		ret.add(TZ_AMERICA_RANKIN_INLET);
		ret.add(TZ_AMERICA_RECIFE);
		ret.add(TZ_AMERICA_REGINA);
		ret.add(TZ_AMERICA_RIO_BRANCO);
		ret.add(TZ_AMERICA_SANTIAGO);
		ret.add(TZ_AMERICA_SANTO_DOMINGO);
		ret.add(TZ_AMERICA_SAO_PAULO);
		ret.add(TZ_AMERICA_SCORESBYSUND);
		ret.add(TZ_AMERICA_SHIPROCK);
		ret.add(TZ_AMERICA_ST_JOHNS);
		ret.add(TZ_AMERICA_ST_KITTS);
		ret.add(TZ_AMERICA_ST_LUCIA);
		ret.add(TZ_AMERICA_ST_THOMAS);
		ret.add(TZ_AMERICA_ST_VINCENT);
		ret.add(TZ_AMERICA_SWIFT_CURRENT);
		ret.add(TZ_AMERICA_TEGUCIGALPA);
		ret.add(TZ_AMERICA_THULE);
		ret.add(TZ_AMERICA_THUNDER_BAY);
		ret.add(TZ_AMERICA_TIJUANA);
		ret.add(TZ_AMERICA_TORONTO);
		ret.add(TZ_AMERICA_TORTOLA);
		ret.add(TZ_AMERICA_VANCOUVER);
		ret.add(TZ_AMERICA_WHITEHORSE);
		ret.add(TZ_AMERICA_WINNIPEG);
		ret.add(TZ_AMERICA_YAKUTAT);
		ret.add(TZ_AMERICA_YELLOWKNIFE);
		ret.add(TZ_ANTARCTICA_CASEY);
		ret.add(TZ_ANTARCTICA_DAVIS);
		ret.add(TZ_ANTARCTICA_DUMONTDURVILLE);
		ret.add(TZ_ANTARCTICA_MAWSON);
		ret.add(TZ_ANTARCTICA_MCMURDO);
		ret.add(TZ_ANTARCTICA_PALMER);
		ret.add(TZ_ANTARCTICA_ROTHERA);
		ret.add(TZ_ANTARCTICA_SOUTH_POLE);
		ret.add(TZ_ANTARCTICA_SYOWA);
		ret.add(TZ_ANTARCTICA_VOSTOK);
		ret.add(TZ_ARCTIC_LONGYEARBYEN);
		ret.add(TZ_ASIA_ADEN);
		ret.add(TZ_ASIA_ALMATY);
		ret.add(TZ_ASIA_AMMAN);
		ret.add(TZ_ASIA_ANADYR);
		ret.add(TZ_ASIA_AQTAU);
		ret.add(TZ_ASIA_AQTOBE);
		ret.add(TZ_ASIA_ASHGABAT);
		ret.add(TZ_ASIA_BAGHDAD);
		ret.add(TZ_ASIA_BAHRAIN);
		ret.add(TZ_ASIA_BAKU);
		ret.add(TZ_ASIA_BANGKOK);
		ret.add(TZ_ASIA_BEIRUT);
		ret.add(TZ_ASIA_BISHKEK);
		ret.add(TZ_ASIA_BRUNEI);
		ret.add(TZ_ASIA_CALCUTTA);
		ret.add(TZ_ASIA_CHOIBALSAN);
		ret.add(TZ_ASIA_CHONGQING);
		ret.add(TZ_ASIA_COLOMBO);
		ret.add(TZ_ASIA_DAMASCUS);
		ret.add(TZ_ASIA_DHAKA);
		ret.add(TZ_ASIA_DILI);
		ret.add(TZ_ASIA_DUBAI);
		ret.add(TZ_ASIA_DUSHANBE);
		ret.add(TZ_ASIA_GAZA);
		ret.add(TZ_ASIA_HARBIN);
		ret.add(TZ_ASIA_HONG_KONG);
		ret.add(TZ_ASIA_HOVD);
		ret.add(TZ_ASIA_IRKUTSK);
		ret.add(TZ_ASIA_ISTANBUL);
		ret.add(TZ_ASIA_JAKARTA);
		ret.add(TZ_ASIA_JAYAPURA);
		ret.add(TZ_ASIA_JERUSALEM);
		ret.add(TZ_ASIA_KABUL);
		ret.add(TZ_ASIA_KAMCHATKA);
		ret.add(TZ_ASIA_KARACHI);
		ret.add(TZ_ASIA_KASHGAR);
		ret.add(TZ_ASIA_KATMANDU);
		ret.add(TZ_ASIA_KRASNOYARSK);
		ret.add(TZ_ASIA_KUALA_LUMPUR);
		ret.add(TZ_ASIA_KUCHING);
		ret.add(TZ_ASIA_KUWAIT);
		ret.add(TZ_ASIA_MACAU);
		ret.add(TZ_ASIA_MAGADAN);
		ret.add(TZ_ASIA_MAKASSAR);
		ret.add(TZ_ASIA_MANILA);
		ret.add(TZ_ASIA_MUSCAT);
		ret.add(TZ_ASIA_NICOSIA);
		ret.add(TZ_ASIA_NOVOSIBIRSK);
		ret.add(TZ_ASIA_OMSK);
		ret.add(TZ_ASIA_ORAL);
		ret.add(TZ_ASIA_PHNOM_PENH);
		ret.add(TZ_ASIA_PONTIANAK);
		ret.add(TZ_ASIA_PYONGYANG);
		ret.add(TZ_ASIA_QATAR);
		ret.add(TZ_ASIA_QYZYLORDA);
		ret.add(TZ_ASIA_RANGOON);
		ret.add(TZ_ASIA_RIYADH);
		ret.add(TZ_ASIA_SAIGON);
		ret.add(TZ_ASIA_SAKHALIN);
		ret.add(TZ_ASIA_SAMARKAND);
		ret.add(TZ_ASIA_SEOUL);
		ret.add(TZ_ASIA_SHANGHAI);
		ret.add(TZ_ASIA_SINGAPORE);
		ret.add(TZ_ASIA_TAIPEI);
		ret.add(TZ_ASIA_TASHKENT);
		ret.add(TZ_ASIA_TBILISI);
		ret.add(TZ_ASIA_TEHRAN);
		ret.add(TZ_ASIA_THIMPHU);
		ret.add(TZ_ASIA_TOKYO);
		ret.add(TZ_ASIA_ULAANBAATAR);
		ret.add(TZ_ASIA_URUMQI);
		ret.add(TZ_ASIA_VIENTIANE);
		ret.add(TZ_ASIA_VLADIVOSTOK);
		ret.add(TZ_ASIA_YAKUTSK);
		ret.add(TZ_ASIA_YEKATERINBURG);
		ret.add(TZ_ASIA_YEREVAN);
		ret.add(TZ_ATLANTIC_AZORES);
		ret.add(TZ_ATLANTIC_BERMUDA);
		ret.add(TZ_ATLANTIC_CANARY);
		ret.add(TZ_ATLANTIC_CAPE_VERDE);
		ret.add(TZ_ATLANTIC_FAEROE);
		ret.add(TZ_ATLANTIC_JAN_MAYEN);
		ret.add(TZ_ATLANTIC_MADEIRA);
		ret.add(TZ_ATLANTIC_REYKJAVIK);
		ret.add(TZ_ATLANTIC_SOUTH_GEORGIA);
		ret.add(TZ_ATLANTIC_ST_HELENA);
		ret.add(TZ_ATLANTIC_STANLEY);
		ret.add(TZ_AUSTRALIA_ADELAIDE);
		ret.add(TZ_AUSTRALIA_BRISBANE);
		ret.add(TZ_AUSTRALIA_BROKEN_HILL);
		ret.add(TZ_AUSTRALIA_DARWIN);
		ret.add(TZ_AUSTRALIA_HOBART);
		ret.add(TZ_AUSTRALIA_LINDEMAN);
		ret.add(TZ_AUSTRALIA_LORD_HOWE);
		ret.add(TZ_AUSTRALIA_MELBOURNE);
		ret.add(TZ_AUSTRALIA_PERTH);
		ret.add(TZ_AUSTRALIA_SYDNEY);
		ret.add(TZ_CET);
		ret.add(TZ_CST6CDT);
		ret.add(TZ_EET);
		ret.add(TZ_EST);
		ret.add(TZ_EST5EDT);
		ret.add(TZ_EUROPE_AMSTERDAM);
		ret.add(TZ_EUROPE_ANDORRA);
		ret.add(TZ_EUROPE_ATHENS);
		ret.add(TZ_EUROPE_BELFAST);
		ret.add(TZ_EUROPE_BELGRADE);
		ret.add(TZ_EUROPE_BERLIN);
		ret.add(TZ_EUROPE_BRATISLAVA);
		ret.add(TZ_EUROPE_BRUSSELS);
		ret.add(TZ_EUROPE_BUCHAREST);
		ret.add(TZ_EUROPE_BUDAPEST);
		ret.add(TZ_EUROPE_CHISINAU);
		ret.add(TZ_EUROPE_COPENHAGEN);
		ret.add(TZ_EUROPE_DUBLIN);
		ret.add(TZ_EUROPE_GIBRALTAR);
		ret.add(TZ_EUROPE_HELSINKI);
		ret.add(TZ_EUROPE_ISTANBUL);
		ret.add(TZ_EUROPE_KALININGRAD);
		ret.add(TZ_EUROPE_KIEV);
		ret.add(TZ_EUROPE_LISBON);
		ret.add(TZ_EUROPE_LJUBLJANA);
		ret.add(TZ_EUROPE_LONDON);
		ret.add(TZ_EUROPE_LUXEMBOURG);
		ret.add(TZ_EUROPE_MADRID);
		ret.add(TZ_EUROPE_MALTA);
		ret.add(TZ_EUROPE_MINSK);
		ret.add(TZ_EUROPE_MONACO);
		ret.add(TZ_EUROPE_MOSCOW);
		ret.add(TZ_EUROPE_NICOSIA);
		ret.add(TZ_EUROPE_OSLO);
		ret.add(TZ_EUROPE_PARIS);
		ret.add(TZ_EUROPE_PRAGUE);
		ret.add(TZ_EUROPE_RIGA);
		ret.add(TZ_EUROPE_ROME);
		ret.add(TZ_EUROPE_SAMARA);
		ret.add(TZ_EUROPE_SAN_MARINO);
		ret.add(TZ_EUROPE_SARAJEVO);
		ret.add(TZ_EUROPE_SIMFEROPOL);
		ret.add(TZ_EUROPE_SKOPJE);
		ret.add(TZ_EUROPE_SOFIA);
		ret.add(TZ_EUROPE_STOCKHOLM);
		ret.add(TZ_EUROPE_TALLINN);
		ret.add(TZ_EUROPE_TIRANE);
		ret.add(TZ_EUROPE_UZHGOROD);
		ret.add(TZ_EUROPE_VADUZ);
		ret.add(TZ_EUROPE_VATICAN);
		ret.add(TZ_EUROPE_VIENNA);
		ret.add(TZ_EUROPE_VILNIUS);
		ret.add(TZ_EUROPE_WARSAW);
		ret.add(TZ_EUROPE_ZAGREB);
		ret.add(TZ_EUROPE_ZAPOROZHYE);
		ret.add(TZ_EUROPE_ZURICH);
		ret.add(TZ_GMT);
		ret.add(TZ_GMT_PLUS_1);
		ret.add(TZ_GMT_PLUS_10);
		ret.add(TZ_GMT_PLUS_11);
		ret.add(TZ_GMT_PLUS_12);
		ret.add(TZ_GMT_PLUS_2);
		ret.add(TZ_GMT_PLUS_3);
		ret.add(TZ_GMT_PLUS_4);
		ret.add(TZ_GMT_PLUS_5);
		ret.add(TZ_GMT_PLUS_6);
		ret.add(TZ_GMT_PLUS_7);
		ret.add(TZ_GMT_PLUS_8);
		ret.add(TZ_GMT_PLUS_9);
		ret.add(TZ_GMT_MINUS_1);
		ret.add(TZ_GMT_MINUS_10);
		ret.add(TZ_GMT_MINUS_11);
		ret.add(TZ_GMT_MINUS_12);
		ret.add(TZ_GMT_MINUS_13);
		ret.add(TZ_GMT_MINUS_14);
		ret.add(TZ_GMT_MINUS_2);
		ret.add(TZ_GMT_MINUS_3);
		ret.add(TZ_GMT_MINUS_4);
		ret.add(TZ_GMT_MINUS_5);
		ret.add(TZ_GMT_MINUS_6);
		ret.add(TZ_GMT_MINUS_7);
		ret.add(TZ_GMT_MINUS_8);
		ret.add(TZ_GMT_MINUS_9);
		ret.add(TZ_GREENWICH);
		ret.add(TZ_HST);
		ret.add(TZ_INDIAN_ANTANANARIVO);
		ret.add(TZ_INDIAN_CHAGOS);
		ret.add(TZ_INDIAN_CHRISTMAS);
		ret.add(TZ_INDIAN_COCOS);
		ret.add(TZ_INDIAN_COMORO);
		ret.add(TZ_INDIAN_KERGUELEN);
		ret.add(TZ_INDIAN_MAHE);
		ret.add(TZ_INDIAN_MALDIVES);
		ret.add(TZ_INDIAN_MAURITIUS);
		ret.add(TZ_INDIAN_MAYOTTE);
		ret.add(TZ_INDIAN_REUNION);
		ret.add(TZ_MET);
		ret.add(TZ_MST);
		ret.add(TZ_MST7MDT);
		ret.add(TZ_PST8PDT);
		ret.add(TZ_PACIFIC_APIA);
		ret.add(TZ_PACIFIC_AUCKLAND);
		ret.add(TZ_PACIFIC_CHATHAM);
		ret.add(TZ_PACIFIC_EASTER);
		ret.add(TZ_PACIFIC_EFATE);
		ret.add(TZ_PACIFIC_ENDERBURY);
		ret.add(TZ_PACIFIC_FAKAOFO);
		ret.add(TZ_PACIFIC_FIJI);
		ret.add(TZ_PACIFIC_FUNAFUTI);
		ret.add(TZ_PACIFIC_GALAPAGOS);
		ret.add(TZ_PACIFIC_GAMBIER);
		ret.add(TZ_PACIFIC_GUADALCANAL);
		ret.add(TZ_PACIFIC_GUAM);
		ret.add(TZ_PACIFIC_HONOLULU);
		ret.add(TZ_PACIFIC_JOHNSTON);
		ret.add(TZ_PACIFIC_KIRITIMATI);
		ret.add(TZ_PACIFIC_KOSRAE);
		ret.add(TZ_PACIFIC_KWAJALEIN);
		ret.add(TZ_PACIFIC_MAJURO);
		ret.add(TZ_PACIFIC_MARQUESAS);
		ret.add(TZ_PACIFIC_MIDWAY);
		ret.add(TZ_PACIFIC_NAURU);
		ret.add(TZ_PACIFIC_NIUE);
		ret.add(TZ_PACIFIC_NORFOLK);
		ret.add(TZ_PACIFIC_NOUMEA);
		ret.add(TZ_PACIFIC_PAGO_PAGO);
		ret.add(TZ_PACIFIC_PALAU);
		ret.add(TZ_PACIFIC_PITCAIRN);
		ret.add(TZ_PACIFIC_PONAPE);
		ret.add(TZ_PACIFIC_PORT_MORESBY);
		ret.add(TZ_PACIFIC_RAROTONGA);
		ret.add(TZ_PACIFIC_SAIPAN);
		ret.add(TZ_PACIFIC_TAHITI);
		ret.add(TZ_PACIFIC_TARAWA);
		ret.add(TZ_PACIFIC_TONGATAPU);
		ret.add(TZ_PACIFIC_TRUK);
		ret.add(TZ_PACIFIC_WAKE);
		ret.add(TZ_PACIFIC_WALLIS);
		ret.add(TZ_PACIFIC_YAP);
		ret.add(TZ_UCT);
		ret.add(TZ_UTC);
		ret.add(TZ_UNIVERSAL);
		ret.add(TZ_WET);
		ret.add(TZ_ZULU);
		return ret;
	}

	public static class NtpServer {
		String address;
		Integer keyId;
		String keyValue;
		boolean prefer;
		
		public NtpServer(String address, Integer keyId, String keyValue, boolean prefer) {
			this.address = address;
			this.keyId = keyId;
			this.keyValue = keyValue;
			this.prefer = prefer;
		}

		public final String getAddress() {
			return address;
		}

		public final Integer getKeyId() {
			return keyId;
		}

		public final String getKeyValue() {
			return keyValue;
		}

		public final boolean isPrefer() {
			return prefer;
		}
	}
	
	private String timeZone;
	private List<NtpServer> ntpServerList;
	private String ntpSourceAddress;
	
	public DateTime(String timeZone) {
		this.timeZone = (StringUtils.isNotBlank(timeZone)) ? timeZone : TZ_UTC;
		this.ntpServerList = null;
		this.ntpServerList = new ArrayList<NtpServer>();
	}

	public final String getTimeZone() {
		return timeZone;
	}

	public final List<NtpServer> getNtpServerList() {
		return Collections.unmodifiableList(ntpServerList);
	}

	public final String getNtpSourceAddress() {
		return ntpSourceAddress;
	}

	public final void setNtpSourceAddress(String v) {
		this.ntpSourceAddress = v;
	}
	
	public final void addNtpServer(NtpServer server) {
		this.ntpServerList.add(server);
	}
	
	public static DateTime createFromNode(Node node, DeviceConfig cfg) {
		DateTime ret = new DateTime(cfg.getString(node, "time-zone/text()"));
		
	
		Node ntp = cfg.getNode(node, "ntp");
		if (ntp != null) {
			String ntpSourceAddress = cfg.getString(ntp, "source-address/text()");
			ret.ntpSourceAddress = (StringUtils.isNotBlank(ntpSourceAddress)) ? ntpSourceAddress : null;
	
			for (Node server : IterableNodeList.apply(cfg.getNodeList(ntp, "server"))) {
				String address = cfg.getString(server, "name/text()");
				String keyName = cfg.getString(server, "key/text()");
				boolean prefer = XML.isBooleanElementSet(server, "prefer");
				Integer keyId = null;
				String keyValue = null;

				if (StringUtils.isNotBlank(keyName)) {
					try {
						Node auth = cfg.getNode(ntp, "authentication-key[normalize-space(name/text())=%s]", XML.xpc(keyName));
						if (auth != null) {
							keyId = Integer.valueOf(keyName);
							keyValue = cfg.getString(auth, "value/text()");
						}
					}
					catch (NumberFormatException e) {
						keyId = null;
						keyValue = null;
					}
				}
			
				NtpServer ntpServer = new NtpServer(address, keyId, keyValue, prefer);
				ret.ntpServerList.add(ntpServer);
			}
		}
	
		return ret;
	}

	public void sortNtpServerList() {
		Collections.sort(ntpServerList, new Comparator<NtpServer>() {

			@Override
			public int compare(NtpServer o1, NtpServer o2) {
				if (o1.prefer && !o2.prefer)
					return -1;
				return 0;
			}
			
		});	
	}
	
	public DeviceConfigure getEditConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					final String authKeyTag = "authentication-key";
					final String serverTag = "server";
					final String trustKeyTag = "trusted-key";
					
					Node system = cfg.systemNode();
					cfg.setValueElement(system, "time-zone", timeZone);
					
					Node ntp = cfg.createElementsIfNotExist(system, "ntp");
					cfg.deleteNode(ntp, authKeyTag);
					cfg.deleteNode(ntp, serverTag);
					cfg.deleteNode(ntp, trustKeyTag);
					
					ArrayList<Integer> keyList = new ArrayList<Integer>();
					for (NtpServer server : ntpServerList) {
						Integer keyId = server.getKeyId();
						String keyValue = server.getKeyValue();
						
						if (keyId != null && StringUtils.isNotBlank(keyValue)) {
							Node auth = cfg.createElement(authKeyTag);
							auth.appendChild(cfg.createElement("name", "%d", keyId));
							auth.appendChild(cfg.createElement("type", "md5"));
							auth.appendChild(cfg.createElement("value", keyValue));
							ntp.appendChild(auth);
							
							keyList.add(keyId);
						}
					}

					for (NtpServer server : ntpServerList) {
						Node snode = cfg.createElement(serverTag);
						snode.appendChild(cfg.createElement("name", server.getAddress()));
						
						Integer keyId = server.getKeyId();
						String keyValue = server.getKeyValue();
						if (keyId != null && StringUtils.isNotBlank(keyValue)) {
							snode.appendChild(cfg.createElement("key", "%d", keyId));
						}

						if (server.isPrefer())
							snode.appendChild(cfg.createElement("prefer"));
						
						ntp.appendChild(snode);
					}
				
					for (Integer kid : keyList) {
						ntp.appendChild(cfg.createElement(trustKeyTag, "%d", kid));
					}
					
					if (!ntp.hasChildNodes()) {
						system.removeChild(ntp);
					}
					else {
						final String srcAddrTag = "source-address";
						cfg.deleteNode(ntp, srcAddrTag);
						if (StringUtils.isNotBlank(ntpSourceAddress)) {
							ntp.appendChild(cfg.createElement(srcAddrTag, ntpSourceAddress));
						}
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config date/time.", e);
				}
			}
			
		};
	}
}
