package to.lef.srxlib.zone;

import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.nic.LogicalInetInterface;

public abstract class InboundTraffic {
	protected static final String TAG_EXCEPT = "except";
	abstract protected String getTagName();

	public DeviceConfigure getOptionsConfigure(final Node zone, final LogicalInetInterface lint) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				InboundTraffic self = InboundTraffic.this;
				String ptag = self.getTagName();
				
				try {
					Node interfaces = cfg.getNode(zone, "interfaces[normalize-space(name/text())=%s]", XML.xpc(lint.getName()));
					if (interfaces == null) {
						interfaces = cfg.createElement("interfaces");
						interfaces.appendChild(cfg.createElement("name", lint.getName()));
						zone.appendChild(interfaces);
					}

					Node traffic  = cfg.createElementsIfNotExist(interfaces, "host-inbound-traffic");
					Node ztraffic = cfg.getNode(zone, "host-inbound-traffic");

					boolean zHasAll = false;
					if (ztraffic != null) {
						Node zAll = cfg.getNode(ztraffic, "%s[normalize-space(name/text())='all']", ptag);
						if (zAll != null) 
							zHasAll = true;
					}
					configOptions(ztraffic, zHasAll, traffic, cfg);
					
					if (traffic == null)
						traffic  = cfg.createElementsIfNotExist(interfaces, "host-inbound-traffic");
					
					if (traffic != null) {
						if (!traffic.hasChildNodes())
							interfaces.removeChild(traffic);
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config host inbound traffic.", e);
				}
			}

		};
	}

	abstract protected void configOptions(Node ztraffic, boolean zHasAll, Node traffic, DeviceConfig cfg) ;
	
	protected void clearOption(Node ztraffic, boolean zHasAll, Node traffic, String tag, DeviceConfig cfg) {
		Node vn;
		String ptag = getTagName();
		
		if (ztraffic != null) {
			vn = cfg.getNode(ztraffic, "%s[normalize-space(name/text())=%s]", ptag, XML.xpc(tag));
			if (vn != null) {
				vn.getParentNode().removeChild(vn);
			}
		}
		
		vn = cfg.getNode(traffic, "%s[normalize-space(name/text())=%s]", ptag, XML.xpc(tag));
		if (vn != null) {
			vn.getParentNode().removeChild(vn);
		}
	}
	
	protected void configOption(Node ztraffic, boolean zHasAll, Node traffic, String tag, boolean value, DeviceConfig cfg) {
		boolean def = (zHasAll) ? true : false;
		Node vn;
		String ptag = getTagName();
		boolean hasAll = false;
		
		if (ztraffic != null) {
			vn = cfg.getNode(ztraffic, "%s[normalize-space(name/text())=%s]", ptag, XML.xpc(tag));
			if (vn != null) {
				if (XML.isBooleanElementSet(vn, TAG_EXCEPT)) {
					def = false;
				}
				else {
					def = true;
				}
			}
		}
		
		vn = cfg.getNode(traffic, "%s[normalize-space(name/text())='all']", ptag);
		if (vn != null) {
			def = true;
			hasAll = true;
		}
	
		if (zHasAll && !hasAll) {
			Node all = cfg.createElement(ptag);
			all.appendChild(cfg.createElement("name", "all"));
			traffic.appendChild(all);
			hasAll = true;
		}
		
		vn = cfg.getNode(traffic, "%s[normalize-space(name/text())=%s]", ptag, XML.xpc(tag));
		if (vn == null) {
			vn = cfg.createElement(ptag);
			vn.appendChild(cfg.createElement("name", tag));
			traffic.appendChild(vn);
		}

		if (value == def) {
			traffic.removeChild(vn);
		}
		else {
			if (zHasAll || hasAll) {
				if (value) {
					traffic.removeChild(vn);
				}
				else {
					cfg.setBooleanElement(vn, TAG_EXCEPT, true);
				}
			}
			else {
				if (value) {
					cfg.setBooleanElement(vn, TAG_EXCEPT, false);
				}
				else {
					traffic.removeChild(vn);
				}
			}
		}
	}
	
	abstract protected void constructOptions(Node ztraffic, boolean zHasAll, Node traffic, DeviceConfig cfg); 
	public void constructFromNode(Node zone, LogicalInetInterface lint, DeviceConfig cfg) {
		String ptag = getTagName();
		Node ztraffic = cfg.getNode(zone, "host-inbound-traffic");
		Node interfaces = cfg.getNode(zone, "interfaces[normalize-space(name/text())=%s]", XML.xpc(lint.getName()));
		Node traffic = null;
	
		if (interfaces != null)
			traffic  = cfg.createElementsIfNotExist(interfaces, "host-inbound-traffic");

		boolean zHasAll = false;
		if (ztraffic != null) {
			Node zAll = cfg.getNode(ztraffic, "%s[normalize-space(name/text())='all']", ptag);
			if (zAll != null) 
				zHasAll = true;
		}
		constructOptions(ztraffic, zHasAll, traffic, cfg);
	}
	
	protected boolean constructOption(Node ztraffic, boolean zHasAll, Node traffic, String tag, DeviceConfig cfg) {
		String ptag = getTagName();
		boolean ret = (zHasAll) ? true : false;
		Node vn;
		
		if (ztraffic != null) {
			vn = cfg.getNode(ztraffic, "%s[normalize-space(name/text())=%s]", ptag, XML.xpc(tag));
			if (vn != null) {
				if (XML.isBooleanElementSet(vn, TAG_EXCEPT)) {
					ret = false;
				}
				else {
					ret = true;
				}
			}
		}
	
		if (traffic != null) {
			vn = cfg.getNode(traffic, "%s[normalize-space(name/text())='all']", ptag);
			if (vn != null) {
				ret = true;
			}

			vn = cfg.getNode(traffic, "%s[normalize-space(name/text())=%s]", ptag, XML.xpc(tag));
			if (vn != null) {
				if (XML.isBooleanElementSet(vn, TAG_EXCEPT)) {
					ret = false;
				}
				else {
					ret = true;
				}
			}
		}
		
		return ret;
	}

}
