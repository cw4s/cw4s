package to.lef.srxlib.zone;

import java.util.ArrayList;
import java.util.List;

import javax.xml.xpath.XPathExpression;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.nic.LogicalInterface;

import java.util.Collections;

public class SecurityZone {
	private final String name;
	private String screenName;
	private List<String> interfaceNameList;
	private boolean tcpReset;
	
	public static SecurityZone createFromNode(Node zone, DeviceConfig config) {
		XPathExpression xpathName = config.compileXpath("name/text()");
	
		String name = XML.getString(xpathName, zone);
		SecurityZone ret = new SecurityZone(name);

		ret.tcpReset = XML.isBooleanElementSet(zone, "tcp-rst");
		ret.screenName = config.getString(zone, "screen/text()");
		NodeList lints = config.getNodeList(zone, "interfaces");
	
		if (lints != null) {
			for (int i = 0; i < lints.getLength(); i++) {
				Node lint = lints.item(i);
				String v = XML.getString(xpathName, lint);
				if (StringUtils.isNotBlank(v))
					ret.interfaceNameList.add(v);
			}
		}
		return ret;
	}

	public boolean isEditable(DeviceConfig cfg) {
		return isEditable(getName(), cfg);
	}
	
	public static boolean isEditable(String name, DeviceConfig cfg) {
		if ("junos-host".equals(name))
			return false;
		
		Node z = cfg.findSecurityZoneNode(name);
		if (z == null) 
			return false;
	
		return true;
	}
	
	public boolean isRemovable(DeviceConfig cfg) {
		return isRemovable(getName(), cfg);
	}
	
	public static boolean isRemovable(String name, DeviceConfig cfg) {
		if ("junos-host".equals(name))
			return false;
		
		Node z = cfg.findSecurityZoneNode(name);
		if (z == null) 
			return false;
	
		return !isInUse(name, cfg);
	}

	public boolean isInUse(DeviceConfig cfg) {
		return isInUse(getName(), cfg);
	}
	
	public static boolean isInUse(String name, DeviceConfig cfg) {
		Node z = cfg.findSecurityZoneNode(name);
		if (z == null) 
			return false;
		
		Node n = cfg.getNode(z, "interfaces/name");
		if (n != null)
			return true;

		n = cfg.getNode(cfg.securityNode(), "address-book[normalize-space(attach/zone/name/text())=%s]", XML.xpc(name));
		if (n != null)
			return true;
		
		Node policies = cfg.policiesNode();
		n = cfg.getNode(policies, "policy[normalize-space(from-zone-name/text())=%s or normalize-space(to-zone-name/text())=%s]", XML.xpc(name), XML.xpc(name));
		if (n != null)
			return true;
		
		return false;
	}
	
	public SecurityZone(String name) {
		this.name = name;
		this.tcpReset = false;
		this.interfaceNameList = new ArrayList<String>();
	}

	public SecurityZone(String name, boolean tcpRst) {
		this(name);
		this.tcpReset = tcpRst;
	}
	
	public final String getName() {
		return name;
	}
	
	public final String getScreenName() {
		return (StringUtils.isNotBlank(screenName)) ? screenName : name + "-screen";
	}

	public final IDSOption getIDSOption(DeviceConfig config) {
		return config.findIDSOption(getScreenName());
	}
	
	public final void setTcpReset(boolean v) {
		tcpReset = v;
	}
	
	public final boolean isTcpReset() {
		return tcpReset;
	}
	
	public final List<String> getInterfaceNameList() {
		return Collections.unmodifiableList(interfaceNameList);
	}
	
	public DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				SecurityZone zone = SecurityZone.this;
				
				try {
					Node zs = config.createElementsIfNotExist("security", "zones");
					Element z = config.createElement("security-zone");

					z.appendChild(config.createElement("name", zone.getName()));
					config.setBooleanElement(z, "tcp-rst", zone.isTcpReset());
					zs.appendChild(z);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config create security zone.", e);
				}
			}
		};
	}

	public DeviceConfigure getBasicConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig configuration, DeviceInformationProvider info) throws DeviceConfigException {
				SecurityZone zone = SecurityZone.this;
				
				try {
					Node n = configuration.findSecurityZoneNode(zone.getName());
					if (n == null)
						throw new IllegalArgumentException("security zone is not found.");
					
					configuration.setBooleanElement(n, "tcp-rst", zone.isTcpReset());
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config basic security zone.", e);
				}
			}
			
		};
	}
	
	public DeviceConfigure getDeleteConfigure() {
		return getDeleteConfigure(getName());
	}
	
	public static DeviceConfigure getDeleteConfigure(final String name) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					Node n = config.findSecurityZoneNode(name);
					if (n != null) {
						String screenName = config.getString(n, "screen/text()");
						n.getParentNode().removeChild(n);
						
						if (StringUtils.isNotBlank(screenName)) {
							IDSOption screen = config.findIDSOption(screenName);
							if (screen != null)
								screen.getDeleteConfigure().config(config, info);
						}
					}
				} catch (Throwable e) {
					throw new DeviceConfigException("Failed to config delete security zone.", e);
				}
			}
		};
	}
	
	public static DeviceConfigure getDeleteInterfaceConfigure(final String name) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					Node n = config.getNode(String.format("security/zones/security-zone/interfaces/name[normalize-space(text())=%s]/..", XML.xpc(name)));
					if (n != null) {
						n.getParentNode().removeChild(n);
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to delete interface from zone.", e);
				}
			}
			
		};
	}
	
	public static DeviceConfigure getAddInterfaceConfigure(final LogicalInterface lint) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
//				final String lintName = lint.getInternalName();
				final String lintName = lint.getName();
				final String zoneName = lint.getZoneName();
				
				getDeleteInterfaceConfigure(lintName).config(cfg, info);
				
				try {
					if (StringUtils.isNotBlank(zoneName)) {
						Node n = cfg.findSecurityZoneNode(zoneName);
						Element interfaces = cfg.createElement("interfaces");
						
						if (n == null) {
							throw new IllegalArgumentException("Zone node is not found.");
						}
						interfaces.appendChild(cfg.createElement("name", lintName));
						n.appendChild(interfaces);
					}
				} catch (Throwable e) {
					throw new DeviceConfigException("Failed to add interface to security zone.", e);
				}
			}
		};
	}

	public DeviceConfigure getIDSOptionConfigure(final IDSOption idsOption) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					Node zoneNode = config.findSecurityZoneNode(name);
				
					if (zoneNode == null) 
						throw new IllegalStateException("Security zone is not configured.");
					
					config.deleteNode(zoneNode, "screen");
					idsOption.getConfigure().config(config, info);
					
					if (config.findIDSOptionNode(idsOption.getName()) != null) {
						zoneNode.appendChild(config.createElement("screen", idsOption.getName()));
					}
				}
				catch (Exception e) {
					throw new DeviceConfigException("Failed to config screen.", e);
				}
			}
			
		};
	}
	
	public int isChanged(DeviceConfig candidate) {
		SecurityZone c = candidate.findSecurityZone(name);
		if (c == null)
			return 1;
		if (!equals(c)) 
			return 2;
		return 0;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
//		result = prime * result + ((screenName == null) ? 0 : screenName.hashCode());
		result = prime * result + (tcpReset ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SecurityZone other = (SecurityZone) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (tcpReset != other.tcpReset) {
			return false;
		}
		return true;
	}
}
