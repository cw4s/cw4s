package to.lef.srxlib.zone;

import org.w3c.dom.Node;

import to.lef.srxlib.BaseConfig;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.SoftwareInformation;
import to.lef.srxlib.dom.XML;

public class InboundProtocol extends InboundTraffic {
	private boolean bfdEnabled;
	private boolean bgpEnabled;
	private boolean dvmrpEnabled;
	private boolean igmpEnabled;
	private boolean ldpEnabled;
	private boolean msdpEnabled;
	private boolean nhrpEnabled;
	private boolean ospfEnabled;
	private boolean ospf3Enabled;
	private boolean pgmEnabled;
	private boolean pimEnabled;
	private boolean ripEnabled;
	private boolean ripngEnabled;
	private boolean routerDiscoveryEnabled;
	private boolean rsvpEnabled;
	private boolean sapEnabled;
	private boolean vrrpEnabled;

	public InboundProtocol() {
		this(false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false, false);
	}
	
	public InboundProtocol(boolean bfdEnabled, boolean bgpEnabled,
			boolean dvmrpEnabled, boolean igmpEnabled, boolean ldpEnabled,
			boolean msdpEnabled, boolean nhrpEnabled, boolean ospfEnabled,
			boolean ospf3Enabled, boolean pgmEnabled, boolean pimEnabled,
			boolean ripEnabled, boolean ripngEnabled,
			boolean routerDiscoveryEnabled, boolean rsvpEnabled,
			boolean sapEnabled, boolean vrrpEnabled) {
		this.bfdEnabled = bfdEnabled;
		this.bgpEnabled = bgpEnabled;
		this.dvmrpEnabled = dvmrpEnabled;
		this.igmpEnabled = igmpEnabled;
		this.ldpEnabled = ldpEnabled;
		this.msdpEnabled = msdpEnabled;
		this.nhrpEnabled = nhrpEnabled;
		this.ospfEnabled = ospfEnabled;
		this.ospf3Enabled = ospf3Enabled;
		this.pgmEnabled = pgmEnabled;
		this.pimEnabled = pimEnabled;
		this.ripEnabled = ripEnabled;
		this.ripngEnabled = ripngEnabled;
		this.routerDiscoveryEnabled = routerDiscoveryEnabled;
		this.rsvpEnabled = rsvpEnabled;
		this.sapEnabled = sapEnabled;
		this.vrrpEnabled = vrrpEnabled;
	}

	@Override
	protected final String getTagName() {
		return "protocols";
	}

	public final boolean isBfdEnabled() {
		return bfdEnabled;
	}
	public final boolean isBgpEnabled() {
		return bgpEnabled;
	}
	public final boolean isDvmrpEnabled() {
		return dvmrpEnabled;
	}
	public final boolean isIgmpEnabled() {
		return igmpEnabled;
	}
	public final boolean isLdpEnabled() {
		return ldpEnabled;
	}
	public final boolean isMsdpEnabled() {
		return msdpEnabled;
	}
	public final boolean isNhrpEnabled() {
		return nhrpEnabled;
	}
	public final boolean isOspfEnabled() {
		return ospfEnabled;
	}
	public final boolean isOspf3Enabled() {
		return ospf3Enabled;
	}
	public final boolean isPgmEnabled() {
		return pgmEnabled;
	}
	public final boolean isPimEnabled() {
		return pimEnabled;
	}
	public final boolean isRipEnabled() {
		return ripEnabled;
	}
	public final boolean isRipngEnabled() {
		return ripngEnabled;
	}
	public final boolean isRouterDiscoveryEnabled() {
		return routerDiscoveryEnabled;
	}
	public final boolean isRsvpEnabled() {
		return rsvpEnabled;
	}
	public final boolean isSapEnabled() {
		return sapEnabled;
	}
	public final boolean isVrrpEnabled() {
		return vrrpEnabled;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (bfdEnabled ? 1231 : 1237);
		result = prime * result + (bgpEnabled ? 1231 : 1237);
		result = prime * result + (dvmrpEnabled ? 1231 : 1237);
		result = prime * result + (igmpEnabled ? 1231 : 1237);
		result = prime * result + (ldpEnabled ? 1231 : 1237);
		result = prime * result + (msdpEnabled ? 1231 : 1237);
		result = prime * result + (nhrpEnabled ? 1231 : 1237);
		result = prime * result + (ospf3Enabled ? 1231 : 1237);
		result = prime * result + (ospfEnabled ? 1231 : 1237);
		result = prime * result + (pgmEnabled ? 1231 : 1237);
		result = prime * result + (pimEnabled ? 1231 : 1237);
		result = prime * result + (ripEnabled ? 1231 : 1237);
		result = prime * result + (ripngEnabled ? 1231 : 1237);
		result = prime * result + (routerDiscoveryEnabled ? 1231 : 1237);
		result = prime * result + (rsvpEnabled ? 1231 : 1237);
		result = prime * result + (sapEnabled ? 1231 : 1237);
		result = prime * result + (vrrpEnabled ? 1231 : 1237);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		InboundProtocol other = (InboundProtocol) obj;
		if (bfdEnabled != other.bfdEnabled) {
			return false;
		}
		if (bgpEnabled != other.bgpEnabled) {
			return false;
		}
		if (dvmrpEnabled != other.dvmrpEnabled) {
			return false;
		}
		if (igmpEnabled != other.igmpEnabled) {
			return false;
		}
		if (ldpEnabled != other.ldpEnabled) {
			return false;
		}
		if (msdpEnabled != other.msdpEnabled) {
			return false;
		}
		if (nhrpEnabled != other.nhrpEnabled) {
			return false;
		}
		if (ospf3Enabled != other.ospf3Enabled) {
			return false;
		}
		if (ospfEnabled != other.ospfEnabled) {
			return false;
		}
		if (pgmEnabled != other.pgmEnabled) {
			return false;
		}
		if (pimEnabled != other.pimEnabled) {
			return false;
		}
		if (ripEnabled != other.ripEnabled) {
			return false;
		}
		if (ripngEnabled != other.ripngEnabled) {
			return false;
		}
		if (routerDiscoveryEnabled != other.routerDiscoveryEnabled) {
			return false;
		}
		if (rsvpEnabled != other.rsvpEnabled) {
			return false;
		}
		if (sapEnabled != other.sapEnabled) {
			return false;
		}
		if (vrrpEnabled != other.vrrpEnabled) {
			return false;
		}
		return true;
	}

	
	@Override
	protected void configOptions(Node ztraffic, boolean zHasAll, Node traffic, DeviceConfig cfg) {
		boolean v6enabled = BaseConfig.FORWARDING_INET6_OPTION_FLOW.equals(cfg.getForwardingInet6Option());
		
		configOption(ztraffic, zHasAll, traffic, "bfd", bfdEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "bgp", bgpEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "dvmrp", dvmrpEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "igmp", igmpEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "ldp", ldpEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "msdp", msdpEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "nhrp", nhrpEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "ospf", ospfEnabled, cfg);
		if (v6enabled) {
			configOption(ztraffic, zHasAll, traffic, "ospf3", ospf3Enabled, cfg);
		}
		else {
			cfg.deleteNode(traffic, "%s[normalize-space(name/text())=%s]", getTagName(), XML.xpc("ospf3"));
		}
		configOption(ztraffic, zHasAll, traffic, "pgm", pgmEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "pim", pimEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "rip", ripEnabled, cfg);
		
		if (v6enabled) {
			configOption(ztraffic, zHasAll, traffic, "ripng", ripngEnabled, cfg);
		}
		else {
			cfg.deleteNode(traffic, "%s[normalize-space(name/text())=%s]", getTagName(), XML.xpc("ripng"));
		}	
		
		configOption(ztraffic, zHasAll, traffic, "router-discovery", routerDiscoveryEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "rsvp", rsvpEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "sap", sapEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "vrrp", vrrpEnabled, cfg);			
	}

	@Override
	protected void constructOptions(Node ztraffic, boolean zHasAll, Node traffic, DeviceConfig cfg) {
		bfdEnabled             = constructOption(ztraffic, zHasAll, traffic, "bfd", cfg);
		bgpEnabled             = constructOption(ztraffic, zHasAll, traffic, "bgp", cfg);
		dvmrpEnabled           = constructOption(ztraffic, zHasAll, traffic, "dvmrp", cfg);
		igmpEnabled            = constructOption(ztraffic, zHasAll, traffic, "igmp", cfg);
		ldpEnabled             = constructOption(ztraffic, zHasAll, traffic, "ldp", cfg);
		msdpEnabled            = constructOption(ztraffic, zHasAll, traffic, "msdp", cfg);
		nhrpEnabled            = constructOption(ztraffic, zHasAll, traffic, "nhrp", cfg);
		ospfEnabled            = constructOption(ztraffic, zHasAll, traffic, "ospf", cfg);
		ospf3Enabled           = constructOption(ztraffic, zHasAll, traffic, "ospf3", cfg);
		pgmEnabled             = constructOption(ztraffic, zHasAll, traffic, "pgm", cfg);
		pimEnabled             = constructOption(ztraffic, zHasAll, traffic, "pim", cfg);
		ripEnabled             = constructOption(ztraffic, zHasAll, traffic, "rip", cfg);
		ripngEnabled           = constructOption(ztraffic, zHasAll, traffic, "ripng", cfg);
		routerDiscoveryEnabled = constructOption(ztraffic, zHasAll, traffic, "router-discovery", cfg);
		rsvpEnabled            = constructOption(ztraffic, zHasAll, traffic, "rsvp", cfg);
		sapEnabled             = constructOption(ztraffic, zHasAll, traffic, "sap", cfg);
		vrrpEnabled            = constructOption(ztraffic, zHasAll, traffic, "vrrp", cfg);
	}
}