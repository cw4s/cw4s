package to.lef.srxlib.zone;

import org.w3c.dom.Node;

import to.lef.srxlib.BaseConfig;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.dom.XML;

public class InboundService extends InboundTraffic {
	private boolean bootpEnabled;
	private boolean dhcpEnabled;
	private boolean dhcpv6Enabled;
	private boolean dnsEnabled;
	private boolean fingerEnabled;
	private boolean ftpEnabled;
	private boolean httpEnabled;
	private boolean httpsEnabled;
	private boolean identResetEnabled;
	private boolean ikeEnabled;
	private boolean lspingEnabled;
	private boolean netconfEnabled;
	private boolean ntpEnabled;
	private boolean pingEnabled;
	private boolean r2cpEnabled;
	private boolean reverseSshEnabled;
	private boolean reverseTelnetEnabled;
	private boolean rloginEnabled;
	private boolean rpmEnabled;
	private boolean rshEnabled;
	private boolean sipEnabled;
	private boolean snmpEnabled;
	private boolean snmpTrapEnabled;
	private boolean sshEnabled;
	private boolean telnetEnabled;
	private boolean tftpEnabled;
	private boolean tracerouteEnabled;
	private boolean xnmClearTextEnabled;
	private boolean xnmSslEnabled;

	public InboundService() {
		this(false, false, false, false,
				false, false, false, false, false,
				false, false, false, false, false,
				false, false, false, false, false,
				false, false, false, false, false,
				false, false, false, false, false);
	}
	
	public InboundService(
			boolean bootpEnabled, boolean dhcpEnabled,
			boolean dhcpv6Enabled, boolean dnsEnabled,
			boolean fingerEnabled, boolean ftpEnabled, boolean httpEnabled,
			boolean httpsEnabled, boolean identResetEnabled,
			boolean ikeEnabled, boolean lspingEnabled,
			boolean netconfEnabled, boolean ntpEnabled,
			boolean pingEnabled, boolean r2cpEnabled,
			boolean reverseSshEnabled, boolean reverseTelnetEnabled,
			boolean rloginEnabled, boolean rpmEnabled, boolean rshEnabled,
			boolean sipEnabled, boolean snmpEnabled,
			boolean snmpTrapEnabled, boolean sshEnabled,
			boolean telnetEnabled, boolean tftpEnabled,
			boolean tracerouteEnabled, boolean xnmClearTextEnabled,
			boolean xnmSslEnabled) {
		this.bootpEnabled = bootpEnabled;
		this.dhcpEnabled = dhcpEnabled;
		this.dhcpv6Enabled = dhcpv6Enabled;
		this.dnsEnabled = dnsEnabled;
		this.fingerEnabled = fingerEnabled;
		this.ftpEnabled = ftpEnabled;
		this.httpEnabled = httpEnabled;
		this.httpsEnabled = httpsEnabled;
		this.identResetEnabled = identResetEnabled;
		this.ikeEnabled = ikeEnabled;
		this.lspingEnabled = lspingEnabled;
		this.netconfEnabled = netconfEnabled;
		this.ntpEnabled = ntpEnabled;
		this.pingEnabled = pingEnabled;
		this.r2cpEnabled = r2cpEnabled;
		this.reverseSshEnabled = reverseSshEnabled;
		this.reverseTelnetEnabled = reverseTelnetEnabled;
		this.rloginEnabled = rloginEnabled;
		this.rpmEnabled = rpmEnabled;
		this.rshEnabled = rshEnabled;
		this.sipEnabled = sipEnabled;
		this.snmpEnabled = snmpEnabled;
		this.snmpTrapEnabled = snmpTrapEnabled;
		this.sshEnabled = sshEnabled;
		this.telnetEnabled = telnetEnabled;
		this.tftpEnabled = tftpEnabled;
		this.tracerouteEnabled = tracerouteEnabled;
		this.xnmClearTextEnabled = xnmClearTextEnabled;
		this.xnmSslEnabled = xnmSslEnabled;
	}

	@Override
	protected final String getTagName() {
		return "system-services";
	}
	
	public final boolean isBootpEnabled() {
		return bootpEnabled;
	}
	public final boolean isDhcpEnabled() {
		return dhcpEnabled;
	}
	public final boolean isDhcpv6Enabled() {
		return dhcpv6Enabled;
	}
	public final boolean isDnsEnabled() {
		return dnsEnabled;
	}
	public final boolean isFingerEnabled() {
		return fingerEnabled;
	}
	public final boolean isFtpEnabled() {
		return ftpEnabled;
	}
	public final boolean isHttpEnabled() {
		return httpEnabled;
	}
	public final boolean isHttpsEnabled() {
		return httpsEnabled;
	}
	public final boolean isIdentResetEnabled() {
		return identResetEnabled;
	}
	public final boolean isIkeEnabled() {
		return ikeEnabled;
	}
	public final boolean isLspingEnabled() {
		return lspingEnabled;
	}
	public final boolean isNetconfEnabled() {
		return netconfEnabled;
	}
	public final boolean isNtpEnabled() {
		return ntpEnabled;
	}
	public final boolean isPingEnabled() {
		return pingEnabled;
	}
	public final boolean isR2cpEnabled() {
		return r2cpEnabled;
	}
	public final boolean isReverseSshEnabled() {
		return reverseSshEnabled;
	}
	public final boolean isReverseTelnetEnabled() {
		return reverseTelnetEnabled;
	}
	public final boolean isRloginEnabled() {
		return rloginEnabled;
	}
	public final boolean isRpmEnabled() {
		return rpmEnabled;
	}
	public final boolean isRshEnabled() {
		return rshEnabled;
	}
	public final boolean isSipEnabled() {
		return sipEnabled;
	}
	public final boolean isSnmpEnabled() {
		return snmpEnabled;
	}
	public final boolean isSnmpTrapEnabled() {
		return snmpTrapEnabled;
	}
	public final boolean isSshEnabled() {
		return sshEnabled;
	}
	public final boolean isTelnetEnabled() {
		return telnetEnabled;
	}
	public final boolean isTftpEnabled() {
		return tftpEnabled;
	}
	public final boolean isTracerouteEnabled() {
		return tracerouteEnabled;
	}
	public final boolean isXnmClearTextEnabled() {
		return xnmClearTextEnabled;
	}
	public final boolean isXnmSslEnabled() {
		return xnmSslEnabled;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (bootpEnabled ? 1231 : 1237);
		result = prime * result + (dhcpEnabled ? 1231 : 1237);
		result = prime * result + (dhcpv6Enabled ? 1231 : 1237);
		result = prime * result + (dnsEnabled ? 1231 : 1237);
		result = prime * result + (fingerEnabled ? 1231 : 1237);
		result = prime * result + (ftpEnabled ? 1231 : 1237);
		result = prime * result + (httpEnabled ? 1231 : 1237);
		result = prime * result + (httpsEnabled ? 1231 : 1237);
		result = prime * result + (identResetEnabled ? 1231 : 1237);
		result = prime * result + (ikeEnabled ? 1231 : 1237);
		result = prime * result + (lspingEnabled ? 1231 : 1237);
		result = prime * result + (netconfEnabled ? 1231 : 1237);
		result = prime * result + (ntpEnabled ? 1231 : 1237);
		result = prime * result + (pingEnabled ? 1231 : 1237);
		result = prime * result + (r2cpEnabled ? 1231 : 1237);
		result = prime * result + (reverseSshEnabled ? 1231 : 1237);
		result = prime * result + (reverseTelnetEnabled ? 1231 : 1237);
		result = prime * result + (rloginEnabled ? 1231 : 1237);
		result = prime * result + (rpmEnabled ? 1231 : 1237);
		result = prime * result + (rshEnabled ? 1231 : 1237);
		result = prime * result + (sipEnabled ? 1231 : 1237);
		result = prime * result + (snmpEnabled ? 1231 : 1237);
		result = prime * result + (snmpTrapEnabled ? 1231 : 1237);
		result = prime * result + (sshEnabled ? 1231 : 1237);
		result = prime * result + (telnetEnabled ? 1231 : 1237);
		result = prime * result + (tftpEnabled ? 1231 : 1237);
		result = prime * result + (tracerouteEnabled ? 1231 : 1237);
		result = prime * result + (xnmClearTextEnabled ? 1231 : 1237);
		result = prime * result + (xnmSslEnabled ? 1231 : 1237);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		InboundService other = (InboundService) obj;
		if (bootpEnabled != other.bootpEnabled) {
			return false;
		}
		if (dhcpEnabled != other.dhcpEnabled) {
			return false;
		}
		if (dhcpv6Enabled != other.dhcpv6Enabled) {
			return false;
		}
		if (dnsEnabled != other.dnsEnabled) {
			return false;
		}
		if (fingerEnabled != other.fingerEnabled) {
			return false;
		}
		if (ftpEnabled != other.ftpEnabled) {
			return false;
		}
		if (httpEnabled != other.httpEnabled) {
			return false;
		}
		if (httpsEnabled != other.httpsEnabled) {
			return false;
		}
		if (identResetEnabled != other.identResetEnabled) {
			return false;
		}
		if (ikeEnabled != other.ikeEnabled) {
			return false;
		}
		if (lspingEnabled != other.lspingEnabled) {
			return false;
		}
		if (netconfEnabled != other.netconfEnabled) {
			return false;
		}
		if (ntpEnabled != other.ntpEnabled) {
			return false;
		}
		if (pingEnabled != other.pingEnabled) {
			return false;
		}
		if (r2cpEnabled != other.r2cpEnabled) {
			return false;
		}
		if (reverseSshEnabled != other.reverseSshEnabled) {
			return false;
		}
		if (reverseTelnetEnabled != other.reverseTelnetEnabled) {
			return false;
		}
		if (rloginEnabled != other.rloginEnabled) {
			return false;
		}
		if (rpmEnabled != other.rpmEnabled) {
			return false;
		}
		if (rshEnabled != other.rshEnabled) {
			return false;
		}
		if (sipEnabled != other.sipEnabled) {
			return false;
		}
		if (snmpEnabled != other.snmpEnabled) {
			return false;
		}
		if (snmpTrapEnabled != other.snmpTrapEnabled) {
			return false;
		}
		if (sshEnabled != other.sshEnabled) {
			return false;
		}
		if (telnetEnabled != other.telnetEnabled) {
			return false;
		}
		if (tftpEnabled != other.tftpEnabled) {
			return false;
		}
		if (tracerouteEnabled != other.tracerouteEnabled) {
			return false;
		}
		if (xnmClearTextEnabled != other.xnmClearTextEnabled) {
			return false;
		}
		if (xnmSslEnabled != other.xnmSslEnabled) {
			return false;
		}
		return true;
	}
	
	@Override
	protected void configOptions(Node ztraffic, boolean zHasAll, Node traffic, DeviceConfig cfg) {
		boolean v6enabled = BaseConfig.FORWARDING_INET6_OPTION_FLOW.equals(cfg.getForwardingInet6Option());
		
		configOption(ztraffic, zHasAll, traffic, "bootp", bootpEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "dhcp", dhcpEnabled, cfg);
		if (v6enabled) {
			configOption(ztraffic, zHasAll, traffic, "dhcpv6", dhcpv6Enabled, cfg);
		}
		else {
			cfg.deleteNode(traffic, "%s[normalize-space(name/text())=%s]", getTagName(), XML.xpc("dhcpv6"));
		}
		configOption(ztraffic, zHasAll, traffic, "dns", dnsEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "finger", fingerEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "ftp", ftpEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "http", httpEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "https", httpsEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "ident-reset", identResetEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "ike", ikeEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "lsping", lspingEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "netconf", netconfEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "ntp", ntpEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "ping", pingEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "r2cp", r2cpEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "reverse-ssh", reverseSshEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "reverse-telnet", reverseTelnetEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "rlogin", rloginEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "rpm", rpmEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "rsh", rshEnabled, cfg);
		if (cfg.softwareInformation().isShipHidden()) {
			clearOption(ztraffic, zHasAll, traffic, "sip", cfg);
		}
		else {
			configOption(ztraffic, zHasAll, traffic, "sip", sipEnabled, cfg);
		}
		configOption(ztraffic, zHasAll, traffic, "snmp", snmpEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "snmp-trap", snmpTrapEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "ssh", sshEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "telnet", telnetEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "tftp", tftpEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "traceroute", tracerouteEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "xnm-clear-text", xnmClearTextEnabled, cfg);
		configOption(ztraffic, zHasAll, traffic, "xnm-ssl", xnmSslEnabled, cfg);	
	}

	@Override
	protected void constructOptions(Node ztraffic, boolean zHasAll, Node traffic, DeviceConfig cfg) {
		bootpEnabled         = constructOption(ztraffic, zHasAll, traffic, "bootp", cfg);
		dhcpEnabled          = constructOption(ztraffic, zHasAll, traffic, "dhcp", cfg);
		dhcpv6Enabled        = constructOption(ztraffic, zHasAll, traffic, "dhcpv6", cfg);
		dnsEnabled           = constructOption(ztraffic, zHasAll, traffic, "dns", cfg);
		fingerEnabled        = constructOption(ztraffic, zHasAll, traffic, "finger", cfg);
		ftpEnabled           = constructOption(ztraffic, zHasAll, traffic, "ftp", cfg);
		httpEnabled          = constructOption(ztraffic, zHasAll, traffic, "http", cfg);
		httpsEnabled         = constructOption(ztraffic, zHasAll, traffic, "https", cfg);
		identResetEnabled    = constructOption(ztraffic, zHasAll, traffic, "ident-reset", cfg);
		ikeEnabled           = constructOption(ztraffic, zHasAll, traffic, "ike", cfg);
		lspingEnabled        = constructOption(ztraffic, zHasAll, traffic, "lsping", cfg);
		netconfEnabled       = constructOption(ztraffic, zHasAll, traffic, "netconf", cfg);
		ntpEnabled           = constructOption(ztraffic, zHasAll, traffic, "ntp", cfg);
		pingEnabled          = constructOption(ztraffic, zHasAll, traffic, "ping", cfg);
		r2cpEnabled          = constructOption(ztraffic, zHasAll, traffic, "r2cp", cfg);
		reverseSshEnabled    = constructOption(ztraffic, zHasAll, traffic, "reverse-ssh", cfg);
		reverseTelnetEnabled = constructOption(ztraffic, zHasAll, traffic, "reverse-telnet", cfg);
		rloginEnabled        = constructOption(ztraffic, zHasAll, traffic, "rlogin", cfg);
		rpmEnabled           = constructOption(ztraffic, zHasAll, traffic, "rpm", cfg);
		rshEnabled           = constructOption(ztraffic, zHasAll, traffic, "rsh", cfg);
		if (cfg.softwareInformation().isShipHidden()) {
			sipEnabled       = false;
		}
		else {
			sipEnabled       = constructOption(ztraffic, zHasAll, traffic, "sip", cfg);
		}
		snmpEnabled          = constructOption(ztraffic, zHasAll, traffic, "snmp", cfg);
		snmpTrapEnabled      = constructOption(ztraffic, zHasAll, traffic, "snmp-trap", cfg);
		sshEnabled           = constructOption(ztraffic, zHasAll, traffic, "ssh", cfg);
		telnetEnabled        = constructOption(ztraffic, zHasAll, traffic, "telnet", cfg);
		tftpEnabled          = constructOption(ztraffic, zHasAll, traffic, "tftp", cfg);
		tracerouteEnabled    = constructOption(ztraffic, zHasAll, traffic, "traceroute", cfg);
		xnmClearTextEnabled  = constructOption(ztraffic, zHasAll, traffic, "xnm-clear-text", cfg);
		xnmSslEnabled        = constructOption(ztraffic, zHasAll, traffic, "xnm-ssl", cfg);
	}
}