package to.lef.srxlib.zone;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;

public class IDSOption {
	public static final int ICMP_FLOOD_THRESHOLD_DEFAULT           = 1000;
	public static final int ICMP_FLOOD_THRESHOLD_MIN               = 1;
	public static final int ICMP_FLOOD_THRESHOLD_MAX               = 100000;
	public static final int UDP_FLOOD_THRESHOLD_DEFAULT            = 1000;
	public static final int UDP_FLOOD_THRESHOLD_MIN                = 1;
	public static final int UDP_FLOOD_THRESHOLD_MAX                = 100000;
	public static final int TCP_SYN_FLOOD_ATTACK_THRESHOLD_DEFAULT = 200;
	public static final int TCP_SYN_FLOOD_ATTACK_THRESHOLD_MIN     = 1;
	public static final int TCP_SYN_FLOOD_ATTACK_THRESHOLD_MAX     = 100000;
	public static final int TCP_SYN_FLOOD_ALARM_THRESHOLD_DEFAULT  = 512;
	public static final int TCP_SYN_FLOOD_ALARM_THRESHOLD_MIN      = 1;
	public static final int TCP_SYN_FLOOD_ALARM_THRESHOLD_MAX      = 100000;
	public static final int TCP_SYN_FLOOD_SRC_THRESHOLD_DEFAULT    = 4000;
	public static final int TCP_SYN_FLOOD_SRC_THRESHOLD_MIN        = 4;
	public static final int TCP_SYN_FLOOD_SRC_THRESHOLD_MAX        = 100000;
	public static final int TCP_SYN_FLOOD_DST_THRESHOLD_DEFAULT    = 4000;
	public static final int TCP_SYN_FLOOD_DST_THRESHOLD_MIN        = 4;
	public static final int TCP_SYN_FLOOD_DST_THRESHOLD_MAX        = 100000;
	public static final int TCP_SYN_FLOOD_TIMEOUT_DEFAULT          = 20;
	public static final int TCP_SYN_FLOOD_TIMEOUT_MIN              = 1;
	public static final int TCP_SYN_FLOOD_TIMEOUT_MAX              = 50;
	public static final int IP_SWEEP_THRESHOLD_DEFAULT             = 5000;
	public static final int IP_SWEEP_THRESHOLD_MIN                 = 5000;
	public static final int IP_SWEEP_THRESHOLD_MAX                 = 1000000;
	public static final int TCP_PORT_SCAN_THRESHOLD_DEFAULT        = 5000;
	public static final int TCP_PORT_SCAN_THRESHOLD_MIN            = 5000;
	public static final int TCP_PORT_SCAN_THRESHOLD_MAX            = 1000000;
	public static final int TCP_SWEEP_THRESHOLD_DEFAULT            = 5000;
	public static final int TCP_SWEEP_THRESHOLD_MIN                = 1000;
	public static final int TCP_SWEEP_THRESHOLD_MAX                = 1000000;
	public static final int UDP_SWEEP_THRESHOLD_DEFAULT            = 5000;
	public static final int UDP_SWEEP_THRESHOLD_MIN                = 1000;
	public static final int UDP_SWEEP_THRESHOLD_MAX                = 1000000;
	public static final int TCP_SYN_ACK_ACK_THRESHOLD_DEFAULT      = 512;
	public static final int TCP_SYN_ACK_ACK_THRESHOLD_MIN          = 128;
	public static final int TCP_SYN_ACK_ACK_THRESHOLD_MAX          = 250000;
	public static final int SRC_LIMIT_THRESHOLD_DEFAULT            = 128;
	public static final int SRC_LIMIT_THRESHOLD_MIN                = 1;
	public static final int SRC_LIMIT_THRESHOLD_MAX                = 50000;
	public static final int DST_LIMIT_THRESHOLD_DEFAULT            = 128;
	public static final int DST_LIMIT_THRESHOLD_MIN                = 1;
	public static final int DST_LIMIT_THRESHOLD_MAX                = 50000;
	
	private final String name;

	private boolean alarmWithoutDrop;
	private boolean icmpFloodEnabled;
	private int icmpFloodThreshold;
	private boolean udpFloodEnabled;
	private int udpFloodThreshold;
	private boolean tcpSynFloodEnabled;
	private int tcpSynFloodAttackThreshold;
	private int tcpSynFloodAlarmThreshold;
	private int tcpSynFloodSrcThreshold;
	private int tcpSynFloodDstThreshold;
	private int tcpSynFloodTimeout;
	private boolean tcpWinnukeEnabled;
	private boolean ipSpoofEnabled;
	private boolean ipSweepEnabled;
	private int ipSweepThreshold;
	private boolean tcpPortScanEnabled;
	private int tcpPortScanThreshold;
	private boolean tcpSweepEnabled;
	private int tcpSweepThreshold;
	private boolean udpSweepEnabled;
	private int udpSweepThreshold;
	private boolean icmpPingOfDeathEnabled;
	private boolean ipTeardropEnabled;
	private boolean icmpFragmentEnabled;
	private boolean icmpLargeEnabled;
	private boolean ipBlockFragment;
	private boolean tcpLandEnabled;
	private boolean tcpSynAckAckEnabled;
	private int tcpSynAckAckThreshold;
	private boolean srcLimitEnabled;
	private int srcLimitThreshold;
	private boolean dstLimitEnabled;
	private int dstLimitThreshold;
	private boolean ipBadOptionEnabled;
	private boolean ipTimestampOptionEnabled;
	private boolean ipSecurityOptionEnabled;
	private boolean ipStreamOptionEnabled;
	private boolean ipRecordRouteOptionEnabled;
	private boolean ipLooseSrcRouteOptionEnabled;
	private boolean ipStrictSrcRouteOptionEnabled;
	private boolean ipSrcRouteOptionEnabled;
	private boolean tcpSynFragmentEnabled;
	private boolean tcpNoFlagEnabled;
	private boolean tcpSynAndFinEnabled;
	private boolean tcpFinNoAckEnabled;
	private boolean ipUnknownProtocolEnabled;

	public static IDSOption createFromNode(Node node, DeviceConfig config) {
		String name = config.getString(node, "name/text()");
		
		if (StringUtils.isBlank(name))
			return null;
		
		IDSOption screen = new IDSOption(name);
		if (config.getNode(node, "alarm-without-drop") != null) 
			screen.setAlarmWithoutDrop(true);
		
		Node tmp = config.getNode(node, "icmp");
		if (tmp != null) {
			Node key = config.getNode(tmp, "flood");
			if (key != null) {
				screen.setIcmpFloodEnabled(true);
				try {
					screen.setIcmpFloodThreshold(Integer.parseInt(config.getString(key, "threshold/text()")));
				} catch (Exception e) { }
			}
		
			key = config.getNode(tmp, "ip-sweep");
			if (key != null) {
				screen.setIpSweepEnabled(true);
				try {
					screen.setIpSweepThreshold(Integer.parseInt(config.getString(key, "threshold/text()")));
				} catch (Exception e) { }			
			}

			if (config.getNode(tmp, "ping-death") != null)
				screen.setIcmpPingOfDeathEnabled(true);
			if (config.getNode(tmp, "fragment") != null)
				screen.setIcmpFragmentEnabled(true);		
			if (config.getNode(tmp, "large") != null)
				screen.setIcmpLargeEnabled(true);		
		}
		
		tmp = config.getNode(node, "ip");
		if (tmp != null) {
			if (config.getNode(tmp, "spoofing") != null)
				screen.setIpSpoofEnabled(true);
			if (config.getNode(tmp, "tear-drop") != null)
				screen.setIpTeardropEnabled(true);
			if (config.getNode(tmp, "block-frag") != null)
				screen.setIpBlockFragment(true);
			if (config.getNode(tmp, "bad-option") != null)
				screen.setIpBadOptionEnabled(true);
			if (config.getNode(tmp, "timestamp-option") != null)
				screen.setIpTimestampOptionEnabled(true);
			if (config.getNode(tmp, "security-option") != null)
				screen.setIpSecurityOptionEnabled(true);
			if (config.getNode(tmp, "stream-option") != null)
				screen.setIpStreamOptionEnabled(true);
			if (config.getNode(tmp, "record-route-option") != null)
				screen.setIpRecordRouteOptionEnabled(true);
			if (config.getNode(tmp, "loose-source-route-option") != null)
				screen.setIpLooseSrcRouteOptionEnabled(true);
			if (config.getNode(tmp, "strict-source-route-option") != null)
				screen.setIpStrictSrcRouteOptionEnabled(true);
			if (config.getNode(tmp, "source-route-option") != null)
				screen.setIpSrcRouteOptionEnabled(true);		
			if (config.getNode(tmp, "unknown-protocol") != null)
				screen.setIpUnknownProtocolEnabled(true);		
		}
		
		tmp = config.getNode(node, "udp");
		if (tmp != null) {
			Node key = config.getNode(tmp, "flood");
			if (key != null) {
				screen.setUdpFloodEnabled(true);
				try {
					screen.setUdpFloodThreshold(Integer.parseInt(config.getString(key, "threshold/text()")));
				} catch (Exception e) { }
			}		
			
			key = config.getNode(tmp, "udp-sweep");
			if (key != null) {
				screen.setUdpSweepEnabled(true);
				try {
					screen.setUdpSweepThreshold(Integer.parseInt(config.getString(key, "threshold/text()")));
				} catch (Exception e) { }
			}				
		}
		
		tmp = config.getNode(node, "tcp");
		if (tmp != null) {
			Node key = config.getNode(tmp, "syn-flood");
			if (key != null) {
				screen.setTcpSynFloodEnabled(true);
				try {
					screen.setTcpSynFloodAttackThreshold(Integer.parseInt(config.getString(key, "attack-threshold/text()")));
				} catch (Exception e) { }
				try {
					screen.setTcpSynFloodAlarmThreshold(Integer.parseInt(config.getString(key, "alarm-threshold/text()")));
				} catch (Exception e) { }			
				try {
					screen.setTcpSynFloodSrcThreshold(Integer.parseInt(config.getString(key, "source-threshold/text()")));
				} catch (Exception e) { }						
				try {
					screen.setTcpSynFloodDstThreshold(Integer.parseInt(config.getString(key, "destination-threshold/text()")));
				} catch (Exception e) { }									
				try {
					screen.setTcpSynFloodTimeout(Integer.parseInt(config.getString(key, "timeout/text()")));
				} catch (Exception e) { }												
			}	
			
			if (config.getNode(tmp, "winnuke") != null)
				screen.setTcpWinnukeEnabled(true);
			
			key = config.getNode(tmp, "port-scan");
			if (key != null) {
				screen.setTcpPortScanEnabled(true);
				try {
					screen.setTcpPortScanThreshold(Integer.parseInt(config.getString(key, "threshold/text()")));
				} catch (Exception e) { }
			}		
		
			key = config.getNode(tmp, "tcp-sweep");
			if (key != null) {
				screen.setTcpSweepEnabled(true);
				try {
					screen.setTcpSweepThreshold(Integer.parseInt(config.getString(key, "threshold/text()")));
				} catch (Exception e) { }
			}		
		
			if (config.getNode(tmp, "land") != null)
				screen.setTcpLandEnabled(true);
			
			key = config.getNode(tmp, "syn-ack-ack-proxy");
			if (key != null) {
				screen.setTcpSynAckAckEnabled(true);
				try {
					screen.setTcpSynAckAckThreshold(Integer.parseInt(config.getString(key, "threshold/text()")));
				} catch (Exception e) { }
			}		
		
			if (config.getNode(tmp, "syn-frag") != null)
				screen.setTcpSynFragmentEnabled(true);
			if (config.getNode(tmp, "tcp-no-flag") != null)
				screen.setTcpNoFlagEnabled(true);		
			if (config.getNode(tmp, "syn-fin") != null)
				screen.setTcpSynAndFinEnabled(true);		
			if (config.getNode(tmp, "fin-no-ack") != null)
				screen.setTcpFinNoAckEnabled(true);		
		}
		
		tmp = config.getNode(node, "limit-session");
		if (tmp != null) {
			try {
				screen.setSrcLimitThreshold(Integer.parseInt(config.getString(tmp, "source-ip-based/text()")));
				screen.setSrcLimitEnabled(true);
			} catch (Exception e) { }		
			
			try {
				screen.setDstLimitThreshold(Integer.parseInt(config.getString(tmp, "destination-ip-based/text()")));
				screen.setDstLimitEnabled(true);
			} catch (Exception e) { }				
		}
		
		return screen;
	}
	
	public IDSOption(String name) {
		this.name = name;
		this.alarmWithoutDrop = false;
		this.icmpFloodEnabled = false;
		this.icmpFloodThreshold = ICMP_FLOOD_THRESHOLD_DEFAULT;
		this.udpFloodEnabled = false;
		this.udpFloodThreshold = UDP_FLOOD_THRESHOLD_DEFAULT;
		this.tcpSynFloodEnabled = false;
		this.tcpSynFloodAttackThreshold = TCP_SYN_FLOOD_ATTACK_THRESHOLD_DEFAULT;
		this.tcpSynFloodAlarmThreshold = TCP_SYN_FLOOD_ALARM_THRESHOLD_DEFAULT;
		this.tcpSynFloodSrcThreshold = TCP_SYN_FLOOD_SRC_THRESHOLD_DEFAULT;
		this.tcpSynFloodDstThreshold = TCP_SYN_FLOOD_DST_THRESHOLD_DEFAULT;
		this.tcpSynFloodTimeout = TCP_SYN_FLOOD_TIMEOUT_DEFAULT;
		this.tcpWinnukeEnabled = false;
		this.ipSpoofEnabled = false;
		this.ipSweepEnabled = false;
		this.ipSweepThreshold = IP_SWEEP_THRESHOLD_DEFAULT;
		this.tcpPortScanEnabled = false;
		this.tcpPortScanThreshold = TCP_PORT_SCAN_THRESHOLD_DEFAULT;
		this.tcpSweepEnabled = false;
		this.tcpSweepThreshold = TCP_SWEEP_THRESHOLD_DEFAULT;
		this.udpSweepEnabled = false;
		this.udpSweepThreshold = UDP_SWEEP_THRESHOLD_DEFAULT;
		this.icmpPingOfDeathEnabled = false;
		this.ipTeardropEnabled = false;
		this.icmpFragmentEnabled = false;
		this.icmpLargeEnabled = false;
		this.ipBlockFragment = false;
		this.tcpLandEnabled = false;
		this.tcpSynAckAckEnabled = false;
		this.tcpSynAckAckThreshold = TCP_SYN_ACK_ACK_THRESHOLD_DEFAULT;
		this.srcLimitEnabled = false;
		this.srcLimitThreshold = SRC_LIMIT_THRESHOLD_DEFAULT;
		this.dstLimitEnabled = false;
		this.dstLimitThreshold = DST_LIMIT_THRESHOLD_DEFAULT;
		this.ipBadOptionEnabled = false;
		this.ipTimestampOptionEnabled = false;
		this.ipSecurityOptionEnabled = false;
		this.ipStreamOptionEnabled = false;
		this.ipRecordRouteOptionEnabled = false;
		this.ipLooseSrcRouteOptionEnabled = false;
		this.ipStrictSrcRouteOptionEnabled = false;
		this.ipSrcRouteOptionEnabled = false;
		this.tcpSynFragmentEnabled = false;
		this.tcpNoFlagEnabled = false;
		this.tcpSynAndFinEnabled = false;
		this.tcpFinNoAckEnabled = false;
		this.ipUnknownProtocolEnabled = false;
	}

	private void config (Node node, DeviceConfig config) {
		if (alarmWithoutDrop)
			config.createElementsIfNotExist(node, "alarm-without-drop");
		else
			config.deleteNode(node, "alarm-without-drop");
		
		Node icmp = config.createElementsIfNotExist(node, "icmp");
		if (ipSweepEnabled) {
			Node key = config.createElementsIfNotExist(icmp, "ip-sweep");
			config.deleteNode(key, "threshold");
			key.appendChild(config.createElement("threshold", "%d", ipSweepThreshold));
		}
		else 
			config.deleteNode(icmp, "ip-sweep");

		if (icmpFragmentEnabled)
			config.createElementsIfNotExist(icmp, "fragment");
		else
			config.deleteNode(icmp, "fragment");
		
		if (icmpLargeEnabled)
			config.createElementsIfNotExist(icmp, "large");
		else
			config.deleteNode(icmp, "large");
		
		if (icmpFloodEnabled) {
			Node key = config.createElementsIfNotExist(icmp, "flood");
			config.deleteNode(key, "threshold");
			key.appendChild(config.createElement("threshold", "%d", icmpFloodThreshold));
		}
		else 
			config.deleteNode(icmp, "flood");

		if (icmpPingOfDeathEnabled)
			config.createElementsIfNotExist(icmp, "ping-death");
		else
			config.deleteNode(icmp, "ping-death");
		
		Node ip = config.createElementsIfNotExist(node, "ip");
		if (ipBadOptionEnabled)
			config.createElementsIfNotExist(ip, "bad-option");
		else
			config.deleteNode(ip, "bad-option");
		
		if (ipRecordRouteOptionEnabled)
			config.createElementsIfNotExist(ip, "record-route-option");
		else
			config.deleteNode(ip, "record-route-option");
	
		if (ipTimestampOptionEnabled)
			config.createElementsIfNotExist(ip, "timestamp-option");
		else
			config.deleteNode(ip, "timestamp-option");
	
		if (ipSecurityOptionEnabled)
			config.createElementsIfNotExist(ip, "security-option");
		else
			config.deleteNode(ip, "security-option");
	
		if (ipStreamOptionEnabled)
			config.createElementsIfNotExist(ip, "stream-option");
		else
			config.deleteNode(ip, "stream-option");
	
		if (ipSpoofEnabled)
			config.createElementsIfNotExist(ip, "spoofing");
		else
			config.deleteNode(ip, "spoofing");
		
		if (ipSrcRouteOptionEnabled)
			config.createElementsIfNotExist(ip, "source-route-option");
		else
			config.deleteNode(ip, "source-route-option");
	
		if (ipLooseSrcRouteOptionEnabled)
			config.createElementsIfNotExist(ip, "loose-source-route-option");
		else
			config.deleteNode(ip, "loose-source-route-option");
	
		if (ipStrictSrcRouteOptionEnabled)
			config.createElementsIfNotExist(ip, "strict-source-route-option");
		else
			config.deleteNode(ip, "strict-source-route-option");
	
		if (ipUnknownProtocolEnabled)
			config.createElementsIfNotExist(ip, "unknown-protocol");
		else
			config.deleteNode(ip, "unknown-protocol");
	
		if (ipBlockFragment)
			config.createElementsIfNotExist(ip, "block-frag");
		else
			config.deleteNode(ip, "block-frag");
	
		if (ipTeardropEnabled)
			config.createElementsIfNotExist(ip, "tear-drop");
		else
			config.deleteNode(ip, "tear-drop");
		
		Node tcp = config.createElementsIfNotExist(node, "tcp");
		if (tcpSynAndFinEnabled)
			config.createElementsIfNotExist(tcp, "syn-fin");
		else
			config.deleteNode(tcp, "syn-fin");
		
		if (tcpFinNoAckEnabled)
			config.createElementsIfNotExist(tcp, "fin-no-ack");
		else
			config.deleteNode(tcp, "fin-no-ack");
		
		if (tcpNoFlagEnabled)
			config.createElementsIfNotExist(tcp, "tcp-no-flag");
		else
			config.deleteNode(tcp, "tcp-no-flag");
		
		if (tcpSynFragmentEnabled)
			config.createElementsIfNotExist(tcp, "syn-frag");
		else
			config.deleteNode(tcp, "syn-frag");
		
		if (tcpPortScanEnabled) {
			Node key = config.createElementsIfNotExist(tcp, "port-scan");
			config.deleteNode(key, "threshold");
			key.appendChild(config.createElement("threshold", "%d", tcpPortScanThreshold));
		}
		else 
			config.deleteNode(tcp, "port-scan");
		
		if (tcpSynAckAckEnabled) {
			Node key = config.createElementsIfNotExist(tcp, "syn-ack-ack-proxy");
			config.deleteNode(key, "threshold");
			key.appendChild(config.createElement("threshold", "%d", tcpSynAckAckThreshold));
		}
		else 
			config.deleteNode(tcp, "syn-ack-ack-proxy");
		
		if (tcpSynFloodEnabled) {
			Node key = config.createElementsIfNotExist(tcp, "syn-flood");
			config.deleteNode(key, "alarm-threshold");
			key.appendChild(config.createElement("alarm-threshold", "%d", tcpSynFloodAlarmThreshold));
			config.deleteNode(key, "attack-threshold");
			key.appendChild(config.createElement("attack-threshold", "%d", tcpSynFloodAttackThreshold));
			config.deleteNode(key, "source-threshold");
			key.appendChild(config.createElement("source-threshold", "%d", tcpSynFloodSrcThreshold));
			config.deleteNode(key, "destination-threshold");
			key.appendChild(config.createElement("destination-threshold", "%d", tcpSynFloodDstThreshold));
			config.deleteNode(key, "timeout");
			key.appendChild(config.createElement("timeout", "%d", tcpSynFloodTimeout));
		}
		else 
			config.deleteNode(tcp, "syn-flood");
		
		if (tcpLandEnabled)
			config.createElementsIfNotExist(tcp, "land");
		else
			config.deleteNode(tcp, "land");
		
		if (tcpWinnukeEnabled)
			config.createElementsIfNotExist(tcp, "winnuke");
		else
			config.deleteNode(tcp, "winnuke");
		
		if (tcpSweepEnabled) {
			Node key = config.createElementsIfNotExist(tcp, "tcp-sweep");
			config.deleteNode(key, "threshold");
			key.appendChild(config.createElement("threshold", "%d", tcpSweepThreshold));
		}
		else 
			config.deleteNode(tcp, "tcp-sweep");

		Node udp = config.createElementsIfNotExist(node, "udp");
		if (udpFloodEnabled) {
			Node key = config.createElementsIfNotExist(udp, "flood");
			config.deleteNode(key, "threshold");
			key.appendChild(config.createElement("threshold", "%d", udpFloodThreshold));
		}
		else 
			config.deleteNode(udp, "flood");	
		
		if (udpSweepEnabled) {
			Node key = config.createElementsIfNotExist(udp, "udp-sweep");
			config.deleteNode(key, "threshold");
			key.appendChild(config.createElement("threshold", "%d", udpSweepThreshold));
		}
		else 
			config.deleteNode(udp, "udp-sweep");
	
		Node limit = config.createElementsIfNotExist(node, "limit-session");
		config.deleteNode(limit, "source-ip-based");
		if (srcLimitEnabled) 
			limit.appendChild(config.createElement("source-ip-based", "%d", srcLimitThreshold));

		config.deleteNode(limit, "destination-ip-based");
		if (dstLimitEnabled)
			limit.appendChild(config.createElement("destination-ip-based", "%d", dstLimitThreshold));
			
		if (config.getNode(ip, "*") == null)
			node.removeChild(ip);
		if (config.getNode(icmp, "*") == null)
			node.removeChild(icmp);
		if (config.getNode(tcp, "*") == null)
			node.removeChild(tcp);
		if (config.getNode(udp, "*") == null)
			node.removeChild(udp);
		if (config.getNode(limit, "*") == null)
			node.removeChild(limit);
	}
	
	public final boolean isIcmpFloodEnabled() {
		return icmpFloodEnabled;
	}

	public final void setIcmpFloodEnabled(boolean icmpFloodEnabled) {
		this.icmpFloodEnabled = icmpFloodEnabled;
	}

	public final int getIcmpFloodThreshold() {
		return icmpFloodThreshold;
	}

	public final void setIcmpFloodThreshold(int icmpFloodThreshold) {
		this.icmpFloodThreshold = icmpFloodThreshold;
	}

	public final boolean isUdpFloodEnabled() {
		return udpFloodEnabled;
	}

	public final void setUdpFloodEnabled(boolean udpFloodEnabled) {
		this.udpFloodEnabled = udpFloodEnabled;
	}

	public final int getUdpFloodThreshold() {
		return udpFloodThreshold;
	}

	public final void setUdpFloodThreshold(int udpFloodThreshold) {
		this.udpFloodThreshold = udpFloodThreshold;
	}

	public final boolean isTcpSynFloodEnabled() {
		return tcpSynFloodEnabled;
	}

	public final void setTcpSynFloodEnabled(boolean synFloodEnabled) {
		this.tcpSynFloodEnabled = synFloodEnabled;
	}

	public final int getTcpSynFloodAttackThreshold() {
		return tcpSynFloodAttackThreshold;
	}

	public final void setTcpSynFloodAttackThreshold(int synFloodAttackThreshold) {
		this.tcpSynFloodAttackThreshold = synFloodAttackThreshold;
	}

	public final int getTcpSynFloodAlarmThreshold() {
		return tcpSynFloodAlarmThreshold;
	}

	public final void setTcpSynFloodAlarmThreshold(int synFloodAlarmThreshold) {
		this.tcpSynFloodAlarmThreshold = synFloodAlarmThreshold;
	}

	public final int getTcpSynFloodSrcThreshold() {
		return tcpSynFloodSrcThreshold;
	}

	public final void setTcpSynFloodSrcThreshold(int synFloodSrcThreshold) {
		this.tcpSynFloodSrcThreshold = synFloodSrcThreshold;
	}

	public final int getTcpSynFloodDstThreshold() {
		return tcpSynFloodDstThreshold;
	}

	public final void setTcpSynFloodDstThreshold(int synFloodDstThreshold) {
		this.tcpSynFloodDstThreshold = synFloodDstThreshold;
	}

	public final int getTcpSynFloodTimeout() {
		return tcpSynFloodTimeout;
	}

	public final void setTcpSynFloodTimeout(int synFloodTimeout) {
		this.tcpSynFloodTimeout = synFloodTimeout;
	}

	public final boolean isTcpWinnukeEnabled() {
		return tcpWinnukeEnabled;
	}

	public final void setTcpWinnukeEnabled(boolean winnukeEnabled) {
		this.tcpWinnukeEnabled = winnukeEnabled;
	}

	public final boolean isIpSpoofEnabled() {
		return ipSpoofEnabled;
	}

	public final void setIpSpoofEnabled(boolean ipSpoofEnabled) {
		this.ipSpoofEnabled = ipSpoofEnabled;
	}

	public final boolean isIpSweepEnabled() {
		return ipSweepEnabled;
	}

	public final void setIpSweepEnabled(boolean ipSweepEnabled) {
		this.ipSweepEnabled = ipSweepEnabled;
	}

	public final int getIpSweepThreshold() {
		return ipSweepThreshold;
	}

	public final void setIpSweepThreshold(int ipSweepThreshold) {
		this.ipSweepThreshold = ipSweepThreshold;
	}

	public final boolean isTcpPortScanEnabled() {
		return tcpPortScanEnabled;
	}

	public final void setTcpPortScanEnabled(boolean portScanEnabled) {
		this.tcpPortScanEnabled = portScanEnabled;
	}

	public final int getTcpPortScanThreshold() {
		return tcpPortScanThreshold;
	}

	public final void setTcpPortScanThreshold(int portScanThreshold) {
		this.tcpPortScanThreshold = portScanThreshold;
	}

	public final boolean isTcpSweepEnabled() {
		return tcpSweepEnabled;
	}

	public final void setTcpSweepEnabled(boolean tcpSweepEnabled) {
		this.tcpSweepEnabled = tcpSweepEnabled;
	}

	public final int getTcpSweepThreshold() {
		return tcpSweepThreshold;
	}

	public final void setTcpSweepThreshold(int tcpSweepThreshold) {
		this.tcpSweepThreshold = tcpSweepThreshold;
	}

	public final boolean isUdpSweepEnabled() {
		return udpSweepEnabled;
	}

	public final void setUdpSweepEnabled(boolean udpSweepEnabled) {
		this.udpSweepEnabled = udpSweepEnabled;
	}

	public final int getUdpSweepThreshold() {
		return udpSweepThreshold;
	}

	public final void setUdpSweepThreshold(int udpSweepThreshold) {
		this.udpSweepThreshold = udpSweepThreshold;
	}

	public final boolean isIcmpPingOfDeathEnabled() {
		return icmpPingOfDeathEnabled;
	}

	public final void setIcmpPingOfDeathEnabled(boolean pingOfDeathEnabled) {
		this.icmpPingOfDeathEnabled = pingOfDeathEnabled;
	}

	public final boolean isIpTeardropEnabled() {
		return ipTeardropEnabled;
	}

	public final void setIpTeardropEnabled(boolean teardropEnabled) {
		this.ipTeardropEnabled = teardropEnabled;
	}

	public final boolean isIcmpFragmentEnabled() {
		return icmpFragmentEnabled;
	}

	public final void setIcmpFragmentEnabled(boolean icmpFragmentEnabled) {
		this.icmpFragmentEnabled = icmpFragmentEnabled;
	}

	public final boolean isIcmpLargeEnabled() {
		return icmpLargeEnabled;
	}

	public final void setIcmpLargeEnabled(boolean icmpLargeEnabled) {
		this.icmpLargeEnabled = icmpLargeEnabled;
	}

	public final boolean isIpBlockFragment() {
		return ipBlockFragment;
	}

	public final void setIpBlockFragment(boolean blockFragment) {
		this.ipBlockFragment = blockFragment;
	}

	public final boolean isTcpLandEnabled() {
		return tcpLandEnabled;
	}

	public final void setTcpLandEnabled(boolean landEnabled) {
		this.tcpLandEnabled = landEnabled;
	}

	public final boolean isTcpSynAckAckEnabled() {
		return tcpSynAckAckEnabled;
	}

	public final void setTcpSynAckAckEnabled(boolean synAckAckEnabled) {
		this.tcpSynAckAckEnabled = synAckAckEnabled;
	}

	public final int getTcpSynAckAckThreshold() {
		return tcpSynAckAckThreshold;
	}

	public final void setTcpSynAckAckThreshold(int synAckAckThreshold) {
		this.tcpSynAckAckThreshold = synAckAckThreshold;
	}

	public final boolean isSrcLimitEnabled() {
		return srcLimitEnabled;
	}

	public final void setSrcLimitEnabled(boolean srcLimitEnabled) {
		this.srcLimitEnabled = srcLimitEnabled;
	}

	public final int getSrcLimitThreshold() {
		return srcLimitThreshold;
	}

	public final void setSrcLimitThreshold(int srcLimitThreshold) {
		this.srcLimitThreshold = srcLimitThreshold;
	}

	public final boolean isDstLimitEnabled() {
		return dstLimitEnabled;
	}

	public final void setDstLimitEnabled(boolean dstLimitEnabled) {
		this.dstLimitEnabled = dstLimitEnabled;
	}

	public final int getDstLimitThreshold() {
		return dstLimitThreshold;
	}

	public final void setDstLimitThreshold(int dstLimitThreshold) {
		this.dstLimitThreshold = dstLimitThreshold;
	}

	public final boolean isIpBadOptionEnabled() {
		return ipBadOptionEnabled;
	}

	public final void setIpBadOptionEnabled(boolean ipBadOptionEnabled) {
		this.ipBadOptionEnabled = ipBadOptionEnabled;
	}

	public final boolean isIpTimestampOptionEnabled() {
		return ipTimestampOptionEnabled;
	}

	public final void setIpTimestampOptionEnabled(boolean ipTimestampOptionEnabled) {
		this.ipTimestampOptionEnabled = ipTimestampOptionEnabled;
	}

	public final boolean isIpSecurityOptionEnabled() {
		return ipSecurityOptionEnabled;
	}

	public final void setIpSecurityOptionEnabled(boolean ipSecurityOptionEnabled) {
		this.ipSecurityOptionEnabled = ipSecurityOptionEnabled;
	}

	public final boolean isIpStreamOptionEnabled() {
		return ipStreamOptionEnabled;
	}

	public final void setIpStreamOptionEnabled(boolean ipStreamOptionEnabled) {
		this.ipStreamOptionEnabled = ipStreamOptionEnabled;
	}

	public final boolean isIpRecordRouteOptionEnabled() {
		return ipRecordRouteOptionEnabled;
	}

	public final void setIpRecordRouteOptionEnabled(
			boolean ipRecordRouteOptionEnabled) {
		this.ipRecordRouteOptionEnabled = ipRecordRouteOptionEnabled;
	}

	public final boolean isIpLooseSrcRouteOptionEnabled() {
		return ipLooseSrcRouteOptionEnabled;
	}

	public final void setIpLooseSrcRouteOptionEnabled(
			boolean ipLooseSrcRouteOptionEnabled) {
		this.ipLooseSrcRouteOptionEnabled = ipLooseSrcRouteOptionEnabled;
	}

	public final boolean isIpStrictSrcRouteOptionEnabled() {
		return ipStrictSrcRouteOptionEnabled;
	}

	public final void setIpStrictSrcRouteOptionEnabled(
			boolean ipStrictSrcRouteOptionEnabled) {
		this.ipStrictSrcRouteOptionEnabled = ipStrictSrcRouteOptionEnabled;
	}

	public final boolean isIpSrcRouteOptionEnabled() {
		return ipSrcRouteOptionEnabled;
	}

	public final void setIpSrcRouteOptionEnabled(boolean ipSrcRouteOptionEnabled) {
		this.ipSrcRouteOptionEnabled = ipSrcRouteOptionEnabled;
	}

	public final boolean isTcpSynFragmentEnabled() {
		return tcpSynFragmentEnabled;
	}

	public final void setTcpSynFragmentEnabled(boolean synFragmentEnabled) {
		this.tcpSynFragmentEnabled = synFragmentEnabled;
	}

	public final boolean isTcpNoFlagEnabled() {
		return tcpNoFlagEnabled;
	}

	public final void setTcpNoFlagEnabled(boolean withoutFragmentEnabled) {
		this.tcpNoFlagEnabled = withoutFragmentEnabled;
	}

	public final boolean isTcpSynAndFinEnabled() {
		return tcpSynAndFinEnabled;
	}

	public final void setTcpSynAndFinEnabled(boolean synAndFinEnabled) {
		this.tcpSynAndFinEnabled = synAndFinEnabled;
	}

	public final boolean isTcpFinNoAckEnabled() {
		return tcpFinNoAckEnabled;
	}

	public final void setTcpFinNoAckEnabled(boolean finNoAckEnabled) {
		this.tcpFinNoAckEnabled = finNoAckEnabled;
	}

	public final boolean isIpUnknownProtocolEnabled() {
		return ipUnknownProtocolEnabled;
	}

	public final void setIpUnknownProtocolEnabled(boolean unknownProtocolEnabled) {
		this.ipUnknownProtocolEnabled = unknownProtocolEnabled;
	}


	public final boolean isAlarmWithoutDrop() {
		return alarmWithoutDrop;
	}

	public final void setAlarmWithoutDrop(boolean alarmWithoutDrop) {
		this.alarmWithoutDrop = alarmWithoutDrop;
	}
	
	public final String getName() {
		return name;
	}

	public DeviceConfigure getConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				IDSOption self = IDSOption.this;
				
				try {
					Node screenNode = config.screenNode();
					Node idsNode = config.findIDSOptionNode(screenNode, name);
					
					if (idsNode == null) {
						idsNode = config.createElement("ids-option");
						idsNode.appendChild(config.createElement("name", name));
						screenNode.appendChild(idsNode);
					}
					
					self.config(idsNode, config);
					
					if (config.getNodeList(idsNode, "*").getLength() == 1) {
						screenNode.removeChild(idsNode);
					}
				}
				catch (Exception e) {
					throw new DeviceConfigException("Failed to config IDS Option.", e);
				}
			}
			
		};
	}
	
	public DeviceConfigure getDeleteConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				Node screen = config.screenNode();
				
				Node n = config.findIDSOptionNode(screen, name);
				if (n != null) {
					screen.removeChild(n);
				}
			}
			
		};
	}
}
