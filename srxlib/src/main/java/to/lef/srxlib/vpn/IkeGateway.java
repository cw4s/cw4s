package to.lef.srxlib.vpn;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class IkeGateway {
	public static final String IKE_VERSION_1 = "v1-only";
	public static final String IKE_VERSION_2 = "v2-only";

	public static final String PEER_TYPE_STATIC = "static";
	public static final String PEER_TYPE_DYNAMIC_HOSTNAME = "hostname";
	public static final String PEER_TYPE_DYNAMIC_INET     = "inet";
	public static final String PEER_TYPE_DYNAMIC_INET6    = "inet6";
	public static final String PEER_TYPE_DYNAMIC_USER_AT_HOSTNAME = "user-at-hostname";
	
	public static final int DPD_INTERVAL_MIN = 10;
	public static final int DPD_INTERVAL_MAX = 60;
	
	public static final int DPD_THRESHOLD_MIN = 1;
	public static final int DPD_THRESHOLD_MAX = 5;

	public static final int NAT_KEEPALIVE_MIN = 1;
	public static final int NAT_KEEPALIVE_MAX = 300;
	
	private final String name;
	private String version;
	private String peerType;
	private String peer;
	private String localId;
	private String externalInterface;
	private String policyName;
	private boolean natTraversalEnabled;
	private int natKeepAlive;
	private int dpdInterval;
	private int dpdThreshold;
	private boolean dpdAlwaysSend;

	public static IkeGateway createFromNode(Node gw, DeviceConfig config) {
		String name = config.getString(gw, "name/text()");
		if (StringUtils.isBlank(name))
			return null;
		
		String version = config.getString(gw, "version/text()");
		String peerType = "";
		String peer = config.getString(gw, "address/text()");
		
		if (StringUtils.isNotBlank(peer)) {
			peerType = PEER_TYPE_STATIC;
		}
		else {
			peer = config.getString(gw, "dynamic/hostname/text()");
			if (StringUtils.isNotBlank(peer)) {
				peerType = PEER_TYPE_DYNAMIC_HOSTNAME;
			}
			else {
				peer = config.getString(gw, "dynamic/inet/text()");
				if (StringUtils.isNotBlank(peer)) {
					peerType = PEER_TYPE_DYNAMIC_INET;
				}
				else {
					peer = config.getString(gw, "dynamic/inet6/text()");
					if (StringUtils.isNotBlank(peer)) {
						peerType = PEER_TYPE_DYNAMIC_INET6;
					}
					else {
						peer = config.getString(gw, "user-at-hostname/inet6/text()");
						if (StringUtils.isNotBlank(peer)) {
							peerType = PEER_TYPE_DYNAMIC_USER_AT_HOSTNAME;
						}
					}
				}
			}
		}

		String externalInterface = config.getString(gw, "external-interface/text()");

		String localId = config.getString(gw, "local-identity/hostname/identity-hostname/text()");
		if (StringUtils.isBlank(localId)) {
			localId = config.getString(gw, "local-identity/inet/identity-ipv4/text()");
		}
		if (StringUtils.isBlank(localId)) {
			localId = config.getString(gw, "local-identity/inet6/identity-ipv6/text()");
		}
		if (StringUtils.isBlank(localId)) {
			localId = config.getString(gw, "local-identity/user-at-hostname/identity-user/text()");
		}
		
		String policyName = config.getString(gw, "ike-policy/text()");
		boolean natTravEnabled = (config.getNode(gw, "no-nat-traversal") == null) ? true : false;
		int natKeepAlive = 0;
		try {
			natKeepAlive = Integer.parseInt(config.getString(gw, "nat-keepalive/text()"));
		}
		catch (Exception e) {
			natKeepAlive = 0;
		}
		
		int dpdInterval = 0;
		int dpdThreshold = 0;
		boolean dpdAlwaysSend = false;
		
		Node dpd = config.getNode(gw, "dead-peer-detection");
		if (dpd != null) {
			dpdAlwaysSend = (config.getNode(dpd, "always-sen") != null) ? true : false;
			
			try {
				dpdInterval = Integer.parseInt(config.getString(dpd, "interval/text()"));
			}
			catch (Exception e) {
				dpdInterval = 0;
			}		
			
			try {
				dpdThreshold = Integer.parseInt(config.getString(dpd, "threshold/text()"));
			}
			catch (Exception e) {
				dpdThreshold = 0;
			}				
		}
		
		return new IkeGateway(name, version, peerType, peer, externalInterface, localId, policyName, natTravEnabled, natKeepAlive, dpdInterval, dpdThreshold, dpdAlwaysSend);
	}
	
	public IkeGateway(String name, String version, String peerType, String peer, String outgoingInterface, String localId, String policyName, boolean natTraversalEnabled, int natKeepAlive, int dpdInterval, int dpdThreshold, boolean dpdAlwaysSend) {
		this.name = name;
		this.version = version;
		this.peerType = peerType;
		this.peer = peer;
		this.localId = localId;
		this.externalInterface = outgoingInterface;
		this.policyName = policyName;
		this.natTraversalEnabled = natTraversalEnabled;
		this.natKeepAlive = natKeepAlive;
		this.dpdInterval = dpdInterval;
		this.dpdThreshold = dpdThreshold;
		this.dpdAlwaysSend = dpdAlwaysSend;
	}

	public final String getName() {
		return name;
	}

	public final String getVersion() {
		return version;
	}
	
	public final String getPeerType() {
		return peerType;
	}

	public final boolean isStaticPeer() {
		return PEER_TYPE_STATIC.equals(peerType);
	}
	
	public final boolean isDynamicPeer() {
		return (PEER_TYPE_DYNAMIC_HOSTNAME.equals(peerType) ||
				PEER_TYPE_DYNAMIC_INET.equals(peerType) ||
				PEER_TYPE_DYNAMIC_INET6.equals(peerType) ||
				PEER_TYPE_DYNAMIC_USER_AT_HOSTNAME.equals(peerType));
	}
	
	public final String getPeer() {
		return peer;
	}

	public final String getLocalId() {
		return localId;
	}

	public final String getExternalInterface() {
		return externalInterface;
	}

	public final String getPolicyName() {
		return policyName;
	}

	public String makeAndSetPolicyName(DeviceConfig config) {
		policyName = makePolicyName(config);
		return policyName;
	}
	
	public String makePolicyName(DeviceConfig config) {
		return makePolicyName(name, config);
	}

	public static String makePolicyName(String name, DeviceConfig config) {
		Node ike = config.ikeNode();
		String ret = name;
		
		if (config.getNode(ike, "policy[normalize-space(name/text())=%s]", ret) == null)
			return ret;
		
		for (int i = 0; ; i++) {
			ret = String.format("%s-%d", name, i);
			if (config.getNode(ike, "policy[normalize-space(name/text())=%s]", ret) == null)
				return ret;		
		}	
	}
	
	public final IkePolicy getPolicy(DeviceConfig config) {
		if (StringUtils.isBlank(policyName))
			return null;
		
		return config.findIkePolicy(policyName);
	}
	
	public final boolean isNatTraversalEnabled() {
		return natTraversalEnabled;
	}

	public final int getNatKeepAlive() {
		return natKeepAlive;
	}

	public final int getDpdInterval() {
		return dpdInterval;
	}

	public final int getDpdThreshold() {
		return dpdThreshold;
	}

	public final boolean isDpdAlwaysSend() {
		return dpdAlwaysSend;
	}

	protected void config(Node node, DeviceConfig config) {
		config.deleteNode(node, "ike-policy");
		if (StringUtils.isNotBlank(policyName))
			node.appendChild(config.createElement("ike-policy", policyName));
		
		config.deleteNode(node, "address");
		config.deleteNode(node, "dynamic");
		if (PEER_TYPE_STATIC.equals(peerType)) {
			if (StringUtils.isBlank(peer))
				throw new IllegalArgumentException("Invalid address.");
			node.appendChild(config.createElement("address", peer));
		}
		else {
			if (StringUtils.isBlank(peer))
				throw new IllegalArgumentException("Invalid peer ID.");		
			Node dynamic = config.createElement("dynamic");
			
			if (PEER_TYPE_DYNAMIC_HOSTNAME.equals(peerType)) {
				dynamic.appendChild(config.createElement(PEER_TYPE_DYNAMIC_HOSTNAME, peer));
			}
			else if (PEER_TYPE_DYNAMIC_INET.equals(peerType)) {
				dynamic.appendChild(config.createElement(PEER_TYPE_DYNAMIC_INET, peer));
			}
			else if (PEER_TYPE_DYNAMIC_INET6.equals(peerType)) {
				dynamic.appendChild(config.createElement(PEER_TYPE_DYNAMIC_INET6, peer));
			}
			else if (PEER_TYPE_DYNAMIC_USER_AT_HOSTNAME.equals(peerType)) {
				dynamic.appendChild(config.createElement(PEER_TYPE_DYNAMIC_USER_AT_HOSTNAME, peer));
			}
			else {
				throw new IllegalArgumentException("Invalid remote gateway type.");		
			}
			
			node.appendChild(dynamic);
		}
			
		
		Node dpd = config.createElementsIfNotExist(node, "dead-peer-detection");
		if (dpdAlwaysSend) 
			config.createElementsIfNotExist(dpd, "always-send");
		else
			config.deleteNode(dpd, "always-send");
		
		config.deleteNode(dpd, "interval");
		if (DPD_INTERVAL_MIN <= dpdInterval && dpdInterval <= DPD_INTERVAL_MAX) 
			dpd.appendChild(config.createElement("interval", "%d", dpdInterval));
	
		if (DPD_THRESHOLD_MIN <= dpdThreshold && dpdThreshold <= DPD_THRESHOLD_MAX)
			dpd.appendChild(config.createElement("threshold", "%d", dpdThreshold));
	
		if (config.getNode(dpd, "*") == null)
			node.removeChild(dpd);
		
		if (natTraversalEnabled) 
			config.deleteNode(node, "no-nat-traversal");
		else
			config.createElementsIfNotExist(node, "no-nat-traversal");
		
		config.deleteNode(node, "nat-keepalive");
		if (NAT_KEEPALIVE_MIN <= natKeepAlive && natKeepAlive <= NAT_KEEPALIVE_MAX) 
			node.appendChild(config.createElement("nat-keepalive", "%d", natKeepAlive));
		
		config.deleteNode(node, "local-identity");
		if (StringUtils.isNotBlank(localId)) {
			Node lid = config.createElement("local-identity");
		
			if (PEER_TYPE_DYNAMIC_INET.equals(peerType)) {
				Node inet = config.createElement("inet");
				inet.appendChild(config.createElement("identity-ipv4", localId));
				lid.appendChild(inet);
			}
			else if (PEER_TYPE_DYNAMIC_INET6.equals(peerType)) {
				Node inet = config.createElement("inet6");
				inet.appendChild(config.createElement("identity-ipv6", localId));
				lid.appendChild(inet);			
			}
			else if (PEER_TYPE_DYNAMIC_USER_AT_HOSTNAME.equals(peerType)) {
				Node uh = config.createElement("user-at-hostname");
				uh.appendChild(config.createElement("identity-user", localId));
				lid.appendChild(uh);
			}
			else {
				Node host = config.createElement("hostname");
				host.appendChild(config.createElement("identity-hostname", localId));
				lid.appendChild(host);			
			}
			node.appendChild(lid);
		}
		
		config.deleteNode(node, "external-interface");
		if (StringUtils.isNotBlank(externalInterface))
			node.appendChild(config.createElement("external-interface", externalInterface));
		
		config.deleteNode(node, "version");
		if (IKE_VERSION_1.equals(version) || IKE_VERSION_2.equals(version))
			node.appendChild(config.createElement("version", version));
	}
	
	public DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				IkeGateway self = IkeGateway.this;
				
				try {
					Node ikeNode = config.ikeNode();
					Node gw = config.findIkeGatewayNode(ikeNode, name);
					
					if (gw != null) {
						throw new IllegalStateException("IKE gateway is already exist.");
					}
					
					gw = config.createElement("gateway");
					gw.appendChild(config.createElement("name", name));
					ikeNode.appendChild(gw);
					
					self.config(gw, config);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IKE gateway.", e);
				}
			}
			
		};
	}
	
	public DeviceConfigure getEditConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				IkeGateway self = IkeGateway.this;
				
				try {
					Node gw = config.findIkeGatewayNode(name);
					
					if (gw == null) {
						throw new IllegalStateException("IKE gateway is not exist.");
					}
					
					self.config(gw, config);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IKE gateway.", e);
				}
			}
			
		};
	}
	
	public DeviceConfigure getDeleteConfigure(final Boolean deletePolicy) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					if (isRemovable(config)) {
						Node ikeNode = config.ikeNode();
						Node node = config.findIkeGatewayNode(ikeNode, getName());
						IkePolicy policy = getPolicy(config);
						
						if (node != null) { 
							ikeNode.removeChild(node);
						
							if (config.getNode(ikeNode, "*") == null) {
								ikeNode.getParentNode().removeChild(ikeNode);
							}	
							else if (deletePolicy && policy != null && policy.isRemovable(config)) {
								policy.getDeleteConfigure().config(config, info);
							}
						}
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IKE Gateway.", e);
				}
			}
			
		};
	}
		
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((peer == null) ? 0 : peer.hashCode());
		result = prime * result + ((peerType == null) ? 0 : peerType.hashCode());
		result = prime * result + (dpdAlwaysSend ? 1231 : 1237);
		result = prime * result + dpdInterval;
		result = prime * result + dpdThreshold;
		result = prime * result + ((localId == null) ? 0 : localId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + natKeepAlive;
		result = prime * result + (natTraversalEnabled ? 1231 : 1237);
		result = prime * result + ((externalInterface == null) ? 0 : externalInterface.hashCode());
		result = prime * result + ((policyName == null) ? 0 : policyName.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof IkeGateway)) {
			return false;
		}
		IkeGateway other = (IkeGateway) obj;
		if (StringUtils.isBlank(peer)) {
			if (StringUtils.isNotBlank(other.peer)) {
				return false;
			}
		} else if (!peer.equals(other.peer)) {
			return false;
		}
		if (StringUtils.isBlank(peerType)) {
			if (StringUtils.isNotBlank(other.peerType)) {
				return false;
			}
		} else if (!peer.equals(other.peerType)) {
			return false;
		}	
		if (dpdAlwaysSend != other.dpdAlwaysSend) {
			return false;
		}
		if (dpdInterval != other.dpdInterval) {
			return false;
		}
		if (dpdThreshold != other.dpdThreshold) {
			return false;
		}
		if (StringUtils.isBlank(localId)) {
			if (StringUtils.isNotBlank(other.localId)) {
				return false;
			}
		} else if (!localId.equals(other.localId)) {
			return false;
		}
		if (StringUtils.isBlank(name)) {
			if (StringUtils.isNotBlank(other.name)) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (natKeepAlive != other.natKeepAlive) {
			return false;
		}
		if (natTraversalEnabled != other.natTraversalEnabled) {
			return false;
		}
		if (StringUtils.isBlank(externalInterface)) {
			if (StringUtils.isNotBlank(other.externalInterface)) {
				return false;
			}
		} else if (!externalInterface.equals(other.externalInterface)) {
			return false;
		}
		if (StringUtils.isBlank(policyName)) {
			if (StringUtils.isNotBlank(other.policyName)) {
				return false;
			}
		} else if (!policyName.equals(other.policyName)) {
			return false;
		}
		if (StringUtils.isBlank(version)) {
			if (StringUtils.isNotBlank(other.version)) {
				return false;
			}
		} else if (!version.equals(other.version)) {
			return false;
		}
		return true;
	}
	
	public boolean isInUse(DeviceConfig config) {
		for (Node policy : IterableNodeList.apply(config.getNodeList(config.ipsecNode(), "vpn"))) {
			if  (config.getNode(policy, "ike/gateway[normalize-space(text())=%s]", XML.xpc(name)) != null) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isRemovable(DeviceConfig config) {
		if (isInUse(config))
			return false;
		
		return true;
	}
	
	public int isChanged(Map<String, IkeGateway> candidate) {
        IkeGateway c = candidate.get(getName());

        if (c == null)
        	return 1;
        if (!equals(c))
        	return 2;

        return 0;
	}
}
