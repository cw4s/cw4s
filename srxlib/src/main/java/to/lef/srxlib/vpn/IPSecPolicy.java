package to.lef.srxlib.vpn;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class IPSecPolicy {
	public static final String PROPOSAL_SET_STANDARD   = "standard";
	public static final String PROPOSAL_SET_COMPATIBLE = "compatible";
	public static final String PROPOSAL_SET_BASIC      = "basic";
	
	public static final String PFS_KEYS_GROUP1  = "group1";
	public static final String PFS_KEYS_GROUP2  = "group2";
	public static final String PFS_KEYS_GROUP5  = "group5";
	public static final String PFS_KEYS_GROUP14 = "group14";

	private final String name;
	private String pfsKeys;
	private String proposalSet;
	private List<String> proposalNameList;

	public static IPSecPolicy createFromNode(Node node, DeviceConfig config) {
		String name = config.getString(node, "name/text()");
		if (StringUtils.isBlank(name))
			return null;
		
		String pfs = config.getString(node, "perfect-forward-secrecy/keys/text()");
		String propSet = config.getString(node, "proposal-set/text()");
	
		IPSecPolicy ret = new IPSecPolicy(name, pfs, propSet);

		if (StringUtils.isBlank(propSet)) {
			for (Node n : IterableNodeList.apply(config.getNodeList(node, "proposals"))) {
				String propName = config.getString(n, "text()");
				if (StringUtils.isNotBlank(propName)) 
					ret.addProposalName(propName);
			}
		}
		
		return ret;
	}
	
	public IPSecPolicy(String name, String pfsKeys, String proposalSet) {
		this.name = name;
		this.pfsKeys = pfsKeys;
		this.proposalSet = proposalSet;
		this.proposalNameList = new ArrayList<>();
	}

	public final String getName() {
		return name;
	}
	
	public final String getPfsKeys() {
		return pfsKeys;
	}

	public final String getProposalSet() {
		return proposalSet;
	}
	
	public final List<String> getProposalNameList() {
		return Collections.unmodifiableList(proposalNameList);
	}

	public final String getProposalName(int index) {
		try {
			return proposalNameList.get(index);
		}
		catch (Exception e) {
			return null;
		}
	}
	
	public final IPSecProposal getProposal(int index, DeviceConfig config) {
		try {
			String propName = proposalNameList.get(index);
			return config.findIPSecProposal(propName);
		}
		catch (Exception e) {
			return null;
		}
	}

	public void addProposalName(String name) {
		if (StringUtils.isBlank(name))
			return;
		
		if (proposalNameList.contains(name))
			return;
		
		proposalNameList.add(name);
	}

	protected void config(Node n, DeviceConfig config) {
		config.deleteNode(n, "perfect-forward-secrecy");
		if (StringUtils.isNotBlank(pfsKeys)) {
			Node pfs = config.createElement("perfect-forward-secrecy");
			pfs.appendChild(config.createElement("keys", pfsKeys));
			n.insertBefore(pfs, config.getNode(n, "proposals"));
		}
		
		config.deleteNode(n, "proposal-set");
		config.deleteNode(n, "proposals");
		if (PROPOSAL_SET_STANDARD.equals(proposalSet) ||
			PROPOSAL_SET_BASIC.equals(proposalSet) ||
			PROPOSAL_SET_COMPATIBLE.equals(proposalSet)) {

			n.appendChild(config.createElement("proposal-set", proposalSet));
		}
		else {
			for (String name: proposalNameList) {
				if (StringUtils.isNotBlank(name)) {
					n.appendChild(config.createElement("proposals", name));
				}
			}
		}
	}
	
	public DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				IPSecPolicy self = IPSecPolicy.this;
				try {
					Node ipsec = config.ipsecNode();
					Node n = config.findIPSecPolicyNode(ipsec, name);
					
					if (n != null) {
						throw new IllegalStateException("IPSec Policy is already exist.");
					}
				
					n = config.createElement("policy");
					n.appendChild(config.createElement("name", name));
					ipsec.appendChild(n);
					
					self.config(n, config);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IPSec Policy.", e);
				}
			}
			
		};
	}
	
	public DeviceConfigure getEditConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				IPSecPolicy self = IPSecPolicy.this;
				
				try {
					Node ipsec = config.ipsecNode();
					Node n = config.findIPSecPolicyNode(ipsec, name);
					
					if (n == null) {
						throw new IllegalStateException("IPSec Policy is not exist.");
					}
					
					self.config(n, config);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IPSec Policy.", e);
				}
			}
			
		};
	}
	
	public DeviceConfigure getDeleteConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					if (isRemovable(config)) {
						Node ipsecNode = config.ipsecNode();
						Node node = config.findIPSecPolicyNode(ipsecNode, getName());
						if (node != null) { 
							ipsecNode.removeChild(node);
							
							if (config.getNode(ipsecNode, "*") == null) 
								ipsecNode.getParentNode().removeChild(ipsecNode);
						}
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IKE Policy.", e);
				}
			}
			
		};
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((pfsKeys == null) ? 0 : pfsKeys.hashCode());
		result = prime * result + ((proposalNameList == null) ? 0 : proposalNameList.hashCode());
		result = prime * result + ((proposalSet == null) ? 0 : proposalSet.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof IPSecPolicy)) {
			return false;
		}
		IPSecPolicy other = (IPSecPolicy) obj;
		if (StringUtils.isBlank(name)) {
			if (StringUtils.isNotBlank(other.name)) {
				return false;
			}
		} else if (!pfsKeys.equals(other.name)) {
			return false;
		}	
		if (StringUtils.isBlank(pfsKeys)) {
			if (StringUtils.isNotBlank(other.pfsKeys)) {
				return false;
			}
		} else if (!pfsKeys.equals(other.pfsKeys)) {
			return false;
		}
		if (proposalNameList == null) {
			if (other.proposalNameList != null) {
				return false;
			}
		} else if (!(other.proposalNameList != null && proposalNameList.size() == other.proposalNameList.size() && proposalNameList.containsAll(other.proposalNameList))) {
			return false;
		}
		if (StringUtils.isBlank(proposalSet)) {
			if (StringUtils.isNotBlank(other.proposalSet)) {
				return false;
			}
			
			if (proposalNameList == null) {
				if (other.proposalNameList != null) {
					return false;
				}
			} else if (!(other.proposalNameList != null && proposalNameList.size() == other.proposalNameList.size() && proposalNameList.containsAll(other.proposalNameList))) {
				return false;
			}		
		} else if (!proposalSet.equals(other.proposalSet)) {
			return false;
		}

		return true;
	}
	
	public boolean isInUse(DeviceConfig config) {
		for (Node vpn : IterableNodeList.apply(config.getNodeList(config.ipsecNode(), "vpn"))) {
			if  (config.getNode(vpn, "ike[normalize-space(ipsec-policy/text())=%s]", XML.xpc(name)) != null) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isRemovable(DeviceConfig config) {
		if (isInUse(config))
			return false;
		
		return true;
	}
	
}
