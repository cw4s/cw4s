package to.lef.srxlib.vpn;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class IkeProposal {
	public static final String AUTH_METHOD_PRESHARED = "pre-shared-keys";
	public static final String AUTH_METHOD_DSA       = "dsa-signatures";
	public static final String AUTH_METHOD_RSA       = "rsa-signatures";
	public static final String AUTH_ALG_MD5     = "md5";
	public static final String AUTH_ALG_SHA_256 = "sha-256";
	public static final String AUTH_ALG_SHA1    = "sha1";
	public static final String DH_GROUP_1   = "group1";
	public static final String DH_GROUP_2   = "group2";
	public static final String DH_GROUP_5   = "group5";
	public static final String DH_GROUP_14  = "group14";
	public static final String ENC_ALG_3DES_CBC    = "3des-cbc";
	public static final String ENC_ALG_AES_CBC_128 = "aes-128-cbc";
	public static final String ENC_ALG_AES_CBC_192 = "aes-192-cbc";
	public static final String ENC_ALG_AES_CBC_256 = "aes-256-cbc";
	public static final String ENC_ALG_DES_CBC     = "des-cbc";
	public static final int LIFETIME_MIN = 180;
	public static final int LIFETIME_MAX = 86400;
	public static final int LIFETIME_DEFAULT = 3600;
	
	private final String name;
	private String authenticationMethod;
	private String authenticationAlgorithm;
	private String dhGroup;
	private String encryptionAlgorithm;
	private int lifeTime;

	public static IkeProposal createFromNode(Node prop, DeviceConfig config) {
		String name = config.getString(prop, "name/text()");
		if (StringUtils.isBlank(name))
			return null;
	
		String authMethod = config.getString(prop, "authentication-method/text()");
		String dhGroup = config.getString(prop, "dh-group/text()");
		String authAlg = config.getString(prop, "authentication-algorithm/text()");
		String encAlg = config.getString(prop, "encryption-algorithm/text()");
		int lifeTime = 0;
		
		try {
			lifeTime = Integer.parseInt(config.getString(prop, "lifetime-seconds/text()"));
		}
		catch (Exception e) {
			lifeTime = 0;
		}
		
		return new IkeProposal(name, authMethod, authAlg, dhGroup, encAlg, lifeTime);
	}
	
	public IkeProposal(String name, String authenticationMethod, String authenticationAlgorithm, String dhGroup, String encryptionAlgorithm, int lifeTime) {
		this.name = name;
		this.authenticationMethod = authenticationMethod;
		this.authenticationAlgorithm = authenticationAlgorithm;
		this.dhGroup = dhGroup;
		this.encryptionAlgorithm = encryptionAlgorithm;
		this.lifeTime = lifeTime;
	}
	
	public final String getName() {
		return name;
	}
	public final String getAuthenticationMethod() {
		return authenticationMethod;
	}
	public final String getAuthenticationAlgorithm() {
		return authenticationAlgorithm;
	}
	public final String getDhGroup() {
		return dhGroup;
	}
	public final String getEncryptionAlgorithm() {
		return encryptionAlgorithm;
	}
	public final int getLifeTime() {
		return lifeTime;
	}

	protected void config(Node prop, DeviceConfig config) {
		config.deleteNode(prop, "authentication-method");
		if (StringUtils.isNotBlank(authenticationMethod))
			prop.appendChild(config.createElement("authentication-method", authenticationMethod));
		
		config.deleteNode(prop, "dh-group");
		if (StringUtils.isNotBlank(dhGroup))
			prop.appendChild(config.createElement("dh-group", dhGroup));
		
		config.deleteNode(prop, "authentication-algorithm");
		if (StringUtils.isNotBlank(authenticationAlgorithm))
			prop.appendChild(config.createElement("authentication-algorithm", authenticationAlgorithm));
		
		config.deleteNode(prop, "encryption-algorithm");
		if (StringUtils.isNotBlank(encryptionAlgorithm))
			prop.appendChild(config.createElement("encryption-algorithm", encryptionAlgorithm));
		
		config.deleteNode(prop, "lifetime-seconds");
		if (LIFETIME_MIN <= lifeTime && lifeTime <= LIFETIME_MAX) {
			prop.appendChild(config.createElement("lifetime-seconds", "%d", getLifeTime()));
		} 
	}
	
	public DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				IkeProposal self = IkeProposal.this;
				try {
					Node ike = config.ikeNode();
					Node n = config.findIkeProposalNode(ike, name);
					
					if (n != null) {
						throw new IllegalStateException("IKE Proposal is already exist.");
					}
				
					n = config.createElement("proposal");
					n.appendChild(config.createElement("name", name));
					ike.appendChild(n);
					
					self.config(n, config);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IKE Proposal.", e);
				}
			}
			
		};
	}

	public DeviceConfigure getEditConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				IkeProposal self = IkeProposal.this;
				try {
					Node ike = config.ikeNode();
					Node n = config.findIkeProposalNode(ike, name);
					
					if (n == null) {
						throw new IllegalStateException("IKE Proposal is not exist.");
					}
				
					self.config(n, config);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IKE Proposal.", e);
				}			
			}
			
		};
	}

	public DeviceConfigure getDeleteConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					if (isRemovable(config)) {
						Node ikeNode = config.ikeNode();
						Node node = config.findIkeProposalNode(ikeNode, getName());
						if (node != null) { 
							ikeNode.removeChild(node);
							
							if (config.getNode(ikeNode, "*") == null) 
								ikeNode.getParentNode().removeChild(ikeNode);
						}
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IKE Proposal.", e);
				}
			}
			
		};
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authenticationAlgorithm == null) ? 0 : authenticationAlgorithm.hashCode());
		result = prime * result + ((authenticationMethod == null) ? 0 : authenticationMethod.hashCode());
		result = prime * result + ((dhGroup == null) ? 0 : dhGroup.hashCode());
		result = prime * result + ((encryptionAlgorithm == null) ? 0 : encryptionAlgorithm.hashCode());
		result = prime * result + lifeTime;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof IkeProposal)) {
			return false;
		}
		IkeProposal other = (IkeProposal) obj;
		if (StringUtils.isBlank(authenticationAlgorithm)) {
			if (StringUtils.isNotBlank(other.authenticationAlgorithm)) {
				return false;
			}
		} else if (!authenticationAlgorithm.equals(other.authenticationAlgorithm)) {
			return false;
		}
		if (StringUtils.isBlank(authenticationMethod)) {
			if (StringUtils.isNotBlank(other.authenticationMethod)) {
				return false;
			}
		} else if (!authenticationMethod.equals(other.authenticationMethod)) {
			return false;
		}
		if (StringUtils.isBlank(dhGroup)) {
			if (StringUtils.isNotBlank(other.dhGroup)) {
				return false;
			}
		} else if (!dhGroup.equals(other.dhGroup)) {
			return false;
		}
		if (encryptionAlgorithm == null) {
			if (other.encryptionAlgorithm != null) {
				return false;
			}
		} else if (!encryptionAlgorithm.equals(other.encryptionAlgorithm)) {
			return false;
		}
		if (lifeTime != other.lifeTime) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	public boolean isInUse(DeviceConfig config) {
		for (Node policy : IterableNodeList.apply(config.getNodeList(config.ikeNode(), "policy"))) {
			if  (config.getNode(policy, "proposals[normalize-space(text())=%s]", XML.xpc(name)) != null) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isRemovable(DeviceConfig config) {
		if (isInUse(config))
			return false;
		
		return true;
	}
	
	public int isChanged(Map<String, IkeProposal> candidate) {
        IkeProposal c = candidate.get(getName());

        if (c == null)
                return 1;
        if (!equals(c))
                return 2;

        return 0;
	}

}
