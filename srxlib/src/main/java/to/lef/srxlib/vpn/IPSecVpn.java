package to.lef.srxlib.vpn;

import java.net.UnknownHostException;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.Util;

public class IPSecVpn {
	public static class ProxyId {
		String localAddress;
		int localMask;
		String remoteAddress;
		int remoteMask;
		String serviceName;
		
		public static ProxyId createFromNode(Node node, DeviceConfig config) {
			if (node == null)
				return null;
			
			String service = config.getString(node, "service");
			
			String localAddr = null;
			int localMask = 0;
			
			String remoteAddr = null;
			int remoteMask = 0;	
			
			try {
				String[] local = StringUtils.split(config.getString(node, "local"), '/');
				localAddr = Util.fixInetAddressString(local[0]);
				localMask = Integer.parseInt(local[1]);
			}
			catch (Exception e) {
				localAddr = null;
				localMask = 0;
			}
			
			try {
				String[] remote = StringUtils.split(config.getString(node, "remote"), '/');
				remoteAddr = Util.fixInetAddressString(remote[0]);
				remoteMask = Integer.parseInt(remote[1]);
			}
			catch (Exception e) {
				remoteAddr = null;
				remoteMask = 0;
			}
			
			return new ProxyId(localAddr, localMask, remoteAddr, remoteMask, service);
		}
		
		public ProxyId(String localAddress, int localMask, String remoteAddress, int remoteMask, String serviceName) {
			try {
				this.localAddress = Util.fixInetAddressString(localAddress);
			}
			catch (UnknownHostException e) {
				this.localAddress = null;
			}
			this.localMask = localMask;
			try {
				this.remoteAddress = Util.fixInetAddressString(remoteAddress);
			} catch (UnknownHostException e) {
				this.remoteAddress = null;
			}
			this.remoteMask = remoteMask;
			this.serviceName = serviceName;
		}

		public final String getLocalAddress() {
			return localAddress;
		}

		public final int getLocalMask() {
			return localMask;
		}

		public final String getRemoteAddress() {
			return remoteAddress;
		}

		public final int getRemoteMask() {
			return remoteMask;
		}

		public final String getServiceName() {
			return serviceName;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((localAddress == null) ? 0 : localAddress.hashCode());
			result = prime * result + localMask;
			result = prime * result + ((remoteAddress == null) ? 0 : remoteAddress.hashCode());
			result = prime * result + remoteMask;
			result = prime * result + ((serviceName == null) ? 0 : serviceName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof ProxyId)) {
				return false;
			}
			ProxyId other = (ProxyId) obj;
			if (StringUtils.isBlank(localAddress)) {
				if (StringUtils.isNotBlank(other.localAddress)) {
					return false;
				}
			} else if (!localAddress.equals(other.localAddress)) {
				return false;
			}
			if (localMask != other.localMask) {
				return false;
			}
			if (StringUtils.isBlank(remoteAddress)) {
				if (StringUtils.isNotBlank(other.remoteAddress)) {
					return false;
				}
			} else if (!remoteAddress.equals(other.remoteAddress)) {
				return false;
			}
			if (remoteMask != other.remoteMask) {
				return false;
			}
			if (StringUtils.isBlank(serviceName)) {
				if (StringUtils.isNotBlank(other.serviceName)) {
					return false;
				}
			} else if (!serviceName.equals(other.serviceName)) {
				return false;
			}
			return true;
		}
	}
	
	private String name;
	private String gatewayName;
	private String policyName;
	private String bindInterfaceName;
	private boolean antiReplayDisabled;
	private boolean monitorEnabled;
	private String monitorDestinationIP;
	private boolean monitorOptimized;
	private String monitorSourceInterfaceName;
	private ProxyId proxyId;
	private boolean establishImmediately;
	
	public static IPSecVpn createFromNode(Node node, DeviceConfig config) {
		String name = config.getString(node, "name/text()");
		if (StringUtils.isBlank(name))
			return null;
		
		String bInt = config.getString(node, "bind-interface/text()");
		
		boolean monitorEnabled   = false;
		boolean monitorOptimized = false;
		String monitorSrcInt     = null;
		String monitorDst        = null;
		Node monitor = config.getNode(node, "vpn-monitor");
		if (monitor != null) {
			monitorEnabled = true;
			
			monitorOptimized = (config.getNode(monitor, "optimized") != null) ? true : false;
			monitorSrcInt    = config.getString(monitor, "source-interface/text()");
			monitorDst       = config.getString(monitor, "destination-ip/text()");
		}
		
		String gwName = null;
		String policyName = null;
		boolean antiReplayDisabled = false;
		ProxyId proxyId = null;
		Node ike = config.getNode(node, "ike");
		if (ike != null) {
			gwName = config.getString(ike, "gateway/text()");
			antiReplayDisabled = (config.getNode(ike, "no-anti-replay") != null) ? true : false;
			policyName = config.getString(ike, "ipsec-policy/text()");

			Node pid = config.getNode(ike, "proxy-identity");
			if (pid != null) {
				proxyId = ProxyId.createFromNode(pid, config);
			}
		}

		boolean establishImmediately = "immediately".equals(config.getString(node, "establish-tunnels/text()"));
		
		return new IPSecVpn(name, gwName, policyName, bInt, antiReplayDisabled, establishImmediately, monitorEnabled, monitorDst, monitorOptimized, monitorSrcInt, proxyId);
	}
	
	public IPSecVpn(String name, String gatewayName, String policyName,
			String bindInterfaceName, boolean antiReplayDisabled, boolean establishImmediately,
			boolean vpnMonitorEnabled, String vpnMonitorDestinationIP,
			boolean vpnMonitorOptimized, String vpnMonitorSourceInterface, ProxyId proxyId) {
		this.name = name;
		this.gatewayName = gatewayName;
		this.policyName = policyName;
		this.bindInterfaceName = bindInterfaceName;
		this.antiReplayDisabled = antiReplayDisabled;
		this.establishImmediately = establishImmediately;
		this.monitorEnabled = vpnMonitorEnabled;
		this.monitorDestinationIP = vpnMonitorDestinationIP;
		this.monitorOptimized = vpnMonitorOptimized;
		this.monitorSourceInterfaceName = vpnMonitorSourceInterface;
		this.proxyId = proxyId;
	}

	public final String getName() {
		return name;
	}

	public final String getGatewayName() {
		return gatewayName;
	}

	public final String getPolicyName() {
		return policyName;
	}
	
	public String makeAndSetPolicyName(DeviceConfig config) {
		policyName = makePolicyName(config);
		return policyName;
	}
	
	public String makePolicyName(DeviceConfig config) {
		return makePolicyName(name, config);
	}

	public static String makePolicyName(String name, DeviceConfig config) {
		Node ipsec = config.ipsecNode();
		String ret = name;
		
		if (config.getNode(ipsec, "policy[normalize-space(name/text())=%s]", ret) == null)
			return ret;
		
		for (int i = 0; ; i++) {
			ret = String.format("%s-%d", name, i);
			if (config.getNode(ipsec, "policy[normalize-space(name/text())=%s]", ret) == null)
				return ret;		
		}	
	}
	
	public final IPSecPolicy getPolicy(DeviceConfig config) {
		if (StringUtils.isBlank(policyName))
			return null;
		
		return config.findIPSecPolicy(policyName);
	}
	public final String getBindInterfaceName() {
		return bindInterfaceName;
	}

	public final boolean isAntiReplayDisabled() {
		return antiReplayDisabled;
	}

	public final boolean isEstablishImmediately() {
		return establishImmediately;
	}
	
	public final boolean isMonitorEnabled() {
		return monitorEnabled;
	}

	public final String getMonitorDestinationIP() {
		return monitorDestinationIP;
	}

	public final boolean isMonitorOptimized() {
		return monitorOptimized;
	}

	public final String getMonitorSourceInterfaceName() {
		return monitorSourceInterfaceName;
	}

	public final ProxyId getProxyId() {
		return proxyId;
	}
	
	protected void config(Node vpn, DeviceConfig config) {
		config.deleteNode(vpn, "bind-interface");
		vpn.appendChild(config.createElement("bind-interface", bindInterfaceName));
	
		if (monitorEnabled) {
			Node monitor = config.createElementsIfNotExist(vpn, "vpn-monitor");
			if (monitorOptimized) 
				config.createElementsIfNotExist(monitor, "optimized");
			else 
				config.deleteNode(monitor, "optimized");
			
			config.deleteNode(monitor, "source-interface");
			if (StringUtils.isNotBlank(monitorSourceInterfaceName))
				monitor.appendChild(config.createElement("source-interface", monitorSourceInterfaceName));
			
			config.deleteNode(monitor, "destination-ip");
			if (StringUtils.isNotBlank(monitorDestinationIP))
				monitor.appendChild(config.createElement("destination-ip", monitorDestinationIP));
			
			if (config.getNode(monitor, "*") == null)
				vpn.removeChild(monitor);
		}
		else {
			config.deleteNode(vpn, "vpn-monitor");
		}
		
		Node ike = config.createElementsIfNotExist(vpn, "ike");
		config.deleteNode(ike, "gateway");
		if (StringUtils.isNotBlank(gatewayName))
			ike.appendChild(config.createElement("gateway", gatewayName));
		
		if (antiReplayDisabled) 
			config.createElementsIfNotExist(ike, "no-anti-replay");
		else
			config.deleteNode(ike, "no-anti-replay");
	
		config.deleteNode(ike, "ipsec-policy");
		if (StringUtils.isNotBlank(policyName))
			ike.appendChild(config.createElement("ipsec-policy", policyName));
	
		if (config.getNode(ike, "*") == null)
			vpn.removeChild(ike);
		
		config.deleteNode(vpn, "establish-tunnels");
		if (establishImmediately)
			vpn.appendChild(config.createElement("establish-tunnels", "immediately"));
	}
	
	public DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				IPSecVpn self = IPSecVpn.this;
				
				try {
					Node ipsec = config.ipsecNode();
					Node vpn = config.findIPSecVpnNode(ipsec, name);
					
					if (vpn != null) {
						throw new IllegalStateException("IPSec VPN is already exist.");
					}
					
					vpn = config.createElement("vpn");
					vpn.appendChild(config.createElement("name", name));
					ipsec.appendChild(vpn);
					
					self.config(vpn, config);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IPSec VPN.", e);
				}
			}
			
		};
	}
	
	public DeviceConfigure getEditConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				IPSecVpn self = IPSecVpn.this;
				
				try {
					Node vpn = config.findIPSecVpnNode(name);
					
					if (vpn == null) {
						throw new IllegalStateException("IPSec VPN is not exist.");
					}
					
					self.config(vpn, config);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IPSec VPN.", e);
				}
			}
			
		};
	}
	
	public DeviceConfigure getDeleteConfigure(final Boolean deletePolicy) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					if (isRemovable(config)) {
						Node ipsecNode = config.ipsecNode();
						Node node = config.findIPSecVpnNode(ipsecNode, getName());
						IPSecPolicy policy = getPolicy(config);
						
						if (node != null) { 
							ipsecNode.removeChild(node);
						
							if (config.getNode(ipsecNode, "*") == null) {
								ipsecNode.getParentNode().removeChild(ipsecNode);
							}	
							else if (deletePolicy && policy != null && policy.isRemovable(config)) {
								policy.getDeleteConfigure().config(config, info);
							}
						}
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IKE Gateway.", e);
				}
			}
			
		};
	}
	
	protected void configProxyId(Node vpn, ProxyId proxyId, DeviceConfig config) {
		Node ike = config.createElementsIfNotExist(vpn, "ike");
		config.deleteNode(ike, "proxy-identity");
		if (proxyId != null && 
			StringUtils.isNotBlank(proxyId.getLocalAddress()) &&
			StringUtils.isNotBlank(proxyId.getRemoteAddress()) &&
			StringUtils.isNotBlank(proxyId.getServiceName())) {

			Node lid = config.createElement("proxy-identity");
			config.deleteNode(lid, "local");
			lid.appendChild(config.createElement("local", "%s/%d", proxyId.getLocalAddress(), proxyId.getLocalMask()));
			
			config.deleteNode(lid, "remote");
			lid.appendChild(config.createElement("remote", "%s/%d", proxyId.getRemoteAddress(), proxyId.getRemoteMask()));		
			
			config.deleteNode(lid, "service");
			lid.appendChild(config.createElement("service", proxyId.getServiceName()));
			
			ike.appendChild(lid);
		} 
	}
	
	public DeviceConfigure getEditProxyIdConfigure(final ProxyId proxyId) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				IPSecVpn self = IPSecVpn.this;
				
				try {
					Node vpn = config.findIPSecVpnNode(name);
					
					if (vpn == null) {
						throw new IllegalStateException("IPSec VPN is not exist.");
					}
					
					self.configProxyId(vpn, proxyId, config);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IPSec VPN proxy id.", e);
				}			
			}
			
		};
	}

	public DeviceConfigure getDeleteProxyIdConfigure() {
		return new DeviceConfigure() {
			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					Node vpn = config.findIPSecVpnNode(name);

					if (vpn == null) {
						throw new IllegalStateException("IPSec VPN is not exist.");
					}

					config.deleteNode(vpn, "ike/proxy-identity");
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IPSec VPN proxy id.", e);
				}	
			}
		};
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (antiReplayDisabled ? 1231 : 1237);
		result = prime * result + (establishImmediately ? 1231 : 1237);
		result = prime * result + ((bindInterfaceName == null) ? 0 : bindInterfaceName.hashCode());
		result = prime * result + ((gatewayName == null) ? 0 : gatewayName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((policyName == null) ? 0 : policyName.hashCode());
		result = prime * result + ((monitorDestinationIP == null) ? 0 : monitorDestinationIP.hashCode());
		result = prime * result + (monitorEnabled ? 1231 : 1237);
		result = prime * result + (monitorOptimized ? 1231 : 1237);
		result = prime * result + ((monitorSourceInterfaceName == null) ? 0 : monitorSourceInterfaceName.hashCode());
		result = prime * result + ((proxyId == null) ? 0 : proxyId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof IPSecVpn)) {
			return false;
		}
		IPSecVpn other = (IPSecVpn) obj;
		if (antiReplayDisabled != other.antiReplayDisabled) {
			return false;
		}
		if (establishImmediately != other.establishImmediately) {
			return false;
		}
		if (StringUtils.isBlank(bindInterfaceName)) {
			if (StringUtils.isNotBlank(other.bindInterfaceName)) {
				return false;
			}
		} else if (!bindInterfaceName.equals(other.bindInterfaceName)) {
			return false;
		}
		if (StringUtils.isBlank(gatewayName)) {
			if (StringUtils.isNotBlank(other.gatewayName)) {
				return false;
			}
		} else if (!gatewayName.equals(other.gatewayName)) {
			return false;
		}
		if (StringUtils.isBlank(name)) {
			if (StringUtils.isNotBlank(other.name)) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (StringUtils.isBlank(policyName)) {
			if (StringUtils.isNotBlank(other.policyName)) {
				return false;
			}
		} else if (!policyName.equals(other.policyName)) {
			return false;
		}
		if (StringUtils.isBlank(monitorDestinationIP)) {
			if (StringUtils.isNotBlank(other.monitorDestinationIP)) {
				return false;
			}
		} else if (!monitorDestinationIP.equals(other.monitorDestinationIP)) {
			return false;
		}
		if (monitorEnabled != other.monitorEnabled) {
			return false;
		}
		if (monitorOptimized != other.monitorOptimized) {
			return false;
		}
		if (StringUtils.isBlank(monitorSourceInterfaceName)) {
			if (StringUtils.isNotBlank(other.monitorSourceInterfaceName)) {
				return false;
			}
		} else if (!monitorSourceInterfaceName.equals(other.monitorSourceInterfaceName)) {
			return false;
		}
		if (proxyId == null) {
			if (other.proxyId != null) {
				return false;
			}
		}
		else if (!proxyId.equals(other.proxyId)) {
			return false;
		}
		
		return true;
	}
	
	public boolean isRemovable(DeviceConfig config) {
		return true;
	}
	
	public int isChanged(Map<String, IPSecVpn> candidate) {
        IPSecVpn c = candidate.get(getName());

        if (c == null)
        	return 1;
        if (!equals(c))
        	return 2;

        return 0;
	}
}
