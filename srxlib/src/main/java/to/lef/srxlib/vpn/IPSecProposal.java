package to.lef.srxlib.vpn;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class IPSecProposal {
	public static final String AUTH_ALG_HMAC_MD5_96      = "hmac-md5-96";
	public static final String AUTH_ALG_HMAC_SHA_256_128 = "hmac-sha-256-128";
	public static final String AUTH_ALG_HMAC_SHA_256_96  = "hmac-sha-256-96";
	public static final String AUTH_ALG_HMAC_SHA1_96     = "hmac-sha1-96";
	
	public static final String ENC_ALG_3DES_CBC    = "3des-cbc";
	public static final String ENC_ALG_AES_CBC_128 = "aes-128-cbc";
	public static final String ENC_ALG_AES_CBC_192 = "aes-192-cbc";
	public static final String ENC_ALG_AES_CBC_256 = "aes-256-cbc";
	public static final String ENC_ALG_DES_CBC     = "des-cbc";

	public static final String PROTOCOL_AH  = "ah";
	public static final String PROTOCOL_ESP = "esp";
	
	public static final int LIFETIME_SECONDS_MIN = 180;
	public static final int LIFETIME_SECONDS_MAX = 86400;
	public static final int LIFETIME_SECONDS_DEFAULT = 28800;

	public static final long LIFETIME_KILOBYTES_MIN = 64L;
	public static final long LIFETIME_KILOBYTES_MAX = 4294967294L;
		
	private final String name;
	private String protocol;
	private String authenticationAlgorithm;
	private String encryptionAlgorithm;
	private int lifetimeSeconds;
	private long lifetimeKilobytes;

	public static IPSecProposal createFromNode(Node node, DeviceConfig config) {
		String name = config.getString(node, "name/text()");
		if (StringUtils.isBlank(name))
			return null;
		
		String protocol = config.getString(node, "protocol/text()");
		String authAlg = config.getString(node, "authentication-algorithm/text()");
		String encAlg = config.getString(node, "encryption-algorithm/text()");
		int lifetime = 0;
		try {
			lifetime = Integer.parseInt(config.getString(node, "lifetime-seconds/text()"));
		}
		catch (Exception e) {
			lifetime = 0;
		}
		long lifesize = 0L;
		try {
			lifesize = Long.parseLong(config.getString(node, "lifetime-kilobytes/text()"));
		} 
		catch (Exception e) {
			lifesize = 0L;
		}
		
		return new IPSecProposal(name, protocol, authAlg, encAlg, lifetime, lifesize);
	}
	
	public IPSecProposal(String name, String protocol, String authenticationAlgorithm, String encryptionAlgorithm, int lifetimeSeconds, long lifetimeKilobytes) {
		this.name = name;
		this.protocol = protocol;
		this.authenticationAlgorithm = authenticationAlgorithm;
		this.encryptionAlgorithm = encryptionAlgorithm;
		this.lifetimeSeconds = lifetimeSeconds;
		this.lifetimeKilobytes = lifetimeKilobytes;
	}

	public final String getName() {
		return name;
	}

	public final String getProtocol() {
		return protocol;
	}

	public final String getAuthenticationAlgorithm() {
		return authenticationAlgorithm;
	}

	public final String getEncryptionAlgorithm() {
		return encryptionAlgorithm;
	}

	public final int getLifetimeSeconds() {
		return lifetimeSeconds;
	}

	public final long getLifetimeKilobytes() {
		return lifetimeKilobytes;
	}

	protected void config(Node n, DeviceConfig config) {
		config.deleteNode(n, "protocol");
		if (StringUtils.isNotBlank(protocol)) {
			n.appendChild(config.createElement("protocol", protocol));
		
			config.deleteNode(n, "authentication-algorithm");
			if (StringUtils.isBlank(authenticationAlgorithm)) {
				throw new IllegalArgumentException("Invalid authentication algorithm.");
			}
			n.appendChild(config.createElement("authentication-algorithm", authenticationAlgorithm));

			config.deleteNode(n, "encryption-algorithm"); 
			if (PROTOCOL_ESP.equals(protocol)) {
				if (StringUtils.isBlank(encryptionAlgorithm)) {
					throw new IllegalArgumentException("Invalid encryption algorithm.");
				}
				n.appendChild(config.createElement("encryption-algorithm", encryptionAlgorithm));
			}
		}
		config.deleteNode(n, "lifetime-seconds");	
		if (LIFETIME_SECONDS_MIN <= lifetimeSeconds && lifetimeSeconds <= LIFETIME_SECONDS_MAX)
			n.appendChild(config.createElement("lifetime-seconds", "%d", lifetimeSeconds));
		
		config.deleteNode(n, "lifetime-kilobytes");	
		if (LIFETIME_KILOBYTES_MIN <= lifetimeKilobytes && lifetimeKilobytes <= LIFETIME_KILOBYTES_MAX)
			n.appendChild(config.createElement("lifetime-kilobytes", "%d", lifetimeKilobytes));	
	}
	
	public DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				IPSecProposal self = IPSecProposal.this;
				try {
					Node ipsec = config.ipsecNode();
					Node n = config.findIPSecProposalNode(ipsec, name);
					
					if (n != null) {
						throw new IllegalStateException("IPSec Proposal is already exist.");
					}
				
					n = config.createElement("proposal");
					n.appendChild(config.createElement("name", name));
					ipsec.insertBefore(n, config.getNode(ipsec, "policy"));
					
					self.config(n, config);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IPSec Proposal.", e);
				}
			}
			
		};
	}

	public DeviceConfigure getEditConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				IPSecProposal self = IPSecProposal.this;
				try {
					Node ipsec = config.ipsecNode();
					Node n = config.findIPSecProposalNode(ipsec, name);
					
					if (n == null) {
						throw new IllegalStateException("IPSec Proposal is not exist.");
					}
					
					self.config(n, config);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IPSec Proposal.", e);
				}
			}
			
		};
	}
	
	public DeviceConfigure getDeleteConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					if (isRemovable(config)) {
						Node ipsecNode = config.ipsecNode();
						Node node = config.findIPSecProposalNode(ipsecNode, getName());
						if (node != null) { 
							ipsecNode.removeChild(node);
							
							if (config.getNode(ipsecNode, "*") == null) 
								ipsecNode.getParentNode().removeChild(ipsecNode);
						}
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IPSec Proposal.", e);
				}
			}
			
		};
	}
		
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authenticationAlgorithm == null) ? 0 : authenticationAlgorithm.hashCode());
		result = prime * result + ((encryptionAlgorithm == null) ? 0 : encryptionAlgorithm.hashCode());
		result = prime * result + (int) (lifetimeKilobytes ^ (lifetimeKilobytes >>> 32));
		result = prime * result + lifetimeSeconds;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((protocol == null) ? 0 : protocol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof IPSecProposal)) {
			return false;
		}
		IPSecProposal other = (IPSecProposal) obj;
		if (StringUtils.isBlank(authenticationAlgorithm)) {
			if (StringUtils.isNotBlank(other.authenticationAlgorithm)) {
				return false;
			}
		} else if (!authenticationAlgorithm.equals(other.authenticationAlgorithm)) {
			return false;
		}
		if (StringUtils.isBlank(encryptionAlgorithm)) {
			if (StringUtils.isNotBlank(other.encryptionAlgorithm)) {
				return false;
			}
		} else if (!encryptionAlgorithm.equals(other.encryptionAlgorithm)) {
			return false;
		}
		if (lifetimeKilobytes != other.lifetimeKilobytes) {
			return false;
		}
		if (lifetimeSeconds != other.lifetimeSeconds) {
			return false;
		}
		if (StringUtils.isBlank(name)) {
			if (StringUtils.isNotBlank(other.name)) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (StringUtils.isBlank(protocol)) {
			if (StringUtils.isNotBlank(other.protocol)) {
				return false;
			}
		} else if (!protocol.equals(other.protocol)) {
			return false;
		}
		return true;
	}

	public boolean isInUse(DeviceConfig config) {
		for (Node policy : IterableNodeList.apply(config.getNodeList(config.ipsecNode(), "policy"))) {
			if  (config.getNode(policy, "proposals[normalize-space(text())=%s]", XML.xpc(name)) != null) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isRemovable(DeviceConfig config) {
		if (isInUse(config))
			return false;
		
		return true;
	}
	
	public int isChanged(Map<String, IPSecProposal> candidate) {
        IPSecProposal c = candidate.get(getName());

        if (c == null)
                return 1;
        if (!equals(c))
                return 2;

        return 0;
	}
}
