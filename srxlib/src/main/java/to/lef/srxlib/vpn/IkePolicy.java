package to.lef.srxlib.vpn;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class IkePolicy {
	public static final String PROPOSAL_SET_STANDARD   = "standard";
	public static final String PROPOSAL_SET_COMPATIBLE = "compatible";
	public static final String PROPOSAL_SET_BASIC      = "basic";
	
	public static final String MODE_MAIN       = "main";
	public static final String MODE_AGGRESSIVE = "aggressive";
	
	private final String name;
	private String proposalSet;
	private String mode;
	private String presharedKey;
	private List<String> proposalNameList;

	public static IkePolicy createFromNode(Node policy, DeviceConfig config) {
		String name = config.getString(policy, "name/text()");
		if (StringUtils.isBlank(name))
			return null;
		
		String mode    = config.getString(policy, "mode/text()");
		String key     = config.getString(policy, "pre-shared-key/ascii-text/text()");
		String propSet = config.getString(policy, "proposal-set/text()");
		
		IkePolicy ret = new IkePolicy(name, mode, key, propSet);
	
		if (StringUtils.isBlank(propSet)) {
			for (Node prop : IterableNodeList.apply(config.getNodeList(policy, "proposals"))) {
				String propName = config.getString(prop, "text()");
				ret.addProposalName(propName);
			}
		}
		
		return ret;
	}
	
	public IkePolicy(String name, String mode, String presharedKey, String proposalSet) {
		this.name = name;
		this.proposalSet = proposalSet;
		this.mode = mode;
		this.presharedKey = presharedKey;
		this.proposalNameList = new ArrayList<>();
	}

	public final String getProposalSet() {
		return proposalSet;
	}

	public final String getMode() {
		return mode;
	}

	public final String getPresharedKey() {
		return presharedKey;
	}

	public final List<String> getProposalNameList() {
		return Collections.unmodifiableList(proposalNameList);
	}

	public final void addProposalName(String name) {
		if (StringUtils.isBlank(name))
			return;
		if (proposalNameList.contains(name))
			return;
		
		proposalNameList.add(name);
	}
	
	public final String getName() {
		return name;
	}

	protected void config(Node node, DeviceConfig config) {
		config.deleteNode(node, "mode");
		if (StringUtils.isNotBlank(mode)) 
			node.appendChild(config.createElement("mode", mode));

		config.deleteNode(node, "proposal-set");
		config.deleteNode(node, "proposals");
		if (PROPOSAL_SET_STANDARD.equals(proposalSet) ||
			PROPOSAL_SET_BASIC.equals(proposalSet) ||
			PROPOSAL_SET_COMPATIBLE.equals(proposalSet)) {
			
			node.appendChild(config.createElement("proposal-set", proposalSet));
		}
		else {
			for (String propName: proposalNameList) {
				node.appendChild(config.createElement("proposals", propName));
			}
		}
		
		config.deleteNode(node, "pre-shared-key");
		if (StringUtils.isNotBlank(presharedKey)) {
			Node psk = config.createElement("pre-shared-key");
			psk.appendChild(config.createElement("ascii-text", presharedKey));
			node.appendChild(psk);
		}
	}
	
	public DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				IkePolicy self = IkePolicy.this;
				
				try {
					Node ikeNode = config.ikeNode();
					Node policy = config.findIkePolicyNode(ikeNode, name);
					
					if (policy != null) {
						throw new IllegalStateException("IKE policy is already exist.");
					}
					
					policy = config.createElement("policy");
					policy.appendChild(config.createElement("name", name));
					ikeNode.appendChild(policy);
					
					self.config(policy, config);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IKE policy.", e);
				}
			}
			
		};
	}

	public DeviceConfigure getEditConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				IkePolicy self = IkePolicy.this;
				
				try {
					Node policy = config.findIkePolicyNode(name);
					
					if (policy == null) {
						throw new IllegalStateException("IKE policy is not exist.");
					}
					
					self.config(policy, config);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IKE policy.", e);
				}
			}
			
		};	
	}
	
	public DeviceConfigure getDeleteConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					if (isRemovable(config)) {
						Node ikeNode = config.ikeNode();
						Node node = config.findIkePolicyNode(ikeNode, getName());
						if (node != null) { 
							ikeNode.removeChild(node);
							
							if (config.getNode(ikeNode, "*") == null) 
								ikeNode.getParentNode().removeChild(ikeNode);
						}
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config IKE Policy.", e);
				}
			}
			
		};
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mode == null) ? 0 : mode.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((presharedKey == null) ? 0 : presharedKey.hashCode());
		result = prime * result + ((proposalNameList == null) ? 0 : proposalNameList.hashCode());
		result = prime * result + ((proposalSet == null) ? 0 : proposalSet.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof IkePolicy)) {
			return false;
		}
		IkePolicy other = (IkePolicy) obj;
		if (StringUtils.isBlank(mode)) {
			if (StringUtils.isNotBlank(other.mode)) {
				return false;
			}
		} else if (!mode.equals(other.mode)) {
			return false;
		}
		if (StringUtils.isBlank(name)) {
			if (StringUtils.isNotBlank(other.name)) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (StringUtils.isBlank(presharedKey)) {
			if (StringUtils.isNotBlank(other.presharedKey)) {
				return false;
			}
		} else if (!presharedKey.equals(other.presharedKey)) {
			return false;
		}

		if (StringUtils.isBlank(proposalSet)) {
			if (StringUtils.isNotBlank(other.proposalSet)) {
				return false;
			}
			
			if (proposalNameList == null) {
				if (other.proposalNameList != null) {
					return false;
				}
			} else if (!(other.proposalNameList != null && proposalNameList.size() == other.proposalNameList.size() && proposalNameList.containsAll(other.proposalNameList))) {
				return false;
			}
		} else if (!proposalSet.equals(other.proposalSet)) {
			return false;
		}
		return true;
	}
	
	public boolean isInUse(DeviceConfig config) {
		for (Node gateway : IterableNodeList.apply(config.getNodeList(config.ikeNode(), "gateway"))) {
			if  (config.getNode(gateway, "ike-policy[normalize-space(text())=%s]", XML.xpc(name)) != null) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isRemovable(DeviceConfig config) {
		if (isInUse(config))
			return false;
		
		return true;
	}
}
