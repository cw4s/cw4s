package to.lef.srxlib;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class UsersInformation extends XML {
	private List<Node> nodeList;
	
	public static class UserEntry {
		private String name;
		private String tty;
		private String from;
		private Date loginDate;
		public UserEntry(String name, String tty, String from, Date loginTime) {
			this.name = name;
			this.tty = tty;
			this.from = from;
			this.loginDate = loginTime;
		}
		public final String getName() {
			return name;
		}
		public final String getTty() {
			return tty;
		}
		public final String getFrom() {
			return from;
		}
		public final Date getLoginDate() {
			return loginDate;
		}
	}
	
	public UsersInformation(Document doc) {
		super(doc);
		
		nodeList = new ArrayList<>();
		for (Node n : IterableNodeList.apply(getNodeList("//system-users-information"))) {
			nodeList.add(n);
		}
		
		if (nodeList.size() == 0) 
			throw new IllegalArgumentException("Invalid user information.");
	}

	public final int getActiveUserCount() {
		int ret = 0;
	
		for (Node n : nodeList) {
			String v = getString(n, "uptime-information/active-user-count/text()");
			if (StringUtils.isNumeric(v))
				ret += Integer.parseInt(v);
		}
		
		return ret;
	}
	
	public List<UserEntry> getUserList() {
		List<UserEntry> ret = new ArrayList<UserEntry>();
	
		for (Node no: nodeList) {
			for (Node n: IterableNodeList.apply(getNodeList(no, "uptime-information/user-table/user-entry"))) {
				try {
					String name = getString(n, "user/text()");
					String tty  = getString(n, "tty/text()");
					String from = getString(n, "from/text()");
					Date   loginTime = new Date(Long.parseLong(getString(n, "login-time/@seconds")) * 1000);

					UserEntry u = new UserEntry(name, tty, from, loginTime);
					ret.add(u);
				}
				catch (NumberFormatException e) {
					e.printStackTrace();
				}

			}

			for (Node n: IterableNodeList.apply(getNodeList(no, "web-management-users/web-management-user"))) {
				try {
					String name = getString(n, "user/text()");
					String tty  = getString(n, "tty/text()");
					String from = getString(n, "from/text()");
					Date   loginTime = new Date(Long.parseLong(getString(n, "login-time/@seconds")) * 1000);

					UserEntry u = new UserEntry(name, tty, from, loginTime);
					ret.add(u);
				}
				catch (NumberFormatException e) {
					e.printStackTrace();
				}		
			}
		}
		
		return Collections.unmodifiableList(ret);
	}
}
