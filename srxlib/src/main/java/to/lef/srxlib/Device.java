package to.lef.srxlib;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import net.juniper.netconf.CommitException;
import net.juniper.netconf.LoadException;
import net.juniper.netconf.NetconfException;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.nic.*;
import to.lef.srxlib.syslog.SyslogMessage;

public class Device extends net.juniper.netconf.Device implements DeviceInformationProvider {
	public final static int DEFAULT_PORT = 830;
	public final static String CACHE_NAME_CANDIDATE_CONFIG         = "candidate_config";
	public final static String CACHE_NAME_RUNNING_CONFIG           = "running_config";
	public final static String CACHE_NAME_EDIT_CONFIG              = "edit_config";
	public final static String CACHE_NAME_ROUTE_INFORMATION        = "route_information";
//	public final static String CACHE_NAME_INTERFACE_INFORMATION    = "interface_information_terse";
	public final static String CACHE_NAME_INTERFACE_INFORMATION    = "interface_information_brief";
	public final static String CACHE_NAME_ZONES_INFORMATION        = "zones_information";
	public final static String CACHE_NAME_SOFTWARE_INFORMATION     = "software_information";
	public final static String CACHE_NAME_CHASSIS_INVENTORY        = "chassis_inventory";
	public final static String CACHE_NAME_USERS_INFORMATION        = "system_users_information";
	public final static String CACHE_NAME_ROUTE_ENGINE_INFORMATION = "route_engine_information";
	public final static String CACHE_NAME_UPTIME_INFORMATION       = "system_uptime_information";
	public final static String CACHE_NAME_FLOW_SESSION_INFORMATION = "flow_session_information_summary";
	public final static String CACHE_NAME_JUNOS_DEFAULTS           = "junos_defaults";
	public final static String CACHE_NAME_SESSION_LOG              = "session_log";
	public final static String CACHE_NAME_MESSAGES_LOG             = "messages_log";
	public final static String CACHE_NAME_ALARM_INFORMATION        = "alarm_information";
	public final static String CACHE_NAME_SYSTEM_ALARM_INFORMATION = "system_alarm_information";
	public final static String CACHE_NAME_PPP_SUMMARY_INFORMATION  = "ppp_summary_information";
	public final static String CACHE_NAME_IPSEC_SECURITY_ASSOCIATIONS = "ipsec_security_associations";
	public final static String CACHE_NAME_IPSEC_INACTIVE_TUNNELS      = "ipsec_inactive_tunnels";
	
//	protected final static String COMMAND_INTERFACE_INFORMATION     = "<get-interface-information><terse/></get-interface-information>";
	protected final static String COMMAND_INTERFACE_INFORMATION     = "<get-interface-information><brief/></get-interface-information>";
	protected final static String COMMAND_ZONES_INFORMATION         = "get-zones-information";
	protected final static String COMMAND_ROUTE_INFORMATION         = "<get-route-information><detail /></get-route-information>";
	protected final static String COMMAND_SOFTWARE_INFORMATION      = "get-software-information";
	protected final static String COMMAND_USERS_INFORMATION         = "get-system-users-information";
	protected final static String COMMAND_ROUTE_ENGINE_INFORMATION  = "get-route-engine-information";
	protected final static String COMMAND_CHASSIS_INVENTORY         = "get-chassis-inventory";
	protected final static String COMMAND_UPTIME_INFORMATION        = "get-system-uptime-information";
	protected final static String COMMAND_FLOW_SESSION_INFORMATION  = "<get-flow-session-information><summary/></get-flow-session-information>";
	protected final static String COMMAND_CANDIDATE_CONFIG          = "<get-config><source><candidate/></source></get-config>";
	protected final static String COMMAND_RUNNING_CONFIG            = "<get-config><source><running/></source></get-config>";
	protected final static String COMMAND_JUNOS_DEFAULTS            = "show configuration groups junos-defaults";
	protected final static String COMMAND_SESSION_LOG               = "show log %s";
	protected final static String COMMAND_MESSAGES_LOG              = "show log messages";
	protected final static String COMMAND_ALARM_INFORMATION         = "get-alarm-information";
	protected final static String COMMAND_SYSTEM_ALARM_INFORMATION  = "get-system-alarm-information";
	protected final static String COMMAND_PPP_SUMMARY_INFORMATION   = "get-ppp-summary-information";
	protected final static String COMMAND_IPSEC_SECURITY_ASSOCIATIONS = "<get-security-associations-information><detail/></get-security-associations-information>";
	protected final static String COMMAND_IPSEC_INACTIVE_TUNNELS      = "<get-inactive-tunnels><detail/></get-inactive-tunnels>";

	private final static Map<String, String> cacheCommandMap = new HashMap<String, String>() {
		private static final long serialVersionUID = 5599753301371137895L;

		{
			put(CACHE_NAME_CANDIDATE_CONFIG, COMMAND_CANDIDATE_CONFIG);
			put(CACHE_NAME_RUNNING_CONFIG, COMMAND_RUNNING_CONFIG);
			put(CACHE_NAME_ROUTE_INFORMATION, COMMAND_ROUTE_INFORMATION);
			put(CACHE_NAME_INTERFACE_INFORMATION, COMMAND_INTERFACE_INFORMATION);
			put(CACHE_NAME_ZONES_INFORMATION, COMMAND_ZONES_INFORMATION);
			put(CACHE_NAME_SOFTWARE_INFORMATION, COMMAND_SOFTWARE_INFORMATION);
			put(CACHE_NAME_CHASSIS_INVENTORY, COMMAND_CHASSIS_INVENTORY);
			put(CACHE_NAME_USERS_INFORMATION, COMMAND_USERS_INFORMATION);
			put(CACHE_NAME_ROUTE_ENGINE_INFORMATION, COMMAND_ROUTE_ENGINE_INFORMATION);
			put(CACHE_NAME_UPTIME_INFORMATION, COMMAND_UPTIME_INFORMATION);
			put(CACHE_NAME_FLOW_SESSION_INFORMATION, COMMAND_FLOW_SESSION_INFORMATION);
			put(CACHE_NAME_JUNOS_DEFAULTS, COMMAND_JUNOS_DEFAULTS);
			put(CACHE_NAME_MESSAGES_LOG, COMMAND_MESSAGES_LOG);
			put(CACHE_NAME_ALARM_INFORMATION, COMMAND_ALARM_INFORMATION);
			put(CACHE_NAME_SYSTEM_ALARM_INFORMATION, COMMAND_SYSTEM_ALARM_INFORMATION);
			put(CACHE_NAME_PPP_SUMMARY_INFORMATION, COMMAND_PPP_SUMMARY_INFORMATION);
			put(CACHE_NAME_SESSION_LOG, COMMAND_SESSION_LOG);
			put(CACHE_NAME_IPSEC_SECURITY_ASSOCIATIONS, COMMAND_IPSEC_SECURITY_ASSOCIATIONS);
			put(CACHE_NAME_IPSEC_INACTIVE_TUNNELS, COMMAND_IPSEC_INACTIVE_TUNNELS);
		}
	};
	
	public static final Set<String> cacheNameMap() {
		return cacheCommandMap.keySet();
	}
	
	private final String loginName;
	private final File cacheDir;
	private final XPath xpath;
	private final DocumentBuilder documentBuilder;
	private String name;
	private boolean persistent = false;
	
	private Timer updateCacheTimer;
	private volatile Boolean connected = false;
	private final Object sessionLock = new Object();
	private final Object cacheLock = new Object();
	private final Object editCacheLock = new Object();
	
	private volatile DeviceConfig editConfigInstance = null;
	private volatile DefaultsConfig defaultsConfigInstance = null;

	private volatile Boolean pppoeSupported = null;
	
	public static class DatabaseHandler implements AutoCloseable {
		ConnectionSource connection;
		
		public DatabaseHandler(File folder) throws SQLException {
//			connection = new JdbcConnectionSource(String.format("jdbc:sqlite:%s/device.db", folder));
			connection = new JdbcConnectionSource("jdbc:sqlite::memory:");
		}
		
		public final <T> int dropTable(Class<T> clazz, boolean ignoreErrors) throws SQLException {
			return TableUtils.dropTable(connection, clazz, true);
		}
		
		public final <T> int createTable(Class<T> clazz) throws SQLException {
			return TableUtils.createTable(connection, clazz);
		}
		
		public final <D extends Dao<T, ?>, T> D createDao(Class<T> clazz) throws SQLException {
			return DaoManager.createDao(connection, clazz);
		}

		@Override
		public void close() throws Exception {
			if (connection != null)
				connection.close();
		}
	}
	
	public Device(String hostname, String login, String password, File cacheDir) throws NetconfException, ParserConfigurationException {
		this(hostname, login, password, null, DEFAULT_PORT, cacheDir);
	}
	
	public Device(String hostname, String login, String password, String pemKeyFile, File cacheDir) throws NetconfException, ParserConfigurationException {
		this(hostname, login, password, pemKeyFile, DEFAULT_PORT, cacheDir);
	}
	
	public Device(String hostname, String login, String password, int port, File cacheDir) throws NetconfException, ParserConfigurationException {
		this(hostname, login, password, null, port, cacheDir);
	}
	
	public Device(String hostname, String login, String password, String pemKeyFile, int port, File cacheDir) throws NetconfException, ParserConfigurationException {
		super(hostname, login, password, pemKeyFile, port);
		this.loginName = login;
		this.cacheDir = cacheDir;
		this.xpath = XML.createXpathInstance();
		this.documentBuilder = XML.createDocumentBuilderInstance();
	}

	public final String getLoginName() {
		return loginName;
	}
	
	public final String name() {
		return getName();
	}
	
	public final String getName() {
		if (StringUtils.isBlank(name)) {
			try {
				name = softwareInformation().getHostname();
			} catch (DeviceCacheException e) {
				name = this.gethostName();
				e.printStackTrace();
			}
		}
		return name;
	}

	public void setName(String v) {
		name = v;
	}

	protected void cacheRpcReply(String name) throws DeviceCacheException {
		String buf;
		String command = cacheCommandMap.get(name);

		try {
			if (CACHE_NAME_SESSION_LOG.equals(name)) {
				DeviceConfig running = DeviceConfig.createFromDocument(string2document(getCachedRpcReplyWithoutSession(CACHE_NAME_RUNNING_CONFIG, CACHE_UPDATE_NO_IF_EXIST)), softwareInformation());
				String logName = running.sessionLogFileName();
				if (StringUtils.isBlank(logName)) {
					throw new NoSuchFileException("Session log is not configured.");
				}
				
				command = String.format(command, logName);
			}
			
			if (command.indexOf("show ") == 0) {
				buf = this.runCliCommand(command + " | display xml");
			}
			else {
				net.juniper.netconf.XML reply = this.executeRPC(command);
				buf = reply.toString();
			}

			final File file = getCacheFile(name);
			final File tmp  = File.createTempFile(name, "tmp");

			try (BufferedWriter bw = new BufferedWriter(new FileWriter(tmp))) {
				bw.write(buf);
			}

			synchronized(cacheLock) {
				if (file.exists())
					file.delete();
				tmp.renameTo(file);
				
				if (CACHE_NAME_JUNOS_DEFAULTS.equals(name)) {
					defaultsConfigInstance = null;
				}
				
				if (CACHE_NAME_INTERFACE_INFORMATION.equals(name)) {
					pppoeSupported = null;
				}
			}
		}
		catch (SAXException | IOException e) {
			throw new DeviceCacheException(name, e);
		}
	}
	
	public static final long CACHE_UPDATE_NO             = -2L;
	public static final long CACHE_UPDATE_NO_IF_EXIST    = -1L;
	public static final long CACHE_UPDATE_FORCE          = 0L;
	public static final long CACHE_UPDATE_DEFAULT        = 600000L; // 10分
	
	protected final boolean isRpcCacheExpired(String name) {
		return isRpcCacheExpired(name, CACHE_UPDATE_NO_IF_EXIST);
	}
	
	protected final boolean isRpcCacheExpired(String name, long expire) {
		return isRpcCacheExpired(name, getCacheFile(name), expire);
	}
	
	protected boolean isRpcCacheExpired(String name, File file, long expire) {
		if (expire == CACHE_UPDATE_NO)
			return false;
		
		if (!file.exists())
			return true;
		
		if (expire == CACHE_UPDATE_NO_IF_EXIST)
			return false;
	
		java.util.Date date = new java.util.Date();
		long current = date.getTime();
		long mod     = file.lastModified();
		
		if ((current - mod) > expire)
			return true;
		
		return false;
	}
	

	public String getCachedRpcReply(String name) throws DeviceCacheException {
		return getCachedRpcReply(name, CACHE_UPDATE_NO_IF_EXIST);
	}

	public String getCachedRpcReply(final String name, final long expire) throws DeviceCacheException {
		String ret = null;
		
		try {
			final File file = getCacheFile(name);
			if (isRpcCacheExpired(name, file, expire)) {
				session(new Callable<Void>() {
					@Override
					public Void call() throws Exception {
						cacheRpcReply(name);
						return null;
					}
				});
			}

			byte[] b;
			synchronized(cacheLock) {
				b = Files.readAllBytes(file.toPath());
			}
			
			ret = new String(b, StandardCharsets.UTF_8);
			if (StringUtils.isBlank(ret)) 
				throw new IllegalArgumentException(String.format("%s is blank.", name));
			
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(name, e);
		}
		return ret;
	}
	
	public String getCachedRpcReplyWithoutSession(final String name, final long expire) throws DeviceCacheException {
		String ret = null;
		
		try {
			final File file = getCacheFile(name);
			if (isRpcCacheExpired(name, file, expire)) {
				cacheRpcReply(name);
			}

			byte[] b;
			synchronized(cacheLock) {
				b = Files.readAllBytes(file.toPath());
			}
			
			ret = new String(b, StandardCharsets.UTF_8);
			if (StringUtils.isBlank(ret)) 
				throw new IllegalArgumentException(String.format("%s is blank.", name));
			
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(name, e);
		}
		return ret;
	}
	
	protected void clearRpcReplyCache(String name) {
		final File file;
	
		if (CACHE_NAME_EDIT_CONFIG.equals(name)) {
			synchronized(editCacheLock) {	
				file = getCacheFile(name);
				if (file.exists()) {
					file.delete();
				}
				
				editConfigInstance = null;
			}		
		}
		else {
			synchronized(cacheLock) {	
				file = getCacheFile(name);
				if (file.exists()) {
					file.delete();
				}
				
				if (CACHE_NAME_JUNOS_DEFAULTS.equals(name)) {
					defaultsConfigInstance = null;
				}
			}
		}
	}
	
	protected final File getCacheFile(final String name) {
		return new File(cacheDir, name + ".xml");
	}

	public void prepare() {
		startUpdateCacheTimer();
	}
	
	public void startUpdateCacheTimer() {
		TimerTask task = new TimerTask() {

			@Override
			public void run() {
				Device device = Device.this;
				try {
					device.updateCache();
				} catch (NetconfException e) {
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		};
		
		updateCacheTimer = new Timer(getName());
		updateCacheTimer.scheduleAtFixedRate(task, 0, 600 * 1000);
	}

	public boolean check() throws Throwable {
		if (isRpcCacheExpired(CACHE_NAME_SOFTWARE_INFORMATION, CACHE_UPDATE_NO_IF_EXIST) || isRpcCacheExpired(CACHE_NAME_CANDIDATE_CONFIG, CACHE_UPDATE_NO_IF_EXIST)) {
			return updateCache();
		}
		return true;
	}
	
	public final boolean updateChassisCache() throws Throwable {
		return updateCache(60000,
				CACHE_NAME_CHASSIS_INVENTORY
				);
	}
	
	public final boolean updateHomeCache() throws Throwable {
		return updateCache(60000,
				CACHE_NAME_INTERFACE_INFORMATION,
				CACHE_NAME_ZONES_INFORMATION,
				CACHE_NAME_SOFTWARE_INFORMATION,
				CACHE_NAME_CHASSIS_INVENTORY,
				CACHE_NAME_USERS_INFORMATION,
				CACHE_NAME_ROUTE_ENGINE_INFORMATION,
				CACHE_NAME_UPTIME_INFORMATION,
				CACHE_NAME_FLOW_SESSION_INFORMATION,
				CACHE_NAME_RUNNING_CONFIG,
				CACHE_NAME_MESSAGES_LOG,
				CACHE_NAME_ALARM_INFORMATION,
				CACHE_NAME_SYSTEM_ALARM_INFORMATION
				);
	}

	public final boolean updateCache() throws Throwable {
		return updateCache(CACHE_UPDATE_DEFAULT);
	}
	
	public final boolean updateCache(long expire) throws Throwable {
		return updateCache(expire, cacheCommandMap.keySet().toArray(new String[0]));
	}
	
	public final boolean updateCache(final long expire, final String ... names) throws Throwable {
		Boolean ret = session(new Callable<Boolean>() {
			@Override
			public Boolean call() throws Exception {
				for(String name : names) {
					if (isRpcCacheExpired(name, expire)) {
						try {
							cacheRpcReply(name);
						}
						catch (DeviceCacheException e) {
							// session_logのNoSuchFileExceptionは無視
							if (!(e.getCause() instanceof NoSuchFileException)) 
								throw e;
							String msg = e.getMessage();
							if (StringUtils.isBlank(msg) || !msg.startsWith(CACHE_NAME_SESSION_LOG)) 
								throw e;
						}
					}
				}

				return true;
			}
		});
		
		if (ret == null)
			return false;
		
		return ret;
	}

	private Document string2document(String xml) throws SAXException, IOException {
		Document ret = null;
		
		final InputSource is = new InputSource(new StringReader(xml));
		synchronized(documentBuilder) {
			ret = documentBuilder.parse(is);
		}

		if (ret == null) {
			throw new IllegalArgumentException("Invlaid xml.");
		}

		return ret;
	}
	
	private final Element document2element(Document doc, String path) {
		Element ret = null;

		ret = (Element) XML.getNode(xpath, doc.getDocumentElement(), path);
		if (ret == null) {
			throw new IllegalArgumentException(String.format("%s path element is not found.", path));
		}
		return ret;
	}

	public final String routeInformationString() throws DeviceCacheException {
		return getCachedRpcReply(CACHE_NAME_ROUTE_INFORMATION);
	}

	@Override
	public final RouteInformation routeInformation() throws DeviceCacheException {
		RouteInformation ret;
	
		try (DatabaseHandler dbh = createDatabaseHandler()) {
			Document doc = string2document(routeInformationString());
			ret = RouteInformation.createFromDocument(doc, document2element(doc, "route-information"), dbh, config());
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(CACHE_NAME_ROUTE_INFORMATION, e);
		}
		
		return ret;
	}
	
	public final String interfaceInformationString() throws DeviceCacheException {
		return getCachedRpcReply(CACHE_NAME_INTERFACE_INFORMATION);
	}

	@Override
	public final InterfaceInformation interfaceInformation() throws DeviceCacheException {
		InterfaceInformation ret = null;
	
		try (DatabaseHandler dbh = createDatabaseHandler()) {
			Document doc = string2document(interfaceInformationString());
			ret = InterfaceInformation.createFromDocument(doc, document2element(doc, "interface-information"), zonesInformation(), dbh, config());
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(CACHE_NAME_INTERFACE_INFORMATION, e);
		}
		
		return ret;
	}

	@Override
	public final PhysicalInterfaceInformation physicalInterfaceInformation() throws DeviceCacheException {
		PhysicalInterfaceInformation ret = null;
	
		try {
			Document doc = string2document(interfaceInformationString());
			ret = new PhysicalInterfaceInformation(doc, document2element(doc, "interface-information"));
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(CACHE_NAME_INTERFACE_INFORMATION, e);
		}
		
		return ret;
	}
	
	public boolean isPPPoESupported() {
		Boolean v = pppoeSupported;
		if (v == null) {
			synchronized(cacheLock) {
				v = pppoeSupported;
				if (v == null) {
					try {
						PhysicalInterfaceInformation phyinfo = physicalInterfaceInformation();
						pppoeSupported = phyinfo.isPhysicalInterfaceExist(PPPoE.PHYSICAL_INTERFACE_NAME);
					}
					catch (Exception e) {
						return false;
					}
				}
			}
		}
		return pppoeSupported;
	}
	
	public final String zonesInformationString() throws DeviceCacheException {
		return getCachedRpcReply(CACHE_NAME_ZONES_INFORMATION);
	}

	@Override
	public final ZonesInformation zonesInformation() throws DeviceCacheException {
		ZonesInformation ret = null;
	
		try {
			Document doc = string2document(zonesInformationString());
			ret = new ZonesInformation(doc);
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(CACHE_NAME_ZONES_INFORMATION, e);
		}
		return ret;
	}
	
	@Override
	public final SoftwareInformation softwareInformation() throws DeviceCacheException {
		try {
			Document doc = string2document(getCachedRpcReply(CACHE_NAME_SOFTWARE_INFORMATION));
			return new SoftwareInformation(doc);
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(CACHE_NAME_SOFTWARE_INFORMATION, e);
		}
	}

	@Override
	public final ChassisInventory chassisInventory() throws DeviceCacheException {
		ChassisInventory ret = null;
	
		try {
			Document doc = string2document(getCachedRpcReply(CACHE_NAME_CHASSIS_INVENTORY));
			ret = new ChassisInventory(doc);
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(CACHE_NAME_CHASSIS_INVENTORY, e);
		}
		return ret;
	}

	@Override
	public final UsersInformation usersInformation() throws DeviceCacheException {
		UsersInformation ret = null;
	
		try {
			Document doc = string2document(getCachedRpcReply(CACHE_NAME_USERS_INFORMATION));
			ret = new UsersInformation(doc);
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(CACHE_NAME_USERS_INFORMATION, e);
		}
		return ret;
	}

	@Override
	public final RouteEngineInformation routeEngineInformation() throws DeviceCacheException {
		RouteEngineInformation ret = null;
	
		try {
			Document doc = string2document(getCachedRpcReply(CACHE_NAME_ROUTE_ENGINE_INFORMATION));
			ret = new RouteEngineInformation(doc);
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(CACHE_NAME_ROUTE_ENGINE_INFORMATION, e);
		}
		return ret;
	}

	@Override
	public final FlowSessionInformation flowSessionInformation() throws DeviceCacheException {
		FlowSessionInformation ret = null;
	
		try {
			Document doc = string2document(getCachedRpcReply(CACHE_NAME_FLOW_SESSION_INFORMATION));
			ret = new FlowSessionInformation(doc);
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(CACHE_NAME_FLOW_SESSION_INFORMATION, e);
		}
		return ret;
	}

	@Override
	public final UptimeInformation uptimeInformation() throws DeviceCacheException {
		UptimeInformation ret = null;
	
		try {
			Document doc = string2document(getCachedRpcReply(CACHE_NAME_UPTIME_INFORMATION));
			ret = new UptimeInformation(doc);
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(CACHE_NAME_UPTIME_INFORMATION, e);
		}
		return ret;
	}

	@Override
	public final AlarmInformation alarmInformation() throws DeviceCacheException {
		AlarmInformation ret = null;
	
		try {
			Document system  = string2document(getCachedRpcReply(CACHE_NAME_SYSTEM_ALARM_INFORMATION));
			Document chassis = string2document(getCachedRpcReply(CACHE_NAME_ALARM_INFORMATION));
			ret = new AlarmInformation(
					new XML(system),
					new XML(chassis)
					);
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(CACHE_NAME_SYSTEM_ALARM_INFORMATION + " " + CACHE_NAME_ALARM_INFORMATION, e);
		}
		return ret;
	}

	@Override
	public final PPPSummaryInformation pppSummaryInformation() throws DeviceCacheException {
		PPPSummaryInformation ret = null;

		if (isPPPoESupported()) {
			try {
				Document doc = string2document(getCachedRpcReply(CACHE_NAME_PPP_SUMMARY_INFORMATION));
				ret = new PPPSummaryInformation(new XML(doc, document2element(doc, "ppp-summary-information")));
			}
			catch (DeviceCacheException e) {
				throw e;
			}
			catch (Throwable e) {
				throw new DeviceCacheException(CACHE_NAME_PPP_SUMMARY_INFORMATION, e);
			}
		}
		else {
			ret = new PPPSummaryInformation();
		}
		return ret;	
	}

	@Override
	public final List<IPSecSAInformation> ipsecSAInformationList() throws DeviceCacheException {
		List<IPSecSAInformation> ret = new ArrayList<>();
		Document doc;
		XML xml;
		
		try {
			doc = string2document(getCachedRpcReply(CACHE_NAME_IPSEC_SECURITY_ASSOCIATIONS));
//			xml = new XML(doc, document2element(doc, "ipsec-security-associations-information"));
			xml = new XML(doc);
			for (Node sainfo: IterableNodeList.apply(xml.getNodeList("//ipsec-security-associations-information"))) {
				for (Node block: IterableNodeList.apply(xml.getNodeList(sainfo, "ipsec-security-associations-block"))) {
					IPSecSAInformation sa = IPSecSAInformation.createFromNode(block, true, xml);
					if (sa != null)
						ret.add(sa);
				}
			}
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(CACHE_NAME_IPSEC_SECURITY_ASSOCIATIONS, e);
		}
		
		try {
			doc = string2document(getCachedRpcReply(CACHE_NAME_IPSEC_INACTIVE_TUNNELS));
//			xml = new XML(doc, document2element(doc, "ipsec-security-associations-information"));
			xml = new XML(doc);

			for (Node sainfo: IterableNodeList.apply(xml.getNodeList("//ipsec-security-associations-information"))) {
				for (Node block: IterableNodeList.apply(xml.getNodeList(sainfo, "ipsec-security-associations-block"))) {
					IPSecSAInformation sa = IPSecSAInformation.createFromNode(block, false, xml);
					if (sa != null)
						ret.add(sa);
				}
			}
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(CACHE_NAME_IPSEC_INACTIVE_TUNNELS, e);
		}
		
		return ret;
	}
	
	public final DeviceConfig runningConfig() throws DeviceCacheException {
		DeviceConfig ret = null;
		try {
			ret = DeviceConfig.createFromDocument(string2document(getCachedRpcReply(CACHE_NAME_RUNNING_CONFIG)), softwareInformation());
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(CACHE_NAME_RUNNING_CONFIG, e);
		}
		return ret;
	}
	
	public final DeviceConfig candidateConfig() throws DeviceCacheException {
		DeviceConfig ret = null;
		try {
			ret = DeviceConfig.createFromDocument(string2document(getCachedRpcReply(CACHE_NAME_CANDIDATE_CONFIG)), softwareInformation());
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(CACHE_NAME_CANDIDATE_CONFIG, e);
		}
		return ret;
	}

	public String editingConfigString() throws DeviceCacheException {
		String ret = null;
		try {
			final File file = getCacheFile(CACHE_NAME_EDIT_CONFIG);
			final File srcFile = getCacheFile(CACHE_NAME_CANDIDATE_CONFIG);
		
			if (!srcFile.exists()) { 
				session(new Callable<Void>() {
					@Override
					public Void call() throws Exception {
						cacheRpcReply(CACHE_NAME_CANDIDATE_CONFIG);
						return null;
					}
				});
			}

			byte[] b;
			synchronized(editCacheLock) {
				if (!file.exists()) {
					Path src = srcFile.toPath();
					Files.copy(src, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
				}
				
				b = Files.readAllBytes(file.toPath());
			}
			
			ret = new String(b, StandardCharsets.UTF_8);
			if (StringUtils.isBlank(ret)) 
				throw new IllegalArgumentException(String.format("%s is blank.", name));
			
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(CACHE_NAME_EDIT_CONFIG, e);
		}
		return ret;		
	}
	
	public final DeviceConfig editingConfig() throws DeviceCacheException {
		DeviceConfig ret = null;
		try {
			String xml = editingConfigString();
			ret = DeviceConfig.createFromDocument(string2document(xml), softwareInformation());
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(CACHE_NAME_EDIT_CONFIG, e);
		}
		return ret;	
	}
	
	public final DefaultsConfig defaultsConfig() throws DeviceCacheException {
		DefaultsConfig ret = null;
		try {
			ret = DefaultsConfig.createFromDocument(string2document(getCachedRpcReply(CACHE_NAME_JUNOS_DEFAULTS)));
		}
		catch (DeviceCacheException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new DeviceCacheException(CACHE_NAME_JUNOS_DEFAULTS, e);
		}
		return ret;	
	}
	
	public final String sessionLogString() throws DeviceCacheException {
		String ret = "";
		try {
			String tmp = getCachedRpcReply(CACHE_NAME_SESSION_LOG, CACHE_UPDATE_NO_IF_EXIST);
			if (StringUtils.isNotBlank(tmp))
				ret = tmp;
		}
		catch (DeviceCacheException e) {
			if (!(e.getCause() instanceof NoSuchFileException)) 
				throw e;
		}
		return ret;
	}

	public final List<SyslogMessage> sessionLogList() throws DeviceCacheException {
		return SyslogMessage.createFromLines(sessionLogString());
	}
	
	public final String messagesLogString() throws DeviceCacheException {
		String ret = "";
		try {
			String tmp = getCachedRpcReply(CACHE_NAME_MESSAGES_LOG, CACHE_UPDATE_NO_IF_EXIST);
			if (StringUtils.isNotBlank(tmp))
				ret = tmp;
		}
		catch (DeviceCacheException e) {
			if (!(e.getCause() instanceof NoSuchFileException)) 
				throw e;
		}
		return ret;
	}

	public final List<SyslogMessage> messagesLogList() throws DeviceCacheException {
		return SyslogMessage.createFromLines(messagesLogString());
	}
	
	public List<LogicalInterface> logicalInterfaceList() throws DeviceCacheException {
		ArrayList<LogicalInterface> ret = new ArrayList<LogicalInterface>();
		DeviceConfig config = editingConfig();
	
		XPathExpression xPathName = config.compileXpath("name/text()");
		for(Node pint: IterableNodeList.apply(physicalInterfaceInformation().physicalInterfaceNodeList())) {
			String pname = XML.getString(xPathName, pint);

			if (Util.isPortPhysicalInterface(pname) ||
					(pname.indexOf("irb") != -1) || 
					(pname.indexOf("lo0") != -1) || 
					(pname.indexOf("vlan") != -1)) {
				Node pconfig = config.findPhysicalInterfaceNode(pname);
				if (pconfig == null)
					continue;

				for (Node unit : IterableNodeList.apply(config.getNodeList(pconfig, "unit"))) {
					LogicalInterface lint = null;

					try {
						lint = LogicalInterface.createFromNode(pname, unit, config);
						if (lint == null)
							continue;

						ret.add(lint);
					}
					catch (Throwable e) {
						e.printStackTrace();
					}
				}
			}			
		}
		
		return ret;
	}

	protected void saveEditingConfig(DeviceConfig config) throws IOException, TransformerFactoryConfigurationError, TransformerException {
		synchronized(editCacheLock) {	
			final File file = getCacheFile(CACHE_NAME_EDIT_CONFIG);
			config.save(file);
			
			editConfigInstance = null;
		}
	}

	public void config(DeviceConfigure configure) throws DeviceConfigException {
		try {
			DeviceConfig config = editingConfig();
			configure.config(config, this);
			saveEditingConfig(config);
		} catch (IOException e) {
			throw new DeviceConfigException("Failed to config device", e);
		} catch (DeviceConfigException e) {
			throw e;
		} catch (TransformerFactoryConfigurationError e) {
			throw new DeviceConfigException("Failed to config device", e);
		} catch (TransformerException e) {
			throw new DeviceConfigException("Failed to config device", e);
		} catch (DeviceCacheException e) {
			throw new DeviceConfigException("Failed to config device", e.getCause());
		} catch (Throwable e) {
			throw new DeviceConfigException("Failed to config device", e);
		}
	}

	public void rollbackConfig(final int idx) throws Throwable {
			session(new Callable<Void>() {

				@Override
				public Void call() throws Exception {
					try {
						net.juniper.netconf.XML reply;
						String command = (idx == 0) ? "<discard-changes/>" : String.format("<load-configuration rollback=\"%d\"/>", idx);
							
						synchronized(cacheLock) {
							reply = Device.this.executeRPC(command);
						}
						if (StringUtils.isNotBlank(reply.findValue(Arrays.asList("rpc-error", "error-severity"))))
							throw new RuntimeException(reply.toString());

						cacheRpcReply(CACHE_NAME_CANDIDATE_CONFIG);
						cacheRpcReply(CACHE_NAME_RUNNING_CONFIG);
						cacheRpcReply(CACHE_NAME_ROUTE_INFORMATION);
						cacheRpcReply(CACHE_NAME_INTERFACE_INFORMATION);

					} catch (Throwable e1) {
						throw (Exception)e1;
					}
									return null;
				}
			});
			
			clearRpcReplyCache(CACHE_NAME_EDIT_CONFIG);
	}
	
	public void syncConfig() throws Throwable {
		session(new Callable<Void>() {

			@Override
			public Void call() throws Exception {
				cacheRpcReply(CACHE_NAME_CANDIDATE_CONFIG);
				cacheRpcReply(CACHE_NAME_ROUTE_INFORMATION);
				cacheRpcReply(CACHE_NAME_INTERFACE_INFORMATION);

				return null;
			}
		});

		clearRpcReplyCache(CACHE_NAME_EDIT_CONFIG);
	}
	
	public void loadConfig() throws Throwable {
		loadConfig(false);
	}

	public void loadConfig(final boolean doCommit) throws Throwable {
		session(new Callable<Void>() {

			@Override
			public Void call() throws Exception {
				boolean isLocked = false;

				try {
					DeviceConfig e = editingConfig();
					isLocked = lockConfig();
					loadXMLConfiguration(e.toXMLString(), "replace");
					if (doCommit) {
						commit();
					}
					
					cacheRpcReply(CACHE_NAME_CANDIDATE_CONFIG);
					cacheRpcReply(CACHE_NAME_RUNNING_CONFIG);
					cacheRpcReply(CACHE_NAME_ROUTE_INFORMATION);
					cacheRpcReply(CACHE_NAME_INTERFACE_INFORMATION);

				} catch (LoadException e1) {
					String reply = getLastRPCReply();
					throw new RuntimeException(reply);
				} catch (CommitException e1) {
					String reply = getLastRPCReply();
					throw new RuntimeException(reply);		
				} catch (Throwable e1) {
					throw (Exception)e1;
				} finally {
					if (isLocked)
						unlockConfig();
				}
				return null;
			}
		});
		
		clearRpcReplyCache(CACHE_NAME_EDIT_CONFIG);
	}
	
	public final DeviceConfig config() throws DeviceCacheException {
		DeviceConfig ret = editConfigInstance;
		
		if (ret == null) {
			synchronized(editCacheLock) {
				ret = editConfigInstance;
				if (ret == null) {
					ret = editConfigInstance = editingConfig();
				}
			}
		}
		
		return ret;
	}

	public final DefaultsConfig defaults() throws DeviceCacheException {
		DefaultsConfig ret = defaultsConfigInstance;
	
		if (ret == null) {
			synchronized(cacheLock) {
				ret = defaultsConfigInstance;
				if (ret == null) {
					ret = defaultsConfigInstance = defaultsConfig();
				}
			}
		}
		
		return ret;
	}
	
//	public final InterfaceInformation interfaces() throws Throwable {
//		return interfaceInformation();
//	}

	@Override
	public void close() {
		Boolean c = connected;
		if (c == true) {
			synchronized(connected) {
				c = connected;
				if (c == true) {
					super.close();
					connected = false;
				}
			}
		}
	}

	@Override
	public void connect() throws NetconfException {
		Boolean c = connected;
		if (c == false) {
			synchronized(connected) {
				c = connected;
				if (c == false) {
					super.connect();
					connected = true;
				}
			}
		}
	}
	
	public boolean connect2() throws NetconfException {
		boolean ret = false;
		Boolean c = connected;
		if (c == false) {
			synchronized(connected) {
				c = connected;
				if (c == false) {
					super.connect();
					connected = true;
					ret = true;
				}
			}
		}
		return ret;
	}

	protected <T> T session(Callable<T> task) throws Throwable {
		T ret = null;
		synchronized(sessionLock) {
			try {
				ret = sessionFuture(task).get();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				e.printStackTrace();
			} catch (ExecutionException e) {
				throw e.getCause();
			}
		}
		return ret;
	}
	
	protected <T> Future<T> sessionFuture(final Callable<T> task) {
		ExecutorService executor = Executors.newSingleThreadExecutor();
		return executor.submit(new Callable<T>() {

			@Override
			public T call() throws Exception {
				Device self = Device.this;
				T ret = null;
				boolean doClose = false;

				try {
					doClose = self.connect2();
					ret = task.call();
				}
				catch (Exception e) {
					self.close();
					throw e;
				}
				finally {
					if (doClose && !self.persistent)
						self.close();
				}
				
				return ret;
			}
		});
	}

	private DatabaseHandler createDatabaseHandler() throws SQLException {
		return new DatabaseHandler(cacheDir);
	}
	
	public void setPersistent(boolean v) {
		persistent = v;
	}
	
	public List<String> allApplicationNameList() throws DeviceCacheException {
		final String anyName = "any";
		
		List<String> ret = new ArrayList<>();
		DeviceConfig editConfig = config();
		DefaultsConfig defaultsConfig = defaults();

		ret.add(anyName);
		ret.addAll(editConfig.applicationNameList());
		ret.addAll(editConfig.applicationSetNameList());
	
		for (String name : defaultsConfig.applicationNameList()) {
			if (!anyName.equals(name)) {
				ret.add(name);
			}
		}
		
		ret.addAll(defaultsConfig.applicationSetNameList());
		
		return ret;
	}
}
