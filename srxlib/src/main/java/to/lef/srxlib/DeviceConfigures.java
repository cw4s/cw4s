package to.lef.srxlib;

import java.util.ArrayList;
import java.util.List;

public class DeviceConfigures implements DeviceConfigure {
	List<DeviceConfigure> configures;

	public DeviceConfigures(DeviceConfigure ... configures) {
		this.configures = new ArrayList<>();
		for (DeviceConfigure c : configures) {
			this.configures.add(c);
		}
	}

	public final DeviceConfigures add(DeviceConfigure configure) {
		this.configures.add(configure);
		return this;
	}
	
	@Override
	public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
		for (DeviceConfigure c: configures) {
			c.config(config, info);
		}
	}

}
