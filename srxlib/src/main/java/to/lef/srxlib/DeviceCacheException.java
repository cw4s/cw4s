package to.lef.srxlib;

public class DeviceCacheException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 691329553437296651L;

	public DeviceCacheException(String message, Throwable e) {
		super(message, e);
	}
	
	public DeviceCacheException(Throwable e) {
		super("Failed to get required information from device.", e);
	}
	
	@Override
	public String getMessage() {
		Throwable cause = getCause();
		
		if (cause != null) {
			return super.getMessage() + ": " + cause.getMessage();
		}
		return super.getMessage();
	}
}
