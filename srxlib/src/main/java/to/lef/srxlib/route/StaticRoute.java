package to.lef.srxlib.route;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.Util;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class StaticRoute {
	public static final String DEFAULT_INET_RIB_NAME = "inet.0";
	public static final String DEFAULT_INET6_RIB_NAME = "inet6.0";

	protected static interface NextHopCallback {
		public boolean onNextHopNode(Node node, StaticRoute route);
	}
	
	protected static class NextHopEnumerator {
		private String ribName;
		private Node routerNode;
		private DeviceConfig config;
	
		private String name;
		private Integer defaultTag;
		private Integer defaultMetric;
		private Integer defaultPreference;
		
		public NextHopEnumerator(String ribName, Node routerNode, DeviceConfig config) {
			this.ribName = ribName;
			this.routerNode = routerNode;
			this.config = config;
			
			String tmp;
			this.name              = config.getString(routerNode, "name/text()");
			tmp                    = config.getString(routerNode, "tag/metric-value/text()");
			this.defaultTag        = (StringUtils.isNumeric(tmp)) ? Integer.valueOf(tmp) : null;
			tmp                    = config.getString(routerNode, "metric/metric-value/text()");
			this.defaultMetric     = (StringUtils.isNumeric(tmp)) ? Integer.valueOf(tmp) : null;
			tmp                    = config.getString(routerNode, "preference/metric-value/text()");
			this.defaultPreference = (StringUtils.isNumeric(tmp)) ? Integer.valueOf(tmp) : null;
		}
		
		public void enumerate(NextHopCallback cb) {
			String tmp;
			for (Node nh :IterableNodeList.apply(config.getNodeList(routerNode, "qualified-next-hop"))) {
				String nhName = config.getString(nh, "name/text()");
				if (StringUtils.isBlank(nhName))
					continue;

				String nhInterface  = null;
				if (Util.isValidIPAddress(nhName)) {
					nhInterface = config.getString(nh, "interface/text()");
				}
				else {
					nhInterface = nhName;
					nhName = null;
				}

				if (StringUtils.isBlank(nhName) && StringUtils.isBlank(nhInterface))
					continue;

				tmp                  = config.getString(nh, "metric/text()");
				Integer nhMetric     = (StringUtils.isNumeric(tmp)) ? Integer.valueOf(tmp) : defaultMetric;
				tmp                  = config.getString(nh, "preference/text()");
				Integer nhPreference = (StringUtils.isNumeric(tmp)) ? Integer.valueOf(tmp) : defaultPreference;

				StaticRoute route = null;
				
				try {
					if (ribName.indexOf("inet6") == 0) {
						route = new Inet6StaticRoute(ribName, name, nhName, nhInterface, defaultTag, nhMetric, nhPreference);
					}
					else if (ribName.indexOf("inet") == 0) {
						route = new InetStaticRoute(ribName, name, nhName, nhInterface, defaultTag, nhMetric, nhPreference);
					}
					else {
						route = new StaticRoute(ribName, name, nhName, nhInterface, defaultTag, nhMetric, nhPreference);
					}	
				}
				catch (UnknownHostException e) {
					
				}
				
				if (route != null) {
					if (!cb.onNextHopNode(nh, route))
						break;
				}
			}
			
			for (Node nh : IterableNodeList.apply(config.getNodeList(routerNode, "next-hop"))) {
				String nhName = config.getString(nh, "text()");
				if (StringUtils.isBlank(nhName))
					continue;

				String nhInterface = null;
				if (!Util.isValidIPAddress(nhName)) {
					nhInterface = nhName;
					nhName = null;
				}		

				StaticRoute route = null;
				
				try {
					if (ribName.indexOf("inet6") == 0) {
						route = new Inet6StaticRoute(ribName, name, nhName, nhInterface, defaultTag, defaultMetric, defaultPreference);
					}
					else if (ribName.indexOf("inet") == 0) {
						route = new InetStaticRoute(ribName, name, nhName, nhInterface, defaultTag, defaultMetric, defaultPreference);
					}
					else {
						route = new StaticRoute(ribName, name, nhName, nhInterface, defaultTag, defaultMetric, defaultPreference);
					}
				}
				catch (UnknownHostException e) {
					
				}
				
				if (route != null) {
					if (!cb.onNextHopNode(nh, route))
						break;
				}
			}		
		}
	}

	
	private final String name;
	private final String ribName;
	private String nextHop;
	private String interfaceName;
	private Integer tag;
	private Integer metric;
	private Integer preference;

	public static List<StaticRoute> createListFromRouteNode(String ribName, Node rn, DeviceConfig cfg) {
		final List<StaticRoute> ret = new ArrayList<StaticRoute>();
	
		NextHopEnumerator enumerator = new NextHopEnumerator(ribName, rn, cfg);
		
		enumerator.enumerate(new NextHopCallback() {

			@Override
			public boolean onNextHopNode(Node node, StaticRoute route) {
				ret.add(route);
				return true;
			}
			
		});
		
		return ret;
	}
	
	public final static StaticRoute apply(String name, int mask, String nextHop, String interfaceName, Integer tag, Integer metric, Integer preference) throws UnknownHostException {
		if (Util.isValidIPv4Address(name)) {
			return new InetStaticRoute(DEFAULT_INET_RIB_NAME, name, mask, nextHop, interfaceName, tag, metric, preference);
		} else if (Util.isValidIPv6Address(name)) {
			return new Inet6StaticRoute(DEFAULT_INET6_RIB_NAME, name, mask, nextHop, interfaceName, tag, metric, preference);
		}
		throw new IllegalArgumentException("Invalid destination address");
	}
	
	public StaticRoute(String ribName, String name, int mask, String nextHop, String interfaceName, Integer tag, Integer metric, Integer preference) throws UnknownHostException {
		this(ribName, String.format("%s/%d",  name, mask), nextHop, interfaceName, tag, metric, preference);
	}
	
	public StaticRoute(String ribName, String name, String nextHop, String interfaceName, Integer tag, Integer metric, Integer preference) throws UnknownHostException {
		String[] names = name.split("\\/");

		this.ribName       = ribName;
		this.name          = String.format("%s/%s", Util.fixInetAddressString(names[0]), names[1]);
		this.nextHop       = nextHop;
		this.interfaceName = interfaceName;
		this.tag           = tag;
		this.metric        = metric;
		this.preference    = preference;
	}

	public final String getRibName() {
		return ribName;
	}
	
	public final String getName() {
		return name;
	}

	public final String getNextHop() {
		return nextHop;
	}

	public final String getInterfaceName() {
		return interfaceName;
	}
	
	public final Integer getTag() {
		return tag;
	}

	public final Integer getMetric() {
		return metric;
	}

	public final Integer getPreference() {
		return preference;
	}

	public final void setMetric(Integer metric) {
		this.metric = metric;
	}

	public final void setPreference(Integer preference) {
		this.preference = preference;
	}

	public void config(Node rn, DeviceConfig cfg) {
		if ((StringUtils.isNotBlank(nextHop) && (StringUtils.isNotBlank(interfaceName)) || metric != null || preference != null)) {
	//		Node qnh = cfg.createElement("qualified-next-hop");
			Node qnh = null;
			
			if (StringUtils.isBlank(nextHop) && StringUtils.isNotBlank(interfaceName)) {
				cfg.deleteNode(rn, "next-hop[normalize-space(text())=%s]", XML.xpc(interfaceName));
				
				qnh = cfg.getNode(rn, "qualified-next-hop[normalize-space(name/text())=%s]", XML.xpc(interfaceName));
				if (qnh == null) {
					qnh = cfg.createElement("qualified-next-hop");
					qnh.insertBefore(cfg.createElement("name", interfaceName), cfg.getNode(qnh, "*"));
					rn.appendChild(qnh);
				}
			}
			else {
				cfg.deleteNode(rn, "next-hop[normalize-space(text())=%s]", XML.xpc(nextHop));
				qnh = cfg.getNode(rn, "qualified-next-hop[normalize-space(name/text())=%s]", XML.xpc(nextHop));
				if (qnh == null) {
					qnh = cfg.createElement("qualified-next-hop");
					qnh.insertBefore(cfg.createElement("name", nextHop), cfg.getNode(qnh, "*"));
					rn.appendChild(qnh);
				}

				if (StringUtils.isNotBlank(interfaceName)) {
					qnh.appendChild(cfg.createElement("interface", interfaceName));
				}
			}
			
			cfg.deleteNode(qnh, "metric");
			if (metric != null) {
				qnh.appendChild(cfg.createElement("metric", "%d", metric));
			}

			cfg.deleteNode(qnh, "preference");
			if (preference != null) {
				qnh.appendChild(cfg.createElement("preference", "%d", preference));
			}
			
		}
		else {
			Node nh = null;
			String n = StringUtils.isNotBlank(nextHop) ? nextHop : interfaceName;
			
			cfg.deleteNode(rn, "qualified-next-hop[normalize-space(name/text())=%s]", XML.xpc(n));
			nh = cfg.getNode(rn, "next-hop[normalize-space(text())=%s]", XML.xpc(n));
			if (nh == null) {
				nh = cfg.createElement("next-hop", n);
				rn.appendChild(nh);	
			}
		/*	
			if (metric != null) {
				cfg.deleteNode(rn, "metric");
				Node tn = cfg.createElement("metric");
				tn.appendChild(cfg.createElement("metric-value", "%d", metric));
				rn.appendChild(tn);
			}
			
			if (preference != null) {
				cfg.deleteNode(rn, "preference");
				Node tn = cfg.createElement("preference");
				tn.appendChild(cfg.createElement("metric-value", "%d", preference));
				rn.appendChild(tn);
			}
			*/
		}
		
		if (tag != null) {
			cfg.deleteNode(rn, "tag");
			Node tn = cfg.createElement("tag");
			tn.appendChild(cfg.createElement("metric-value", "%d", tag));
			rn.appendChild(tn);
		}
	}
	
	public DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				final String tagStatic = "static";
				final StaticRoute self = StaticRoute.this;
				
				try {
					if (StringUtils.isBlank(nextHop) && StringUtils.isBlank(interfaceName)) {
						throw new IllegalArgumentException("Next hop address and interface name are invalid.");
					}
					
					Node ribNode = cfg.ribNode(self.ribName);
					Node staticNode   = cfg.createElementsIfNotExist(ribNode, tagStatic);
				
					for (StaticRoute route : cfg.staticRouteList(ribName, name)) {
						if (self.isSameTarget(route)) {
							throw new IllegalArgumentException("The route is already exist.");
						}
					}
			
					Node r = null;
					for (Node n : IterableNodeList.apply(cfg.getNodeList(staticNode, "route"))) {
						if (Util.isNetworkAddressEqualTo(name, cfg.getString(n, "name/text()"))) {
							r = n;
							break;
						}
					}
				
					if (r == null) {
						r = cfg.createElement("route");
						r.appendChild(cfg.createElement("name", name));
						staticNode.appendChild(r);
					}
					
					self.config(r, cfg);
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config static route.", e);
				}
			}
			
		};
	}

	public final DeviceConfigure getEditConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				final StaticRoute self = StaticRoute.this;
				
				try {
					final Node routeNode = config.findStaticRouteNode(ribName, name);
					
					if (routeNode != null) {
						self.config(routeNode, config);
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config static route.", e);
				}			
			}
			
		};
	}
	
	public final DeviceConfigure getDeleteConfigure() {
		return getDeleteConfigure(ribName, name, nextHop, interfaceName);
	}
	
	public static DeviceConfigure getDeleteConfigure(final String ribName, final String dstName, final String gwName, final String intName) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				if (StringUtils.isBlank(gwName) && StringUtils.isBlank(intName))
					return;
				
				try {
					final StaticRoute dummy = new StaticRoute(ribName, dstName, gwName, intName, null, null, null);
					final Node routeNode = cfg.findStaticRouteNode(ribName, dstName);
					
					if (routeNode != null) {
						NextHopEnumerator enumerator = new NextHopEnumerator(ribName, routeNode, cfg);
						enumerator.enumerate(new NextHopCallback() {

							@Override
							public boolean onNextHopNode(Node node, StaticRoute route) {
								if (dummy.isSameTarget(route)) {
									routeNode.removeChild(node);
								}
									
								return true;
							}
							
						});
					
						if (cfg.getNode(routeNode, "next-hop") == null && cfg.getNode(routeNode, "qualified-next-hop") == null) {
							routeNode.getParentNode().removeChild(routeNode);
						}
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to delete static route.", e);
				}
			}
		};
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((interfaceName == null) ? 0 : interfaceName.hashCode());
		result = prime * result + ((metric == null) ? 0 : metric.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((nextHop == null) ? 0 : nextHop.hashCode());
		result = prime * result + ((preference == null) ? 0 : preference.hashCode());
		result = prime * result + ((ribName == null) ? 0 : ribName.hashCode());
		result = prime * result + ((tag == null) ? 0 : tag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof StaticRoute)) {
			return false;
		}
		StaticRoute other = (StaticRoute) obj;
		if (StringUtils.isBlank(interfaceName)) {
			if (StringUtils.isNotBlank(other.interfaceName)) {
				return false;
			}
		} else if (!interfaceName.equals(other.interfaceName)) {
			return false;
		}
		if (metric == null) {
			if (other.metric != null) {
				return false;
			}
		} else if (!metric.equals(other.metric)) {
			return false;
		}
		if (StringUtils.isBlank(name)) {
			if (StringUtils.isNotBlank(other.name)) {
				return false;
			}
		} else if (!Util.isNetworkAddressEqualTo(name, other.name)) {
			return false;
		}
		if (StringUtils.isBlank(nextHop)) {
			if (StringUtils.isNotBlank(other.nextHop)) {
				return false;
			}
		} else if (!nextHop.equals(other.nextHop)) {
			return false;
		}
		if (preference == null) {
			if (other.preference != null) {
				return false;
			}
		} else if (!preference.equals(other.preference)) {
			return false;
		}
		if (StringUtils.isBlank(ribName)) {
			if (StringUtils.isNotBlank(other.ribName)) {
				return false;
			}
		} else if (!ribName.equals(other.ribName)) {
			return false;
		}
		if (tag == null) {
			if (other.tag != null) {
				return false;
			}
		} else if (!tag.equals(other.tag)) {
			return false;
		}
		return true;
	}
	
	public boolean isSameTarget(StaticRoute other) {
		if (StringUtils.isBlank(interfaceName)) {
			if (StringUtils.isNotBlank(other.interfaceName)) {
				return false;
			}
		} else if (!interfaceName.equals(other.interfaceName)) {
			return false;
		}
		if (StringUtils.isBlank(name)) {
			if (StringUtils.isNotBlank(other.name)) {
				return false;
			}
		} else if (!Util.isNetworkAddressEqualTo(name, other.name)) {
			return false;
		}
		if (StringUtils.isBlank(nextHop)) {
			if (StringUtils.isNotBlank(other.nextHop)) {
				return false;
			}
		} else if (!nextHop.equals(other.nextHop)) {
			return false;
		}
		if (StringUtils.isBlank(ribName)) {
			if (StringUtils.isNotBlank(other.ribName)) {
				return false;
			}
		} else if (!ribName.equals(other.ribName)) {
			return false;
		}
		return true;
	}
}
