package to.lef.srxlib.route;

import java.net.UnknownHostException;


public class InetStaticRoute extends StaticRoute {

	public InetStaticRoute(String ribName, String name, int mask, String nextHop, String interfaceName, Integer tag, Integer metric, Integer preference) throws UnknownHostException {
		super(ribName, name, mask, nextHop, interfaceName, tag, metric, preference);
	}

	public InetStaticRoute(String ribName, String name, String nextHop, String interfaceName, Integer tag, Integer metric, Integer preference) throws UnknownHostException {
		super(ribName, name, nextHop, interfaceName, tag, metric, preference);
	}
}
