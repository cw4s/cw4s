package to.lef.srxlib;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.digest.Md5Crypt;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.w3c.dom.Node;

import to.lef.srxlib.dom.XML;
import to.lef.srxlib.nic.LogicalInetInterface;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Util {

	private Util() {
	}
	
	public static String fixInetAddressString(String v) throws UnknownHostException {
		return InetAddress.getByName(v).getHostAddress();
	}
	
	public static boolean isValidIPv4Address(String v) {
		return InetAddressValidator.getInstance().isValidInet4Address(v);
	}
	
	public static boolean isValidIPv6Address(String v) {
		return InetAddressValidator.getInstance().isValidInet6Address(v);
	}

	public static boolean isValidIPAddress(String v) {
		return (isValidIPv4Address(v) || isValidIPv6Address(v));
	}

	public static boolean isNetworkAddressEqualTo(String a, String b) {
		try {
			String[] as = a.split("\\/");
			String[] bs = b.split("\\/");

			return (isIPAddressEqualTo(as[0], bs[0]) && as[1].equals(bs[1]));
		}
		catch (Throwable e) {
			return false;
		}
	}

	public static boolean isAddressInNetwork(InetAddress a, String b, int mask) {
		IPAddress addr = new IPAddress(a, mask);
		return addr.isSameNetwork(b);
	}
	
	public static boolean isAddressInNetwork(String a, String b, int mask) {
		IPAddress addr = new IPAddress(a, mask);
		return addr.isSameNetwork(b);
	}
	
	public static boolean isIPAddressEqualTo(String a, String b) {
		try {
			return (InetAddress.getByName(a).equals(InetAddress.getByName(b)));
		}
		catch (Throwable e) {
			return false;
		}
	}
/*
	public static InetAddress getBroadcastAddress(String addr, int mask) {
		InetAddress ret = null;
		int  max = 0;
		byte m   = 0;
		try {
			if (isValidIPv4Address(addr)) {
				max = 32;
			}
			else if (isValidIPv6Address(addr)) {
				max = 128;
			}
			else {
				return null;
			}
		
			if (mask < 0 || max < mask) 
				return null;
			
			InetAddress ia = InetAddress.getByName(addr);
			byte[]      a  = ia.getAddress();
			
			for (int i = 0, mod = 7; i < max; i++) {
				if (i >= mask) {
					m |= (1 << mod);
				}
				
				if (mod == 0) {
					a[i / 8] |= m;
					m         = 0;
					mod       = 7;
				}
				else {
					mod--;
				}
			}
			
			ret = InetAddress.getByAddress(a);
		}
		catch (Throwable e) {
			e.printStackTrace();
		}
		
		return ret;	
	}
	
	public final static InetAddress getNetworkAddress(String addr, int mask) {
		InetAddress ret = null;
		try {
			ret = getNetworkAddress(InetAddress.getByName(addr), mask);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	public static InetAddress getNetworkAddress(InetAddress addr, int mask) {
		InetAddress ret = null;
		int  max = 0;
		byte m   = 0;
		try {
			if (addr instanceof Inet4Address) {
				max = 32;
			}
			else if (addr instanceof Inet6Address) {
				max = 128;
			}
			else {
				return null;
			}
		
			if (mask < 0 || max < mask) 
				return null;
			
			byte[] a  = addr.getAddress();
			for (int i = 0, mod = 7; i < max; i++) {
				if (i < mask) {
					m |= (1 << mod);
				}
				
				if (mod == 0) {
					a[i / 8] &= m;
					m         = 0;
					mod       = 7;
				}
				else {
					mod--;
				}
			}
			
			ret = InetAddress.getByAddress(a);
		}
		catch (Throwable e) {
			e.printStackTrace();
		}
		
		return ret;
	}
*/
	public static boolean isValidHostAddress(String addr, int mask, boolean isAcceptZero) {
		if (StringUtils.isBlank(addr))
			return false;
		
		if (isAcceptZero && addr.equals("0.0.0.0") && mask == 0)
			return true;
	
		try {
			IPAddress a = new IPAddress(addr, mask);
			return a.isHostAddress();
		}
		catch (Throwable e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static boolean isValidNetworkAddress(String addr, int mask) {
		try {
			IPAddress a = new IPAddress(addr, mask);
			return InetAddress.getByName(addr).equals(a.getNetworkAddress());
		}
		catch (Throwable e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
/*	
	public static boolean isValidNetworkAddress(String addr, int mask) {
		int  max = 0;
		byte m   = 0;
		try {
			if (isValidIPv4Address(addr)) {
				max = 32;
			}
			else if (isValidIPv6Address(addr)) {
				max = 128;
			}
			else {
				return false;
			}
		
			if (mask < 0 || max < mask) 
				return false;
			
			InetAddress ia = InetAddress.getByName(addr);
			byte[]      a  = ia.getAddress();
			byte[]      b  = Arrays.copyOf(a, a.length);
			
			for (int i = 0, mod = 0; i < max; i++) {
				if (i < mask) {
					m |= (1 << mod);
				}
				
				if (mod == 7) {
					a[i / 8] &= m;
					m         = 0;
					mod       = 0;
				}
				else {
					mod++;
				}
			}
		
			return Arrays.equals(a, b);
		}
		catch (Throwable e) {
			
		}
		
		return false;
	}
*/	
	public static boolean isValidInet4Type(int v) {
		switch(v) {
		case LogicalInetInterface.INET4_TYPE_STATIC:
		case LogicalInetInterface.INET4_TYPE_DHCP:
		case LogicalInetInterface.INET4_TYPE_NEGOTIATE:
		case LogicalInetInterface.INET4_TYPE_UNNUMBERED:
			return true;
		}
		return false;
	}
	
	public static boolean isValidIdentifier(String v) {
		Pattern p = Pattern.compile("^[0-9a-zA-Z:\\.\\/\\-_]+$");
		Matcher m = p.matcher(v);
		
		if (m.find()) {
			return (v.indexOf("junos-") != 0);
		}
		return false;
	}
	
	public static String[] parseNumberRange(String v) {
		Pattern p = Pattern.compile("^(\\d+)\\-(\\d+)$");
		Matcher m = p.matcher(v);
		
		if (m.find()) {
			return new String[]{m.group(1), m.group(2)};
		}
		return new String[]{v};
	}
	
	public static String displayNumberRange(String l, String h) {
		if (StringUtils.isBlank(l))
			return "";
		
		if (StringUtils.isNumeric(l) && StringUtils.isNumeric(h))
			return String.format("%s-%s", l, h);
		
		return l;
	}

	public static boolean isValidNumberRange(String l, String h) {
		if (l.equals(h))
			return true;
	
		if (StringUtils.isNumeric(l) && StringUtils.isNumeric(h)) {
			final int il = Integer.parseInt(l);
			final int ih = Integer.parseInt(h);

			return (il < ih);
		}
		
		return false;
	}
	
	public static boolean isValidApplicationTimeout(int v) {
		return (4 <= v && v <= 129600);
	}

	@SuppressWarnings("serial")
	private static final Map<String, Integer> applicationProtocolMap  = new LinkedHashMap<String, Integer>() {
		{
			put("ah",     51);
			put("egp",     8);
			put("esp",    50);
			put("gre",    47);
			put("icmp",    1);
			put("icmp6",  58);
			put("igmp",    2);
			put("ipip",   94);
			put("ospf",   89);
			put("pim",   103);
			put("rsvp",   46);
			put("sctp",  132);
			put("tcp",     6);
			put("udp",    17);
		}
	};

	public static Integer applicationProtocolToNumber(String v) {
		if (StringUtils.isBlank(v))
			return null;
		
		if (StringUtils.isNumeric(v)) {
			Integer n = Integer.valueOf(v);
			if (0 <= n && n <= 255)
				return n;
		}
		
		return applicationProtocolMap.get(v);
	}
	
	public static boolean isValidApplicationProtocol(String v) {
		if (StringUtils.isNumeric(v)) {
			final int n = Integer.parseInt(v);
			return (0 <= n && n <= 255);
		}

		return applicationProtocolMap.keySet().contains(v);
	}

	@SuppressWarnings("serial")
	private static final Map<String, Integer> applicationPortMap = new LinkedHashMap<String, Integer>() {
		{
			put("afs",           1483);
			put("bgp",            179);
			put("biff",           512);
			put("bootpc",          68);
			put("bootps",          67);
			put("cmd",            514);
			put("cvspserver",    2401);
			put("dhcp",            67);
			put("domain",          53);
			put("eklogin",       2105);
			put("ekshell",       2106);
			put("exec",           512);
			put("finger",          79);
			put("ftp",             21);
			put("ftp-data",        20);
			put("http",            80);
			put("https",          443);
			put("ident",          113);
			put("imap",           143);
			put("kerberos-sec",    88);
			put("klogin",         543);
			put("kpasswd",        761);
			put("krb-prop",       754);
			put("krbupdate",      760);
			put("kshell",         544);
			put("ldap",           389);
			put("ldp",            646);
			put("login",          513);
			put("mobileip-agent", 434);
			put("mobilip-mn",     435);
			put("msdp",           639);
			put("netbios-dgm",    138);
			put("netbios-ns",     137);
			put("netbios-ssn",    139);
			put("nfsd",          2049);
			put("nntp",           119);
			put("ntalk",          518);
			put("ntp",            123);
			put("pop3",           110);
			put("pptp",          1723);
			put("printer",        515);
			put("radacct",       1813);
			put("radius",        1812);
			put("rip",            520);
			put("rkinit",        2108);
			put("smtp",            25);
			put("snmp",           161);
			put("snmptrap",       162);
			put("snpp",           444);
			put("socks",         1080);
			put("ssh",             22);
			put("sunrpc",         111);
			put("syslog",         514);
			put("tacacs",          49);
			put("tacacs-ds",       65);
			put("talk",           517);
			put("telnet",          23);
			put("tftp",            69);
			put("timed",          525);
			put("who",            513);
			put("xdmcp",          177);
			put("zephyr-clt",    2103);
			put("zephyr-hm",     2104);
			put("zephyr-srv",    2102);	
		}
	};
	
	public static Integer applicationPortToNumber(String v) {
		if (StringUtils.isBlank(v))
			return null;
		
		if (StringUtils.isNumeric(v)) {
			Integer n = Integer.valueOf(v);
			if (0 <= n && n <= 65535)
				return n;
		}
		
		return applicationPortMap.get(v);
	}
	
	public static boolean isValidApplicationPort(String v) {
		if (StringUtils.isNumeric(v)) {
			final int n = Integer.parseInt(v);
			return (0 <= n && n <= 65535);
		}
	
		return applicationPortMap.keySet().contains(v);
	}
	
	public static boolean isValidApplicationIcmpType(String v) {
		final List<String> types = java.util.Arrays.asList(
				"echo-reply", "echo-request", "info-reply", "info-request", "mask-reply",
				"mask-request", "parameter-problem", "redirect", "router-advertisement",
				"router-solicit", "source-quench", "time-exceeded", "timestamp",
				"timestamp-reply", "unreachable"
		  );
		
		if (StringUtils.isNumeric(v)) {
			final int n = Integer.parseInt(v);
			return (0 <= n && n <= 255);
		}
		
		return types.contains(v);
	}
	
	public static boolean isValidApplicationIcmpCode(String v) {
		final List<String> codes = java.util.Arrays.asList(
		  "communication-prohibited-by-filtering",
		  "destination-host-prohibited",
		  "destination-host-unknown",
		  "destination-network-prohibited",
		  "destination-network-unknown",
		  "fragmentation-needed",
		  "host-precedence-violation",
		  "host-unreachable",
		  "host-unreachable-for-tos",
		  "ip-header-bad",
		  "network-unreachable",
		  "network-unreachable-for-tos",
		  "port-unreachable",
		  "precedence-cutoff-in-effect",
		  "protocol-unreachable",
		  "redirect-for-host",
		  "redirect-for-network",
		  "redirect-for-tos-and-host",
		  "redirect-for-tos-and-net",
		  "required-option-missing",
		  "source-host-isolated",
		  "source-route-failed",
		  "ttl-eq-zero-during-reassembly",
		  "ttl-eq-zero-during-transit"
		  );
		
		if (StringUtils.isNumeric(v)) {
			final int n = Integer.parseInt(v);
			return (0 <= n && n <= 255);
		}
		
		return codes.contains(v);
	}
	
	public static String interfaceNameForId(String name) {
		return name.replaceAll("[\\.\\/]", "_");
	}
	
	public static int string2Netmask(String str) throws UnknownHostException {
		InetAddress addr = InetAddress.getByName(str);
		int max = 32;
		int ret = 0;
		int mod;
		
		if (addr instanceof Inet6Address) {
			max = 128;
		}

		byte[] a = addr.getAddress();
		for (ret = 0, mod = 7; ret < max; ret++) {
			if ((a[ret / 8] & (1 << mod)) == 0)
				break;
			
			if (mod == 0) {
				mod = 7;
			}
			else {
				mod--;
			}
		}
		
		return ret;
	}
	
	public static boolean canInterfaceCreateSubIf(String name, DeviceConfig config) {
		if (name.startsWith("reth"))
			return true;
		
		if (!isPortPhysicalInterface(name))
			return false;
	
		Node phy = config.findPhysicalInterfaceNode(name);
		if (phy != null) {
			if (config.getNode(phy, "gigether-options/redundant-parent") != null)
				return false;
			
			if (config.getNode(phy, "unit/family/ethernet-switching") != null)
				return false;
		}
	
		if (config.getNode(config.interfacesNode(), "interface[normalize-space(fabric-options/member-interfaces/name/text())=%s]", XML.xpc(name)) != null)
			return false;
		
		return true;
	}
	
	public static boolean isPortPhysicalInterface(String name) {
		if (StringUtils.isBlank(name))
			return false;
		
		if (name.startsWith("ge-") 
				|| name.startsWith("fe-")
				|| name.startsWith("xe-")
				|| name.startsWith("et-"))
			return true;
		
		return false;
	}
	
	public static String makeMD5Password(String password) {
		return Md5Crypt.md5Crypt(password.getBytes());
	}
}
