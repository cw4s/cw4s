package to.lef.srxlib;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

import org.json.JSONObject;

public class SoftwareInformation extends XML {
	private List<Node> nodeList;
	
	public SoftwareInformation(Document doc) {
		super(doc);
		
		nodeList = new ArrayList<>();
		for (Node n : IterableNodeList.apply(getNodeList("//software-information"))) {
			nodeList.add(n);
		}
		
		if (nodeList.size() == 0)
			throw new IllegalArgumentException("Invalid software information.");
	}

	public int nodeCount() {
		return nodeList.size();
	}

	public final String getHostname() {
		return getHostname(0);
	}
	
	public final String getHostname(int nodeIdx) {
		return getString(nodeList.get(nodeIdx), "host-name/text()");
	}

	public final String getProductModel() {
		return getProductModel(0);
	}
	
	public final String getProductModel(int nodeIdx) {
		return getString(nodeList.get(nodeIdx), "product-model/text()");
	}
	
	public final String getPackageComment() {
		return getPackageComment(0);
	}
	
	public final String getPackageComment(int nodeIdx) {
		return getString(nodeList.get(nodeIdx), "package-information/comment/text()");
	}

	public final boolean isL2ng() {
		Version v = new Version(getPackageComment());
		if (v.getMajorVersion() > 15 || (v.getMajorVersion() == 15 && v.getMinorVersion() >= 1))
				return true;
		return false;
	}

	public final boolean isShipHidden() {
		Version v = new Version(getPackageComment());
		return v.isSipHiddenVersion();
	}
	
	public final int getMaxPolicyCount() {
		int ret = 80000;
		String model = getProductModel();
		
		if (StringUtils.isNotBlank(model)) {
			DeviceProfile prof = DeviceProfile.getInstance();
			model = model.toLowerCase();
			
			try {
				Map<String, JSONObject> dMap = prof.getDeviceMap();
				for(String key: dMap.keySet()) {
					if (model.length() >= key.length() && model.startsWith(key)) {
						JSONObject d = dMap.get(key);
						ret= d.getInt("policy_count_max");
					}
				}
			}
			catch (Exception e) {
				ret = 80000;
			}
		}

		return ret;
	}
}
