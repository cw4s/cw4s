package to.lef.srxlib;

import java.util.List;

public interface DeviceInformationProvider {
	public RouteInformation routeInformation() throws DeviceCacheException;
	public InterfaceInformation interfaceInformation() throws DeviceCacheException;
	public PhysicalInterfaceInformation physicalInterfaceInformation() throws DeviceCacheException;
	public ZonesInformation zonesInformation() throws DeviceCacheException;
	public SoftwareInformation softwareInformation() throws DeviceCacheException;
	public ChassisInventory chassisInventory() throws DeviceCacheException;
	public UsersInformation usersInformation() throws DeviceCacheException;
	public RouteEngineInformation routeEngineInformation() throws DeviceCacheException;
	public FlowSessionInformation flowSessionInformation() throws DeviceCacheException;
	public UptimeInformation uptimeInformation() throws DeviceCacheException;
	public AlarmInformation alarmInformation() throws DeviceCacheException;
	public PPPSummaryInformation pppSummaryInformation() throws DeviceCacheException;
	public List<IPSecSAInformation> ipsecSAInformationList() throws DeviceCacheException;
}
