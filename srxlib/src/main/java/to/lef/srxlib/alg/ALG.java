package to.lef.srxlib.alg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Node;

import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class ALG {
	public static final String ALG_DNS         = "dns";
	public static final String ALG_FTP         = "ftp";
	public static final String ALG_H323        = "h323";
	public static final String ALG_IKE_ESP_NAT = "ike-esp-nat";
	public static final String ALG_MGCP        = "mgcp";
	public static final String ALG_MSRPC       = "msrpc";
	public static final String ALG_PPTP        = "pptp";
	public static final String ALG_REAL        = "real";
	public static final String ALG_RSH         = "rsh";
	public static final String ALG_RTSP        = "rtsp";
	public static final String ALG_SCCP        = "sccp";
	public static final String ALG_SIP         = "sip";
	public static final String ALG_SQL         = "sql";
	public static final String ALG_SUNRPC      = "sunrpc";
	public static final String ALG_TALK        = "talk";
	public static final String ALG_TFTP        = "tftp";
	
	public static final List<String> algList() {
		List<String> ret = new ArrayList<String>();
		ret.add(ALG_DNS);
		ret.add(ALG_FTP);
		ret.add(ALG_H323);
		ret.add(ALG_IKE_ESP_NAT);
		ret.add(ALG_MGCP);
		ret.add(ALG_MSRPC);
		ret.add(ALG_PPTP);
		ret.add(ALG_REAL);
		ret.add(ALG_RSH);
		ret.add(ALG_RTSP);
		ret.add(ALG_SCCP);
		ret.add(ALG_SIP);
		ret.add(ALG_SQL);
		ret.add(ALG_SUNRPC);
		ret.add(ALG_TALK);
		ret.add(ALG_TFTP);
		return ret;
	}
	
	private Map<String,Boolean> algMap;

	public ALG() {
		algMap = new HashMap<String,Boolean>();
		for(String name: algList()) {
			algMap.put(name, (ALG_IKE_ESP_NAT.equals(name)) ? false : true);
		}
	}
	
	public void set(String name, boolean v) {
		if (!algMap.containsKey(name)) 
			throw new IllegalArgumentException("Unsupported alg: " + name);
		algMap.put(name, v);
	}
	
	public boolean get(String name) {
		if (!algMap.containsKey(name))
			throw new IllegalArgumentException("Unsupported alg: " + name);
		return algMap.get(name);
	}

	public List<String> enabledALGList() {
		List<String> ret = new ArrayList<>();
		
		for (String name : algList()) {
			if (get(name))
				ret.add(name);
		}
		return Collections.unmodifiableList(ret);
	}
	
	public final boolean isDnsEnabled() {
		return get(ALG_DNS);
	}
	public final void setDnsEnabled(boolean v) {
		set(ALG_DNS, v);
	}
	public final boolean isFtpEnabled() {
		return get(ALG_FTP);
	}
	public final void setFtpEnabled(boolean v) {
		set(ALG_FTP, v);
	}
	public final boolean isH323Enabled() {
		return get(ALG_H323);
	}
	public final void setH323Enabled(boolean v) {
		set(ALG_H323, v);
	}
	public final boolean isIkeEspNatEnabled() {
		return get(ALG_IKE_ESP_NAT);
	}
	public final void setIkeEspNatEnabled(boolean v) {
		set(ALG_IKE_ESP_NAT, v);
	}
	public final boolean isMgcpEnabled() {
		return get(ALG_MGCP);
	}
	public final void setMgcpEnabled(boolean v) {
		set(ALG_MGCP, v);
	}
	public final boolean isMsrpcEnabled() {
		return get(ALG_MSRPC);
	}
	public final void setMsrpcEnabled(boolean v) {
		set(ALG_MSRPC, v);
	}
	public final boolean isPptpEnabled() {
		return get(ALG_PPTP);
	}
	public final void setPptpEnabled(boolean v) {
		set(ALG_PPTP, v);
	}
	public final boolean isRealEnabled() {
		return get(ALG_REAL);
	}
	public final void setRealEnabled(boolean v) {
		set(ALG_REAL, v);
	}
	public final boolean isRshEnabled() {
		return get(ALG_RSH);
	}
	public final void setRshEnabled(boolean v) {
		set(ALG_RSH, v);
	}
	public final boolean isRtspEnabled() {
		return get(ALG_RTSP);
	}
	public final void setRtspEnabled(boolean v) {
		set(ALG_RTSP, v);
	}
	public final boolean isSccpEnabled() {
		return get(ALG_SCCP);
	}
	public final void setSccpEnabled(boolean v) {
		set(ALG_SCCP, v);
	}
	public final boolean isSipEnabled() {
		return get(ALG_SIP);
	}
	public final void setSipEnabled(boolean v) {
		set(ALG_SIP, v);
	}
	public final boolean isSqlEnabled() {
		return get(ALG_SQL);
	}
	public final void setSqlEnabled(boolean v) {
		set(ALG_SQL, v);
	}
	public final boolean isSunrpcEnabled() {
		return get(ALG_SUNRPC);
	}
	public final void setSunrpcEnabled(boolean v) {
		set(ALG_SUNRPC, v);
	}
	public final boolean isTalkEnabled() {
		return get(ALG_TALK);
	}
	public final void setTalkEnabled(boolean v) {
		set(ALG_TALK, v);
	}
	public final boolean isTftpEnabled() {
		return get(ALG_TFTP);
	}
	public final void setTftpEnabled(boolean v) {
		set(ALG_TFTP, v);
	}

	public boolean isInUse(String name, DeviceConfig cfg) {
		Node applications = cfg.applicationsNode();
		
		for(Node application : IterableNodeList.apply(cfg.getNodeList(applications, "application"))) {
			if (cfg.getNode(application, "term[normalize-space(alg/text())=%s]", XML.xpc(name)) != null)
				return true;
		}
		return false;
	}
	
	public static ALG createFromNode(Node alg, DeviceConfig cfg) {
		ALG ret = new ALG();

		for (String name: algList()) {
			Node n = cfg.getNode(alg, name);
			if (n != null) {
				if (ALG_IKE_ESP_NAT.equals(name)) {
					if (XML.isBooleanElementSet(n, "enable"))
						ret.algMap.put(name, true);
				}
				else {
					if (XML.isBooleanElementSet(n, "disable"))
						ret.algMap.put(name, false);
				}
			}
		}
		
		return ret;
	}
	
	public DeviceConfigure getBasicConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				Node alg = cfg.algNode();
				
				for (String name: algList()) {
					boolean v = algMap.get(name);
					Node n = null;
					
					if (ALG_IKE_ESP_NAT.equals(name)) {
						if (v) {
							n = cfg.createElementsIfNotExist(alg, name);
							cfg.setBooleanElement(n, "enable", true);
						}
						else {
							n = cfg.getNode(alg, name);
							if (n != null) {
								cfg.setBooleanElement(n, "enable", false);
							}
						}
					}
					else {
						if (v) {
							n = cfg.getNode(alg, name);
							if (n != null) {
								cfg.setBooleanElement(n, "disable", false);
							}
						}
						else {
							n = cfg.createElementsIfNotExist(alg, name);
							cfg.setBooleanElement(n, "disable", true);
						}					
					}
					
					if (n != null && cfg.getNode(n, "*") == null) {
						alg.removeChild(n);
					}
				}
				
				if (cfg.getNode(alg, "*") == null) {
					alg.getParentNode().removeChild(alg);
				}
			}
			
		};
	}
}
