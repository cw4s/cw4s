package to.lef.srxlib;


import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class ZonesInformation extends XML {
	List<Node> nodeList;
	
	public ZonesInformation(Document doc) {
		super(doc);
		
		nodeList = new ArrayList<>();
		for (Node n : IterableNodeList.apply(getNodeList("//zones-information"))) {
			nodeList.add(n);
		}
		
		if (nodeList.size() == 0) 
			throw new IllegalArgumentException("Invalid zones information.");
		
	}
	
	public String getZoneNameByInterface(String interfaceName) {
		for (Node no : nodeList) {
			Node n = getNode(no, "zones-security/zones-security-interfaces/zones-security-interface-name[normalize-space(text())=%s]/../..", xpc(interfaceName));
			if (n != null) 
				return getString(n, "zones-security-zonename/text()");
		}
		
		return null;
	}
}
