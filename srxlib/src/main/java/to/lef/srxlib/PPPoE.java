package to.lef.srxlib;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.nat.Dip;
import to.lef.srxlib.nic.LogicalInterface;
import to.lef.srxlib.nic.PPPoEInetInterface;
import to.lef.srxlib.route.StaticRoute;
import to.lef.srxlib.zone.SecurityZone;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class PPPoE {
	public static final String PHYSICAL_INTERFACE_NAME = "pp0";
	protected static final boolean PortPPPoEInterface = false;
	public static enum PPPOE_AUTH {
		NONE, CHAP, PAP, ANY
	};

	private int unitId;
	private PPPOE_AUTH authType;

	private String userName;
	private String password;

	private String underlyingInterfaceName;
	private String serviceName;
	private String accessConcentrator;
	private int autoReconnect;
	private int idleTimeout;

	private boolean propagatedToDhcp;
	
	public static PPPoE createFromNode(Node unit, DeviceConfig config) {
		try {
			int unitId = Integer.parseInt(config.getString(unit, "name/text()"));
			return createFromNode(unitId, unit, config);
		}
		catch (NumberFormatException e) {
			return null;
		}
	}
	
	public static PPPoE createFromNode(int unitId, Node unit, DeviceConfig config) {
		 PPPoE ret = new PPPoE(unitId);
		 Node ppp = config.getNode(unit, "ppp-options");
		 if (ppp != null) {
			 Node pap = config.getNode(ppp, "pap");
			 if (pap != null)  {
				 ret.authType = PPPOE_AUTH.PAP;
				 ret.userName = config.getString(pap, "local-name");
				 ret.password = config.getString(pap, "local-password");
			 }
			 
			 Node chap = config.getNode(ppp, "chap");
			 if (chap != null) {
				 ret.authType = PPPOE_AUTH.CHAP;
				 ret.userName = config.getString(chap, "local-name");
				 ret.password = config.getString(chap, "default-chap-secret");
			 }
			 
			 if (chap != null && pap != null) {
				 ret.authType = PPPOE_AUTH.ANY;
			 }
		 }
		 Node pppoe = config.getNode(unit, "pppoe-options");
		 if (pppoe != null) {
			 ret.underlyingInterfaceName = config.getString(pppoe, "underlying-interface/text()");
			 ret.serviceName = config.getString(pppoe, "service-name/text()");
			 ret.accessConcentrator = config.getString(pppoe, "access-concentrator/text()");

			 try {
				 ret.autoReconnect = Integer.valueOf(config.getString(pppoe, "auto-reconnect/text()"));
			 }
			 catch (NumberFormatException e) {
				 ret.autoReconnect = 0;
			 }

			 try {
				 ret.idleTimeout = Integer.valueOf(config.getString(pppoe, "idle-timeout/text()"));
			 }
			 catch (NumberFormatException e) {
				 ret.idleTimeout = 0;
			 }
		 }
		
		 if (config.getNode("system/services/dhcp/propagate-ppp-settings[normalize-space(text())=%s]", XML.xpc(ret.getName())) == null) {
			 ret.propagatedToDhcp = false;
		 }
		 else {
			 ret.propagatedToDhcp = true;
		 }
		 
		 return ret;
	 }

	public PPPoE(int unitId) {
		this.unitId = unitId;
		authType = PPPOE_AUTH.NONE;
		
		autoReconnect = 0;
		idleTimeout   = 0;
	}

	public final String getName() {
		 return String.format("%s.%d", PHYSICAL_INTERFACE_NAME, unitId);
	}
	
	public final int getUnitId() {
		return unitId;
	}
	
	public void setPPPOption(PPPOE_AUTH type, String userName, String password) {
		this.authType = type;
		this.userName = userName;
		this.password = password;
	}

	public void setPPPoEOption(String interfaceName, int autoReconnect, int idleTimeout, String accessConcentrator, String serviceName) {
		this.underlyingInterfaceName = interfaceName;
		this.autoReconnect = autoReconnect;
		this.idleTimeout = idleTimeout;
		this.accessConcentrator = accessConcentrator;
		this.serviceName = serviceName;
	}
	
	public final PPPOE_AUTH getAuthType() {
		return authType;
	}

	public final String getUserName() {
		return userName;
	}

	public final String getPassword() {
		return password;
	}
	
	public final String getUnderlyingInterfaceName() {
		return underlyingInterfaceName;
	}

	public final String getServiceName() {
		return serviceName;
	}

	public final String getAccessConcentrator() {
		return accessConcentrator;
	}

	public final int getAutoReconnect() {
		return autoReconnect;
	}

	public final int getIdleTimeout() {
		return idleTimeout;
	}

	public final boolean isPropagatedToDhcp() {
		return propagatedToDhcp;
	}

	public final void setPropagatedToDhcp(boolean propagatedToDhcp) {
		this.propagatedToDhcp = propagatedToDhcp;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + unitId;
		result = prime * result + ((authType == null) ? 0 : authType.hashCode());
		result = prime * result + autoReconnect;
		result = prime * result + idleTimeout;
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((serviceName == null) ? 0 : serviceName.hashCode());
		result = prime * result + ((underlyingInterfaceName == null) ? 0 : underlyingInterfaceName.hashCode());
		result = prime * result + ((accessConcentrator == null) ? 0 : accessConcentrator.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		result = prime * result + (propagatedToDhcp ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof PPPoE)) {
			return false;
		}
		PPPoE other = (PPPoE) obj;
		if (unitId != other.unitId) {
			return false;
		}
		if (authType != other.authType) {
			return false;
		}
		if (autoReconnect != other.autoReconnect) {
			return false;
		}
		if (idleTimeout != other.idleTimeout) {
			return false;
		}
		if (StringUtils.isBlank(password)) {
			if (StringUtils.isNotBlank(other.password)) {
				return false;
			}
		} else if (!password.equals(other.password)) {
			return false;
		}
		if (StringUtils.isBlank(serviceName)) {
			if (StringUtils.isNotBlank(other.serviceName)) {
				return false;
			}
		} else if (!serviceName.equals(other.serviceName)) {
			return false;
		}
		if (StringUtils.isBlank(underlyingInterfaceName)) {
			if (StringUtils.isNotBlank(other.underlyingInterfaceName)) {
				return false;
			}
		} else if (!underlyingInterfaceName.equals(other.underlyingInterfaceName)) {
			return false;
		}
		if (StringUtils.isBlank(accessConcentrator)) {
			if (StringUtils.isNotBlank(other.accessConcentrator)) {
				return false;
			}
		} else if (!accessConcentrator.equals(other.accessConcentrator)) {
			return false;
		}
		if (StringUtils.isBlank(userName)) {
			if (StringUtils.isNotBlank(other.userName)) {
				return false;
			}
		} else if (!userName.equals(other.userName)) {
			return false;
		}
		if (propagatedToDhcp != other.propagatedToDhcp) {
			return false;
		}
		
		return true;
	}


	public final DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					PPPoEInetInterface intf = new PPPoEInetInterface(PHYSICAL_INTERFACE_NAME, unitId);
					intf.getCreateConfigure().config(cfg, info);
					
					Node unit = cfg.findPPPoENode(unitId);
					if (unit == null) {
						throw new IllegalStateException("unit node is not found.");
					}
					
					configCreate(unit, cfg, info);
				}
				catch (DeviceConfigException e) {
					throw e;
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config PPPoE instance.", e);
				}
			}
			
		};
	}
	
	protected void configCreate(Node unit, DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
		configUnderlyingInterface(true, cfg, info);

		configPPP(unit, cfg);
		configPPPoE(unit, cfg);
		configPropagateToDhcp(cfg);

		cfg.deleteNode(unit, "family");
		cfg.createElementsIfNotExist(unit, "family", "inet", "negotiate-address");
	}

	public DeviceConfigure getEditConfigure(final PPPoE old) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				PPPoE self = PPPoE.this;
				
				try {
					Node unit = cfg.findLogicalInterfaceNode(PHYSICAL_INTERFACE_NAME, getUnitId());
					if (!self.underlyingInterfaceName.equals(old.underlyingInterfaceName)) {
						old.configUnderlyingInterface(false, cfg, info);
						self.configUnderlyingInterface(true, cfg, info);
					}
					
					configPPP(unit, cfg);
					configPPPoE(unit, cfg);
					configPropagateToDhcp(cfg);
				}
				catch (DeviceConfigException e) {
					throw e;
				}
				catch (Exception e) {
					throw new DeviceConfigException("Failed to config edit pppoe instance.", e);
				}
			}
			
		};
	}
	
	private void configPropagateToDhcp(DeviceConfig cfg) {
		 cfg.deleteNode("system/services/dhcp/propagate-ppp-settings[normalize-space(text())=%s]", XML.xpc(getName()));
		 if (propagatedToDhcp) {
			 Node dhcp = cfg.createElementsIfNotExist("system", "services", "dhcp");
			 dhcp.appendChild(cfg.createElement("propagate-ppp-settings", getName()));
		 }
	}
	
	private void configPPP(Node unit, DeviceConfig cfg) {
		final String userNameTag = "local-name";
		boolean authSet = false;
		Node ppp = cfg.createElementsIfNotExist(unit, "ppp-options");
		
		if (authType == PPPOE_AUTH.CHAP || authType == PPPOE_AUTH.ANY) {
			final String passwordTag = "default-chap-secret";
			
			if (authType != PPPOE_AUTH.ANY)
				cfg.deleteNode(ppp, "pap");
			
			Node chap = cfg.createElementsIfNotExist(ppp, "chap");
			Node passive = cfg.createElementsIfNotExist(chap, "passive");

			cfg.deleteNode(chap, userNameTag);
			chap.insertBefore(cfg.createElement(userNameTag, userName), passive);
		
			if (StringUtils.isNotBlank(password)) {
				cfg.deleteNode(chap, passwordTag);
				chap.insertBefore(cfg.createElement(passwordTag, password), passive);
			}
			
			authSet = true;
		}
		if (authType == PPPOE_AUTH.PAP || authType == PPPOE_AUTH.ANY) {
			final String passwordTag = "local-password";
		
			if (authType != PPPOE_AUTH.ANY)
				cfg.deleteNode(ppp, "chap");
			
			Node pap = cfg.createElementsIfNotExist(ppp, "pap");
			Node passive = cfg.createElementsIfNotExist(pap, "passive");
			
			cfg.deleteNode(pap, userNameTag);
			pap.insertBefore(cfg.createElement(userNameTag, userName), passive);
			
			if (StringUtils.isNotBlank(password)) {
				cfg.deleteNode(pap, passwordTag);
				pap.insertBefore(cfg.createElement(passwordTag, password), passive);		
			}
			
			authSet = true;
		}
		
		if (!authSet) {
			cfg.deleteNode(ppp, "chap");
			cfg.deleteNode(ppp, "pap");
		}
	}
	
	private void configPPPoE(Node unit, DeviceConfig cfg) {
		final String interfaceTag = "underlying-interface";
		final String autoConnectTag = "auto-reconnect";
		final String idleTimeoutTag = "idle-timeout";
		final String acTag = "access-concentrator";
		final String serviceTag = "service-name";
		
		Node pppoe = cfg.createElementsIfNotExist(unit, "pppoe-options");
	
		cfg.deleteNode(pppoe, interfaceTag);
		cfg.deleteNode(pppoe, autoConnectTag);
		cfg.deleteNode(pppoe, idleTimeoutTag);
		cfg.deleteNode(pppoe, acTag);
		cfg.deleteNode(pppoe, serviceTag);
		
		Node client = cfg.createElementsIfNotExist(pppoe, "client");
	
		pppoe.insertBefore(cfg.createElement(interfaceTag, underlyingInterfaceName), client);
		pppoe.insertBefore(cfg.createElement(autoConnectTag, "%d", autoReconnect), client);
		pppoe.insertBefore(cfg.createElement(idleTimeoutTag, "%d", idleTimeout), client);
		
		if (StringUtils.isNotBlank(accessConcentrator)) {
			pppoe.insertBefore(cfg.createElement(acTag, accessConcentrator), client);
		}

		if (StringUtils.isNotBlank(serviceName)) {
			pppoe.insertBefore(cfg.createElement(serviceTag, serviceName), client);
		}
	}


	public final DeviceConfigure getDeleteConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					LogicalInterface lint = cfg.findLogicalInterface(PHYSICAL_INTERFACE_NAME, unitId);
					if (lint != null && lint instanceof PPPoEInetInterface && lint.isRemovable(cfg)) {
//						configUnderlyingInterface(false, cfg); 複数のpp0にバインドされているかもしれないのでここでは消さない
						lint.getDeleteConfigure().config(cfg, info);
					}
					else {
						throw new IllegalStateException("PPPoE Interface can not remove.");
					}
				}
				catch (DeviceConfigException e) {
					throw e;
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config PPPoE instance.", e);
				}
			}
			
		};
	}
	
	private void configUnderlyingInterface(boolean doBound, DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
		final String encap = "encapsulation";
		String name = getName();
		
		Node unit = cfg.findLogicalInterfaceNode(underlyingInterfaceName);
		if (unit == null) {
			throw new IllegalArgumentException(String.format("Underlying Interface %s is not configured.", underlyingInterfaceName));
		}
		
		cfg.deleteNode(unit, encap);
		cfg.deleteNode(unit, "family");
		
		if (doBound) {
			SecurityZone.getDeleteInterfaceConfigure(underlyingInterfaceName).config(cfg, info);
			unit.appendChild(cfg.createElement(encap, "ppp-over-ether"));
			
			configDipFromInterface(underlyingInterfaceName, name, cfg);
			configVipFromInterface(underlyingInterfaceName, name, cfg);
			configMipFromInterface(underlyingInterfaceName, name, cfg);
			configProxyArpFromInterface(underlyingInterfaceName, name, cfg);
			configRouteNextHopInterface(underlyingInterfaceName, name, cfg);
		}
	}
	
	private void configVipFromInterface(String fromInterfaceName, String toInterfaceName, DeviceConfig cfg) {
		Node dnat = cfg.natDestinationNode();
		
		for(Node ruleSet: IterableNodeList.apply(cfg.getNodeList(dnat, "rule-set"))) {
			Node from = cfg.getNode(ruleSet, "from[normalize-space(interface/text())=%s]", XML.xpc(fromInterfaceName));
			if (from != null) {
				cfg.setValueElement(from, "interface", toInterfaceName);
			}
		}
	}
	
	private void configMipFromInterface(String fromInterfaceName, String toInterfaceName, DeviceConfig cfg) {
		Node snat = cfg.natStaticNode();
		
		for(Node ruleSet: IterableNodeList.apply(cfg.getNodeList(snat, "rule-set"))) {
			Node from = cfg.getNode(ruleSet, "from[normalize-space(interface/text())=%s]", XML.xpc(fromInterfaceName));
			if (from != null) {
				cfg.setValueElement(from, "interface", toInterfaceName);
			}		
		}	
	}

	private void configDipFromInterface(String fromInterfaceName, String toInterfaceName, DeviceConfig cfg) {
		Node snat = cfg.natSourceNode();
		LogicalInterface lint = cfg.findLogicalInterface(fromInterfaceName);
				
		for (Node pool: IterableNodeList.apply(cfg.getNodeList(snat, "pool"))) {
			try {
				Dip dip = Dip.createFromNode(pool, cfg);
				if (dip.isBelongsTo(lint)) {
					String desc = dip.getDescription();
					if (StringUtils.isNotBlank(desc) && !desc.equals(fromInterfaceName)) 
						continue;
					cfg.deleteNode(pool, "description");
					pool.appendChild(cfg.createElement("description", toInterfaceName));
				}
			}
			catch (Exception e) {
				
			}
		}
	}
	
	private void configProxyArpFromInterface(String fromInterfaceName, String toInterfaceName, DeviceConfig cfg) {
		Node n = cfg.natProxyArpNode();
		for (Node intNode: IterableNodeList.apply(cfg.getNodeList(n, "interface"))) {
			if (fromInterfaceName.equals(cfg.getString(intNode, "name/text()"))) {
				cfg.setValueElement(intNode, "name", toInterfaceName);
			}
		}
		
		n = cfg.natProxyNdpNode();
		for (Node intNode: IterableNodeList.apply(cfg.getNodeList(n, "interface"))) {
			if (fromInterfaceName.equals(cfg.getString(intNode, "name/text()"))) {
				cfg.setValueElement(intNode, "name", toInterfaceName);
			}
		}	
	}
	
	private void configRouteNextHopInterface(String fromInterfaceName, String toInterfaceName, DeviceConfig cfg) {
		Node ro = cfg.routingOptionsNode();
		
		for (Node route: IterableNodeList.apply(cfg.getNodeList(ro, "static/route"))) {
			Node qnh = cfg.getNode(route, "qualified-next-hop");
			if (qnh != null && fromInterfaceName.equals(cfg.getString(qnh, "interface/text()"))) {
				cfg.setValueElement(qnh, "interface", toInterfaceName);
			}
		}

		for (Node rib : IterableNodeList.apply(cfg.getNodeList(ro, "rib"))) {
			for (Node route: IterableNodeList.apply(cfg.getNodeList(rib, "static/route"))) {
				Node qnh = cfg.getNode(route, "qualified-next-hop");
				if (qnh != null && fromInterfaceName.equals(cfg.getString(qnh, "interface/text()"))) {
					cfg.setValueElement(qnh, "interface", toInterfaceName);
				}
			}
		}
	}
	
	public int isChanged(DeviceConfig candidate) {
		PPPoE c = candidate.findPPPoE(unitId);
		if (c == null)
			return 1;
		if (!equals(c)) 
			return 2;
		return 0;
	}
	
	public boolean isRemovable(DeviceConfig cfg) {
		LogicalInterface lint = cfg.findLogicalInterface(PHYSICAL_INTERFACE_NAME, unitId);
		if (lint == null)
			return true;
		return lint.isRemovable(cfg);
	}
	
	public List<StaticRoute> findDefaultRouteList(DeviceConfig config) {
		List<StaticRoute> ret = new ArrayList<>();
	
		for (StaticRoute r : config.staticRouteList(StaticRoute.DEFAULT_INET_RIB_NAME)) {
			if ("0.0.0.0/0".equals(r.getName()) && StringUtils.isBlank(r.getNextHop()) && getName().equals(r.getInterfaceName())) {
				ret.add(r);
			}
		}
		
		return ret;
	}
}