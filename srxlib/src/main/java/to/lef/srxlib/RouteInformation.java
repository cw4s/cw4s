package to.lef.srxlib;

import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import to.lef.srxlib.dom.XML;
import to.lef.srxlib.route.Inet6StaticRoute;
import to.lef.srxlib.route.InetStaticRoute;
import to.lef.srxlib.route.StaticRoute;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collections;
import java.util.concurrent.Callable;
public class RouteInformation extends XML {
	public static final String PROTOCOL_NAME_LOCAL  = "Local";
	public static final String PROTOCOL_NAME_DIRECT = "Direct";
	public static final String PROTOCOL_NAME_STATIC = "Static";
	
	private List<RouteEntry> routeEntryList;
	
	@DatabaseTable(tableName = "route_information")
	public static class RouteEntry {
		public static final int CONFIG_STATUS_NONE     = 0;
		public static final int CONFIG_STATUS_REMOVED  = 1;
		public static final int CONFIG_STATUS_EXIST    = 2;
		public static final int CONFIG_STATUS_NEW      = 3;
		
		public static final String TABLE_FIELD_NAME = "tableName";
		public static final String DESTINATION_FIELD_NAME = "destination";
		public static final String PROTOCOL_FIELD_NAME = "protocol";
		public static final String GATEWAY_FIELD_NAME = "gateway";
		public static final String INTERFACE_FIELD_NAME = "interface";
		
		@DatabaseField(generatedId = true)
		private Integer id;
		@DatabaseField(columnName = TABLE_FIELD_NAME, canBeNull = false)
		private String tableName;
		@DatabaseField(columnName = DESTINATION_FIELD_NAME, indexName = "idx_route_entry", canBeNull = false)
		private String destination;
		@DatabaseField(columnName = PROTOCOL_FIELD_NAME, indexName = "idx_route_entry")
		private String protocolName;
		@DatabaseField(columnName = GATEWAY_FIELD_NAME, indexName = "idx_route_entry")
		private String gateway;
		@DatabaseField(columnName = INTERFACE_FIELD_NAME, indexName = "idx_route_entry")
		private String interfaceName;
		@DatabaseField
		private String configGateway;
		@DatabaseField
		private String configInterfaceName;
		@DatabaseField
		private Integer preference;
		@DatabaseField
		private Integer configPreference;
		@DatabaseField
		private Integer metric;
		@DatabaseField
		private Integer configMetric;
		@DatabaseField
		private Integer tag;
		@DatabaseField
		private Integer configTag;
		@DatabaseField(canBeNull = false)
		boolean selected;
		@DatabaseField(canBeNull = false)
		boolean active;
		@DatabaseField(canBeNull = false)
		int configStatus;
		
		public RouteEntry() {
			// ORMLite needs a no-arg constructor
		}

		public RouteEntry(String tableName, String destination, String gateway, String protocolName, String interfaceName, 
				Integer preference, Integer metric, Integer tag, 
				boolean selected, boolean active, int configStatus) {
			this.tableName     = tableName;
			this.destination   = destination;
			this.gateway       = gateway;
			this.protocolName  = protocolName;
			this.interfaceName = interfaceName;
			this.preference    = preference;
			this.metric        = metric;
			this.tag           = tag;
			this.selected      = selected;
			this.active        = active;
			this.configStatus  = configStatus;
		}

		public RouteEntry(StaticRoute route)  {
			if (route instanceof InetStaticRoute) {
				this.tableName = "inet.0";
			}
			else if (route instanceof Inet6StaticRoute) {
				this.tableName = "inet6.0";
			}
			this.destination   = route.getName();
			this.protocolName  = PROTOCOL_NAME_STATIC;
//			this.gateway       = (StringUtils.isBlank(route.getNextHop())) ? null : route.getNextHop();
//			this.interfaceName = (StringUtils.isBlank(route.getInterfaceName())) ? null : route.getInterfaceName();
			this.active        = false;
			this.selected      = false;
			this.configStatus  = CONFIG_STATUS_NEW;
			set(route);
		}
	
		public void set(StaticRoute route) {
			this.configGateway       = (StringUtils.isBlank(route.getNextHop())) ? null : route.getNextHop();
			this.configInterfaceName = (StringUtils.isBlank(route.getInterfaceName())) ? null : route.getInterfaceName();
			this.configPreference    = route.getPreference();
			this.configMetric        = route.getMetric();
			this.configTag           = route.getTag();
			
			if (this.configStatus != CONFIG_STATUS_NEW)
				this.configStatus = CONFIG_STATUS_EXIST;
		}
		
		public final Integer getId() {
			return id;
		}

		public final String getTableName() {
			return tableName;
		}

		public final String getDestination() {
			return destination;
		}

		public final String getGateway() {
			return gateway;
		}
		
		public final String getConfigGateway() {
			return configGateway;
		}

		public final String getProtocolName() {
			return protocolName;
		}

		public final String getInterfaceName() {
			return interfaceName;
		}

		public final String getConfigInterfaceName() {
			return configInterfaceName;
		}

		public final Integer getPreference() {
			return preference;
		}

		public final Integer getConfigPreference() {
			return configPreference;
		}

		public final Integer getMetric() {
			return metric;
		}

		public final Integer getConfigMetric() {
			return configMetric;
		}

		public final Integer getTag() {
			return tag;
		}

		public final Integer getConfigTag() {
			return configTag;
		}

		public final boolean isSelected() {
			return selected;
		}

		public final boolean isActive() {
			return active;
		}

		public final int getConfigStatus() {
			return configStatus;
		}

		public final boolean isNew() {
			return (configStatus ==  CONFIG_STATUS_NEW);
		}
		
		public final boolean isRemoved() {
			return (configStatus ==  CONFIG_STATUS_REMOVED);
		}
		
		public final boolean isExist() {
			return (configStatus ==  CONFIG_STATUS_EXIST);
		}
		
		public final boolean isStatic() {
			return (PROTOCOL_NAME_STATIC.equals(protocolName));
		}
	}

	public static RouteInformation createFromDocument(Document doc, Element root, Device.DatabaseHandler dbh, DeviceConfig cfg) throws Exception {
		RouteInformation ret = null;

		ret = new RouteInformation(doc, root);
		dbh.dropTable(RouteInformation.RouteEntry.class, true);
		dbh.createTable(RouteInformation.RouteEntry.class);

		Dao<RouteInformation.RouteEntry, Integer> dao = dbh.createDao(RouteInformation.RouteEntry.class);
		ret.updateRouteEntries(dao);
		ret.updateRouteEntries(dao, cfg);

		QueryBuilder<RouteEntry, Integer> qb = dao.queryBuilder();
		qb
		.orderBy("destination", true)
		.orderBy("gateway", true);

		ret.routeEntryList = qb.query();

		return ret;
	}

	private void updateRouteEntries(final Dao<RouteEntry, Integer> dao, final DeviceConfig cfg) throws Exception {
		dao.callBatchTasks(new Callable<Void>() {

			@SuppressWarnings("unchecked")
			@Override
			public Void call() throws SQLException {
				List<StaticRoute> staticRouteList = cfg.staticRouteList();

				for (StaticRoute route: staticRouteList) {
					QueryBuilder<RouteEntry, Integer> qb = dao.queryBuilder();
					Where<RouteEntry, Integer> where = qb.where();

					if (StringUtils.isNotBlank(route.getInterfaceName())) {
						where.and(
								where.eq(RouteEntry.TABLE_FIELD_NAME, route.getRibName()),
								where.eq(RouteEntry.DESTINATION_FIELD_NAME, route.getName()),
								where.eq(RouteEntry.PROTOCOL_FIELD_NAME, PROTOCOL_NAME_STATIC),
								(StringUtils.isBlank(route.getNextHop())) ? where.or(where.isNull(RouteEntry.GATEWAY_FIELD_NAME), where.eq(RouteEntry.GATEWAY_FIELD_NAME,  "")) : where.eq(RouteEntry.GATEWAY_FIELD_NAME, route.getNextHop()),
										(StringUtils.isBlank(route.getInterfaceName())) ? where.or(where.isNull(RouteEntry.INTERFACE_FIELD_NAME), where.eq(RouteEntry.INTERFACE_FIELD_NAME, "")) : where.eq(RouteEntry.INTERFACE_FIELD_NAME, route.getInterfaceName())
								);
					}
					else {
						where.and(
								where.eq(RouteEntry.TABLE_FIELD_NAME, route.getRibName()),
								where.eq(RouteEntry.DESTINATION_FIELD_NAME, route.getName()),
								where.eq(RouteEntry.PROTOCOL_FIELD_NAME, PROTOCOL_NAME_STATIC),
								(StringUtils.isBlank(route.getNextHop())) ? where.or(where.isNull(RouteEntry.GATEWAY_FIELD_NAME), where.eq(RouteEntry.GATEWAY_FIELD_NAME,  "")) : where.eq(RouteEntry.GATEWAY_FIELD_NAME, route.getNextHop())
								);						
					}

					List<RouteEntry> list = qb.query();
					if (list.size() > 0) {
						for (RouteEntry entry: list) {
							entry.set(route);
							dao.update(entry);
						}
					}
					else {
						RouteEntry entry = new RouteEntry(route);
						dao.create(entry);
					}
				}
				
				return null;
			}
			
		});
	}
	
	private void updateRouteEntries(final Dao<RouteEntry, Integer> dao) throws Exception {
		dao.callBatchTasks(new Callable<Void>() {

			@Override
			public Void call() throws SQLException {
				NodeList routeTables = getNodeList("route-table");
				for (int i = 0; i < routeTables.getLength(); i++) {
					Node routeTable = routeTables.item(i);

					final String tableName = getString(routeTable, "table-name/text()");

					NodeList rtList = getNodeList(routeTable, "rt");
					for (int j = 0; j < rtList.getLength(); j++) {
						Node rt = rtList.item(j);

						final String destination  = getString(rt, "rt-destination/text()");
						final String prefixLength = getString(rt, "rt-prefix-length/text()");

						NodeList entryList = getNodeList(rt, "rt-entry");
						for (int k = 0; k < entryList.getLength(); k++) {
							Node rtEntry = entryList.item(k);

							final String  protocolName  = getString(rtEntry, "protocol-name/text()");
							final String  preference    = getString(rtEntry, "preference/text()");
							final String  metric        = getString(rtEntry, "metric/text()");
							final String  tag           = getString(rtEntry, "rt-tag/text()");
							final boolean currentActive = isBooleanElementSet(rtEntry, "current-active");

							NodeList nhList = getNodeList(xpath, rtEntry, String.format("nh/nh-string[normalize-space(text())=%s]/..", xpc("Next hop")));
							for (int l = 0; l < nhList.getLength(); l++) {
								Node nh = nhList.item(l);

								final String gateway = getString(xpath, nh, "to/text()");
								String interfaceName = getString(xpath, nh, "via/text()");
								if (StringUtils.isBlank(interfaceName)) {
									interfaceName = getString(xpath, nh, "nh-local-interface/text()");
								}
								final boolean selected = isBooleanElementSet(nh, "selected-next-hop");

								try {
									RouteEntry entry = new RouteEntry(
											tableName,
											String.format("%s/%s", Util.fixInetAddressString(destination), prefixLength),
											(StringUtils.isBlank(gateway)) ? null : gateway,
											(StringUtils.isBlank(protocolName)) ? null : protocolName,
											(StringUtils.isBlank(interfaceName)) ? null : interfaceName,
											(StringUtils.isNumeric(preference)) ? Integer.valueOf(preference) : null,
											(StringUtils.isNumeric(metric)) ? Integer.valueOf(metric) : null,
											(StringUtils.isNumeric(tag)) ? Integer.valueOf(tag) : null,
											selected,
											currentActive,
											(PROTOCOL_NAME_STATIC.equals(protocolName)) ? RouteEntry.CONFIG_STATUS_REMOVED : RouteEntry.CONFIG_STATUS_NONE
											);

									dao.create(entry);
								} catch (NumberFormatException e) {
									e.printStackTrace();
								} catch (UnknownHostException e) {
									e.printStackTrace();
								} 
							}
						}
					}
				}

				return null;
			}
		});
		

	}
	
	private RouteInformation(Document doc, Element root) {
		super(doc, root);
		routeEntryList = new ArrayList<RouteEntry>();
	}
	
	public final List<RouteEntry> getRouteEntryList() {
		return Collections.unmodifiableList(routeEntryList);
	}
}
