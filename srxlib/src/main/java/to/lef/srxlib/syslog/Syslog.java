package to.lef.srxlib.syslog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public abstract class Syslog {
	public static final String FACILITY_ANY = "any";
	public static final String FACILITY_AUTHORIZATION = "authorization";
	public static final String FACILITY_CHANGE_LOG = "change-log";
	public static final String FACILITY_CONFLICT_LOG = "conflict-log";
	public static final String FACILITY_DAEMON = "daemon";
	public static final String FACILITY_DFC = "dfc";
	public static final String FACILITY_EXTERNAL = "external";
	public static final String FACILITY_FIREWALL = "firewall";
	public static final String FACILITY_FTP = "ftp";
	public static final String FACILITY_INTERACTIVE_COMMANDS = "interactive-commands";
	public static final String FACILITY_KERNEL = "kernel";
	public static final String FACILITY_NTP = "ntp";
	public static final String FACILITY_PFE = "pfe";
	public static final String FACILITY_SECURITY = "security";
	public static final String FACILITY_USER = "user";
	
	public static final String LEVEL_ANY = "any";
	public static final String LEVEL_ALERT = "alert";
	public static final String LEVEL_CRITICAL = "critical";
	public static final String LEVEL_EMERGENCY = "emergency";
	public static final String LEVEL_ERROR = "error";
	public static final String LEVEL_INFO = "info";
	public static final String LEVEL_NONE = "none";
	public static final String LEVEL_NOTICE = "notice";
	public static final String LEVEL_WARNING = "warning";

	protected static final String TAG_HOST = "host";
	protected static final String TAG_FILE = "file";
	protected static final String RT_FLOW_SESSION = "RT_FLOW_SESSION";
			
	protected String name;
	protected String facility;
	protected String level;
	protected boolean trafficLog;

	public static List<String> facilityList() {
		return Arrays.asList(
				FACILITY_ANY,
				FACILITY_AUTHORIZATION,
				FACILITY_CHANGE_LOG,
				FACILITY_CONFLICT_LOG,
				FACILITY_DAEMON,
				FACILITY_DFC,
				FACILITY_EXTERNAL,
				FACILITY_FIREWALL,
				FACILITY_FTP,
				FACILITY_INTERACTIVE_COMMANDS,
				FACILITY_KERNEL,
				FACILITY_NTP,
				FACILITY_PFE,
				FACILITY_SECURITY,
				FACILITY_USER
				);
	}

	public static List<String> levelList() {
		return Arrays.asList(
				LEVEL_EMERGENCY,
				LEVEL_ALERT,
				LEVEL_CRITICAL,
				LEVEL_ERROR,
				LEVEL_WARNING,
				LEVEL_NOTICE,
				LEVEL_INFO,
				LEVEL_ANY,
				LEVEL_NONE
				);
	}

	public static List<Syslog> createFromNode(Node node, DeviceConfig config) {
		List<Syslog> ret = new ArrayList<Syslog>();
		List<String> levelList = levelList();
		
		String tagName = ((Element)node).getTagName();
		
		String name = config.getString(node, "name/text()");
		if (StringUtils.isBlank(name))
			return ret;
		
		boolean isTrafficLog = (config.getNode(node, "match[normalize-space(text())=%s]", XML.xpc(RT_FLOW_SESSION)) != null);
		
		if (TAG_HOST.equals(tagName)) {
			Integer port = null;
			String p = config.getString(node, "port");
			if (StringUtils.isNumeric(p))
				port = Integer.valueOf(p);
		
			String srcAddr = config.getString(node, "source-address");
			
			for (Node contentsNode : IterableNodeList.apply(config.getNodeList(node, "contents"))) {
				String facility = FACILITY_ANY;
				String level = LEVEL_NONE;
				
				if (contentsNode != null) {
					facility = config.getString(contentsNode, "name/text()");
			
					for (String l : levelList) {
						if (config.getNode(contentsNode, l) != null) {
							level = l;
							break;
						}
					}
				}
				ret.add(new SyslogHost(name, facility, level, port, srcAddr, isTrafficLog));
			}
		}
		else if (TAG_FILE.equals(tagName)) {
			boolean isStructuredData = (config.getNode(node, "structured-data") != null);

			for (Node contentsNode : IterableNodeList.apply(config.getNodeList(node, "contents"))) {
				String facility = FACILITY_ANY;
				String level = LEVEL_NONE;
				
				if (contentsNode != null) {
					facility = config.getString(contentsNode, "name/text()");
			
					for (String l : levelList) {
						if (config.getNode(contentsNode, l) != null) {
							level = l;
							break;
						}
					}
				}
				ret.add(new SyslogFile(name, facility, level, isStructuredData, isTrafficLog));
			}
		}
		
		return ret;
	}
	
	public Syslog(String name, String facility, String level, boolean trafficLog) {
		this.name = name;
		this.facility = facility;
		this.level = level;
		this.trafficLog = trafficLog;
	}

	public final String getName() {
		return name;
	}

	public final String getFacility() {
		return facility;
	}

	public final String getLevel() {
		return level;
	}

	public final boolean isTrafficLog() {
		return trafficLog;
	}

	protected void configSyslog(Node n, DeviceConfig config) {
//		config.deleteNode(n, "contents");

		if (StringUtils.isNotBlank(facility)) {
			Node contents = config.getNode(n, "contents[normalize-space(name/text())=%s]", XML.xpc(facility));
			if (contents == null) {
				contents = config.createElement("contents");
				contents.appendChild(config.createElement("name", facility));
				n.appendChild(contents);
			}
			
			for (String l : levelList()) {
				config.deleteNode(contents, l);
			}
			if (StringUtils.isNotBlank(level))
				contents.appendChild(config.createElement(level));
		}
		
		if (trafficLog) {
			config.deleteNode(n, "match");
			n.appendChild(config.createElement("match", RT_FLOW_SESSION));
		} 
		else {
			config.deleteNode(n, "match[normalize-space(text())=%s]", XML.xpc(RT_FLOW_SESSION));
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((facility == null) ? 0 : facility.hashCode());
		result = prime * result + ((level == null) ? 0 : level.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (trafficLog ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Syslog)) {
			return false;
		}
		Syslog other = (Syslog) obj;
		if (StringUtils.isBlank(facility)) {
			if (StringUtils.isNotBlank(other.facility)) {
				return false;
			}
		} else if (!facility.equals(other.facility)) {
			return false;
		}
		if (StringUtils.isBlank(level)) {
			if (StringUtils.isNotBlank(other.level)) {
				return false;
			}
		} else if (!level.equals(other.level)) {
			return false;
		}
		if (StringUtils.isBlank(name)) {
			if (StringUtils.isNotBlank(other.name)) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (trafficLog != other.trafficLog) {
			return false;
		}
		return true;
	}

	public static DeviceConfigure getDeleteAllConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				config.deleteNode(config.syslogNode(), "host|file");
			}
			
		};
	}

	public static DeviceConfigure getLogModeConfigure(final boolean isStreamMode) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				Node n = config.createElementsIfNotExist("security", "log");
				config.deleteNode(n, "mode");
				
				if (isStreamMode) {
					n.appendChild(config.createElement("mode", "stream"));
				}
				else {
					n.appendChild(config.createElement("mode", "event"));
				}
			}
			
		};
	}
	
	abstract public DeviceConfigure getCreateConfigure();
//	abstract public DeviceConfigure getEditConfigure();
	abstract public DeviceConfigure getDeleteConfigure();
	

}
