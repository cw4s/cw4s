package to.lef.srxlib.syslog;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.Util;
import to.lef.srxlib.dom.XML;

public class SyslogHost extends Syslog {
	private Integer port;
	private String sourceAddress;
	
	public SyslogHost(String name, String facility, String level, Integer port, String srcAddr, boolean trafficLog) {
		super(name, facility, level, trafficLog);
		this.port = port;
		this.sourceAddress = srcAddr;
	}

	public final Integer getPort() {
		return port;
	}

	public final String getSourceAddress() {
		return sourceAddress;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((port == null) ? 0 : port.hashCode());
		result = prime * result + ((sourceAddress == null) ? 0 : sourceAddress.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof SyslogHost)) {
			return false;
		}
		SyslogHost other = (SyslogHost) obj;
		if (port == null) {
			if (other.port != null) {
				return false;
			}
		} else if (!port.equals(other.port)) {
			return false;
		}
		if (StringUtils.isBlank(sourceAddress)) {
			if (StringUtils.isNotBlank(other.sourceAddress)) {
				return false;
			}
		} else if (!sourceAddress.equals(other.sourceAddress)) {
			return false;
		}
		return true;
	}

	@Override
	public DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					if (StringUtils.isBlank(name) || !Util.isValidIdentifier(name))
						throw new IllegalArgumentException("Invalid IP address/hostname");
		
					Node syslogNode = config.syslogNode();
					Node n = config.getNode(syslogNode, "host[normalize-space(name/text())=%s]", XML.xpc(name));
					if (n == null) {
						n = config.createElement("host");
						n.appendChild(config.createElement("name", name));
						syslogNode.appendChild(n);
					}
				
					configSyslog(n, config);

				}
				catch (Exception e) {
					throw new DeviceConfigException("Failed to config Syslog host.", e);
				}
			}
			
		};
	}
/*	
	@Override
	public DeviceConfigure getEditConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					if (StringUtils.isBlank(name) || !Util.isValidIdentifier(name))
						throw new IllegalArgumentException("Invalid IP address/hostname");
			
					Node n = config.getNode(config.syslogNode(), "host[normalize-space(name/text())=%s]", XML.xpc(name));
					if (n == null)
						throw new IllegalArgumentException("Syslog host is not found.");
					
					configSyslog(n, config);
				}
				catch (Exception e) {
					throw new DeviceConfigException("Failed to config Syslog host.", e);
				}
			}
			
		};
	}
*/
	@Override
	public DeviceConfigure getDeleteConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				Node syslogNode = config.syslogNode();
				Node n = config.getNode(syslogNode, "host[normalize-space(name/text())=%s]", XML.xpc(name));

				if (n != null) {
					if (StringUtils.isNotBlank(facility)) {
						config.deleteNode(n, "contents[normalize-space(name/text())=%s]", XML.xpc(facility));
					}

					if (config.getNodeList(n, "contents").getLength() == 0) {
						syslogNode.removeChild(n);
					}
				}
			}
		};
	}

	@Override
	protected void configSyslog(Node n, DeviceConfig config) {
		super.configSyslog(n, config);
	
		config.deleteNode(n, "port");
		if (port != null) {
			n.appendChild(config.createElement("port", "%d", port));
		}
					
		config.deleteNode(n, "source-address");
		if (StringUtils.isNotBlank(sourceAddress)) {
			n.appendChild(config.createElement("source-address", sourceAddress));
		}
	}
}
