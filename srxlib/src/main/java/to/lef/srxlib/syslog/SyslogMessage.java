package to.lef.srxlib.syslog;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;

public class SyslogMessage {
	private final static Pattern processNamePattern = Pattern.compile("^(.+?)(?:\\[(\\d+)\\])?:$");
	private final static Pattern tagPattern = Pattern.compile("^([A-Z_]+):\\s(.+)$");
	private final Date timestamp;
	private final String hostname;
	private final String processName;
	private final String processId;
	private final String tag;
	private final String message;

	public static List<SyslogMessage> createFromLines(String lines) {
		List<SyslogMessage> ret = new ArrayList<>();
	
		if (StringUtils.isNotBlank(lines)) {
			for (String line: lines.split("\\n")) {
				SyslogMessage msg = createFromLine(line);
				if (msg != null)
					ret.add(msg);
			}
		}
		return ret;
	}
	
	public static SyslogMessage createFromLine(String line) {
		Date timestamp = null;
		String hostname = null;
		String processName = null;
		String processId = null;
		String tag = null;
		String message = null;

		if (StringUtils.isBlank(line))
			return null;
		
		if (line.startsWith("<")) {
			String[] tokens = line.split("\\s+", 7);
			if (tokens.length == 7) {
				try {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
					ParsePosition pp = new ParsePosition(0);				
					timestamp = sdf.parse(tokens[1], pp);
					// XXX DateUtilsを使うとうまくparseできない？？
//					timestamp = DateUtils.parseDate(tokens[1], "yyyy-MM-dd'T'HH:mm:ss.SSSX");
				}
				catch (Exception e) {
					timestamp = null;
				}

				if (timestamp != null && StringUtils.isNotBlank(tokens[6])) {
					String tmp = tokens[6];
					if (tmp.startsWith("[")) {
						int idx = tmp.indexOf("]");
						if (idx != -1) {
							message = tmp.substring(idx + 2);
						}
					}
					else {
						message = tmp.substring(0, 2);
					}
					
					if (StringUtils.isNotBlank(message))
						return new SyslogMessage(timestamp, tokens[2], tokens[3], tokens[4], tokens[5], message);
				}
			}
		} 
		else {
			String[] tokens = line.split("\\s+");
			if (tokens.length > 3) {
				if (StringUtils.isNumeric(tokens[3])) {
					try {
						timestamp = DateUtils.parseDate(String.format("%s %s %s %s", tokens[0], tokens[1], tokens[2], tokens[3]), 
								Locale.ENGLISH, "MMM dd HH:mm:ss.SSS yyyy", "MMM dd HH:mm:ss yyyy");

						hostname = tokens[4];
						processName = tokens[5];
						message = StringUtils.join(tokens, " ", 6, tokens.length);
					} 
					catch (Exception e) {
						timestamp = null;
					}
				}
				
				if (timestamp == null)	{
					try {
						timestamp = DateUtils.parseDate(String.format("%s %s %s", tokens[0], tokens[1], tokens[2]), 
								Locale.ENGLISH, "MMM dd HH:mm:ss.SSS", "MMM dd HH:mm:ss");
						hostname = tokens[3];
						processName = tokens[4];
						message = StringUtils.join(tokens, " ", 5, tokens.length);	
					}
					catch (Exception e) {
						timestamp = null;
					}
				}
				
				if (timestamp == null)
					return null;
			
				if (StringUtils.isNotBlank(processName)) {
					Matcher matcher = processNamePattern.matcher(processName);
					
					if (matcher.find()) {
						processName = matcher.group(1);
						if (matcher.groupCount() > 1)
							processId = matcher.group(2);
					}
				}
			
				if (StringUtils.isNotBlank(message)) {
					Matcher matcher = tagPattern.matcher(message);
					if (matcher.find()) {
						tag = matcher.group(1);
						message = matcher.group(2);
					}
				}
				
				return new SyslogMessage(timestamp, hostname, processName, processId, tag, message);
			}
		}
		
		return null;
	}
	
	public SyslogMessage(Date timestamp, String hostname, String processName, String processId, String tag, String message) {
		this.timestamp = timestamp;
		this.hostname = hostname;
		this.processName = processName;
		this.processId = processId;
		this.tag = tag;
		this.message = message;
	}

	public final Date getTimestamp() {
		return timestamp;
	}

	public final String getHostname() {
		return hostname;
	}
	
	public final String getProcessName() {
		return processName;
	}

	public final String getProcessId() {
		return processId;
	}

	public final String getTag() {
		return tag;
	}
	
	public final String getMessage() {
		return message;
	}
}
