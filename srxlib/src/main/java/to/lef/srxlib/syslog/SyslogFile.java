package to.lef.srxlib.syslog;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.Util;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.DeviceInformationProvider;

public class SyslogFile extends Syslog {
	private boolean structuredData;
	
	public SyslogFile(String name, String facility, String level, boolean structuredData, boolean trafficLog) {
		super(name, facility, level, trafficLog);
		this.structuredData = structuredData;
	}
	
	public final boolean isStructuredData() {
		return structuredData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (structuredData ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof SyslogFile)) {
			return false;
		}
		SyslogFile other = (SyslogFile) obj;
		if (structuredData != other.structuredData) {
			return false;
		}
		return true;
	}
	
	@Override
	protected void configSyslog(Node n, DeviceConfig config) {
		super.configSyslog(n, config);
		
		config.deleteNode(n, "structured-data");
		if (structuredData)
			n.appendChild(config.createElement("structured-data"));
	}

	@Override
	public DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					if (StringUtils.isBlank(name) || !Util.isValidIdentifier(name))
						throw new IllegalArgumentException("Invalid filename");

					if (!trafficLog && isTrafficLogName(config, name))
						throw new IllegalArgumentException(name + " is used as a traffic log.");
					
					Node syslogNode = config.syslogNode();
					Node n = config.getNode(syslogNode, "file[normalize-space(name/text())=%s]", XML.xpc(name));
					if (n == null) {
						n = config.createElement("file");
						n.appendChild(config.createElement("name", name));
						syslogNode.appendChild(n);
					}
					configSyslog(n, config);
				}
				catch (Exception e) {
					throw new DeviceConfigException("Failed to config Syslog file.", e);
				}
			}
			
		};
	}
	
/*	
	@Override
	public DeviceConfigure getEditConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					if (StringUtils.isBlank(name) || !Util.isValidIdentifier(name))
						throw new IllegalArgumentException("Invalid filename");
			
					Node n = config.getNode(config.syslogNode(), "file[normalize-space(name/text())=%s]", XML.xpc(name));
					if (n == null)
						throw new IllegalArgumentException("Syslog file is not found.");
					
					configSyslog(n, config);
				}
				catch (Exception e) {
					throw new DeviceConfigException("Failed to config Syslog file.", e);
				}
			}
			
		};
	}
*/
	
	@Override
	public DeviceConfigure getDeleteConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				Node syslogNode = config.syslogNode();
				Node n = config.getNode(syslogNode, "file[normalize-space(name/text())=%s]", XML.xpc(name));
				
				if (n != null) {
					if (StringUtils.isNotBlank(facility)) {
						config.deleteNode(n, "contents[normalize-space(name/text())=%s]", XML.xpc(facility));
					}
					
					if (config.getNodeList(n, "contents").getLength() == 0) {
						syslogNode.removeChild(n);
					}
				}
			}
		};
	}
	
	public static boolean isTrafficLogName(DeviceConfig config, String name) {
		Node n = config.findSyslogFileNode(name);

		if (n == null)
			return false;
		
		return (config.getNode(n, "match[normalize-space(text())=%s]", XML.xpc(RT_FLOW_SESSION)) != null);
	}
	
}
