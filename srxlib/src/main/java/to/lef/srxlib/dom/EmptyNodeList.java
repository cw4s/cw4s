package to.lef.srxlib.dom;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class EmptyNodeList implements NodeList {
    public static final EmptyNodeList instance = new EmptyNodeList();

    @Override
    public Node item(int index) {
        return null;
    }

    @Override
    public int getLength() {
        return 0;
    }
}
