package to.lef.srxlib.dom;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.*;

import to.lef.srxlib.Util;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.File;
import java.io.StringWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XML {
	private final static XPathFactory           XPathFactoryInstance           = XPathFactory.newInstance();
	private final static DocumentBuilderFactory DocumentBuilderFactoryInstance = DocumentBuilderFactory.newInstance();
	private final static TransformerFactory     TransformerFactoryInstance     = TransformerFactory.newInstance();
	
	protected final Document doc;
	protected final Element  root;
	protected final XPath    xpath;

	public static DocumentBuilder createDocumentBuilderInstance() throws ParserConfigurationException {
		DocumentBuilder ret;
		synchronized(DocumentBuilderFactoryInstance) {
			ret = DocumentBuilderFactoryInstance.newDocumentBuilder();
		}
		return ret;
	}
	
	public static XPath createXpathInstance() {
		XPath ret;
		synchronized(XPathFactoryInstance) {
			ret = XPathFactoryInstance.newXPath();
		}
		return ret;
	}

	public static Transformer createTransformerInstance() throws TransformerConfigurationException {
		Transformer ret;
		synchronized(TransformerFactoryInstance) {
			ret = TransformerFactoryInstance.newTransformer();
		}
		ret.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
		ret.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		ret.setOutputProperty(OutputKeys.INDENT, "yes");
		ret.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		
		return ret;
	}

	public final XPath getXPathInstance() {
		return xpath;
	}
	
	public final static String getTextContent(Node n) {
		return StringUtils.trim(n.getTextContent());
	}
	
	public final static String getString(XPathExpression xpath, Node o) {
		synchronized(xpath) {
			try {
				return StringUtils.trim((String)xpath.evaluate(o, XPathConstants.STRING));
			}
			catch (XPathExpressionException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	public final String getString(XPathExpression xpath) {
		return getString(xpath, root);
	}
	
	public final static String getString(XPath xpath, Node o, String path) {
		synchronized(xpath) {
			try {
				return StringUtils.trim((String)xpath.evaluate(path, o, XPathConstants.STRING));
			}
			catch (XPathExpressionException e) {
				throw new RuntimeException(e);
			}		
		}
	}
	
	public final static String getString(XPath xpath, Node o, String path, Object ...objects) {
		return getString(xpath, o, String.format(path, objects));
	}
	
	public final String getString(Node o, String path) {
		return getString(xpath, o, path);
	}
	
	public final String getString(Node o, String path, Object ...objects) {
		return getString(o, String.format(path, objects));
	}
	
	public final String getString(String path) {
		return getString(xpath, root, path);
	}
	
	public final String getString(String path, Object ...objects) {
		return getString(String.format(path, objects));
	}
	
	public final static Node getNode(XPathExpression xpath, Node o) {
		synchronized(xpath) {
			try {
				return (Node)xpath.evaluate(o, XPathConstants.NODE);
			}
			catch (XPathExpressionException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	public final Node getNode(XPathExpression xpath) {
		return getNode(xpath, root);
	}
	
	public final static Node getNode(XPath xpath, Node o, String path) {
		synchronized(xpath) {
			try {
				return (Node)xpath.evaluate(path, o, XPathConstants.NODE);
			}
			catch (XPathExpressionException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	public final static Node getNode(XPath xpath, Node o, String path, Object ...objects) {
		return getNode(xpath, o, String.format(path, objects));
	}
	
	public final Node getNode(Node o, String path) {
		return getNode(xpath, o, path);
	}
	
	public final Node getNode(Node o, String path, Object ...objects) {
		return getNode(o, String.format(path, objects));
	}
	
	public final Node getNode(String path) {
		return getNode(xpath, root, path);
	}
	
	public final Node getNode(String path, Object ...objects) {
		return getNode(String.format(path, objects));
	}
	
	public final static NodeList getNodeList(XPathExpression xpath, Node o) {
        NodeList ret;
		synchronized(xpath) {
			try {
				ret = (NodeList)xpath.evaluate(o, XPathConstants.NODESET);
			}
			catch (XPathExpressionException e) {
				throw new RuntimeException(e);
			}
		}
        return (ret == null) ? EmptyNodeList.instance : ret;
	}
	
	public final NodeList getNodeList(XPathExpression xpath) {
		return getNodeList(xpath, root);
	}

	public final static NodeList getNodeList(XPath xpath, Node o, String path) {
        NodeList ret;
		synchronized(xpath) {
			try {
				ret = (NodeList)xpath.evaluate(path, o, XPathConstants.NODESET);
			}
			catch (XPathExpressionException e) {
				throw new RuntimeException(e);
			}
		}
        return (ret == null) ? EmptyNodeList.instance : ret;
	}

	public final static NodeList getNodeList(XPath xpath, Node o, String path, Object ...objects) {
		return getNodeList(xpath, o, String.format(path, objects));
	}
	
	public final NodeList getNodeList(Node o, String path) {
		return getNodeList(xpath, o, path);
	}

	public final NodeList getNodeList(Node o, String path, Object ... objects) {
		return getNodeList(o, String.format(path, objects));
	}
	
	public final NodeList getNodeList(String path) {
		return getNodeList(xpath, root, path);
	}
	
	public final NodeList getNodeList(String path, Object ...objects) {
		return getNodeList(String.format(path, objects));
	}

	public static void deleteNodeList(NodeList nl) {
		if (nl != null) {
			for(Node n: IterableNodeList.apply(nl)) {
				Node p = n.getParentNode();
				if (p != null)
					p.removeChild(n);
			}
		}
	}
	
	public final static void deleteNode(XPathExpression xpath, Node o) {
		synchronized(xpath) {
			try {
				deleteNodeList((NodeList)xpath.evaluate(o, XPathConstants.NODESET));
			}
			catch (XPathExpressionException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	public final void deleteNode(XPathExpression xpath) {
		deleteNode(xpath, root);
	}

	public final static void deleteNode(XPath xpath, Node o, String path) {
		synchronized(xpath) {
			try {
				deleteNodeList((NodeList)xpath.evaluate(path, o, XPathConstants.NODESET));
			}
			catch (XPathExpressionException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public final static void deleteNode(XPath xpath, Node o, String path, Object ...objects) {
		deleteNode(xpath, o, String.format(path, objects));
	}
	
	public final void deleteNode(Node o, String path) {
		deleteNode(xpath, o, path);
	}

	public final void deleteNode(Node o, String path, Object ... objects) {
		deleteNode(o, String.format(path, objects));
	}
	
	public final void deleteNode(String path) {
		deleteNode(xpath, root, path);
	}
	
	public final void deleteNode(String path, Object ...objects) {
		deleteNode(String.format(path, objects));
	}

	public static final void removeAllChildNodes(Node n) {
		while (n.hasChildNodes()) {
			n.removeChild(n.getFirstChild());
		}
	}
	
	public final Node findNodeByIPAddress(Node nd, String path, String vpath, String v) {
		NodeList nl = getNodeList(nd, path);
		int l = nl.getLength();
		for (int i = 0; i < l; i++) {
			Node n = nl.item(i);
			if (Util.isIPAddressEqualTo(v, getString(n, vpath))) {
				return n;
			}
		}
		return null;
	}
	
	public final Node findNodeByNetworkAddress(Node nd, String path, String vpath, String v) {
		NodeList nl = getNodeList(nd, path);
		int l = nl.getLength();
		for (int i = 0; i < l; i++) {
			Node n = nl.item(i);
			if (Util.isNetworkAddressEqualTo(v, getString(n, vpath))) {
				return n;
			}
		}
		return null;
	}
		
	public final static String xpc(String s) { // escape String to concat
		Matcher matcher = Pattern.compile("['\"]").matcher(s);
		StringBuilder buffer = new StringBuilder("concat(");
		int start = 0;
		while (matcher.find()) {
			buffer.append("'")
			.append(s.substring(start, matcher.start()))
			.append("',");
			buffer.append("'".equals(matcher.group()) ? "\"'\"," : "'\"',");
			start = matcher.end();
		}
		if (start == 0) {
			return "'" + s + "'";
		}
		return buffer.append("'")
				.append(s.substring(start))
				.append("'")
				.append(")")
				.toString();
	}

	public XML(Document doc, Element root) {
		if (doc == null)
			throw new NullPointerException("Document is null.");
		if (root == null)
			throw new NullPointerException("Element is null.");
		this.xpath = createXpathInstance();
		this.doc = doc;
		this.root = root;
	}

	public XML(Document doc) {
		this(doc, doc.getDocumentElement());
	}

	public final Document getDocument() {
		return doc;
	}
	
	public final Element getRootElement() {
		return root;
	}

	public final Text createTextNode(String v) {
		return doc.createTextNode(v);
	}

	public final Text createTextNode(String v, Object ...objects) {
		return createTextNode(String.format(v, objects));
	}
	
	public final Element createElement(String name) {
		return doc.createElement(name);
	}

	public final Element createElement(String name, String text) {
		return createElement(doc, name, text);
	}
	
	public static final Element createElement(Document doc, String name, String text) {
		Element ret = doc.createElement(name);
		ret.appendChild(doc.createTextNode(text));
		return ret;
	}

	public final Element createElement(String name, String format, Object ...objects) {
		return createElement(doc, name, format, objects);
	}
	
	public final static Element createElement(Document doc, String name, String format, Object ...objects) {
		return createElement(doc, name, String.format(format, objects));
	}

	public final Node createElementsIfNotExist(String...names) {
		return createElementsIfNotExist(root, names);
	}
	
	public final Node createElementsIfNotExist(Node root, String...names) {
		Node r = root;
		int l = names.length;
		
		for (int i = 0; i < l; i++) {
			String name = names[i];
			Node n = getNode(r, name);
			
			if (n == null) {
				n = createElement(name);
				r.appendChild(n);
			}
			r = n;
		}
		return r;
	}
	
	public final void setValueElement(Node e, String name, String format, Object ...objects) {
		setValueElement(doc, e, name, String.format(format, objects));
	}
	
	public final void setValueElement(Node e, String name, String v) {
		setValueElement(doc, e, name, v);
	}

	public final static void setValueElement(Document doc, Node e, String name, String format, Object ...objects) {
		setValueElement(doc, e, name, String.format(format, objects));
	}
	
	public static void setValueElement(Document doc, Node e, String name, String v) {
		Node tn = null;

		for (Node n : IterableNodeList.apply(e.getChildNodes())) {
			if (n.getNodeType() == Node.ELEMENT_NODE) {
				if (((Element)n).getTagName().equals(name)) {
					tn = n;
					break;
				}
			}
		}

		if (StringUtils.isNotBlank(v)) {
			if (tn == null) {
				e.appendChild(createElement(doc, name, v));
			}
			else {
				while (tn.hasChildNodes()) {
					tn.removeChild(tn.getFirstChild());
				}
				tn.appendChild(doc.createTextNode(v));
			}
		}
		else {
			if (tn != null) {
				e.removeChild(tn);
			}
		}	
	}
	
	public final void setBooleanElement(Node e, String name, boolean v) {
		setBooleanElement(doc, e, name, v);
	}
	
	public static void setBooleanElement(Document doc, Node e, String name, boolean v) {
		NodeList childs = e.getChildNodes();
		int childLength = childs.getLength();

		for (int i = 0; i < childLength; i++) {
			Node child = childs.item(i);
			if (child.getNodeType() == Node.ELEMENT_NODE) {
				Element el = (Element)child;
				if (el.getTagName().equals(name)) {
					if (v) {
						return;
					}
					else {
						el.getParentNode().removeChild(el);
						return;
					}
				}
			}
		}
	
		if (v)
			e.appendChild(doc.createElement(name));
	}

	public static boolean isBooleanElementSet(Node e, String name) {
		NodeList childs = e.getChildNodes();
		int childLength = childs.getLength();

		for (int i = 0; i < childLength; i++) {
			Node child = childs.item(i);
			if (child.getNodeType() == Node.ELEMENT_NODE) {
				Element el = (Element)child;
				if (el.getTagName().equals(name)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public final XPathExpression compileXpath(String path) {
		try {
			return xpath.compile(path);
		}
		catch (XPathExpressionException e) {
			throw new RuntimeException(e);
		}
	}
	
	public final void save(final File file) throws TransformerConfigurationException, TransformerException {
		save(file, true);
	}
	
	public final void save(final File file, final boolean deleteOnFailed) throws TransformerConfigurationException, TransformerException {
		save(file, doc, deleteOnFailed);
	}
	
	public static synchronized void save(final File file, final Node doc) throws TransformerException {
		save(file, doc, true);
	}

	private static void removeWhiteSpace(Node doc) {
        doc.normalize();
		XPath xPath = createXpathInstance();
		NodeList nodeList = getNodeList(xPath, doc, "//text()[normalize-space(.)='']");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			node.getParentNode().removeChild(node);
		}
	}
	
	public static synchronized void save(final File file, final Node doc, final boolean deleteOnFailed) throws TransformerException {
		boolean done = false;
		try {
			Transformer transformer = createTransformerInstance();
			removeWhiteSpace(doc);
			transformer.transform(new DOMSource(doc), new StreamResult(file));
			done = true;
		}
		finally {
			if (done == false && deleteOnFailed && file.exists())  
				file.delete();
		}
	}

	public String toXMLString() throws Exception {
		return toXMLString(root);
	}
	
	public static String toXMLString(final Node doc) throws TransformerException {
		StringWriter stringWriter = new StringWriter();
		Transformer transformer = createTransformerInstance();
		removeWhiteSpace(doc);
		transformer.transform(new DOMSource(doc), new StreamResult(stringWriter));	
		
		return stringWriter.toString();
	}
}