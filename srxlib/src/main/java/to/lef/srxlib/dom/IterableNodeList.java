package to.lef.srxlib.dom;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class IterableNodeList implements Iterable<Node>, NodeList {
	private NodeList nodeList;

	public class NodeIterator implements Iterator<Node> {
		private int index;

		private NodeIterator() {
			index = 0;
		}

		@Override
		public final boolean hasNext() {
			return index < nodeList.getLength();
		}

		@Override
		public final Node next() {
			Node ret = nodeList.item(index++);
			if (ret == null) 
				throw new NoSuchElementException();
			return ret;
		}

		@Override
		public final void remove() {
			throw new UnsupportedOperationException();
		}

	}

	public IterableNodeList(NodeList nodeList) {
		this.nodeList = (nodeList != null) ? nodeList : EmptyNodeList.instance;
	}

	@Override
	public final Iterator<Node> iterator() {
		return new NodeIterator();
	}

	@Override
	public final Node item(int index) {
		return nodeList.item(index);
	}

	@Override
	public final int getLength() {
		return nodeList.getLength();
	}	
	
	public static final IterableNodeList apply(NodeList nodeList) {
		return new IterableNodeList(nodeList);
	}
}

