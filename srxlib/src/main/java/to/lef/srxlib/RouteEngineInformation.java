package to.lef.srxlib;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class RouteEngineInformation extends XML {
	private List<Node> nodeList;
	
	public RouteEngineInformation(Document doc) {
		super(doc);
		
		nodeList = new ArrayList<>();
		for (Node n : IterableNodeList.apply(getNodeList("//route-engine-information"))) {
			nodeList.add(n);
		}
		
		if (nodeList.size() == 0)
			throw new IllegalArgumentException("Invalid route engine information.");
	}

	public final int getMemorySystemTotalUtil() throws NumberFormatException {
		return getMemorySystemTotalUtil(0);
	}
	
	public int getMemorySystemTotalUtil(int nodeIdx) throws NumberFormatException {
		Node no = nodeList.get(nodeIdx);
		String val = getString(no, "route-engine/memory-system-total-util/text()");
		if (StringUtils.isBlank(val)) {
			// SRX1400
			val = getString(no, "route-engine/memory-buffer-utilization/text()");
			if (StringUtils.isBlank(val)) {
				throw new RuntimeException("Unsupported route engine information format.");
			}
		}
		
		return Integer.parseInt(val);
	}
	
	public final int getCpuIdle() throws NumberFormatException {
		return getCpuIdle(0);
	}
	
	public int getCpuIdle(int nodeIdx) throws NumberFormatException {
		return Integer.parseInt(getString(nodeList.get(nodeIdx), "route-engine/cpu-idle/text()"));
	}
	
	public final int getCpuUtil() throws NumberFormatException {
		return getCpuUtil(0);
	}
	
	public int getCpuUtil(int nodeIdx) throws NumberFormatException {
		return 100 - getCpuIdle(nodeIdx);
	}
}
 