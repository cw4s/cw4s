package to.lef.srxlib.nat;

import java.net.UnknownHostException;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.Util;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.nic.LogicalInetInterface;
import to.lef.srxlib.nic.LogicalInterface;

public class Dip {
	private String name;
	private String description;
	private String startAddress;
	private String endAddress;
	private boolean proxyArpEnabled;
	
	public static Dip createFromNode(Node pool, DeviceConfig cfg) throws UnknownHostException {
		String name = cfg.getString(pool, "name/text()");
		String description = cfg.getString(pool, "description/text()");
		String startAddress = cfg.getString(pool, "address/name/text()");
		String endAddress = cfg.getString(pool, "address/to/ipaddr/text()");
		
		if (StringUtils.isBlank(name) || StringUtils.isBlank(startAddress))
			return null;
		
		if (StringUtils.isBlank(endAddress))
			endAddress = startAddress;
		
		int idx = startAddress.indexOf("/");
		if (idx != -1)
			startAddress = startAddress.substring(0, idx);
		idx = endAddress.indexOf("/");
		if (idx != -1)
			endAddress = endAddress.substring(0, idx);
	
		boolean proxyArpEnabled = false;
		Node n = (Util.isValidIPv4Address(startAddress)) ? cfg.natProxyArpNode() : cfg.natProxyNdpNode();
		for (Node intNode: IterableNodeList.apply(cfg.getNodeList(n, "interface"))) {
			String intName = cfg.getString(intNode, "name/text()");

			for(Node address: IterableNodeList.apply(cfg.getNodeList(intNode, "address")))  {
				try {
					ProxyArp o = ProxyArp.createFromNode(intName, address, cfg);
					if (o != null) {
						ProxyArp s = new ProxyArp(intName, startAddress, null, endAddress, null);
						if (o.equals(s)) {
							proxyArpEnabled = true;
							break;
						}
					}
				}
				catch (UnknownHostException e) {
					e.printStackTrace();
				}
			}
		}		
	
		return new Dip(name, description, startAddress, endAddress, proxyArpEnabled);
	}
	
	public Dip(String name, String description, String startAddress, String endAddress, boolean proxyArpEnabled) throws UnknownHostException {
		this.name = name;
		this.description = description;
		this.startAddress = Util.fixInetAddressString(startAddress);
		this.endAddress = Util.fixInetAddressString(endAddress);
		this.proxyArpEnabled = proxyArpEnabled;
	}

	public Dip(String startAddress, String endAddress, boolean proxyArpEnabled) throws UnknownHostException {
		this(null, null, startAddress, endAddress, proxyArpEnabled);
	}

	public final String getName() {
		return name;
	}

	public final String getDisplayName() {
		if (StringUtils.isBlank(endAddress))
			return String.format("DIP (%s)", startAddress);
		return String.format("DIP (%s - %s)", startAddress, endAddress);
	}

	public final String getDescription() {
		return description;
	}
	
	public final String getStartAddress() {
		return startAddress;
	}

	public final String getEndAddress() {
		return endAddress;
	}

	public boolean isBelongsTo(LogicalInterface lint) {
		if (name.startsWith(String.format("dip-%s-", lint.getNameForId())))
			return true;

		if (StringUtils.isNotBlank(description) && description.equals(lint.getName()))
			return true;
		
		return false;
	}
	
	private String makePoolName(LogicalInetInterface lint, Node source, DeviceConfig config) {
		for (int i = 1; ; i++) {
			String name = String.format("dip-%s-%d", lint.getNameForId(), i);
			Node n = config.getNode(source, "pool[normalize-space(name/text())=%s]", XML.xpc(name));
			
			if (n == null)
				return name;
		}
	}
	
	public DeviceConfigure getCreateConfigure(final LogicalInetInterface lint) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				
				try {
					if (!Util.isValidIPAddress(startAddress))
						throw new IllegalArgumentException("Invalid start address.");
				

					Node source = cfg.natSourceNode();
					String name = makePoolName(lint, source, cfg);
					
					Node pool = cfg.createElement("pool");
					pool.appendChild(cfg.createElement("name", name));
					pool.appendChild(cfg.createElement("description", lint.getName()));
					Node address = cfg.createElement("address");
					
					if (Util.isValidIPv4Address(startAddress))
						address.appendChild(cfg.createElement("name", startAddress + "/32"));
					else
						address.appendChild(cfg.createElement("name", startAddress + "/128"));
						
					if (StringUtils.isNotBlank(endAddress)) {
						if (!Util.isValidIPAddress(endAddress))
							throw new IllegalArgumentException("Invalid end address.");

						if (!endAddress.equals(startAddress)) {
							Node to = cfg.createElement("to");
							if (Util.isValidIPv4Address(endAddress))
								to.appendChild(cfg.createElement("ipaddr", endAddress + "/32"));
							else
								to.appendChild(cfg.createElement("ipaddr", endAddress));
							address.appendChild(to);
						}
					}
					
					pool.appendChild(address);
					source.insertBefore(pool, cfg.getNode(source, "pool"));
					
					if (proxyArpEnabled) {
						ProxyArp arp = new ProxyArp(lint.getName(), startAddress, null, endAddress, null);
						arp.getCreateConfigure().config(cfg, info);
					}
				}
				catch (DeviceConfigException e) {
					throw e;
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config DIP", e);
				}
			}
			
		};
	}

	public static boolean isRemovable(String name, DeviceConfig config) {
		Node srcNat = config.natSourceNode();
		
		for (Node ruleSet: IterableNodeList.apply(config.getNodeList(srcNat, "rule-set"))) {
			for (Node rule: IterableNodeList.apply(config.getNodeList(ruleSet, "rule"))) {
				if (name.equals(config.getString(rule, "then/source-nat/pool/pool-name/text()")))
					return false;
			}
		}
		
		return true;
	}
	
	public final DeviceConfigure getDeleteConfigure() {
		return getDeleteConfigure(name);
	}
	
	public static DeviceConfigure getDeleteConfigure(final String name) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					Node source = cfg.natSourceNode();
					Node pool = cfg.getNode(source, "pool/name[normalize-space(text())=%s]/..", XML.xpc(name));

					String startAddress = null;
					String endAddress = null;
					
					if (pool != null) {
						startAddress = cfg.getString(pool, "address/name/text()");
						endAddress = cfg.getString(pool, "address/to/ipaddr/text()");
						pool.getParentNode().removeChild(pool);
					}
					
					for (Node ruleSet: IterableNodeList.apply(cfg.getNodeList(source, "rule-set"))) {
						for (Node rule: IterableNodeList.apply(cfg.getNodeList(ruleSet, "rule"))) {
							String poolName = cfg.getString(rule, "then/source-nat/pool/pool-name/text()");
							if (name.equals(poolName)) {
								ruleSet.removeChild(rule);
							}
						}
						
						if (cfg.getNode(ruleSet, "rule") == null) 
							source.removeChild(ruleSet);
					}
					
					if (StringUtils.isNotBlank(startAddress)) {
						String[] tmp = startAddress.split("\\/", 2);
						Node n = (Util.isValidIPv4Address((tmp.length == 2) ? tmp[0] : startAddress)) ? cfg.natProxyArpNode() : cfg.natProxyNdpNode();
						for (Node intNode: IterableNodeList.apply(cfg.getNodeList(n, "interface"))) {
							String intName = cfg.getString(intNode, "name/text()");

							for(Node address: IterableNodeList.apply(cfg.getNodeList(intNode, "address")))  {
								try {
									ProxyArp o = ProxyArp.createFromNode(intName, address, cfg);
									if (o != null) {
										ProxyArp s = ProxyArp.createInstance(intName, startAddress, endAddress);
										if (o.equals(s)) {
											o.getDeleteConfigure().config(cfg, info);
										}
									}
								}
								catch (UnknownHostException e) {
									e.printStackTrace();
								}
							}
						}		
					}
				}
				catch (DeviceConfigException e) {
					throw e;
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to delete DIP.", e);
				}
			}
			
		};
	}
	
	public int isChanged(Map<String, Dip> map) {
		Dip c = map.get(getName());
		if (c == null)
			return 1;
		if (!equals(c)) 
			return 2;
		return 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
//		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((endAddress == null) ? 0 : endAddress.hashCode());
		result = prime * result + (proxyArpEnabled ? 1231 : 1237);
		result = prime * result + ((startAddress == null) ? 0 : startAddress.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Dip)) {
			return false;
		}
		Dip other = (Dip) obj;
		if (StringUtils.isBlank(endAddress)) {
			if (StringUtils.isNotBlank(other.endAddress)) {
				return false;
			}
		} else if (!endAddress.equals(other.endAddress)) {
			return false;
		}
		if (proxyArpEnabled != other.proxyArpEnabled) {
			return false;
		}
		if (StringUtils.isBlank(startAddress)) {
			if (StringUtils.isNotBlank(other.startAddress)) {
				return false;
			}
		} else if (!startAddress.equals(other.startAddress)) {
			return false;
		}
		return true;
	}
}
