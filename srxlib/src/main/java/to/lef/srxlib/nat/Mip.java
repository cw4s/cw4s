package to.lef.srxlib.nat;

import java.net.UnknownHostException;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.IPAddress;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.nic.LogicalInetInterface;
import to.lef.srxlib.nic.LogicalInterface;
import to.lef.srxlib.policy.AddressElement;
import to.lef.srxlib.policy.AddressSet;

public class Mip {
	private String name;
	private String ruleSetName;
	private String fromInterfaceName;
	private String prefixName;
	private IPAddress mappedAddress;
	private IPAddress prefixAddress;
	/*
	private String mappedAddress;
	private String prefixAddress;
	private int mappedMask;
	private int prefixMask;
	*/
	private boolean proxyArpEnabled;

	public static Mip createFromNode(Node rule, DeviceConfig cfg) throws UnknownHostException {
		Node ruleSet = rule.getParentNode();
		if (ruleSet == null)
			return null;
		
		String ruleSetName = cfg.getString(ruleSet, "name/text()");
		String fromInterfaceName = cfg.getString(ruleSet, "from/interface/text()");
		
		if (StringUtils.isBlank(ruleSetName))
			return null;
		
		if (StringUtils.isBlank(fromInterfaceName))
			return null;
		
		String name = cfg.getString(rule, "name/text()");
		IPAddress mappedAddress = null;
		String tmp = cfg.getString(rule, "static-nat-rule-match/destination-address/dst-addr");
		if (StringUtils.isNotBlank(tmp))
			mappedAddress = new IPAddress(tmp);

		String prefixName = null;
		IPAddress prefixAddress = null;
		
		tmp = cfg.getString(rule, "then/static-nat/prefix/addr-prefix/text()");
		if (StringUtils.isNotBlank(tmp))
			prefixAddress = new IPAddress(tmp);
		
		if (StringUtils.isBlank(name))
			return null;
		
		if (mappedAddress == null)
			return null;
		
		if (prefixAddress == null) {
			prefixName = cfg.getString(rule, "then/static-nat/prefix-name/addr-prefix-name/text()");
			if (StringUtils.isBlank(prefixName)) 
				return null;
			
			Node addrNode = cfg.findAddressNode("global", prefixName);
			if (addrNode != null) {
				tmp = cfg.getString(addrNode, "ip-prefix/text()");
				if (StringUtils.isNotBlank(tmp))
					prefixAddress = new IPAddress(tmp);
			}
			
			if (prefixAddress == null)
				return null;
		}
		
		ProxyArp arp = new ProxyArp(fromInterfaceName, mappedAddress);
		return new Mip(name, ruleSetName, fromInterfaceName, mappedAddress, prefixName, prefixAddress, (arp != null && arp.isExist(cfg)));
	}
	
	public Mip(String name, String ruleSetName, String fromInterfaceName, String mappedAddress, Integer mappedMask, String prefixName, String prefixAddress,  Integer prefixMask, boolean proxyArpEnabled) throws UnknownHostException {
		this(name, ruleSetName, fromInterfaceName, 
				(mappedMask == null) ? new IPAddress(mappedAddress) : new IPAddress(mappedAddress, mappedMask), 
				prefixName, (prefixMask == null) ? new IPAddress(prefixAddress) : new IPAddress(prefixAddress, prefixMask), proxyArpEnabled);
	}
	
	public Mip(String name, String ruleSetName, String fromInterfaceName, IPAddress mappedAddress, String prefixName, IPAddress prefixAddress, boolean proxyArpEnabled) throws UnknownHostException {
		this.name = name;
		this.ruleSetName = ruleSetName;
		this.fromInterfaceName = fromInterfaceName;
		this.prefixName = prefixName;
		this.mappedAddress = mappedAddress;
		this.prefixAddress = prefixAddress;
		
		/*
		this.mappedAddress = Util.fixInetAddressString(mappedAddress);
		this.prefixAddress = Util.fixInetAddressString(prefixAddress);
		
		if (mappedMask != null) {
			this.mappedMask = mappedMask;
		}
		else if (Util.isValidIPv4Address(mappedAddress)) {
			this.mappedMask = 32;
		}
		else if (Util.isValidIPv6Address(mappedAddress)){
			this.mappedMask = 128;
		}
		else {
			throw new IllegalArgumentException("Invalid mapped address.");
		}
		
		if (prefixMask != null) {
			this.prefixMask = prefixMask;
		}
		else if (Util.isValidIPv4Address(prefixAddress)) {
			this.prefixMask = 32;
		}
		else if (Util.isValidIPv6Address(prefixAddress)){
			this.prefixMask = 128;
		}
		else {
			throw new IllegalArgumentException("Invalid prefix address.");
		}
		*/
		this.proxyArpEnabled = proxyArpEnabled;
	}

	public Mip(String mappedAddress, Integer mappedMask, String prefixAddress, Integer prefixMask, boolean proxyArpEnabled) throws UnknownHostException {
		this(null, null, null, mappedAddress, mappedMask, null, prefixAddress, prefixMask, proxyArpEnabled);
	}

	public final String getRuleSetName() {
		return ruleSetName;
	}
	
	public final String getName() {
		return name;
	}
	public final String getMappedAddress() {
		return (mappedAddress == null) ? null : mappedAddress.getAddress();
	}
	public final String getPrefixAddress() {
		return (prefixAddress == null) ? null : prefixAddress.getAddress();
	}
	public final int getMappedMask() {
		return (mappedAddress == null) ? 0 : mappedAddress.getNetmask();
	}
	public final int getPrefixMask() {
		return (prefixAddress == null) ? 0 : prefixAddress.getNetmask();
	}

	public boolean isBelongsTo(LogicalInetInterface lint) {
		if (!lint.getName().equals(fromInterfaceName))
			return false;

		return true;
	}

	private String makeRuleName(Node ruleSetNode, DeviceConfig cfg) {
		String ret = null;
		String setName = cfg.getString(ruleSetNode, "name/text()");
		for (int i = 1; ; i++) {
			ret = String.format("%s-%d", setName, i);
			if (cfg.getNode(ruleSetNode, "rule/name[normalize-space(text())=%s]/..", XML.xpc(ret)) == null)
				return ret;
		}
	}
	
	private Node configPrepareRuleSet(LogicalInetInterface lint, DeviceConfig cfg) {
		Node s = cfg.natStaticNode();
//		Node ruleSet = cfg.findNatStaticRuleSetNodeByInterface(s,lint.getInternalName());
		Node ruleSet = cfg.findNatStaticRuleSetNodeByInterface(s,lint.getName());
		if (ruleSet != null)
			return ruleSet;
	
		for (int i = 0; ; i++) {
			String ruleSetName = (i == 0) ? String.format("mip-%s", lint.getNameForId()) : String.format("mip-%s-%d", lint.getNameForId(), i);
		
			ruleSet = cfg.findNatStaticRuleSetNode(ruleSetName);
			if (ruleSet != null)
				continue;
			
			ruleSet = cfg.createElement("rule-set");
			ruleSet.appendChild(cfg.createElement("name", ruleSetName));
			Node from = cfg.createElement("from");
			from.appendChild(cfg.createElement("interface", lint.getName()));
			ruleSet.appendChild(from);

			s.appendChild(ruleSet);

			return ruleSet;
		}
	}

	public String makeAddressElement(String ruleName, DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
		for (int i = 0; ; i++) {
			String name = (i == 0) ? ruleName : String.format("%s-%d", ruleName, i);
			Node addrNode = config.findAddressNode("global", name);
			
			if (addrNode != null)
				continue;
			
			AddressElement addr = new AddressElement("global", name, prefixAddress);
			addr.setDescription(String.format("MIP(%s/%d)", mappedAddress.getAddress(), mappedAddress.getNetmask()));
			addr.getCreateConfigure().config(config, info);
			
			return name;
		}
	}
	
	public DeviceConfigure getCreateConfigure(final LogicalInetInterface lint) {

		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					if (mappedAddress == null || !mappedAddress.isHostAddress()) {
						throw new IllegalArgumentException("Invalid mapped address.");
					}
					
					if (prefixAddress == null || !prefixAddress.isHostAddress()) {
						throw new IllegalArgumentException("Invalid hsot address.");
					}

//					fromInterfaceName = lint.getInternalName();
					fromInterfaceName = lint.getName();
					Node ruleSet = configPrepareRuleSet(lint, cfg);
					String ruleName = makeRuleName(ruleSet, cfg);
			
					Node rule = cfg.createElement("rule");
					rule.appendChild(cfg.createElement("name", ruleName));

					Node match = cfg.createElement("static-nat-rule-match");
					Node dest  = cfg.createElement("destination-address");
					dest.appendChild(cfg.createElement("dst-addr", mappedAddress.toString()));
					match.appendChild(dest);
					rule.appendChild(match);
				
					Node then = cfg.createElement("then");
					Node snat = cfg.createElement("static-nat");

					String prefixName = makeAddressElement(ruleName, cfg, info);
					Node prefix = cfg.createElement("prefix-name");
					prefix.appendChild(cfg.createElement("addr-prefix-name", prefixName));
					snat.appendChild(prefix);

				/*	
					Node prefix = cfg.createElement("prefix");
					prefix.appendChild(cfg.createElement("addr-prefix", "%s/%d", prefixAddress, prefixMask));
					snat.appendChild(prefix);
				*/	
					then.appendChild(snat);
					rule.appendChild(then);
					
					ruleSet.appendChild(rule);
		
					if (proxyArpEnabled) {
						ProxyArp arp = new ProxyArp(fromInterfaceName, mappedAddress);
						arp.getCreateConfigure().config(cfg, info);
					}
				}
				catch (DeviceConfigException e) {
					throw e;
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config mip.", e);
				}
			}
			
		};
	}

	public boolean isRemovable(DeviceConfig config) {
		if (StringUtils.isNotBlank(prefixName)) {
			AddressElement a = config.findAddress("global", prefixName);
			if (a != null)
				return !a.isInUse(config);
			AddressSet aset = config.findAddressSet("global", prefixName);
			if (aset != null)
				return !aset.isInUse(config);
		}
		return true;
	}

/*	
	public static boolean isRemovable(String fromInterfaceName, String name, DeviceConfig config) {
		for(Mip mip: config.mipList(fromInterfaceName)) {
			if (StringUtils.isBlank(mip.getName()) || !mip.getName().equals(name))
				continue;
			
			if (StringUtils.isNotBlank(mip.prefixName)) {
				AddressElement a = config.findAddress("global", mip.prefixName);
				if (a != null)
					return a.isRemovable(config);
				AddressSet aset = config.findAddressSet("global", mip.prefixName);
				if (aset != null)
					return aset.isRemovable(config);
			}		
		}
		return true;
	}
*/
	
	public DeviceConfigure getDeleteConfigure() {
		return getDeleteConfigure(fromInterfaceName, name);
	}
	
	public static DeviceConfigure getDeleteConfigure(LogicalInterface lint, String name) {
		return getDeleteConfigure(lint.getName(), name);
	}
	
	protected static DeviceConfigure getDeleteConfigure(final String fromInterfaceName, final String name) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig config, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					Node rule = config.findInterfaceMipNode(fromInterfaceName, name);
					if (rule == null)
						return;
					
					Node ruleSet = rule.getParentNode();
					
					String dstAddr = config.getString(rule, "static-nat-rule-match/destination-address/dst-addr/text()");
					String prefixName = config.getString(rule, "then/static-nat/prefix-name/addr-prefix-name/text()");
					
					ruleSet.removeChild(rule);
					if (StringUtils.isNotBlank(dstAddr)) {
						ProxyArp arp = ProxyArp.createInstance(fromInterfaceName, dstAddr);
						if (arp != null)
							arp.getDeleteConfigure().config(config, info);
					}
					
					if (StringUtils.isNotBlank(prefixName)) {
						AddressElement a = config.findAddress("global", prefixName);
						if (a != null) {
							a.getDeleteConfigure().config(config, info);
						} 
						else {
							AddressSet aset = config.findAddressSet("global", prefixName);
							if (aset != null) {
								aset.getDeleteConfigure().config(config, info);
							}
						}
					}
					
					if (config.getNode(ruleSet, "rule") == null) {
						ruleSet.getParentNode().removeChild(ruleSet);
					}				
				}
				catch (DeviceConfigException e) {
					throw e;
				}
				catch (Exception e) {
					throw new DeviceConfigException("Failed to config Mip.", e);
				}
			}
			
		};
	}

	public int isChanged(Map<String, Mip> map) {
		Mip c = map.get(getName());
		if (c == null)
			return 1;
		if (!equals(c)) 
			return 2;
		return 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fromInterfaceName == null) ? 0 : fromInterfaceName .hashCode());
		result = prime * result + ((mappedAddress == null) ? 0 : mappedAddress.hashCode());
		result = prime * result + ((prefixAddress == null) ? 0 : prefixAddress.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Mip)) {
			return false;
		}
		Mip other = (Mip) obj;
		if (StringUtils.isBlank(fromInterfaceName)) {
			if (StringUtils.isNotBlank(other.fromInterfaceName)){
				return false;
			}
		} else if (!fromInterfaceName.equals(other.fromInterfaceName)) {
			return false;
		}
		if (mappedAddress == null) {
			if (other.mappedAddress != null) {
				return false;
			}
		} else if (!mappedAddress.equals(other.mappedAddress)) {
			return false;
		}
		if (prefixAddress == null) {
			if (other.prefixAddress != null) {
				return false;
			}
		} else if (!prefixAddress.equals(other.prefixAddress)) {
			return false;
		}
		
		return true;
	}
}
