package to.lef.srxlib.nat;

import java.net.UnknownHostException;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.IPAddress;
import to.lef.srxlib.Util;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;

public class ProxyArp {
	private String interfaceName;
	private IPAddress fromAddress;
	private IPAddress toAddress;

	public static ProxyArp createFromNode(String interfaceName, Node address, DeviceConfig cfg) throws UnknownHostException {
		String from = cfg.getString(address, "name/text()");
		if (StringUtils.isBlank(from))
			return null;
		String[] s = from.split("\\/", 2);
		return createInstance(interfaceName, from, (Util.isValidIPv4Address((s.length == 2) ? s[0] : from)) ? cfg.getString(address, "to/ipaddr/text()") : cfg.getString(address, "to/ipv6addr/text()"));
	}

	public static final ProxyArp createInstance(String interfaceName, String fromAddress) throws UnknownHostException {
		return createInstance(interfaceName, fromAddress, null);
	}

	public static ProxyArp createInstance(String interfaceName, String fromAddress, String toAddress) throws UnknownHostException {
		String fAddress = fromAddress;
		Integer fMask = null;
		String tAddress = toAddress;
		Integer tMask = null;

		if (StringUtils.isBlank(fAddress))
			return null;

		String[] s = fAddress.split("\\/", 2);
		if (s.length == 2) {
			fAddress = s[0];
			fMask = Integer.valueOf(s[1]);
		}

		if (StringUtils.isNotBlank(tAddress)) {
			s = tAddress.split("\\/", 2);
			if (s.length == 2) {
				tAddress = s[0];
				tMask = Integer.valueOf(s[1]);
			}
		}

		return new ProxyArp(interfaceName, fAddress, fMask, tAddress, tMask);		
	}

	public ProxyArp(String interfaceName, String fromAddress, Integer fromMask) throws UnknownHostException {
		this(interfaceName, fromAddress, fromMask, null, null);
	}

	public ProxyArp(String interfaceName, IPAddress fromAddress) {
		this(interfaceName, fromAddress, null);
	}
	
	public ProxyArp(String interfaceName, String fromAddress) throws UnknownHostException {
		this(interfaceName, fromAddress, null, null, null);
	}

	public ProxyArp(String interfaceName, String fromAddress, String toAddress) throws UnknownHostException {
		this(interfaceName, fromAddress, null, toAddress, null);
	}

	public ProxyArp(String interfaceName, String fromAddress, Integer fromMask, String toAddress, Integer toMask) throws UnknownHostException {
		this(interfaceName, 
				(fromMask == null) ? new IPAddress(fromAddress) : new IPAddress(fromAddress, fromMask),
				(StringUtils.isBlank(toAddress)) ? null : (toMask == null) ? new IPAddress(toAddress) : new IPAddress(toAddress, toMask)
				);
	}

	public ProxyArp(String interfaceName, IPAddress fromAddress, IPAddress toAddress) {
		if (fromAddress == null) 
			throw new IllegalArgumentException("Invalid from address.");
			
		this.interfaceName = interfaceName;
		this.fromAddress = fromAddress;
		this.toAddress = toAddress;

		if (this.toAddress != null && this.toAddress.equals(this.fromAddress)) {
			this.toAddress = null;
		}
	}
	
	public DeviceConfigure getCreateConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				ProxyArp self = ProxyArp.this;
				try {
					if (self.isExist(cfg))
						return;

					Node arp = (fromAddress.isIPv4Address()) ? cfg.natProxyArpNode() : cfg.natProxyNdpNode();
					Node intNode = cfg.getNode(arp, "interface/name[normalize-space(text())=%s]/..", XML.xpc(interfaceName));

					if (intNode == null) {
						intNode = cfg.createElement("interface");
						intNode.appendChild(cfg.createElement("name", interfaceName));
						arp.appendChild(intNode);
					}

					Node address = cfg.createElement("address");
					address.appendChild(cfg.createElement("name", fromAddress.toString()));
					intNode.appendChild(address);

					if (toAddress != null) {
						Node to = cfg.createElement("to");
						address.appendChild(to);

						if (toAddress.isIPv4Address()) {
							to.appendChild(cfg.createElement("ipaddr", toAddress.toString()));
						}
						else {
							to.appendChild(cfg.createElement("ipv6addr", toAddress.getAddress()));
						}
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config proxy arp.", e);
				}
			}
		};
	}

	public boolean isExist(DeviceConfig cfg) {
		for (ProxyArp other: cfg.proxyArpList(interfaceName)) {
			if (other.equals(this)) 
				return true;
		}
		return false;
	}

	public boolean isInUse(DeviceConfig cfg) {
		for (Node ruleSet: IterableNodeList.apply(cfg.getNodeList(cfg.natStaticNode(), "rule-set"))) {
			String from = cfg.getString(ruleSet, "from/interface/text()");
			if (StringUtils.isBlank(from))
				continue;

			if (!from.equals(interfaceName))
				continue;

			for (Node rule: IterableNodeList.apply(cfg.getNodeList(ruleSet, "rule"))) {
				try {
					ProxyArp other = ProxyArp.createInstance(interfaceName, cfg.getString(rule, "static-nat-rule-match/destination-address/dst-addr/text()"));
					if (equals(other))
						return true;
				} 
				catch (UnknownHostException e) {
					e.printStackTrace();
				}
			}
		}

		for (Node ruleSet: IterableNodeList.apply(cfg.getNodeList(cfg.natDestinationNode(), "rule-set"))) {
			String from = cfg.getString(ruleSet, "from/interface/text()");
			if (StringUtils.isBlank(from))
				continue;

			if (!from.equals(interfaceName))
				continue;

			for (Node rule: IterableNodeList.apply(cfg.getNodeList(ruleSet, "rule"))) {
				try {
					ProxyArp other = ProxyArp.createInstance(interfaceName, cfg.getString(rule, "dest-nat-rule-match/destination-address/dst-addr/text()"));
					if (equals(other)) 
						return true;
				} 
				catch (UnknownHostException e) {
					e.printStackTrace();
				}
			}
		}

		for (Node pool: IterableNodeList.apply(cfg.getNodeList(cfg.natSourceNode(), "pool"))) {
			try {
				ProxyArp other = ProxyArp.createInstance(interfaceName, 
						cfg.getString(pool, "address/name/text()"),
						cfg.getString(pool, "address/to/ipaddr/text()")
						);
				if (equals(other)) 
					return true;
			} 
			catch (UnknownHostException e) {
				e.printStackTrace();
			}			
		}
		return false;
	}

	public DeviceConfigure getDeleteConfigure() {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				ProxyArp self = ProxyArp.this;
				try {
					if (isInUse(cfg))
						return;

					Node n = (fromAddress.isIPv4Address()) ? cfg.natProxyArpNode() : cfg.natProxyNdpNode();
					for (Node intNode: IterableNodeList.apply(cfg.getNodeList(n, "interface"))) {
						String intName = cfg.getString(intNode, "name/text()");
						if (!interfaceName.equals(intName))
							continue;

						for(Node address: IterableNodeList.apply(cfg.getNodeList(intNode, "address")))  {
							try {
								ProxyArp o = createFromNode(interfaceName, address, cfg);
								if (self.equals(o))
									intNode.removeChild(address);
							}
							catch (UnknownHostException e) {
								e.printStackTrace();
							}
						}

						if (cfg.getNode(intNode, "address") == null)
							n.removeChild(intNode);
					}	
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config proxy arp.", e);
				}
			}
		};
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fromAddress == null) ? 0 : fromAddress.hashCode());
		result = prime * result + ((interfaceName == null) ? 0 : interfaceName.hashCode());
		result = prime * result + ((toAddress == null) ? 0 : toAddress.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ProxyArp)) {
			return false;
		}
		ProxyArp other = (ProxyArp) obj;
		if (fromAddress == null) {
			if (other.fromAddress != null) {
				return false;
			}
		} else if (!fromAddress.equals(other.fromAddress)) {
			return false;
		}
		if (StringUtils.isBlank(interfaceName)) {
			if (StringUtils.isNotBlank(other.interfaceName)) {
				return false;
			}
		} else if (!interfaceName.equals(other.interfaceName)) {
			return false;
		}
		if (toAddress == null) {
			if (other.toAddress != null) {
				return false;
			}
		} else if (!toAddress.equals(other.toAddress)) {
			return false;
		}
		
		return true;
	}
}
