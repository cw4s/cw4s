package to.lef.srxlib.nat;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import to.lef.srxlib.DeviceInformationProvider;
import to.lef.srxlib.DeviceConfig;
import to.lef.srxlib.DeviceConfigException;
import to.lef.srxlib.DeviceConfigure;
import to.lef.srxlib.IPAddress;
import to.lef.srxlib.dom.IterableNodeList;
import to.lef.srxlib.dom.XML;
import to.lef.srxlib.nic.LogicalInetInterface;
import to.lef.srxlib.nic.LogicalInterface;
import to.lef.srxlib.policy.AddressElement;
import to.lef.srxlib.policy.AddressSet;

public class Vip {
	private String name;
	private String ruleSetName;
	private String poolName;
	private String fromInterfaceName;
	private IPAddress virtualAddress;
	private int virtualPort;
	private IPAddress mapAddress;
	private String mapProtocol;
	private int mapPort;
	private boolean proxyArpEnabled;
//	private String addressName;
	
	public static Vip createFromNode(Node rule, DeviceConfig cfg) throws UnknownHostException {
		Node ruleSet = rule.getParentNode();

		String ruleSetName = cfg.getString(ruleSet, "name/text()");
		String fromInterfaceName = cfg.getString(ruleSet, "from/interface/text()");
		if (StringUtils.isBlank(fromInterfaceName))
			return null;

		String name = cfg.getString(rule, "name/text()");
		String tmp = cfg.getString(rule, "dest-nat-rule-match/destination-address/dst-addr/text()");
		if (StringUtils.isBlank(tmp))
			return null;

		IPAddress virtualAddress = new IPAddress(tmp);
		int virtualPort = 0;
		
		if (cfg.isNewerThan12_1X47())
			tmp = cfg.getString(rule, "dest-nat-rule-match/destination-port/name/text()");
		else
			tmp = cfg.getString(rule, "dest-nat-rule-match/destination-port/dst-port/text()");
		
		if (StringUtils.isNotBlank(tmp)) 
			virtualPort = Integer.parseInt(tmp);

		String mapProtocol = cfg.getString(rule, "dest-nat-rule-match/protocol/text()");
		if (StringUtils.isBlank(mapProtocol))
			return null;

		String poolName = cfg.getString(rule, "then/destination-nat/pool/pool-name/text()");
		if (StringUtils.isBlank(poolName))
			return null;

		Node poolNode = cfg.findNatDestinationPoolNode(poolName);
		tmp = cfg.getString(poolNode, "address/ipaddr/text()");
		if (StringUtils.isBlank(tmp))
			return null;

		IPAddress mapAddress = new IPAddress(tmp);
		
		int mapPort = 0;
		tmp = cfg.getString(poolNode, "address/port/text()");
		if (StringUtils.isNotBlank(tmp)) 
			mapPort = Integer.parseInt(tmp);

		String addressName = cfg.getString(rule, "decription/text()");
		
		ProxyArp arp = new ProxyArp(fromInterfaceName, virtualAddress);
		return new Vip(name, ruleSetName, poolName, fromInterfaceName, virtualAddress, virtualPort, mapAddress, mapProtocol, mapPort, addressName, (arp != null && arp.isExist(cfg)));
	}

	public Vip(String virtualAddress, Integer virtualMask, int virtualPort, String mapAddress, Integer mapMask, String mapProtocol, int mapPort, boolean proxyArpEnabled) throws UnknownHostException {
		this(null, null, null, null, virtualAddress, virtualMask, virtualPort, mapAddress, mapMask, mapProtocol, mapPort, null, proxyArpEnabled);
	}

	public Vip(String name, String ruleSetName, String poolName, String fromZoneName, String virtualAddress, Integer virtualMask, int virtualPort, String mapAddress, Integer mapMask, String mapProtocol, int mapPort, String addressName, boolean proxyArpEnabled) throws UnknownHostException {
		this(name, ruleSetName, poolName, fromZoneName, 
				(virtualMask == null) ? new IPAddress(virtualAddress) : new IPAddress(virtualAddress, virtualMask), virtualPort,
				(mapMask == null) ? new IPAddress(mapAddress) : new IPAddress(mapAddress, mapMask), mapProtocol, mapPort,
				addressName, proxyArpEnabled
				);
	}
	
	public Vip(String name, String ruleSetName, String poolName, String fromZoneName, IPAddress virtualAddress, int virtualPort, IPAddress mapAddress, String mapProtocol, int mapPort, String addressName, boolean proxyArpEnabled) throws UnknownHostException {
		this.name = name;
		this.ruleSetName = ruleSetName;
		this.poolName = poolName;
		this.fromInterfaceName = fromZoneName;
		this.virtualAddress = virtualAddress;
		this.virtualPort = virtualPort;
		this.mapAddress = mapAddress;
		this.mapProtocol = mapProtocol;
		this.mapPort = mapPort;
//		this.addressName = addressName;
		this.proxyArpEnabled = proxyArpEnabled;
	}

	private static boolean isPoolInUse(String poolName, DeviceConfig cfg) {
		if (StringUtils.isBlank(poolName))
			return false;

		Node natDest = cfg.natDestinationNode();
		for (Node ruleSet: IterableNodeList.apply(cfg.getNodeList(natDest, "ruleset"))) {
			for (Node rule: IterableNodeList.apply(cfg.getNodeList(ruleSet, "rule"))) {
				String name = cfg.getString(rule, "then/destination-nat/pool/pool-name/text()");
				if (poolName.equals(name))
					return true;
			}
		}

		return false;
	}

	public boolean isBelongsTo(LogicalInetInterface lint) {
		if (!lint.getName().equals(fromInterfaceName))
			return false;

		return true;
	}

	public final String getName() {
		return name;
	}

	public final String getRuleSetName() {
		return ruleSetName;
	}

	public final String getPoolName() {
		return poolName;
	}

	public final String getFromZoneName() {
		return fromInterfaceName;
	}

	public final String getVirtualAddress() {
		return (virtualAddress == null) ? null : virtualAddress.getAddress();
	}

	public final int getVirtualMask() {
		return (virtualAddress == null) ? 0 : virtualAddress.getNetmask();
	}

	public final int getVirtualPort() {
		return virtualPort;
	}

	public final String getMapAddress() {
		return (mapAddress == null) ? null : mapAddress.getAddress();
	}

	public final int getMapMask() {
		return (mapAddress == null) ? 0 : mapAddress.getNetmask();
	}

	public final String getMapProtocol() {
		return mapProtocol;
	}

	public final int getMapPort() {
		return mapPort;
	}

	public String makeAddressDescription() {
		return String.format("VIP(%s (%s))", virtualAddress.toString(), mapAddress.toString());
	}

	public String makeAddressSetDescription() {
		return String.format("VIP(%s)", virtualAddress.toString());
	}

	public String makeAddressSetName(Node ruleSetNode, DeviceConfig cfg) {
		String ret = null;
		String setName = cfg.getString(ruleSetNode, "name/text()");
		for (int i = 1; ; i++) {
			ret = String.format("%s-set-%d", setName, i);
			
			if (cfg.isAddressOrAddressSetExist("global", ret))
				continue;
			
			return ret;
		}	
	}
	
	private Node configPrepareRuleSet(Node natDest, LogicalInetInterface lint, DeviceConfig cfg) {
		Node ruleSet = cfg.findNatDestinationRuleSetNodeByInterface(natDest, lint.getName());
		if (ruleSet != null)
			return ruleSet;

		for (int i = 0; ; i++) {
			String ruleSetName = (i == 0) ? String.format("vip-%s", lint.getNameForId()) : String.format("vip-%s-%d", lint.getNameForId(), i);

			ruleSet = cfg.findNatDestinationRuleSetNode(natDest, ruleSetName);
			if (ruleSet != null)
				continue;

			ruleSet = cfg.createElement("rule-set");
			ruleSet.appendChild(cfg.createElement("name", ruleSetName));
			Node from = cfg.createElement("from");
			from.appendChild(cfg.createElement("interface", lint.getName()));
			ruleSet.appendChild(from);

			natDest.appendChild(ruleSet);
			
			this.ruleSetName = ruleSetName;
			return ruleSet;
		}
	}

	private String makeRuleName(Node ruleSetNode, DeviceConfig cfg) {
		String ret = null;
		String setName = cfg.getString(ruleSetNode, "name/text()");
		for (int i = 1; ; i++) {
			ret = String.format("%s-%d", setName, i);
			
			if (cfg.isAddressOrAddressSetExist("global", ret))
				continue;
			
			if (cfg.getNode(ruleSetNode, "rule/name[normalize-space(text())=%s]/..", XML.xpc(ret)) == null)
				return ret;
		}
	}

	private static boolean isPoolInUse(Node dnat, String poolName, DeviceConfig cfg) {
		for (Node ruleSet: IterableNodeList.apply(cfg.getNodeList(dnat, "rule-set"))) {
			if (cfg.getNode(ruleSet, "rule[normalize-space(then/destination-nat/pool/pool-name/text())=%s]", XML.xpc(poolName)) != null) {
				return true;
			}
		}
		return false;
	}
	
	private Node configPreparePool(Node natDest, LogicalInetInterface lint, DeviceConfig cfg) {
		for (Node n: IterableNodeList.apply(cfg.getNodeList(natDest, "pool"))) {
			String tmp = cfg.getString(n, "address/ipaddr/text()");
			if (StringUtils.isBlank(tmp))
				continue;

			IPAddress address = new IPAddress(tmp);
			if (!address.equals(mapAddress))
				continue;

			String port = cfg.getString(n, "address/port/text()");
			if (StringUtils.isBlank(port)) {
				if (mapPort != 0)
					continue;
			}
			else if (Integer.parseInt(port) != mapPort) {
				continue;
			}

			return n;
		}

		for (int i = 1; ; i++) {
			String name = String.format("vip-%s-%d", lint.getNameForId(), i);
			Node n = cfg.getNode(natDest, "pool[normalize-space(name/text())=%s]", XML.xpc(name));
			if (n != null) 
				continue;

			if (cfg.findAddress("global", name) != null)
				continue;

			Node pool = cfg.createElement("pool");
			pool.appendChild(cfg.createElement("name", name));
			Node address = cfg.createElement("address");
			address.appendChild(cfg.createElement("ipaddr", "%s", mapAddress.toString()));
			if (mapPort > 0) {
				address.appendChild(cfg.createElement("port", "%d", mapPort));
			}
			pool.appendChild(address);
			natDest.insertBefore(pool, cfg.getNode(natDest, "pool"));

			return pool;
		}
	}


	public DeviceConfigure getCreateConfigure(final LogicalInetInterface lint) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					if (virtualAddress == null || !virtualAddress.isHostAddress()) {
						throw new IllegalArgumentException("Invalid virtual address.");
					}

					if (mapAddress == null || !mapAddress.isHostAddress()) {
						throw new IllegalArgumentException("Invalid map address.");
					}

					String virtAddr = virtualAddress.toString();
					
//					fromInterfaceName = lint.getInternalName();
					fromInterfaceName = lint.getName();

					Node natDest = cfg.natDestinationNode();
					Node pool = configPreparePool(natDest, lint, cfg);
					String poolName = cfg.getString(pool, "name/text()");

					Node ruleSet = configPrepareRuleSet(natDest, lint, cfg);
					String ruleName = makeRuleName(ruleSet, cfg);

					Node rule = cfg.createElement("rule");
					rule.appendChild(cfg.createElement("name", ruleName));
					
					Node match = cfg.createElement("dest-nat-rule-match");
					Node dest  = cfg.createElement("destination-address");
					dest.appendChild(cfg.createElement("dst-addr", virtAddr));
					match.appendChild(dest);

					if (virtualPort > 0) {
						Node dport  = cfg.createElement("destination-port");
						if (cfg.isNewerThan12_1X47())
							dport.appendChild(cfg.createElement("name", "%d", virtualPort));
						else
							dport.appendChild(cfg.createElement("dst-port", "%d", virtualPort));
						match.appendChild(dport);
					}

					match.appendChild(cfg.createElement("protocol", mapProtocol));
					rule.appendChild(match);

					Node then = cfg.createElement("then");
					Node dnat = cfg.createElement("destination-nat");
					Node pooln = cfg.createElement("pool");
					pooln.appendChild(cfg.createElement("pool-name", "%s", poolName));
					dnat.appendChild(pooln);
					then.appendChild(dnat);
					rule.appendChild(then);

					ruleSet.appendChild(rule);

					if (proxyArpEnabled) {
						ProxyArp arp = new ProxyArp(fromInterfaceName, virtualAddress);
						arp.getCreateConfigure().config(cfg, info);
					}

					String addressDescription = makeAddressDescription();
					AddressElement addressElement = cfg.findAddressByDescription("global", addressDescription);
					if (addressElement == null) {
						addressElement = new AddressElement("global", ruleName, mapAddress);
						addressElement.setDescription(addressDescription);
						
						addressElement.getCreateConfigure().config(cfg, info);
					}
					rule.appendChild(cfg.createElement("description", addressElement.getName()));

					String addressSetDescription = makeAddressSetDescription();
					AddressSet addressSet = cfg.findAddressSetByDescription("global", addressSetDescription);
					if (addressSet == null) {
						addressSet = new AddressSet("global", makeAddressSetName(ruleSet, cfg), Arrays.asList(addressElement.getName()), new ArrayList<String>(), addressSetDescription);
						addressSet.getCreateConfigure().config(cfg, info);
					}
					else {
						AddressSet newAddressSet = new AddressSet(addressSet);
						newAddressSet.addAddressName(addressElement.getName());
						newAddressSet.getEditConfigure(addressSet).config(cfg, info);
					}
				}
				catch (DeviceConfigException e) {
					throw e;
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to config Vip.", e);
				}
			}

		};
	}

	public boolean isRemovable(DeviceConfig config) {
		AddressSet addressSet = config.findAddressSetByDescription("global", makeAddressSetDescription());
		if (addressSet != null) {
			if (addressSet.isInUse(config))
				return false;
		}
		
		AddressElement addressElement = config.findAddressByDescription("global", makeAddressDescription());
		if (addressElement != null) {
			if (AddressElement.isInUse("global", addressElement.getName(), addressSet, config))
				return false;
		}
		
		return true;
	}

	public final DeviceConfigure getDeleteConfigure() {
		return getDeleteConfigure(fromInterfaceName, name);
	}

	public static DeviceConfigure getDeleteConfigure(LogicalInterface lint, String name) {
//		return getDeleteConfigure(lint.getInternalName(), name);
		return getDeleteConfigure(lint.getName(), name);
	}

	protected static DeviceConfigure getDeleteConfigure(final String fromInterfaceName, final String name) {
		return new DeviceConfigure() {

			@Override
			public void config(DeviceConfig cfg, DeviceInformationProvider info) throws DeviceConfigException {
				try {
					Node rule = cfg.findInterfaceVipNode(fromInterfaceName, name);
					if (rule == null)
						return;
					
					Node ruleSet = rule.getParentNode();
					Node dnat = ruleSet.getParentNode();
					
					String addr = cfg.getString(rule, "dest-nat-rule-match/destination-address/dst-addr/text()");
					String poolName = cfg.getString(rule, "then/destination-nat/pool/pool-name/text()");
					String addressName = cfg.getString(rule, "description/text()");
							
					ruleSet.removeChild(rule);
					if (StringUtils.isNotBlank(addr)) {
						ProxyArp.createInstance(fromInterfaceName, addr).getDeleteConfigure().config(cfg, info);
					}
					
					if (StringUtils.isNotBlank(poolName) && !isPoolInUse(poolName, cfg)) {
						if (!isPoolInUse(dnat, poolName, cfg))
							cfg.deleteNode(dnat, "pool/name[normalize-space(text())=%s]/..", XML.xpc(poolName));
					}

					if (StringUtils.isNotBlank(addressName)) {
						AddressElement.getDeleteConfigure("global", addressName).config(cfg, info);
						Node ab = cfg.addressBookNode("global");
						for (Node set: IterableNodeList.apply(cfg.getNodeList(ab, "address-set"))) {
							Node a = cfg.getNode(set, "address[normalize-space(name/text())=%s]", XML.xpc(addressName));
							if (a != null) {
								set.removeChild(a);
								if (cfg.getNode(set, "address") == null && cfg.getNode(set, "address-set") == null)
									ab.removeChild(set);
							}
						}
					}
					
					if (cfg.getNode(ruleSet, "rule") == null) {
						dnat.removeChild(ruleSet);
					}
				}
				catch (Throwable e) {
					throw new DeviceConfigException("Failed to delete vip.", e);
				}
			}
		};
	}

	public int isChanged(Map<String, Vip> map) {
		Vip c = map.get(getName());
		if (c == null)
			return 1;
		if (!equals(c)) 
			return 2;
		return 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fromInterfaceName == null) ? 0 : fromInterfaceName .hashCode());
		result = prime * result + ((mapAddress == null) ? 0 : mapAddress.hashCode());
		result = prime * result + mapPort;
		result = prime * result + ((mapProtocol == null) ? 0 : mapProtocol.hashCode());
		result = prime * result + ((virtualAddress == null) ? 0 : virtualAddress.hashCode());
		result = prime * result + virtualPort;
		result = prime * result + (proxyArpEnabled ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Vip)) {
			return false;
		}
		Vip other = (Vip) obj;
		if (StringUtils.isBlank(fromInterfaceName)) {
			if (StringUtils.isNotBlank(other.fromInterfaceName)) {
				return false;
			}
		} else if (!fromInterfaceName.equals(other.fromInterfaceName)) {
			return false;
		}
		if (mapAddress == null) {
			if (other.mapAddress != null) {
				return false;
			}
		} else if (!mapAddress.equals(other.mapAddress)) {
			return false;
		}
		if (mapPort != other.mapPort) {
			return false;
		}
		if (StringUtils.isBlank(mapProtocol)) {
			if (StringUtils.isNotBlank(other.mapProtocol)) {
				return false;
			}
		} else if (!mapProtocol.equals(other.mapProtocol)) {
			return false;
		}
		if (virtualAddress == null) {
			if (other.virtualAddress != null) {
				return false;
			}
		} else if (!virtualAddress.equals(other.virtualAddress)) {
			return false;
		}
		if (virtualPort != other.virtualPort) {
			return false;
		}
		if (proxyArpEnabled != other.proxyArpEnabled) {
			return false;
		}
		return true;
	}
}

