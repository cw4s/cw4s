package to.lef.srxlib;

import javax.xml.xpath.XPath;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class DefaultsConfig extends BaseConfig {
	public DefaultsConfig(Document doc, Element root) {
		super(doc, root);
	}
	
	public static DefaultsConfig createFromDocument(Document doc) {
		final Element e = doc.getDocumentElement();
		final String tagName = e.getTagName();

		DefaultsConfig ret = null;
		if (tagName.equals("rpc-reply")) {
			final XPath xpath = createXpathInstance();
			Element root = (Element) getNode(xpath, e, "configuration/groups/name[normalize-space(text())='junos-defaults']/..");
			if (root != null) {
				ret = new DefaultsConfig(doc, root);
			}
		}
		else {
			throw new IllegalArgumentException("Unknown document.");
		}
		
		return ret;
	}
}
