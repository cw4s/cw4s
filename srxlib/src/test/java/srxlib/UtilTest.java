package srxlib;

import static org.junit.Assert.*;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import to.lef.srxlib.IPAddress;
import to.lef.srxlib.Util;

public class UtilTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetNetworkAddress() throws UnknownHostException {
		assertTrue(InetAddress.getByName("192.168.0.0").equals(IPAddress.getNetworkAddress("192.168.0.10", 24)));
		assertFalse(InetAddress.getByName("192.168.0.1").equals(IPAddress.getNetworkAddress("192.168.0.10", 24)));
		assertFalse(InetAddress.getByName("192.168.10.0").equals(IPAddress.getNetworkAddress("192.168.0.10", 24)));
		assertTrue(InetAddress.getByName("192.168.10.0").equals(IPAddress.getNetworkAddress("192.168.10.10", 24)));
		assertTrue(InetAddress.getByName("192.168.10.0").equals(IPAddress.getNetworkAddress("192.168.10.10", 28)));
	}

	@Test
	public void testGetBroadcastAddress() throws UnknownHostException {
		assertTrue(InetAddress.getByName("192.168.0.255").equals(IPAddress.getBroadcastAddress("192.168.0.10", 24)));
		assertTrue(InetAddress.getByName("192.168.255.255").equals(IPAddress.getBroadcastAddress("192.168.0.10", 16)));
		assertFalse(InetAddress.getByName("192.168.0.1").equals(IPAddress.getBroadcastAddress("192.168.0.10", 24)));
		assertFalse(InetAddress.getByName("192.168.10.0").equals(IPAddress.getBroadcastAddress("192.168.0.10", 24)));
		assertTrue(InetAddress.getByName("192.168.10.255").equals(IPAddress.getBroadcastAddress("192.168.10.10", 24)));
		assertTrue(InetAddress.getByName("192.168.0.15").equals(IPAddress.getBroadcastAddress("192.168.0.1", 28)));
	}

	@Test
	public void testIsValidNetworkAddress() {
		assertTrue(Util.isValidNetworkAddress("192.168.10.0", 24));
		assertTrue(Util.isValidNetworkAddress("192.168.0.0", 16));
		assertFalse(Util.isValidNetworkAddress("192.168.10.5", 24));
		assertFalse(Util.isValidNetworkAddress("192.168.0.5", 16));
	}
	
	@Test
	public void testIsValidHostAddress() {
		assertTrue(Util.isValidHostAddress("0.0.0.0", 0, true));
		assertFalse(Util.isValidHostAddress("0.0.0.0", 0, false));
		
		assertFalse(Util.isValidHostAddress("192.168.0.1", 0, false));
		assertFalse(Util.isValidHostAddress("192.168.0.0", 24, false));
		assertFalse(Util.isValidHostAddress("192.168.0.255", 24, false));
		assertTrue(Util.isValidHostAddress("192.168.0.1", 24, false));
		
		assertTrue(Util.isValidHostAddress("192.168.0.1", 28, false));
		
		assertTrue(Util.isValidHostAddress("192.168.160.202", 32, false));
	}
	
	@Test
	public void testInterfaceNameForId() {
		assertEquals("ge-0_0_0_0", Util.interfaceNameForId("ge-0/0/0.0"));
		assertEquals("fe-1_0_0_1", Util.interfaceNameForId("fe-1/0/0.1"));
	}
	
	@Test
	public void testString2Netmask() throws UnknownHostException {
		assertEquals(Util.string2Netmask("255.255.255.255"), 32);
		assertEquals(Util.string2Netmask("255.255.255.248"), 29);
		assertEquals(Util.string2Netmask("255.255.255.192"), 26);
		assertEquals(Util.string2Netmask("255.255.255.0"), 24);
		assertEquals(Util.string2Netmask("255.255.0.0"), 16);
		assertEquals(Util.string2Netmask("255.0.0.0"), 8);
		assertEquals(Util.string2Netmask("0.0.0.0"), 0);
	}
	
	@Test
	public void testIsValidIPAddress() {
		assertTrue(Util.isValidIPAddress("192.168.30.2"));
		assertFalse(Util.isValidIPAddress("192.168.256.2"));
	}
}
