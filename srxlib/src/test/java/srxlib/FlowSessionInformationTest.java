package srxlib;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import to.lef.srxlib.*;

public class FlowSessionInformationTest {
	@ClassRule
	public static TemporaryFolder testFolder = new TemporaryFolder();
	
	@Test
	public void test() throws Throwable {
		Device device = MockDevice.instance(testFolder.getRoot());
		FlowSessionInformation info = device.flowSessionInformation();
		
		assertEquals(info.getActiveUnicastSessionCount(), 29);
		assertEquals(info.getActiveMulticastSessionCount(), 0);
		assertEquals(info.getMaxSessionCount(), 65536);
	}

	@Test
	public void testMultiple() throws Throwable {
		@SuppressWarnings("serial")
		Device device = MockDevice.instance(testFolder.getRoot(), new HashMap<String, String>() {
			{
				put(Device.CACHE_NAME_FLOW_SESSION_INFORMATION, "flow_session_information_summary-multi.xml");
			}
		});
		FlowSessionInformation info = device.flowSessionInformation();
		
		assertEquals(info.getActiveUnicastSessionCount(), 30);
		assertEquals(info.getActiveMulticastSessionCount(), 12);
		assertEquals(info.getMaxSessionCount(), 1572864);
	}
}
