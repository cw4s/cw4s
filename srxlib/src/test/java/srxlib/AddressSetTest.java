package srxlib;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.util.ArrayList;
import java.util.Arrays;

import to.lef.srxlib.*;
import to.lef.srxlib.policy.AddressElement;
import to.lef.srxlib.policy.AddressSet;
import to.lef.srxlib.policy.Policy;

public class AddressSetTest {
	@ClassRule
	public static TemporaryFolder testFolder = new TemporaryFolder();
	
	protected static Device device;
	protected DeviceConfig config;

	protected AddressElement address_1;
	protected AddressElement address_2;
	protected AddressSet     set_1;
	protected AddressSet     set_2;
	protected static final String untrust = "untrust";
	
	@BeforeClass
	public static void setUpBeforeClass() throws Throwable {
		device = MockDevice.instance(testFolder.getRoot());
	}

	@Before
	public void setUp() throws Throwable {
		config = device.editingConfig();
		
		address_1 = new AddressElement(untrust, "test-address-pre1", "192.168.10.1", 24);
		address_1.getCreateConfigure().config(config, device);
		
		address_2 = new AddressElement(untrust, "test-address-pre2", "192.168.10.2", 24);
		address_2.getCreateConfigure().config(config, device);
		
		AddressElement a = new AddressElement(untrust, "test-address-for-set-1", "192.168.20.1", 24);
		a.getCreateConfigure().config(config, device);
		
		set_1 = new AddressSet(untrust, "test-address-set-pre1", Arrays.asList(a.getName()), new ArrayList<String>());
		set_1.getCreateConfigure().config(config, device);
		
		a = new AddressElement(untrust, "test-address-for-set-2", "192.168.20.2", 24);
		a.getCreateConfigure().config(config, device);
		
		set_2 = new AddressSet(untrust, "test-address-set-pre2", Arrays.asList(a.getName()), new ArrayList<String>());
		set_2.getCreateConfigure().config(config, device);
	}
	
	@Test
	public void testEquals() throws Throwable {
		AddressSet set1 = new AddressSet("untrust", "test-address-1", 
				Arrays.asList(
						address_1.getName(),
						address_2.getName()
						), 
				Arrays.asList(
						set_1.getName(),
						set_2.getName()
						));

		AddressSet set2 = new AddressSet("untrust", "test-address-1", 
				Arrays.asList(
						address_2.getName(),
						address_1.getName()
						), 
				Arrays.asList(
						set_1.getName(),
						set_2.getName()
						));
		
		assertTrue(set1.equals(set2));
		
		set2 = new AddressSet("untrust", "test-address-1", 
				Arrays.asList(
						address_1.getName(),
						address_2.getName()
						), 
				Arrays.asList(
						set_2.getName(),
						set_1.getName()
						));
		
		assertTrue(set1.equals(set2));	
	}
	
	@Test
	public void testCreateAndRemove() throws Throwable {
		final String name = "test-address-set1";
		
		AddressSet set1 = new AddressSet(untrust, name, Arrays.asList(address_1.getName()), Arrays.asList(set_1.getName()));
		set1.getCreateConfigure().config(config, device);
		
		AddressSet set2 = config.findAddressSet(untrust, name);
		assertNotNull(set2);
	
		assertTrue(set1.equals(set2));
		assertTrue(set2.getAddressNameList().contains(address_1.getName()));
		assertTrue(set2.getAddressSetNameList().contains(set_1.getName()));
		
		assertTrue(AddressSet.isRemovable(untrust, name, config));
		assertTrue(set1.isRemovable(config));
		assertFalse(AddressSet.isInUse(untrust, name, config));
		assertFalse(set1.isInUse(config));
		
		AddressSet.getDeleteConfigure(untrust, name).config(config, device);
		set2 = config.findAddressSet(untrust, name);
		assertNull(set2);
	}

	@Test
	public void testInUseAddressSet() throws Throwable {
		final String name ="test-addresss-set1";
		
		AddressSet set1 = new AddressSet(untrust, name, Arrays.asList(address_1.getName()), Arrays.asList(set_1.getName()));
		set1.getCreateConfigure().config(config, device);

		assertTrue(set_1.isInUse(config));	
	}
	
	@Test
	public void testInUsePolicyFrom() throws Throwable {
		Policy policy = new Policy("policy-1", untrust, "trust", 
				Arrays.asList(set_1.getName()), Arrays.asList("any"), Arrays.asList("junos-http"), 
				Policy.ACTION_PERMIT, false, false);
		policy.getCreateConfigure().config(config, device);
		
		assertTrue(set_1.isInUse(config));
	}
	
	public void testInUsePolicyTo() throws Throwable {
		Policy policy = new Policy("policy-1", "trust", untrust,
				Arrays.asList("any"), Arrays.asList(set_1.getName()), Arrays.asList("junos-http"), 
				Policy.ACTION_PERMIT, false, false);
		policy.getCreateConfigure().config(config, device);
	
		assertTrue(set_1.isInUse(config));
	}
	
	@Test
	public void testEdit1() throws Throwable {
		final String name1 = "test-address-set-1";
		final String name2 = "test-address-set-2";
		final String name3 = "test-address-set-3";

		final String policy1name = "tset-policy1";
		final String policy2name = "tset-policy2";
	
		
		AddressSet set1 = new AddressSet(untrust, name1, 
				Arrays.asList(address_1.getName(), address_2.getName()),
				Arrays.asList(set_1.getName(), set_2.getName()));
		set1.getCreateConfigure().config(config, device);
	
		AddressSet set3 = new AddressSet(untrust, name3, new ArrayList<String>(), Arrays.asList(set1.getName()));
		set3.getCreateConfigure().config(config, device);

		AddressSet set4 = config.findAddressSet(untrust, name3);
		assertTrue(set4.getAddressSetNameList().contains(name1));
		
		Policy policy1 = new Policy(policy1name, untrust, "trust",
				Arrays.asList(name1), Arrays.asList("any"), Arrays.asList("junos-http"), 
				Policy.ACTION_PERMIT, false, false);
		policy1.getCreateConfigure().config(config, device);
		
		Policy policy3 = config.findPolicy(untrust, "trust", policy1name);
		assertTrue(policy3.getSourceNameList().contains(name1));
				
		Policy policy2 = new Policy(policy2name, "trust", untrust,
				Arrays.asList("any"), Arrays.asList(name1), Arrays.asList("junos-http"), 
				Policy.ACTION_PERMIT, false, false);
		policy2.getCreateConfigure().config(config, device);
		
		policy3 = config.findPolicy("trust", untrust, policy2name);
		assertTrue(policy3.getDestinationNameList().contains(name1));
		
		AddressSet set2 = new AddressSet(untrust, name2, 
				Arrays.asList(address_1.getName()),
				Arrays.asList(set_1.getName()));
		set2.getEditConfigure(set1).config(config, device);
		
		set4 = config.findAddressSet(untrust, name3);
		assertFalse(set4.getAddressSetNameList().contains(name1));
		assertTrue(set4.getAddressSetNameList().contains(name2));
		
		policy3 = config.findPolicy(untrust, "trust", policy1name);
		assertFalse(policy3.getSourceNameList().contains(name1));
		assertTrue(policy3.getSourceNameList().contains(name2));
		
		policy3 = config.findPolicy("trust", untrust, policy2name);
		assertFalse(policy3.getDestinationNameList().contains(name1));
		assertTrue(policy3.getDestinationNameList().contains(name2));
	}
}
