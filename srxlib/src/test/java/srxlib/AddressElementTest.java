package srxlib;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import to.lef.srxlib.*;
import to.lef.srxlib.policy.AddressElement;
import to.lef.srxlib.policy.AddressSet;
import to.lef.srxlib.policy.Policy;

import java.util.Arrays;

public class AddressElementTest {
	@ClassRule
	public static TemporaryFolder testFolder = new TemporaryFolder();
	
	protected static Device device;
	protected DeviceConfig config;
	
	@BeforeClass 
	public static void setUpBeforeClass() throws Throwable {
		device = MockDevice.instance(testFolder.getRoot());
    }

	@Before
    public void setUp() throws Throwable {
		config = device.editingConfig();
	}
	
	@Test
	public void testCreateAndRemove() throws Throwable {
		final String name ="test-addresss-1";
		final String zone = "trust";
	
		AddressElement addr1 = new AddressElement(zone, name, "192.168.10.10", 24);
		addr1.getCreateConfigure().config(config, device);
		
		AddressElement addr2 = config.findAddress(zone, name);
		assertNotNull(addr2);		
	
		assertTrue(addr1.equals(addr2));
		
		assertTrue(AddressElement.isRemovable(addr1.getZoneName(), addr1.getName(), config));
		assertTrue(addr1.isRemovable(config));
		
		assertFalse(AddressElement.isInUse(addr1.getZoneName(), addr1.getName(), config));
		assertFalse(addr1.isInUse(config));
		
		AddressElement.getDeleteConfigure(addr1.getZoneName(), addr1.getName()).config(config, device);
		
		addr2 = config.findAddress(addr1.getZoneName(), addr1.getName());
		assertNull(addr2);		
	}

	@Test
	public void testInUseAddressSet() throws Throwable {
		final String name ="test-addresss-1";
		final String zone = "trust";
		
		AddressElement addr1 = new AddressElement(zone, name, "192.168.10.10", 24);
		addr1.getCreateConfigure().config(config, device);

		AddressSet set = new AddressSet(zone, "test-address-set1", Arrays.asList(name), new ArrayList<String>());
		set.getCreateConfigure().config(config, device);

		assertTrue(addr1.isInUse(config));
	}
	
	@Test
	public void testInUsePolicyFrom() throws Throwable {
		final String name ="test-addresss-1";
		final String zone = "trust";
		
		AddressElement addr1 = new AddressElement(zone, name, "192.168.10.10", 24);
		addr1.getCreateConfigure().config(config, device);

		Policy policy = new Policy("policy-1", zone, "untrust", 
				Arrays.asList(name), Arrays.asList("any"), Arrays.asList("junos-http"), 
				Policy.ACTION_PERMIT, false, false);
		policy.getCreateConfigure().config(config, device);
		
		assertTrue(addr1.isInUse(config));
	}
	
	@Test
	public void testInUsePolicyTo() throws Throwable {
		final String name ="test-addresss-1";
		final String zone = "trust";
		
		AddressElement addr1 = new AddressElement(zone, name, "192.168.10.10", 24);
		addr1.getCreateConfigure().config(config, device);

		Policy policy = new Policy("policy-1", "untrust", zone,
				Arrays.asList("any"), Arrays.asList(name), Arrays.asList("junos-http"), 
				Policy.ACTION_PERMIT, false, false);
		policy.getCreateConfigure().config(config, device);
	
		assertTrue(addr1.isInUse(config));
	}
	
	@Test
	public void testEdit1() throws Throwable {
		final String name1 = "test-address-1";
		final String name2 = "test-address-2";
		final String zone1 = "trust";
		final String zone2 = "untrust";
		
		AddressElement addr1 = new AddressElement(zone1, name1, "192.168.10.10", 24);
		addr1.getCreateConfigure().config(config, device);
		
		AddressElement addr2 = new AddressElement(zone2, name2, "192.168.10.11", 16);
		addr2.getEditConfigure(addr1).config(config, device);
		
		AddressElement addr3 = config.findAddress(zone1, name1);
		assertNull(addr3);
		
		addr3 = config.findAddress(zone2, name2);
		assertNotNull(addr3);
		
		assertTrue(addr2.equals(addr3));
	}
	
	@Test
	public void testEdit2() throws Throwable {
		final String name1 = "test-address-1";
		final String name2 = "test-address-2";
		final String zone1 = "trust";
		final String zone2 = "untrust";
		
		AddressElement addr1 = new AddressElement(zone1, name1, "lef.to");
		addr1.getCreateConfigure().config(config, device);
		
		AddressElement addr2 = new AddressElement(zone2, name2, "kether.jp");
		addr2.getEditConfigure(addr1).config(config, device);
		
		AddressElement addr3 = config.findAddress(zone1, name1);
		assertNull(addr3);
		
		addr3 = config.findAddress(zone2, name2);
		assertNotNull(addr3);
		
		assertTrue(addr2.equals(addr3));
	}
	
	@Test
	public void testEdit3() throws Throwable {
		final String name1 = "test-address-1";
		final String name2 = "test-address-2";
		final String zone  = "untrust";
	
		final String set1name = "tset-addrsess-set1";
		final String policy1name = "tset-policy1";
		final String policy2name = "tset-policy2";
		final String policy1zone = "trust";
		final String policy2zone = "untrust";
		
		AddressElement addr1 = new AddressElement(zone, name1, "192.168.10.10", 24);
		addr1.getCreateConfigure().config(config, device);
		
		AddressSet set1 = new AddressSet(zone, set1name, Arrays.asList(name1), new ArrayList<String>());
		set1.getCreateConfigure().config(config, device);

		AddressSet set2 = config.findAddressSet(zone, set1name);
		assertTrue(set2.getAddressNameList().contains(name1));
		
		Policy policy1 = new Policy(policy1name, zone, policy1zone,
				Arrays.asList(name1), Arrays.asList("any"), Arrays.asList("junos-http"), 
				Policy.ACTION_PERMIT, false, false);
		policy1.getCreateConfigure().config(config, device);
		
		Policy policy3 = config.findPolicy(zone, policy1zone, policy1name);
		assertTrue(policy3.getSourceNameList().contains(name1));
				
		Policy policy2 = new Policy(policy2name, policy2zone, zone,
				Arrays.asList("any"), Arrays.asList(name1), Arrays.asList("junos-http"), 
				Policy.ACTION_PERMIT, false, false);
		policy2.getCreateConfigure().config(config, device);
		
		policy3 = config.findPolicy(policy2zone, zone, policy2name);
		assertTrue(policy3.getDestinationNameList().contains(name1));
	
		AddressElement addr2 = new AddressElement(zone, name2, "192.168.10.11", 16);
		addr2.getEditConfigure(addr1).config(config, device);
		
		set2 = config.findAddressSet(zone, set1name);
		assertFalse(set2.getAddressNameList().contains(name1));
		assertTrue(set2.getAddressNameList().contains(name2));

		policy3 = config.findPolicy(zone, policy1zone, policy1name);
		assertFalse(policy3.getSourceNameList().contains(name1));
		assertTrue(policy3.getSourceNameList().contains(name2));
		
		policy3 = config.findPolicy(policy2zone, zone, policy2name);
		assertFalse(policy3.getDestinationNameList().contains(name1));
		assertTrue(policy3.getDestinationNameList().contains(name2));
	}
	
}
