package srxlib;

import static org.junit.Assert.*;

import org.junit.Test;

import to.lef.srxlib.Version;

public class VersionTest {
	@Test
	public void testVersion1() {
		Version v = new Version("12.1X46");
		assertNotNull(v);
		assertEquals(v.getMajorVersion(), 12);
		assertEquals(v.getMinorVersion(), 1);
		assertTrue(v.isTypeX());
		assertFalse(v.isTypeR());
		assertEquals(v.getTypeMajorVersion(), 46);
		assertEquals(v.getTypeMinorVersion(), 0);
		assertEquals(v.toString(), "12.1X46");
	}
	
	@Test
	public void testVersion2() {
		Version v = new Version("15.1");
		assertEquals(v.getMajorVersion(), 15);
		assertEquals(v.getMinorVersion(), 1);
		assertFalse(v.isTypeX());
		assertFalse(v.isTypeR());
		assertEquals(v.getTypeMajorVersion(), 0);
		assertEquals(v.getTypeMinorVersion(), 0);
		assertEquals(v.toString(), "15.1");
	}
	
	@Test
	public void testVersion3() {
		Version v = new Version("10.4R3.4");
		assertEquals(v.getMajorVersion(), 10);
		assertEquals(v.getMinorVersion(), 4);
		assertFalse(v.isTypeX());
		assertTrue(v.isTypeR());
		assertEquals(v.getTypeMajorVersion(), 3);
		assertEquals(v.getTypeMinorVersion(), 4);
		assertEquals(v.toString(), "10.4R3.4");
	}
	
	@Test
	public void testVersion4() {
		Version v = new Version("10.0R4.9");
		assertEquals(v.getMajorVersion(), 10);
		assertEquals(v.getMinorVersion(), 0);
		assertFalse(v.isTypeX());
		assertTrue(v.isTypeR());
		assertEquals(v.getTypeMajorVersion(), 4);
		assertEquals(v.getTypeMinorVersion(), 9);
		assertEquals(v.toString(), "10.0R4.9");
	}
	
	@Test
	public void testVersion5() {
		Version v = new Version("15.1X49-D40.6");
		assertEquals(v.getMajorVersion(), 15);
		assertEquals(v.getMinorVersion(), 1);
		assertTrue(v.isTypeX());
		assertFalse(v.isTypeR());
		assertEquals(v.getTypeMajorVersion(), 49);
		assertEquals(v.getTypeMinorVersion(), 0);
		assertTrue(v.hasDVersion());
		assertEquals(v.getDMajorVersion(), 40);
		assertEquals(v.getDMinorVersion(), 6);
		assertEquals(v.toString(), "15.1X49-D40.6");
	}

	@Test
	public void testIsNewerThan12_1X47() {
		assertFalse(Version.isNewerThan12_1X47("12.1X46"));
		assertFalse(Version.isNewerThan12_1X47("12.1X45"));
		assertFalse(Version.isNewerThan12_1X47("12.1X44"));
		assertFalse(Version.isNewerThan12_1X47("10.4R3.4"));
		assertFalse(Version.isNewerThan12_1X47("12.1R47"));
		assertTrue(Version.isNewerThan12_1X47("12.1X47"));
		assertTrue(Version.isNewerThan12_1X47("12.3X48"));
		assertTrue(Version.isNewerThan12_1X47("15.1"));
	}
	
	@Test
	public void testIsSipHiddenVersion() {
		assertTrue(Version.isSipHiddenVersion("15.1X49-D40"));
		assertTrue(Version.isSipHiddenVersion("15.1X49-D40.6"));
		assertTrue(Version.isSipHiddenVersion("15.1X49-D40.1"));
		assertTrue(Version.isSipHiddenVersion("15.1X49.2-D40.1"));
		assertTrue(Version.isSipHiddenVersion("15.1X50"));
		assertTrue(Version.isSipHiddenVersion("15.2X48-D39.0"));
		assertTrue(Version.isSipHiddenVersion("16.2X48-D39.0"));
		assertTrue(Version.isSipHiddenVersion("15.2X48"));
		assertTrue(Version.isSipHiddenVersion("16.0X48"));
		assertFalse(Version.isSipHiddenVersion("15.1X49-D39"));
		assertFalse(Version.isSipHiddenVersion("15.1X49"));
		assertFalse(Version.isSipHiddenVersion("15.1X48-D40"));
		assertFalse(Version.isSipHiddenVersion("15.1X48"));
		assertFalse(Version.isSipHiddenVersion("15.0X49-D40"));
		assertFalse(Version.isSipHiddenVersion("15.0"));
		assertFalse(Version.isSipHiddenVersion("14.1X49-D40"));
		assertFalse(Version.isSipHiddenVersion("15.1R49-D40"));
		assertFalse(Version.isSipHiddenVersion("15.1X49"));
	}
}
