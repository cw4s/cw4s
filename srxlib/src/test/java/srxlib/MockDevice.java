package srxlib;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import to.lef.srxlib.*;

import org.apache.commons.lang3.StringUtils;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;

public class MockDevice {
	public static Device instance(File dir) throws Throwable {
		return instance(dir, new HashMap<String, String>());
	}
	
	public static Device instance(File dir, Map<String, String> map) throws Throwable {
		Device d = new Device("192.168.10.10", "root", "dummy", dir);
		Device ret = Mockito.spy(d);

		for(String name: Device.cacheNameMap()) {
			doReturn(loadString(fileName(map, name))).when(ret).getCachedRpcReply(eq(name), Mockito.anyLong());
		}
		doReturn(loadString(fileName(map, Device.CACHE_NAME_EDIT_CONFIG))).when(ret).editingConfigString();
		
		return ret;
	}

	private static String fileName(Map<String, String> map, String key) {
		String ret = map.get(key);
		if (StringUtils.isBlank(ret))
			return key + ".xml";
		return ret;
	}
	
	private static String loadString(String name) {
		try {
			URL url = MockDevice.class.getClassLoader().getResource(name);
			if (url == null)
				System.out.println(name + " is null");
			byte[] b = Files.readAllBytes(Paths.get(url.toURI()));

			return new String(b, StandardCharsets.UTF_8);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
}
