package srxlib;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import to.lef.srxlib.*;
import to.lef.srxlib.nic.LogicalInterface;
import to.lef.srxlib.nic.PortInetInterface;

public class PortInetInterfaceTest {
	@ClassRule
	public static TemporaryFolder testFolder = new TemporaryFolder();
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test2014_0527_001() throws Throwable {
		final String phyIntName = "ge-0/0/0";
		final int unitId = 0;
		
		@SuppressWarnings("serial")
		Device device = MockDevice.instance(testFolder.getRoot(), new HashMap<String, String>() {
			{
				put(Device.CACHE_NAME_EDIT_CONFIG, "2014-0527-001.xml");
			}
		});
		DeviceConfig config = device.config();
		
		PortInetInterface int1 = new PortInetInterface(phyIntName, unitId);
		int1.setInet4AddressAndMask("192.168.10.1", 28);
		int1.setZoneName("untrust");
		int1.getCreateConfigure().config(config, device);
		
		LogicalInterface int2 = config.findLogicalInterface(phyIntName, unitId);
		assertEquals(int1, int2);
	}

}
