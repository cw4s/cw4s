package srxlib;

import static org.junit.Assert.*;

import java.net.UnknownHostException;

import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import to.lef.srxlib.*;
import to.lef.srxlib.nat.ProxyArp;

public class ProxyArpTest {
	@ClassRule
	public static TemporaryFolder testFolder = new TemporaryFolder();
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEquals() throws UnknownHostException {
		ProxyArp o1 = new ProxyArp("ge-0/0/0.1", "192.168.160.82", null, "192.168.160.88", null);
		ProxyArp o2 = new ProxyArp("ge-0/0/0.1", "192.168.160.82", null, "192.168.160.88", null);
		
		assertEquals(o1, o2);
	}

	@Test
	public void testIsExist() throws Throwable {
		Device device = MockDevice.instance(testFolder.getRoot());
		DeviceConfig config = device.config();
		
		ProxyArp o1 = new ProxyArp("ge-0/0/0.1", "192.168.160.82", null, "192.168.160.88", null);
		o1.getCreateConfigure().config(config, device);
		assertTrue(o1.isExist(config));
		
		o1.getDeleteConfigure().config(config, device);
		assertFalse(o1.isExist(config));
	}
}
