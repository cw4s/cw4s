package srxlib;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.util.ArrayList;
import java.util.Arrays;

import to.lef.srxlib.*;
import to.lef.srxlib.policy.ApplicationElement;
import to.lef.srxlib.policy.ApplicationSet;
import to.lef.srxlib.policy.ApplicationTerm;
import to.lef.srxlib.policy.Policy;

public class ApplicationElementTest {
	@ClassRule
	public static TemporaryFolder testFolder = new TemporaryFolder();

	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
	protected static Device device;
	protected DeviceConfig config;

	@BeforeClass
	public static void setUpBeforeClass() throws Throwable {
		device = MockDevice.instance(testFolder.getRoot());
	}

	@Before
	public void setUp() throws Throwable {
		config = device.editingConfig();
	}

	@Test
	public void testCreateAndRemove() throws Throwable {
		final String name = "test-app-1";
	
		ApplicationElement app1 = ApplicationElement.createInstance(name, 10, 
			Arrays.asList(
				ApplicationTerm.createCustomInstance("t1", "tcp", "", "", "80", "80", "", "", "")
					));
		app1.getCreateCustomConfigure().config(config, device);
		
		ApplicationElement app2 = config.findApplication(name);
		assertNotNull(app2);		
	
		assertTrue(app1.equals(app2));
		
		assertTrue(ApplicationElement.isRemovable(app1.getName(), config));
		assertTrue(app1.isRemovable(config));
		
		assertFalse(ApplicationElement.isInUse(app1.getName(), config));
		assertFalse(app1.isInUse(config));
		
		ApplicationElement.getDeleteConfigure(app1.getName()).config(config, device);
		
		app2 = config.findApplication(app1.getName());
		assertNull(app2);		
	}
	
	@Test
	public void testInUseApplicationSet() throws Throwable {
		final String name ="test-app-1";
		
		ApplicationElement app1 = ApplicationElement.createInstance(name, 10, 
				Arrays.asList(
						ApplicationTerm.createCustomInstance("t1", "tcp", "", "", "80", "80", "", "", "")
						));
		app1.getCreateCustomConfigure().config(config, device);

		ApplicationSet set = new ApplicationSet("test-app-set1", Arrays.asList(name), new ArrayList<String>());
		set.getCreateConfigure().config(config, device);

		assertTrue(app1.isInUse(config));
	}

	@Test
	public void testInUsePolicy() throws Throwable {
		final String name ="test-app-1";
		
		ApplicationElement app1 = ApplicationElement.createInstance(name, 10, 
				Arrays.asList(
						ApplicationTerm.createCustomInstance("t1", "tcp", "", "", "80", "80", "", "", "")
						));
		app1.getCreateCustomConfigure().config(config, device);

		Policy policy = new Policy("test-policy-1", "trust", "untrust", 
				Arrays.asList("any"), Arrays.asList("any"), Arrays.asList(name),
				Policy.ACTION_PERMIT, false, false);
		policy.getCreateConfigure().config(config, device);

		assertTrue(app1.isInUse(config));
	}

	@Test
	public void testEdit1() throws Throwable {
		final String name1 = "test-app-1";
		final String name2 = "test-app-2";
		
		ApplicationElement app1 = ApplicationElement.createInstance(name1, 10, 
				Arrays.asList(
						ApplicationTerm.createCustomInstance("", "tcp", "", "", "80", "80", "", "", ""),
						ApplicationTerm.createCustomInstance("", "tcp", "", "", "8080", "8008", "", "", "")
						));
		app1.getCreateCustomConfigure().config(config, device);
	
		ApplicationElement app2 = ApplicationElement.createInstance(name2, 20, 
				Arrays.asList(
						ApplicationTerm.createCustomInstance("t1", "tcp", "", "", "443", "443", "", "", "")
						));
		app2.getEditCustomConfigure(app1).config(config, device);
		
		
		ApplicationElement app3 = config.findApplication(name1);
		assertNull(app3);
		
		app3 = config.findApplication(name2);
		assertNotNull(app3);
		
		assertTrue(app2.equals(app3));
	}
	
	@Test
	public void testEdit2() throws Throwable {
		final String name1 = "test-app-1";
		final String name2 = "test-app-2";
		
		ApplicationElement app1 = ApplicationElement.createInstance(name1, 10, 
				Arrays.asList(
						ApplicationTerm.createCustomInstance("t1", "tcp", "", "", "80", "80", "", "", "")
						));
		app1.getCreateCustomConfigure().config(config, device);
	
		ApplicationElement app2 = ApplicationElement.createInstance(name2, 20, 
				Arrays.asList(
						ApplicationTerm.createCustomInstance("t2", "tcp", "", "", "443", "443", "", "", ""),
						ApplicationTerm.createCustomInstance("t1", "tcp", "", "", "8080", "8081", "", "", "")
						));
		
		app2.getEditCustomConfigure(app1).config(config, device);
		
		
		ApplicationElement app3 = config.findApplication(name1);
		assertNull(app3);
		
		app3 = config.findApplication(name2);
		assertNotNull(app3);
	
		assertTrue(app2.equals(app3));
	}
	
	@Test
	public void testEdit3() throws Throwable {
		final String name1 = "test-app-1";
		final String name2 = "test-app-2";
	
		final String set1name = "tset-app-set1";
		final String policy1name = "tset-policy1";
		
		ApplicationElement app1 = ApplicationElement.createInstance(name1, 10, 
				Arrays.asList(
						ApplicationTerm.createCustomInstance("t1", "tcp", "", "", "80", "80", "", "", "")
						));
		app1.getCreateCustomConfigure().config(config, device);
	
		
		ApplicationSet set1 = new ApplicationSet(set1name, Arrays.asList(name1), new ArrayList<String>());
		set1.getCreateConfigure().config(config, device);

		ApplicationSet set2 = config.findApplicationSet(set1name);
		assertTrue(set2.getApplicationNameList().contains(name1));
		
		Policy policy1 = new Policy(policy1name, "trust", "untrust",
				Arrays.asList("any"), Arrays.asList("any"), Arrays.asList(name1), 
				Policy.ACTION_PERMIT, false, false);
		policy1.getCreateConfigure().config(config, device);
		
		Policy policy2 = config.findPolicy("trust", "untrust", policy1name);
		assertTrue(policy2.getApplicationNameList().contains(name1));
		
		ApplicationElement app2 = ApplicationElement.createInstance(name2, 20, 
				Arrays.asList(
						ApplicationTerm.createCustomInstance("t2", "tcp", "", "", "443", "443", "", "", ""),
						ApplicationTerm.createCustomInstance("t1", "tcp", "", "", "8080", "8081", "", "", "")
						));
		
		app2.getEditCustomConfigure(app1).config(config, device);
			
		set2 = config.findApplicationSet(set1name);
		assertFalse(set2.getApplicationNameList().contains(name1));
		assertTrue(set2.getApplicationNameList().contains(name2));

		policy2 = config.findPolicy("trust", "untrust", policy1name);
		assertFalse(policy2.getApplicationNameList().contains(name1));
		assertTrue(policy2.getApplicationNameList().contains(name2));
	}
	
	@Test
	public void testEqual() throws Throwable {
		final String name1 = "test-app-1";
		
		ApplicationElement app1 = ApplicationElement.createInstance(name1, 10, 
				Arrays.asList(
						ApplicationTerm.createCustomInstance("t2", "tcp", "", "", "443", "443", "", "", ""),
						ApplicationTerm.createCustomInstance("t1", "tcp", "", "", "8080", "8081", "", "", "")
						));

	
		ApplicationElement app2 = ApplicationElement.createInstance(name1, 10, 
				Arrays.asList(
						ApplicationTerm.createCustomInstance("t1", "tcp", "", "", "8080", "8081", "", "", ""),
						ApplicationTerm.createCustomInstance("t2", "tcp", "", "", "443", "443", "", "", "")
						));
		
		assertTrue(app1.equals(app2));
	}
	
	@Test
	public void test2014_0528_001_1() throws Throwable {
		final String name1 = "test-app-1";
		
		thrown.expect(DeviceConfigException.class);
		thrown.expectMessage("No valid service.");
		
		DeviceConfig config = device.config();
		ApplicationElement app1 = ApplicationElement.createInstance(name1, 10, new ArrayList<ApplicationTerm>());
	
		app1.getCreateCustomConfigure().config(config, device);
	}
	
	@Test
	public void test2014_0528_001_2() throws Throwable {
		final String name1 = "test-app-1";
		
		thrown.expect(DeviceConfigException.class);
		thrown.expectMessage("No valid service.");
		
		DeviceConfig config = device.config();
		ApplicationElement app1 = ApplicationElement.createInstance(name1, 10, 
				Arrays.asList(
						ApplicationTerm.createCustomInstance("t2", "tcp", "", "", "443", "443", "", "", ""),
						ApplicationTerm.createCustomInstance("t1", "tcp", "", "", "8080", "8081", "", "", "")
						));

		app1.getCreateCustomConfigure().config(config, device);
	
		ApplicationElement app2 = config.findApplication(name1);
		assertNotNull(app2);
		assertTrue(app1.equals(app2));
		
		app2 = ApplicationElement.createInstance(name1, 10, new ArrayList<ApplicationTerm>());
		
		app2.getEditCustomConfigure(app1).config(config, device);
	}
}
