package srxlib;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import to.lef.srxlib.*;
import to.lef.srxlib.policy.ApplicationElement;
import to.lef.srxlib.policy.ApplicationSet;
import to.lef.srxlib.policy.ApplicationTerm;
import to.lef.srxlib.policy.Policy;

public class ApplicationSetTest {
	@ClassRule
	public static TemporaryFolder testFolder = new TemporaryFolder();
	
	protected static Device device;
	protected DeviceConfig config;

	protected ApplicationElement app_1;
	protected ApplicationElement app_2;
	protected ApplicationSet     set_1;
	protected ApplicationSet     set_2;
	protected static final String untrust = "untrust";
	protected static final String trust   = "trust";
	
	@BeforeClass
	public static void setUpBeforeClass() throws Throwable {
		device = MockDevice.instance(testFolder.getRoot());	
	}

	@Before
	public void setUp() throws Throwable {
		config = device.editingConfig();
				
		app_1 = ApplicationElement.createInstance("test-app-pre1", 10, 
				Arrays.asList(
						ApplicationTerm.createCustomInstance("t1", "tcp", "", "", "80", "80", "", "", "")
						));
		app_1.getCreateCustomConfigure().config(config, device);
	
		app_2 = ApplicationElement.createInstance("test-app-pre2", 10, 
				Arrays.asList(
						ApplicationTerm.createCustomInstance("t1", "tcp", "", "", "443", "443", "", "", "")
						));
		app_2.getCreateCustomConfigure().config(config, device);
		
		ApplicationElement a = ApplicationElement.createInstance("test-app-for-set-1", 10, 
				Arrays.asList(
						ApplicationTerm.createCustomInstance("t1", "tcp", "", "", "554", "554", "", "", "")
						));
		a.getCreateCustomConfigure().config(config, device);
		
		set_1 = new ApplicationSet("test-app-set-pre1", Arrays.asList(a.getName()), new ArrayList<String>());
		set_1.getCreateConfigure().config(config, device);
	
		a = ApplicationElement.createInstance("test-app-for-set-2", 10, 
				Arrays.asList(
						ApplicationTerm.createCustomInstance("t1", "tcp", "", "", "8080", "8080", "", "", "")
						));
		a.getCreateCustomConfigure().config(config, device);
		
		set_2 = new ApplicationSet("test-app-set-pre2", Arrays.asList(a.getName()), new ArrayList<String>());
		set_2.getCreateConfigure().config(config, device);	
	}

	@Test
	public void testEquals() throws Throwable {
		ApplicationSet set1 = new ApplicationSet("test-app-1", 
				Arrays.asList(
						app_1.getName(),
						app_2.getName()
						), 
				Arrays.asList(
						set_1.getName(),
						set_2.getName()
						));

		ApplicationSet set2 = new ApplicationSet("test-app-1", 
				Arrays.asList(
						app_2.getName(),
						app_1.getName()
						), 
				Arrays.asList(
						set_1.getName(),
						set_2.getName()
						));
		
		assertTrue(set1.equals(set2));
		
		set2 = new ApplicationSet("test-app-1", 
				Arrays.asList(
						app_1.getName(),
						app_2.getName()
						), 
				Arrays.asList(
						set_2.getName(),
						set_1.getName()
						));
		
		assertTrue(set1.equals(set2));
	}
	
	@Test
	public void testCreateAndRemove() throws Throwable {
		final String name = "test-app-set1";
		
		ApplicationSet set1 = new ApplicationSet(name, Arrays.asList(app_1.getName()), Arrays.asList(set_1.getName()));
		set1.getCreateConfigure().config(config, device);
		
		ApplicationSet set2 = config.findApplicationSet(name);
		assertNotNull(set2);
	
		assertTrue(set1.equals(set2));
		assertTrue(set2.getApplicationNameList().contains(app_1.getName()));
		assertTrue(set2.getApplicationSetNameList().contains(set_1.getName()));
		
		assertTrue(ApplicationSet.isRemovable(name, config));
		assertTrue(set1.isRemovable(config));
		assertFalse(ApplicationSet.isInUse(name, config));
		assertFalse(set1.isInUse(config));
		
		ApplicationSet.getDeleteConfigure(name).config(config, device);
		set2 = config.findApplicationSet(name);
		
		assertNull(set2);
	}
	
	@Test
	public void testInUseAddressSet() throws Throwable {
		final String name ="test-app-set1";
		
		ApplicationSet set1 = new ApplicationSet(name, Arrays.asList(app_1.getName()), Arrays.asList(set_1.getName()));
		set1.getCreateConfigure().config(config, device);

		assertTrue(set_1.isInUse(config));	
	}
	
	@Test
	public void testInUsePolicyFrom() throws Throwable {
		Policy policy = new Policy("policy-1", untrust, trust, 
				Arrays.asList("any"), Arrays.asList("any"), Arrays.asList(set_1.getName()), 
				Policy.ACTION_PERMIT, false, false);
		policy.getCreateConfigure().config(config, device);
		
		assertTrue(set_1.isInUse(config));
	}
	
	@Test
	public void testEdit1() throws Throwable {
		final String name1 = "test-app-set-1";
		final String name2 = "test-app-set-2";
		final String name3 = "test-app-set-3";

		final String policy1name = "tset-policy1";
		
		ApplicationSet set1 = new ApplicationSet(name1, 
				Arrays.asList(app_1.getName(), app_2.getName()),
				Arrays.asList(set_1.getName(), set_2.getName()));
		set1.getCreateConfigure().config(config, device);
	
		ApplicationSet set3 = new ApplicationSet(name3, new ArrayList<String>(), Arrays.asList(set1.getName()));
		set3.getCreateConfigure().config(config, device);

		ApplicationSet set4 = config.findApplicationSet(name3);
		assertTrue(set4.getApplicationSetNameList().contains(name1));
		
		Policy policy1 = new Policy(policy1name, untrust, "trust",
				Arrays.asList("any"), Arrays.asList("any"), Arrays.asList(name1), 
				Policy.ACTION_PERMIT, false, false);
		policy1.getCreateConfigure().config(config, device);
		
		Policy policy2 = config.findPolicy(untrust, "trust", policy1name);
		assertTrue(policy2.getApplicationNameList().contains(name1));
				
		ApplicationSet set2 = new ApplicationSet(name2, 
				Arrays.asList(app_1.getName()),
				Arrays.asList(set_1.getName()));
		set2.getEditConfigure(set1).config(config, device);
		
		set3 = config.findApplicationSet(name3);
		assertFalse(set3.getApplicationSetNameList().contains(name1));
		assertTrue(set3.getApplicationSetNameList().contains(name2));
		
		policy2 = config.findPolicy(untrust, "trust", policy1name);
		assertFalse(policy2.getApplicationNameList().contains(name1));
		assertTrue(policy2.getApplicationNameList().contains(name2));
	}
}
