name := "srxlib"
maintainer := "Koji Ogawa <pierre@lef.to>"

version := "1.1.1"

scalaVersion := "2.12.8"

//javacOptions in ThisBuild ++= Seq("-Xlint:deprecation")

libraryDependencies ++= Seq(
    "org.mockito" % "mockito-core" % "2.27.0" % "test",
    "org.powermock" % "powermock-module-junit4" % "2.0.2" % "test",
    "org.powermock" % "powermock-api-mockito2" % "2.0.2" % "test",
    "com.novocode" % "junit-interface" % "0.11" % "test->default",
    "org.json" % "json" % "20180813",
    "org.apache.commons" % "commons-lang3" % "3.8.1",
    "commons-codec" % "commons-codec" % "1.11",
    "com.j256.ormlite" % "ormlite-core" % "5.1",
    "com.j256.ormlite" % "ormlite-jdbc" % "5.1",
    "org.xerial" % "sqlite-jdbc" % "3.27.2.1",
    "org.xerial.thirdparty" % "nestedvm" % "1.0",
    ("commons-validator" % "commons-validator" % "1.6").exclude("commons-beanutils","commons-beanutils"),
//    "commons-validator" % "commons-validator" % "1.4.1",
    "net.java.dev.jna" % "jna" % "5.2.0"
)
