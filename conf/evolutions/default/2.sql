# --- !Ups

alter table "devices" add column persistent INTEGER default 0;

# --- !Downs

alter table "devices" drop column persistent;

