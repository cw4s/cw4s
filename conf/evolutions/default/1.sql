# --- !Ups

create table "devices" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"name" varchar(255) NOT NULL,"hostname" varchar(255) NOT NULL,"port" INTEGER NOT NULL,"username" varchar(255) NOT NULL,"password" varchar(255) NOT NULL,"createdAt" BIGINT NOT NULL,"updatedAt" BIGINT NOT NULL);
create unique index "idx_name" on "devices" ("name");

# --- !Downs

drop table "devices";

