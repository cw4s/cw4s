import PlayKeys._
import play.twirl.sbt.Import._
//import AssemblyKeys._
import com.typesafe.config._

scalaVersion := "2.12.8"

name := "cw4s"
maintainer := "Koji Ogawa <pierre@lef.to>"

val conf = ConfigFactory.parseFile(new File("conf/application.conf")).resolve()

version := conf.getString("app.version")

libraryDependencies ++= Seq(
  filters,
  evolutions,
  guice,
  "org.xerial" % "sqlite-jdbc" % "3.27.2.1",
  "org.apache.commons" % "commons-lang3" % "3.8.1",
  "com.typesafe.slick" %% "slick" % "3.3.0",
  "com.typesafe.play" %% "play-slick" % "4.0.0",
  "com.typesafe.play" %% "play-slick-evolutions" % "4.0.0",
  "org.webjars" %% "webjars-play" % "2.7.0",
  "org.webjars" % "jquery" % "1.12.4",
  "org.webjars" % "jquery-ui" % "1.11.4",
  "org.webjars" % "html5shiv" % "3.7.2",
  "org.webjars" % "respond" % "1.4.2",
  "org.webjars" % "jquery-blockui" % "2.65",
  "org.webjars.npm" % "codemirror-minified" % "5.16.0",
  "org.webjars" % "google-diff-match-patch" % "20121119-1",
  "com.typesafe.play" %% "play-json" % "2.7.0"
)

//scalacOptions ++= List("-Ybackend:GenBCode", "-Ydelambdafy:method", "-target:jvm-1.8")

// play.Project.playScalaSettings

TwirlKeys.templateImports ++= Seq(
    "org.apache.commons.lang3.StringEscapeUtils.escapeEcmaScript",
    "scala.collection.JavaConverters._",
    "to.lef.srxlib",
    "models.Forms._"
)


lazy val cw4s = project.in(file("."))
    .enablePlugins(PlayScala)
    .enablePlugins(SbtWeb)
    .enablePlugins(LauncherJarPlugin)
    .aggregate(srxlib)
    .dependsOn(srxlib)

pipelineStages in Assets := Seq(uglify)

includeFilter in (Assets, LessKeys.less) := "*.less"

excludeFilter in (Assets, LessKeys.less) := "_*.less"

lazy val srxlib = project

mainClass in assembly := Some("play.core.server.ProdServerStart")

fullClasspath in assembly += Attributed.blank(PlayKeys.playPackageAssets.value)

test in assembly := {}


assemblyExcludedJars in assembly := {
      val cp = (fullClasspath in assembly).value
      cp filter { jar =>
        jar.data.getName == "commons-logging-1.2.jar" ||
        jar.data.getName == "commons-collections-3.2.1.jar"
      }
}

assemblyMergeStrategy in assembly := {
  case manifest if manifest.contains("MANIFEST.MF") =>
    // We don't need manifest files since sbt-assembly will create
    // one with the given settings
    MergeStrategy.discard
  case referenceOverrides if referenceOverrides.contains("reference-overrides.conf") =>
    // Keep the content for all reference-overrides.conf files
    MergeStrategy.concat
  case x =>
    // For all the other files, use the default sbt-assembly merge strategy
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}

//assemblyMergeStrategy in assembly := {
//    case PathList("META-INF", "io.netty.versions.properties") => MergeStrategy.last
//    case x =>
//        val oldStrategy = (assemblyMergeStrategy in assembly).value
//        oldStrategy(x)
//}

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator

// Compile the project before generating Eclipse files, so that generated .scala or .class files for views and routes are present
EclipseKeys.preTasks := Seq(compile in Compile)
